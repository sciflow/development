FROM node:22-alpine3.20

WORKDIR /usr/local/build

RUN apk add --no-cache \
         git \
         git \
         graphicsmagick

ENV PANDOC_VERSION=3.1.12.1
RUN wget https://github.com/jgm/pandoc/releases/download/$PANDOC_VERSION/pandoc-$PANDOC_VERSION-linux-amd64.tar.gz && \
    tar xvzf pandoc-$PANDOC_VERSION-linux-amd64.tar.gz --strip-components 1 -C /usr/local/

RUN wget https://github.com/lierdakil/pandoc-crossref/releases/download/v0.3.17.0f/pandoc-crossref-Linux.tar.xz && \
    tar -xf pandoc-crossref-Linux.tar.xz && \
    mv pandoc-crossref /usr/local/bin/

WORKDIR /usr/src

RUN rm -rf /usr/local/build
