FROM node:20-bookworm

# Dependencies used for various tools (pandoc, princexml, libreoffice)
RUN apt-get -q --no-allow-insecure-repositories update \
  && DEBIAN_FRONTEND=noninteractive \
  apt-get install --assume-yes --no-install-recommends \
  git \
  wget \
  cabextract \
  xz-utils \
  ca-certificates=* \
  locales \
  ca-certificates-java \
  openjdk-17-jre-headless \
  openjdk-17-jre \
  openjdk-17-jdk-headless \
  openjdk-17-jdk \
  pdftk \
  zlib1g-dev \
  openssh-client \
  tex-gyre \
  gdebi \
  fonts-stix \
  fonts-texgyre \
  fonts-lmodern \
  libcairo2 \
  libfreetype6 \
  libgif7 \
  libgomp1 \
  libjpeg-dev \
  libpixman-1-0 \
  libxml2 \
  graphicsmagick \
  inkscape \
  && rm -rf /var/lib/apt/lists/*
