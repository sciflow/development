FROM docker:26.1.3-dind-alpine3.19

RUN echo "http://dl-cdn.alpinelinux.org/alpine/v3.19/main" >> /etc/apk/repositories && \
    echo "http://dl-cdn.alpinelinux.org/alpine/v3.19/community" >> /etc/apk/repositories

RUN apk update && apk add --no-cache --virtual .gyp \
    nodejs \
    npm \
    yarn \
    python3 \
    py3-pip \
    scons \
    py3-psutil \
    py3-yaml \
    py3-cheetah \
    git \
    gcc \
    g++ \
    make \
    openssl-dev \
    linux-headers \
    cmake \
    curl-dev \
    chromium

RUN node --version | grep -E "^v20\." || (echo "Unsupported Node.js version" && exit 1)

RUN echo -e "http://dl-cdn.alpinelinux.org/alpine/edge/testing" > /etc/apk/repositories
#RUN yarn global add node-gyp

ENV CHROME_BIN='/usr/bin/chromium-browser'

RUN rm -rf /var/cache/apk/*
