import { JSONSchema7 } from "json-schema";
import { Observable } from "rxjs";

import { EditorState, Transaction } from 'prosemirror-state';

export interface JSONPatch {
    /** Operation to perform: add, remove, replace, move, copy, or test */
    op: 'add' | 'remove' | 'replace' | 'move' | 'copy' | 'test';
    /** JSON Pointer string specifying the target location for the operation */
    path: string;
    /** The value to be used within the operations (required for add, replace, test) */
    value?: any;
    /** The source path (required for move and copy) */
    from?: string;
}

export interface PluginRunState {
    /** An array of PartState objects representing the document parts */
    readonly parts: ReadonlyArray<PartState>;
    /** The manuscript JSON */
    readonly manuscript: any;
}

export interface PartState {
    /** Unique identifier for the document part */
    readonly partId: string;
    /** ProseMirror EditorState for the document part */
    readonly state: EditorState;
}

export interface PartTransaction {
    /** Unique identifier for the document part */
    partId: string;
    /** ProseMirror Transaction to be applied */
    tr: Transaction;
}

export interface TagFunction {
    /**
     * Processes document parts to add tag marks for later processing.
     * @param parts - An array of PartState objects representing the document parts.
     * @param manuscript the document JSON
     * @returns A Promise resolving to an array of PartTransaction objects or an Observable emitting PartTransaction objects.
     */
    (state: Readonly<PluginRunState>): Promise<Action[]> | Observable<Action[]>;
}

export interface PatchFunction {
    /**
     * Identifies actions to transform the document and executes them.
     * @param parts - An array of PartState objects representing the document parts.
     * * @param manuscript the document JSON
     * @param userInput - (Optional) User input collected from previous executions.
     * @returns A Promise resolving to an array of Action objects or an Observable emitting Action objects.
     */
    (state: Readonly<PluginRunState>, userInput?: any): Promise<Action[]> | Observable<Action[]>;
}

export interface Message {
    /** The plugin or system that created the action */
    source: string;
    /** A message for the user */
    message?: string;
    /** Additional data payload */
    payload?: any;
    /** The user ID or system ID that created the action */
    userId: string;
    /** Timestamp in UTC when the action was created */
    timestamp: string;
    runId: string;
}

export interface Action extends Message {
    /** A patch applied to the manuscript's JSON structure (excluding ProseMirror document parts) */
    jsonPatches?: JSONPatch[];
    /** A transaction applied to a specific part of the ProseMirror document */
    transactions?: PartTransaction[];
    /** A UI to render for a user in order to get a decision */
    schema?: JSONSchema7;
}

export interface PluginManifest {
    /** Name of the plugin */
    name: string;

    /** Version of the plugin */
    version: string;

    /** Description of what the plugin does */
    description?: string;

    /** Events or triggers that cause the plugin to run */
    triggers: string[];

    /** Optional priority to determine execution order */
    priority?: number;

    /** Other custom metadata fields as needed */
    [key: string]: any;
}

export interface Plugin {
    /** Unique identifier for the plugin */
    id: string;

    /** Manifest content from plugin.yml */
    manifest: PluginManifest;

    /** Tagging function exported by the plugin */
    tag?: TagFunction;

    /** Patch function exported by the plugin */
    patch?: PatchFunction;

    triggers: {
        event: string;
    }[];

    /** Execution priority of the plugin (higher runs first) */
    priority: number;
}