export async function tag(input) {
  const actions = [];

  const { parts, manuscript } = input;

  for (const part of parts) {
    const state = part.state;
    let tr = state.tr;
    const tagMark = state.schema.marks.tags;

    state.doc.descendants((node, pos) => {
      // example of tagging all paragraph contents
      if (node.type.name === 'paragraph') {
        tr = tr.addMark(pos, pos + node.nodeSize, tagMark.create({ tags: [{ key: 'found-one' }] }));
      }
    });

    actions.push({ partId: part.partId, transactions: [{ partId: part.partId, tr }] });
  }

  return actions;
}
