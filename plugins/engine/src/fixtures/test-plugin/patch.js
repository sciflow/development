export async function getActions(
    parts
) {
    const actions = [];
    for (const { partId, state } of parts) {
        state.doc.descendants((node) => {
            if (node.type.name === 'image' && !node.attrs.alt) {
                const key = `image-${partId}-${node.attrs.id}`;
                
                actions.push({
                    source: 'my-plugin',
                    message: `Found image without alt text in part ${partId}: ${node.attrs.src}`,
                    userId: 'system',
                    timestamp: new Date().toISOString(),
                });
            }
        });
    }
    
    return actions;
}