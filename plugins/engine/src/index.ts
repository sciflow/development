import { catchError, filter, map, Observable, share, Subject, throwError, timeout } from "rxjs";
import { v4 as uuidv4 } from 'uuid';

import { loadPluginsFromDisk } from './helpers';
import { Action, PluginRunState, Plugin, PartState } from "./types";

export class PluginError extends Error {
    constructor(message: string, public payload: any) {
        super(message);

        // Set the prototype explicitly.
        Object.setPrototypeOf(this, PluginError.prototype);
    }

    toString() {
        return `${this.name}: ${this.message}`;
    }

    toJSON() {
        return { message: this.message, payload: this.payload };
    }
}

function isPromise<T>(obj: any): obj is Promise<T> {
    return !!obj && typeof obj.then === 'function';
}

function isObservable<T>(obj: any): obj is Observable<T> {
    return !!obj && typeof obj.subscribe === 'function';
}

const getActions = async (result: Promise<Action[]> | Observable<Action[]>): Promise<Action[]> => {
    if (isPromise(result)) {
        return result as Promise<Action[]>;
    } else if (isObservable(result)) {
        // for now we only wait for the observable to finish
        return new Promise<Action[]>((resolve, reject) => {
            const actions: Action[] = [];
            result
                .pipe(
                    timeout(120000), // Automatically throws a TimeoutError if tagResult does not emit within 120 seconds
                    catchError(() => throwError(() => new Error('Plugin timed out')))
                )
                .subscribe({
                    complete: () => resolve(actions),
                    next: (actions) => actions && actions.push(...actions),
                    error: (err) => { reject(err) }
                });
        });
    } else {
        return [];
    }
}

export class PluginEngine {
    /** list of available plugins */
    plugins: Promise<Plugin[]> = Promise.resolve([]);

    runCache: { [key: string]: PluginRunState; } = {};

    updates$ = new Subject<Action[]>();

    constructor(private PLUGIN_DIRS: string) {
        this.plugins = this.loadPlugins();
    }

    /**
     * Starts a run.
     * @returns the run id
     */
    run(state: PluginRunState): string {
        const runId = uuidv4();
        this.runCache[runId] = { ...state };
        return runId;
    };

    observe(runId: string): Observable<Action[]> {
        return this.updates$.pipe(map((actions => actions.filter(a => a.runId === runId))), filter(a => a && a.length > 0), share());
    }

    getRunState(runId: string): PluginRunState {
        return this.runCache[runId];
    }

    async step(runId: string, event: string): Promise<void> {
        let state = this.runCache[runId];
        // TODO should we create a semaphore for a run cache?
        for (let plugin of await this.plugins) {
            if (!plugin.triggers?.some((e: any) => e.event === event)) { continue; }

            const updateState = async (result: Observable<Action[]> | Promise<Action[]>) => {
                const actions = await getActions(result);
                for (let action of actions) {
                    if (action.jsonPatches) {
                        // TODO
                        debugger;
                    }
                    if (action.transactions) {
                        for (let tr of action.transactions) {
                            let partEditorState = state.parts.find((p: PartState) => p.partId === tr.partId)
                            try {
                                const updatedEditorState = partEditorState?.state.applyTransaction(tr.tr);
                                if (updatedEditorState) {
                                    state = {
                                        ...state,
                                        parts: [...state.parts.map(s => s.partId === tr.partId ? { ...s, state: updatedEditorState?.state } : s)]
                                    }
                                    this.updates$.next([{ ...action, runId }]);
                                }
                            } catch (e) {
                                console.error(e);
                                debugger;
                            } finally {
                            }
                        }
                    }
                }
            };

            try {
                if (typeof plugin.tag === 'function') { await updateState(plugin.tag(state)); }
                if (typeof plugin.patch === 'function') { await updateState(plugin.patch(state, {})); }
            } catch (e: any) {
                debugger;
                throw new PluginError('Could not execute ' + plugin.id + ' plugin', { message: e.message });
            }
        }

        this.runCache[runId] = state;
    }

    private async loadPlugins(): Promise<Plugin[]> {
        if (!this.PLUGIN_DIRS) { return []; }
        const logger = {};
        const instances = await loadPluginsFromDisk(this.PLUGIN_DIRS, { logger });
        //logger.info('Loaded plugins', { ids: instances.map(instance => ({ id: instance.id, priority: instance.priority })) });
        return instances;
    }
}
