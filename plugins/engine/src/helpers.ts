import { existsSync, readFileSync } from "fs";
import { join, basename } from "path";
//@ts-ignore
import * as yaml from 'js-yaml';
import { sync } from "glob";
import { PluginManifest } from "./types";

export const loadPluginsFromDisk = async (PLUGIN_DIRS: string, { logger }: any) => {

    const dynamicImport = async (path: string) => {
        try {
            const loadedModule = await import(/* webpackIgnore: true */ path);
            return loadedModule;
        } catch (e: any) {
            debugger;
            logger.error('Could not load plugin module', { message: e.message, path });
        }
    }

    let instances: any[] = [];
    for (let PLUGIN_DIR of PLUGIN_DIRS?.split(',').map(s => s.trim()).filter(s => s?.length > 0) || []) {
        if (!PLUGIN_DIR) { continue; }
        const pluginDir = PLUGIN_DIRS && existsSync(PLUGIN_DIR) ? PLUGIN_DIR : undefined;
        if (pluginDir) {
            let plugins = sync(`${pluginDir}/*/plugin.yml`);
            for (let plugin of plugins) {
                if (existsSync(plugin)) {
                    const manifestFile = readFileSync(plugin, 'utf-8');
                    const manifest = yaml.load(manifestFile) as PluginManifest;
                    const moduleLocation = plugin.replace('/plugin.yml', '');
                    let tag, patch;
                    if (manifest.spec.tag && manifest.spec.tag.type === 'esm') {
                        if (existsSync(join(moduleLocation, manifest.spec.tag.source))) {
                            try {
                                const result = await dynamicImport(join(moduleLocation, manifest.spec.tag.source));
                                if (result.tag && typeof result.tag === 'function') {
                                    tag = result.tag;
                                }
                            } catch (e: any) {
                                logger.error('Could not load plugin module', { message: e.message, type: 'tag', manifest });
                            }
                        }
                    }

                    if (manifest.spec.patch && manifest.spec.patch.type === 'esm') {
                        if (existsSync(join(moduleLocation, manifest.spec.patch.source))) {
                            try {
                                const result = await dynamicImport(join(moduleLocation, manifest.spec.patch.source));
                                if (result.tag && typeof result.tag === 'function') {
                                    patch = result.tag;
                                }
                            } catch (e: any) {
                                debugger;
                                logger.error('Could not load plugin module', { message: e.message, type: 'patch', manifest });
                            }
                        }
                    }

                    const id = manifest.metadata?.name || basename(moduleLocation);
                    if (patch || tag) {
                        instances.push({
                            id,
                            manifest,
                            tag,
                            patch,
                            triggers: manifest.triggers || [],
                            priority: manifest.priority || 0
                        });
                    }
                }
            }
        }
    }

    instances = instances.sort((a, b) => b.priority - a.priority); // higher comes first
    return instances;
}