import { test } from 'node:test';
import assert from 'node:assert/strict';
import { PluginEngine } from './index.js';
import { join } from 'path';
import { Action } from './types.js';
import { EditorState } from 'prosemirror-state';
import { exampleSetup } from 'prosemirror-example-setup';
import { addListNodes } from 'prosemirror-schema-list';
import { schema as basicSchema } from 'prosemirror-schema-basic';
import { MarkSpec, Schema } from 'prosemirror-model';

test('Plugin engine', async () => {
  const tagsMark: MarkSpec = {
    attrs: { tags: { default: [] } },
    inclusives: false,
    toDOM(mark) { return ['span', { 'data-tags': JSON.stringify(mark.attrs.tags.map((tag: any) => tag.key).join(' ')) }]; },
    parseDOM: [{ tag: 'span[data-tags]' }]
  };

  const marks = basicSchema.spec.marks.addToEnd('tags', tagsMark);
  let nodes = addListNodes(basicSchema.spec.nodes, 'paragraph block*', 'block');

  const schema = new Schema({
    nodes,
    marks
  });

  const doc = schema.nodes.doc.create({}, schema.nodes.paragraph.create({ id: 'p1' }, [schema.text('A test paragraph.')]));

  const engine = new PluginEngine(join(__dirname, 'fixtures'));
  const part1 = EditorState.create({
    doc,
    schema
  });

  const runId = engine.run({ parts: [{ partId: '1', state: part1 }], manuscript: { authors: [] } });

  const actions = [];
  engine.observe(runId).subscribe((a: Action[]) => actions.push(...a));

  await engine.step(runId, 'test-stage');

  const state = engine.getRunState(runId);
  const partDoc = state.parts.find(p => p.partId === '1')?.state?.doc;
  const firstChild = partDoc?.content.child(0)?.child(0);
  const firstMarkType = firstChild?.marks[0]?.type?.name
  assert.equal(actions.length, 1);
  assert.equal(firstMarkType, 'tags');
});
