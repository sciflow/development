import { expect } from 'chai';
import compromise from 'compromise';
import { referencePlugin } from './referencePlugin.js';

// compromise.verbose(true);
compromise.plugin(referencePlugin);

describe('References plugin', () => {
    it('should correctly tag and extract components from reference entries', () => {
        const testCases = [
            { text: '[1] Forsythe, H., & Rivera, S. (2024). Climate Change and Renewable Energy: Exploring the Nexus. Journal of Environmental Science and Policy, 18(3), 234-256.', referenceNumber: '[1]', authors: 'Forsythe, H., & Rivera, S.', year: '(2024)', title: 'Climate Change and Renewable Energy: Exploring the Nexus', journal: 'Journal of Environmental Science and Policy', volume: '18', issue: '3', pages: '234-256' },
            { text: '[2] Chaudhari, A., Smith, J. A., & Nguyen, L. (2024). Machine Learning in Wildlife Conservation: A Data-Driven Approach. Artificial Intelligence Review, 49(4), 445-469.', referenceNumber: '[2]', authors: 'Chaudhari, A., Smith, J. A., & Nguyen, L.', year: '(2024)', title: 'Machine Learning in Wildlife Conservation: A Data-Driven Approach', journal: 'Artificial Intelligence Review', volume: '49', issue: '4', pages: '445-469' },
            { text: '[3] Johnson, E., Forsythe, H., & Rivera, S. (2024). Impacts of Sustainable Practices on Urban Development. International Journal of Urban Planning and Development, 5(2), 89-102.', referenceNumber: '[3]', authors: 'Johnson, E., Forsythe, H., & Rivera, S.', year: '(2024)', title: 'Impacts of Sustainable Practices on Urban Development', journal: 'International Journal of Urban Planning and Development', volume: '5', issue: '2', pages: '89-102' }
        ];

        testCases.forEach(({ text, referenceNumber, authors, year, title, journal, volume, issue, pages }) => {
            const doc = compromise(text);
            
            const extractedReferenceNumber = doc.match('#ReferenceNumber').out('text');

            expect(extractedReferenceNumber).to.equal(referenceNumber, 'Trying to extract reference number from ' + text);
        });
    });
});