import { SFMarkType, SFNodeType } from '@sciflow/schema';
import nlp from 'compromise';

import { EditorState } from 'prosemirror-state';
import { TransportManuscriptFile } from './types.js';

import { affiliationPlugin } from './authorPlugin.js';
import { authorPlugin } from './authorPlugin.js';
import { headingPlugin } from './headingPlugin.js';
import { abstractPlugin } from './abstractPlugin.js';
import { lexicon as names } from './helpers.js';

import { Node } from 'prosemirror-model';
import { matchNodesToMetaData, MetricData } from './metrics.js';

nlp.plugin({ words: names });
nlp.plugin(authorPlugin);
nlp.plugin(affiliationPlugin);
nlp.plugin(headingPlugin);
nlp.plugin(abstractPlugin);

export const sizeFromPtString = (s: string) => s?.match(/(\d+)pt/)?.[0];

export const extractTextSizes = (doc: Node) => {
  let sizes: any = {};

  doc.descendants((node, pos, parent) => {
    const styleMark = node?.marks?.find(m => m.type.name === 'style');
    if (styleMark) {
      const size = styleMark.attrs?.size?.match(/(\d+)pt/);
      if (size?.[1]) {
        if (!sizes[size?.[1]]) {
          sizes[size?.[1]] = { node, pos, parent, size: size?.[1] };
        }
      }
    }
  });

  return Object.keys(sizes).map(size => ({ size, ...sizes[size] })).sort((a, b) => b?.size - a?.size);
};

export const tag = (input: { state: EditorState, manuscript: TransportManuscriptFile, stats?: any; intelligence?: any; }) => {
  const manuscript = input.manuscript;
  let state = input.state;
  const tr = state.tr;
  let logs: any[] = [];
  const tagMarkType = input.state.schema.marks[SFMarkType.tags];

  const taggable = (node: Node) => [SFNodeType.paragraph, SFNodeType.header, SFNodeType.heading, SFNodeType.blockquote].includes(node.type.name);

  let prevMetrics: MetricData[] = [];

  // we sometimes get blockquotes that wrap the front because they were indented, we'd like to remove them
  let wrappingBlockquotes: Node[] = [];

  let i = 0;
  console.log(tr.doc.nodeSize, 'tagging document (meta data)');
  tr.doc.descendants((node: Node, pos: number, parent: Node | null) => {

    // not tagging big documents WIP
    if (pos > 15000) { return false; }

    // we do not process table content for titles and affiliations (as of now)
    if ([SFNodeType.table].includes(node?.type.name) && pos <= 2) { return false; }

    let match;
    // we match taggable nodes based on a metrics object instead of going through all matchers here
    if (taggable(node)) {
      try {
        const nodeMetrics = matchNodesToMetaData({ node, parent, stats: input.stats, intelligence: input.intelligence, pos, documentSize: tr.doc.nodeSize }, prevMetrics);
        prevMetrics.push(nodeMetrics);
        match = nodeMetrics.matches.sort((a, b) => b.confidence - a.confidence)?.[0];
        if (match && match.confidence > 0) {
          // console.log(i, match?.type, pos);
          let attrs = node.marks?.find(m => m.type.name == tagMarkType.name)?.attrs;
          tr.removeNodeMark(pos, tagMarkType);
          let tags = attrs?.tags || [];
          tags = [{ key: match.type, value: JSON.stringify({ data: match.data }) }, ...tags]
            .filter((v, index, array) => array.findIndex(v2 => v2.key === v.key) === index);
          tr.addNodeMark(pos, tagMarkType.create({
            ...(attrs || {}),
            tags
          }));
        }
      } catch (e: any) {
        console.error('Could not tag node', { type: match?.type, message: e.message, nodeType: node?.type?.name });
        debugger;
      }
    }

    // we don't want blockquotes around the front sections (stemming from indents in word that pandoc detected as a blockquote)
    if (parent?.type.name === SFNodeType.blockquote) {
      if (match?.type === 'authors') {
        wrappingBlockquotes.push(parent as Node);
      }
    }
  });

  for (let el of wrappingBlockquotes.filter((n, i, a) => a.indexOf(n) === i)) {
    tr.doc.descendants((node: Node, pos: number, _parent: Node | null) => {
      if (el.attrs?.id?.length > 0 && el.attrs.id === node.attrs.id) {
        let attrs = node.marks?.find(m => m.type.name == tagMarkType.name)?.attrs;
          tr.removeNodeMark(pos, tagMarkType);
          let tags = attrs?.tags || [];
          tags = [{ key: 'blockquote-wrapping-front', value: {}}, ...tags]
            .filter((v, index, array) => array.findIndex(v2 => v2.key === v.key) === index);
          tr.addNodeMark(pos, tagMarkType.create({
            ...(attrs || {}),
            tags
          }));
      }
    });
  }

  if (tr.docChanged) {
    state = state.apply(tr);
  }

  return { tr, state, logs, manuscript };
}
