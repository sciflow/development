export const keywordPlugin = {
    /** Add custom tags */
    tags: {
        Keyword: {
            isA: 'Concept',
        },
        KeywordsHeading: {
            isA: 'Heading',
        }
    },

    /** Post-process tagger */
    compute: {
        postProcessKeywords: (doc: any) => {
            doc.match('^(keywords|key words)').tag('KeywordsHeading', 'keywords-heading-pattern');
            if (doc.has('#KeywordsHeading')) {
                const term = doc.splitAfter('#KeywordsHeading').last();
                const keywords = term.text().split((/[,;]/)).map(s => s.trim());
                for (let keyword of keywords) { doc.match(keyword).tag('Keyword'); }
            }
        }
    },
    /** Run it on init */
    hooks: ['postProcessKeywords']
};