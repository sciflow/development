export const headingPlugin = {
    /** Add custom tags */
    tags: {
      Heading: {
        isA: 'Section',
      }
    },
  
    /** Post-process tagger */
    compute: {
      postProcessHeadings: (doc: any) => {
        const headings: { text: string; type?: string; locale?: string; numbering?: string; role?: string; }[] = [
          { text: 'Abstract', type: 'abstract', locale: 'en-US' },
          { text: 'Keywords', type: 'chapter', role: 'keywords', locale: 'en-US' },
          { text: 'References', type: 'bibliography', locale: 'en-US' },
          { text: 'Bibliography', type: 'bibliography', locale: 'en-US' },
          { text: 'Introduction', type: 'chapter', role: 'introduction', locale: 'en-US' },
          { text: 'Background', type: 'chapter', role: 'background', locale: 'en-US' },
          { text: 'Literature Review', type: 'chapter', role: 'literature-review', locale: 'en-US' },
          { text: 'Methods', type: 'chapter', role: 'methods', locale: 'en-US' },
          { text: 'Methodology', type: 'chapter', role: 'methods', locale: 'en-US' },
          { text: 'Materials', type: 'chapter', role: 'materials', locale: 'en-US' },
          { text: 'Data', type: 'chapter', role: 'data', locale: 'en-US' },
          { text: 'Results', type: 'chapter', role: 'results', locale: 'en-US' },
          { text: 'Discussion', type: 'chapter', role: 'discussion', locale: 'en-US' },
          { text: 'Conclusion', type: 'chapter', role: 'conclusion', locale: 'en-US' },
          { text: 'Conclusions and Future Work', type: 'chapter', role: 'conclusion', locale: 'en-US' },
          { text: 'Acknowledgements', type: 'chapter', role: 'acknowledgements', locale: 'en-US' },
          { text: 'Acknowledgments', type: 'chapter', role: 'acknowledgements', locale: 'en-US' },
          { text: 'Appendix', type: 'chapter', role: 'appendix', locale: 'en-US' },
          { text: 'Supplementary Material', type: 'chapter', role: 'supplementary-material', locale: 'en-US' },
          { text: 'Figures and Tables', type: 'chapter', role: 'figures-tables', locale: 'en-US' },
          { text: 'Glossary', type: 'chapter', role: 'glossary', locale: 'en-US' },
          { text: 'Index', type: 'chapter', role: 'index', locale: 'en-US' },
          { text: 'Resumen', type: 'abstract', locale: 'es-ES' },
          { text: 'Palabras clave', type: 'chapter', role: 'keywords', locale: 'es-ES' },
          { text: 'Introducción', type: 'chapter', role: 'introduction', locale: 'es-ES' },
          { text: 'Metodología', type: 'chapter', role: 'methods', locale: 'es-ES' },
          { text: 'Resultados', type: 'chapter', role: 'results', locale: 'es-ES' },
          { text: 'Discusión', type: 'chapter', role: 'discussion', locale: 'es-ES' },
          { text: 'Conclusiones', type: 'chapter', role: 'conclusion', locale: 'es-ES' },
          { text: 'Agradecimientos', type: 'chapter', role: 'acknowledgements', locale: 'es-ES' },
          { text: 'Apéndice', type: 'chapter', role: 'appendix', locale: 'es-ES' },
          { text: 'Referencias', type: 'bibliography', locale: 'es-ES' },
          { text: 'Zusammenfassung', type: 'abstract', locale: 'de-DE' },
          { text: 'Schlüsselwörter', type: 'chapter', role: 'keywords', locale: 'de-DE' },
          { text: 'Einleitung', type: 'chapter', role: 'introduction', locale: 'de-DE' },
          { text: 'Methodik', type: 'chapter', role: 'methods', locale: 'de-DE' },
          { text: 'Ergebnisse', type: 'chapter', role: 'results', locale: 'de-DE' },
          { text: 'Diskussion', type: 'chapter', role: 'discussion', locale: 'de-DE' },
          { text: 'Schlussfolgerungen', type: 'chapter', role: 'conclusion', locale: 'de-DE' },
          { text: 'Danksagungen', type: 'chapter', role: 'acknowledgements', locale: 'de-DE' },
          { text: 'Anhang', type: 'chapter', role: 'appendix', locale: 'de-DE' },
          { text: 'Literaturverzeichnis', type: 'bibliography', locale: 'de-DE' },
          { text: 'Conflicts of Interest', type: 'chapter', role: 'conflicts-of-interest', locale: 'en-US' },
          { text: 'Funding Statement', type: 'chapter', role: 'funding-statement', locale: 'en-US' },
          { text: 'Conflictos de Interés', type: 'chapter', role: 'conflicts-of-interest', locale: 'es-ES' },
          { text: 'Declaración de Financiación', type: 'chapter', role: 'funding-statement', locale: 'es-ES' },
          { text: 'Interessenkonflikte', type: 'chapter', role: 'conflicts-of-interest', locale: 'de-DE' },
          { text: 'Förderungserklärung', type: 'chapter', role: 'funding-statement', locale: 'de-DE' },
        ];
  
        headings.forEach(heading => {
          doc.match(`^${heading.text}$`).tag('Heading').data({
            headingType: heading.type,
            headingLocale: heading.locale,
            ...(heading.role && { headingRole: heading.role }),
            ...(heading.numbering && { headingNumbering: heading.numbering }),
          });
        });
      }
    },
  
    /** Run it on init */
    hooks: ['postProcessHeadings']
  };