/**
 * Metrics.ts
 * Extracts metrics from provided ProseMirror nodes using NLP and weighted functions.
 */

import nlp from 'compromise';
import { franc } from 'franc';
import { JSDOM } from 'jsdom';
import { DOMSerializer, Node } from 'prosemirror-model';

import { schemas, SFMarkType, SFNodeType } from '@sciflow/schema';

import { abstractPlugin } from './abstractPlugin.js';
import { affiliationPlugin } from './authorPlugin.js';
import { authorPlugin } from './authorPlugin.js';
import { headingPlugin } from './headingPlugin.js';
import { lexicon as names } from './helpers.js';
import { keywordPlugin } from './keywordsPlugin.js';
import { referencePlugin } from './referencePlugin.js';

interface Tag {
    [key: string]: string[];
}

export interface MetricData {
    node: Node;
    nlpDoc: any;
    text: string;
    tags: Tag[];
    /** Absolute ProseMirror position number in the document */
    documentPosition: number;
    /** Relative position in document (0-1) */
    posInDocPercent: number;
    /** The matched node from the secondary docx input */
    match?: { node: Node, pos: number; parent?: Node };
    posInDoc: number;
    frequency: { [key: string]: number };
    nthBiggestFont?: number;
    titleCaseWords?: number;
    wordCount: number;
    nodeStyle?: any;
    matches: { type: string; confidence: number; avgComp?: number; data?: any; }[];
}

interface Weighting {
    [key: string]: number;
}

const dom = new JSDOM(``, {
    url: "https://localhost" // see https://github.com/jsdom/jsdom/issues/2383
});
const document = dom.window.document;

// register plugins
nlp.plugin({ words: names });
nlp.plugin(referencePlugin);
nlp.plugin(authorPlugin);
nlp.plugin(affiliationPlugin);
nlp.plugin(headingPlugin);
nlp.plugin(abstractPlugin);
nlp.plugin(keywordPlugin);

const taggable = (node: Node) => [SFNodeType.paragraph, SFNodeType.header, SFNodeType.heading].includes(node.type.name);

export const sizeFromPtString = (s: string) => s?.match(/(\d+)pt/)?.[0];

const extractTagFrequencies = (nlpDoc: any) => {
    const terms: { [word: string]: string[] }[] = nlpDoc.terms().out('tags');
    const tagFrequencies: Record<string, number> = {};

    for (let term of terms) {
        for (let word of Object.keys(term)) {
            const tags = term[word];
            for (let tag of tags) {
                if (!tagFrequencies[tag]) {
                    tagFrequencies[tag] = 0;
                }
                tagFrequencies[tag]++;
            }
        }
    }

    return tagFrequencies;
};

const extractTextSizes = (doc: Node) => {
    let sizes: any = {};

    doc.descendants((node, pos, parent) => {
        const styleMark = node?.marks.find(m => m.type.name === 'style');
        if (styleMark) {
            const size = styleMark.attrs?.size?.match(/(\d+)pt/);
            if (size?.[1]) {
                if (!sizes[size?.[1]]) {
                    sizes[size?.[1]] = { node, pos, parent, size: size?.[1] };
                }
            }
        }
    });

    return Object.keys(sizes).map(size => ({ size, ...sizes[size] })).sort((a, b) => b?.size - a?.size);
};

const nodeToDOM = (node: Node, schema = 'manuscript') => {
    // create text from the node converting sup and sub to markdown
    const serializer2 = DOMSerializer.fromSchema(schemas[schema]);
    const content: any = serializer2.serializeNode(node, { document });
    if (content?.querySelectorAll) {
        const sup = content?.querySelectorAll('sup');
        sup.forEach(n => n.parentNode?.replaceChild(document.createTextNode(` ${n.textContent} `), n));
        const sub = content?.querySelectorAll('sub');
        sub.forEach(n => n.parentNode?.replaceChild(document.createTextNode(` ${n.textContent} `), n));
    }

    return content;
}

export const matchNodesToMetaData = (input: { stats: any; node: Node; parent: Node | null; pos: number; documentSize: number; intelligence?: any; }, prevMetrics: MetricData[]): MetricData => {

    const sizes = input?.stats ? extractTextSizes(input?.stats.doc) : [];
    const fontSizes = sizes.map(s => s.size);

    let content = nodeToDOM(input.node, 'manuscript');
    let text = content?.textContent;

    // go through the stats document (a secondary import to extract details pandoc cannot capture (like styles))
    // we do not get any IDs from pandoc right now (as we do from our own docx transformation)
    const secondaryImportMatches: any = [];
    if (text.length > 0) {
        input?.stats?.doc.descendants((statsNode: Node, pos: number, p: Node) => {

            let nContent = nodeToDOM(statsNode, 'docx');
            if (taggable(input.node) &&
                // compare the html content
                (nContent?.textContent?.length > 0 && nContent?.textContent === text) ||
                // compare the node text
                // pandoc does not preserve tabs or double spaces, so we need to replace them with strings for matching
                (statsNode.textContent?.trim()?.replaceAll('\t', ' ')?.replaceAll('  ', ' ') === text?.replaceAll('  ', ' ')?.trim())) {
                secondaryImportMatches.push({ node: statsNode, pos, parent: p });
            }
        });
    }

    let posInDocPercent = Number.parseFloat((input.pos / input.documentSize).toFixed(3));

    const match = secondaryImportMatches[0];
    let nthBiggestFont: number | undefined = undefined;
    const nodeStyle = match?.node?.marks?.find((m: any) => m.type.name === 'style')?.attrs;
    if (nodeStyle?.size) {
        const size = sizeFromPtString(nodeStyle?.size);
        if (size) {
            nthBiggestFont = fontSizes.findIndex(s => size >= s) + 1;
        }
    }

    if (match) {
        // we use the match since it's more accurate than the pandoc imported node
        content = nodeToDOM(match.node, 'docx');
        text = content?.textContent;
    }

    //const nlpContent = texts.join(' '); // make sure all words are separated from other characters like sup and *
    const nlpDoc = nlp(text);

    const tags = nlpDoc.terms().out('tags');
    const frequency = extractTagFrequencies(nlpDoc);
    const wordCount = nlpDoc.wordCount();
    const titleCaseWords = text.split(' ').filter(word => word[0] && word[0] === word[0]?.toUpperCase()).length;

    const metrics: MetricData = {
        node: input.node,
        match,
        nlpDoc,
        text,
        tags,
        documentPosition: input.pos,
        posInDocPercent,
        posInDoc: input.pos,
        frequency,
        nthBiggestFont,
        nodeStyle,
        titleCaseWords,
        wordCount,
        matches: []
    };

    const prev = prevMetrics?.length > 0 ? prevMetrics[prevMetrics.length - 1] : undefined;

    if (match) {
        metrics.matches.push({ type: 'title', ...isTitle(metrics, prev, nodeStyle, input.intelligence) });
        metrics.matches.push({ type: 'heading', ...isHeading(metrics, prev, nodeStyle, input.intelligence) });
        metrics.matches.push({ type: 'keywords', ...isKeywords(metrics, input.intelligence) });
        metrics.matches.push({ type: 'abstract', ...isAbstract(metrics, input.intelligence) });
        metrics.matches.push({ type: 'authors', ...isAuthor(metrics, input.intelligence) });
        metrics.matches.push({ type: 'affiliation', ...isAffiliation(metrics, input.intelligence) });
        metrics.matches.push({ type: 'text', ...isText(metrics) });
        metrics.matches.push({ type: 'caption', ...isCaption(metrics, input.intelligence) });
        metrics.matches.push({ type: 'reference', ...isReference(metrics, input.intelligence) });

        const avg = metrics.matches.map(m => m.confidence).reduce((val, curr) => val + curr, 0) / metrics.matches.length;
        metrics.matches = metrics.matches
            .sort((a, b) => b.confidence - a.confidence)
            .map((a) => ({ ...a, value: Number.parseFloat(a.confidence.toFixed(3)), avgComp: Number.parseFloat((a.confidence / avg).toFixed(3)) }));

        console.log(metrics.matches?.[0]?.type, '\t', text.substring(0, 50) + '...');
        console.table(metrics.matches.slice(0, 3));
    } else {
        console.log('No matches for ' + input.node?.textContent?.slice(0, 20), input.node?.type.name, input.node?.attrs?.id);
    }

    return metrics;
}

const isHeading = (data: MetricData, prevNodeMetrics?: MetricData, nodeStyle?: any, posInDocPercent?: number, intelligence?: any): { data?: any; confidence: number; } => {

    const evaluateAcademicTitle = (data: MetricData, weights: Weighting): number => {
        let score = 0;

        for (const [tag, count] of Object.entries(data.frequency)) {
            score += count ? (weights[tag] || 0) : 0;
        }

        if (data.node.type.name === 'heading') { score += weights.isPreferredNodeName; }
        if (posInDocPercent != undefined && posInDocPercent > 0.1) { score += weights.documentPosition; }
        if (data.nthBiggestFont && data.nthBiggestFont <= 3 && data.nthBiggestFont > 1) { score += weights.nthBiggestFont; }

        return score;
    };

    const weighting: Weighting = {
        Heading: 3,
        isPreferredNodeName: 2,
        Noun: 0.6,
        Verb: 0.5,
        Singular: 0.8,
        Author: -0.5,
        Conjunction: 0.2,
        PresentTense: 0.3,
        Infinitive: 0.2,
        Uncountable: 0.5,
        Pronoun: -0.5,
        Adverb: 0.1,
        PastTense: 0.1,
        Preposition: 0.3,
        Determiner: 0.2,
        Modal: 0.1,
        Auxiliary: 0.2,
        Gerund: 0.2,
        Possessive: 0.1,
        Plural: 0.5,
        wordCount: 0.1,
        documentPosition: 0.8,
        nthBiggestFont: 0.6,
        sameAlignAsPrev: 0.6,
        sameSizeAsPrev: 0.3
    };

    const confidence = evaluateAcademicTitle(data, weighting);

    return {
        data: {},
        confidence
    };
};

const isTitle = (data: MetricData, prevNodeMetrics?: MetricData, nodeStyle?: any, intelligence?: any): { data?: any; confidence: number; } => {

    // when multiple paragraphs make up a title node we use these indicators to see where the title ends
    const sameAlignAsPrev = prevNodeMetrics?.nodeStyle?.align == undefined || prevNodeMetrics?.nodeStyle?.align === nodeStyle?.align;
    const sameSizeAsPrev = prevNodeMetrics?.nthBiggestFont == undefined || prevNodeMetrics?.nthBiggestFont === data.nthBiggestFont;

    const matchId = data.match?.node?.attrs?.id;
    const titleId = intelligence?.front?.title?.titleHTMLElId;

    const evaluateAcademicTitle = (data: MetricData, weights: Weighting): number => {

        let score = 0;

        
        for (const [tag, count] of Object.entries(data.frequency)) {
            score += count ? (weights[tag] || 0) : 0;
        }
        
        const tags = data?.node?.marks?.find(m => m.type.name === SFMarkType.tags)?.attrs?.tags;
        const prevTags = prevNodeMetrics?.node?.marks?.find(m => m.type.name === SFMarkType.tags)?.attrs?.tags;

        const hasClass = tags?.some(t => t.key === 'title');
        const prevHasClassTitle = prevTags?.some(t => t.key === 'title');

        // if the previous node had a title class and this one does not, that is a strong indicator
        if (prevHasClassTitle && !hasClass) { score -= weights.className * 2; }
        if (!prevHasClassTitle && !hasClass) { score -= weights.className; }
        if (hasClass) { score += weights.className; }
        if (data.node.type.name === 'header') { score += weights.isPreferredNodeName; }
        if (sameAlignAsPrev) { score += weights.sameAlignAsPrev; }
        if (sameSizeAsPrev) { score += weights.sameSizeAsPrev; }
        if (data.posInDoc && data.posInDoc === 0) { score += weights.startOfDoc; }
        if (data.posInDocPercent != undefined && data.posInDocPercent < 0.35) { score += weights.documentPosition; }
        if (data.posInDocPercent != undefined && data.posInDocPercent > 0.35) { score -= weights.documentPosition; }
        if (data.posInDocPercent != undefined && data.posInDocPercent >= 0.50) { score -= weights.documentPosition * 2; }
        if (data.wordCount > 3 && data.wordCount < 40) { score += weights.wordCount; }
        if (data.wordCount > 80) { score -= weights.wordCount; }
        if (data.nthBiggestFont === 1) { score += weights.biggestFont; }
        if (data.nthBiggestFont === 2) { score += weights.secondBiggestFont; }
        if (data.titleCaseWords && (data.titleCaseWords / data.wordCount) > 0.5) { score += weights.titleCaseWords; }

        if (matchId?.length > 0 && matchId === titleId) {
            score += 100;
        }

        return score;
    };

    const weights: Weighting = {
        className: 3, // class name matches title
        Department: -2.5,
        University: -1.5,
        AffiliationNumber: -5, // use affiliation
        ReferenceNumber: -5, // and reference tagging as negatives
        Author: -1,
        wordCount: 0.1,
        documentPosition: 2,
        startOfDoc: 2,
        biggestFont: 1.5,
        titleCaseWords: 1,
        isPreferredNodeName: 1,
        secondBiggestFont: 0.8,
        sameAlignAsPrev: 0.6,
        sameSizeAsPrev: 0.3
    };

    if (matchId?.length > 0 && matchId === titleId) {
        return {
            data: { title: intelligence?.front.title, subtitle: intelligence?.front.subtitle },
            confidence: 100
        };
    }

    const confidence = evaluateAcademicTitle(data, weights);

    return {
        data: {},
        confidence
    };
};

const isCaption = (data: MetricData, intelligence: any): { data?: any; confidence: number; } => {

    const id = data.match?.node?.attrs?.id;
    const caption = intelligence?.elements?.find(s => id?.length > 0 && s.class === 'caption' && s.id === id);

    if (caption) {
        return {
            data: caption,
            confidence: 100
        }
    }

    return {
        data: {},
        confidence: -1
    };
};

const isText = (data: MetricData): { data?: any; confidence: number; } => {

    const evaluateText = (data: MetricData, weights: Weighting): number => {
        let score = 0;

        for (const [tag, count] of Object.entries(data.frequency)) {
            score += count ? (weights[tag] || 0) : 0;
        }

        if (data.node.type.name === 'paragraph') { score += weights.isPreferredNodeName; }
        if (data.titleCaseWords && (data.titleCaseWords / data.wordCount) > 0.5) { score += weights.titleCaseWords }
        if (data.nthBiggestFont && data.nthBiggestFont >= 2) { score += weights.nthBiggestFont; }

        return score;
    };

    const weights: Weighting = {
        isPreferredNodeName: 1,
        Noun: 0.6,
        Verb: 0.5,
        Singular: 0.8,
        Person: -0.8,
        Author: -0.8,
        Conjunction: 0.2,
        PresentTense: 0.3,
        Infinitive: 0.2,
        Uncountable: 0.5,
        Pronoun: -0.5,
        Adverb: 0.1,
        PastTense: 0.1,
        Preposition: 0.3,
        Determiner: 0.2,
        Modal: 0.1,
        Auxiliary: 0.2,
        Gerund: 0.2,
        Possessive: 0.1,
        Plural: 0.5,
        wordCount: 0.1,
        titleCaseWords: -0.5,
        documentPosition: 0.8,
        nthBiggestFont: 0.6,
        sameAlignAsPrev: 0.6,
        sameSizeAsPrev: 0.3
    };

    const confidence = evaluateText(data, weights);

    return {
        data: {},
        confidence
    };
};

const isReference = (data: MetricData, intelligence: any): { data?: any; confidence: number; } => {
    const evaluateReference = (data: MetricData, weights: Weighting): number => {
        let score = 0;

        const matchId = data.match?.node?.attrs?.id;
        // TODO
        const reference = intelligence?.data?.references?.find(r => r.htmlElId === matchId);
        const hasReferences = intelligence?.elements?.some(s => s.class === 'reference');
        if (reference) {
            console.log(reference);
            score += 50;
            debugger;
        } else if (hasReferences) {
            score = -10;
        }

        for (const [tag, count] of Object.entries(data.frequency)) {
            score += count ? (weights[tag] || 0) : 0;
        }

        if (data.node.type.name === 'paragraph') { score += weights.isPreferredNodeName; }
        if (data.posInDocPercent != undefined && data.posInDocPercent > 0.7) { score += weights.documentPosition; }
        if (data.nthBiggestFont && data.nthBiggestFont < 1) { score += weights.notBiggestFont; };

        return score;
    };

    const weights: Weighting = {
        isPreferredNodeName: 1,
        Person: 1.0,
        Author: 1.5,
        LastName: 2,
        ReferenceNumber: 15,
        documentPosition: 5,
        notBiggestFont: 5
    };

    const confidence = evaluateReference(data, weights);
    const references = [];

    return {
        data: references,
        confidence
    };
};

const isAbstract = (data: MetricData, intelligence?: any) => {

    const matchId = data.match?.node?.attrs?.id;
    // TODO
    const abstractPart = intelligence?.data?.parts?.find(p => p.type === 'abstract');
    const abstractEl = intelligence?.elements?.filter(p => p.sectionType === 'abstract');

    const headingInTitle = abstractPart?.htmlElId?.length > 0 && abstractPart?.htmlElId === abstractPart?.titleHTMLElId;
    if (!headingInTitle && matchId?.length > 0 && abstractPart?.titleHTMLElId === matchId) {
        return {
            confidence: 101,
            data: {
                type: headingInTitle ? 'title-and-content' : 'title',
                htmlContent: abstractPart.title,
                node: data.node,
                lang: abstractPart.lang
            }
        };
    }

    if (matchId?.length > 0 && abstractPart?.htmlElId === matchId) {
        return {
            confidence: 101,
            data: {
                type: 'content',
                htmlContent: abstractPart.htmlContent,
                node: data.node,
                lang: abstractPart.lang
            }
        };
    }

    let abstract, confidence = 0, retval;
    if (data.nlpDoc.has('#AbstractHeading')) {
        const match = data.nlpDoc.match('#AbstractHeading');
        abstract = data.node;
        retval = {
            type: data.node.textContent?.length > (match.text().length + 2) ? 'title-and-content' : 'title',
            node: data.node,
            matchedText: match.text(),
            lang: franc(data.node.textContent) // this will often have to rely on one word and not be precise -> we should use the next paragraph
        }
        confidence = 100;
    }

    return {
        confidence,
        data: retval || {}
    }
}

const isKeywords = (data: MetricData, intelligence?: any) => {

    const matchId = data.match?.node?.attrs?.id;
    const keywordsMatch = intelligence?.front?.keywords?.find(p => p.htmlElId === matchId);

    if (matchId?.length > 0 && keywordsMatch) {
        return {
            confidence: 100,
            data: keywordsMatch
        };
    }

    let keywords, confidence = 0;
    /* if (data.nlpDoc.has('#KeywordsHeading')) {
        const term = data.nlpDoc.splitAfter('#KeywordsHeading').last();
        keywords = term.text().split((/[,;]/)).map(s => s.trim());
        debugger;
        confidence = 100;
    }; */

    return {
        confidence,
        data: keywords
    }
}

const isAffiliation = (data: MetricData, intelligence?: any): { data?: any; confidence: number; } => {

    const matchId = data.match?.node?.attrs?.id;
    const matchingAffiliation = intelligence?.contrib?.affiliations?.find(a => a.htmlElId === matchId && matchId?.length > 0);
    let score = 0;
    let retval: any = {}

    if (matchingAffiliation) {
        return {
            confidence: 101,
            data: matchingAffiliation
        };
    }

    const elements = intelligence?.elements?.filter(el => el.id === matchId && (el.class === 'corresponding-author-email' || el.class === 'affiliation')) || [];
    for (let element of elements) {
        score = 100;
        retval = { correspondence: intelligence?.contrib?.correspondenceEmails };
    }

    return {
        data: retval,
        confidence: score
    };
};

const isAuthor = (data: MetricData, intelligence?: any): { data?: any; confidence: number; } => {

    const matchId = data.match?.node?.attrs?.id;
    const matchingAuthors = intelligence?.contrib?.authors?.filter(a => a.htmlElId === matchId && matchId?.length > 0);

    let score = data?.match?.node?.attrs?.tagList?.includes('authors') ? 50 : 0;
    const doc = nlp(data.text);
    let authors: any[] = [];

    if (matchingAuthors?.length > 0) {
        return {
            confidence: 100,
            data: matchingAuthors
        }
    }

    const elements = intelligence?.elements?.filter(el => el.id === matchId && (el.class === 'author-name' || el.class === 'author-email')) || [];
    for (let element of elements) {
        return {
            confidence: 100,
            data: {
                element
            }
        }
    }

    const evaluateAuthor = (data: MetricData, weights: Weighting): number => {

        for (const [tag, count] of Object.entries(data.frequency)) {
            score += count ? (weights[tag] || 0) : 0;
        }

        if (data.node.type.name === 'paragraph') { score += weights.isPreferredNodeName; }
        if (data.posInDocPercent != undefined && data.posInDocPercent < 0.2) { score += weights.documentPosition; }
        if (data.nthBiggestFont && data.nthBiggestFont >= 2) { score += weights.nthBiggestFont; }

        //score += weights.wordCount * data.wordCount;
        //score += (data.documentPosition ? weights.documentPosition * data.documentPosition : 0);
        //score += (data.nthBiggestFont ? weights.nthBiggestFont * data.nthBiggestFont : 0);

        return score;
    };

    const authorWeighting: Weighting = {
        Person: 2.0,
        Department: -2.5,
        University: -1.5,
        Adjective: -4,
        Verb: -4,
        PastTense: -2,
        LastName: 1.8,
        ReferenceNumber: -10,
        FirstName: 1.5,
        isPreferredNodeName: 0.5,
        Author: 2,
        documentPosition: 1,
        nthBiggestFont: 1
    };

    const confidence = evaluateAuthor(data, authorWeighting);
    const people = doc.people();

    people.forEach((person) => {
        const firstNameDoc = person.splitBefore('#LastName').first();
        const affiliationNumbers = person?.after('').match('#AffiliationNumber').out('array');
        const correspondingSymbols = person?.after('').match('#SpecialCharacter').out('array');

        const author = {
            name: person.ifNo('#LastName').ifNo('#FirstName')?.text(),
            firstName: firstNameDoc.match('#Person*').out('array').join(' '),
            lastName: person.match('#LastName').text(),
            prefix: person.match('#Prefix').text(),
            suffix: person.match('#Suffix').text(),
            affiliationNumbers,
            correspondingSymbols
        };

        Object.keys(author).forEach(key => {
            if (author[key] == undefined || author[key] === '') {
                delete author[key];
            }
        });

        authors.push(author);
    });

    return {
        data: authors,
        confidence
    };
};