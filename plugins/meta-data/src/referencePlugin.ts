export const referencePlugin = {
    /** Add custom tags */
    tags: {
        ReferenceNumber: {
            isA: 'Indicator',
        },
        Authors: {
            isA: 'Person',
        },
        Title: {
            isA: 'Work',
        },
        Journal: {
            isA: 'Publication',
        },
        Year: {
            isA: 'Date',
        },
        Volume: {
            isA: 'Publication',
        },
        Issue: {
            isA: 'Publication',
        },
        Pages: {
            isA: 'Publication',
        }
    },

    /** Post-process tagger */
    compute: {
        postProcessReferences: (doc: any) => {
            // Tagging reference numbers

            const referenceCounter = doc.text().trim().match(/^\[\d+\]|\d+\./);
            if (referenceCounter?.[0]) {
                doc.match(referenceCounter[0]).tag('ReferenceNumber', 'reference-number-pattern');
            }
        }
    },

    /** Run it on init */
    hooks: ['postProcessReferences']
};
