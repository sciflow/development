export const lexicon = {
  // Chinese Names
  'Wei': 'FirstName',
  'Xiao': 'FirstName',
  'Li': 'FirstName LastName',
  'Jian': 'FirstName',
  'Hui': 'FirstName',
  'Ming': 'FirstName',
  'Fang': 'FirstName',
  'Chen': 'FirstName LastName',
  'Jing': 'FirstName',
  'Ping': 'FirstName',
  'Zhang': 'LastName',
  'Wang': 'LastName',
  'Zhao': 'LastName',
  'Liu': 'LastName',
  'Yang': 'LastName',
  'Huang': 'LastName',
  'Wu': 'LastName',
  'Zhou': 'LastName',

  // Indian Names
  'Amit': 'FirstName',
  'Priya': 'FirstName',
  'Raj': 'FirstName',
  'Asha': 'FirstName',
  'Ravi': 'FirstName',
  'Neha': 'FirstName',
  'Vijay': 'FirstName',
  'Sita': 'FirstName',
  'Anil': 'FirstName',
  'Kiran': 'FirstName',
  'Patel': 'LastName',
  'Singh': 'LastName',
  'Sharma': 'LastName',
  'Gupta': 'LastName',
  'Kumar': 'LastName',
  'Reddy': 'LastName',
  'Menon': 'LastName',
  'Iyer': 'LastName',
  'Desai': 'LastName',
  'Nair': 'LastName',

  // Vietnamese Names
  'Anh': 'FirstName',
  'Binh': 'FirstName',
  'Chau': 'FirstName',
  'Dung': 'FirstName',
  'Giang': 'FirstName',
  'Hai': 'FirstName',
  'Hoa': 'FirstName',
  'Khanh': 'FirstName',
  'Lan': 'FirstName',
  'Minh': 'FirstName',
  'Nguyen': 'LastName',
  'Tran': 'LastName',
  'Le': 'LastName',
  'Pham': 'LastName',
  'Hoang': 'LastName',
  'Phan': 'LastName',
  'Vu': 'LastName',
  'Vo': 'LastName',
  'Dang': 'LastName',
  'Bui': 'LastName',

  // Other Common Asian Names
  'Satoshi': 'FirstName', // Japanese
  'Haruto': 'FirstName',  // Japanese
  'Yuna': 'FirstName',    // Japanese
  'Jisoo': 'FirstName',   // Korean
  'Jiho': 'FirstName',    // Korean
  'Minho': 'FirstName',   // Korean
  'Arisa': 'FirstName',   // Thai
  'Kanya': 'FirstName',   // Thai
  'Layla': 'FirstName',   // Filipino
  'Jose': 'FirstName',    // Filipino
  'Suzuki': 'LastName',   // Japanese
  'Tanaka': 'LastName',   // Japanese
  'Kim': 'LastName',      // Korean
  'Park': 'LastName',     // Korean
  'Lee': 'LastName',      // Korean
  'Wong': 'LastName',     // Hong Kong
  'Lim': 'LastName',      // Singapore
  'Tan': 'LastName',      // Singapore
  'Reyes': 'LastName',    // Filipino
  'Santos': 'LastName',   // Filipino
  'Sinta': 'FirstName', // Latvian

  // Arabic Names
  'Ahmed': 'FirstName',
  'Ali': 'FirstName',
  'Omar': 'FirstName',
  'Hassan': 'FirstName LastName',
  'Fatima': 'FirstName',
  'Aisha': 'FirstName',
  'Mohammed': 'FirstName LastName',
  'Hussein': 'FirstName LastName',
  'Khalid': 'FirstName',
  'Yusuf': 'FirstName',
  'Abdullah': 'FirstName',
  'Rahman': 'LastName',
  'Saud': 'LastName',
  'Aziz': 'LastName',
  'Salman': 'LastName',
  'Farouk': 'LastName',

  // Persian Names
  'Reza': 'FirstName',
  'Hossein': 'FirstName',
  'Mehdi': 'FirstName',
  'Leila': 'FirstName',
  'Fatemeh': 'FirstName',
  'Sara': 'FirstName',
  'Neda': 'FirstName',
  'Parvin': 'FirstName',
  'Shirin': 'FirstName',
  'Ebrahim': 'FirstName',
  'Farhad': 'FirstName',
  'Golzar': 'LastName',
  'Rostami': 'LastName',
  'Mahdavi': 'LastName',
  'Karimi': 'LastName',
  'Mousavi': 'LastName',

  // Other Common Names
  'Arjun': 'FirstName', // Indian
  'Sanjay': 'FirstName', // Indian
  'Yuki': 'FirstName', // Japanese
  'Naoko': 'FirstName', // Japanese
  'Hiroshi': 'FirstName', // Japanese
  'Mina': 'FirstName', // Korean
  'Seo-jun': 'FirstName', // Korean
  'Tariq': 'FirstName', // Arabic
  'Bilal': 'FirstName', // Arabic
  'Nadia': 'FirstName', // Arabic
  'Zara': 'FirstName', // Arabic
  'Imran': 'FirstName', // Arabic
  'Shiva': 'FirstName', // Persian
  'Roxana': 'FirstName', // Persian
  'Omid': 'FirstName', // Persian
  'Fariba': 'FirstName', // Persian
  'Mehran': 'FirstName', // Persian

  // Polish Names
  'Jan': 'FirstName',
  'Anna': 'FirstName',
  'Krzysztof': 'FirstName',
  'Maria': 'FirstName',
  'Piotr': 'FirstName',
  'Agnieszka': 'FirstName',
  'Tomasz': 'FirstName',
  'Katarzyna': 'FirstName',
  'Pawel': 'FirstName',
  'Barbara': 'FirstName',
  'Nowak': 'LastName',
  'Nowicki': 'LastName',
  'Kowalski': 'LastName',
  'Wojcik': 'LastName',
  'Kaminski': 'LastName',
  'Lewandowski': 'LastName',
  'Zielinski': 'LastName',
  'Szymanski': 'LastName',
  'Dabrowski': 'LastName',
  'Kozlowski': 'LastName',
  'Mazur': 'LastName',

  // Russian Names
  'Ivan': 'FirstName',
  'Olga': 'FirstName',
  'Sergey': 'FirstName',
  'Elena': 'FirstName',
  'Dmitry': 'FirstName',
  'Tatiana': 'FirstName',
  'Alexey': 'FirstName',
  'Natalia': 'FirstName',
  'Mikhail': 'FirstName',
  'Anastasia': 'FirstName',
  'Ivanov': 'LastName',
  'Petrov': 'LastName',
  'Sidorov': 'LastName',
  'Smirnov': 'LastName',
  'Kuznetsov': 'LastName',
  'Popov': 'LastName',
  'Vasiliev': 'LastName',
  'Novikov': 'LastName',
  'Fedorov': 'LastName',
  'Morozov': 'LastName',

   // Indonesian Names
   'Budi': 'FirstName',
   'Siti': 'FirstName',
   'Ahmad': 'FirstName',
   'Sri': 'FirstName',
   'Agus': 'FirstName',
   'Hendra': 'FirstName',
   'Dewi': 'FirstName',
   'Rina': 'FirstName',
   'Sukma': 'FirstName',
   'Wayan': 'FirstName',
   'Putra': 'LastName',
   'Yudha': 'LastName',
   'Hadi': 'LastName',
   'Prasetyo': 'LastName',
   'Suryadi': 'LastName',
   'Santoso': 'LastName',
   'Kusuma': 'LastName',
   'Lestari': 'LastName',
   'Wijaya': 'LastName',
   'Sutanto': 'LastName',

   // North African Names
   'Abdel': 'FirstName',
   'Noura': 'FirstName',
   'Yassine': 'FirstName',
   'Zineb': 'FirstName',
   'Amine': 'FirstName',
   'Khadija': 'FirstName',
   'Rachid': 'FirstName',
   'Souad': 'FirstName',
   'Karim': 'FirstName',
   'Samira': 'FirstName',
   'Bennani': 'LastName'
};
