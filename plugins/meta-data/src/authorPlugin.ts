/***
 * authorPlugin.ts
 * Author Plugin for compromise (NPM)
 * example input: "Xiao Mei Zhang 1*, Robert J. Patterson 2, Emily N. Miller 3†"
 */

export const authorPlugin = {
  /** Add custom tags */
  tags: {
    Author: { isA: 'Person' },
    FirstName: { isA: 'Author' },
    LastName: { isA: 'Author' },
    MiddleName: { isA: 'Author' },
    Name: { isA: 'Author' },
    Suffix: { isA: 'Author' },
    Prefix: { isA: 'Author' },
    CorrespondingAuthor: { isA: 'Author' },
    AffiliationNumber: { isA: 'Identifier' },
    Delimiter: { isA: 'Punctuation' },
    TitleWord: { isA: 'Term' },
    Abbreviation: { isA: 'Term' },
    SpecialCharacter: { isA: 'Punctuation' },
    ContextualWord: { isA: 'Term' },
    ORCID: { isA: 'Identifier' },
    Correspondence: { isA: 'Term' }
  },
  compute: {
    postProcessAuthors: (doc) => {
      // Match full names with possible middle names/initials, suffixes, and hyphenated names
      doc.match('#ProperNoun+ (Jr|Sr|Dr|PhD)(\\.|\\s)?').tag('Author', 'author-pattern');
      doc.match('#ProperNoun+-#ProperNoun+ (Jr|Sr|Dr|PhD)(\\.|\\s)?').tag('Author', 'hyphenated-author');
      doc.match("#ProperNoun+ O'#ProperNoun+").tag('Author', 'apostrophe-name');
      doc.match("#ProperNoun+ de la #ProperNoun+").splitBefore('de la').last().tag('LastName').tag('Author', 'de-la-name');

      // Define common organizational keywords
      const orgKeywords = [
        'Universität', 'Bibliothek', 'Institut', 'Fakultät', 'Research',
        'Library', 'Department', 'Academy', 'University', 'School', 'College',
        'Center', 'Programme', 'Program'
      ];

      // Match organization patterns dynamically
      doc
        .match(`#ProperNoun+ (${orgKeywords.join('|')}) #ProperNoun*`)
        .tag('Organization')
        .unTag('Person')
        .unTag('Author');

      // Handle single-token organizational keywords
      doc.match(orgKeywords.join('|')).tag('Organization').unTag('Author').unTag('Person');

      // Untag adjacent proper nouns in organizations
      doc
        .match('#ProperNoun+ der? #Organization')
        .tag('Organization')
        .unTag('Author')
        .unTag('Person');

      // Handle hyphenated names as part of organizations
      doc
        .match('#Organization #Hyphenated')
        .unTag('Person')
        .unTag('Author')
        .tag('Organization');

      // Extract suffixes
      doc
        .match('#ProperNoun (Jr|Sr|Dr|PhD)$').splitBefore('#ProperNoun ').last()
        .tag('Suffix')
        .unTag('#MiddleName')
        .unTag('#FirstName')
        .unTag('#LastName');

      const authorList = doc.match('#Author').match('!#Suffix');
      authorList.forEach((term, index) => {
        const length = authorList.length;
        if (index === 0) {
          // first should be the first name unless known as a last name already
          term.ifNo('#LastName').tag('FirstName');
        } else if (index === length - 1) {
          term.ifNo('#FirstName').tag('LastName');
        } else {
          // tag everything in the middle, in case of ambiguity, use the first name
          if (term?.match('#FirstName').length !== 0 || term?.match('#LastName').length === 0) {
            term?.tag('MiddleName').unTag('#LastName');
          }
        }
      });

      doc.match('correspondence * /@/').tag('Correspondence');

      // Handle single-part names as Name
      doc.match('#Author').ifNo('#FirstName').ifNo('#LastName').ifNo('#Suffix').tag('Name');

      // Match delimiters
      doc.match(',|;|and').tag('Delimiter', 'delimiter-pattern');

      // Match common academic or professional titles
      doc.match('(Dr|Professor|PhD|MD|DDS|JD|Esq|MSc|BSc|BA|MA)\\.?').tag('TitleWord', 'title-word-pattern');

      // Match abbreviations and initials
      doc.match('#Initial').tag('Abbreviation', 'abbreviation-pattern');
      doc.match('#Acronym').tag('Abbreviation', 'abbreviation-pattern');

      // Match special characters
      doc.match('[\\*†‡§¶\\|]').tag('SpecialCharacter', 'special-character-pattern');

      // Match contextual words
      doc.match('(et al|and colleagues|co-authors|collaborators)').tag('ContextualWord', 'contextual-word-pattern');

      // Match ORCID identifiers
      doc.match('\\d{4}-\\d{4}-\\d{4}-\\d{4}').tag('ORCID', 'orcid-pattern');
    }
  },

  /** Run it on init */
  hooks: ['postProcessAuthors']
};

/**
 * authorPlugin.ts
 * Extracts structured data from affiliation strings like:
 *  - 1 Department of Literature, Fiction University, Imaginary Town, IT 67890
 *  - 2 Geography Education Faculty of Teacher Training and Education
 *  - 3 Master Program on Environmental Science, SPs, Universitas Padjadjaran
 *  - 4 Department of Geography Education, FPIPS, Universitas Pendidikan Indonesia
 * 
 * Example structure in XML.
 * <aff>
    <label>1</label>
    <institution-wrap>
        <institution>Example University</institution>
        <institution content-type="department">Department of Computer Science</institution>
    </institution-wrap>
    <addr-line>123 Example Street</addr-line>
    <city>Berlin</city>
    <country>Germany</country>
</aff>
 */

export const affiliationPlugin = {
  /** Add custom tags */
  tags: {
    AffiliationNumber: {
      isA: 'Indicator',
    },
    University: {
      isA: 'Organization',
    },
    Department: {
      isA: 'Organization',
    },
    Faculty: {
      isA: 'Organization',
    },
    Program: {
      isA: 'Organization',
    },
    City: {
      isA: 'Place',
    },
    Country: {
      isA: 'Place',
    },
    AddrLine: {
      isA: 'Address',
    }
  },

  /** Post-process tagger */
  compute: {
    postProcessAffiliations: (doc: any) => {
      const affiliationNumber = doc.text().trim().match(/^(\d+)\s.+$/);
      if (affiliationNumber?.[1]) {
        doc.match(affiliationNumber[1]).tag('AffiliationNumber', 'reference-number-pattern');
      }

      // Tagging universities
      doc.match('University of #ProperNoun+').tag('University', 'university-pattern');
      doc.match('#ProperNoun University').tag('University', 'university-pattern');
      doc.match('Universitas #ProperNoun+').tag('University', 'universitas-pattern');

      // Tagging departments
      doc.match('Department of #ProperNoun').tag('Department', 'department-pattern');
      doc.match('#ProperNoun Department').tag('Department', 'department-pattern');

      // Tagging faculties
      doc.match('#ProperNoun Faculty').tag('Faculty', 'faculty-pattern');
      doc.match('Faculty of #ProperNoun').tag('Faculty', 'faculty-pattern');

      // Tagging programs
      doc.match('Program on #ProperNoun').tag('Program', 'program-pattern');
      doc.match('#ProperNoun Program').tag('Program', 'program-pattern');

      // Tagging cities and countries
      doc.match('#ProperNoun (City|Town|Village)').tag('City', 'city-pattern');

      // Tagging address lines
      doc.match('#Number #ProperNoun+ Street').tag('AddrLine', 'addrline-pattern');
    }
  },

  /** Run it on init */
  hooks: ['postProcessAffiliations']
};
