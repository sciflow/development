import { expect } from 'chai';
import compromise from 'compromise';
import { authorPlugin } from './authorPlugin.js';
import { lexicon as words } from './helpers.js';
import { keywordPlugin } from './keywordsPlugin.js';

// Load the plugin at the start
// compromise.verbose(true);
compromise.plugin({ words });
compromise.plugin(keywordPlugin);

describe('Keywords plugin', () => {
    it('should correctly tag keywords', () => {
        const testCases = [
            { text: 'Keywords: One, two, three', keywords: ['One', 'two', 'three'] },
            { text: 'Keywords: One; two; three', keywords: ['One', 'two', 'three'] },
            { text: 'Key words: One; two; three', keywords: ['One', 'two', 'three'] },
            { text: 'Abstract: keywords, test', keywords: [] },
        ];

        testCases.forEach(({ text, keywords }) => {
            const doc = compromise(text);
            const keywordMatches = doc.match('#Keyword').out('array').map(s => s.replace(/[,;]/, ''));
            expect(keywordMatches.length).to.equal(keywords.length);
            for (let i = 0;i<keywordMatches.length - 1;i++) {
                expect(keywords[i]).to.equal(keywordMatches[i]);
            }
        });
    });
});