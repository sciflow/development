export const abstractPlugin = {
    /** Add custom tags */
    tags: {
        Abstract: {
            isA: 'Concept',
        },
        AbstractHeading: {
            isA: 'Heading',
        }
    },

    /** Post-process tagger */
    compute: {
        postProcessAbstracts: (doc: any) => {
            doc.match('^(abstract|summary)').tag('AbstractHeading', 'abstract-heading-pattern');
            if (doc.has('#AbstractHeading')) {
                const term = doc.splitAfter('#AbstractHeading').last();
                const abstractText = term.text().split((/(\n|$)/)).map(s => s.trim())[0];
                doc.match(abstractText).tag('Abstract');
            }
        }
    },
    /** Run it on init */
    hooks: ['postProcessAbstracts']
};
