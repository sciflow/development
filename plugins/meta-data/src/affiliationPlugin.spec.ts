import { expect } from 'chai';
import compromise from 'compromise';
import { affiliationPlugin } from './authorPlugin.js';
import { authorPlugin } from './authorPlugin.js';

compromise.verbose();
compromise.plugin(authorPlugin);
compromise.plugin(affiliationPlugin);

describe('Affiliation plugin', () => 
    
    it('detects affiliations', async () => {
        const testCases = [
            {
                text: '3   Institute of Creative Writing, Dreamland University, Fantasyville, FV 54321',
                expectedTags: {
                    AffiliationNumber: '3'
                }
            },
            {
                text: '  1 Department of Literature, Fiction University, Imaginary Town, Italy  ',
                expectedTags: {
                    AffiliationNumber: '1',
                    Department: 'Department of Literature',
                    University: 'Fiction University',
                    City: 'Imaginary Town',
                    Country: 'Italy',
                }
            },
            {
                text: '2 Geography Education Faculty of Teacher Training and Education',
                expectedTags: {
                    AffiliationNumber: '2',
                    Faculty: 'Geography Education Faculty of Teacher Training and Education',
                }
            },
            {
                text: '3 Master Program on Environmental Science, SPs, Universitas Padjadjaran',
                expectedTags: {
                    AffiliationNumber: '3',
                    Program: 'Master Program on Environmental Science',
                    University: 'Universitas Padjadjaran',
                }
            },
            {
                text: '4 Department of Geography Education, FPIPS, Universitas Pendidikan Indonesia',
                expectedTags: {
                    AffiliationNumber: '4',
                    Department: 'Department of Geography Education',
                    University: 'Universitas Pendidikan Indonesia',
                }
            }
        ];

        for (let { text, expectedTags } of testCases) {
            const doc = compromise(text);
            Object.entries(expectedTags).forEach(([tag, expectedValue]) => {
                const extractedValue = doc.match(`#${tag}`).out('text');
                expect(extractedValue).to.equal(expectedValue, `Failed checking ${tag}`);
            });
        }

    }));
