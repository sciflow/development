import rawData from './fixtures/monster.sfo-raw.json';
import { tag } from './tag.js';
import { expect } from 'chai';

import { fromJSON, schemas } from '@sciflow/schema';
import { EditorState } from 'prosemirror-state';

describe('Tagging of meta data', async () => {
    it('Identifies authors', async () => {
        const { manuscript, docxStats } = rawData;
        const fromJSONManuscript = fromJSON('manuscript');
        docxStats.doc = fromJSON('docx')(JSON.parse(JSON.stringify(docxStats.doc)));


        const schema = schemas.manuscript;
        


        const doc = fromJSONManuscript(manuscript.document);

        let state = EditorState.create({
            doc,
            schema
        });

        const result = tag({ state, manuscript, stats: docxStats });
        const taggedAuthorsMessage = result.logs.filter(l => l.message === 'Tagged authors');
        expect(taggedAuthorsMessage.length).to.equal(1, 'Expecting one author');
        const authors = taggedAuthorsMessage[0].payload;

        expect(authors.length).to.equal(3);
        expect(authors[0].lastName).to.equal('Thomson');
        expect(authors[0].firstName).to.equal('Liu Mei');
        expect(authors[1].lastName).to.equal('Patterson');
        expect(authors[1].firstName).to.equal('Robert J.');
        expect(authors[2].lastName).to.equal('Miller');
        expect(authors[2].firstName).to.equal('Emily N.');

        debugger;
    });
});
