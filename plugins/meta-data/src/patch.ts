import { SFNodeType } from '@sciflow/schema';
import { Fragment, Node, Slice } from 'prosemirror-model';
import { EditorState, Transaction } from 'prosemirror-state';
import { Action, TransportManuscriptFile } from './types.js';
import { insertPoint, liftTarget } from 'prosemirror-transform';

const slugify = (s: string) => s?.toString().toLowerCase().replace(/\s+/g, '-').replace(/[^\w\-]+/g, '').replace(/\-\-+/g, '-').replace(/^-+/, '').replace(/-+$/, '').replace(/^[0-9]+/, '_');

/** Lists the actions available given a state
 * These may be based on tags or nodeids.
 */
export const actions = (input: { state: EditorState; manuscript: TransportManuscriptFile; intelligence?: any }): Action[] => {
  let actions: any[] = [];

  let prev: { node: Node; parent: Node | null } | undefined;
  input.state.doc.descendants((node: Node, pos: number, parent: Node | null) => {
    let attrs: { tags?: { key: string; value?: any; }[] } = node.marks?.find(m => m.type.name === 'tags')?.attrs || {};

    const nodeId = node.attrs?.id;
    if (attrs.tags && ['authors', 'keywords', 'abstract', 'references', 'reference', 'affiliation', 'title'].some(keyword => attrs.tags?.some(tag => tag.key))) {
      for (let tag of attrs.tags) {
        let value;
        if (tag?.value && typeof tag?.value === 'string' && tag.value.startsWith('{')) {
          value = JSON.parse(tag.value);
        }
        switch (tag?.key) {
          case 'blockquote-wrapping-front':
            actions.push({
              id: nodeId + '-' + 'blockquote-wrapping-front',
              name: 'Removing unnecessary indents around text: ' + node.textContent,
              nodeId,
              tag,
              node,
              from: pos,
              to: pos + node.nodeSize
            });
            break;
          case 'title':
            actions.push({
              id: nodeId + '-' + 'meta-data-title',
              name: 'Change title to: ' + node.textContent,
              nodeId,
              tag,
              node,
              from: pos,
              to: pos + node.nodeSize
            });
            break;
          case 'subtitle':
            actions.push({
              id: nodeId + '-' + 'meta-data-sub-title',
              name: 'Change subtitle to: ' + node.textContent,
              nodeId,
              tag,
              node,
              from: pos,
              to: pos + node.nodeSize
            });
            break;
          case 'authors':
            {
              let actionName = 'Adding authors';
              if (value?.data && Array.isArray(value?.data)) {
                actionName += ': ' + value?.data?.map((v) => v.name || v.lastName).join(', ');
              }
              actions.push({
                id: nodeId + '-' + 'meta-data',
                name: actionName,
                nodeId,
                tag,
                node
              });
            }
            break;
          case 'affiliation':
            actions.push({
              id: nodeId + '-' + 'meta-data',
              name: 'Adding affiliations: ' + value?.data?.affiliation?.slice(0, 15),
              nodeId,
              tag,
              node
            });
            break;
          case 'correspondence':
            debugger;
            actions.push({
              id: nodeId + '-' + 'meta-data',
              name: 'Adding correspondence: ' + value?.data?.text?.slice(0, 15),
              nodeId,
              tag,
              node
            });
            break;
          case 'abstract':
            actions.push({
              id: nodeId + '-' + 'meta-data',
              name: 'Marking section as abstract',
              nodeId,
              tag,
              node
            });
            break;
          case 'keywords':
            actions.push({
              id: nodeId + '-' + 'meta-data',
              name: 'Marking section as keywords',
              nodeId,
              tag,
              node
            });
            break;
          case 'references':
            actions.push({
              id: nodeId + '-' + 'meta-data',
              name: 'Marking section as references',
              nodeId,
              tag,
              node
            });
            break;
        }
      }
    }

    prev = { node, parent };
  });

  return actions;
}

/**
* Takes a list of actions (which may include user decision) and applies them to the document.
* @param state 
* @param actions 
* @returns a transaction and any applied actions in the logs.
*/
export const patch = async (input: { state: EditorState, manuscript: TransportManuscriptFile; intelligence?: any; }, actions: Action[]): Promise<{ tr: Transaction | undefined; logs: any[]; state?: EditorState, manuscript?: any; } | undefined> => {
  const tr = input.state.tr;
  const schema = input.state.schema;
  const state = input.state;
  const manuscript = input.manuscript;

  const logs: any[] = [];

  manuscript.authors = [...(manuscript.authors || [])];

  function getConsecutiveElements(array: any[]) {
    return array.reduce((acc, current, index, src) => {
      const compareTo = acc[acc.length - 1]?.to;
      if (!compareTo || current.from === compareTo) {
        acc.push(current);
      }
      return acc;
    }, []);
  }


  const titleActions = actions.filter(action => action.tag?.key === 'title');
  const consecutiveTitleNodes = getConsecutiveElements(titleActions);
  // combine all directly following title tags

  if (consecutiveTitleNodes.length > 0) {
    input.manuscript.title = consecutiveTitleNodes.map((a: Action) => a.node?.textContent || '').join('')?.replaceAll('\n').trim();
    const allParagraphs = consecutiveTitleNodes.every(n => n.node.type.name === SFNodeType.paragraph);

    if (allParagraphs) {
      let from, to;
      const content: Node[] = [];
      tr.doc.descendants((node, pos, parent) => {
        if (node?.attrs?.id && consecutiveTitleNodes.some(n => n.nodeId === node?.attrs?.id)) {
          if (from === undefined) {
            from = pos;
          }

          if (node.content) {
            const c: Slice = node.slice(0);
            c && content.push(...c.content.content);
          }
          to = pos + node.nodeSize;
        }
      });

      const headerType = input.state.schema.nodes[SFNodeType.header];
      const headingType = input.state.schema.nodes[SFNodeType.heading];
      if (headerType && from !== undefined) {
        let title;

        try {
          // TODO subtitle
          const heading = headingType.create({ id: 'title', type: 'title' }, content);
          heading.check();
          title = headerType.createAndFill({ id: 'title-header' }, [heading]);
          title.check();
        } catch (e: any) {
          console.error('Could not created header', { message: e.message, title });
        }

        const insertAt = insertPoint(tr.doc, 1, headerType);
        if (insertAt != undefined && title) {
          // remove the old content
          console.log(tr.doc.slice(from, to));
          tr.deleteRange(from, to);
          // insert the header
          tr.insert(insertAt, title);
        }
      }

    }

  }

  for (let action of actions) {
    const tag = action.tag;
    let value;
    if (tag?.value && typeof tag?.value === 'string' && tag.value.startsWith('{')) {
      value = JSON.parse(tag.value);
    }
    switch (tag?.key) {
      case 'blockquote-wrapping-front':
        tr.doc.descendants((node, pos) => {
          if (action.nodeId && node?.attrs?.id === action.nodeId) {
            tr.replaceWith(pos, pos + node.nodeSize, node.content);
            logs.push(action);
          }
        });
        break;
      case 'authors':
        if (value?.data && Array.isArray(value?.data) && value.data.length > 0) {
          logs.push(action);
          for (let author of [...value?.data].sort((a: any, b: any) => a.i - b.i)) {
            const { i, name, symbols, suffix, prefix, firstName, lastName, email, correspondingAuthor } = author;
            const authorObj = {
              type: 'author' as ('author' | 'group'),
              authorId: slugify(`${name || lastName || firstName || email}-${i ?? value.data.indexOf(author)}`), roles: ['author'],
              name: !lastName ? name : undefined,
              firstName,
              lastName,
              email,
              prefix,
              suffix,
              rank: i,
              allowedRoles: ['Author'],
              correspondingAuthor,
              deceased: false,
              hideInPublication: false,
              symbols,
              affiliationNumbers: author.affiliationNumbers || []
            };

            for (let key of Object.keys(authorObj)) {
              if (authorObj[key] === undefined) { delete authorObj[key]; }
            }

            manuscript.authors.push(authorObj);
          }

          // delete the author node
          tr.doc.descendants((node, pos, parent) => {
            if (action.nodeId && node?.attrs?.id === action.nodeId) {
              tr.deleteRange(pos, pos + node.nodeSize);
            }
          });
        } else {
          // delete the unused author node
          tr.doc.descendants((node, pos, parent) => {
            if (action.nodeId && node?.attrs?.id === action.nodeId) {
              tr.deleteRange(pos, pos + node.nodeSize);
            }
          });
        }
        break;
      case 'affiliation':
        const affiliation = value?.data;
        tr.doc.descendants((node, pos, parent) => {
          if (action.nodeId && node?.attrs?.id === action.nodeId) {
            tr.deleteRange(pos, pos + node.nodeSize);
          }
        });

        for (let author of manuscript.authors) {
          if (!author.positions) { author.positions = []; }
          if (author.affiliationNumbers?.some(id => id === affiliation.affiliationNumber)) {
            delete affiliation.htmlElId;
            author.positions.push(affiliation);
          }
        }

        break;
      case 'correspondence':
        debugger;
        const correspondence = value?.data;
        // TODO
        tr.doc.descendants((node, pos, parent) => {
          if (action.nodeId && node?.attrs?.id === action.nodeId) {
            tr.deleteRange(pos, pos + node.nodeSize);
          }
        });
        break;
      case 'abstract':
        if (value?.data?.type) {
          if (!manuscript.parts) { manuscript.parts = []; }
          if (!value?.data.node) { continue; }

          tr.doc.descendants((node, pos, parent) => {
            if (action.nodeId && node?.attrs?.id === action.nodeId) {
              // type: title-and-content -> the heading title is inside the paragraph of the abstract
              switch (value?.data?.type) {
                case 'title':
                  break;
                case 'title-and-content':
                  // we should remove the title and move it into its own heading.
                  if (node.child(0)?.textContent?.trim() === value?.data?.matchedText) {
                    let add = 1;
                    if (node.child(1)?.textContent?.startsWith(' ')) { add++; } // also deleting any space after
                    tr.deleteRange(pos + 1, pos + node.child(0)?.nodeSize + add);
                    const heading = schema.nodes[SFNodeType.heading].create({ id: slugify('heading-' + value?.data?.matchedText), role: 'abstract', numbering: 'none' }, [schema.text(value?.data?.matchedText?.replaceAll(/[\.:]/g, ''))]);
                    value?.data?.matchedText?.length > 0 && tr.insert(pos + 1, [heading]);
                  }
                  break;
                case 'content':
                  // we just continue for now
                  break;
              }
            }
          });

          let node;
          tr.doc.descendants((n) => {
            if (n?.attrs?.id === action.nodeId) { node = n; }
          });

          let { lang } = value?.data;
          node = node?.toJSON ? node?.toJSON() : node;

          manuscript.parts.push({
            type: 'abstract',
            lang,
            document: node
          });
        }
        break;
      case 'keywords':
        if (!manuscript.keywords) { manuscript.keywords = []; }
        if (!value?.data) { continue; }
        manuscript.keywords.push(value?.data);
        tr.doc.descendants((node, pos, parent) => {
          if (action.nodeId && node?.attrs?.id === action.nodeId) {
            tr.replaceWith(pos, pos + node.nodeSize, [
              schema.nodes[SFNodeType.heading].create({ role: 'keywords', locale: value?.data?.lang }, [schema.text(value?.data?.title || 'Keywords')]),
              schema.nodes[SFNodeType.paragraph].create({ role: 'keywords-paragraph' }, [schema.text(manuscript.keywords!.map(k => k.values).join(', '))])
            ]);
          }
        });
        break;
      case 'references':
        // TODO
        break;
    }
  }

  tr.doc.check();

  return { tr, logs, state, manuscript };
}