export interface Part {
    id?: string;
    type?: 'abstract' | string;
    lang?: string;
    document?: any;
}

export interface TransportManuscriptFile {
    title?: string;
    authors: any[];
    affiliations?: any[];
    keywords?: {
        title?: string;
        values: string[];
        lang?: string;
    }[];
    parts?: Part[];
    /** references as CSL */
    references?: any[];
    /** A list of all files used in the document (e.g. to replace thumbnails with large resolution images in export) */
    files?: any[];
    metaData?: object;
}

export interface Action {
    /** A deterministic unique (functional) id for the action */
    id: string;
    name: string;
    nodeId?: string;
    node?: any;
    figureId?: string;
    tag?: any;
    /** A JSON schema for any needed input data */
    schema: any;
}
