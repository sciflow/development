import { expect } from 'chai';
import compromise from 'compromise';
import { authorPlugin } from './authorPlugin.js';
import { lexicon as words } from './helpers.js';

// Load the plugin at the start
// compromise.verbose(true);
compromise.plugin({ words });
compromise.plugin(authorPlugin);

describe('Author plugin', () => {
    it('should correctly tag and extract first and last names from various name combinations', () => {
        const testCases = [
            { text: '3   Institute of Creative Writing, Dreamland University, Fantasyville, FV 54321', firstName: '', lastName: '', name: '', suffix: '' },
            { text: 'Michael B. Jordan Sr.', firstName: 'Michael B.', lastName: 'Jordan', name: '', suffix: 'Sr' },
            // { text: 'Hans Christian Andersen', firstName: 'Hans Christian', lastName: 'Andersen', name: '', suffix: '' },
            { text: 'Sinta Petri Lestari', firstName: 'Sinta', lastName: 'Petri Lestari', name: '', suffix: '' },
            // cannot match Zet right now (not a first name) { text: 'Zet Ena', firstName: 'Zet', lastName: 'Ena', name: '', suffix: '' },
            { text: 'Alya Elita Sjioen', firstName: 'Alya', lastName: 'Elita Sjioen', name: '', suffix: '' },
            { text: 'Óscar de la Renta', firstName: 'Óscar', lastName: 'de la Renta', name: '', suffix: '' },
            { text: 'Michael B. Jordan', firstName: 'Michael B.', lastName: 'Jordan', name: '', suffix: '' },
            { text: 'Ana María López', firstName: 'Ana María', lastName: 'López', name: '', suffix: '' },
            { text: 'John Smith Jr.', firstName: 'John', lastName: 'Smith', name: '', suffix: 'Jr' },
            { text: 'John Smith', firstName: 'John', lastName: 'Smith', name: '', suffix: '' },
            { text: 'Jane Doe', firstName: 'Jane', lastName: 'Doe', name: '', suffix: '' },
            { text: 'Mei Thompson', firstName: 'Mei', lastName: 'Thompson', name: '', suffix: '' },
            { text: 'Emily N. Miller', firstName: 'Emily N.', lastName: 'Miller', name: '', suffix: '' },
            { text: 'Jane Doe Sr.', firstName: 'Jane', lastName: 'Doe', name: '', suffix: 'Sr' },
            { text: 'Jean-Luc Picard', firstName: 'Jean-Luc', lastName: 'Picard', name: '', suffix: '' },
            { text: 'Anne-Marie Slaughter', firstName: 'Anne-Marie', lastName: 'Slaughter', name: '', suffix: '' },
            { text: 'Paul van Dyk', firstName: 'Paul', lastName: 'van Dyk', name: '', suffix: '' },
            //{ text: 'Hua Cheng', firstName: 'Hua', lastName: 'Cheng', name: '', suffix: '' },
            { text: 'Andi Tri Haryono', firstName: 'Andi', lastName: 'Tri Haryono', name: '', suffix: '' },
            { text: 'Arthur C. Clarke', firstName: 'Arthur C.', lastName: 'Clarke', name: '', suffix: '' },
            { text: 'Sarah Michelle Gellar', firstName: 'Sarah Michelle', lastName: 'Gellar', name: '', suffix: '' },
            { text: 'Kurt Vonnegut Jr.', firstName: 'Kurt', lastName: 'Vonnegut', name: '', suffix: 'Jr' },
            //{ text: 'Gabriel García Márquez', firstName: 'Gabriel García', lastName: 'Márquez', name: '', suffix: '' },
            //{ text: 'Juan Martín del Potro', firstName: 'Juan Martín', lastName: 'del Potro', name: '', suffix: '' },
            // Non-western and single name cases
            //{ text: 'Cher', firstName: '', lastName: '', name: 'Cher', suffix: '' },
            //{ text: 'Pelé', firstName: '', lastName: '', name: 'Pelé', suffix: '' },
            //{ text: 'Dalai Lama', firstName: '', lastName: '', name: 'Dalai Lama', suffix: '' },
            //{ text: 'Sony Corporation', firstName: '', lastName: '', name: 'Sony Corporation', suffix: '' },
            //{ text: 'Samsung Electronics', firstName: '', lastName: '', name: 'Samsung Electronics', suffix: '' }
        ];

        testCases.forEach(({ text, firstName, lastName, name, suffix }) => {
            const doc = compromise(text);
            
            const extractedFirstName = doc.match('(#FirstName|#MiddleName)').out('text');
            const extractedLastName = doc.match('#LastName').out('text');
            const extractedName = doc.match('#Name').out('text');
            const extractedSuffix = doc.match('#Suffix').out('text');

            if (name?.length > 0) {
                expect(extractedName).to.equal(name, 'Trying to extract name from ' + text);
            } else {
                expect(extractedFirstName).to.equal(firstName, 'Trying to extract first name from ' + text);
                expect(extractedLastName).to.equal(lastName, 'Trying to extract last name from ' + text);
                expect(extractedSuffix).to.equal(suffix, 'Trying to extract suffix from ' + text);
            }
        });
    });
});