# Meta data

The meta data plugin detects anything from section types to keywords, authors, and affiliations.

If an intelligence file is provided the matching is improved using the supplied data and IDs.