import { EditorState, Transaction } from 'prosemirror-state';
import { Node } from 'prosemirror-model';
import { Action, TransportManuscriptFile } from './types.js';

const isNode = typeof process !== 'undefined' && process.release && process.release.name === 'node';

async function createThumbnail(imageUrl: string, maxWidth: number, opts: { document: Document }): Promise<string> {
  return new Promise((resolve, reject) => {
    // Create an image element
    const img = new Image();
    img.src = imageUrl;

    // Ensure the image is loaded before drawing it
    img.onload = () => {
      // Calculate the height to maintain the aspect ratio
      const scaleFactor = maxWidth / img.width;
      const calculatedHeight = img.height * scaleFactor;

      // Create a canvas element
      const canvas = opts.document.createElement('canvas');
      canvas.width = maxWidth; // Set width to maxWidth
      canvas.height = calculatedHeight; // Height calculated to maintain aspect ratio

      // Get the context of the canvas
      const ctx = canvas.getContext('2d');
      if (!ctx) {
        reject('Unable to get canvas context');
        return;
      }

      // Set the drawing quality to high if needed (optional)
      ctx.imageSmoothingQuality = 'high';

      // Draw the image on canvas
      ctx.drawImage(img, 0, 0, maxWidth, calculatedHeight); // Draw scaled image

      // Prepare to draw text
      ctx.font = 'bold 12px Arial'; // Set font size and family
      ctx.fillStyle = 'black'; // Set text color
      ctx.textAlign = 'right'; // Align text to the right
      ctx.textBaseline = 'top'; // Align text to the top

      // Draw text showing the original image dimensions
      const text = `preview: ${img.width} x ${img.height}`;
      ctx.fillText(text, maxWidth - 10, 10); // Position text in the top-right corner, with some padding

      // Get the resulting thumbnail image URL in WebP format
      const thumbnailUrl = canvas.toDataURL('image/webp'); // Saving as WebP

      // Resolve the Promise with the thumbnail URL
      resolve(thumbnailUrl);
    };

    // Handle image loading errors
    img.onerror = (err) => {
      console.error('Error loading the image:', err);
      reject(err);
    };
  });
}

/** Lists the actions available given a state
 * These may be based on tags or nodeids.
 */
export const actions = (input: { state: EditorState, manuscript: TransportManuscriptFile }): Action[] => {

  const pluginId = 'process-assets';
  let actions: any[] = [];
  const files = input.manuscript?.files || [];
  if (!files.some(f => f.url?.length > 0)) { return []; }

  input.state.doc.descendants((node, pos: number) => {
    const file = files.find(f => f.id === node.attrs.id);
    const nodeId = node.attrs?.id;
    if (node.type.name === 'figure' && file) {
      actions.push({
        id: nodeId + '-' + 'process-assets',
        pluginId,
        name: 'Updating the figure preview for ' + file.name,
        nodeId,
        node
      });
    } else if (node.type.name === 'image' && file) {
      actions.push({
        id: nodeId + '-' + 'process-assets',
        pluginId,
        name: 'Updating the image preview for ' + file.name,
        nodeId,
        node
      });
    }
  });

  return actions;
}

/**
* Takes a list of actions (which may include user decision) and applies them to the document.
* @param state 
* @param actions 
* @returns a transaction and any applied actions in the logs.
*/
export const patch = async (input: { state: EditorState, manuscript: TransportManuscriptFile }, actions: Action[]): Promise<{ tr: Transaction | undefined; logs: any[]; } | undefined> => {

  if (isNode) {
    const { JSDOM } = await import('jsdom');
    const dom = new JSDOM();
    document = dom.window.document;
  }

  if (!document) { throw new Error('document must be defined'); }

  const logs: any[] = [];
  const files = input.manuscript?.files || [];

  const images: any[] = [];

  const tr = input.state.tr;
  tr.doc.descendants((node, pos: number) => {
    if (node.type.name === 'figure' && files.some(f => f.id === node.attrs.id)) {
      const file = files.find(f => f.id === node.attrs.id);
      if (!file) { return; }
      images.push({ file, node, pos });
    } else if (node.type.name === 'image' && files.some(f => f.id === node.attrs.id)) {
      const file = files.find(f => f.id === node.attrs.id);
      if (!file) { return; }
      images.push({ file, node, pos });
    }
  });

  for (let { node, file, pos } of images) {
    const action = actions.find(a => a.nodeId === node.attrs.id);
    if (!action) { continue; }
    try {
      const thumbnail = await createThumbnail(file.url, 350, { document });
      if (thumbnail) {
        console.log('Updating thumbnail ' + node.attrs.id, node, pos, file, thumbnail.length);
        console.log('%c ', `font-size: 10rem; background:no-repeat center/90% url(${thumbnail});`);
        tr.doc.descendants((node, pos: number) => {
          if (node.type.name === 'figure' && node.attrs.id === file.id) {
            tr.setNodeAttribute(pos, 'src', thumbnail);
            tr.scrollIntoView();
            logs.push({
              ...action,
              thumbnail
            });
          } else if (node.type.name === 'image' && node.attrs.id === file.id) {
            tr.setNodeAttribute(pos, 'src', thumbnail);
            tr.scrollIntoView();
            logs.push({
              ...action,
              thumbnail
            });
          }
        });
      }
    } catch (e) {
      console.error('Could not load image', e);
    }
  }

  return { tr, logs };
}