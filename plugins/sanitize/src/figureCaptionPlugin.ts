export const figureCaptionPlugin = {
    /** Add custom tags */
    tags: {
        FigureCaption: {
            isA: 'Heading',
        }
    },

    /** Post-process tagger */
    compute: {
        postProcessFigureCaptions: (doc: any) => {
            if (doc.text().trim().startsWith('Figure') || doc.text().trim().startsWith('Abbildung')) {
                doc.match('Figure').tag('FigureCaption');
                doc.match('Abbildung').tag('FigureCaption');
            }

            if (doc.text().trim().startsWith('Table') || doc.text().trim().startsWith('Tabelle')) {
                doc.match('Table').tag('TableCaption');
                doc.match('Tabelle').tag('TableCaption');
            }
        }
    },

    /** Run it on init */
    hooks: ['postProcessFigureCaptions']
};