import { EditorState, Transaction } from 'prosemirror-state';
import { Node } from 'prosemirror-model';
import { fromJSON, SFNodeType } from '@sciflow/schema';

const findNode = (haystack: Node, matcher: (n: Node, pos: number, p: Node | null) => boolean, range?: { from: number; to: number; }): { pos: number; node: Node; parent?: Node } | undefined => {
    let matchedNode, matchedParent, matchedPos;
    if (!range) { range = { from: 0, to: haystack.content.size }; }
    haystack.nodesBetween(range.from, range.to, (n, pos, p) => {
        if (matcher(n, pos, p)) {
            matchedNode = n;
            matchedParent = p;
            matchedPos = pos;
            return false;
        }
    });

    return (matchedPos && matchedNode) ? { pos: matchedPos, node: matchedNode, parent: matchedParent } : undefined;
}

const findNodes = (haystack: Node, matcher: (n: Node, pos: number, p: Node | null) => boolean, range?: { from: number; to: number; }): { pos: number; node: Node; parent: Node | null }[] => {
    let matches: { node: Node; parent: Node | null; pos: number; }[] = [];
    if (!range) { range = { from: 0, to: haystack.content.size } }
    haystack.nodesBetween(range.from, range.to, (n, pos, p) => {
        if (matcher(n, pos, p)) {
            matches.push({ node: n, pos, parent: p });
        }
    });
    return matches;
};

/** Lists the actions available given a state
 * These may be based on tags or nodeids.
 */
export const actions = (input: { state: EditorState }) => {
    let actions: any[] = [];

    let headings: Node[] = [];
    const pluginId = 'sanitize';

    input.state.doc.descendants((node) => {
        if (node.type.name === 'heading') {
            headings.push(node);
        }
        let attrs: { tags?: { key: string; value?: any; }[] } = node.marks?.find(m => m.type.name === 'tags')?.attrs || {};
        if (attrs?.tags?.length && attrs?.tags?.length > 0) {
            for (let tag of attrs.tags) {

                let value;
                if (tag.value && typeof tag.value === 'string' && tag.value.startsWith('{')) {
                    value = JSON.parse(tag.value);
                }

                if (node.type.name === 'figure' || node.type.name === 'table') {
                    const nodeId = node.attrs.id;
                    switch (tag?.key) {
                        case 'table-with-caption':
                            actions.push({
                                id: nodeId + '-' + tag.key,
                                name: `Moving caption from a table row ${node?.textContent?.slice(0, 55) || nodeId}`,
                                nodeId,
                                node,
                                tag,
                                schema: {
                                    type: 'object',
                                    properties: {
                                        do: {
                                            name: 'Move caption',
                                            description: `Moving caption from a table row ${node?.textContent?.slice(0, 55) || nodeId}`,
                                            type: 'boolean',
                                            default: true
                                        }
                                    }
                                }
                            });
                            break;
                        case 'image-in-empty-table':
                            actions.push({
                                id: nodeId + '-' + tag.key,
                                name: `Remove the table surrounding the image ${value?.alt?.slice(0, 55) || node?.textContent?.slice(0, 55) || nodeId}`,
                                nodeId,
                                node,
                                tag,
                                schema: {
                                    type: 'object',
                                    properties: {
                                        do: {
                                            name: 'Remove table',
                                            description: `Remove the table surrounding the image ${value?.alt?.slice(0, 55) || node?.textContent?.slice(0, 55) || nodeId}`,
                                            type: 'boolean',
                                            default: true
                                        }
                                    }
                                }
                            });
                            break;
                        case 'equation-in-table':
                            actions.push({
                                id: nodeId + '-' + tag.key,
                                name: `Removing the table around the equation ${node?.textContent?.slice(0, 55) || nodeId}`,
                                nodeId,
                                node,
                                tag,
                                schema: {
                                    type: 'object',
                                    properties: {
                                        do: {
                                            name: 'Remove table',
                                            description: `Removing the table around the equation ${node?.textContent?.slice(0, 55) || nodeId}`,
                                            type: 'boolean',
                                            default: true
                                        }
                                    }
                                }
                            });
                            break;
                    }

                }
            }
        }
    });

    if (headings.length === 0) {
        actions.push({
            id: 'sanitize-no-headings',
            pluginId,
            name: 'No headings found',
            description: 'No headings were detected in the manuscript'
        });
    }
    return actions;
}

interface PluginOutput {
    tr?: Transaction | undefined;
    logs?: any[];
}

/**
 * Takes a list of actions (which may include user decision) and applies them to the document.
 * @param state 
 * @param actions 
 * @returns a transaction and any applied actions in the logs.
 */
export const patch = (input: { state: EditorState }, actions: any[] = []): PluginOutput | undefined => {
    if (!input.state) { return undefined; }
    const schema = input.state.schema;
    let tr = input.state.tr;

    const logs: any[] = [];
    for (let action of actions) {
        if (!action.tag) { continue; }

        // we must use the tr document since it will change after each transformation
        let match = findNode(tr.doc, (n) => n.attrs?.id === action.nodeId);
        if (!match?.node || !match?.pos) { return undefined; }
        let from = match?.pos;
        let to = match.pos + match?.node.content.size;

        let value;
        if (action.tag.value && typeof action.tag.value === 'string' && action.tag.value.startsWith('{')) {
            value = JSON.parse(action.tag.value);
        }

        // get the content between from and to we want to replace

        switch (action.tag.key) {
            case 'table-with-caption':
                if (!match.node) { break; }
                const captionRowId = JSON.parse(action.tag.value)?.captionNodeId;
                const captionRow = findNode(tr.doc, (n) => captionRowId && n.attrs?.id === captionRowId, { from, to });

                const captionContent: Node[] = [];
                captionRow?.node.forEach((c) => {
                    if ((c.type.name === SFNodeType.table_cell || c.type.name === SFNodeType.table_header) && c.childCount > 0) {
                        captionContent.push(c.child(0));
                    } else {
                        throw new Error('Unknown row content ' + c.type.name);
                    }
                });

                // if the table is inside a figure we need to replace the existing figure/caption
                if (match?.parent && from && match?.parent?.type?.name === SFNodeType.figure && captionRow) {
                    // delete the row from the table
                    tr.delete(captionRow.pos, captionRow.pos + captionRow.node.nodeSize);
                    match = findNode(tr.doc, (n) => n.attrs?.id === action.nodeId);
                    if (!match) { throw new Error('Expected to find node to update'); }

                    from = match.pos;
                    to = match.pos + match.node.content.size;
                    const res = tr.doc.resolve(from);
                    // expand from and to around the figure (was only the table before)
                    const parentPos = match.pos - res.parentOffset;
                    if (match.parent && match.parent?.type.name === SFNodeType.figure) {
                        from = parentPos - 1;
                        to = parentPos + match.parent.content.size;
                    }

                    // maintain existing captions
                    const caption = findNode(tr.doc, ((n) => n.type.name === SFNodeType.caption), { from, to });
                    // maintain existing captions
                    if (caption) {
                        // if a caption exists already we just add to it
                        tr.insert(caption.pos + 1, captionContent);
                    }
                    else {
                        // create a new caption
                        tr.insert(to, schema.nodes[SFNodeType.caption].create({}, captionContent));
                    }
                }
                else {
                    // create a figure around the table and place the table inside
                    const figure = schema.nodes[SFNodeType.figure].createAndFill();
                    const caption = schema.nodes[SFNodeType.caption].createAndFill();
                    // TODO we are expecting figures to exist for now
                    // move the table into a figure and add the caption
                }

                // update the match and check whether we want to fall through to image in empty table cleanups
                match = findNode(tr.doc, (n: Node) => n.attrs?.id === action.nodeId);
                if (!match) {
                    break;
                }

                // remove tables around single images inside a table with one cell
                const image = findNodes(match.node, (n: Node) => n.type.name === SFNodeType.image);
                if (match.node.textContent?.trim()?.length === 0 && image.length === 1) {
                    match = findNode(tr.doc, (n) => n.attrs?.id === action.nodeId);
                    if (match) {
                        from = match.pos;
                        to = match.pos + match.node.content.size;
                        const table = findNode(tr.doc, ((n) => (n.type.name === SFNodeType.table)), { from, to });
                        // delete the table
                        if (table && match.parent?.type.name === 'figure') {
                            tr.delete(table.pos, table.pos + table.node.nodeSize);
                            const figure = findNode(tr.doc, (n) => n.attrs?.id === match?.parent?.attrs.id);
                            if (figure) {
                                const attrs = {
                                    ...figure.node.attrs,
                                    src: image[0].node.attrs.src,
                                    id: image[0].node.attrs.id,
                                    width: image[0].node.attrs.width,
                                    height: image[0].node.attrs.height,
                                    alt: image[0].node.attrs.alt,
                                    title: image[0].node.attrs.title,
                                    type: 'image'
                                };

                                tr.setNodeMarkup(figure.pos, null, attrs, figure.node.marks);
                            }
                        }
                    }
                }
                break;
            case 'image-in-empty-table':
                const caption = findNode(tr.doc, ((n, pos) => (n.type.name === SFNodeType.caption)), { from, to });
                const table = findNode(tr.doc, ((n, pos) => (n.type.name === SFNodeType.table)), { from, to });
                const images = table && findNodes(table.node, (n) => n.type.name === SFNodeType.image);
                if (images?.length === 1) {
                    const image = images[0].node; // use just the first one for now
                    const figure = schema.nodes['figure'].createAndFill({
                        src: image.attrs.src,
                        id: image.attrs.id,
                        alt: image.attrs.alt
                    }, caption?.node ? [caption.node] : []);
                    if (figure) {
                        tr.replaceWith(from, to, figure);
                        logs.push(action);
                    }
                }
                break;
            case 'equation-in-table':
                const rows = findNodes(match.node, (n) => n.type.name === SFNodeType.table_row || n.type.name === SFNodeType.table_header);
                const numberingString = action.tag?.value?.numberingString;
                const equations: { node: any; numberingString?: string; }[] = []; // TODO add numbering strings here
                // we'll want to collect all equations in the table and then replace the original one
                for (let row of rows) {
                    const math = findNodes(row.node, (n) => n.type.name === SFNodeType.math);
                    if (math) {
                        equations.push(...math.map((eq) => ({ node: eq.node, numberingString: undefined })));
                    }
                }
                if (equations.length > 0) {
                    const content = equations
                        .filter(e => e.node.attrs.tex?.length > 0)
                        .map(eq => schema.nodes['paragraph'].createAndFill({}, [schema.nodes['math'].create({
                            id: eq.node.attrs?.id,
                            style: numberingString?.length > 0 ? 'block' : 'inline',
                            tex: eq.node.attrs?.tex,
                            label: numberingString
                        })]))
                        .filter(eq => eq != null);
                    tr.replaceWith(from, to, content as Node[]);
                    logs.push(action);
                }
                break;
        }
    }
    if (tr.docChanged) {
        return {
            tr, logs: logs.map(l => {
                delete l.node;
                return l;
            })
        };
    }
    return undefined;
}