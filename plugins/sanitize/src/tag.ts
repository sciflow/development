import nlp from 'compromise';

import { SFMarkType, SFNodeType } from '@sciflow/schema';
import { EditorState } from 'prosemirror-state';
import { figureCaptionPlugin } from './figureCaptionPlugin.js';

nlp.plugin(figureCaptionPlugin);

export const tag = (input: { state: EditorState, manuscript: any, stats?: any; intelligence?: any; }) => {
    const manuscript = input.manuscript;
    let state = input.state;
    const tr = state.tr;
    const tagMarkType = input.state.schema.marks[SFMarkType.tags];
    let logs: any[] = [];

    tr.doc.descendants((node, pos: number) => {
        if (node.type.name === SFNodeType.table) {
            if (node.childCount === 0) { return; }
            const lastRow = node.child(node.childCount - 1);
            const firstRow = node.child(0);
            const lastRowIsFigureCaption = nlp(lastRow.textContent).has('#FigureCaption');
            const firstRowIsFigureCaption = nlp(firstRow.textContent).has('#FigureCaption');
            const lastRowIsTableCaption = nlp(lastRow.textContent).has('#TableCaption');
            const firstRowIsTableCaption = nlp(firstRow.textContent).has('#TableCaption');

            let figureCaptionRow;
            if (firstRowIsFigureCaption) { figureCaptionRow = firstRow; }
            if (lastRowIsFigureCaption) { figureCaptionRow = lastRow; }

            // handle figures where the only text should be the caption itself
            if (figureCaptionRow && node.textContent === figureCaptionRow.textContent) {
                let attrs = node.marks?.find(m => m.type.name == tagMarkType.name)?.attrs;
                tr.removeNodeMark(pos, tagMarkType);
                let tags = attrs?.tags || [];
                tags = [{ key: 'table-with-caption', value: JSON.stringify({ captionNodeId: figureCaptionRow.attrs.id, type: 'figure' }) }, ...tags]
                .filter((v, index, array) => array.findIndex(v2 => v2.key === v.key) === index);
                tr.addNodeMark(pos, tagMarkType.create({
                    ...(attrs || {}),
                    tags
                }));
            }

            let tableCaptionRow;
            if (firstRowIsTableCaption) { tableCaptionRow = firstRow; }
            if (lastRowIsTableCaption) { tableCaptionRow = lastRow; }

            // handle tables where the caption could be one of the rows
            if (tableCaptionRow) {
                let attrs = node.marks?.find(m => m.type.name == tagMarkType.name)?.attrs;
                tr.removeNodeMark(pos, tagMarkType);
                let tags = attrs?.tags || [];
                tags = [{ key: 'table-with-caption', value: JSON.stringify({ captionNodeId: tableCaptionRow.attrs.id, type: 'table' }) }, ...tags]
                .filter((v, index, array) => array.findIndex(v2 => v2.key === v.key) === index);
                tr.addNodeMark(pos, tagMarkType.create({
                    ...(attrs || {}),
                    tags
                }));
            }

            if (node.childCount === 1) { }
            if (node.childCount === 2) { }
        }
    });

    if (tr.docChanged) {
        state = state.apply(tr);
    }

    return { tr, state, logs, manuscript };
};
