import { EditorState, Transaction } from 'prosemirror-state';
import { Node } from 'prosemirror-model';
import { Action } from './types.js';

/** Lists the actions available given a state
 * These may be based on tags or nodeids.
 */
export const actions = (input: { state: EditorState }): Action[] => {
  let actions: any[] = [];

  let prev: { node: Node; parent: Node | null } | undefined;
  input.state.doc.descendants((node: Node, pos: number, parent: Node | null) => {
    let attrs: { tags?: { key: string; value?: any; }[] } = node.marks?.find(m => m.type.name === 'tags')?.attrs || {};
    const nodeId = node.attrs?.id;
    if (attrs.tags?.some(t => t.key === 'caption')) {
      if (prev?.parent?.type?.name === 'figure') {
        // check if there is no caption or if it is empty
        if (!(prev?.node?.type.name === 'caption') || prev?.node?.textContent?.length === 0) {
          actions.push({
            id: nodeId + '-' + 'caption',
            name: 'Joining caption: ' + node.textContent?.slice(0, 10),
            nodeId,
            figureId: prev?.parent.attrs.id,
            node
          });
        }
      } else if (prev?.node?.type?.name === 'image' && prev?.parent?.type?.name === 'paragraph' && prev?.parent?.childCount === 1) {
        actions.push({
          id: nodeId + '-' + 'caption',
          name: 'Joining caption with previous image: ' + node.textContent?.slice(0, 10),
          nodeId,
          imageParagraphId: prev?.parent.attrs.id,
          node
        });
      }
    }

    prev = { node, parent };
  });

  return actions;
}

/**
* Takes a list of actions (which may include user decision) and applies them to the document.
* @param state 
* @param actions 
* @returns a transaction and any applied actions in the logs.
*/
export const patch = async (input: { state: EditorState }, actions: Action[]): Promise<{ tr: Transaction | undefined; logs: any[]; } | undefined> => {
  const tr = input.state.tr;
  const schema = input.state.schema;
  const logs = [];

  for (const action of actions) {
    let newCaption: { node: Node; pos: number } | undefined;
    // delete the caption node
    const matches: { node: Node, pos: number }[] = [];
    tr.doc.descendants((node, pos, _parent) => {
      if (action.nodeId && node?.attrs?.id === action.nodeId) {
        matches.push({ node, pos });
        return true;
      }
    });

    const m = matches[0];
    if (m) {
      newCaption = { node: m.node, pos: m.pos };
      tr.deleteRange(m.pos, m.pos + m.node.nodeSize);
    }

    if (matches.length > 1) {
      console.error('Node ' + m?.node?.attrs.id + ' existed more than once', { nodeId: action.nodeId, name: action.name, matches });
      throw new Error('Node ' + m?.node?.attrs.id + ' existed more than once');
    }

    let figureOrImageParagraph: { node: Node; pos: number } | undefined; // find the figure
    let figureContent: Node[] = [];
    let figureAttrs: any = {};
    // find the figure/paragraph to replace
    tr.doc.descendants((node, pos, parent) => {
      if (!node?.attrs?.id) { return; }
      if (action.figureId) {

        // collect content
        // go through all nodes where the figure is the parent
        if (figureOrImageParagraph && parent?.attrs?.id != undefined && figureOrImageParagraph?.node?.attrs?.id === parent?.attrs?.id) {
          if (node.type.name !== 'caption') {
            figureContent.push(node);
          }
        }

        if (node?.attrs?.id !== action.figureId) { return; }
        figureOrImageParagraph = { node, pos };
        figureAttrs = { ...node.attrs };

      } else if (action.imageParagraphId) {
        if (node?.attrs?.id === action.imageParagraphId) { figureOrImageParagraph = { node, pos }; }
        // we use the image id and alt, src to create the figure
        if (figureOrImageParagraph && parent?.attrs?.id != undefined && figureOrImageParagraph?.node?.attrs?.id === parent?.attrs?.id) {
          if (node.type.name === 'image') {
            figureAttrs = { ...node.attrs };
          }
        }
      }
    });

    if (!figureOrImageParagraph || !newCaption) { return { tr, logs }; }

    const caption = schema.nodes['caption'].create({}, newCaption.node);
    const newFigure = schema.nodes['figure'].create({ ...figureAttrs }, [...figureContent, caption]);
    logs.push(action);
    tr.replaceWith(figureOrImageParagraph.pos, figureOrImageParagraph.pos + figureOrImageParagraph.node?.nodeSize, newFigure);
  }

  tr.doc.check();

  return { tr, logs };
}