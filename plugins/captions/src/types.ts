export interface TransportManuscriptFile {
    authors: any[];
    /** references as CSL */
    references?: any[];
    /** A list of all files used in the document (e.g. to replace thumbnails with large resolution images in export) */
    files?: any[];
    metaData?: object;
}

export interface Action {
    /** A deterministic unique (functional) id for the action */
    id: string;
    name: string;
    nodeId?: string;
    node?: any;
    figureId?: string; // if the caption should be joined with a previous figure
    imageParagraphId?: string; // if the caption should be joined with a previous paragraph
    tag?: any;
    /** A JSON schema for any needed input data */
    schema: any;
}