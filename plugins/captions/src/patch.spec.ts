import { expect } from 'chai';

import mock from './fixtures/captioned-table1.json' assert { type: "json" };
import { actions, patch } from './patch.js';
import sfSchemas from '@sciflow/schema';
import { EditorState } from 'prosemirror-state';

describe('Table caption', async () => {
    it('Should not remove a table when joining the caption', async () => {
        const doc = (sfSchemas as any).fromJSON('manuscript')(mock);
        const state = EditorState.create({ doc });
        const pluginActions = actions({ state });
        expect(pluginActions.length).to.equal(1);
        expect(pluginActions[0].nodeId).to.equal('paragraph-with-caption');
        
        const patchResult = await patch({ state }, pluginActions);
        expect(patchResult?.logs?.[0]?.nodeId).to.equal('paragraph-with-caption');

        const figure = patchResult?.tr?.doc.toJSON().content[1];
        expect(figure.type).to.equal('figure');
        expect(figure.content.length).to.equal(2);
        expect(figure.content[1].type).to.equal('caption');
        expect(figure.content[0].type).to.equal('table');
    });
});