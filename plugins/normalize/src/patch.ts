import { EditorState, Transaction } from 'prosemirror-state';
import { Mark, MarkType, Node, NodeType } from 'prosemirror-model';
import { fromJSON, SFNodeType } from '@sciflow/schema';
import crypto from 'crypto';

interface PluginOutput {
    tr?: Transaction | undefined;
    logs?: any[];
}

export const actions = (input: { state: EditorState }) => {
    let actions: any[] = [];

    let headings: Node[] = [];
    const pluginId = 'normalize';

    let mustFixIDs = false;
    const tr = input.state.tr;
    tr.doc.descendants((node: Node) => {
        let { id, ...rest } = node.attrs;
        const marks = node.marks || [];

        const nodeHasIdAttr = typeof id !== 'undefined';
        const marksHaveIdAttr = marks.find((m: Mark) => m.attrs && m.attrs.id !== 'undefined');

        if (nodeHasIdAttr || marksHaveIdAttr) {
            mustFixIDs = true;
        }
    });

    if (mustFixIDs) {
        actions.push({
            id: 'normalize-ids',
            name: 'Checked document for the first time and added missing IDs',
            nodeId: null,
            node: null,
            tag: null,
        });
    }

    return actions;
}

export const createId = (): string => 'abcdefghijklmnopqrstuvwxyz'.charAt(Math.floor(Math.random() * 26)) + Math.random().toString(36).substr(2, 12);

/**
 * Takes a list of actions (which may include user decision) and applies them to the document.
 * @param state 
 * @param actions 
 * @returns a transaction and any applied actions in the logs.
 */
export const patch = (input: { state: EditorState }, actions: any[] = []): PluginOutput | undefined => {
    if (!input.state) { return undefined; }
    const schema = input.state.schema;
    const tr = input.state.tr;

    const ids: { [key: string]: true; } = {};

    const logs = [...actions];

    /** Creates a hash based on the node content */
    const generateDeterministicId = (node: Node, parentId?: string) => {
        const hashData = JSON.stringify({
            text: node.textContent || '',
            attrs: { ...node.attrs, id: undefined }, // Exclude existing ID from hashing
            parentId: parentId || ''
        });
        return crypto.createHash('sha256').update(hashData).digest('hex').slice(0, 12);
    };
    
    const getUniqueId = (node: Node, parent: Node | null) => {
        const parentId = parent?.attrs?.id;
        let newId = generateDeterministicId(node, parentId);
        if (parentId) { newId = `${parentId.substr(0,5)}-${newId}`; }
        while (ids[newId]) { newId = newId.slice(0, 10) + createId().slice(-2); }
        ids[newId] = true;
        return newId;
    };

    /** mark an id as used */
    const markAsUsed = (id: string) => {
        if (!id) { return; }
        ids[id] = true;
    }
    const isUsed = (id: string) => { return ids[id]; }

    const hasId = (obj: any, isNode = true) => {
        let type;
        if (isNode) {
            type = schema.nodes[obj.type.name];
            if (!type) {
                throw new Error('Unknown node type ' + obj.type.name);
            }
        }
        else {
            type = schema.marks[obj.type.name];
            if (!type) {
                throw new Error('Unknown mark type ' + obj.type.name);
            }
        }
        return (type as any)?.attrs?.id != undefined;
    };

    if (actions.length > 0) {
        tr.doc.descendants((node, pos, parent) => {

            let { id, ...rest } = node.attrs;
            const marks = node.marks || [];
            const nodeHasIdAttr = hasId(node);
            const marksHaveIdAttr = marks.some(m => hasId(m, false));

            if (marksHaveIdAttr) {
                for (let mark of marks) {
                    if (mark.attrs.id === null || isUsed(mark.attrs.id)) {
                        const newMark = schema.marks[mark.type.name].create({ ...mark.attrs, id: getUniqueId(node, parent) });
                        tr.removeMark(pos, pos + node.nodeSize, mark);
                        tr.addMark(pos, pos + node.nodeSize, newMark);
                    } else if (mark.attrs.id?.length > 0) {
                        markAsUsed(mark.attrs.id);
                    }
                }
            }
            
            if (nodeHasIdAttr) {
                if (id === null || isUsed(id)) {
                    // get a new id for the node if needed
                    id = getUniqueId(node, parent);
                    tr.setNodeMarkup(pos, null, { id, ...rest }, marks.map(mark => {
                        if (mark.attrs.id === null) {
                            // @ts-ignore
                            mark.attrs.id = getUniqueId(node, parent);
                        }
                        return mark;
                    }));
                } else {
                    markAsUsed(node.attrs.id);
                }
            }
        });

    }

    if (tr.docChanged) {
        return {
            logs,
            tr
        }
    }

    return undefined;
}