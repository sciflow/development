#!/bin/sh

for dir in */ ; do
  if [ -d "$dir" ]; then
    echo "Processing $dir"
    cd "$dir" || exit
    
    yarn
    yarn build
    cd ..
  fi
done

echo "Plugins have been built."