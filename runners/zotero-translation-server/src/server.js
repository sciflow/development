const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const { post } = require('koa-route');

const { parseReferenceFile } = require("./zotero-translate");

parseReferenceFile(`@Book{hicks2001,
    author    = "von Hicks, III, Michael",
    title     = "Design of a Carbon Fiber Composite Grid Structure for the GLAST
                 Spacecraft Using a Novel Manufacturing Technique",
    publisher = "Stanford Press",
    year      =  2001,
    address   = "Palo Alto",
    edition   = "1st,",
    isbn      = "0-69-697269-4"
   }`).then(console.log).catch(console.error);

const app = new Koa();
app.use(bodyParser({ enableTypes: ['text', 'json'] }));
app.use(post('/', async (ctx) => ctx.response.body = JSON.stringify(await parseReferenceFile(ctx.request.body || ''), null, 2)));
app.listen('3000');
