const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");
const glob = require('glob');

const path = require("path");
const fs = require("fs");
const nodeExternals = require('webpack-node-externals');
const ChangeDetectionPlugin = require('./build-tools/change-detection-plugin')

const esmBuild = require('./webpack.esm.config');

let entry = {
    'server/server.cjs': path.join(__dirname, 'server/src/server.ts'),
    'libs/export/build-schema.cjs': path.join(__dirname, "libs/export/src/html/build-schema.ts")
};

const externals = nodeExternals({
    additionalModuleDirs: [
        "node_modules",
        path.join(__dirname, "node_modules")
    ],
    allowlist: []
});

glob.sync('libs/*/src/public-api.ts').forEach((fileName) => {
    const name = fileName.replace('libs/', '').replace('/src/public-api.ts', '');
    entry['libs/' + name + '/index.cjs'] = path.join(__dirname, fileName);
});

const cjsBuild = {
    entry,
    target: 'node20',
    mode: process.env.NODE_ENV === 'development' ? 'development' : 'production',
    optimization: {
        minimize: false,
        removeAvailableModules: false,
        removeEmptyChunks: false,
        splitChunks: false
    },
    externals: [externals],
    plugins: [
        new CopyPlugin({
            patterns: [
                {
                    from: path.join(__dirname, "libs/*/package.json"),
                    to: "[path]/[name][ext]",
                    toType: 'template'
                },
                {
                    from: path.join(__dirname, "node_modules/basscss-sass/scss"),
                    to: "libs/tdk/basscss-sass/scss"
                },
                {
                    from: path.join(__dirname, "libs/*/package.json"),
                    to: "[path]/[name][ext]",
                    toType: 'template'
                },
                {
                    from: path.join(__dirname, "libs/*/tsconfig.json"),
                    to: "[path]/[name][ext]",
                    toType: 'template'
                },
                { from: path.join(__dirname, "libs/tdk/src/styles"), to: "libs/tdk/styles" },
                {
                    from: path.join(__dirname, "libs/schema/src/types/*"), async to({ absoluteFilename }) {
                        // export the (renderers|components)/name/name.schema.ts part
                        return 'libs/schema' + absoluteFilename.replace(path.join(absoluteFilename, '..', '..', '..'), '');
                    }
                },
                {
                    from: path.join(__dirname, "libs/export/src/html/**/*.(schema|example|interface).@(ts|js|json)"), async to({ absoluteFilename }) {
                        // export the (renderers|components)/name/name.schema.ts part
                           return absoluteFilename.replace(
                             path.join(__dirname, 'libs/export/src/html/'),
                             'libs/export/',
                           );
                    }
                },
                {
                    from: path.join(__dirname, "libs/export/src/html/**/*.scss"), async to({ absoluteFilename }) {
                        // copy all style files
                        const prefix = path.join(__dirname, "libs/export/src/html");
                        return 'libs/export' + absoluteFilename.replace(prefix, '');
                    }
                },
                {
                    from: path.join(__dirname, "libs/export/src/html/**/*.hbs"), async to({ absoluteFilename }) {
                        // export the (renderers|components)/name/name.hbs part
                        return 'libs/export' + absoluteFilename.replace(path.join(absoluteFilename, '..', '..', '..'), '');
                    }
                },
                { from: path.join(__dirname, "libs/export/src/html/**/*.schema.ts"), to: "server" },
                { from: path.join(__dirname, "server/package.json"), to: "server" },
                { from: path.join(__dirname, "libs/cite/src/csl"), to: "server/csl" },
                { from: path.join(__dirname, "libs/cite/src/csl"), to: "csl" },
                { from: path.join(__dirname, "server/src/static"), to: 'server/static' }
            ]
        }),
        new ChangeDetectionPlugin({
          	directoryPattern: path.join(
            	__dirname,
            	'libs/export/src/html/**/*.schema.@(ts|tsx)',
          	),
          	script: path.join(__dirname, 'build-tools/build-component-schema.ts'),
            tsconfig: path.join(__dirname, 'libs/export/tsconfig.json')
        }),
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name]',
        libraryTarget: 'commonjs2'
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.xml$/i,
                use: [
                    {
                        loader: 'raw-loader',
                        options: {
                            esModule: false,
                        },
                    },
                ],
            },
            {
                test: /\.([cm]?ts|tsx)$/,
                use: [
                    {
                        loader: 'ts-loader'
                    }
                ],
                exclude: /node_modules/
            },
            {
                test: /\.(graphql|gql)$/,
                exclude: /node_modules/,
                loader: 'graphql-tag/loader',
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    {
                        loader: 'raw-loader',
                        options: {
                            esModule: false,
                        },
                    },
                ]
            }
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".cjs", ".js", ".d.ts", ".mjs", ".graphql"],
        extensionAlias: {
            ".js": [".js", ".ts"],
            ".cjs": [".cjs", ".cts"],
            ".mjs": [".mjs", ".mts"]
        },
        plugins: [
            new TsconfigPathsPlugin({ configFile: path.join(__dirname, "./tsconfig.json") })
        ]
    }
};

module.exports = [cjsBuild, ...esmBuild];
