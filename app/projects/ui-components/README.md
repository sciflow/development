# UiComponents

## Development

To develop the library, run in the terminal:

```bash
ng build ui-components --watch
```

In a separate terminal, navigate to the `app/dist/libs/ui-components` directory and run:

```bash
yarn link
```

Ensure that the symlink is pointing towards the `app/dist/libs/ui-components` directory.

For applications using this library in development, ensure that it is installed via `package.json` located in your application folder.

```json
{
  "dependencies": {
    ...
    "@sciflow/ui-components": "../platform/sfo/app/dist/libs/ui-components"
  }
}
```

In your main application (that's consuming the library) directory, run: `yarn link @sciflow/ui-components`.

## Publishing

After building your library with `ng build ui-components`, go to the dist folder `cd dist/ui-components` and run `npm publish`.

## Running unit tests

Run `ng test ui-components` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
