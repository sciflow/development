export type AdmonitionType = 'info' | 'success' | 'warning' | 'tip' | 'note' | 'danger' | 'default';
