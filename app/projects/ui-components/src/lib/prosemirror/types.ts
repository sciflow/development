// TODO (duplicate): Was running into issues importing from project/editor but needed for being able to execute prosemirror commands
export interface Command {
  id?: string;
  materialIconName?: string;
  svgIcon?: string;
  tooltip: string;
  run: (state, dispatch) => any;
  active: boolean;
}
