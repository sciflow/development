import { NgModule } from '@angular/core';
import { SfoDynamicFormModule } from '../form-builder';
import { SharedModule } from '../shared';
import { NodeMetaEditorDialog } from './node-meta-editor-dialog/node-meta-editor-dialog.component';

@NgModule({
  declarations: [NodeMetaEditorDialog],
  imports: [SharedModule, SfoDynamicFormModule],
  exports: [NodeMetaEditorDialog],
})
export class SfoProseMirrorUiModule {}
