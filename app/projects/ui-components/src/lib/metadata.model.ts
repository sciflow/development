import {JSONSchema7, JSONSchema7Type} from 'json-schema';

export interface Example {
  _comment: string;
  [key: string]: string | boolean | string[] | [key: string] | string;
}

export type ExampleTypes = Example | string | number;

/**
 * @fileoverview SfoUiJSONSchema7
 * @typedef {object} SfoUiJSONSchema7
 */
export interface SfoUiJSONSchema7 extends JSONSchema7 {
  properties?: {
    [key: string]: SfoUiJSONSchema7;
  };

  /** UI Icon name to be consumed by the Angular Material Icon library */
  _ui_icon?: string;

  /** SFO Widget field to be consumed for specialized UI components from the `SfoWidgetModule` */
  _ui_widget?: string;

  /** Read me description to be consumed by the UI components */
  _readme?: string;

  /** Optional sub-schema for array items */
  items?: SfoUiJSONSchema7;

  /** Default value of the field */
  default?: JSONSchema7Type;

  /** Defined list of pre-defined values for consumption. */
  examples?: ExampleTypes[];
}

// TODO: delete this when no longer used
/**
 * @deprecated Please use `SfoUiJSONSchema7` instead.
 */
export interface MetaData extends SfoUiJSONSchema7 {}
