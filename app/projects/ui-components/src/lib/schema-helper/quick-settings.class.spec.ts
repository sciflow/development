import { TestBed } from '@angular/core/testing';
import { SfoUiJSONSchema7 } from '../metadata.model';
import thesisVariantOne from './fixtures/thesis-template-variant-1.json';
import thesis from './fixtures/thesis-template.json';
import { SfoQuickSettingsService } from './quick-settings.class';
import { SfoSchemaHelperModule } from './schema-helper.module';

describe('SfoQuickSettingsService', () => {
  let service: SfoQuickSettingsService;
  let schema;
  let schemaTwo;

  beforeEach(() => {
    schema = JSON.parse(JSON.stringify(thesis));
    schemaTwo = JSON.parse(JSON.stringify(thesisVariantOne));

    TestBed.configureTestingModule({ imports: [SfoSchemaHelperModule] });
    service = TestBed.inject(SfoQuickSettingsService);
  });

  it('GIVEN - default set up - WHEN - instantiated - THEN - it should be created', () => {
    expect(service).toBeTruthy();
  });

  it('GIVEN - a schema with a settings configuration - WHEN - getFields is called - THEN - it should return the quick settings fields', () => {
    // ARRANGE
    const expectedSchema: SfoUiJSONSchema7 = {
      title: 'Thesis template (v2) schema',
      description: 'All configurations that work with this template',
      type: 'object',
      properties: {
        'Page-PdfPage': {
          title: 'PDF essentials',
          description: 'Set up page margins, fonts and page sizes.',
          type: 'object',
          properties: {
            page: {
              type: 'object',
              description: 'Page Size, Fonts, Margins, etc.',
              properties: {
                size: {
                  title: 'Size',
                  description: 'The page size.',
                  examples: ['A4', 'A5', '17cm 24cm'],
                  default: 'A4',
                  type: 'string',
                } as SfoUiJSONSchema7,
              },
            },
            typesetting: {
              type: 'object',
              title: 'Typesetting',
              description: 'General options for typesetting.',
              properties: {
                indent: {
                  title: 'Indentation',
                  'description': 'Whitespace before the first line of a paragraph',
                  default: '0mm',
                  type: 'string',
                } as SfoUiJSONSchema7,
                'paragraphSpacing': {
                  'title': 'Paragraph spacing',
                  'description': 'Space between two paragraphs',
                  'default': '2mm',
                  'anyOf': [
                    {
                      'type': 'string',
                    },
                    {
                      'type': 'number',
                    },
                  ],
                } as SfoUiJSONSchema7,
                'lineHeight': {
                  'title': 'Line height',
                  'default': 1.2,
                  'anyOf': [
                    {
                      'type': 'string',
                    },
                    {
                      'type': 'number',
                    },
                  ],
                } as SfoUiJSONSchema7,
              },
              examples: [
                {
                  _comment: 'Apa style spacing.',
                  indent: '1cm',
                  paragraphSpacing: '0',
                },
                {
                  _comment: 'Default spacing using paragraphs.',
                  indent: '0',
                  paragraphSpacing: '2mm',
                },
              ],
            },
          },
          $schema: 'http://json-schema.org/draft-07/schema#',
          component: {
            kind: 'Page',
            metadata: {
              name: 'PdfPage',
            },
            runners: ['princexml'],
          },
          $id: '/template/schema/sciflow-thesis2-page-pdfpage',
        },
      },
    } as SfoUiJSONSchema7;

    // ACT
    const fields = SfoQuickSettingsService.getQuickSettingsSchema(schema);

    // ASSERT
    expect(fields).toEqual(expectedSchema);
  });

  it('GIVEN - a schema examples on root and nested properties - WHEN - getFields is called - THEN - it should return the quick settings fields', () => {
    // ARRANGE
    const expectedSchema: SfoUiJSONSchema7 = {
      title: 'Thesis template (v2) schema',
      description: 'All configurations that work with this template',
      type: 'object',
      properties: {
        'Page-PdfPage': {
          type: 'object',
          properties: {
            page: {
              type: 'object',
              properties: {
                size: {
                  title: 'Size',
                  description: 'The page size.',
                  examples: ['Test-1', 'Test-2', 'Test-3'],
                  default: 'A4',
                  type: 'string',
                } as SfoUiJSONSchema7,
                margins: {
                  type: 'object',
                  properties: Object({
                    outer: Object({
                      title: 'The outer margin',
                      default: '20mm',
                      type: 'string',
                      examples: ['1mm', '2mm', '3mm'],
                    }),
                  }),
                } as SfoUiJSONSchema7,
              },
              examples: [{ _comment: 'Test Example Page 1', size: '1cm' }],
            },
            typesetting: {
              type: 'object',
              properties: {
                indent: {
                  title: 'Indentation',
                  description: 'Whitespace before at the start of a new paragraph',
                  default: '0mm',
                  type: 'string',
                } as SfoUiJSONSchema7,
                paragraphSpacing: {
                  title: 'Paragraph spacing',
                  description: 'Space between two paragraphs',
                  default: '2mm',
                  type: 'string',
                } as SfoUiJSONSchema7,
                lineHeight: {
                  title: 'Line height',
                  default: '1.6',
                  type: 'string',
                } as SfoUiJSONSchema7,
              },
              examples: [
                {
                  _comment: 'Apa style spacing.',
                  indent: '1cm',
                  paragraphSpacing: '0',
                },
                {
                  _comment: 'Default spacing using paragraphs.',
                  indent: '0',
                  paragraphSpacing: '2mm',
                },
              ],
            },
          },
          component: {
            kind: 'Page',
            metadata: {
              name: 'PdfPage',
            },
            runners: ['princexml'],
          },
          $id: '/template/schema/sciflow-thesis2-page-pdfpage',
        },
      },
    } as SfoUiJSONSchema7;

    // ACT
    const fields = SfoQuickSettingsService.getQuickSettingsSchema(schemaTwo);

    // ASSERT
    expect(fields).toEqual(expectedSchema);
  });

  it('GIVEN - a schema with no quickSettings configured - WHEN - getQuickSettingsSchema is called - THEN - it should return undefined', () => {
    // ARRANGE
    const modifiedSchema = JSON.parse(JSON.stringify(schema));
    modifiedSchema.properties.Settings.properties = {};

    // ACT
    const getFields = SfoQuickSettingsService.getQuickSettingsSchema(modifiedSchema);

    // ASSERT
    expect(getFields).toBe(undefined);
  });

  describe('WITH - a schema with settings configuration and NO components', () => {
    const customSchema: SfoUiJSONSchema7 = JSON.parse(JSON.stringify(thesis));

    beforeEach(() => {
      const componentName: string = 'Page-PdfPage';

      if (customSchema.properties?.[componentName]) {
        delete customSchema.properties?.[componentName];
      }
    });

    it('GIVEN - pre-conditions - WHEN - getQuickSettingsSchema is called - THEN - it should throw an error', () => {
      // ARRANGE
      // ACT
      const getFields = () => SfoQuickSettingsService.getQuickSettingsSchema(customSchema);

      // ASSERT
      expect(getFields).toThrowError('Schema does not contain component: Page-PdfPage');
    });
  });

  describe('WITH - a schema with no settings configuration', () => {
    const customSchema: SfoUiJSONSchema7 = JSON.parse(JSON.stringify(thesis));

    beforeEach(() => {
      if (customSchema.properties?.Settings) {
        delete customSchema.properties?.Settings;
      }
    });

    it('GIVEN - pre-conditions - WHEN - getQuickSettingsSchema is called - THEN - it should return undefined', () => {
      // ARRANGE
      // ACT
      const result = SfoQuickSettingsService.getQuickSettingsSchema(customSchema);
      // ASSERT
      expect(result).toEqual(undefined);
    });
  });
});
