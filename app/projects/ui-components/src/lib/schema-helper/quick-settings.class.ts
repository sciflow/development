import { Example, SfoUiJSONSchema7 } from '../metadata.model';
import { patchObject } from './utils/quick-settings-helper';

/**
 * Represents a setting for a component defined in the configuration.
 * @interface Setting
 * @example
 * [
 *   {
 *     "component": {
 *       "name": "Page-PdfPage",
 *       "options": [
 *         {
 *           "path": "page.size"
 *         },
 *       ]
 *     }
 *   }
 * ]
 */
interface Setting {
  /**
   * The component information for the setting.
   * @property {object} component
   * @property {string} component.name - The name of the configuration.
   * @property {SettingOption[]} component.options - The options for the setting.
   */
  component: {
    name: string;
    options: SettingOption[];
  };
}

/**
 * Represents an option within a setting.
 * @interface SettingOption
 * @example
 * {
 *   "path": "page.size",
 *   "title": "Size",
 *   "description": "The page size."
 * }
 */
interface SettingOption {
  path: string;
  title: string;
  description: string;
  [key: string]: unknown;
}

export class SfoQuickSettingsService {
  /**
   * Patches a sourceObject with a MetaData object
   * @param sourceObject - The values that need to be applied.
   * @param targetSchema - The targeted object that needs to be patched
   * @returns The patched MetaData object
   */
  static patchQuickSettingsSchema(
    sourceObject: object,
    targetSchema: SfoUiJSONSchema7,
  ): SfoUiJSONSchema7 {
    return patchObject(sourceObject, targetSchema) as SfoUiJSONSchema7;
  }

  /**
   * Get the quick setting fields
   * @param schema - The schema
   * @returns The quick settings schema if found, otherwise undefined.
   */
  static getQuickSettingsSchema(schema: SfoUiJSONSchema7): SfoUiJSONSchema7 | undefined {
    const quickSettings: Setting[] = this.getCustomSettingComponents(schema, 'quickSettings');

    if (quickSettings.length === 0) {
      return;
    }

    const filteredSchema: SfoUiJSONSchema7 = {
      ...schema,
      properties: {},
    };

    for (const component of quickSettings) {
      if (!schema.properties) return;

      const subSchema: SfoUiJSONSchema7 = schema.properties[component?.component?.name];

      if (!subSchema) {
        throw new Error(`Schema does not contain component: ${component?.component?.name}`);
      }

      const paths: string[] = component.component.options.map((option) => option.path);

      if (paths.length === 0) {
        console.warn('Schema does not contain Options for: ', component?.component?.name);
        continue;
      }

      const result = SfoQuickSettingsService.traverse(subSchema, [], paths, component);

      if (result.remainingPaths.length !== 0) {
        console.warn('Paths not resolved', result.remainingPaths);
      }

      if (!filteredSchema.properties) {
        filteredSchema.properties = {};
      }

      filteredSchema.properties[component.component.name] = result.updatedSchema;
    }

    return filteredSchema;
  }

  /**
   * Get custom setting components from the schema
   * @param schema - The schema
   * @param customSetting - The custom setting to retrieve
   * @returns An array of settings
   */
  static getCustomSettingComponents(schema: unknown, customSetting: string): Setting[] {
    const settings = schema?.['properties'].Settings;

    if (!settings?.properties[customSetting]) {
      return [];
    }

    const settingComponents: Setting[] =
      settings.properties[customSetting].properties.components.default;

    return settingComponents;
  }

  /**
   * Traverse the schema recursively
   * @param node - The current node
   * @param pathSegment - The path segment
   * @param paths - The paths to traverse
   * @param quickSettings - The quick settings
   * @returns The updated schema and remaining paths
   */
  private static traverse(
    node: SfoUiJSONSchema7,
    pathSegment: string[],
    paths: string[],
    quickSettings: Setting,
  ) {
    if (node.type !== 'object' || !node.properties) {
      const cleanNode = { ...node };
      delete cleanNode.additionalProperties;
      return { updatedSchema: cleanNode, remainingPaths: paths };
    }

    const updatedProperties: { [key: string]: SfoUiJSONSchema7 } = {};
    const remainingPaths: string[] = paths;

    for (const key in node.properties) {
      const keyPath = pathSegment.concat(key);
      const completePath = keyPath.join('.');

      // If the current property matches one of the paths, traverse its children
      if (paths.some((path) => path.startsWith(completePath))) {
        const { updatedSchema, remainingPaths: updatedPaths } = SfoQuickSettingsService.traverse(
          node.properties[key],
          keyPath,
          paths,
          quickSettings,
        );

        let updatedNode = { ...updatedSchema };
        delete updatedNode.additionalProperties;

        if (updatedNode['examples']) {
          updatedNode = SfoQuickSettingsService.filterExamples(updatedNode);
        }

        const quickSettingProperties = quickSettings.component.options.find(
          (option) => option.path === completePath,
        );

        if (quickSettingProperties) {
          const { path: _path, ...propertiesWithoutPath } = quickSettingProperties;
          updatedProperties[key] = { ...updatedNode, ...propertiesWithoutPath };
        } else {
          updatedProperties[key] = updatedNode;
        }

        let index = updatedPaths.indexOf(completePath);
        while (index !== -1) {
          updatedPaths.splice(index, 1);
          index = updatedPaths.indexOf(completePath);
        }
      }
    }

    const cleanNode = { ...node };
    delete cleanNode.additionalProperties;

    return {
      updatedSchema: { ...cleanNode, properties: updatedProperties },
      remainingPaths,
    };
  }

  /**
   * Filter examples in the schema
   * @param schema - The schema to filter
   * @returns The filtered schema
   */
  private static filterExamples(schema: SfoUiJSONSchema7): SfoUiJSONSchema7 {
    if (schema.type === 'object' && schema.examples) {
      schema.examples = schema.examples.filter((example) => {
        if (typeof example === 'string' || typeof example === 'number') {
          // Filter out non-object examples (e.g., strings or numbers)
          return false;
        } else {
          // Filter out examples with keys not present in schema.properties
          for (const key in example) {
            if (key !== '_comment' && schema.properties && !schema.properties[key]) {
              return false;
            }
          }
          return true;
        }
      }) as Example[]; // Cast the filtered result back to Example[]
    }

    return schema;
  }
}
