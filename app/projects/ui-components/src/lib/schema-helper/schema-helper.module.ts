import {NgModule} from '@angular/core';
import {SfoQuickSettingsService} from './quick-settings.class';

@NgModule({
  declarations: [],
  imports: [],
  exports: [],
  providers: [SfoQuickSettingsService],
})
export class SfoSchemaHelperModule {}
