export function getValueFromObject(path: string, object: unknown) {
  if (!object) {
    return undefined;
  }

  if (!path) {
    throw Error('Path is required');
  }

  if (object[path] != undefined) {
    return object[path];
  }
  const segments = path.split('.');
  if (segments.length === 0) {
    return undefined;
  }
  if (object[segments[0]] === undefined) {
    throw Error('Field not found for: ' + path);
  }
  return getValueFromObject(
    segments.slice(1, segments.length).join('.'),
    object[segments[0]].properties, // modified to look through properties
  );
}

export function patchObject(source: unknown, target: unknown) {
  if (!source) {
    return target;
  }

  if (!target) {
    return {};
  }

  for (const key in source) {
    if (Object.prototype.hasOwnProperty.call(source, key)) {
      if (Object.prototype.hasOwnProperty.call(target?.['properties'], key)) {
        if (typeof source[key] === 'object' && typeof target?.['properties'][key] === 'object') {
          target['properties'][key] = patchObject(source[key], target['properties'][key]);
        } else if (source[key] !== undefined) {
          target['properties'][key]['default'] = source[key];
        }
      }
    }
  }

  return target;
}
