import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormArray, FormBuilder, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {SfoUiJSONSchema7} from '../../metadata.model';
import {DynamicFieldComponent} from './dynamic-field.component';
import {UnsortedKeyvaluePipe} from './unsorted-key-value.pipe';

describe('DynamicFieldComponent', () => {
  let component: DynamicFieldComponent;
  let fixture: ComponentFixture<DynamicFieldComponent>;
  let formBuilder: FormBuilder;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        DynamicFieldComponent,
        UnsortedKeyvaluePipe,
        // DynamicInputComponent // Uncomment to ignore DynamicInput errors
      ],
      imports: [
        NoopAnimationsModule,
        MatExpansionModule,
        MatButtonModule,
        MatIconModule,
        ReactiveFormsModule,
      ],
      providers: [FormBuilder],
    }).compileComponents();

    formBuilder = TestBed.inject(FormBuilder);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFieldComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('GIVEN - default setup - WHEN - instantiating - THEN - it should be create', () => {
    expect(component).toBeTruthy();
  });

  it('GIVEN - valid schema and formArray - WHEN - adding a new row - THEN - it should add a new row to the array control', () => {
    // ARRANGE
    const schema: SfoUiJSONSchema7 = {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          itemField: {type: 'string', title: 'Item Field'} as SfoUiJSONSchema7,
        },
      } as SfoUiJSONSchema7,
    } as SfoUiJSONSchema7;

    const aFormGroup: FormGroup = formBuilder.group({
      testArray: formBuilder.array([]),
    });

    component.schema = schema;
    component.aFormControl = aFormGroup;
    component.control = 'testArray';
    fixture.detectChanges();

    const formArray: FormArray = component.aFormControl.get('testArray') as FormArray;

    // ACT
    component.addRow(formArray, component.schema);
    fixture.detectChanges();

    // ASSERT
    expect(formArray.length).toBe(1);
  });

  it('GIVEN - a valid schema and invalid formArray - WHEN - adding a new row - THEN - it should do nothing', () => {
    // ARRANGE
    const schema: SfoUiJSONSchema7 = {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          itemField: {type: 'string', title: 'Item Field'} as SfoUiJSONSchema7,
        },
      } as SfoUiJSONSchema7,
    } as SfoUiJSONSchema7;

    const aFormGroup: FormGroup = formBuilder.group({
      testArray: formBuilder.array([]),
    });

    component.schema = schema;
    component.aFormControl = aFormGroup;
    component.control = 'testArray';
    fixture.detectChanges();

    const formArray: FormArray = component.aFormControl.get('testArray') as FormArray;

    // ACT
    component.addRow({} as FormArray, component.schema);
    fixture.detectChanges();

    // ASSERT
    expect(formArray.length).toBe(0);
  });

  it('GIVEN - valid schema and formArray - WHEN - removing a row - THEN - it should remove a row from the array control', () => {
    // ARRANGE
    const schema: SfoUiJSONSchema7 = {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          itemField: {type: 'string', title: 'Item Field'} as SfoUiJSONSchema7,
        },
      } as SfoUiJSONSchema7,
    } as SfoUiJSONSchema7;

    const aFormGroup: FormGroup = formBuilder.group({
      testArray: formBuilder.array([
        formBuilder.group({
          itemField: ['value1'],
        }),
        formBuilder.group({
          itemField: ['value2'],
        }),
      ]),
    });

    component.schema = schema;
    component.aFormControl = aFormGroup;
    component.control = 'testArray';

    fixture.detectChanges();

    const testArray = component.aFormControl.get('testArray') as FormArray;

    // ACT
    component.removeRow(0, testArray);
    fixture.detectChanges();

    // ASSERT
    expect(testArray.length).toBe(1);
    expect(testArray.at(0).value).toEqual({itemField: 'value2'});
  });

  it('GIVEN - valid schema and formArray - WHEN - removing a non-existing row - THEN - it should do nothing', () => {
    // ARRANGE
    const schema: SfoUiJSONSchema7 = {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          itemField: {type: 'string', title: 'Item Field'} as SfoUiJSONSchema7,
        },
      } as SfoUiJSONSchema7,
    } as SfoUiJSONSchema7;

    const aFormGroup: FormGroup = formBuilder.group({
      testArray: formBuilder.array([
        formBuilder.group({
          itemField: ['value1'],
        }),
        formBuilder.group({
          itemField: ['value2'],
        }),
      ]),
    });

    component.schema = schema;
    component.aFormControl = aFormGroup;
    component.control = 'testArray';

    fixture.detectChanges();

    const testArray = component.aFormControl.get('testArray') as FormArray;

    // ACT
    component.removeRow(999, testArray);
    fixture.detectChanges();

    // ASSERT
    expect(testArray.length).toBe(2);
    expect(testArray.at(0).value).toEqual({itemField: 'value1'});
  });
});
