/**
 * Settings view type that displays high-level children wrapped in collapsible panels in a single column layout.
 */
type QuickSettingsView = 'quickSettings';

/**
 * Settings view type that displays settings in a single column layout with maximum width.
 */
type SimpleSettingsView = 'simple';

/**
 * Settings view type that displays settings with vertical navigation using a drawer.
 */
type DefaultSettingsView = 'default';

/**
 * Settings view type that displays settings with horizontal navigation using tabs.
 */
type TabSettingsView = 'tabbed';

/**
 * The view type for the settings. This determines the layout or mode in which settings are displayed.
 * - `default` - Vertical navigation using drawer
 * - `tabbed` - Horizontal navigation using tabs
 * - `simple` - Single column, max width
 * - `quickSettings` - Single column, high level children are wrapped in panels
 * @default 'default'
 */
export type RenderMode =
  | QuickSettingsView
  | SimpleSettingsView
  | DefaultSettingsView
  | TabSettingsView;
