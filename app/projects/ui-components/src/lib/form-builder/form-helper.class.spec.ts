import { SfoFormHelperService } from './form-helper.class';

describe('SfoFormHelperService', () => {
  describe('removeNullValues', () => {
    it('should remove null, undefined, and empty string values from an object', () => {
      const input = {
        key1: 'value1',
        key2: null,
        key3: undefined,
        key4: '',
        key5: 'value5',
      };
      const expectedOutput = {
        key1: 'value1',
        key5: 'value5',
      };
      expect(SfoFormHelperService.removeNullValues(input)).toEqual(expectedOutput);
    });

    it('should remove null, undefined, and empty string values from an array', () => {
      const input = [null, 'value', undefined, '', 'value2'];
      const expectedOutput = ['value', '', 'value2'];
      expect(SfoFormHelperService.removeNullValues(input)).toEqual(expectedOutput);
    });

    it('should remove nested null, undefined, and empty string values from an object', () => {
      const input = {
        key1: 'value1',
        key2: {
          nestedKey1: null,
          nestedKey2: 'value2',
          nestedKey3: '',
        },
        key3: undefined,
      };
      const expectedOutput = {
        key1: 'value1',
        key2: {
          nestedKey2: 'value2',
        },
      };
      expect(SfoFormHelperService.removeNullValues(input)).toEqual(expectedOutput);
    });

    it('should return undefined if the entire object is cleaned out', () => {
      const input = {
        key1: null,
        key2: undefined,
        key3: '',
      };
      expect(SfoFormHelperService.removeNullValues(input)).toBe(undefined);
    });

    it('should remove null, undefined values but keep empty strings from an array', () => {
      const input = [null, 'value', undefined, '', 'value2'];
      const expectedOutput = ['value', '', 'value2'];
      expect(SfoFormHelperService.removeNullValues(input)).toEqual(expectedOutput);
    });

    it('should preserve empty strings when schema specifies minLength: 0', () => {
      const input = {
        key1: 'value1',
        key2: '',
        key3: '',
        key4: null,
      };
      const schema = {
        properties: {
          key1: { type: 'string' },
          key2: { type: 'string', minLength: 0 },
          key3: { type: 'string', minLength: 1 },
          key4: { type: 'string' },
        },
      };
      const expectedOutput = {
        key1: 'value1',
        key2: '',
      };
      expect(SfoFormHelperService.removeNullValues(input, schema)).toEqual(expectedOutput);
    });

    it('should handle nested schemas correctly when preserving empty strings', () => {
      const input = {
        user: {
          name: 'John',
          bio: '',
          notes: '',
        },
        settings: {
          theme: '',
          notifications: true,
        },
      };
      const schema = {
        properties: {
          user: {
            type: 'object',
            properties: {
              name: { type: 'string' },
              bio: { type: 'string', minLength: 0 },
              notes: { type: 'string' },
            },
          },
          settings: {
            type: 'object',
            properties: {
              theme: { type: 'string', minLength: 0 },
              notifications: { type: 'boolean' },
            },
          },
        },
      };
      const expectedOutput = {
        user: {
          name: 'John',
          bio: '',
        },
        settings: {
          theme: '',
          notifications: true,
        },
      };
      expect(SfoFormHelperService.removeNullValues(input, schema)).toEqual(expectedOutput);
    });

    it('should handle nested objects and remove empty objects', () => {
      const input = {
        key1: 'value1',
        key2: {
          nestedKey1: null,
          nestedKey2: 'value2',
          nestedKey3: {
            deepNested: null,
            emptyObj: {},
          },
        },
        key3: {},
        key4: { allEmpty: null },
      };
      const expectedOutput = {
        key1: 'value1',
        key2: {
          nestedKey2: 'value2',
        },
      };
      expect(SfoFormHelperService.removeNullValues(input)).toEqual(expectedOutput);
    });

    it('should handle arrays with nested objects correctly', () => {
      const input = {
        items: [
          { id: 1, name: 'Item 1', desc: '' },
          { id: 2, name: '', desc: null },
          { id: null, name: null, desc: null },
        ],
      };
      const schema = {
        properties: {
          items: {
            type: 'array',
            items: {
              type: 'object',
              properties: {
                id: { type: 'number' },
                name: { type: 'string', minLength: 0 },
                desc: { type: 'string' },
              },
            },
          },
        },
      };
      const expectedOutput = {
        items: [{ id: 1, name: 'Item 1' }, { id: 2 }],
      };
      expect(SfoFormHelperService.removeNullValues(input, schema)).toEqual(expectedOutput);
    });
  });

  describe('removeDefaults', () => {
    it('should remove properties that match the default values from an object', () => {
      const instance = {
        key1: 'value1',
        key2: 'default',
        key3: 'value3',
      };
      const schema = {
        properties: {
          key1: { type: 'string' },
          key2: { type: 'string', default: 'default' },
          key3: { type: 'string', default: 'default3' },
        },
      };
      const expectedOutput = {
        key1: 'value1',
        key3: 'value3',
      };
      expect(SfoFormHelperService.removeDefaults(instance, schema)).toEqual(expectedOutput);
    });

    it('should remove nested properties that match the default values from an object', () => {
      const instance = {
        key1: 'value1',
        key2: {
          nestedKey1: 'nestedDefault',
          nestedKey2: 'nestedValue2',
        },
        key3: 'value3',
      };
      const schema = {
        properties: {
          key1: { type: 'string' },
          key2: {
            type: 'object',
            properties: {
              nestedKey1: { type: 'string', default: 'nestedDefault' },
              nestedKey2: { type: 'string' },
            },
          },
          key3: { type: 'string', default: 'default3' },
        },
      };
      const expectedOutput = {
        key1: 'value1',
        key2: {
          nestedKey2: 'nestedValue2',
        },
        key3: 'value3',
      };
      expect(SfoFormHelperService.removeDefaults(instance, schema)).toEqual(expectedOutput);
    });

    it('should remove array items that match the default array values', () => {
      const instance = {
        key1: 'value1',
        key2: ['item1', 'item2'],
      };
      const schema = {
        properties: {
          key1: { type: 'string' },
          key2: { type: 'array', default: ['item1', 'item2'] },
        },
      };
      const expectedOutput = {
        key1: 'value1',
      };
      expect(SfoFormHelperService.removeDefaults(instance, schema)).toEqual(expectedOutput);
    });

    it('should return the instance unchanged if no defaults are matched', () => {
      const instance = {
        key1: 'value1',
        key2: 'nonDefault',
      };
      const schema = {
        properties: {
          key1: { type: 'string' },
          key2: { type: 'string', default: 'default' },
        },
      };
      expect(SfoFormHelperService.removeDefaults(instance, schema)).toEqual(instance);
    });

    it('should return an empty object if all values match the defaults', () => {
      const instance = {
        key1: 'default',
        key2: 'default2',
      };
      const schema = {
        properties: {
          key1: { type: 'string', default: 'default' },
          key2: { type: 'string', default: 'default2' },
        },
      };
      expect(SfoFormHelperService.removeDefaults(instance, schema)).toEqual({});
    });
  });
});
