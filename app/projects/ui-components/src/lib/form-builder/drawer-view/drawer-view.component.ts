import {
  Component,
  ComponentRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatSelectionList} from '@angular/material/list';
import {BehaviorSubject, Subscription} from 'rxjs';
import {SfoUiJSONSchema7} from '../../metadata.model';
import {FormService} from '../form.service';
import {DrawerViewContentComponent} from './drawer-view-content/drawer-view-content.component';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {map} from 'rxjs/operators';

interface Tab {
  id: string;
  label: string;
  description?: string;
  icon?: string;
  tag?: string;
}

@Component({
  selector: 'sfo-drawer-view',
  templateUrl: './drawer-view.component.html',
  styleUrls: ['./drawer-view.component.scss'],
  standalone: false,
})
export class DrawerViewComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription = new Subscription();
  private currentComponent?: ComponentRef<DrawerViewContentComponent>;
  private lastRenderedControl: string | null = null;

  schema$ = new BehaviorSubject<SfoUiJSONSchema7 | null>(null);
  isSmallScreen$ = this.breakpointObserver
    .observe(['(max-width: 52em)'])
    .pipe(map((result) => result.matches));

  @ViewChild('vcr', {static: true, read: ViewContainerRef}) vcr!: ViewContainerRef;
  @ViewChild('matSelectionList') matSelectionList: MatSelectionList;

  @Input() aFormControl: FormGroup;
  @Input() set schema(data: SfoUiJSONSchema7 | null) {
    this.schema$.next(data);
  }

  activeTab: string = '';
  tabs: Tab[] = [];
  advancedMode$ = this.formService.advancedMode$;

  constructor(
    private formService: FormService,
    private breakpointObserver: BreakpointObserver,
  ) {}

  ngOnInit(): void {
    this.subscriptions.add(
      this.schema$.subscribe(() => {
        this.initializeTabs();
        this.renderDynamicComponent('');
      }),
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
    this.reset();
    this.schema$.complete();
  }

  private initializeTabs(): void {
    this.reset();
    this.tabs = [];
    this.activeTab = '';
    this.lastRenderedControl = null;

    if (this.matSelectionList) {
      this.matSelectionList.deselectAll();
    }

    const schema = this.schema$.getValue();
    if (!schema || !schema.properties) {
      return;
    }

    this.tabs = Object.entries(schema.properties).map(([key, value]) => ({
      label: value?.title ?? key,
      icon: value['_ui_icon'],
      id: key,
      tag: value['_ui_tag'],
      description: value?.description,
    }));
  }

  selectConfiguration(control: string): void {
    if (this.activeTab === control) {
      return;
    }

    this.activeTab = control;
    this.renderDynamicComponent(control);
  }

  private renderDynamicComponent(control: string): void {
    const schema = this.schema$.getValue();

    if (!schema || this.lastRenderedControl === control) {
      return;
    }

    this.destroyCurrentComponent();

    this.currentComponent = this.vcr.createComponent(DrawerViewContentComponent, {
      projectableNodes: [[document.createTextNode('')]],
    });
    const instance = this.currentComponent.instance;
    instance.schema = schema;
    instance.aFormControl = this.aFormControl;
    instance.control = control;

    this.lastRenderedControl = control;
    this.currentComponent.changeDetectorRef.detectChanges();
  }

  private destroyCurrentComponent(): void {
    if (this.currentComponent) {
      this.currentComponent.destroy();
      this.currentComponent = undefined;
      this.lastRenderedControl = null;
    }
  }

  private reset(): void {
    this.destroyCurrentComponent();
    if (this.vcr) {
      this.vcr.clear();
    }
  }

  trackByLabel(_: number, tab: Tab): string {
    return tab.id;
  }

  toggleAdvanceMode(): void {
    this.formService.toggleAdvancedMode();
  }
}
