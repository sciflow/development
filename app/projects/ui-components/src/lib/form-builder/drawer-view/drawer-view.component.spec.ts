import {ChangeDetectorRef} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {MatListModule} from '@angular/material/list';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {By} from '@angular/platform-browser';

import {SfoUiJSONSchema7} from '../../metadata.model';
import {DrawerViewContentComponent} from './drawer-view-content/drawer-view-content.component';
import {DrawerViewComponent} from './drawer-view.component';

describe('SettingsViewComponent', () => {
  let component: DrawerViewComponent;
  let fixture: ComponentFixture<DrawerViewComponent>;
  let cdr: ChangeDetectorRef;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DrawerViewComponent, DrawerViewContentComponent],
      imports: [
        ReactiveFormsModule,
        MatListModule,
        MatIconModule,
        NoopAnimationsModule,
        MatSidenavModule,
        // DynamicFieldModule, // ignore import to stop DynamicField component errors
      ],
      providers: [ChangeDetectorRef],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawerViewComponent);
    component = fixture.componentInstance;
    cdr = fixture.debugElement.injector.get(ChangeDetectorRef);
    fixture.detectChanges();
  });

  it('GIVEN - default setup -  WHEN - instantiating - THEN - it should be created', () => {
    expect(component).toBeTruthy();
  });

  it('GIVEN - default setup - WHEN - instantiating - THEN - it should initialize with empty schema and tabs', () => {
    // ARRANGE
    // ACT
    // ASSERT
    expect(component.schema$.getValue()).toBeNull();
    expect(component.tabs.length).toBe(0);
  });

  it('GIVEN - a schema - WHEN - rendering - THEN - it should update tabs when schema is set', () => {
    // ARRANGE
    const schema: SfoUiJSONSchema7 = {
      title: 'Test Schema',
      type: 'object',
      properties: {
        Page: {type: 'string', title: 'Page'} as SfoUiJSONSchema7,
        Typography: {type: 'string', title: 'Typography'} as SfoUiJSONSchema7,
      },
    };

    // ACT
    component.schema = schema;
    fixture.detectChanges();

    // ASSERT
    expect(component.tabs.length).toBe(2);
    expect(component.tabs[0].label).toBe('Page');
    expect(component.tabs[1].label).toBe('Typography');
  });

  it('GIVEN - a configuraiton - WHEN - selecting the configuration - THEN - it should render dynamic component on tab selection', () => {
    // ARRANGE
    const schema: SfoUiJSONSchema7 = {
      title: 'Test Schema',
      type: 'object',
      properties: {
        Page: {type: 'string', title: 'Page'} as SfoUiJSONSchema7,
      },
    };

    component.schema = schema;
    fixture.detectChanges();
    component.aFormControl = new FormGroup({
      Page: new FormControl(''),
    });

    // ACT
    component.selectConfiguration('Page');
    fixture.detectChanges();

    // ASSERT
    expect(component.activeTab).toBe('Page');
    const dynamicComponent = fixture.debugElement.query(By.directive(DrawerViewContentComponent));
    expect(dynamicComponent).toBeTruthy();
  });

  it('GIVEN - SettignsViewEditorComponent instance - WHEN - destroying this instance - THEN - it should clear dynamic component views', () => {
    // ARRANGE
    component.vcr.createComponent(DrawerViewContentComponent);

    // ACT
    component.ngOnDestroy();
    fixture.detectChanges();

    // ASSERT
    expect(component.vcr.length).toBe(0);
  });
});
