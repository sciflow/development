import {ExampleTypes} from '../../metadata.model';

export interface MenuExample {
  data: ExampleTypes;
  isActive: boolean;
}

export interface MenuItem {
  path: string;
  examples: MenuExample[];
}
