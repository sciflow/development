import {ComponentFixture, TestBed, fakeAsync, tick} from '@angular/core/testing';
import {FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';

import {PresetMenuComponent} from './preset-menu.component';
import {SfoUiJSONSchema7} from '../../metadata.model';
describe('PresetMenuComponent', () => {
  let component: PresetMenuComponent;
  let fixture: ComponentFixture<PresetMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [PresetMenuComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PresetMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('GIVEN - default setup - WHEN - instantiating - THEN - it should be created', () => {
    expect(component).toBeTruthy();
  });

  it('GIVEN - a message - WHEN - setPresetMesage is called - THEN - it should correctly set preset message', fakeAsync(() => {
    // ARRANGE
    const message = 'Test message';

    // ACTION
    component.setPresetMessage(message);

    // ASSERT
    expect(component.presetMessage).toEqual(`Active: ${message}`);

    // Clean up
    clearInterval(component['timeoutInterval']);
  }));

  it('GIVEN - a message - WHEN - setPresetMesage is called - THEN - it should correctly set preset message clear the message after 3 seconds', fakeAsync(() => {
    // ARRANGE
    const message = 'Test message';
    const duration = 3000;

    // ACTION
    component.setPresetMessage(message);
    tick(duration + 1);
    fixture.detectChanges();

    // ASSERT
    expect(component.presetMessage).toEqual('');

    // Clean up
    clearInterval(component['timeoutInterval']);
  }));

  describe('WITH - a schema, form and propertyName', () => {
    const pageSubProperty: string = 'page';

    const mockSchema: SfoUiJSONSchema7 = {
      'type': 'object',
      'properties': {
        'size': {
          'title': 'Size',
          'description': 'The page size.',
          'examples': ['A4', 'A5', '17cm 24cm'],
          'default': 'A4',
          'type': 'string',
        } as SfoUiJSONSchema7,
      },
    };

    const form: FormGroup = new FormGroup({
      size: new FormControl(undefined),
    });

    it('GIVEN - initial condition - WHEN - initializing - THEN - it should initialize menuItems', () => {
      // ARRANGE

      component.schema = mockSchema;
      component.propertyName = pageSubProperty;
      component.aFormControl = form;

      const expected = [
        {
          'path': 'page.size',
          'examples': [
            {
              'data': 'A4',
              'isActive': false,
            },
            {
              'data': 'A5',
              'isActive': false,
            },
            {
              'data': '17cm 24cm',
              'isActive': false,
            },
          ],
        },
      ];

      // ACTION
      fixture.detectChanges();
      component.ngOnInit();

      // ASSERT
      const menuItemElement = fixture.nativeElement.querySelector(
        '[data-test="presetmMenu.menuItems"]',
      );

      expect(component.menuItems).toEqual(expected);
      expect(menuItemElement).not.toBe(undefined);
    });
  });

  describe('WITH - an invalid schema, form and propertyName', () => {
    const pageSubProperty: string = 'page';

    const mockSchema: SfoUiJSONSchema7 = {
      'type': 'object',
      'properties': {
        'size': {
          'title': 'Size',
          'description': 'The page size.',
          'default': 'A4',
          'type': 'string',
        } as SfoUiJSONSchema7,
      },
    };

    const form: FormGroup = new FormGroup({
      size: new FormControl(undefined),
    });

    it('GIVEN - initial condition - WHEN - initializing - THEN - it should initialize menuItems', () => {
      // ARRANGE

      component.schema = mockSchema;
      component.propertyName = pageSubProperty;
      component.aFormControl = form;

      const expected = [];

      // ACTION
      fixture.detectChanges();
      component.ngOnInit();

      // ASSERT
      const menuItemElement = fixture.nativeElement.querySelector(
        '[data-test="presetmMenu.menuItems"]',
      );

      expect(component.menuItems).toEqual(expected);
      expect(menuItemElement).not.toBe(undefined);
    });
  });
});
