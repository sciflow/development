import {TestBed} from '@angular/core/testing';
import {FormArray, FormBuilder, FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {FormService} from './form.service'; // Update the path as needed
import {SfoUiJSONSchema7} from '../metadata.model';

describe('FormService', () => {
  let service: FormService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      providers: [FormBuilder, FormService],
    });

    service = TestBed.inject(FormService);
  });

  it('should generate a form group for a simple schema with basic types', () => {
    // ARRANGE
    const schema: SfoUiJSONSchema7 = {
      type: 'object',
      properties: {
        firstName: {type: 'string'},
        age: {type: 'number'},
        isActive: {type: 'boolean'},
      },
    };

    // ACT
    const formGroup = service.generateForm(schema);

    // ASSERT
    expect(formGroup instanceof FormGroup).toBe(true);
    expect(formGroup.contains('firstName')).toBe(true);
    expect(formGroup.contains('age')).toBe(true);
    expect(formGroup.contains('isActive')).toBe(true);
  });

  it('should generate a form group with nested objects', () => {
    // ARRANGE
    const schema: SfoUiJSONSchema7 = {
      type: 'object',
      properties: {
        address: {
          type: 'object',
          properties: {
            street: {type: 'string'},
            city: {type: 'string'},
          },
        },
      },
    };

    // ACT
    const formGroup = service.generateForm(schema);

    // ASSERT
    expect(formGroup instanceof FormGroup).toBe(true);
  });

  it('should handle an array of strings correctly', () => {
    // ARRANGE
    const schema: SfoUiJSONSchema7 = {
      type: 'object',
      properties: {
        tags: {
          type: 'array',
          items: {type: 'string'},
        },
      },
    };

    // ACT
    const formGroup = service.generateForm(schema);

    // ASSERT
    expect(formGroup instanceof FormGroup).toBe(true);

    const tagsArray = formGroup.get('tags') as FormArray;
    expect(tagsArray instanceof FormArray).toBe(true);
    expect(tagsArray.length).toBe(0); // No default values, so array should be empty

    // Adding a value to check if form structure is correct
    tagsArray.push(new FormControl('test-tag')); // Directly create a new FormControl
    expect(tagsArray.length).toBe(1);
    expect(tagsArray.at(0).value).toBe('test-tag');
  });

  it('should handle default values correctly', () => {
    // ARRANGE
    const schema: SfoUiJSONSchema7 = {
      type: 'object',
      properties: {
        firstName: {type: 'string', default: 'John'},
        age: {type: 'number', default: 30},
      },
    };

    // ACT
    const formGroup = service.generateForm(schema);

    // ASSERT
    expect(formGroup instanceof FormGroup).toBe(true);
    expect(formGroup.get('firstName')?.value).toBe(null);
    expect(formGroup.get('age')?.value).toBe(null);
  });

  it('should handle an array of nested objects', () => {
    // ARRANGE
    const schema: SfoUiJSONSchema7 = {
      type: 'object',
      properties: {
        contacts: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              type: {type: 'string'},
              number: {type: 'string'},
            },
          },
        },
      },
    };

    // ACT
    const formGroup = service.generateForm(schema);

    // ASSERT
    expect(formGroup instanceof FormGroup).toBe(true);

    const contactsArray = formGroup.get('contacts') as FormArray;
    expect(contactsArray instanceof FormArray).toBe(true);

    // Adding a contact manually
    contactsArray.push(
      service.buildFormGroup({
        type: 'object',
        properties: {
          type: {type: 'string'},
          number: {type: 'string'},
        },
      }),
    );

    const newContact = contactsArray.at(0) as FormGroup;
    expect(newContact.get('type')).toBeTruthy();
    expect(newContact.get('number')).toBeTruthy();
  });
});
