import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormGroup, ReactiveFormsModule} from '@angular/forms';
import {DynamicFormComponent} from './dynamic-form.component';
import {FormService} from './form.service';
import {SfoUiJSONSchema7} from '../metadata.model';
import {DynamicFieldComponent} from './form-fields';

describe('DynamicFormComponent', () => {
  let component: DynamicFormComponent;
  let fixture: ComponentFixture<DynamicFormComponent>;
  let formService: FormService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DynamicFormComponent, DynamicFieldComponent],
      imports: [ReactiveFormsModule],
      providers: [FormService],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFormComponent);
    component = fixture.componentInstance;
    formService = TestBed.inject(FormService);
    fixture.detectChanges();
  });

  it('GIVEN -  no schema and default provider - WHEN - instantiating - THEN - it should be created', () => {
    expect(component).toBeTruthy();
  });

  it('GIVEN - a schema - WHEN instantiating - THEN - it should create form on init', () => {
    // ARRANGE
    const jsonSchema: SfoUiJSONSchema7 = {} as SfoUiJSONSchema7;
    spyOn(formService, 'generateForm').and.returnValue(new FormGroup({}));
    component.jsonSchema = jsonSchema;

    // ACT
    component.ngOnInit();

    // ASSERT
    expect(formService.generateForm).toHaveBeenCalledWith(jsonSchema);
    expect(component.parentForm).toBeTruthy();
  });
});
