import { createAction, props } from '@ngrx/store';

export const updatePart = createAction('[Outline] Updating a part', props<{ part: any[]; }>());
