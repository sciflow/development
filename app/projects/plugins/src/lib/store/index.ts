import { Action, ActionReducer, createReducer, createSelector, on } from '@ngrx/store';

import { createAction, props } from '@ngrx/store';

export const setPlugins = createAction('[Plugins] Setting plugins', props<{ plugins: any[]; }>());
export const addActions = createAction('[Plugins] Adding actions', props<{ actions: PluginAction[]; }>());
export const updateActions = createAction('[Plugins] Updating action', props<{ actions: PluginAction[]; clear?: boolean; }>());
export const deleteActions = createAction('[Plugins] Updating action', props<{ matcher: (action: PluginAction[]) => boolean }>());

export const setPluginState = createAction('[Plugins] Setting plugin state', props<{ pluginId: string; state: any; }>());
export const addLogs = createAction('[Plugins] Adding logs', props<{ logs: any[]; }>());

export const pluginsFeatureKey = 'plugins';

export interface PluginAction {
    /** A deterministic unique (functional) id for the action */
    id: string;
    pluginId?: string;
    name: string;
    nodeId?: string;
    tag?: any;
    /** A JSON schema for any needed input data */
    schema?: any;
    blocking?: boolean;
    [key: string]: any;
}

export interface Plugin<PluginState> {
    id: string;
    name: string;
    title: string;
    description?: string,
    triggers: { event: string; }[];
    runners: 'browser' | 'server';
    scripts: { name: 'patch' | 'tag'; url: string; }[];
    /** Plugin state */
    state: PluginState;
}

export interface Plugins {
    plugins: Plugin<any>[];
    logs: any[];
    actions: PluginAction[];
}

const initialState: Plugins = {
    plugins: [],
    logs: [],
    actions: []
};

export const pluginsReducer = createReducer(
    initialState,
    on(setPlugins, (state, action) => ({
        ...state,
        plugins: action.plugins
    })),
    on(setPluginState, (state, action) => ({
        ...state, plugins: [...state.plugins.map(p => {
            if (p.id !== action.pluginId) { return p; }
            return { ...p, state: action.state };
        }),]
    })),
    on(addActions, (state, action) => ({
        ...state,
        actions: [...state.actions || [], ...action.actions]
    })),
    on(updateActions, (state, action) => action.clear ? ({ ...state, actions: action.actions }) : ({
        ...state,
        actions: state.actions.map(a => action.actions.find(b => a.id === b.id) || a)
    })),
    on(addLogs, (state, action) => ({
        ...state,
        logs: [...state.logs || [], ...action.logs]
    }))
);

export const selectPluginsState = (state): Plugins => state.plugins;

/**
 * Selects the plugins
 */
export const selectPlugins = createSelector(
    selectPluginsState,
    (state: Plugins) => state.plugins
);

/**
 * Selects the plugins
 */
export const selectPluginById = (pluginId: string) => createSelector(
    selectPluginsState,
    (state: Plugins) => state.plugins.find(p => p.id === pluginId)
);

/**
 * Selects the actions
 */
export const selectPluginActions = createSelector(
    selectPluginsState,
    (state: Plugins) => state.actions
);

/**
 * Selects the logs
 */
export const selectLogs = createSelector(
    selectPluginsState,
    (state: Plugins) => state.logs
);

export const pluginsMetadataReducer: ActionReducer<Plugins, any> = (state: Plugins | undefined, action: Action) => {
    return pluginsReducer(state, action);
};
