/*
 * Public API Surface of plugins
 */

export * from './lib/plugins.service';
export * from './lib/plugins.component';
