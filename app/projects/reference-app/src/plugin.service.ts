import { Injectable } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
import { firstValueFrom } from 'rxjs';

async function loadAndExecuteModule(moduleUrl) {
  try {
    return await import(/* webpackIgnore: true */ moduleUrl);
  } catch (error) {
    console.error('Error loading module:', error);
  }
}

@Injectable({
  providedIn: 'root'
})
export class PluginService {

  constructor(private apollo: Apollo) { }

  async loadPlugins(onlyRequired = true) {
    const result: any = await firstValueFrom(this.apollo.query({
      query: gql`{
          plugins {
          id
          name
          title
          description
          runners
          triggers {
            event
          }
          scripts {
            name
            url
          }
        }
    }`
    }));

    return result?.data?.plugins?.filter(p => !onlyRequired || p.id === 'process-assets');
  }

  async loadPlugin(plugin: any) {
    const scripts = await Promise.all(
      plugin.scripts.map(async (script) => ({
        ...script,
        content: await loadAndExecuteModule(script.url),
      })),
    );

    return {
      ...plugin,
      scripts,
    };
  }
}
