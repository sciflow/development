import { createAction, props } from '@ngrx/store';
import { OptionState } from 'projects/author-management/src/lib/author-management.reducer';

/**
 * Sets the state to maximized
 */
export const maximized = createAction('[Author Management] Maximized');

 /**
  * Sets the state to minimized
  */
export const minimized = createAction('[Author Management] Minimized');

/**
 * Initialize Users.
 */
export const initializeUsers = createAction('[User] Initialize', props<{ user: OptionState }>());
