import { Action, ActionReducer, createReducer, createSelector, on } from '@ngrx/store';
import * as userActions from './userAction.actions';

export const editorFeatureKey = 'store';

export interface UserActionState {
    uploadState: boolean;
    project;
    file: object;
    filename: string;
    openProject: boolean;
    template: string;
}

const initialState: UserActionState = {
    uploadState: false,
    project: undefined,
    file: {},
    filename: '',
    openProject: false,
    template: ''
};

export const appStateReducer = createReducer(
    initialState,
    on(userActions.openProjectDca, (state, action) => ({ ...state, openProject: action.openProject })),
    on(userActions.setTemplate, (state, action) => ({ ...state, template: action.template }))
);

export const doucumentUploadState = (state): UserActionState => state.store;

export const FileUpladState = createSelector(
    doucumentUploadState,
    (state: UserActionState) => state.uploadState
);

export const docfile = createSelector(
    doucumentUploadState,
    (state: UserActionState) => state.file
);

export const docfilename = createSelector(
    doucumentUploadState,
    (state: UserActionState) => state.filename
);

export const selectOpenProjectDca = createSelector(
    doucumentUploadState,
    (state: UserActionState) => state.openProject
);

export const selectTemplate = createSelector(
    doucumentUploadState,
    (state: UserActionState) => state?.template
);

export const editorReducer: ActionReducer<UserActionState, any> = (state: UserActionState | undefined, action: Action) => {
    return appStateReducer(state, action);
};
