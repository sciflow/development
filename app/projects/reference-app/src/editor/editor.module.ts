import { CommonModule } from '@angular/common';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';
import { EditorModule as SfoEditorModule } from 'editor';
import { EditorComponent } from './editor/editor.component';

import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from 'shared';
import { DeactivateGuard } from './editor/deactivate-guard';
import { StartComponent } from './editor/start/start.component';
import { ErrorDialogComponent } from './error-handler/error-dialog/error-dialog.component';
import { ErrorDialogService } from './error-handler/error-dialog/error-dialog.service';
import { PageNotFoundComponent } from './error-handler/page-not-found/page-not-found.component';
import { PreviewActionsCenterComponent } from './preview/preview-actions-center/preview-actions-center.component';
import { PreviewAssestsComponent } from './preview/preview-assets/preview-assets.component';
import { PreviewComponent } from './preview/preview.component';
import { ProjectComponent } from './project/project.component';
import { projectFeatureKey, reducer } from './project/project.reducer';
import { ProcessService } from './shared/services/process.service';
import { AssetPlugin } from './shared/services/processes/asset-plugin';
import { simpleModeGuard } from './simple-mode.guard';
import { editorFeatureKey, editorReducer } from './userAction.reducer';
import { pluginsFeatureKey, pluginsMetadataReducer } from 'projects/plugins/src/lib/store';
import { pluginsGuard } from '../plugins.guard';
import { SfoAdmonitionModule } from 'projects/ui-components/src';

@NgModule({
  declarations: [
    EditorComponent,
    ProjectComponent,
    StartComponent,
    ErrorDialogComponent,
    PageNotFoundComponent,
    PreviewComponent,
    PreviewAssestsComponent,
    PreviewActionsCenterComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MatButtonModule,
    MatDividerModule,
    MatExpansionModule,
    MatIconModule,
    MatSidenavModule,
    MatToolbarModule,
    OverlayModule,
    MatSlideToggleModule,
    MatListModule,
    MatGridListModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatCardModule,
    PortalModule,
    MatTableModule,
    MatButtonToggleModule,
    MatTooltipModule,
    MatToolbarModule,
    MatListModule,
    MatProgressBarModule,
    SfoEditorModule,
    SfoAdmonitionModule,
    MatDialogModule,
    MatListModule,
    StoreModule.forFeature(pluginsFeatureKey, pluginsMetadataReducer),
    StoreModule.forFeature(projectFeatureKey, reducer),
    StoreModule.forFeature(editorFeatureKey, editorReducer),
    RouterModule.forChild([
      {
        path: '',
        redirectTo: 'start',
        pathMatch: 'full',
      },
      {
        path: 'start',
        component: StartComponent,
      },
      {
        path: 'start-simple',
        component: StartComponent,
        canActivate: [simpleModeGuard],
      },
      {
        path: 'start/:projectId',
        component: StartComponent,
      },
      {
        path: 'preview/:projectId/:documentName',
        component: PreviewComponent,
        canActivate: [pluginsGuard]
      },
      {
        path: 'preview/:projectId/:documentName',
        component: PreviewComponent,
        canActivate: [pluginsGuard]
      },
      {
        path: 'edit/:projectId/:documentName/:version',
        component: EditorComponent,
        canDeactivate: [DeactivateGuard],
        children: [
          { path: 'project', outlet: 'context', component: ProjectComponent },
          {
            path: 'authors',
            outlet: 'context',
            loadChildren: () =>
              import('projects/author-management/src/public-api').then(
                (m) => m.AuthorManagementModule,
              ),
          },
          {
            path: 'references',
            outlet: 'context',
            loadChildren: () =>
              import('projects/reference-management/src/projects').then(
                (m) => m.ReferenceManagementModule,
              ),
          },
          {
            path: 'outline',
            outlet: 'context',
            loadChildren: () =>
              import('projects/document-outline/src/public-api').then(
                (m) => m.DocumentOutlineModule,
              ),
          },
          {
            path: 'export',
            outlet: 'context',
            loadChildren: () =>
              import('projects/export/src/public-api').then((m) => m.ExportModule),
          },
        ],
      },
    ]),
  ],
  providers: [
    DeactivateGuard,
    {
      provide: MatDialogRef,
      useValue: {},
    },
    ErrorDialogService,
  ],
  exports: [EditorComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class EditorModule {
  constructor(private processService: ProcessService) {
    const assetProcess = new AssetPlugin();
    this.processService.addPlugin(assetProcess);
  }
}
