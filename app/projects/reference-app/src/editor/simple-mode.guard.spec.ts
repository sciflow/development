import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { simpleModeGuard } from './simple-mode.guard';

describe('simpleModeGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => simpleModeGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
