import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import generate from 'boring-name-generator';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import { FileService } from 'shared';
import { v4 as uuidv4 } from 'uuid';
import { ProjectService } from '../../shared/services/project.service';
import JSZip from 'jszip';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss'],
  standalone: false
})
export class StartComponent implements OnInit {
  @ViewChild('fileUpload', { static: true }) fileUpload;

  format = 'docx';

  instanceData = JSON.parse(this.document.getElementById('instance-data').innerHTML);
  projectId$ = this.route.params.pipe(map(({ projectId }) => projectId));
  project$ = this.projectId$.pipe(
    filter((projectId) => projectId?.length > 0),
    switchMap((projectId) =>
      this.projectService.loadFiles(projectId).valueChanges.pipe(
        map((result) => {
          const project = result.data?.project;
          return {
            ...project,
            sizeInMb: Math.round((project.size / 1024 / 1024) * 100) / 100,
            resources: project.resources
              .map((r) => {

                const split = r.Key.split('.');
                const name = split[0]?.replace(projectId + '/', '');
                let ext = split.slice(1, split.length).join('.');

                let type;
                switch (ext) {
                  case 'docx':
                    type = 'word';
                    break;
                  case 'docx.json':
                    type = 'intelligence';
                    // intelligence file
                    break;
                  case 'pandoc.json':
                    type = 'pandoc';
                    // pandoc ast
                    break;
                  /* case '.csl.json':
                    type = 'references';
                    break; */
                  case 'json':
                    type = 'manuscript';
                    // saved manuscript
                    break;
                  default:
                    type = 'unknown';
                    break;
                }

                const prepped = project.resources.some(r2 => r2.Key === r.Key + '.json');
                console.log(type, name, ext);
                return {
                  ...r,
                  type,
                  name,
                  prepped,
                  latestVersion: r.versions?.[0] || { LastModified: r.LastModified },
                };
              })
              .filter((r) => ['word', 'manuscript'].includes(r.type))
              .sort((a, b) => b.LastModified - a.LastModified),
          };
        }), tap(console.log)
      ),
    ),
  );

  constructor(
    private router: Router,
    private fileService: FileService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private store: Store,
    private projectService: ProjectService,
    @Inject(DOCUMENT) private document: any,
  ) { }

  ngOnInit(): void { }

  openResource(resource) {
    const version = resource.latestVersion;
    const project = resource.Key.split('/')[0];
    const subPath = encodeURIComponent(resource.Key.replace(project + '/', ''));
    if (resource.Key.indexOf('.json') === resource.Key.length - 5) {
      this.router.navigateByUrl(`/edit/${project}/${subPath}/${version?.VersionId}`);
    } else {
      this.router.navigateByUrl(`/preview/${project}/${subPath}`);
    }
  }

  public async onUpload(event, target = 'docx'): Promise<void> {
    event.preventDefault();
    event.stopPropagation();
    let projectId = this.route.snapshot.params?.projectId;
    if (!projectId) {
      // starting a new project
      projectId = generate().dashed + '-' + uuidv4();
    }

    const file: File = (event.srcElement || event.target).files.item(0);
    if (!file) {
      return;
    }

    switch (file.type) {
      case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
        {
          //const zip = new JSZip();
          //const content = await zip.loadAsync(await file.arrayBuffer());
        }
        break;
      case 'application/zip':
        {
          const zip = new JSZip();
          const content = await zip.loadAsync(await file.arrayBuffer());
          if (target === 'tex') {
            const files = Object.keys(content.files);
            if (!files.includes('manuscript.tex')) {
              this.snackBar.open('ZIP must include manuscript.tex file');
              return;
            }
          }
        }
        break;
      default:
        console.error('Unrecognized type ' + file.type);
    }

    const uploadFile: any = await this.fileService.uploadFile(projectId, file);
    this.fileUpload.nativeElement.value = '';
    if (!uploadFile) {
      this.snackBar.open('Could not process file', 'Close', { duration: 5000 });
    } else {
      console.log('Updated successfully', uploadFile);
      this.router.navigateByUrl(`/preview/${uploadFile.key}`);
    }
  }
}
