import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Store } from '@ngrx/store';
import { removeAllInstances } from 'editor';
import { EditorComponent } from './editor.component';

@Injectable()
export class DeactivateGuard implements CanDeactivate<EditorComponent> {

  canDeactivate(component: EditorComponent): Promise<boolean> {
    return component.canDeactivate();
  }

  constructor(private store: Store) {}
}
