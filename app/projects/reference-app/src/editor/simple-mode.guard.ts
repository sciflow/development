import { CanActivateFn } from '@angular/router';

export const simpleModeGuard: CanActivateFn = (route, state) => {
  localStorage.setItem('simple-mode', 'true');
  return true;
};
