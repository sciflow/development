import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Subject} from 'rxjs';
import {projectsList, selectedDocument} from './project.actions';
import {ProjectService} from '../shared/services/project.service';
import {FileService} from 'shared';
import {map, switchMap, tap} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import {selectIsDirty} from '../../app/app-state.reducer';

@Component({
    selector: 'app-project',
    templateUrl: './project.component.html',
    styleUrls: ['./project.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: false
})
export class ProjectComponent implements AfterViewInit, OnDestroy {
  private fileUpload;
  @ViewChild('fileUpload') set content(content: HTMLInputElement) {
    // since acceptTos is hidden inside an *ngIf it will be undefined in the beginning
    if (content) {
      this.fileUpload = content;
    }
  }

  isDirty$ = this.store.select(selectIsDirty);
  stop$ = new Subject<void>();
  load$ = new Subject<{projectId: string; documentName?: string; version?: string}>();

  projectId$ = this.route.parent?.params.pipe(map(({projectId}) => projectId));
  files$ = this.route.parent?.params.pipe(
    switchMap(({projectId, documentName, version}) => {
      console.log('Loading data for', projectId, documentName, version);
      this.projectService.loadFiles(projectId).refetch();
      this.loading = false;
      return this.projectService
        .loadFiles(projectId)
        .valueChanges.pipe(map((queryResult) => ({projectId, version, queryResult})));
    }),
    map(({projectId, version, queryResult}) => {
      const result = queryResult.data?.project;
      const project = result.resources
        .map((resource) => {
          const fileName = resource.Key.replace(`${projectId}/`, '');
          const ext = fileName.split('.')[fileName.split('.').length - 1];

          return {
            id: FileService.getFileIdFromName(fileName),
            projectId,
            ext,
            ...resource,
            fileName,
          };
        })
        .filter((res) => res.ext === 'docx' || res.ext === 'json');

      const originalDocuments = project.filter((resultDocx) => resultDocx.ext === 'docx');
      const savedDocuments = project.filter((resultJson) => resultJson.ext === 'json');
      const resourcesAvailable =
        originalDocuments.length === 0 && savedDocuments.length === 0 ? false : true;

      return originalDocuments.map((projectResult) => {
        const savedDocumentWithVersions = savedDocuments.find(
          (savedDocument) =>
            projectResult.id === savedDocument.id &&
            projectResult.projectId === savedDocument.projectId,
        );
        this.loading = false;

        // updatating lastModified date
        this.updateDate(projectResult, savedDocumentWithVersions, version);

        return {
          ...projectResult,
          versions: [
            {
              ...projectResult.versions[0],
              initialDocument: true,
              fileName: projectResult.fileName,
              currentDocumentVersion: version,
            },
            ...(savedDocumentWithVersions?.versions || []).map((v) => ({
              ...v,
              fileName: savedDocumentWithVersions.fileName,
              currentDocumentVersion: version,
            })),
          ],
        };
      });
    }),
    tap((files) => this.store.dispatch(projectsList({projectsAvailable: files.length > 0}))),
  );

  loading = true;
  project$ = new Array();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fileService: FileService,
    private store: Store,
    private projectService: ProjectService,
    private snackBar: MatSnackBar,
  ) {}

  updateDate(projectResult, savedDocumentWithVersions, version): void {
    let docx = projectResult?.versions.slice(0, 1);
    let json = savedDocumentWithVersions?.versions.slice(0, 3);
    docx = docx?.filter((element) => element.VersionId === version);
    json = json?.filter((element) => element.VersionId === version);
    if (docx?.length !== 0 || (json?.length !== 0 && json !== undefined)) {
      const updateDate = docx?.length !== 0 ? docx : json;
      this.store.dispatch(selectedDocument({lastModified: updateDate[0].LastModified}));
    }
  }

  public async onUpload(event, projectId, prefix?: string): Promise<void> {
    event.preventDefault();
    event.stopPropagation();
    const uploadFile: any = await this.fileService.uploadFile(
      projectId,
      (event.srcElement || event.target).files.item(0),
      prefix
    );
    this.fileUpload.nativeElement.value = '';
    if (!uploadFile) {
      this.snackBar.open('Could not process file', 'Close', {duration: 5000});
    } else {
      console.log('Updated successfully', uploadFile);
      this.router.navigateByUrl(`/preview/${uploadFile.key}/${uploadFile.version.VersionId}`);
    }
  }

  /**
   * Redirects to a version of the resource.
   */
  documentRedirect(resource, version): void {
    this.router.navigateByUrl(
      `/edit/${resource.projectId}/${version.fileName}/${version.VersionId}/(context:project)`,
    );
  }

  ngAfterViewInit(): void {
    this.route.parent?.params.subscribe((params) => this.load$.next(params as any));
  }

  submit($event): void {
    this.fileUpload.nativeElement.click();
  }

  ngOnDestroy(): void {
    this.stop$.next();
  }
}
