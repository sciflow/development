import {Component, inject} from '@angular/core';
import {PluginStoreService} from '../../shared/services/plugin.service';
import { Store } from '@ngrx/store';
import { selectPluginById } from 'projects/plugins/src/lib/store';
import { Asset, AssetStatus } from '../asset.service';

@Component({
    selector: 'app-preview-assets',
    templateUrl: './preview-assets.component.html',
    styleUrls: ['./preview-assets.component.scss'],
    standalone: false
})
export class PreviewAssestsComponent {
  private pluginStore: PluginStoreService = inject(PluginStoreService);
  processAssetsPlugin$ = this.store.select(selectPluginById('process-assets'));

  isProcessing(assets: Asset[]) {
    return assets?.some(asset => asset.status === AssetStatus.Processing);
  }

  hasFailed(assets: Asset[]) {
    return assets?.some(asset => asset.status === AssetStatus.Error);
  }

  isSuccess(assets: Asset[]) {
    return !assets || assets.every(asset => asset.status === AssetStatus.Success);
  }

  constructor(private store: Store) {}
}
