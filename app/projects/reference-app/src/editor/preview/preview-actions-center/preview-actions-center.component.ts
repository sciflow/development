import { Component, OnInit, inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, ReplaySubject, Subscription, map, reduce, tap, withLatestFrom } from 'rxjs';
import { FileService } from 'shared';
import { StatusState } from '../../shared/models/state.enum';
import { PluginStoreService } from '../../shared/services/plugin.service';
import { ProcessService } from '../../shared/services/process.service';
import { Store } from '@ngrx/store';
import { WorkflowService } from '../workflow.service';
import { selectLogs, selectPluginActions, selectPluginById, selectPlugins } from 'projects/plugins/src/lib/store';
import { DataSource } from '@angular/cdk/collections';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { trigger, transition, style, animate } from '@angular/animations';

interface PluginStatusSummary {
  name: string;
  status: StatusState;
  description: string;
  gauge: {
    value: number;
    total: number;
    color: string;
  };
}

@Component({
    selector: 'app-preview-actions-center',
    templateUrl: './preview-actions-center.component.html',
    styleUrls: ['./preview-actions-center.component.scss'],
    animations: [
        trigger('fade', [
            transition('void => *', [style({ opacity: 0 }), animate(6000, style({ opacity: 1 }))]),
            transition('* => void', [animate(6000, style({ opacity: 0 }))]),
        ]),
    ],
    standalone: false
})
export class PreviewActionsCenterComponent {
  StatusState = StatusState;

  actions$ = this.store.select(selectPluginActions);
  logs$ = this.store.select(selectLogs);
  activeStep$ = this.workflow.activeStep$;

  dataSource = new ActionDataSource(this.store);
  displayedColumns: string[] = ['process', 'description', 'status'];

  data$ = this.workflow.data$.pipe(map((data) => {
    return {
      authors: data?.manuscript?.authors,
      references: data?.manuscript?.references,
      metaData: data?.manuscript?.metaData
    };
  }));

  pluginStatus$ = this.store.select(selectPluginActions).pipe(
    withLatestFrom(this.store.select(selectPlugins)),
    withLatestFrom(this.store.select(selectLogs)),
    map(([[actions, plugins], logs]) => {
      let pluginLogs: any = {};

      for (let plugin of plugins) {
        const pluginActions = actions.filter(a => a.pluginId === plugin.id);
        const pluginLogMessages = logs.filter(log => log.payload?.pluginId === plugin.id);
        if (pluginActions.length === 0 && pluginLogMessages.length === 0) { continue; }
        const withErrors = actions.some(a => a.status === StatusState.Error);
        const success = actions.some(a => a.status === StatusState.Success);
        const processing = actions.some(a => a.status === StatusState.Processing);

        if (!pluginLogs[plugin.id]) {
          pluginLogs[plugin.id] = {
            pluginId: plugin.id,
            name: plugin?.title || plugin?.name || plugin.id,
            description: plugin?.description
          };
        }

        if (!pluginLogs[plugin.id]?.gauge) {
          pluginLogs[plugin.id].gauge = { value: 0, total: 0, color: 'green' };
        }

        for (let message of [...pluginLogMessages, ...pluginActions]) {
          if (processing) {
            pluginLogs[plugin.id].status = StatusState.Processing;
          } else if (withErrors) {
            pluginLogs[plugin.id].gauge.total++;
            pluginLogs[plugin.id].gauge.color = 'warn';
            pluginLogs[plugin.id].status = StatusState.Error;
          } else if (success) {
            pluginLogs[plugin.id].gauge.total++;
            pluginLogs[plugin.id].gauge.value++;
            pluginLogs[plugin.id].status = StatusState.Success;
          } else {
            // we asume that no status is success
            pluginLogs[plugin.id].gauge.total++;
            pluginLogs[plugin.id].gauge.value++;
            pluginLogs[plugin.id].status = StatusState.Success;
          }
        }
      }

      return Object.keys(pluginLogs)
        .map(pluginId => (pluginLogs[pluginId])).map(plugin => {
          if (plugin.gauge) {
            plugin.gauge.percent = plugin.gauge.value > 0 ? Math.round(plugin.gauge.value / plugin.gauge.total * 100) : 0;
          }
          return plugin;
        })

    }));

  constructor(private store: Store, private workflow: WorkflowService) { }

  get current() {
    return this.workflow.activeStep$.value;
  }

  get next() {
    return this.workflow.next;
  }

  canGoNext(actions) {
    return this.workflow.canGoNext(actions);
  }

  async goNext() {
    this.workflow.goNext();
  }
}

class ActionDataSource extends DataSource<PluginStatusSummary> {
  constructor(private data$: Observable<any>) { super(); }

  connect(): Observable<PluginStatusSummary[]> {
    return this.data$;
  }

  disconnect() { }
}