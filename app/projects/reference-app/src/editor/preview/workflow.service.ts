import { Injectable } from '@angular/core';

import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AnnotatedManuscriptResponse, schemas } from '@sciflow/schema';
import { WindowRef } from 'projects/author-management/src/lib/WindowRef';
import { Plugin, addActions, addLogs, selectLogs, selectPluginActions, selectPluginById, selectPlugins, setPluginState, updateActions } from 'projects/plugins/src/lib/store';
import { Node } from 'prosemirror-model';
import { EditorState, Transaction } from 'prosemirror-state';
import { BehaviorSubject, Observable, Subject, firstValueFrom, timer } from 'rxjs';
import { FileService } from 'shared';
import { PluginService } from '../../plugin.service';
import { StatusState } from '../shared/models/state.enum';
import { Asset, AssetService, AssetStatus } from './asset.service';


interface AssetAction extends Action {
  asset: Asset;
}

interface PluginRunner {
  /**
   * Returns all applicable actions
   */
  actions: (input: TransportData) => Promise<Action[]>,
  /**
   * Updates the input based on the given actions.
   */
  patch: (input: TransportData, actions: Action[]) => Promise<TransportData>
}

type stepType = 'loading' | 'start' | 'import' | 'edit';

type CriticalSeverity = 'error';
type WarningSeverity = 'warning';
type InformationSeverity = 'info';
type ErrorSeverityLevel = CriticalSeverity | WarningSeverity | InformationSeverity;

// type Init = 'init';
// type Processing = 'processing';
// type Success = 'success';
// type Status = Init | Processing | Success | ErrorSeverityLevel;

export interface Action {
  /** A deterministic unique (functional) id for the action */
  id: string;
  pluginId?: string;
  name: string;
  nodeId?: string;
  tag?: any;
  /** A JSON schema for any needed input data */
  schema?: any;
  blocking?: boolean;
  [key: string]: any;
}

interface TransportData {
  projectId: string;
  documentName: string;
  state: EditorState;
  manuscript: TransportManuscriptFile;
}

export interface TransportManuscriptFile {
  authors: any[];
  /** references as CSL */
  references?: any[];
  /** A list of all files used in the document (e.g. to replace thumbnails with large resolution images in export) */
  files?: any[];
  metaData?: object;
}

@Injectable({
  providedIn: 'root'
})
export class WorkflowService {

  static stepOrder: { name: stepType, title: string; description?: string; }[] = [
    {
      name: 'loading',
      title: 'Loading ..'
    },
    {
      name: 'start',
      title: 'Verify document',
      description: 'Check headings and content for accuracy'
    },
    {
      name: 'import',
      title: 'Optimize content',
      description: 'We\'re refining your document for better use'
    },
    {
      name: 'edit',
      title: 'Make edits',
      description: 'Review and correct any issues yourself'
    }
  ];

  activeStep$ = new BehaviorSubject<{ name: stepType, title: string; description?: string; icon?: string; } | undefined>(WorkflowService.stepOrder[0]);

  plugins$ = this.store.select(selectPlugins);

  data$ = new BehaviorSubject<TransportData | undefined>(undefined);

  private _transactions$ = new Subject<Transaction>();
  private _state: EditorState;

  applyTransaction(tr: Transaction) {
    this._transactions$.next(tr);
  }

  get transactions$(): Observable<Transaction> {
    return this._transactions$;
  }

  get state(): EditorState {
    return this._state;
  }

  constructor(private store: Store, private pluginService: PluginService, private fileService: FileService, private assetService: AssetService, private router: Router, private snackBar: MatSnackBar, private window: WindowRef) { }

  /** Initializes an import workflow with a document */
  async initialize(projectId, documentName, processCitations = true) {
    const activatePlugins = localStorage.getItem('importer.plugins.enabled') != undefined;
    const events = activatePlugins ? ['document-enriched'] : [];
    const result = await this.fileService.getDocument(projectId, documentName, processCitations, events);
    console.info('Initializing workflow for ' + projectId + ' / ' + documentName, { processCitations, events });

    const logs: any[] = [];
    if (result.errors) {
      const documentError = result.errors.find((error) => error.path[0] === 'document');
      console.error(
        'Could not render preview',
        { documentError, errors: result.errors },
        result.preview,
        result.key,
      );
      if (documentError?.extensions?.errors) {
        logs.push({
          type: 'pandoc-error',
          level: 'fatal',
          message: documentError.message,
          payload: {
            code: documentError.extensions?.code,
            errors: documentError.extensions?.errors
          }
        });
        this.store.dispatch(addLogs({ logs }));

        return undefined;
      }
    }

    const document = result?.manuscript?.document;
    if (result.logs?.length > 0) {
      this.store.dispatch(addLogs({ logs: result.logs }));
    }

    if (!document) {
      console.error('Not document found');
      return;
    }

    const schema = schemas.manuscript;
    let state = EditorState.create({
      doc: Node.fromJSON(schema, document),
      schema,
    });

    console.info('Loaded document', result);

    this._state = state;

    this.activeStep$.next(WorkflowService.stepOrder[0]);
    this.data$.next({
      state,
      documentName,
      projectId,
      manuscript: {
        files: result.manuscript.assets || result.manuscript.files,
        authors: result.manuscript.authors || [],
        references: result.manuscript.references,
        metaData: result.manuscript.metaData || {}
      }
    });
  }

  get next() {
    const index = this.activeStep$.value ? WorkflowService.stepOrder.findIndex(step => step.name === this.activeStep$.value?.name) : -1;
    return WorkflowService.stepOrder[index + 1];
  }

  async goNext(): Promise<void> {
    const index = this.activeStep$.value ? WorkflowService.stepOrder.findIndex(step => step.name === this.activeStep$.value?.name) : -1;
    const next = WorkflowService.stepOrder[index + 1];
    const data = this.data$.value;

    const actions = await firstValueFrom(this.store.select(selectPluginActions));
    if (!this.canGoNext(actions)) { return; }

    if (next && data) {
      const runner = await this.createRunner(next.name);
      let actions = await runner.actions(data);

      const logs = await firstValueFrom(this.store.select(selectLogs));
      if (!actions || actions.length === 0 && logs.length === 0) {
        // skip this step
        console.info('No actions for ' + next.name);
        this.activeStep$.next(next);
        return this.goNext();
      }

      this.snackBar.open('Running: ' + next.title, undefined, { duration: 1500 });

      this.store.dispatch(addActions({ actions }));
      const newData = await runner.patch(data, actions);
      if (newData) {
        console.info('Finished ' + next?.name, { index });
        // TODO handle unresolved actions
        actions = await runner.actions(newData);
        this.store.dispatch(updateActions({ actions, clear: true }));
        this.data$.next(newData);
      }

      this.activeStep$.next(next);
    }
  }

  canGoNext(actions: Action[]): boolean {
    const activeStep = this.activeStep$.value;
    if (!activeStep) { return true; }

    return actions.every((action => (action.blocking !== true && !action.schema) || action.value?.do?.default === true));
  }

  /** Copys over assets from the source container (docx/zip) */
  private async start(): Promise<PluginRunner> {
    const pluginId = 'process-assets';
    const updateAssetState = async (asset: Asset, values: any) => {
      const plugin: Plugin<{ assets: Asset[] }> | undefined = await firstValueFrom(this.store.select(selectPluginById(pluginId)));
      if (!plugin) { throw new Error(pluginId + ' plugin is required'); } // TODO find a better way to ensure a plugin is active
      let state = plugin.state || {};
      if (!state.assets) { state.assets = []; }
      if (!state.assets.some(a => a.id === asset.id)) {
        state = {
          ...state,
          assets: [...state.assets, { ...asset, ...values }]
        };
      } else {
        state = {
          ...state,
          assets: [...state.assets.map((a) => {
            if (!a.id || a.id !== asset.id) { return a; }
            return { ...a, ...values };
          })]
        };
      }

      this.store.dispatch(setPluginState({ pluginId: plugin.id, state }));
    }

    return {
      actions: async (input) => {
        // We use a custom UI to allow additional file uploads etc.
        // only process files that do not have a URL already
        const assets = [...(input.manuscript.files as Asset[] || [])];
        const actions: AssetAction[] = [];
        for (let asset of assets) {
          if (asset.url) {
            actions.push({
              id: 'process-asset-copy-' + asset.id,
              pluginId,
              name: asset.id + ' present',
              blocking: false,
              status: StatusState.Success,
              asset
            });
          } else {
            actions.push({
              id: 'process-asset-copy-' + asset.id,
              name: 'Copy ' + asset.id,
              pluginId,
              blocking: true,
              status: StatusState.Processing,
              asset
            });
            await updateAssetState(asset, { status: AssetStatus.Processing });
          }

        }

        return actions;
      },
      patch: async (input: TransportData, actions: Action[]) => {
        const logs: any[] = [];
        if (!input.manuscript?.files) { throw new Error('Files must be provided as an input'); }
        const assets: Asset[] = [];
        for (let asset of input.manuscript.files) {
          const action = actions.find(a => a.asset.id === asset.id);
          if (action) {
            await updateAssetState(action.asset, { status: AssetStatus.Processing });
            // we do intermittent updates since copying and transforming might take a while
            try {
              const result = await this.assetService.copyAsset(asset, { projectId: input.projectId, documentName: input.documentName });
              if (result.url) {
                asset = result;
              }
              // mark asset as completed
              await updateAssetState(asset, { ...result, status: AssetStatus.Success });
            } catch (e) {
              // mark asset as failed
              await updateAssetState(asset, { status: AssetStatus.Error });
              logs.push({ type: 'error', message: e.message }); // TODO type this
            }
          }

          assets.push(asset);
        }

        return {
          ...input,
          manuscript: {
            ...input.manuscript,
            files: assets
          }
        };
      }
    };
  }

  /** Read the initial manuscript */
  private async import(): Promise<PluginRunner> {
    return {
      actions: async (input: TransportData) => this.getActions('document-enriched', input),
      patch: async (input: TransportData, actions: Action[]) => {
        const patchResult = await this.triggerPluginsFor('document-enriched', input, actions);
        if (patchResult) {
          return {
            ...input,
            state: patchResult.state
          };
        }
        return input;
      }
    };
  }

  /** Prepare the manuscript to be loaded into the editor */
  private async edit(): Promise<PluginRunner> {
    return {
      actions: async (input: TransportData) => {
        const actions = await this.getActions('start-editing', input);
        // we need to make sure that at least one action is included to trigger the patch function
        actions.push({
          id: 'start-editing',
          name: 'Start editing'
        });
        return actions;
      },
      patch: async (input: TransportData, _actions: Action[]) => {
        const file = new File(
          [JSON.stringify({
            ...input.manuscript,
            document: input.state.doc.toJSON()
          }, null, 2)],
          FileService.getFileIdFromName(input.documentName) + '.json',
          { type: 'application/json' },
        );
        const firstDocument: any = await this.fileService.uploadFile(input.projectId, file);
        await firstValueFrom(timer(500));
        // we use window instead of the router to force re-initializiation
        this.window.href = `/edit/${firstDocument?.key}/${firstDocument?.version?.VersionId || 'unversioned'}/(context:outline)`;

        return input;
      }
    };
  }

  /** Steps ahead activating the next plugins */
  private async createRunner(name: stepType): Promise<PluginRunner> {
    if (!(typeof this[name] === 'function')) { throw new Error('Undefined step ' + name); }
    return this[name as string].call(this);
  }

  /** Runs a plugin and returns the available actions */
  private async getActions(event: string, input: { state: EditorState; manuscript: TransportManuscriptFile }): Promise<Action[]> {
    const plugins = await firstValueFrom(this.plugins$);

    let actions: Action[] = [];
    for (let plugin of plugins.filter(
      (plugin) =>
        plugin.runners?.includes('browser') &&
        plugin.triggers?.some((t) => t.event === event),
    )) {
      try {
        const content = await this.pluginService.loadPlugin(plugin);
        const patchJs = content.scripts.find((s) => s.name === 'patch');
        if (patchJs) {
          // check the actions first and see if user confirmation is needed
          if (typeof patchJs.content.actions === 'function') {
            actions = [...actions, ...(patchJs.content.actions(input)?.map(a => ({ ...a, pluginId: plugin.id })) || [])]
          }
        }
      } catch (e) {
        console.error('Could not run plugin actions' + plugin.name, { message: e.message });
        console.error(e.stack);
      }
    }

    console.info('Actions for ' + event, actions);
    return actions;
  }

  /** Runs a plugin and returns the updated data */
  private async triggerPluginsFor(
    event: string,
    input: { state: EditorState; manuscript: TransportManuscriptFile },
    actions: any[]
  ): Promise<any> {
    console.time('Executed plugins for ' + event);
    const plugins = await firstValueFrom(this.plugins$);
    for (let plugin of plugins.filter(
      (plugin) =>
        plugin.runners?.includes('browser') &&
        plugin.triggers?.some((t) => t.event === event),
    )) {
      try {
        const content = await this.pluginService.loadPlugin(plugin);
        const patchJs = content.scripts.find((s) => s.name === 'patch');
        if (patchJs) {
          let result = patchJs.content.patch(input, actions.filter(a => !a.pluginId || a.pluginId === plugin.id));
          if (typeof result?.then === 'function') { result = await result; }

          if (this._state && result.tr) {
            this.applyTransaction(result.tr);
            try {
              const applyResult = this._state.applyTransaction(result.tr);
              if (applyResult.state) { this._state = applyResult.state; }
            } catch (e) {
              console.error('Could not apply transaction', result);
              debugger;
            }
          }

          if (result?.state) { input.state = result.state; }
          if (result?.manuscript) {
            input.manuscript = {
              ...input.manuscript,
              ...result.manuscript,
            };
          }
          if (result?.tr) {
            if (result.tr.docChanged) {
              input.state = input.state.apply(result.tr);
            }
          }

          console.info(`[${event}] Completed plugin ${plugin.name}`, result, input.state?.doc?.nodeSize);
        }
      } catch (e) {
        console.error('Could not run plugin ' + plugin.name, { message: e.message });
        console.error(e.stack);
      }
    }

    console.timeEnd('Executed plugins for ' + event);
    return {
      ...input,
    };
  }
}
