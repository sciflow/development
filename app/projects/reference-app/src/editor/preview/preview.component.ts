import { animate, style, transition, trigger } from '@angular/animations';
import { AfterViewInit, Component, Inject, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { DOMSerializer } from 'prosemirror-model';
import { debounceTime, distinctUntilChanged, distinctUntilKeyChanged, filter, map, shareReplay, switchMap, takeUntil, tap } from 'rxjs/operators';
import { FileService } from 'shared';
import { EditorView, NodeView } from 'prosemirror-view';

import { DOCUMENT } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store } from '@ngrx/store';
import { selectLogs, selectPluginActions } from 'projects/plugins/src/lib/store';
import { Subject, combineLatest, firstValueFrom } from 'rxjs';
import { PluginStoreService } from '../shared/services/plugin.service';
import { WorkflowService } from './workflow.service';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';

@Component({
    selector: 'app-preview',
    templateUrl: './preview.component.html',
    styleUrls: ['./preview.component.scss'],
    animations: [
        trigger('fade', [
            transition('void => *', [style({ opacity: 0 }), animate(45000, style({ opacity: 1 }))]),
            transition('* => void', [animate(20000, style({ opacity: 0 }))]),
        ]),
    ],
    standalone: false
})
export class PreviewComponent implements AfterViewInit {
  @ViewChild('editor') editorDiv;
  params$ = combineLatest([this.activatedRoute.params, this.activatedRoute.queryParamMap]).pipe(
    (map(([params, queryParams]) => {
      return { projectId: params.projectId, documentName: params.documentName, processCitations: queryParams?.get('processCitations') !== 'false' };
    })),
    distinctUntilKeyChanged('projectId'),
    distinctUntilChanged((prev, curr) => prev?.projectId == curr?.projectId &&
      prev?.documentName == curr?.documentName &&
      prev?.processCitations == curr?.processCitations)
  );

  stop$ = new Subject<void>();
  private view: EditorView;

  logHasFatals$ = this.store.select(selectLogs).pipe(map((logs) => logs.some(log => log.level === 'fatal' || log.level === 'error')));
  logs$ = this.store.select(selectLogs);

  instanceData = JSON.parse(this.document.getElementById('instance-data').innerHTML);
  showLabels = localStorage.getItem('editor-show-labels') != undefined;

  actions$ = this.store.select(selectPluginActions);

  get current() {
    return this.workflow.activeStep$.value;
  }

  get next() {
    return this.workflow.next;
  }

  /** The plugin loaded from the backend. */
  manuscript$ = this.params$.pipe(
    switchMap(async ({ projectId, documentName, processCitations }) => this.workflow.initialize(projectId, documentName, processCitations)),
    switchMap(_ => this.workflow.data$.asObservable()),
    shareReplay(1)
  );

  html$ = this.manuscript$.pipe(
    switchMap(async (manuscript) => {
      if (!manuscript?.state) { return undefined; }

      const serializer2 = DOMSerializer.fromSchema(manuscript.state.schema);
      const content = serializer2.serializeFragment(manuscript.state.doc.content);
      const el = this.document.createElement('div');
      el.append(content);

      return this.domSanitizer.bypassSecurityTrustHtml(el.innerHTML);
    }),
  );

  constructor(
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private fileService: FileService,
    private domSanitizer: DomSanitizer,
    private router: Router,
    private snackBar: MatSnackBar,
    private pluginStoreService: PluginStoreService,
    @Inject(DOCUMENT) private document,
    private store: Store,
    private workflow: WorkflowService
  ) { }

  scrollTo(id: string) {
    if (!id) {
      return;
    }
    const el = this.document.getElementById(id);
    el?.scrollIntoView();
  }

  async ngAfterViewInit() {
    await firstValueFrom(this.manuscript$);
    this.workflow.goNext();
    this.workflow.data$.pipe(takeUntil(this.stop$), debounceTime(200)).subscribe((data) => {


      if (this.current?.name === 'start') {
        this.openWelcomeDialog();
      }

      if (!data?.state) { return; }
      if (!this.view && this.editorDiv) {
        this.view = new EditorView(this.editorDiv.nativeElement, {
          state: this.workflow.state,
          dispatchTransaction: (transaction) => {
            // do nothing
          },
          handleDOMEvents: {
            /* 'focus': (view, event) => {
              event.preventDefault();
              event.stopPropagation();
              return true;
            } */
          }
        });
      } else {
        this.view?.updateState(data.state);
      }
    });
  }

  openWelcomeDialog() {
    if (localStorage.getItem('show-initial-import-tip1')) { return; }
    const dialogRef = this.dialog.open(WelcomeDialog, {
      width: '50vw'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        localStorage.setItem('show-initial-import-tip1', Date.now() + '');
      }
    });
  }

  onDestroy() {
    this.view.destroy();
    this.stop$.next();
  }
}

@Component({
    selector: 'welcome-dialog',
    templateUrl: 'welcome-dialog.component.html',
    imports: [MatCardModule, MatDialogModule, MatButtonModule, MatButtonModule]
})
export class WelcomeDialog {}