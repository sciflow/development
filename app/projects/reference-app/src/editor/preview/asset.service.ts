import { Injectable } from '@angular/core';
import { getType, getExtension } from 'mime';
import { FileService } from 'shared';

export interface Asset {
  id: string;
  status?: AssetStatus;
  type?: 'image' | string;
  name?: string;
  key: string;
  url?: string;
  stats?: object;
}

export enum AssetStatus {
  Initial = 'initial',
  Processing = 'processing',
  Success = 'success',
  Error = 'error',
}

@Injectable({
  providedIn: 'root'
})
export class AssetService {

  constructor(private fileService: FileService) { }

  /** Copys an asset from one URL to a new one that can be used alongside the imported document. */
  async copyAsset(asset: Asset, { projectId, documentName }): Promise<Asset> {
    // import files are usually extracted from the original file (e.g. DOCX/ZIP/..)
    const fileUrl = `/import/extract-asset/${projectId}/${documentName}/${encodeURIComponent(asset.key)}`; // being explicit with media here
    let retries = 0;
    const maxRetries = 3;
    while (retries < maxRetries) {
      const file = await fetch(fileUrl);
      let fileName = asset.key;
      const ext = fileName.split('.').pop();
      const fileNameWOExtension = fileName.replace(ext || '', '');
      if (file.status === 200) {
        const contentType = file.headers.get('Content-Type');
        if (contentType) {
          const blob = await file.arrayBuffer();
          const returnExt = getExtension(contentType);
          if (ext && returnExt && returnExt !== ext) { fileName = fileName.replace(ext, returnExt); }
          // we prefix with the document name to upload to a unique path inside the project
          // we concatenate / resulting in a virtual folder structure
         fileName = encodeURIComponent(FileService.getFileIdFromName(documentName) + '/' + fileNameWOExtension + returnExt);
         const uploadResult = await this.fileService.uploadFile(projectId, new File([blob], fileName));

         // we keep the original id, since that's whats referenced in the document
         const url = `/export/asset/${projectId}/${fileName}`;
         const newFile = {
          id: asset.id || asset.key?.replaceAll('/', '_'),
          type: 'image',
          key: uploadResult.name || asset.key, // use the new name if possible
          name: uploadResult.name || asset.key, // use the new name if possible
          url
        };
         console.info('Upload result', { uploadResult, newFile });
         return newFile;
        }
      } else {
        throw new Error('Could not transform asset'); // TODO add reasons
      }
    }

    throw new Error('Could not transform asset'); // TODO add reasons
  }
}
