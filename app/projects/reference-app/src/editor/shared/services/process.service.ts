import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, catchError, from, map, mergeMap, of, tap} from 'rxjs';
import {PluginState} from '../models/plugin.model';
import {StatusState} from '../models/state.enum';

@Injectable({
  providedIn: 'root',
})
export class ProcessService {
  private plugins: BehaviorSubject<PluginState[]> = new BehaviorSubject<PluginState[]>([]);

  plugins$ = this.plugins.asObservable();

  // Selector
  isPluginsProcessing$: Observable<boolean> = this.plugins$.pipe(
    map((queue) => queue.some((process) => process.status === StatusState.Processing)),
  );

  addPlugin(process: PluginState): void {
    const currentQueue = this.plugins.value;
    this.plugins.next([...currentQueue, process]);
  }

  startProcessingPlugins(): Observable<any> {
    return from(this.plugins.value).pipe(
      mergeMap((process) => {
        process.status = StatusState.Processing;
        this.emitUpdatedQueue(process);

        return process.execute().pipe(
          map(() => {
            process.status = StatusState.Success;
            process.description = 'Processed successfully.';
            this.emitUpdatedQueue(process);
          }),
          catchError((error) => {
            process.status = StatusState.Error;
            process.description = error?.message || 'Something went wrong.';
            this.emitUpdatedQueue(process);
            return of(error);
          }),
        );
      }),
    );
  }

  private emitUpdatedQueue(updatedProcess: PluginState): void {
    const updatedQueue = this.plugins.value.map((process) =>
      process.id === updatedProcess.id ? {...process, ...updatedProcess} : process,
    );
    this.plugins.next(updatedQueue);
  }
}
