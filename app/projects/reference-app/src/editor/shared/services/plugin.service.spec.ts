import {TestBed} from '@angular/core/testing';

import {PluginStoreService} from './plugin.service';

describe('PluginService', () => {
  let service: PluginStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PluginStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
