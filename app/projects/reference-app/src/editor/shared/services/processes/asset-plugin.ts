import {inject} from '@angular/core';
import {Observable, catchError, combineLatest, from, map, switchMap, take} from 'rxjs';
import {FileService} from 'shared';
import {PluginState} from '../../models/plugin.model';
import {StatusState} from '../../models/state.enum';
import {PluginStoreService} from '../plugin.service';

export class AssetPlugin implements PluginState {
  private fileService: FileService = inject(FileService);
  private pluginStore: PluginStoreService = inject(PluginStoreService);

  id: string = 'asset';
  isBlocking: boolean = false;
  status = StatusState.Initial;

  execute(): Observable<any> {
    return combineLatest([
      this.pluginStore.assets$,
      this.pluginStore.projectId$,
      this.pluginStore.documentName$,
    ]).pipe(
      take(1),
      switchMap(([assetsState, projectId, documentName]) => {
        const assets = assetsState.map(({status, ...asset}) => asset);

        return from(this.fileService.extractAssets(projectId, documentName, assets)).pipe(
          switchMap(() => this.pluginStore.assets$.pipe(take(1))),
          map((assets) => {
            const hasError = assets.some((asset) => asset.status === StatusState.Error);

            if (hasError) {
              throw new Error('At least one asset has an error status');
            }
          }),

          catchError((error) => {
            throw error;
          }),
        );
      }),
    );
  }
}
