interface ImageAttributes {
  src: string;
}

interface ImageDetails {
  attrs: ImageAttributes;
}

interface IdMap {
  [key: string]: ImageDetails;
}

export interface Listing {
  idMap: IdMap;
}
