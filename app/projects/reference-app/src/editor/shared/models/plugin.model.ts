import {Observable} from 'rxjs';
import {StatusState} from './state.enum';

export interface Plugin {
  id: string; // plugin name
  description?: string; // description of what the plugin does
  triggers?: {event: string}[];
  runners?: 'browser' | 'server';
  scripts?: {name: 'patch' | 'tag'}[];
}

export interface PluginState extends Plugin {
  status: StatusState;
  isBlocking: boolean;
  execute(): Observable<any>;
}
