export interface Asset {
  id: string;
  key: string;
  stats?: object;
}
