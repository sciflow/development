import { inject } from '@angular/core';
import {CanActivateFn } from '@angular/router';
import { Store } from '@ngrx/store';
import { setPlugins } from 'projects/plugins/src/lib/store';
import { PluginService } from './plugin.service';

export const pluginsGuard: CanActivateFn = async (route, state) => {

  const activatePlugins = localStorage.getItem('importer.plugins.enabled') != undefined;

  const pluginService: PluginService = inject(PluginService);
  const store = inject(Store);
  const plugins = await pluginService.loadPlugins(!activatePlugins);

  store.dispatch(setPlugins({ plugins }));

  return true;
};
