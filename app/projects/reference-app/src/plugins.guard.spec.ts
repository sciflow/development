import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { pluginsGuard } from './plugins.guard';

describe('pluginsGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => pluginsGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
