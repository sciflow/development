import { createAction, props } from "@ngrx/store";

export const fileUploaded = createAction('[Shared] File uploaded', props<{ projectId: string; extension: string; name: string; key: string; }>());