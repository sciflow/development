import { NgModule } from '@angular/core';
import { SchemaComponent } from './schema.component';



@NgModule({
  declarations: [
    SchemaComponent
  ],
  imports: [
  ],
  exports: [
    SchemaComponent
  ]
})
export class SchemaModule { }
