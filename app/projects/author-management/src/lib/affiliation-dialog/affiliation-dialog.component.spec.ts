import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AffiliationDialogComponent} from './affiliation-dialog.component';

describe('AffiliationDialogComponent', () => {
  let component: AffiliationDialogComponent;
  let fixture: ComponentFixture<AffiliationDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AffiliationDialogComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AffiliationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
