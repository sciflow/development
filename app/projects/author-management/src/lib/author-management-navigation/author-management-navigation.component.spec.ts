import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AuthorManagementNavigationComponent } from './author-management-navigation.component';

describe('AuthorManagementNavigationComponent', () => {
  let component: AuthorManagementNavigationComponent;
  let fixture: ComponentFixture<AuthorManagementNavigationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorManagementNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorManagementNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
