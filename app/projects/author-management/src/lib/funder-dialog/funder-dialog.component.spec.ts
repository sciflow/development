import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FunderDialogComponent } from './funder-dialog.component';

describe('FunderDialogComponent', () => {
  let component: FunderDialogComponent;
  let fixture: ComponentFixture<FunderDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FunderDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FunderDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
