/*
 * Public API Surface of author-management
 */

export * from './lib/author-management.module';
export * from './lib/author-management.actions';
export * from './lib/author-management.reducer';
