import {ComponentFixture, TestBed} from '@angular/core/testing';

import {FigurePreviewModalComponent} from './figure-preview-modal.component';

xdescribe('FigurePreviewModalComponent', () => {
  let component: FigurePreviewModalComponent;
  let fixture: ComponentFixture<FigurePreviewModalComponent>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [FigurePreviewModalComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FigurePreviewModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
