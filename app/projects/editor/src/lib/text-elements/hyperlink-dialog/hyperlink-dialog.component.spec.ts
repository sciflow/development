import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HyperlinkDialogComponent } from './hyperlink-dialog.component';

xdescribe('HyperlinkDialogComponent', () => {
  let component: HyperlinkDialogComponent;
  let fixture: ComponentFixture<HyperlinkDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HyperlinkDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HyperlinkDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
