/**
 * @license
 * Copyright SciFlow GmbH All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at http://sciflow.org/license
 */

import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { EditorOverlay } from './editor-overlay';
import { EditorOverlayContainer } from './editor-overlay-container';
import { ErrorDialogComponent } from './error-dialog/error-dialog.component';
import { ErrorDialogService } from './error-dialog/error-dialog.service';
import { PMEditorComponent } from './pm-editor.component';
import { SelectionActionsComponent } from './selection-actions/selection-actions.component';
import { StructureMenuComponent } from './structure-menu/structure-menu.component';
import { QuickActionsComponent } from './quick-actions/quick-actions.component';
import { QuickActionsMenuComponent } from './quick-actions/quick-actions-menu/quick-actions-menu.component';
import { MatBadgeModule } from '@angular/material/badge';

@NgModule({
  declarations: [
    PMEditorComponent,
    SelectionActionsComponent,
    StructureMenuComponent,
    ErrorDialogComponent,
    QuickActionsComponent,
    QuickActionsMenuComponent,
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatBottomSheetModule,
    MatSelectModule,
    MatSnackBarModule,
    MatIconModule,
    MatTooltipModule,
    MatDialogModule,
    PortalModule,
    OverlayModule,
    MatFormFieldModule,
    MatInputModule,
    MatMenuModule,
    MatDividerModule,
    MatBadgeModule,
  ],
  providers: [
    EditorOverlayContainer,
    EditorOverlay,
    {
      provide: MatDialogRef,
      useValue: {},
    },
    ErrorDialogService,
  ],
  exports: [PMEditorComponent],
})
export class ProsemirrorModule {}
