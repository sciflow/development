import { FootnoteComponent } from '../../text-elements/footnote/footnote.component';
import { insertFootnote } from '../commands';
import { schemas } from '@sciflow/schema';
import { Node } from 'prosemirror-model';
import { NodeView } from 'prosemirror-view';

export class FootnoteView implements NodeView {
  dom;

  constructor(private node, private outerView, private bottomSheet, private getPos, private injector, private editorView) {
    this.dom = document.createElement('footnote');
    this.dom.addEventListener('click', this.openDialog.bind(this));
    this.dom.setAttribute('id', node.attrs.id);
    // FIXME remove workaround for schema conversion
  }

  openDialog(): void {
    const bottomSheetRef = this.bottomSheet.open(FootnoteComponent, {
      data: { node: this.transformToInlineSchema(this.node) }
    });
    bottomSheetRef.afterDismissed().subscribe(result => {
      if (!result) { return; }
      const footnote = this.transformToDocumentSchema(result);
      insertFootnote(footnote)(this.outerView.state, this.outerView.dispatch);
      const element = document.getElementById(footnote.attrs.id);
      element?.scrollIntoView({
        behavior: 'smooth',
        block: 'center',
        inline: 'nearest'
      });
    });
  }

  /**
   * Incoming update from the editor.
   */
  update(node): boolean {
    if (!node.sameMarkup(this.node)) { return false; }
    if (node.type !== this.node.type) { return false; }

    this.node = node;
    return true;
  }

  transformToInlineSchema(footnote: Node): Node {
    const serialized = footnote.toJSON();
    const doc = Node.fromJSON(schemas.inline, { ...serialized, type: 'doc' });
    return doc;
  }

  /** Transforms from the inline schema to the document schema */
  transformToDocumentSchema(footnoteInlineDoc: Node): Node {
    const serialized = footnoteInlineDoc.toJSON();
    const targetNode = Node.fromJSON(this.outerView.state.schema, { ...serialized, type: 'footnote', attrs: this.node.attrs });
    return targetNode;
  }
}
