import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectElements, selectInstances, selectSelection } from '../editor.reducer';
import { map } from 'rxjs/operators';

@Component({
    selector: 'sfo-outline',
    templateUrl: './outline.component.html',
    styleUrls: ['./outline.component.css'],
    standalone: false
})
export class OutlineComponent implements OnInit {

  editors$ = this.store.select(selectInstances);
  headings$ = this.store.select(selectElements).pipe(map((elements) => elements.filter(element => element.type === 'heading')));
  figures$ = this.store.select(selectElements).pipe(map((elements) => elements.filter(element => element.type === 'figure')));
  citations$ = this.store.select(selectElements).pipe(map((elements) => elements.filter(element => element.type === 'citation').filter((c, i, citations) => citations.findIndex(c2 => c2.label === c.label) === i)));
  selection$ = this.store.select(selectSelection);

  navigateTo(event, id: string) {
    event.preventDefault();
    document.getElementById(id)?.scrollIntoView();
  }

  constructor(private store: Store) { }

  ngOnInit(): void {
    console.log('outline');
  }

}
