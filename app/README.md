# EditingUi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli)

## Development server

Run `yarn start` for a dev server. Navigate to `https://localhost:4200/`.

## Upgrading Angular

Run the cleaning script to remove all installed node_modules first.

```bash
cd app
```

Make sure angular knows to use yarn (since we are not in the directory with the yarn.lock)

```bash
ng config --global cli.packageManager yarn
```

Update Angular and Angular material to the latest version using Angular's [update guide](https://angular.dev/update-guide).

Since both Angular and our server use Webpack to build the code it might make sense to upgrade our serverside to the latest version used by Angular.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
