import * as js2xmlparser from 'js2xmlparser';

export interface TestCase {
  classname: string;
  name: string;
  file?: string;
  time: number;
  failureMessage?: string;
  failureDescription?: string;
  skipped?: boolean;
  description?: string;
  logFile?: string;
}

export interface TestSuite {
  name: string;
  tests: number;
  failures: number;
  errors: number;
  skipped: number;
  testCases: TestCase[];
}

export interface TestSuites {
  testSuites: TestSuite[];
}

export function createJUnitXMLReport(testSuites: TestSuites): string {
  const suites = testSuites.testSuites.map((suite) => ({
    '@': {
      name: suite.name,
      tests: suite.tests,
      failures: suite.failures,
      errors: suite.errors,
      skipped: suite.skipped,
    },
    testcase: suite.testCases.map((testCase) => {
      const baseCase: any = {
        '@': {
          classname: testCase.classname,
          name: testCase.name,
          file: testCase.file,
          time: testCase.time.toFixed(3),
        },
      };

      if (testCase.failureMessage) {
        const failureElement: any = {
          '@': {
            message: testCase.failureMessage,
          },
          '#': testCase.failureDescription || '',
        };
        if (testCase.logFile) {
          failureElement.logFile = testCase.logFile;
        }
        return {
          ...baseCase,
          failure: failureElement,
        };
      } else if (testCase.skipped) {
        return {
          ...baseCase,
          skipped: {
            '@': {},
            '#': testCase.description || '',
          },
        };
      }

      return baseCase;
    }),
  }));

  return js2xmlparser.parse('testsuites', {testsuite: suites});
}
