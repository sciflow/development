import { mkdirSync, writeFileSync } from 'fs';
import { sync as globSync } from 'glob';
import { basename, dirname, extname, join } from 'path';
import { createLogger, format, transports } from 'winston';
import { kebabToPascal } from '../libs/export/src/utils';
import {
  generateSchema,
  generateNodeSchema,
  getComponentPaths,
} from '../libs/export/src/html/json-schema-2';

interface SchemaGenerationConfig {
  baseUrl: string;
  outDir: string;
  schemaGenerator: typeof generateSchema | typeof generateNodeSchema;
}

const logger = createLogger({
  level: 'info',
  format: format.json(),
  defaultMeta: { service: 'schema' },
  transports: [new transports.Console()],
});

async function gatherAndWriteSchemas(config: SchemaGenerationConfig): Promise<void> {
  const pattern = `${config.baseUrl}/**/*.schema.@(ts|tsx)`;
  const schemaFilePaths: string[] = globSync(pattern);

  const schemaKinds: string[] = schemaFilePaths.map((filePath) => {
    const fileName = basename(filePath, extname(filePath));
    return kebabToPascal(fileName.replace('.schema', ''));
  });

  const kindToFilePath: Map<string, string> = getComponentPaths(schemaKinds, config.baseUrl);

  for (const [kind, filePath] of kindToFilePath) {
    const relativePath = filePath.replace(config.baseUrl, '');
    const outputFilePath = join(config.outDir, relativePath.replace(/\.[jt]sx?$/, '.json'));
    const generatedSchema = await config.schemaGenerator(kind, filePath, config.baseUrl);

    mkdirSync(dirname(outputFilePath), { recursive: true });
    writeFileSync(outputFilePath, JSON.stringify(generatedSchema, null, 2));
    logger.info(`Generated schema for ${kind} → ${outputFilePath}`);
  }
}

async function generateGenericSchema(): Promise<void> {
  const genericConfigPath =
    'libs/export/src/html/renderers/generic-configuration/generic-configuration.schema.ts';
  const outputPath = join(
    'dist',
    'libs',
    'export',
    'renderers',
    'generic-configuration',
    'generic-configuration.schema.json',
  );

  const genericSchema = await generateSchema('Generic', genericConfigPath);
  mkdirSync(dirname(outputPath), { recursive: true });
  writeFileSync(outputPath, JSON.stringify(genericSchema, null, 2));
  logger.info(`Writing generic schema to ${outputPath}`);
}

export async function run(): Promise<void> {
  await generateGenericSchema();

  const configs: SchemaGenerationConfig[] = [
    {
      baseUrl: join('libs', 'export', 'src', 'html', 'components'),
      outDir: join('dist', 'libs', 'export', 'components'),
      schemaGenerator: generateSchema,
    },
    {
      baseUrl: join('libs', 'export', 'src', 'html', 'node-schemas'),
      outDir: join('dist', 'libs', 'export', 'node-schemas'),
      schemaGenerator: generateNodeSchema,
    },
  ];

  for (const config of configs) {
    await gatherAndWriteSchemas(config);
  }
}

if (require.main === module) run();
