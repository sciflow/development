const fs = require('fs');
const {spawn} = require('child_process');
const glob = require('glob');
const chokidar = require('chokidar');

const PLUGIN_NAME = 'change-detection-plugin';

class ChangeDetectionPlugin {
  constructor(options) {
    this.directoryPattern = options.directoryPattern;
    this.script = options.script;
    this.tsconfig = options.tsconfig;
    this.fileTimestamps = new Map();
    this.watcher = null;
    this.isScriptRunning = false;
  }

  apply(compiler) {
    const logger = compiler.getInfrastructureLogger(PLUGIN_NAME);

    // Single exec is "run" event, can use "beforeRun"
    compiler.hooks.beforeRun.tap(PLUGIN_NAME, (compilation, callback) => {
      this.runScript(logger, callback);
    });

    // Watch exec is "watchRun" event
    compiler.hooks.watchRun.tapAsync(PLUGIN_NAME, (compilation, callback) => {
      // Use glob to get the list of files
      glob(this.directoryPattern, (err, files) => {
        if (err) {
          logger.error(`Error reading directory pattern ${this.directoryPattern}:`, err);
          return callback();
        }

        const changedFiles = [];

        files.forEach((file) => {
          const stats = fs.statSync(file);

          const prevTimestamp = this.fileTimestamps.get(file);
          const currentTimestamp = stats.mtimeMs;

          if (prevTimestamp !== undefined && prevTimestamp !== currentTimestamp) {
            changedFiles.push(file);
          }

          this.fileTimestamps.set(file, currentTimestamp);
        });

        if (changedFiles.length > 0) {
          logger.info(`Changes detected in: ${changedFiles.join('\n')}`);
          this.runScript(logger, callback);
        } else {
          callback();
        }

        // Initialize the watcher if not already done
        if (!this.watcher) {
          this.initializeWatcher(logger);
        }
      });
    });
  }

  initializeWatcher(logger) {
    this.watcher = chokidar.watch(this.directoryPattern, {persistent: true, followSymlinks: true});

    this.watcher.on('ready', () => {
      logger.info('Watcher initialized and ready');
      this.runScript(logger);
    });

    this.watcher.on('change', (filePath) => {
      this.runScript(logger);
    });
  }

  runScript(logger, callback = () => {}) {
    if (this.isScriptRunning) {
      logger.log('Script is already running, skipping execution.');
      callback();
      return;
    }

    this.isScriptRunning = true;

    logger.info(`Running script ${[this.script]}`);

    const args = [];
    if (this.tsconfig) {
      args.push('--project', this.tsconfig);
    }
    args.push(this.script);

    const process = spawn('ts-node', args, {stdio: 'pipe'});

    let scriptOutput = '';
    let scriptError = '';

    process.stdout.on('data', (data) => {
      scriptOutput += data.toString();
    });

    process.stderr.on('data', (data) => {
      scriptOutput += data.toString();
    });

    process.on('close', (code) => {
      this.isScriptRunning = false;
      console.log(scriptOutput);

      if (code !== 0) {
        logger.error(`Script exited with code ${code}`);
      } else {
        logger.info('Script executed successfully');
      }
      callback();
    });
  }
}

module.exports = ChangeDetectionPlugin;
