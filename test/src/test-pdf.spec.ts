import { comparePdfToSnapshot } from 'pdf-visual-diff';
import { expect } from 'chai';
import { join } from 'path';

describe('Visual regression test for pdf', () => {
    const options = {
        tolerance: 0.000001
    };

    it('Simple PDF', async () => {
        console.log(join(__dirname, 'outdir', 'test-pdf.pdf'))
        const x = await comparePdfToSnapshot(join(__dirname, '..', 'outdir', 'test-pdf.pdf'), __dirname, 'test-pdf', options);
        expect(x).to.be.true;
    });
});
