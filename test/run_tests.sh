#!/bin/sh

# Exit immediately if a command exits with a non-zero status
set -e

rm -rf outdir
mkdir outdir

# Define the log file
LOG_FILE="outdir/logs.json"

../sfo transform \
        --source "${PWD}/src/fixtures/templates/test-pdf/manuscript.json" \
        --target "${PWD}/outdir/test-pdf.pdf" \
        --template-dir="${PWD}/src/fixtures/templates" \
        --temp-path "${PWD}/outdir" \
        --font-dir "${PWD}/outdir/fonts" \
        --format pdf \
        --template=test-pdf \
        --force \
        --debug > "$LOG_FILE"

sleep 1

# Initialize a flag to track if there are error logs
error_found=0

# Read the file, filter out JSON formatted log messages, and process them
grep -E '^\{.*\}$' "$LOG_FILE" | python3 -c '
import sys
import json

errors = []
for line in sys.stdin:
    try:
        parsed = json.loads(line)
        level = parsed.get("level")
        message = parsed.get("message")
        if level and message:
            print(f"Level: {level}, Message: {message}")
            if level == "error":
                errors.append(message)
    except json.JSONDecodeError:
        continue

if errors:
    print("\nError messages found:")
    for error in errors:
        print(error)
    sys.exit(1)
'

# Check if the Python script exited with errors
if [ $? -eq 1 ]; then
  echo "Please check logs.json for more details."
  exit 1
else
  echo "No error logs found."
fi

# Check if the file exists
if [ ! -f "outdir/test-pdf.pdf" ]; then
  echo "Error: File outdir/test-pdf.pdf does not exist."
  exit 1
fi

mocha **/*.spec.ts --timeout 60000
