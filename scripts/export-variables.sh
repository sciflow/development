#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e

# ==============================================================================
# Script Description:
# This script is designed to be used in a GitLab CI pipeline. It retrieves the
# latest semantic version tag from the git repository and generates a semantic
# package version based on the branch and tag information. Specifically, it:
# 1. Fetches the latest semantic version tag.
# 2. Determines the current branch name.
# 3. Processes the branch name to create a branch alias.
# 4. Sets the semantic package version based on the branch and tag conditions.
#    - If the "release" tag is present and the branch is "main", the semantic
#      package version is set to the semantic version tag.
#    - Otherwise, it incorporates the project name, branch alias, and pipeline ID.
# 5. Exports the semantic version tag, package version, and branch alias for use
#    in subsequent CI pipeline steps.
# ==============================================================================

# Function to print an error message and exit
error_exit() {
  echo "ERROR: $1" >&2
  exit 1
}

# Get the latest semantic version tag
get_latest_tag() {
  local tag
  tag=$(git tag --points-at HEAD --sort=-version:refname 2>/dev/null | grep -E "^[0-9]+\.[0-9]+\.[0-9]+$" | head -n 1)
  if [ -z "$tag" ]; then
    tag=$(git describe --tags --match "[0-9]*.[0-9]*.[0-9]*" --abbrev=0 2>/dev/null || echo "0.0.0")
  fi
  echo "$tag"
}

# Check if the "release" tag is present
is_release_tag_present() {
  git tag --points-at HEAD | grep -q "^release$"
}

# Main script
main() {
  echo "Creating variable values in ${PWD}"
  SEMANTIC_VERSION_TAG=$(get_latest_tag) || error_exit "Failed to get the latest tag"

  # Set default values if not defined
  CI_PROJECT_NAME="${CI_PROJECT_NAME:-local}"
  [ "$CI_PROJECT_NAME" == "development" ] && CI_PROJECT_NAME="dev"
  CI_PIPELINE_ID="${CI_PIPELINE_ID:-0}"

  # Get current git branch if CI_COMMIT_REF_NAME is empty
  if [ -z "$CI_COMMIT_REF_NAME" ]; then
    CI_COMMIT_REF_NAME=$(git rev-parse --abbrev-ref HEAD) || error_exit "Failed to get the current git branch"
  fi

  # Process CI_COMMIT_REF_NAME for BRANCH_ALIAS
  BRANCH_ALIAS="${CI_COMMIT_REF_NAME}"
  [ "$BRANCH_ALIAS" == "development" ] && BRANCH_ALIAS="dev"
  BRANCH_ALIAS="${BRANCH_ALIAS:0:10}"  # Shorten to 10 chars if longer
  BRANCH_ALIAS="${BRANCH_ALIAS//\//-}"  # Replace / with -

  # Determine SEMANTIC_PACKAGE_VERSION
  if [ "$CI_COMMIT_REF_NAME" == "main" ] && is_release_tag_present; then
    SEMANTIC_PACKAGE_VERSION="${SEMANTIC_VERSION_TAG}"
  elif [ "$CI_COMMIT_REF_NAME" == "main" ]; then
    SEMANTIC_PACKAGE_VERSION="${SEMANTIC_VERSION_TAG}-${CI_PROJECT_NAME}-${CI_PIPELINE_ID}"
  else
    SEMANTIC_PACKAGE_VERSION="${SEMANTIC_VERSION_TAG}-${CI_PROJECT_NAME}-${BRANCH_ALIAS}-${CI_PIPELINE_ID}"
  fi

  # Export variables
  export SEMANTIC_VERSION_TAG
  export SEMANTIC_PACKAGE_VERSION
  export BRANCH_ALIAS

  # Print variables
  echo "SEMANTIC_VERSION_TAG=${SEMANTIC_VERSION_TAG}"
  echo "SEMANTIC_PACKAGE_VERSION=${SEMANTIC_PACKAGE_VERSION}"
  echo "BRANCH_ALIAS=${BRANCH_ALIAS}"
}

# Call the main function
main
