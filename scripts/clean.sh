#!/bin/bash
echo "This will completely reset your workspace"

read -p "Are you sure? This might take a while (y/N)" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo "removing old node_modules. Time to get some ☕"
    find . -maxdepth 3 -type d -name "node_modules" -exec rm -rf {} +
    echo "unlinking @sciflow libraries"
    rm -rf ~/.config/yarn/link/@sciflow
    rm -rf dist
else
    echo "Not removing anything"
fi




