#!/bin/sh

BASE_DIR=$(pwd)
: ${SCOPE:="@sciflow"}

export SCOPE

basedir=$(pwd)
packagedir=${basedir}/pkg-dist
echo "basedir:" $basedir
echo "packagedir:" $packagedir

mkdir $packagedir

cd $basedir/dist/libs

for dir in */ ; do
  if [ -d "$dir" ]; then
    project=${dir%/}
    echo "Processing library $project"
    cd "$dir" || exit

    node "$basedir/scripts/publish-packages-version.js" "$(pwd)"
    yarn pack --json --filename ${project}.tgz
    mv "${project}.tgz" $packagedir/

    if [ "$PUBLISH" != "false" ]; then
        if [ -n "$CI_PIPELINE_ID" ]; then
            echo "${SCOPE}:registry=https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/" > .npmrc
            echo "//${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}" >> .npmrc
            npm publish
        else
            echo "CI_PIPELINE_ID is not set. Skipping npm publish for $dir."
        fi
    fi
    
    cd ..
  fi
done

cd $basedir/app/dist/libs

for dir in */ ; do
  if [ -d "$dir" ]; then
    project=${dir%/}
    echo "Processing app library $project"
    cd "$dir" || exit

    node "$basedir/scripts/publish-packages-version.js" "$(pwd)"
    yarn pack --json --filename ${project}.tgz
    mv "${project}.tgz" $packagedir/

    if [ "$PUBLISH" != "false" ]; then
        if [ -n "$CI_PIPELINE_ID" ]; then
            echo "${SCOPE}:registry=https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/" > .npmrc
            echo "//${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}" >> .npmrc
            npm publish
        else
            echo "CI_PIPELINE_ID is not set. Skipping npm publish for $dir."
        fi
    fi
    
    cd ..
  fi
done

if [ "$PUBLISH" = "true" ]; then
  echo "Libraries published."
else
  echo "Libraries created but not published."
fi