#!/usr/bin/env node

const fs = require('fs');
const path = require('path');

const [,, packageJsonDir] = process.argv;
const { SEMANTIC_PACKAGE_VERSION } = process.env;

const updatePackageJson = () => {
  if (!SEMANTIC_PACKAGE_VERSION || SEMANTIC_PACKAGE_VERSION?.length < 5) {
    console.warn('Not updating package version since SEMANTIC_PACKAGE_VERSION was not provided');
    return;
  }
  console.log('Updating package', { packageJsonDir, SEMANTIC_PACKAGE_VERSION });
  const packageFile = path.join(packageJsonDir, 'package.json');
  
  // Read the existing package.json file
  fs.readFile(packageFile, 'utf8', (err, data) => {
    if (err) {
      console.error('Error reading package.json:', err);
      return;
    }

    // Parse the JSON data
    let packageData;
    try {
      packageData = JSON.parse(data);
    } catch (parseError) {
      console.error('Error parsing package.json:', parseError);
      return;
    }

    // Update the package name and version
    const version = packageData.version.split('.');
    packageData.version = SEMANTIC_PACKAGE_VERSION;

    // Convert the updated data back to JSON
    const updatedData = JSON.stringify(packageData, null, 2);
    console.log('Updated package', { version: packageData.version, name: packageData.name });

    // Write the updated data back to package.json
    fs.writeFile(packageFile, updatedData, 'utf8', (writeErr) => {
      if (writeErr) {
        console.error('Error writing package.json:', writeErr);
        return;
      }

      console.log('package.json has been updated successfully.');
    });
  });
};

updatePackageJson();