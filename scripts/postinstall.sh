#!/bin/sh

# keep this in sh since it needs to run in environments without bash

# exit on errors
set -e

# Function to check if a directory exists
directory_exists() {
    if [ -d "$1" ]; then
        return 0  # Directory exists
    else
        return 1  # Directory does not exist
    fi
}

# to undo:
rm -rf ~/.config/yarn/link/@sciflow
# yarn link @sciflow/export @sciflow/schema @sciflow/import @sciflow/tdk @sciflow/support @sciflow/cite @sciflow/tdk @sciflow/ui-components @sciflow/plugin

basedir=$(pwd)
echo $basedir

echo "Installing local copies of libraries in dist folder to node_modules"

libs="cite export import schema support plugin tdk"
for lib in $libs; do
    cd "$basedir/dist/libs/$lib"
    pwd
    yarn link
    cd "$basedir"
    yarn link "@sciflow/$lib"
done

# Angular libraries
angular_lib_baseDir="$basedir/app"

angular_lib="ui-components"
for lib in $angular_lib; do

    lib_path="$angular_lib_baseDir/dist/libs/$lib"

    cd "$angular_lib_baseDir"
    yarn build "$lib" # always build the library before linking for latest changes

    cd "$lib_path"
    pwd
    yarn link
    cd "$basedir"
    yarn link "@sciflow/$lib"
done

cd "$basedir/plugins" || exit
./build.sh

cd "$basedir"
./scripts/create-packages.sh

cd "$basedir"
