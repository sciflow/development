FROM gitpod/workspace-full:2024-12-11-07-51-54

RUN sudo apt-get -q --no-allow-insecure-repositories update \
  && DEBIAN_FRONTEND=noninteractive \
  sudo apt-get install --assume-yes --no-install-recommends \
  git \
  wget \
  cabextract \
  xz-utils \
  ca-certificates=* \
  locales \
  ca-certificates-java \
  openjdk-17-jre-headless \
  openjdk-17-jre \
  openjdk-17-jdk-headless \
  openjdk-17-jdk \
  pdftk \
  zlib1g-dev \
  openssh-client \
  tex-gyre \
  gdebi \
  fonts-stix \
  fonts-texgyre \
  fonts-lmodern \
  libcairo2 \
  libfreetype6 \
  libgif7 \
  libgomp1 \
  libjpeg-dev \
  libpixman-1-0 \
  libxml2 \
  graphicsmagick \
  inkscape \
  libjpeg8 \
  libavif-dev \
  libheif-dev \
  libpng-dev \
  libwebp-dev \
  libx11-dev \
  libvips-dev

RUN node --version | grep -E "^v22\." || (echo "Unsupported Node.js version" && exit 1)

# install pandoc 
RUN wget https://github.com/jgm/pandoc/releases/download/3.6/pandoc-3.6-1-amd64.deb && \
    sudo dpkg -i pandoc-3.6-1-amd64.deb && \
    sudo apt-get install -f && \
    rm pandoc-3.6-1-amd64.deb
