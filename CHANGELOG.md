# Changelog

## 2024.1

Our biggest release yet brings around 400 changes and improvements. This includes a lot of new configuration options for templates, including customizable headers and footers, bug fixes, and framework upgrades.

### Features

- Improved error handling for document import issues, including footnotes and images in headings.
- Enhanced figure caption detection.
- Individual styles for HTML (see the new `HtmlPage` component).
- Added DPI checks for web and print publications.
- Added missing fields in JATS XML.
- Improved EPUB export (available through the CLI).
- Introduced Handlebar header and footer templates.
- Added a ZIP snapshot with all assets for portability.
- Experimental support for TeX imports.
- Automatic detection of abstracts, keywords, and major headings based on natural language processing (NLP).
- CSL styles are now automatically loaded from [Zotero](https://www.zotero.org/styles) (requires server to make outgoing calls to zotero.org).

### Smaller Improvements and Fixes

- SVG handling.
- Equation layouts.
- Landscape figures.
- List break behavior.
- Cover page design for monographs.
- Support for blank pages.
- Support for page offsets.
- Improved font handling for locally provided fonts.

### Upgrades

- Angular 17 (including Material)
- Node 20
- Pandoc 3.1.12.1
- PagedJS 0.4.3

## 2023.1

This was our initial release.
