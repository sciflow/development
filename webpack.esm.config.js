const path = require('path');
const nodeExternals = require('webpack-node-externals');

// List of libraries to build
const librariesToBuild = ['schema'];

// Create a webpack configuration for each project in the list
const webpackConfigs = librariesToBuild.map(libName => {
  const externals = nodeExternals({
    additionalModuleDirs: [
      'node_modules',
      path.join(__dirname, `libs/${libName}`, "node_modules")
    ],
    allowlist: []
  });

  return {
    entry: `./libs/${libName}/src/index.ts`,
    output: {
      filename: 'index.mjs',
      path: path.resolve(__dirname, `dist/libs/${libName}`),
      libraryTarget: 'module',
    },
    externals: [externals],
    experiments: {
      outputModule: true,
    },
    resolve: {
      extensions: ['.ts', '.js'],
    },
    module: {
      rules: [
        {
          test: /\.ts$/,
          use: {
            loader: 'ts-loader',
            options: {
              configFile: path.resolve(__dirname, 'tsconfig-esm.json'),
            },
          },
          exclude: /node_modules/,
        },
      ],
    },
    externals: {
      react: 'react',
      'react-dom': 'react-dom',
    },
    mode: 'production',
    optimization: {
      minimize: false,
    },
  };
});

module.exports = webpackConfigs;