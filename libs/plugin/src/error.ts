export class PluginError extends Error {
    constructor(message: string, public payload: any) {
        super(message);

        // Set the prototype explicitly.
        Object.setPrototypeOf(this, PluginError.prototype);
    }

    toString() {
        return `${this.name}: ${this.message}`;
    }

    toJSON() {
        return { message: this.message, payload: this.payload };
    }
}