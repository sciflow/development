import { existsSync, readFileSync } from "fs";
import yaml from 'js-yaml';
import { basename, join } from "path";
import { sync } from "glob";
import { EditorState } from 'prosemirror-state';
import { Node } from 'prosemirror-model';
import { getPaths, setValue, slugify } from "@sciflow/export";
import { PatchFunction, PluginAction } from "./types";
import { PluginError } from "./error";
import { AnnotatedManuscriptResponse } from "@sciflow/schema";

/** Gets a value from an object if provided with a path (e.g. object.key.key2 ) */
export const getValueFromSchema = (path: string, object: any) => {
    if (!path || !object) { return undefined; }
    if (object?.properties?.[path]) { return object?.properties[path]; }
    const segments = path.split('.');
    if (segments.length === 0) { return undefined; }
    if (object?.properties?.[segments[0]] === undefined) { return undefined; }
    return getValueFromSchema(segments.slice(1, segments.length).join('.'), object?.properties[segments[0]]);
}


export const getValues = (jsonSchema: any) => {
    const paths = getPaths('', jsonSchema);
    const values = {};
    for (let path of paths) {
        const value = getValueFromSchema(path, jsonSchema);
        if (value?.default != undefined) {
            setValue(path, values, value.default);
        }
    }
    return values;
}

export const mapPlugin = (plugin) => {
    const name = plugin.manifest.metadata?.name;
    const triggers = plugin.manifest.triggers || [];
    const runners = plugin.manifest.runners || [];

    const scripts: any[] = [];
    if (plugin.manifest.spec.tag) {
        scripts.push({ name: 'tag', url: join('/plugins/script/', name, plugin.manifest.spec.tag.source.replace('dist/', '')) });
    }

    if (plugin.manifest.spec.patch) {
        scripts.push({ name: 'patch', url: join('/plugins/script/', name, plugin.manifest.spec.patch?.source?.replace('dist/', '')) });
    }

    return {
        id: slugify(name),
        name,
        ...plugin.manifest.metadata,
        triggers,
        runners,
        scripts
    };
}

export const loadPluginsFromDisk = async (PLUGIN_DIRS: string, { logger }) => {

    const dynamicImport = async (path: string) => {
        try {
            const loadedModule = await import(/* webpackIgnore: true */ path);
            return loadedModule;
        } catch (e) {
            debugger;
            logger.error('Could not load plugin module', { message: e.message, path });
        }
    }

    let instances: any[] = [];
    for (let PLUGIN_DIR of PLUGIN_DIRS?.split(',').map(s => s.trim()).filter(s => s?.length > 0) || []) {
        if (!PLUGIN_DIR) { continue; }
        const pluginDir = PLUGIN_DIRS && existsSync(PLUGIN_DIR) ? PLUGIN_DIR : undefined;
        if (pluginDir) {
            let plugins = sync(`${pluginDir}/*/plugin.yml`);
            for (let plugin of plugins) {
                if (existsSync(plugin)) {
                    const manifestFile = readFileSync(plugin, 'utf-8');
                    const manifest = yaml.load(manifestFile);
                    const moduleLocation = plugin.replace('/plugin.yml', '');
                    let tag, patch;
                    if (manifest.spec.patch && manifest.spec.patch.type === 'esm') {
                        if (existsSync(join(moduleLocation, manifest.spec.patch.source))) {
                            try {
                                patch = await dynamicImport(join(moduleLocation, manifest.spec.patch.source));
                            } catch (e) {
                                debugger;
                                logger.error('Could not load plugin module', { message: e.message, type: 'patch', manifest });
                            }
                        }
                    }
                    if (manifest.spec.tag && manifest.spec.tag.type === 'esm') {
                        if (existsSync(join(moduleLocation, manifest.spec.tag.source))) {
                            try {
                                tag = await dynamicImport(join(moduleLocation, manifest.spec.tag.source));
                            } catch (e) {
                                logger.error('Could not load plugin module', { message: e.message, type: 'tag', manifest });
                            }
                        }
                    }

                    const id = manifest.metadata?.name || basename(moduleLocation);
                    if (patch || tag) {
                        instances.push({
                            id,
                            manifest,
                            tag,
                            patch,
                            priority: manifest.priority || 0
                        });
                    }
                }
            }
        }
    }

    instances = instances.sort((a, b) => b.priority - a.priority); // higher comes first
    return instances;
}

export const tag = async (event: string, input: { state: EditorState; manuscript: any; stats?: any; intelligence?: any; }, { plugins, logger }): Promise<{ state?: EditorState; manuscript?: any; logs: any[]; }> => {
    let state = input.state;
    let manuscript = input.manuscript;
    let logs: any[] = [];
    for (let plugin of plugins) {
        try {
            if (typeof plugin.tag?.tag === 'function') {
                let result = plugin.tag.tag({ state, manuscript: input.manuscript, stats: input.stats, intelligence: input.intelligence });
                if (typeof result.then === 'function') { result = await result; }
                if (result.state) { state = result.state; }
                if (result.logs) { logs = [...logs, ...result.logs]; }
            }
        } catch (e) {
            debugger;
            throw new PluginError('Could not execute ' + plugin.id + ' plugin', { message: e.message });
        }
    }

    return {
        manuscript,
        state,
        logs
    }
}

/**
 * Gets all available actions given the plugins and the current document.
 */
export const getActions = async (state: EditorState, event: string, { plugins, logger }) => {
    let actions: { actions: PluginAction[]; plugin: any }[] = [];
    for (let plugin of plugins) {
        if (!plugin?.patch?.actions) {
            console.error('No patch actions', { plugin });
            throw new Error('Expected plugin to have patch actions');
        }
        if (plugin.patch.actions && typeof plugin.patch.actions === 'function') {
            try {
                let pluginActions = plugin.patch.actions({ state });
                if (typeof pluginActions.then === 'function') {
                    pluginActions = await pluginActions;
                }
                if (pluginActions && pluginActions.length > 0) {
                    actions.push({
                        actions: pluginActions,
                        plugin
                    });
                }
            } catch (e) {
                logger.error('Could not get plugin actions for ' + plugin.id, { message: e.message, event });
                debugger;
                actions.push({
                    plugin,
                    actions: [{
                        id: 'plugin-actions-error-' + plugin.id,
                        pluginId: plugin.id,
                        message: 'Could not retrieve plugin actions for ' + plugin.name
                    }]
                });
            }
        }
    }
    return actions;
}

export const patch = async (pluginActions: { plugin: any, actions: PluginAction[] }[], input: { state: EditorState; manuscript: any; intelligence?: AnnotatedManuscriptResponse }, { logger }): Promise<{ state?: EditorState; manuscript?: any; logs: any[]; }> => {
    let { state, manuscript } = input;
    let logs: any[] = [];
    for (let { plugin, actions } of pluginActions) {
        try {
            // get the schema default values for automated executions
            const defaults = actions.filter(action => action.pluginId === plugin.id || !action.pluginId).map(action => ({
                ...action,
                values: getValues(action.schema)
            }));

            if (!plugin.patch) { continue; }
            console.time('Running plugin ' + plugin.id);
            const patchFn: PatchFunction<{ state?: EditorState; manuscript?: any; intelligence?: AnnotatedManuscriptResponse; }> = plugin.patch.patch;
            let result = patchFn({ state, manuscript, intelligence: input.intelligence }, defaults);
            if (typeof (result as any)?.then === 'function') {
                result = await (result as any);
            }
            console.timeEnd('Running plugin ' + plugin.id);

            if (result?.manuscript) {
                manuscript = {
                    ...manuscript,
                    ...result.manuscript
                };
            }

            if (result?.tr) {
                // update the state based on each plugin so we can hand fresh data to the next one
                result.tr.doc.check();
                try {
                    state = state.apply(result.tr);
                } catch (e) {
                    console.error('Could not apply transaction', { state, tr: result.tr });
                    debugger;
                }
            }

            logs = [...logs, ...(result?.logs?.map(l => ({
                type: 'plugin-log',
                level: 'info',
                id: slugify(plugin.id) + '-' + l.id,
                payload: {
                    pluginId: plugin.id,
                    ...l
                },
                event: {
                    pluginId: plugin.id,
                    trigger: 'document-enriched',
                }
            })) || [])];
            logger.info('Plugin completed ' + plugin.id, logs);

            // TODO error handling
        } catch (e) {
            logger.error('Could not run plugin', { plugin: plugin.id, message: e.message, stack: e.stack?.split('\n').slice(0, 2).join('') });
            logs.push({
                id: 'plugin-excution-error',
                name: 'Could not run ' + slugify(plugin.id),
                payload: {
                    pluginId: plugin.id,
                    message: e.message,
                    skippedActions: actions.length
                }
            });
            debugger;
        }
    }

    return {
        state,
        manuscript,
        logs
    };
}