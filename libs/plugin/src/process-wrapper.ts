import { fromJSON } from "@sciflow/schema";
import { EditorState } from "prosemirror-state";
import { getActions, patch, PluginError, tag } from './';

/**
 * Processes JSON using the plugins.
 */
export const processPluginsByEvents = async (manuscript, { schema, docxStats, intelligence, events = ['document-enriched'], logger, plugins }) => {
    const doc = fromJSON('manuscript')(manuscript.document);
    let logs: any[] = [];

    logger.info('Processing events', { events });

    let state = EditorState.create({
        doc,
        schema
    });

    // run plugins on SciFlow JSON
    for (let event of events) {
        const pluginsForEvent = plugins.filter(plugin => {
            if (plugin.manifest.triggers.some(t => t.event === event)) { return true; }
            return false;
        })
        logger.info('Processing event', { events, plugins: pluginsForEvent.map(p => p.id)});
        console.time('Running ' + event);
        let pluginRun: any = { event };
        let stage = 'tag';
        try {
            const tagResult = await tag(event, { state, manuscript, stats: docxStats, intelligence }, { plugins: pluginsForEvent, logger });
            if (tagResult?.state) {
                state = tagResult.state;
                manuscript = tagResult.manuscript;
                logs = [...logs, ...(tagResult.logs || [])];
                pluginRun.tag = true;
            }
            stage = 'actions';
            const passActions = await getActions(state, event, { plugins: pluginsForEvent, logger });
            if (passActions) {
                pluginRun.actions = passActions.length;
            }
            const passResult = await patch(passActions, { state, manuscript, intelligence }, { logger });
            stage = 'patch';
            if (passResult.state) {
                state = passResult.state;
                pluginRun.patch = true;
            }
            if (passResult.manuscript) {
                manuscript = passResult.manuscript;
            }
            logs = [...logs, ...(passResult.logs || [])];
        } catch (e: any) {
            logger.error('Could not get plugin actions for ' + event, { event, stage, message: e.message });
            throw new PluginError('Could not process plugin actions', { event, message: e.message, stage, reason: e.payload });

        }
        console.timeEnd('Running ' + event);
        console.table(pluginRun);
    }

    manuscript.document = state.doc.toJSON();
    return { manuscript, logs };
}