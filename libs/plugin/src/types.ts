import { EditorState, Transaction } from 'prosemirror-state';
import { Node } from 'prosemirror-model';

export type PatchFunction<InputData> = (input: InputData, userInputs: any[]) => {
    tr?: Transaction;
    logs: any[];
} & Partial<InputData>;


export interface PluginAction {
    /** A deterministic unique (functional) id for the action */
    id?: string;
    type?: string;
    pluginId?: string;
    /** @deprecated use message instead */
    name?: string;
    message?: string;
    nodeId?: string;
    /** A JSON schema for any needed input data */
    schema?: any;
    blocking?: boolean;
    [key: string]: any;
    plugin?: any;
    tag?: { key: string; value: any; }
}

export interface PluginMessage extends PluginAction {
    level: 'verbose' | 'info' | 'warning' | 'error' | 'fatal';
    /** The message text */
    message: string;
    /** Additional details of the message */
    payload: object;
}

export interface TransportManuscriptFile {
    authors: any[];
    /** references as CSL */
    references?: any[];
    /** A list of all files used in the document (e.g. to replace thumbnails with large resolution images in export) */
    files?: any[];
    metaData?: object;
}
