import { download } from 'google-fonts-helper';
import { join } from 'path';
import { createHash } from 'crypto';
import { URLSearchParams, URL } from 'url';

/**
 * Creates a hash from the provided font URL and target
 */
export const createFontHash = (fontUrl, target = 'web') => {
    if (!fontUrl) { return '_empty'; }
    try {
        const params = new URL(fontUrl).searchParams;
        const fontFamily = [...params.getAll('family')].slice(0, 2).map(f => f.split(':')?.[0]?.replaceAll(' ', '')?.substring(0, 5));
        const families = fontFamily.map(f => encodeURIComponent(f)).join('_');
        const hash = createHash('md5').update(params.toString() + target).digest('hex');
        return `${families}${hash}`;
    } catch {
        return `${createHash('md5').update(fontUrl).digest('hex')}_${target}`;
    }
};

/**
 * Downloads a css file and all needed font files. If a target other than web is provided
 * a header will be sent to Google, resulting in ttf files instead of web fonts (e.g. woff2)
 */
export const downloadFont = async (url, { path, urlPrefix, target = 'web' }): Promise<{ message: string; payload: any; }> => {
    const headers = target === 'web' ? undefined : {
        'user-agent': [
            'CustomRuntime/1.0.0'
        ].join(' ')
    };
    const options = {
        base64: false,
        overwriting: false,
        outputDir: path,
        stylePath: join(path, 'fonts.scss'),
        fontsDir: path,
        fontsPath: urlPrefix
    };
    const downloader = download(url, target === 'web' ? options : {
        ...options,
        headers
    });
    const result = await downloader.execute();
    return { message: `Downloaded font`, payload: { headers } };
}
