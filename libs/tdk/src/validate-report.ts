import {stringify} from 'csv-stringify';
import {createWriteStream, existsSync, mkdirSync, writeFileSync} from 'fs';
import {basename, dirname, join} from 'path';
import {TestCase, TestSuite, TestSuites, createJUnitXMLReport} from '../../../build-tools/report';
import {validateYamlFile} from './validate';

export async function generateValidationReport(
  templatePaths: string[],
  componentPaths: string[] = [],
  outputPath: string = 'out',
): Promise<number> {
  const summary: {
    name: string;
    tests: number;
    ok: boolean;
    failures: number;
    errors: number;
    skipped: number;
  }[] = []; // overall summary

  for (const templatePath of templatePaths) {
    let testSuites: TestSuite[] = [];
    let failures = 0,
      errors = 0,
      skipped = 0;

    const parentDir = basename(dirname(templatePath));
    const outDir = join(outputPath, parentDir);

    if (!existsSync(outDir)) {
      mkdirSync(outDir, {recursive: true});
    }

    let start = Date.now();
    let validationResult: {
      file: string;
      isValid: boolean | null;
      results: {
        kind: string;
        isValid: boolean | null;
        errors: string | object[] | undefined;
      }[];
    } | null;

    try {
      validationResult = await validateYamlFile(templatePath, componentPaths);

      if (!validationResult) {
        throw new Error(`No validation result for file: ${templatePath}`);
      }

      const testCases: TestCase[] = validationResult.results.map((result) => {
        const testCase: TestCase = {
          classname: `${parentDir}`,
          name: `Validation passed: ${result.kind}`,
          file: templatePath,
          time: (Date.now() - start) / 1000,
        } as TestCase;

        if (result.isValid === false) {
          testCase.name = `Validation failed: ${result.kind}`;
          testCase.failureMessage = `Validation failed for ${result.kind}`;
          testCase.failureDescription = JSON.stringify(result.errors, null, 2);
          failures++;
          errors += result.errors?.length ?? 0;
        }

        if (result.isValid === null) {
          testCase.name = `Validation skipped: ${result.kind}`;
          testCase.skipped = true;
          testCase.description = result.errors?.toString();
          skipped++;
        }

        return testCase;
      });

      const testSuite: TestSuite = {
        name: `${parentDir}`,
        tests: testCases.length,
        failures,
        errors,
        skipped,
        testCases: testCases,
      };

      testSuites.push(testSuite);

      const junitReport: TestSuites = {testSuites};
      const junitReportName = join(outDir, 'junit.xml');
      const report = createJUnitXMLReport(junitReport);
      if (!report) {
        throw new Error('Could not create report');
      }

      writeFileSync(junitReportName, report);

      const hasFailures = validationResult.results.some((result) => result.isValid === false);
      if (hasFailures) {
        writeFileSync(join(outDir, 'TESTS_FAILED'), 'See junit.xml');
      }

      summary.push({
        name: parentDir,
        tests: testSuite.tests,
        ok: !hasFailures,
        failures,
        errors,
        skipped,
      });

      // Write the full validation result to a JSON file
      writeFileSync(
        join(outDir, 'validation-result.json'),
        JSON.stringify(validationResult, null, 2),
      );
    } catch (e: any) {
      console.error('Could not process', templatePath, ': ', e.message);

      testSuites.push({
        name: `Validation of ${templatePath}`,
        tests: 1,
        failures: 1,
        errors: 0,
        skipped: 0,
        testCases: [
          {
            classname: templatePath,
            name: 'Execution failed: ' + e.message,
            file: templatePath,
            time: (Date.now() - start) / 1000,
            failureMessage: 'Execution failed with: ' + e.message,
            failureDescription:
              'The validation failed to execute. Check log for details. ' + e.stack,
          },
        ],
      });

      throw new Error(e);
    }
  }

  const summaryForCsv = summary.map((item) => ({
    ...item,
    ok: item.ok.toString(),
  }));

  stringify(summaryForCsv, {
    header: true,
  }).pipe(createWriteStream(join(outputPath, 'validation-report.csv')));

  const overallFailure: number = summary.find((value) => value.ok === false) ? 1 : 0;

  return overallFailure;
}
