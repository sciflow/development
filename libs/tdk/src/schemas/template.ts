import {Structure} from './structure';

type runnerType = 'princexml' | 'pagedjs';

interface EnvironmentOptions {
  label: string;
  counterLabel: string;
}

/**
    The template definition version
    @additionalProperties false
    @required true 
*/
interface Template {
  kind: string;
  page?: string;
  spec: any;
  // metaData?: {
  //     /**
  //     The template definition version
  // */
  //     version: 'alpha';
  //     /** The template title */
  //     title: string;
  //     /** A short text describing the template */
  //     description: string;
  //     /** The publication type */
  //     type: 'article' | 'thesis';
  //     /** A readme file (may contain markdown) */
  //     readme: string;
  //     /** The exports compatible with this template */
  //     runners: runnerType[];
  //     /** The structure of the template */
  //     structure?: Structure;
  //     /** Meta data */
  //     metaData: unknown;

  //     /** Typography */
  //     typography?: {
  //         fontSize?: string;
  //         fontFamily?: string;
  //     }

  //     layout?: {
  //         [environment: string]: EnvironmentOptions
  //     }
  // }
}

export interface YamlDocument {
  kind: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  spec: any;
}
