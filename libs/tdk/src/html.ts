import { fetchFromZotero, getDefaults } from '@sciflow/cite';
import { ExportOptions, createHTMLZip } from '@sciflow/export';
import { DocumentSnapshot } from '@sciflow/schema';
import { createLogger, format, transports } from 'winston';
let { styleXML, localeXML } = getDefaults();
const { LOG_LEVEL = 'info' } = process.env;
const transactionLogger = createLogger({
    format: format.json(),
    defaultMeta: { service: 'tdk-logger' },
    transports: [
        new transports.Console({ level: LOG_LEVEL })
    ]
});

/**
 * Renderer function for the template (default export).
 * Splits a document consisting of a single chapter into multiple parts for more advanced processing.
 * @returns a zip buffer with the manuscript.html and all files inside.
 */
export const defaultHTMLTemplate = async (documentSnapshot: DocumentSnapshot, template, opts: {
    debug?: boolean;
    inline: boolean;
    scripts: any[];
    runner?: string;
    citationStyle?: string;
    locale?: string;
    logging?: any;
}): Promise<Buffer> => {
    documentSnapshot.parts = documentSnapshot.parts.map(part => ({
        ...part,
        id: part.partId || part.id
    }));

    // default to provided style, or the template style or the default
    const citationStyleId = opts?.citationStyle || template.configuration?.citationStyle?.id || 'apa';
    const locale = opts?.locale || template.configuration?.language || 'en-US';
    styleXML = await fetchFromZotero(citationStyleId) || styleXML;
    const citationStyleXML = { [locale]: styleXML };

    const options: ExportOptions = {
        configurations: template.configurations,
        metaData: documentSnapshot.metaData,
        metaDataSchema: template.metaData,
        customTemplateComponents: template.customTemplateComponents,
        customRenderers: template.customRenderers,
        stylePaths: template.stylePaths || [],
        assetBasePaths: [], // TODO
        inline: opts?.inline,
        scripts: opts?.scripts || [],
        citationStyleXML,
        localeXML: { [locale]: localeXML }, // TODO load different locales
        logging: opts.logging || {
            transactionId: 'tdk',
            logger: transactionLogger
        },
        runner: opts?.runner,
        debug: opts?.debug
    };

    const file = await createHTMLZip(documentSnapshot, options);

    return file;
}
