import {
  GenericConfiguration,
  decamelize,
  getConfigName,
  readYAMLDocumentsFromFile,
  validAtKeywords,
} from '@sciflow/export';
import Ajv, {AnySchema, ValidateFunction} from 'ajv';
import {existsSync} from 'fs';
import * as fs from 'fs/promises';
import {sync} from 'glob';
import * as path from 'path';
import {createLogger, format, transports} from 'winston';
const { LOG_LEVEL = 'info' } = process.env;
const logger = createLogger({
  level: LOG_LEVEL,
  defaultMeta: {service: 'validate'},
  format: format.combine(
    format.timestamp(),
    format.errors({stack: true}),
    format.printf((info) => `${info.timestamp} [${info.level}]: ${info.message}`),
  ),
  transports: [new transports.Console({ level: LOG_LEVEL })],
});

/**
 * Loads a file and parses its contents based on the file extension.
 * Supports .json, .yml and .yaml extensions.
 *
 * @param {string} filePath - The path to the file to load.
 * @returns {Promise<object>} The parsed contents of the file.
 * @throws Will throw an error if the file type is unsupported or if there is an error loading the file.
 */
export const loadFile = async (filePath: string) => {
  try {
    const fileContents = await fs.readFile(filePath, 'utf8');

    if (path.extname(filePath) === '.json') {
      return JSON.parse(fileContents);
    } else if (path.extname(filePath) === '.yaml' || path.extname(filePath) === '.yml') {
      return readYAMLDocumentsFromFile(filePath);
    } else {
      throw new Error('Unsupported file type');
    }
  } catch (error) {
    logger.error(`Error loading file "${filePath}": ${error?.message}`);
    throw error;
  }
};

/**
 * Validates a YAML file containing template configurations against the corresponding JSON schemas.
 *
 * @param {string} filePath - The path to the YAML file to validate.
 * @param {string[]} componentPaths - An optional array of paths to search for the schema files.
 * @returns {Promise<{kind: string; isValid: boolean; errors: string | object[] | undefined} | null>} The validation result, indicating whether the configurations are valid and any errors found.
 * @throws Will throw an error if there is an issue loading or validating the file.
 */
export const validateYamlFile = async (
  filePath: string,
  componentPaths: string[] = [],
): Promise<{
  file: string;
  isValid: boolean | null;
  results: {kind: string; isValid: boolean | null; errors: string | object[] | undefined}[];
} | null> => {
  let validatedCount = 0;
  let skippedCount = 0;

  try {
    const templateConfigurations: GenericConfiguration<unknown>[] = await loadFile(filePath);

    if (!templateConfigurations || templateConfigurations.length === 0) {
      throw new Error(`Template file at "${filePath}" has no configurations.`);
    }

    const results: Array<{
      kind: string;
      isValid: boolean | null;
      errors: string | object[] | undefined;
    }> = [];

    logger.info(`Validating template path: ${filePath}`);

    for (const configuration of templateConfigurations) {
      const name = getConfigName(configuration);

      validatedCount++;
      logger.info(
        `Validating ${validatedCount} out of ${templateConfigurations.length} documents. Kind: ${name}`,
      );

      const {kind, isValid, errors} = await validateConfiguration(configuration, componentPaths);
      let formattedErrors = errors;

      if (isValid === null) {
        logger.warn(`Validation skipped for '${name}'. Reason: ${JSON.stringify(errors)}`);
        skippedCount++;
      }

      if (isValid === false && errors) {
        if (Array.isArray(errors)) {
          formattedErrors = errors.map((error) => {
            return {
              name,
              templatePath: error?.['dataPath'] ? 'spec' + error?.['dataPath'] : 'spec',
              schemaPath: error?.['schemaPath'],
              params: {...error?.['params']},
              message: error?.['message'],
            };
          });
        }

        const errorDetails = JSON.stringify(formattedErrors, null, 2);
        const errorLogMsg: string = `Validation failed for document kind "${kind}" with errors: ${errorDetails}\n${JSON.stringify({documentPath: filePath}, null, 2)}`;
        logger.error(errorLogMsg);
      }

      results.push({kind: name, isValid, errors: formattedErrors});
    }

    logger.info(
      `Completed validation. ${validatedCount - skippedCount} out of ${templateConfigurations.length} configuration(s), skipped ${skippedCount} documents in "${filePath}".`,
    );

    const validResults = results.filter((result) => result.isValid !== null);

    if (validResults.length === 0) {
      // If all results were skipped (isValid is null), overall result is null
      return {file: filePath, isValid: null, results};
    } else {
      // Otherwise, overall result is true only if all non-null results are true
      const overallValidity = validResults.every((result) => result.isValid === true);
      return {file: filePath, isValid: overallValidity, results};
    }
  } catch (e) {
    logger.error(`An error occurred while validating "${filePath}" - ${e}`);
    throw new Error(e);
  }
};

/**
 * Validates a single configuration document against its corresponding JSON schema.
 *
 * @param {GenericConfiguration<unknown>} configuration - The configuration document to validate.
 * @param {string[]} componentPaths - An optional array of paths to search for the schema files.
 * @returns {Promise<{kind: string; isValid: boolean | null; errors?: string | object[]}>} The validation result, indicating the document kind, whether it is valid, and any errors found.
 * @throws Will throw an error if the document kind or spec is missing or if there is an issue loading the schema file.
 */
export const validateConfiguration = async (
  configuration: GenericConfiguration<unknown>,
  componentPaths: string[] = [],
): Promise<{
  kind: string;
  isValid: boolean | null;
  errors?: string | object[];
}> => {
  const kind: string = configuration?.kind || '';
  const componentSchema: AnySchema | null = await loadCompiledFile(kind, componentPaths);

  if (!componentSchema) {
    return {
      kind,
      isValid: null,
      errors: [{message: `The schema could not be found for "${kind}".`}],
    };
  }

  const ajv = new Ajv({allErrors: true, strict: true});

  for (let keyword of validAtKeywords) {
    ajv.addKeyword(keyword); // Add custom keywords to Ajv instance
  }

  const validate = ajv.compile(componentSchema);
  const isValid = validate(configuration) as boolean;

  return {
    kind,
    isValid,
    errors: validate.errors || [],
  };
};

const loadCompiledFile = async (
  configKind: string,
  componentPaths: string[],
): Promise<AnySchema | null> => {
  const baseDir = __dirname;
  const defaultComponentPath: string = path.resolve(baseDir, '../../../dist/libs/export/components');
  const componentPath = resolveComponentPath(configKind, componentPaths, defaultComponentPath);

  if (!componentPath) {
    logger.warn(
      `The schema could not be found for "${configKind}".\nSearched paths:\n${componentPaths}\n${defaultComponentPath}`,
    );
    return null;
  }

  try {
    const jsonSchema: AnySchema = await loadFile(componentPath);
    return jsonSchema;
  } catch (e) {
    logger.warn(`The file does not contain properties for validation "${componentPath}"`);
    return null;
  }
};

/**
 * Resolves the path to a schema file by searching through the provided component paths and default paths.
 *
 * @param {string} kind - The ID of the schema to resolve.
 * @param {string[]} componentPaths - An array of paths to search for the schema files.
 * @param {string} defaultComponentPath - The default path to search for the schema files.
 * @returns {string | null} The resolved path to the schema file if found, otherwise undefined.
 */
const resolveComponentPath = (
  kind: string,
  componentPaths: string[],
  defaultComponentPath: string,
): string | null => {
  const decamelizeSlug = decamelize(kind);

  // Resolve the component paths to absolute paths
  const resolvedComponentPaths = componentPaths.map((p) => path.resolve(p));

  // Search through supplied component paths in reverse order (latest element takes priority)
  for (let i = resolvedComponentPaths.length - 1; i >= 0; i--) {
    const customPath = resolvedComponentPaths[i];
    const glob: string = `${customPath}/**/${decamelizeSlug}.schema.json`;
    const potentialPaths: string[] = sync(glob);
    if (potentialPaths.length > 0 && existsSync(potentialPaths[0])) {
      return potentialPaths[0];
    }
  }

  // If not found in custom paths, use glob.sync for the default component path
  const defaultPaths = sync(`${defaultComponentPath}/**/${decamelizeSlug}.schema.json`);

  if (defaultPaths.length > 0 && existsSync(defaultPaths[0])) {
    return defaultPaths[0];
  }

  return null;
};
