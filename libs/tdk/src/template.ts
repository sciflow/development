import { Font, FontConfiguration, GenericConfiguration, getComponentSchema, patchSchema, readYAMLDocumentsFromFile, sameConfigurationAndLocales, ConfigurationConfiguration, decamelize, buildTemplateSchema } from '@sciflow/export';
import { listObjects, s3ToStream } from '@sciflow/support';
import { createHash } from 'crypto';
import fs, { existsSync, readFileSync } from 'fs';
import { Configuration } from 'libs/export/src/html/components/configuration/configuration.component';
import { dirname, join, resolve } from 'path';
import { dirSync } from 'tmp';
import * as ts from "typescript";
import { NodeVM } from 'vm2';
import winston from 'winston';
import { createFontHash, downloadFont } from './fonts';
import { JSONSchema7 } from 'json-schema';
const { LOG_LEVEL = 'info' } = process.env;
const logger = winston.createLogger({ format: winston.format.json(), transports: new winston.transports.Console({ level: LOG_LEVEL }), level: LOG_LEVEL });

/**
 * Reads all files from a directory.
 */
export const readFiles = (path: string): string[] => {
    let assetPaths: string[] = [];
    if (!fs.existsSync(join(path))) { return assetPaths; }
    const assetFiles: fs.Dirent[] = fs.readdirSync(path, { withFileTypes: true });
    for (let assetFile of assetFiles) {
        if (assetFile.isDirectory()) {
            assetPaths = [...assetPaths, ...readFiles(join(path, assetFile.name))];
        } else {
            assetPaths.push(join(path, assetFile.name));
        }
    }

    return assetPaths.flat();
};

/**
 * Reads a template from disk.
 * @param templateSource the location on disk where templates are stored
 * @param templateSlug the template slug
 * @returns the template configurations and assets
 */
export const loadTemplateFromDisk = (templateSource: string, templateSlug: string) => {
    if (templateSource && existsSync(templateSource)) {
        const templatePath = join(templateSource, templateSlug);
        if (!existsSync(templatePath)) { return null; }
        const assets = readFiles(templatePath);

        const configurationPath = assets.find(asset => asset.endsWith('configuration.sfo.yml') || asset.endsWith('configuration.yml'));
        if (!configurationPath) { return null; }

        const configurations: GenericConfiguration<unknown>[] = readYAMLDocumentsFromFile(configurationPath);
        const configuration = configurations.find(c => c.kind === Configuration.name) as GenericConfiguration<ConfigurationConfiguration>;
        if (!configuration) { return null; }

        // hide the configuration in the UI
        if (!configuration.ui?.showTo) { configuration.ui = { showTo: [] }; }

        if (!configuration.spec?.assets) { configuration.spec.assets = []; }
        const assetFile = assets.find(path => path.endsWith('styles.scss'));
        let stylePaths: any[] = [];

        if (assetFile) {
            configuration.spec?.assets.push({ id: 'asset-styles', type: 'asset', path: assetFile.replace(templatePath, '.'), inline: true });
            stylePaths = [{ path: 'styles.scss', filedId: 'asset-styles', position: 'start' }];
        }

        configuration.spec.assets = configuration.spec?.assets.map(asset => {
            if (asset.inline && asset.path?.startsWith('./')) {
                const path = join(templatePath, asset.path);
                if (existsSync(path)) {
                    asset.content = readFileSync(path, 'utf-8');
                }
            }
            return asset;
        });

        if (configurations?.length > 0) {
            if (!configurations.some(c => c.kind === 'Environment')) {
                configurations.push({
                    kind: 'Environment',
                    spec: {
                        environments: {}
                    }
                });
            }
        }

        return {
            configuration,
            configurations,
            stylePaths,
            assets
        }

    } else {
        logger.warn('Trying to fetch a template without a source directory', { templateSlug, templateSource });
    }
}

/**
 * Loads a template from a provided path or S3 storage.
 * @deprecated this will be replaced with loadTemplateFromDisk (which is not feature complete yet)
 */
export const loadTemplate = async ({
    projectId, templateId
}, opts: {
    /** The directory where templates are being stored */
    templateDir,
    /** The path to the template components */
    componentPath,
    /** The local disc location of the fonts */
    fontPath,
    /** The prefix to where fonts are served, e.g. /fonts/ or https://url/fonts/ */
    fontUrlPrefix,
    /** Configurations that should be injected on top of the ones configured in the templaate */
    additionalConfigurations?,
    /** a base url to reach external files */
    baseUrl?: string,
    runner?: string
} = {
            templateDir: process.env.TEMPLATE_SOURCE,
            fontPath: process.env.FONT_PATH,
            componentPath: undefined,
            fontUrlPrefix: process.env.FONT_URL_PREFIX,
            additionalConfigurations: [],
            runner: undefined
        }) => {

    let customTemplateDirectory: any[] = [];
    if (projectId?.length > 0) {
        // read custom templates from the project directory
        customTemplateDirectory = await listObjects(projectId);
    }

    let assets: string[] = [];
    let templateDirPath = join(opts.templateDir, templateId);
    const dirObj = dirSync({ unsafeCleanup: true });
    if (templateId?.length > 0) {
        // try to find the template in the shared folder
        if (opts.templateDir) {
            logger.info('Loading template from file system', templateId);
            if (fs.existsSync(join(opts.templateDir, templateId))) {
                assets = readFiles(templateDirPath);
            }
        } else {
            logger.info('Loading template from s3', templateId);
            // TODO this logic should be moved to a shared lib to be used by all runners
            for (let asset of customTemplateDirectory
                .filter((a) => a.Key.startsWith(`${projectId}/exports/${templateId}`))) {
                try {
                    const assetFileName = asset.Key.replace(`${projectId}/exports/${templateId}/`, '');
                    const assetFile = join(dirObj.name, 'exports', templateId, assetFileName);
                    fs.mkdirSync(dirname(assetFile), { recursive: true });
                    const assetStream = fs.createWriteStream(assetFile);
                    await s3ToStream({ Key: asset.Key }, assetStream);
                    await new Promise((resolve) => assetStream.on('finish', resolve));
                    assets.push(assetFile);
                } catch (e: any) {
                    console.error('Could not access file', e.message, asset.Key);
                }
            }
        }

        let templateScript = assets.find(path => path.endsWith('template.tsx'));
        let render;
        let customRenderers = {};
        let stylePaths: any[] = [{ path: 'styles.scss', position: 'start' }];
        let customTemplateComponents = [];

        if (templateScript) {
            const script = fs.readFileSync(templateScript, 'utf-8');
            // TODO check for possible exploits / need of an additional sandbox
            // a better way might be to transpile using tsc before and then npm installing deps
            let result = ts.transpileModule(script, {
                compilerOptions: {
                    module: ts.ModuleKind.CommonJS,
                    target: ts.ScriptTarget.ES2020,
                    lib: ['DOM'],
                    jsx: ts.JsxEmit.React,
                    reactNamespace: 'TSX'
                }
            });
            // FIXME restrict this for security reasons
            // TODO limit max run time
            const root = [
                resolve(__dirname),
                resolve(__dirname, '../..', 'node_modules'),
                resolve(__dirname, '../../..', 'node_modules')
            ];
            const vm = new NodeVM({
                require: {
                    external: ['@sciflow/*'],
                    root,
                    resolve: (moduleName: string) => {
                        if (fs.existsSync(resolve(__dirname, '../../..', 'node_modules', moduleName))) {
                            return resolve(__dirname, '../../..', 'node_modules', moduleName);
                        } else if (fs.existsSync(resolve(__dirname, '../..', 'node_modules', moduleName))) {
                            return resolve(__dirname, '../..', 'node_modules', moduleName);
                        } else if (fs.existsSync(resolve(__dirname, 'node_modules', moduleName))) {
                            return resolve(__dirname, 'node_modules', moduleName);
                        }

                        console.error('Could not resolve ' + moduleName, {
                            paths: [
                                resolve(__dirname, 'node_modules', moduleName),
                                resolve(__dirname, '../..', 'node_modules', moduleName),
                                resolve(__dirname, '../../..', 'node_modules', moduleName)
                            ]
                        });

                        throw new Error('Could not resolve ' + moduleName);
                    }
                }
            });
            const scriptResult = vm.run(result.outputText, '/data/template.js');
            render = scriptResult.default;
            if (scriptResult.customRenderers) { customRenderers = scriptResult.customRenderers; }
            if (scriptResult.customTemplateComponents) { customTemplateComponents = scriptResult.customTemplateComponents; }
            if (scriptResult.stylePaths) { stylePaths = scriptResult.stylePaths; }
        }
        logger.info('Loaded template', { projectId, templateId });

        const configurationPath = assets.find(path => path.endsWith('configurations.yml') || path.endsWith('configuration.sfo.yml'));
        const metaDataSchemaPath = assets.find(path => path.endsWith('.schema.yml'));

        // transform local style paths into fully resolved paths
        stylePaths = stylePaths.map(({ path, position }) => {
            const asset = assets.find(assetPath => assetPath.endsWith(path));
            if (asset) {
                return { path: asset, position };
            }

            return null;
        }).filter(v => v != null);

        let css, configurations = opts?.additionalConfigurations || [], metaData, metaDataSchemaVariants = [];
        if (configurationPath) {
            configurations = readYAMLDocumentsFromFile(configurationPath);
            metaData = metaDataSchemaPath ? readYAMLDocumentsFromFile(metaDataSchemaPath)[0] : {};
        }

        if (configurations?.length > 0) {
            if (!configurations.some(c => c.kind === 'Environment')) {
                configurations.push({
                    kind: 'Environment',
                    spec: {
                        environments: {}
                    }
                });
            }

            // make sure each config only exists once (last one wins so we can overwrite by adding configs to the end)
            configurations = configurations?.filter((config, index, list) => {
                const reverse = list.slice().reverse();
                const reverseIndex = reverse.indexOf(config);
                return reverse.findIndex(c => sameConfigurationAndLocales(config, c)) === reverseIndex;
            });
            // make sure we only process configurations meant for this export
            configurations =
                configurations?.filter(configuration => {
                    if (!opts?.runner) { return true; }
                    if (Array.isArray(configuration.runners)) {
                        return configuration.runners.includes(opts?.runner);
                    }
                    return true;
                });

            const metaDataSchema = getComponentSchema(configurations.map(c => c.kind), configurations, opts.componentPath);
            metaDataSchemaVariants = metaDataSchema && configurations.filter(c => c.kind === 'Variant').map(variant => patchSchema(metaDataSchema, [variant]));
            if (metaDataSchema?.properties && metaData) {
                metaDataSchema.properties['TemplateMetaData'] = metaData
                // shallow merge both schemas
                metaData = {
                    ...metaDataSchema,
                    properties: {
                        ...metaDataSchema.properties,
                        TemplateMetaData: metaData
                    }
                };
            } else {
                metaData = metaDataSchema;
            }
        }

        const fontDir = join(opts?.fontPath || join(opts?.templateDir, '..', 'fonts'));

        /** The absolute path to the font files on the disk */
        let localAbsoluteFontPaths: string[] = [];
        /** The prefix added to the css to load the font on the web (url or path) */
        let urlPrefixForExports;

        /** The absolute path to the font files on the disk */
        let fontPath;
        /** The prefix added to the css to load the font on the web (url or path) */
        let urlPrefix;

        const fontConfiguration: GenericConfiguration<FontConfiguration> = configurations?.find(c => c.kind === Font.name);
        try {
            // read font files hosted inside the container (as opposed to Google Fonts (see below))
            if (fontConfiguration?.spec?.path) {
                const absoluteFontPath = join(fontDir, fontConfiguration.spec.path);
                if (existsSync(join(absoluteFontPath, 'fonts.scss'))) {
                    urlPrefixForExports = join('fonts', fontConfiguration.spec.path);
                    stylePaths.push({ path: join(absoluteFontPath, 'fonts.scss'), position: 'start' });
                    localAbsoluteFontPaths.push(absoluteFontPath);
                } else {
                    logger.warn('No entry fonts.scss file found for ' + fontConfiguration.spec.path);
                }
            }
        } catch (e) {
            logger.error('Could not load font configuration', { message: e.message, spec: fontConfiguration.spec })
        }

        const googleFontConfiguration = configurations?.find(c => c.kind === 'GoogleFont');

        if (googleFontConfiguration?.spec?.api?.url) {
            const fontTarget = opts?.runner === 'princexml' ? 'ttf' : 'web';
            const hashValue = createFontHash(googleFontConfiguration?.spec?.api?.url, fontTarget);
            fontPath = join(fontDir, hashValue);
            // if we get a font path we use it as an absolute path in the font URL (e.g. for local production through the cli)
            urlPrefix = opts?.fontUrlPrefix ? join(opts?.fontUrlPrefix, hashValue) : '/' + join('fonts', hashValue);
            if (existsSync(fontPath)) {
                logger.info('Google Fonts already present', { fontPath, urlPrefix, spec: googleFontConfiguration?.spec });
            } else {
                logger.info('Downloading Google Fonts', { fontPath, urlPrefix, spec: googleFontConfiguration?.spec });
                await downloadFont(googleFontConfiguration?.spec?.api?.url, { path: fontPath, urlPrefix, target: fontTarget });
            }
            stylePaths.push({ path: join(fontPath, 'fonts.scss'), position: 'start' });
        }

        const files = configurations?.find(({ kind }) => kind === 'Configuration')?.spec.assets?.map((asset) => {
            let url = asset.url || asset.path?.replace('./', '');
            let id = asset.id || createHash('md5').update(url).digest('hex');

            if (opts.baseUrl && asset.path?.startsWith('tpl/')) {
                url = `${opts.baseUrl}/export/${asset.path}`;
                id = asset.id || id;
            }

            if (asset.inline) {
                const file = assets.find(f => f.startsWith(templateDirPath) && f.endsWith(url));
                if (file && existsSync(file)) {
                    asset.content = readFileSync(file, 'utf-8');
                }
            }

            return {
                ...asset,
                id,
                // we now use role for logos so they can be images, too
                type: asset.type === 'logo' ? 'image' : asset.type,
                static: true,
                url,
                role: asset.role || asset.type === 'logo' ? 'logo' : undefined,
                locales: asset.locales || undefined
            };
        }) || [];

        return {
            render,
            assets,
            files,
            customRenderers,
            customTemplateComponents,
            configuration: configurations?.find(conf => conf.kind === 'Configuration')?.spec,
            configurations,
            metaData,
            metaDataSchemaVariants,
            stylePaths
        };
    } else {
        throw new Error('Template must be provided');
    }
};

const translations = {};

/**
 * Gets a template schema from a list of configurations.
 */
export const getLocalizedTemplateAndSchema = async (configurations: GenericConfiguration<any>[], metaDataSchema?: JSONSchema7, opts?: { runner?: string; locale?: string; variants?: string[]; getTranslation<T>(confSlug: string, locale: string): Promise<T> }) => {

    const configuration = configurations.find(c => c.kind === 'Configuration') as GenericConfiguration<ConfigurationConfiguration> | undefined;
    const slug = configuration?.slug || 'undefined-template';

    const locale = opts?.locale || configuration?.spec.language || 'en-US';
    const variants = opts?.variants || [];
    const runner = opts?.runner;

    if (typeof opts?.getTranslation === 'function') {
        // add default translations to the components (where none are defined)
        configurations = configurations.map(configuration => {
            const slug = decamelize(configuration.kind);
            const translationsForLocale = opts?.getTranslation(slug, locale);
            // find the translations for the provided locale
            if (!translationsForLocale) { return configuration; }
            return {
                ...configuration,
                translations: {
                    [locale]: {
                        // use the translations from the service first
                        ...translationsForLocale,
                        // merge in the template translations (overwriting the keys that have been translated)
                        ...(configuration.translations?.[locale] || {})
                    }
                }
            }
        }) || [];
    }

    // add default translations to the components (where none are defined)
    configurations = configurations.map(configuration => {
        const kindSlug = decamelize(configuration.kind);
        // find the translations for the provided locale
        const translationsForLocale = translations[kindSlug]?.[locale];
        if (!translationsForLocale) { return configuration; }
        return {
            ...configuration,
            translations: {
                [locale]: {
                    // use the translations from the service first
                    ...translationsForLocale,
                    // merge in the template translations (overwriting the keys that have been translated)
                    ...(configuration.translations?.[locale] || {})
                }
            }
        }
    }) || [];

    // make sure we only process configurations meant for this export
    configurations = configurations?.filter(configuration => {
        if (!runner) { return true; }
        if (Array.isArray(configuration.runners)) {
            return configuration.runners.includes(runner);
        }
        return true;
    });

    const dirs = [join(__dirname, '../export/package.json'), join(__dirname, '../../node_modules/@sciflow/export/package.json'), join(__dirname, '../node_modules/@sciflow/export/package.json')].map((path) => ({ path, exists: existsSync(path) }));
    if (dirs.some(d => d.exists)) {
        const path = dirs.find(d => d.exists)?.path as string;
        const exportDistDir = dirname(path);
        const result = await buildTemplateSchema(configurations, locale, variants, exportDistDir);
        if (!result) { return null; }

        configurations = result.configurations;

        const template = {
            slug,
            citationStyle: configuration?.citationStyle,
            customTemplateComponents: [],
            assets: [],
            customRenderers: {},
            configuration,
            configurations,
            metaData: metaDataSchema,
            schema: {
                $schema: 'http://json-schema.org/schema#',
                $id: `/template/schema/${slug}.json`,
                title: (configuration?.title || slug) + ' schema',
                description: 'All configurations that work with this template',
                type: 'object',
                properties: {
                    ...result.schema.properties
                }
            },
            prosemirrorNodeSchemas: result.prosemirrorNodeSchemas,
            stylePaths: [],
            locale,
            variants
        };

        return template;
    } else {
        console.warn('Did not find components in dirs', { __dirname, dirs });
    }

    return null;
};