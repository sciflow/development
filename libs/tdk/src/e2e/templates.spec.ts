import {expect} from 'chai';
import {sync} from 'glob';
import {describe, it} from 'mocha';
import {join, resolve} from 'path';
import {validateYamlFile} from '../validate';

function formatErrorMessages(results) {
  return results
    .map((result) => {
      const errorDetails = result.errors;

      if (Array.isArray(errorDetails)) {
        return errorDetails
          .map((err) => {
            const path = err?.['templatePath'];
            const additionalProperty = err.params.additionalProperty;
            return `- Error in Kind '${result.kind}':\n  - Path: ${path}\n  - Message: ${err.message}\n  - Additional Property: ${additionalProperty}`;
          })
          .join('\n');
      }

      return errorDetails;
    })
    .join('\n');
}

describe('Dynamic YAML File Validation', function () {
  let templates: string[] = [];

  before(async () => {
    const baseUrl = resolve(__dirname, '../../../../templates');
    const templateGlob = join(baseUrl, '**/*.sfo.@(yml|yaml)');
    templates = sync(templateGlob);

    describe(`Validating templates.`, () => {
      for (const templatePath of templates) {
        it(`${templatePath} - should be a valid template`, async function () {
          if (!this.test) {
            expect.fail(`Mocha context for this unit test has no property 'test'.`);
          }

          let result: {
            file: string;
            isValid: boolean | null;
            results: {
              kind: string;
              isValid: boolean | null;
              errors: string | object[] | undefined;
            }[];
          } | null;

          try {
            result = await validateYamlFile(templatePath);

            if (!result?.isValid && result?.results && result?.results.length > 0) {
              this.test['consoleOutputs'] = [`Errors:\n${formatErrorMessages(result?.results)}`];
            }

            expect(result?.isValid).to.be.true;
          } catch (error) {
            if (error?.message) {
              this.test['consoleErrors'] = [error.message];
            }

            console.error('Validation failed: ', error.message);
            expect.fail(`Expected template to be valid.`);
          }
        });
      }
    });
  });

  it('This is a required placeholder to allow before() to work', function () {});
});
