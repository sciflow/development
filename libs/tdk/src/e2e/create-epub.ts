#!/usr/bin/env yarn run ts-node

import { renderEPUB } from '@sciflow/export';
import { readFileSync, writeFileSync } from 'fs';
import { join } from 'path';
import { loadTemplate } from '../public-api';

let templateDir = join(__dirname, 'fixtures', 'templates');
const componentPath = join(__dirname, '..', '..', '..', 'libs', 'export', 'src', 'html');
const outPath = join(__dirname, 'outfiles');

/**
 * Generates an EPUB file.
 */
const main = async (documentSnapshot, filename = documentSnapshot.documentId  + '.epub') => {
    const template = await loadTemplate({ projectId: undefined, templateId: 'basic-epub' }, { templateDir, componentPath, fontPath: undefined, fontUrlPrefix: undefined });
    console.log('Read template', template.metaData);

    const parts = documentSnapshot.parts.map(part => ({
        ...part,
        id: part.partId || part.id
    }));
    const references = Object.keys(documentSnapshot.references || {}).map(id => documentSnapshot.references[id]);
    const document = await renderEPUB(
        {
            documentId: 'test',
            ...documentSnapshot,
            parts,
            title: 'Test',
            references
        },
        {
            configurations: template.configurations,
            metaData: documentSnapshot.metaData,
            metaDataSchema: template.metaData,
            customTemplateComponents: template.customTemplateComponents,
            customRenderers: template.customRenderers,
            stylePaths: template.stylePaths || [],
            scripts: []
        });

    writeFileSync(join(outPath, filename), document);
    console.log('Wrote epub ' + join(outPath, filename));
};

export default main;