import {expect} from 'chai';
import {describe, it} from 'mocha';
import * as path from 'path';
import {validateConfiguration, validateYamlFile} from '../validate';
import {GenericConfiguration} from '@sciflow/export';

describe('Validate', function () {
  describe('validateYamlFile', () => {
    it('should return true when validating each document of positive template against its corresponding schemas', async () => {
      const positiveTemplate: string = path.join(
        __dirname,
        'fixtures',
        'templates',
        'positive-template',
        'configurations.yml',
      );

      const result = await validateYamlFile(positiveTemplate);

      expect(result?.isValid).to.be.true;
    });

    it('should return false when validating a negative template against its corresponding schema', async () => {
      const negativeTemplate: string = path.join(
        __dirname,
        'fixtures',
        'templates',
        'negative-template',
        'configurations.yml',
      );

      const result = await validateYamlFile(negativeTemplate);

      expect(result?.isValid).to.be.false;
    });

    it('should return null when validating a template with missing spec', async () => {
      const missingTemplate: string = path.join(
        __dirname,
        'fixtures',
        'templates',
        'missing-template',
        'configurations.yml',
      );

      const result = await validateYamlFile(missingTemplate);
      expect(result?.isValid).to.be.null;
    });

    it('should throw an error while validating an invalid template file path', async () => {
      const path: string = '';

      try {
        await validateYamlFile(path);
        throw new Error('Expected function to throw an error, but it did not.');
      } catch (error) {
        expect(error.message).to.include("ENOENT: no such file or directory, open ''");
      }
    });
  });

  describe('validateConfiguration', () => {
    it('should return true when validating a configuration that allows custom keywords', async () => {
      const yamlDocument: GenericConfiguration<unknown> = {
        'kind': 'Environment',
        'spec': {
          'environments': {
            'table': {
              'captionSide': 'above',
            },
            'customKeyword': {
              'captionSide': 'above',
            },
          },
        },
      } as GenericConfiguration<unknown>;

      const result = await validateConfiguration(yamlDocument);
      expect(result.isValid).to.be.true;
    });

    it('should return true when validating a configuration with an invalid custom component path but a valid default path', async () => {
      const yamlDocument: GenericConfiguration<unknown> = {
        kind: 'TableOfContents',
        spec: {
          label: 'Table of Contents',
        },
      } as GenericConfiguration<unknown>;

      const result = await validateConfiguration(yamlDocument, ['/invalid/path']);

      expect(result.isValid).to.be.true;
    });

    it('should return false while validating a configuration with a component schema that does not exist', async () => {
      const yamlDocument: GenericConfiguration<unknown> = {
        kind: 'Should-not-Exist',
        spec: 'invalid',
      } as GenericConfiguration<unknown>;

      const result = await validateConfiguration(yamlDocument);

      expect(result.isValid).to.be.null;
    });

    it('should return true when validating a valid metadata', async () => {
      const yamlDocument: GenericConfiguration<unknown> = {
        kind: 'Variant',
        metadata: {
          name: 'default',
          title: 'Orange Yellow',
        },
      } as GenericConfiguration<unknown>;

      const result = await validateConfiguration(yamlDocument);
      expect(result.isValid).to.be.true;
    });

    it('should return null while validating an empty configuration', async () => {
      const yamlDocument: GenericConfiguration<unknown> = {} as GenericConfiguration<unknown>;

      const result = await validateConfiguration(yamlDocument);
      expect(result.isValid).to.be.null;
    });

    it('should throw an error for unknown custom keywords in the configuration', async () => {
      const invalidConfiguration: GenericConfiguration<unknown> = {
        kind: 'TableOfContents',
        spec: {
          customKeyword: 'CustomKeyword',
        },
      };

      const result = await validateConfiguration(invalidConfiguration);

      expect(result.isValid).to.be.false;
    });
  });

  describe('Translations', () => {
    it('should return true when validating a configuration with a de-DE translation', async () => {
      const yamlDocument: GenericConfiguration<unknown> = {
        kind: 'TableOfContents',
        spec: {},
        translations: {
          'de-DE': {
            label: 'Inhaltsverzeichnis',
          },
        },
      } as GenericConfiguration<unknown>;

      const result = await validateConfiguration(yamlDocument);

      expect(result.isValid).to.be.true;
    });

    it('should return true when validating a configuration with a de-DE translation', async () => {
      const yamlDocument: GenericConfiguration<unknown> = {
        kind: 'TableOfContents',
        spec: {
          label: 'Table of Contents',
        },
        translations: {
          'de-DE': {
            label: 'Inhaltsverzeichnis',
          },
        },
      } as GenericConfiguration<unknown>;

      const result = await validateConfiguration(yamlDocument);

      expect(result.isValid).to.be.true;
    });

    it('should return true when validating a configuration with multiple translations', async () => {
      const yamlDocument: GenericConfiguration<unknown> = {
        kind: 'TableOfContents',
        spec: {
          label: 'Table of Contents',
        },
        translations: {
          'de-DE': {
            label: 'Inhaltsverzeichnis',
          },
          'en-US': {
            label: 'Table of Contents',
          },
          'fr-FR': {
            label: 'Table des Matières',
          },
        },
      } as GenericConfiguration<unknown>;

      const result = await validateConfiguration(yamlDocument);

      expect(result.isValid).to.be.true;
    });

    it('should return true when validating a configuration with some translations and some missing translations', async () => {
      const yamlDocument: GenericConfiguration<unknown> = {
        kind: 'TableOfContents',
        spec: {
          label: 'Table of Contents',
        },
        translations: {
          'de-DE': {
            label: 'Inhaltsverzeichnis',
          },
          'en-US': {},
          'fr-FR': {
            label: 'Table des Matières',
          },
        },
      } as GenericConfiguration<unknown>;

      const result = await validateConfiguration(yamlDocument);

      expect(result.isValid).to.be.true;
    });

    it('should return false if configuration has spec but translations has invalid keyword', async () => {
      const yamlDocument: GenericConfiguration<unknown> = {
        kind: 'TableOfContents',
        spec: {
          label: 'Valid Spec',
        },
        translations: {
          'de-DE': {
            title: 'This follows a different schema',
          },
        },
      } as GenericConfiguration<unknown>;

      const result = await validateConfiguration(yamlDocument);

      expect(result.isValid).to.be.false;
    });

    it('should return false if configuration has valid spec but invalid translations with wrong data type', async () => {
      const yamlDocument: GenericConfiguration<unknown> = {
        kind: 'TableOfContents',
        spec: {
          label: 'Valid Spec',
        },
        translations: {
          'de-DE': {
            title: 123,
          },
        },
      } as GenericConfiguration<unknown>;

      const result = await validateConfiguration(yamlDocument);

      expect(result.isValid).to.be.false;
    });
  });

  it('should return null when both spec and translations are empty but schema is valid', async () => {
    const yamlDocument: GenericConfiguration<unknown> = {
      kind: 'ValidKind',
      spec: {},
      translations: {},
    } as GenericConfiguration<unknown>;

    const result = await validateConfiguration(yamlDocument);

    expect(result.isValid).to.be.null;
  });
});
