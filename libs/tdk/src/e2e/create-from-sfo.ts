import { createId, DocumentSnapshot, DocumentSnapshotPart, SFNodeType, SimplifiedManuscriptFile } from "@sciflow/schema";
import { createParts } from "@sciflow/export";
import { readFileSync } from "fs";
import { join } from "path";
import createXMLZip from './create-xml';

const main = async (documentSnapshot: SimplifiedManuscriptFile, filename = createId() + '.zip') => {

    if (!documentSnapshot.document.content) {
        throw new Error('Document must not be empty');
    }

    const parts = createParts(documentSnapshot.document.content);
    const title: DocumentSnapshotPart = parts.find(part => part.partId === 'title') || {
        partId: 'title',
        type: 'title',
        schema: 'title',
        document: {
            type: SFNodeType.document,
            content: [
                { type: SFNodeType.heading, content: [{ type: SFNodeType.text, text: 'Untitled document' }] }
            ]
        }
    };

    const snapshot: DocumentSnapshot = {
        title,
        createdAt: documentSnapshot.lastModified,
        files: documentSnapshot.files || [],
        parts: parts.filter(p => p.partId !== 'title'),
        index: {
            index: parts.filter(p => p.partId !== 'title').map(p => p.partId)
        },
        metaData: documentSnapshot.metaData,
        authors: documentSnapshot.authors,
        references: documentSnapshot.references || [],
        documentId: createId()
    };

    await createXMLZip(snapshot, filename);
}

export default main;
