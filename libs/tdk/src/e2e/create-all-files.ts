import { readdirSync, readFileSync } from "fs";
import { join } from "path";

import createHTML from './create-html';
import createEPUB from './create-epub';
import createXML from "./create-xml";
import createSFO from "./create-from-sfo";

const main = async () => {
    const snapshots = readdirSync(join(__dirname, 'fixtures'))
        .filter(name => name.endsWith('.snapshot.json'));

    for (let snapshot of snapshots) {
        const documentSnapshot = JSON.parse(readFileSync(join(__dirname, 'fixtures', snapshot), 'utf-8'));
        if (snapshot.endsWith('.sfo.snapshot.json')) {
            await createSFO(documentSnapshot, snapshot + '.xml.zip');
        } else {
            await createXML(documentSnapshot, snapshot + '.xml.zip');
            await createHTML(documentSnapshot, snapshot + '.html.zip');
            await createEPUB(documentSnapshot, snapshot + '.epub');
        }
    }
}

main().catch(console.error);
