/**
 * @title PDF Setup
 * @description Set up page margins, fonts and page sizes.
 */
interface SimpleConfiguration {
  /**
   * @title A
   * @description Some description
   * @defaults 1
   */
  a: number;
}
