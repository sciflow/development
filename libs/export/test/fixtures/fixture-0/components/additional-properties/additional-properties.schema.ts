interface SubSchema {
  c: string;
}

interface AdditionalPropertiesConfiguration {
  a: {
    [b: string]: SubSchema;
  };
  d: number;
}
