/**
 * @title B
 */
interface B {
  /**
   * @title c
   */
  c: string;
}

/**
 * @title A
 */
interface AConfiguration extends B {
  /**
   * @title A
   */
  a: number;
}
