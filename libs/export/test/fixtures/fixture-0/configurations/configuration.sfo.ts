const configuration = [
  {
    kind: 'Base',
    spec: {
      a: 1,
      b: {
        b1: 2,
      },
      c: {
        c1: 'pre {\n  font-family: ‘Noto Sans Mono’, monospace;\n}',
      },
    },
  },
  {
    kind: 'SpecOnly',
    spec: {
      b: 1,
    },
  },
  {
    kind: 'TranslationOnly',
    spec: {},
    translations: {
      'de-DE': {
        d: 10,
      },
    },
  },
  {
    kind: 'SpecAndTranslation',
    spec: {
      a: 1,
    },
    translations: {
      'de-DE': {
        b: {
          b1: 20,
        },
      },
    },
  },
];
