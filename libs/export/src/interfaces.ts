import { DocumentNode, Author, SFNodeType, DocumentSnapshotResource, DocumentSnapshotImage, counterStyle, DocumentSnapshot, PlacementOption } from '@sciflow/schema';
import { ReferenceRenderer, CSLReference } from "@sciflow/cite";
import { TemplateComponent } from './component';
import { NodeRenderer, RendererData } from './public-api';
import { GenericEnvironment } from './html/renderers/figure/figure.schema';
import { Logger } from 'winston';
import { Operation } from "fast-json-patch";

export { GenericConfiguration } from './html/renderers/generic-configuration/generic-configuration.schema';

export interface DocumentPartMeta {
    id: string;
    title?: string;
    type?: string;
    role?: string;
    numbering?: string;
}

export interface TocHeading {
    /** The title as a plain text */
    title?: string;
    /** A HTML reprentation fot he title, possibly including sup, sub, i and b elements next to spans. */
    titleHTML?: string;
    id: string;
    /** The physical level of the node (e.g. h1) */
    level: number;
    /**
     * The bookmark level of the node for table of contents or PDF labels (none for no bookmark)
     * If not set, defaults to the value of the heading level.
    */
    bookmarkLevel?: 'none' | number;
    counterStyle: counterStyle;
    /** The string that contains the prefix and numbering of
     *  the heading: e.g. 1.1.1 where 1.1 is the prefix and 
     *  the heading is the first sub-sub-chapter, so 1*/
    numberingString: string;
    /** The document part id for the heading */
    partId?: string;
    /** The fule name the elemennt will be hosted in */
    hostFileName?: string;
    /**
     * The placement of the heading in the document regions (front, body, back..)
     */
    placement?: string;
    /**
     * The numbering style for the page the heading appears on (e.g. lower-roman).
     */
    placementNumbering?: string;
    /**
     * Whether to display the heading in the toc
     */
    skipToc?: boolean;
    isTitle?: boolean;
    standalone?: boolean;
}

export interface DocumentTables {
    toc?: any;
    headings: DocumentElement<SFNodeType.heading>[];
    /** 
     * * @deprecated use the label environments. 
     * A list of all equations in the document  */
    equations: DocumentElement<SFNodeType.math>[];
    /**
     * @deprecated use the label environments. 
     * A list of image figures
    */
    figures: DocumentElement<SFNodeType.figure>[];
    footnotes: DocumentElement<SFNodeType.footnote>[];
    /** All footnotes and citations in order (e.g. to create endnotes ) */
    footnotesAndCitations: DocumentElement<SFNodeType.footnote | SFNodeType.citation>[]
    /**
     * @deprecated use the label environments.  
     * A list of all tables in the document  */
    tables: DocumentElement<SFNodeType.table>[];
    /** A list of all all labeled entities */
    labels: { [label: string]: DocumentElement<any>[] };
    /** All images including figure and standalone images */
    images: DocumentElement<SFNodeType.image>[];
    /** 
     * @deprecated use the label environments.  
     * A list of all code blocks in the document  */
    codeBlocks: DocumentElement<SFNodeType.code>[];
    /** A list of all document parts */
    parts: DocumentPartMeta[];
    /** A list of all citations */
    citations: DocumentElement<SFNodeType.citation>[];
    ids: any[];
    idMap: {};
}

export interface TemplateOptions {
    placement?: { [placement: string]: PlacementOption }
    [key: string]: any;
}

export interface SciFlowDocumentData extends RendererData {
    document: DocumentData;
    citationRenderer: ReferenceRenderer,
    templateOptions?: TemplateOptions;
    virtualDocument: Document;
    documentSnapshot?: DocumentSnapshot;
    metaData?: any;
    metaDataSchema?: any;
    configurations?: any[];
}

/**
 * @deprecated should be migrated to DocumentSnapshot
 */
export interface DocumentData {
    documentId: string;
    index: any;
    titlePart: any;
    locale?: string;
    title: string | undefined;
    metaData: any | undefined;
    subtitle: string | undefined;
    files: DocumentSnapshotResource[];
    parts: any[];
    images: DocumentSnapshotImage[];
    authors: Author[];
    /** Listings of document elements like headings, equations, footnotes, .. */
    tables?: DocumentTables;
    /** The document annotations (only if specifically requested) */
    annotations?: any[];
    references: any[];
    wordCount?: { total?: number; chars?: number; };
}

export interface DocumentElement<T> {
    id: string;
    type: T,
    title?: string;
    /** A title HTML string */
    titleHTML?: string;
    attrs?: any;
    /** A document part this element may be part of */
    partId?: string;
    /** The file name the element will be exported into (for cross file linking) */
    hostFileName?: string;
    /** The id to the figure, blockquote, etc that will be referenced */
    nodeId?: string;
    content?: DocumentNode<any>[];
    environment?: GenericEnvironment;
    /** A custom number assigned to this environment (e.g. 1.2a) */
    customNumber?: string;
    /** The label name like Table */
    customLabel?: string;
    /** The label including the name like Table, the numbering like 1.1 and the suffix like : */
    completeLabel?: string;
    label?: DocumentNode<SFNodeType.label>;
    /** The type of the parent */
    parentType?: string;
    nearestHeading?: string;
}

/**
 * The data handed to a template for rendering for documents with one text body (e.g. as created with OS-APS).
 * For more complex multi-part documents @see DocumentSnapshot
 */
export interface SingleDocumentExportData {
    title?: string;
    document: DocumentNode<SFNodeType.document>,
    authors: Author[],
    /** A list of styles to include with the position where they should be rendered (e.g. as a last style or as a first one) */
    stylePaths?: { path: string; position?: 'start' | 'end' }[];
    references: CSLReference[],
    configuration?: any,
    configurations?: any[],
    templateOptions: any,
    /** A JSON meta data object (created by a user modifying settings) */
    metaData?: unknown;
    /** A JSON meta data schema (possibly including default values) */
    metaDataSchema?: any;
    files?: { id: string; name?: string; url: string; }[];
    citationStyleData?: { styleXML: string; localeXML: string; }
}

export type runnerType = 'princexml' | 'pagedjs' | 'html' | 'epub' | 'xml' | 'raw' | string;

export interface ExportOptions {
    logging?: {
        /** A transaction id used for tracing during exports */
        transactionId: string;
        /** An optional Winston logger to write to */
        logger: Logger;
    }

    /** A specific locale to be used for the export */
    locale?: string;
    /** The configuration YAML objects for each kind (also @see customTemplateComponents) */
    configurations: any[];
    /** The meta data set in the document */
    metaData: any;
    /** The JSON meta data schema (including defaults) */
    metaDataSchema: any;
    /** An object containing custom renderers */
    customRenderers: { [nodeType: string]: NodeRenderer<any, SciFlowDocumentData> };
    /** An array containing custom components to render a kind in order (last overwrites previous) */
    customTemplateComponents: TemplateComponent<any, SciFlowDocumentData>[];
    /** Paths to additional scss files */
    stylePaths: {
        path: string;
        /** Start puts the style to the very start of the template, end overwrites previous styles */
        position?: 'start' | 'end',
        /** A runner the styles are restricted to (e.g. princexml, html, xml, ..) */
        runners?: runnerType[];
    }[];
    /**
     * Paths that may contain additional component resources.
     */
    componentPaths?: string[];
    /** Base paths for import of style files */
    assetBasePaths?: string[];
    /** Whether to render inline where possible (i.e. styles, scripts, etc.). Images may be linking to an external URL. */
    inline?: boolean;
    scripts: { src?: string; content?: string; }[];
    files?: DocumentSnapshotResource[];
    /** An indicator as to what the output will be processed as (e.g. raw, princexml, epub, pagedjs, ..) */
    runner?: runnerType;
    /** A host to set as a base href for inlined image/font/resource paths that are not absolute */
    baseHref?: string;

    citationStyleXML?: { [locale: string]: string; }
    localeXML?: { [locale: string]: string; }

    /**
     * Whether to output debug information
     */
    debug?: boolean;
    /**
     * Whether fonts should be defined in a separate css file.
     */
    separateFonts?: boolean;
}
