import { GenericConfiguration } from "./interfaces";

/**
 * Compares two configurations based on name and runner configuration (but not the locales).
 */
export const sameConfiguration = (a: GenericConfiguration<any>, b: GenericConfiguration<any>) => {
    if (a.metadata?.name !== b.metadata?.name) { return false; }
    if (a.kind !== b.kind) { return false; }
    if (a.page != b.page) { return false; }
    if (a.runners?.join() !== b.runners?.join()) { return false; }
    return true;
};

/** Compares whether two configurations are the same based on the supported locales */
export const sameConfigurationAndLocales = (a: GenericConfiguration<any>, b: GenericConfiguration<any>) => {
    if (!sameConfiguration(a, b)) { return false; }
    if ((!a.locales || a.locales?.length === 0) && (!b.locales || b.locales?.length === 0)) { return true; }
    const localesA = a.locales || [];
    const localesB = b.locales || [];
    // a nd b are the same?
    if (localesA.length === 0 && localesB.length === 0) { return true; }
    if (localesB.length > 0 && localesA.length > 0 && localesA.filter(la => localesB.includes(la)).length === localesB.length) {
        return true;
    }

    // so even if a includes one more locale than b we will leave both in.
    return false;
}