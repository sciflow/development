// This utility file should not contain any imports as these helpers are to be used without other libraries.

import { GenericConfiguration } from "../interfaces";

/**
 * Converts a kebab-case string to PascalCase.
 *
 * This function takes a string in kebab-case format (words separated by hyphens)
 * and converts it to PascalCase format, where each word is capitalized and
 * concatenated without any separators.
 *
 * @param {string} s - The kebab-case string to convert.
 * @returns {string} The converted PascalCase string.
 *
 * @example
 * // returns 'TableOfContents'
 * kebabToPascal('table-of-contents');
 *
 * @example
 * // returns 'CamelCaseExample'
 * kebabToPascal('camel-case-example');
 */
export const kebabToPascal = (s: string): string =>
  s
    .split('-')
    .map((word) => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase())
    .join('');

/** Takes a string and removes special characters
 * Never starts with a number to be a valid XML attribute.
 */
export const slugify = (title: string) => {
  if (!title) {
    return undefined;
  }
  // slug courtesy of https://gist.github.com/mathewbyrne/1280286
  return title
    .toString()
    .toLowerCase()
    .replace(/\s+/g, '-') // Replace spaces with -
    .replace(/[^\w\-]+/g, '') // Remove all non-word chars
    .replace(/\-\-+/g, '-') // Replace multiple - with single -
    .replace(/^-+/, '') // Trim - from start of text
    .replace(/-+$/, '') // Trim - from end of text
    .replace(/^[0-9]+/, '_');
};

/**
 * This function takes a camelCase string and converts it to kebab-case,
 * where each uppercase letter is replaced with a hyphen followed by the
 * lowercase equivalent.
 *
 * @param {string} s - The camelCase string to convert.
 * @returns {string} - The converted kebab-case string.
 *
 * @example
 * // returns 'hello-world'
 * decamelize('helloWorld');
 *
 * @example
 * // returns 'my-variable-name'
 * decamelize('myVariableName');
 */
export const decamelize = (s: string) =>
  s
    .replace(/([a-z\d])([A-Z])/g, '$1-$2')
    .replace(/([A-Z]+)([A-Z][a-z\d]+)/g, '$1-$2')
    .toLowerCase();

/**
 * Generates a unique configuration name based on the provided configuration object.
 *
 * @param {Object} config - The configuration object.
 * @throws {Error} If the config object is empty.
 * @returns {string} The generated configuration name.
 */
export const getConfigName = (config: GenericConfiguration<any>): string => {
  if (!config) {
    debugger;
    throw new Error('Config must not be empty');
  }
  if (config?.metadata?.name) {
    // combine the kind (e.g. Page with a unique identifier e.g. forPdf -> Page-forPdf)
    return config.metadata.name.startsWith(config.kind)
      ? config.metadata.name
      : `${config.kind}-${config.metadata.name}`;
  } else if (config.metadata?.title) {
    const name = slugify(config.metadata?.title);
    return `${config.kind}-${name}`;
  }

  // if a page has been set, append it to make the configuration unique
  return config.page ? `${config.kind}-${config.page.toLowerCase()}` : `${config.kind}`;
};

export const dfs = (
  schema: object,
  node: string,
  visited: {[key: string]: boolean},
  stack: {[key: string]: boolean},
): boolean => {
  visited[node] = true;
  stack[node] = true;

  const properties = schema['definitions'][node].properties;
  if (properties) {
    for (const key in properties) {
      if (properties.hasOwnProperty(key)) {
        const ref = properties[key].$ref;
        if (ref) {
          const refKey = ref.split('/').pop()!;
          if (!visited[refKey]) {
            if (dfs(schema, refKey, visited, stack)) {
              return true;
            }
          } else if (stack[refKey]) {
            return true;
          }
        }
      }
    }
  }

  stack[node] = false;
  return false;
};

export const removeNestedNullUndefined = (obj) => {
  for (const key in obj) {
    if (obj[key] === null || obj[key] === undefined) {
      delete obj[key];
    } else if (typeof obj[key] === 'object') {
      removeNestedNullUndefined(obj[key]);
    }
  }
};
