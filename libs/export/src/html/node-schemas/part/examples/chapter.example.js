exports.default = [
  {
    label: 'Normal chapter',
    value: 'chapter',
    description: 'The standard chapter type'
  },
  {
    label: 'Abstract',
    value: 'abstract',
    description:
      'A summary of your work. We might move this section onto an extra page during export, depending on the template you choose.'
  },
  {
    label: 'Bibliography',
    value: 'bibliography',
    description: 'A reference list that sums up all references used in the document.'
  },
  {
    label: 'Appendix',
    value: 'appendix',
    description: 'An appendix for supplemental material, usually numbered with A, B, ..'
  },
  {
    label: 'Part',
    value: 'part',
    description: 'A new part of the document, used in larger documents to group chapters that belong together.'
  },
  {
    label: 'Free part',
    value: 'free',
    description: 'A chapter with no headings (e.g. to use as a cover page)'
  }
];
