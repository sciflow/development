exports.default = [
  {
    label: 'Cover',
    value: 'cover',
    description: 'The cover pages (usually with no numbering and a free format)',
  },
  {
    label: 'Front',
    value: 'front',
    description:
      'Templates may use the frontmatter to change page numbering to Roman numerals or apply different styling.',
  },
  {
    label: 'Body',
    value: 'body',
    description: 'Document body (default). Usually this is correct.',
  },
  {
    label: 'Back',
    value: 'back',
    description: 'Backmatter',
  },
];
