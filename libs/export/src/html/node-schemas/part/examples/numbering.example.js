exports.default = [
  {
    label: 'Decimal',
    value: 'decimal',
    description: '1, 1.2, 2, ..'
  },
  {
    label: 'Alphabetic',
    value: 'alpha',
    description: 'A, B, ..'
  },
  {
    label: 'Roman',
    value: 'roman',
    description: 'I, II, III, ..'
  },
  {
    label: 'None',
    value: 'none',
    description: 'No numbering'
  }
];
