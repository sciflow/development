exports.default = [
  {
    label: 'None',
    value: 'none',
    description: 'Do not check spelling in this chapter',
  },
  {
    label: 'English (US)',
    value: 'en-US',
    description: '',
  },
  {
    label: 'English (GB)',
    value: 'en-GB',
    description: '',
  },
  {
    label: 'German (DE)',
    value: 'de-DE',
    description: '',
  },
  {
    label: 'French',
    value: 'fr-FR',
    description: '',
  },
  {
    label: 'Spanish',
    value: 'es-ES',
    description: '',
  },
  {
    label: 'Polish',
    value: 'pl-PL',
    description: '',
  },
  {
    label: 'Portuguese',
    value: 'pt-PT',
    description: '',
  },
  {
    label: 'Persian',
    value: 'fa-IR',
    description: '',
  },
  {
    label: 'Arabic',
    value: 'ar',
    description: '',
  },
];
