interface AltTitle {
  /**
   * @title Type
   */
  type: string;
  /**
   * @title Content
   * @_ui_widget sfo-rich-text
   */
  content: string;
}

export interface PartAttr {
  /**
   * @title Hide in Table of Contents
   */
  skipToc: boolean;
  /**
   * @title Alt Titles
   */
  alTitle: AltTitle[];
}

export interface PartConfiguration {
  /**
   * @_ui_hide true
   */
  id: string;
  /**
   * @title Chapter Type
   * @examples require('./examples/chapter.example.js')
   * @default 'chapter'
   * @_ui_widget sfo-radio-group
   */
  type: 'chapter' | 'abstract' | 'bibliography' | 'appendix' | 'part' | 'free';
  /**
   * @title Language
   * @examples require('./examples/language.example.js')
   * @default 'chapter'
   * @_ui_widget sfo-radio-group
   */
  language: 'none' | 'en-US' | 'en-GB' | 'de-DE' | 'fr-FR' | 'es-ES' | 'pl-PL' | 'fa-FR' | 'ar';
  /**
   * @title Numbering
   * @description Most templates will choose decimal numbering by default
   * @examples require('./examples/numbering.example.js')
   * @_ui_widget sfo-radio-group
   */
  numbering: 'decimal' | 'alpha' | 'roman' | 'none';
  /**
   * @title Placement
   * @description Some of our templates (e.g. thesis templates) divide the document into front- and backmatter as well as the body. Pages in the frontmatter might be numbered differently.
   * @examples require('./examples/placement.example.js')
   * @default body
   * @_ui_group Role Placement
   * @_ui_widget sfo-radio-group
   */
  placement: 'cover' | 'front' | 'body' | 'back';
  /**
   * @title Role
   * @_ui_group Role Placement
   */
  role: string;
  /**
   * @_ui_group Advanced
   */
  data?: PartAttr;
  /**
   * @title Text Direction
   * @_ui_hide true
   */
  textDirection: 'ltr' | 'rtl' | 'auto' | undefined;
}
