export interface ImageConfiguration {
  /**
   * @_ui_hide true
   */
  id: string;
  /**
   * @title Source
   * @description URL or path to the figure image.
   * @_ui_hide true
   */
  src: string;
  /**
   * @title Alt
   * @description How would you describe this object and its content for visually impaired readers?
   */
  alt: string;
  /**
   * @title Title
   * @_ui_hide true
   */
  title: string;
  /**
   * @title Width
   * @description Width of the figure in pixels.
   * @_ui_hide true
   */
  width: number;

  /**
   * @title Height
   * @description Height of the figure in pixels.
   * @_ui_hide true
   */
  height: number;

  /**
   * @title Meta Data
   * @_ui_hide true
   */
  metaData: string;

  /**
   * @title Mark as decorative
   * @description Decorative images do not provide information of content and only serve as a visual interest.
   */
  decorative: boolean;
}
