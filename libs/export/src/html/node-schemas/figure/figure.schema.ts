export interface FigureConfiguration {
  /**
   * @_ui_hide true
   */
  id: string;
  /**
   * @title Source
   * @description URL or path to the figure image.
   * @_ui_hide true
   */
  src: string;
  /**
   * @title Width
   * @description Width of the figure in pixels.
   * @_ui_hide true
   */
  width: number;

  /**
   * @title Height
   * @description Height of the figure in pixels.
   * @_ui_hide true
   */
  height: number;
  /**
   * @title Type
   * @description The type of media content.
   * @_ui_hide true
   */
  type: 'figure' | string;

  /**
   * @title Environment
   * @description Context in which the figure appears.
   * @_ui_hide true
   */
  environment?: string;

  /**
   * @title Orientation
   * @description The orientation of the figure.
   * @_ui_hide true
   * @default "portrait"
   */
  orientation: 'portrait' | 'landscape';
  /**
   * @title Scale Width
   * @_ui_hide true
   */
  'scale-width': number;
  /**
   * @_ui_hide true
   */
  'float-placement': string;
  /**
   * @_ui_hide true
   */
  'float-reference': string;
  /**
   * @_ui_hide true
   */
  'float-defer-page': string;
  /**
   * @_ui_hide true
   */
  'float-modifier': string;
  /**
   * @title Alt
   * @description How would you describe this object and its content for visually impaired readers?
   */
  alt: string;

  /**
   * @title Mark as decorative
   * @description Decorative images do not provide information of content and only serve as a visual interest.
   */
  decorative: boolean;
}
