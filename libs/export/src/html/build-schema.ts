import { sync } from 'glob';
import { join, resolve } from 'path';
import * as TJS from 'typescript-json-schema';
import { ScriptTarget, ModuleKind, ModuleResolutionKind } from 'typescript';
import { createLogger, format, transports } from 'winston';
import { writeFileSync } from 'fs';
const { LOG_LEVEL = 'info' } = process.env;
const logger = createLogger({
    level: LOG_LEVEL,
    format: format.json(),
    defaultMeta: { service: 'generate-schema' },
    transports: [
        new transports.Console({ level: LOG_LEVEL })
    ],
});

const run = async () => {
    // make sure we always start in the @scifloe/export directory
    const baseUrl = join(__dirname);
    const schemaGlob = `${baseUrl}/@(components|renderers)/**/*.schema.@(ts|tsx)`;
    console.log('building schema from ', { schemaGlob, baseUrl });
    let files = sync(schemaGlob);
    console.log('reading files', files.length);
    let schema = { definitions: {} };
    if (files.length === 0) { throw new Error('No schema files found in ' + schemaGlob); }

    const compilerOptions: TJS.CompilerOptions = {
        strictNullChecks: true,
        target: ScriptTarget.ES2020,
        lib: ['es6', 'ES2021', 'dom'],
        module: ModuleKind.CommonJS,
        skipLibCheck: true,
        allowUnionTypes: true,
        noEmit: true,
        baseUrl,
        experimentalDecorators: true,
        emitDecoratorMetadata: true,
        allowUnusedLabels: true
    };

    const settings: TJS.PartialArgs = {
        tsNodeRegister: true, // enable requires,
    };

    let program;
    try {
        program = TJS.getProgramFromFiles(
            files.map(file => resolve(file)),
            compilerOptions
        );
    } catch (e: any) {
        logger.error('Could not get program files', { message: e.message, compilerOptions });
        throw new Error('Could not get program files: ' + e.message + ' / ' + files);
    }

    let schemaFragment;
    try {
        schemaFragment = TJS.generateSchema(program, "*", settings);
        if (!schemaFragment) {
            console.warn('Did not generate a schema for ' + files);
            debugger;
        } else {
            schema = {
                definitions: {
                    ...schema.definitions,
                    ...(schemaFragment.definitions || {})
                }
            };
        }
    } catch (e: any) {
        logger.error('Could not generate schema', { message: e.message });
        throw new Error('Could not generate schema: ' + e.message + ' / ' + files);
    }

    const exportPath = join(baseUrl, 'schema.json');
    console.log({ exportPath });
    writeFileSync(exportPath, JSON.stringify(schema, null, 2));
}

run().catch(console.error);

