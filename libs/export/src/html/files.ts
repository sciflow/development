import mime from 'mime';
import sharp from 'sharp';
import { createLogger, format, transports } from 'winston';

import { fetch } from 'cross-fetch';

import { existsSync, readFileSync } from 'fs';
import { extname } from 'path';
import { parse } from 'url';
import { Environment } from './components/environment/environment.component';
import { FigureRendererConfiguration } from './renderers/figure/figure.schema';
import { DocumentSnapshotResource } from '@sciflow/schema';
const { LOG_LEVEL = 'info' } = process.env;
const logger = createLogger({
    level: LOG_LEVEL,
    format: format.json(),
    defaultMeta: { service: 'html-main-files' },
    transports: [
        new transports.Console({ level: LOG_LEVEL })
    ],
});

export const transformFiles = async (exportData, options, { assetDir, fontDir }) => {

    const startTime = Date.now();
    // extract the mime from all files and make sure they have an ID
    let files: DocumentSnapshotResource[] = [...(exportData.files || []), ...(options?.files || [])]
        .map(file => {
            const mimeType = mime.getType(file.url);
            if (mimeType) { file.mimeType = mimeType; }
            return { ...file, id: (file.fileId || file.id) as string };
        });

    if (options?.inline) { return files; }

    let fileCount = 0;
    const isNumber = (x: undefined | string | number) => {
        if (typeof x === 'number') { return true; }
        if (typeof x === 'string') { return /[0-9]+/.test(x); }
        return false;
    };

    // add a placeholder for if an image is missing
    const defaultBlankImage = await sharp({
        create: {
            width: 400,
            height: 300,
            channels: 4,
            background: { r: 0, g: 0, b: 0, alpha: 0.3 }
        }
    })
        .flatten({ background: 'white' })
        .webp({ lossless: false, quality: 10 });

    // replace all images with placeholders for debug previews or download the original image file
    files = await Promise.all(files.map(async (file) => {
        if (file.inline === true) { return file; }
        switch (file.type) {
            case 'image':
                // make sure to remove any query strings that might still be attached
                const parsedUrl = parse(file.url);
                let ext = extname(parsedUrl.pathname || '');
                if (parsedUrl.query) {
                    const params = new URLSearchParams(parsedUrl.query);
                    if (params.get('format')) {
                        ext = '.' + params.get('format') as string;
                    }
                }

                let width = isNumber(file?.dimensions?.width) && file?.dimensions?.width as number || 400;
                let height = isNumber(file?.dimensions?.height) && file?.dimensions?.height as number || 300;

                // check whether the size will be supported by the WebP format
                // TODO we do not correct the ratio here
                if (width > 16383 || height > 16383) {
                    width = Math.min(16383, width);
                    height = Math.min(16383, height);
                    logger.warn('Image ' + file.fileId + ' exeeded 14bit in width or height');
                }

                let blankImage = defaultBlankImage;
                if (options?.debug === true) {
                    try {
                        const paddingY = Math.round(width - Math.min(width, 400) / 2);
                        const paddingX = Math.round(height - Math.min(height, 300) / 2);
                        blankImage = await sharp({
                            text: {
                                text: `${file.id} @ ${width}x${height}`,
                                width: Math.min(width, 400),
                                height: Math.min(height, 300),
                                align: 'center',
                                wrap: 'word'
                            }
                        })
                            .extend({
                                top: paddingX,
                                right: paddingY,
                                left: paddingY,
                                bottom: paddingX
                            })
                            .flatten({ background: 'white' })
                            .webp({ lossless: false, quality: 20 });
                    } catch (e) {
                        logger.warn('Could not create image', { url: file.url, documentId: exportData.documentId });
                    }

                    if (assetDir?.file(`${file.id}.webp`, blankImage.toBuffer(), { binary: true })) {
                        const fileName = `${file.id}.webp`;
                        const filePath = `assets/${fileName}`;
                        file.originalUrl = file.url;
                        file.url = filePath;
                        file.name = fileName;
                        file.mimeType = 'image/webp';
                        logger.info(`Did not fetch for debugging (${file.url})`, { documentId: exportData.documentId });
                        return file;
                    }
                } else {
                    let source;
                    try {
                        logger.info(`Processing ${fileCount}/${files.length} (${file.url})`, { documentId: exportData.documentId });
                        const controller = new AbortController();
                        const id = setTimeout(() => {
                            logger.warn('Could not fetch file (timed out)', { url: file.url, documentId: exportData.documentId });
                            controller.abort();
                        }, 15000); // 15s
                        source = await fetch(file.url, { signal: controller.signal });
                        clearTimeout(id);
                    } catch (e: any) {
                        logger.warn('Could not complete request', { message: e.message, documentId: exportData.documentId });
                    }

                    if (source?.status === 200) {
                        fileCount++;
                        logger.info(`Fetched ${fileCount}/${files.length} (${file.url})`, { documentId: exportData.documentId });
                        const content = await source.blob();
                        if (content) {
                            try {
                                const contentExt = mime.getExtension(content.type);
                                // we save the images to the content dir because apple books showed issues with relative imports
                                const b = Buffer.from(await content.arrayBuffer());
                                if (assetDir?.file(`${file.id}.${contentExt}`, b, { binary: true })) {
                                    const fileName = `${file.id}.${contentExt}`;
                                    const filePath = `assets/${fileName}`;
                                    file.originalUrl = file.url;
                                    file.url = filePath;
                                    file.name = fileName;
                                    file.mimeType = content.type;

                                    return file;
                                }
                            } catch (e) {
                                debugger;
                                logger.error('Could not save image', { url: file.url, type: content.type });
                            }
                        }
                    }
                }
                break;
            case 'font':
                if (existsSync(file.url)) {
                    fontDir?.file(file.id, readFileSync(file.url), { binary: true });
                }
                break;
        }
        return file;
    }));

    logger.info('Transformed files', { documentId: exportData.documentId, count: files.length, timeMs: (Date.now() - startTime)});

    return files;
}

/**
 * Replaces an image inside a ZIP folder with a compressed version (if image is supported by sharp).
 */
export const transformImage = async (file, { imageDirectory, engine }) => {
    try {
        if (file.type !== 'image') { return file; }
        const fileName = file.name as string;
        // do not process files twice
        if (imageDirectory.file('fileName')) { return file; }
        const b = await imageDirectory?.file(fileName)?.async('uint8array');
        if (!b || b.length === 0) { return file; }
        let image, metaData;
        try {
            image = await sharp(b).withMetadata();
            metaData = await image.metadata();
        } catch (e: any) {
            if (e.message === 'Input buffer contains unsupported image format') { return file; }
            debugger;
            logger.error('error', 'Could not read image', { fileName, message: e.message });
            return file;
        }

        file.originalUrl = file.url;
        file.metaData = metaData;
        if (!file.dimensions) {
            // we do not override dimensions that may have been set lower on purpose
            file.dimensions = {
                width: metaData.width as string | number,
                height: metaData.height as string | number
            };
        }

        const environmentComponent = new Environment(engine);
        const configuration: FigureRendererConfiguration = environmentComponent.getValues();
        if (configuration.transformImages?.format) {
            let extension;
            // base numbers for 300dpi on A4
            let quality = configuration.transformImages?.quality;
            let dpi = configuration.transformImages.dpi;

            const maxPixelWidth = Math.round(2481 / 300 * dpi);
            const maxPixelHeight = Math.round(3507 / 300 * dpi);

            try {
                let convertedImage;
                if (metaData.width && metaData.height) {
                    if (metaData.width >= metaData.height) {
                        // landscape
                        convertedImage = await image
                            // using the max height to restrict the landscape image
                            // e.g. in case it is rotated
                            .flatten({ background: 'white' })
                            .resize({ width: Math.min(metaData.width, maxPixelHeight) });
                    } else {
                        // portrait
                        convertedImage = await image
                            // using the max width to restrict the portait image
                            .flatten({ background: 'white' })
                            .resize({ width: Math.min(metaData.height, maxPixelHeight) });
                    }
                }

                if (!convertedImage) { return file; }

                let imageBuffer, mimeType;
                const format = configuration.transformImages?.format?.toLowerCase();
                switch (format) {
                    case 'webp':
                        extension = 'webp';
                        mimeType = 'image/webp';
                        imageBuffer = await convertedImage.webp({ lossless: false, quality }).toBuffer();
                        break;
                    case 'png':
                        extension = 'png';
                        mimeType = 'image/png';
                        imageBuffer = await convertedImage.png({ lossless: false, quality }).toBuffer();
                        break;
                    case 'jpeg':
                        extension = 'jpeg';
                        mimeType = 'image/jpeg';
                        imageBuffer = await convertedImage.jpeg({ lossless: false, quality }).toBuffer();
                        break;
                }

                if (imageBuffer) {
                    const fileName2 = `${file.id}.${extension}`;
                    file.name = fileName2;
                    const filePathCompressed = `assets/${fileName2}`;
                    if (imageDirectory?.file(`${file.id}.${extension}`, imageBuffer, { binary: true })) {
                        file.compressedUrl = filePathCompressed;
                        file.mimeType = mimeType;
                        if (fileName !== fileName2) {
                            // only remove the file if it wasn't replaced just now
                            await imageDirectory?.remove(fileName);
                        }
                    }
                }
            } catch (e: any) {
                debugger;
                logger.warn('error', 'Could not transform webp', { message: e.message });
            }
        }

        return file;
    } catch (e: any) {
        logger.error('Could not transform image', { message: e.message });
        return file;
    }
};