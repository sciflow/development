import { TSX } from '../TSX';

import { expect } from 'chai';
import { describe, it } from 'mocha';

import { JSDOM } from 'jsdom';
import { TextRenderer, basicNode } from './renderers/inline';
import { ParagraphRenderer } from './renderers/paragraph';
import { canJoin, domFromMarkedText, mergeTextNodes } from './renderers/text';
import { DocumentNode, SFMarkType, SFNodeType } from '@sciflow/schema';

// test helper
const markedText = (text: string, marks: string[]): DocumentNode<SFNodeType.text> => ({
    type: SFNodeType.text, marks: marks.map(type => ({ type })), text
});

/**
 * A test implementation of the export engine.
 */
class TestEngine {
    data: any;

    constructor() {

        const dom = new JSDOM();
        this.data = {
            virtualDocument: dom.window.document
        };
    }

    renderers = {
        [SFNodeType.text]: new TextRenderer(this as any),
        [SFNodeType.paragraph]: new ParagraphRenderer(this as any),
        [SFNodeType.hardBreak]: new (basicNode('br'))(this as any),
        [SFNodeType.footnote]: new (basicNode('footnote'))(this as any)
    };

    async renderContent(node: DocumentNode<any>) {
        if (!node.content) { return []; }
        return await Promise.all(node.content.map(c => this.render(c)));
    }

    async render(node: DocumentNode<any>) {
        const renderer = this.renderers[node.type];
        if (!renderer) { throw new Error('Unknown type ' + node.type); }
        return renderer.render(node, undefined);
    }
}

describe('Text renderer', async () => {
    it('Creates nested dom', async () => {
        expect((domFromMarkedText([{ type: SFMarkType.strong }, { type: SFMarkType.emphasis }], 'italic and strong') as HTMLElement)?.outerHTML)
            .to.equal('<b><i>italic and strong</i></b>');
    });

    it('Joins nodes correctly', async () => {
        // b only adds new tags -> allow
        expect(canJoin(markedText('a', ['bdi']), markedText('b', ['em', 'bdi']))).to.be.true;
        // both have no tags -> allow
        expect(canJoin(markedText('a', []), markedText('b', []))).to.be.true;
        // b does not have all of a's tags -> don't allow
        expect(canJoin(markedText('a', ['bdi', 'strong']), markedText('b', ['em', 'bdi']))).to.be.false;

        const x = mergeTextNodes([
            markedText('A<', []),
            markedText('>B', [])
        ]);

        expect((<div>
            {mergeTextNodes([
                markedText('A<', []),
                markedText('>B', [])
            ])}
        </div>).innerHTML).to.equal('A&lt;&gt;B');

        expect(mergeTextNodes([
            markedText('A<', [SFMarkType.bdi, SFMarkType.emphasis]),
            markedText('>B', [SFMarkType.bdi, SFMarkType.emphasis])
            // we expect bdi to be on the outside
        ]).map((node: any) => node.outerHTML || node.textContent).join('')).to.equal('<bdi><i>A&lt;&gt;B</i></bdi>');

        const n = mergeTextNodes([
            markedText('Starting ', []),
            markedText('with ', [SFMarkType.bdi]),
            markedText('italic text', [SFMarkType.emphasis, SFMarkType.bdi]),
            markedText(', ', [SFMarkType.emphasis, SFMarkType.bdi]),
            markedText('bold text', [SFMarkType.emphasis, SFMarkType.bdi, SFMarkType.strong]),
            markedText(' ...', [SFMarkType.emphasis, SFMarkType.bdi]),
            markedText('2', [SFMarkType.superscript]),
            markedText('.', [])
        ]);

        expect(n.map((node: any) => node.outerHTML || node.textContent).join('')).to.equal(
            'Starting <bdi>with <i>italic text, <b>bold text</b> ...</i></bdi><sup>2</sup>.'
        );

        const n2 = mergeTextNodes([
            markedText('Hello,', [SFMarkType.emphasis]),
            markedText(' this is a ', []),
            markedText('test', [SFMarkType.strong]),
            markedText(' sentence.', [])
        ]);

        expect(n2.map((n2: any) => n2.outerHTML || n2.textContent).join('')).to.equal(
            '<i>Hello,</i> this is a <b>test</b> sentence.'
        );
    });
    it('Renders paragraphs with the right mark order', async () => {
        const engine = new TestEngine();
        const result = await engine.render({ "type": "paragraph", "attrs": { "dir": "rtl" }, "content": [{ "type": "text", "text": "A" }, { "type": "text", "marks": [{ "type": "bdi" }], "text": "(PRE, " }, { "type": "text", "marks": [{ "type": "bdi" }, { "type": "em" }], "text": "BDI" }, { "type": "text", "marks": [{ "type": "bdi" }], "text": " POST تست)" }] });
        expect((<div>{result}</div>).innerHTML).to.equal('<p dir="rtl">A<bdi>(PRE, <i>BDI</i> POST تست)</bdi></p>');
    });

    it('Matches mixed paragraphs', async () => {
        const engine = new TestEngine();
        const para = {
            "type": "paragraph",
            "attrs": {
                "id": "ugp9bpbybob"
            },
            "content": [
                {
                    "type": "footnote",
                    "attrs": {
                        "id": "hq96xdtozzja",
                        "type": "footnote"
                    },
                    "content": [
                        {
                            "type": "text",
                            "text": "text"
                        }
                    ]
                },
                {
                    "type": "text",
                    "marks": [
                        {
                            "type": "bdi"
                        }
                    ],
                    "text": "(PRE, "
                },
                {
                    "type": "text",
                    "marks": [
                        {
                            "type": "bdi"
                        },
                        {
                            "type": "em"
                        }
                    ],
                    "text": "Compendium of Chronicles "
                },
                {
                    "type": "text",
                    "marks": [
                        {
                            "type": "bdi"
                        }
                    ],
                    "text": "۲۷)"
                },
                {
                    "type": "text",
                    "text": "."
                }
            ]
        };

        const result = await engine.render(para);
        expect((<div>{result}</div>).innerHTML).to.equal('<p id="ugp9bpbybob"><footnote id="hq96xdtozzja" type="footnote">text</footnote><bdi>(PRE, <i>Compendium of Chronicles </i>۲۷)</bdi>.</p>');
    });
});
