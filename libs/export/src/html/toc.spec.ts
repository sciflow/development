import { it, describe } from 'mocha';
import { expect } from 'chai';
import { generateToc } from '../renderers';
import { DocumentNode, SFNodeType, rootDoc } from '@sciflow/schema';

const parts: DocumentNode<rootDoc>[] = [
    {
        type: SFNodeType.part,
        content: [
            {
                type: SFNodeType.document,
                attrs: { numbering: 'none' },
                content: [
                    {
                        type: SFNodeType.heading,
                        attrs: { level: 1 },
                        content: [
                            { type: SFNodeType.text, text: 'Part title (not numbered)' }
                        ]
                    }
                ]
            },
            {
                type: SFNodeType.document,
                attrs: { numbering: 'decimal' },
                content: [
                    {
                        type: SFNodeType.heading,
                        attrs: { level: 2 },
                        content: [
                            { type: SFNodeType.text, text: 'Chapter title (numbered)' }
                        ]
                    }
                ]
            }
        ]
    }
];

const chapters: DocumentNode<SFNodeType.document>[] = [
    {
        type: SFNodeType.document,
        attrs: { numbering: 'none' },
        content: [
            {
                type: SFNodeType.heading,
                attrs: { level: 1 },
                content: [
                    { type: SFNodeType.text, text: 'Abstract' }
                ]
            }
        ]
    },
    {
        type: SFNodeType.document,
        attrs: { numbering: 'decimal' },
        content: [
            {
                type: SFNodeType.heading,
                attrs: { level: 1 },
                content: [
                    { type: SFNodeType.text, text: 'Introduction' }
                ]
            }
        ]
    },
    {
        type: SFNodeType.document,
        attrs: {},
        content: [
            {
                type: SFNodeType.heading,
                attrs: { level: 1 },
                content: [
                    { type: SFNodeType.text, text: 'Methods' }
                ]
            },
            {
                type: SFNodeType.heading,
                attrs: { level: 2 },
                content: [
                    { type: SFNodeType.text, text: 'Expanding on the methods..' }
                ]
            }
        ]
    },{
        type: SFNodeType.document,
        attrs: { numbering: 'none' },
        content: [
            {
                type: SFNodeType.heading,
                attrs: { level: 1 },
                content: [
                    { type: SFNodeType.text, text: 'References' }
                ]
            }
        ]
    },
    {
        type: SFNodeType.document,
        attrs: { numbering: 'alpha' },
        content: [
            {
                type: SFNodeType.heading,
                attrs: { level: 1 },
                content: [
                    { type: SFNodeType.text, text: 'Appendix' }
                ]
            },
            {
                type: SFNodeType.heading,
                attrs: { level: 2 },
                content: [
                    { type: SFNodeType.text, text: 'Expanding on the appendix..' }
                ]
            }
        ]
    }
];

describe('TOC generation', async () => {
    it('Numbers parts and their included chapters', async () => {
        const toc = await generateToc(parts);
        expect(toc.headings.length).to.equal(2);
        expect(toc.headings[0].numberingString).to.be.empty;
        expect(toc.headings[1].numberingString).to.equal('1');
    });

    it('Numbers chapters', async () => {
        const toc = await generateToc(chapters);
        expect(toc.headings.length).to.equal(7);
        expect(toc.headings[0].numberingString).to.be.empty;
        expect(toc.headings[1].numberingString).to.equal('1');
        expect(toc.headings[2].numberingString).to.equal('2');
        expect(toc.headings[3].numberingString).to.equal('2.1');
        expect(toc.headings[4].numberingString).to.equal('');
        expect(toc.headings[5].numberingString).to.equal('A');
        expect(toc.headings[6].numberingString).to.equal('A.1');
    });
});
