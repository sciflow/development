
import { DocumentData, SciFlowDocumentData } from "../interfaces";
import { RenderingEngine } from "../renderers";
import { JSDOM } from 'jsdom';

import { getDefaults, ReferenceRenderer } from '@sciflow/cite';
import { DocumentSnapshot } from '@sciflow/schema';

import { Logger } from 'winston';
import { TemplateComponent } from '../component';
import htmlRenderers from './renderers';
import { mapDoc } from "./convert";
import { PartFileMap } from "./generateListings";

const dom = new JSDOM();
const virtualDocument = dom.window.document;

interface CreateEngineArgs {
    citationRenderer: ReferenceRenderer;
    templateOptions: any;
    /** Legacy document format (@see DocumentSnapshot) */
    document: DocumentData;
    /** The newer document snapshot format */
    documentSnapshot?: DocumentSnapshot;
    metaData?: any;
    metaDataSchema?: any;
    customRenderers?: any;
    configurations?: any[];
    /** The base heading level that determines numbering for all children (1 usually, 2 for nested documents) */
    baseLevel?: number;
    /** A list of template components (classes/functions with the component interface) */
    templateComponents?: TemplateComponent<any, SciFlowDocumentData>[];
    /** The locations to find assets related to component paths */
    componentPaths?: string[];
    runner?: string;
    partFileMap?: PartFileMap;
    logging?: {
        logger?: Logger,
        transactionId?: string;
    }
}

/**
 * Returns a renderer that can render a document.
 */
export const createRenderingEngine = async (data: CreateEngineArgs, debug = false, id?: string): Promise<RenderingEngine<HTMLElement, SciFlowDocumentData>> => {

    data = {
        metaData: {},
        customRenderers: {},
        metaDataSchema: null,
        configurations: [],
        documentSnapshot: undefined,
        ...data
    };

    const engine = new RenderingEngine<HTMLElement, SciFlowDocumentData>(
        {
            ...htmlRenderers,
            ...data.customRenderers
        },
        {
            document: {
                ...data.document
            },
            documentSnapshot: data.documentSnapshot,
            virtualDocument,
            citationRenderer: data.citationRenderer,
            templateOptions: data.templateOptions,
            metaData: data.metaData,
            metaDataSchema: data.metaDataSchema,
            configurations: data.configurations,
            templateComponents: data.templateComponents,
            componentPaths: data.componentPaths,
            partFileMap: data.partFileMap
        },
        [],
        debug,
        data.runner
    );

    if (id) {
        engine.id = id;
    }

    engine.logging = data.logging;

    await engine.generateListings(
        data.document.parts.map(part => mapDoc(part, engine.placement)),
        data.baseLevel,
        data.partFileMap
    );
    return engine;
}
