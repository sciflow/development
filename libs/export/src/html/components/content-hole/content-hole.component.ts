import { Component, PartForRendering, TemplateComponent } from '../../../component';

@Component()
export class ContentHole extends TemplateComponent<any, any> {
    public async renderDOM(defaults?: { runner?: string | undefined; componentPaths?: string[] | undefined; data?: unknown; } | undefined): Promise<PartForRendering | undefined> {
        return {
            id: this.uniqueKey,
            placement: this.page,
            dom: []
        };   
    }
}
