import { Component, PartForRendering, TemplateComponent } from '../../../component';
import { PermissionsMetaConfiguration } from './permissions-meta.schema';

@Component({})
export class PermissionsMeta extends TemplateComponent<PermissionsMetaConfiguration, any> {
}