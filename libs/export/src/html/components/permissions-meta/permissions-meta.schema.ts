import {Permissions} from '../shared/general.interface';

/**
 * @title Permissions
 * @description Copyright statements, ..
 * @_ui_icon copyright
 * @_ui_tag meta
 */
export interface PermissionsMetaConfiguration extends Permissions {}