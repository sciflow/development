import { TSX } from '../../../TSX';
import { TemplateComponent, Component, PartForRendering } from '../../../component';
import { MonographTitleConfiguration } from './monograph-title.schema';

@Component({
    readme: `# Cover page and front section

    The cover and front section of a monograph can be setup through this configuration.`,
    styleUrls: ['./monograph-title.scss'],
    templateUrl: './coverpage.hbs'
})
export class MonographTitle extends TemplateComponent<MonographTitleConfiguration, any> {
    public async renderDOM(defaults?: { runner?: string; componentPaths?: string[]; data?: unknown; }): Promise<PartForRendering | undefined> {
        // we only render a cover page if no cover parts are present
        const staticCover = this.get('staticCover');
        if (this.engine.data.document.parts.some(p => p.options?.placement === 'cover' || p.placement === 'cover') && staticCover !== true) {
            return {
                id: this.uniqueKey,
                placement: this.page,
                dom: []
            };
        }

        const fileId = this.getAs<string>('template.fileId');
        if (fileId) {
            const file = this.engine.data.document.files.find(f => f.id === fileId);
            if (file) {
                if (file.inline && file.content) {
                    this.setTemplate(file.content);
                } else {
                    this.setTemplate(file.url);
                }
            }
        }
        return super.renderDOM(defaults);
    }
}
