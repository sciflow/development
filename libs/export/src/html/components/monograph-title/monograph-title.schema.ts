interface MonographTextProperties {
    /**
     * @title Font size
     */
    fontSize: string;
}

/**
 * @title Monograph title
 * @_ui_icon title
 */
export interface MonographTitleConfiguration {
    /**
     * @title Template
     * @description The template to be used for rendering the content */
    template?: {
        /** A reference to a file */
        fileId?: string;
        /**
         * A preset to be used
         * @examples ["monograph", "journal"]
         */
        preset?: string;
    }

    /**
     * @title Static cover page
     * @default false
     * @description Whether a static cover is generated even if the document contains cover page content.
     */
    staticCover: boolean;
    typography: {
        /**
         * @title Normal text
         */
        paragraph: MonographTextProperties;
        /**
         * @title Largest text size
         */
        h1: MonographTextProperties;
        /**
         * @title Large text size
         */
        h2: MonographTextProperties;
        /**
         * @title Medium text size
         */
        h3: MonographTextProperties;
    }
}
