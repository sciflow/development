import { Component, TemplateComponent } from '../../../component';
import { MetaDataConfiguration } from './meta-data.schema';

@Component({
    readme: `
    # Meta Data
    General meta data for the document`
})
export class MetaData extends TemplateComponent<MetaDataConfiguration, any> {}
