/**
 * @title Meta data
 * @_ui_icon dataset
 * @_ui_tag meta
 */
export interface MetaDataConfiguration {
    /**
     * @title Locale
     * @description The locale to use in the document
     **/
    locale?: string;

    /**
     * @title Citation style
     * @description The citation style name to be used (e.g. apa). You may check https://www.zotero.org/styles for examples
     */
    citationStyle?: string;
    [key: string]: any;
}
