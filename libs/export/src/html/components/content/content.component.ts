import { Component, TemplateComponent } from '@sciflow/export';
import { ContentConfiguration } from './content.schema';

@Component({
    readme: `
    # Content
    General heading structure for initial documents.
    `,
    styles: ``
})
// @ts-ignore
export class Content extends TemplateComponent<ContentConfiguration, any> {}
