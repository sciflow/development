interface Part {
    /**
     * @title ID
     * @default an optional identifier (if needed)
    */
    id?: string;
    /**
     * The type of part
     * @default chapter
     */
    type?: string;
    /**
     * @title Locale
     * @description The chapter's locale (if different from he document)
     */
    locale?: string;
    /**
     * @title A label to put above the chapter UI
    */
    label?: string;
    /**
     * @title role 
     * @description a role like introduction, imprint, ..
     */
    role?: string;
    /**
     * @title Custom page break behavior
     * @examples [["before"], ["after"], ["right"], []]
     */
    pageBreak?: string[];
    /**
     * @title Schema
     * @description An optional schema name for the editor (ProseMirror)
     * @examples ["chapter", "free", "title"]
     */
    schema?: string;
    /**
     * @title Placement
     * @description The placement in the document (front, body, back)
     */
    placement?: string;
    /**
     * Part numbering (none, decimal, ..)
     * @default decimal
     */
    numbering?: string;
    /**
     * Content to start with
     */
    document?: {
        /**
         * The format the document is provided in
         * @default pandoc-markdown 
         */
        type: string;
        /**
         * The actual document
         * @default # Chapter name
         */
        content: string;
    };
    /**
     * @title Skip toc
     * @description Whether to show the part in the table of contents
     */
    skipToc?: boolean;
}

export interface ReferenceContent {
    type: 'csl-json';
    content: string;
}

export interface ContentConfiguration {
    /**
     * The structure of the document.
     */
    parts?: Part[];
    references?: ReferenceContent[];
    /**
     * A file that contains the content (as opposed to the spec configuration)
     */
    fileId?: string;
}
