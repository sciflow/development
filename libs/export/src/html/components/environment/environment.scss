@use "tables";
@use "@sciflow/tdk/styles/debug" as debug;
@use "sass:math";

@debug "Figure with font size - #{get('typography.caption.fontSize', 'no default')}";

figure {
    margin: 0;
}

.figure-wrapper:not([data-role="native-table"]) {
    // don't break inside of figures unless they contain a table
    break-inside: avoid;

    figure {
        break-inside: avoid;
    }
    .figure-body {
        break-inside: avoid;
    }
}

figcaption,
.caption-body {
    break-inside: avoid;

    line-height: #{get("typography.caption.lineHeight")};

    .label {
        white-space-collapse: preserve;
    }
}

td {
    p:first-child {
        text-indent: initial;
    }
}

figcaption,
caption {
    margin: #{get("typography.caption.marginY")} 0 #{get("typography.caption.marginY")} 0;
    line-height: #{get("typography.caption.lineHeight")};
}

// don't break before a fig caption
figcaption {
    break-before: avoid;
    line-height: #{get("typography.caption.lineHeight")};
}

// prevent breaking between the caption and the table/figure if the caption is above
figure[data-caption-side="before"],
figure[data-caption-side="above"] {
    figcaption {
        break-after: avoid;
    }
}

// we need tables to break inside
table {
    td,
    th {
        // word-break: break-word;
        overflow-wrap: break-word;
    }
    tbody {
        break-inside: auto;
        tr:last-child {
            break-before: avoid;
        }
        tr[data-break-after="page"] {
            break-after: page;
        }
    }
}

/* We set up images no never be larger than a page height (or width io case of landscape)
For landscape figures we use prince-shrink-to-fit to scale the image to the page width
The removed rem are to leave room for at least a line or two of captions. */

@if isDebug() {
    img {
        border: thin dotted black;
    }
}

img {
    max-height: calc(
        var(--content-height) - var(--figure-caption-min-height) - #{get("typography.caption.marginY")} - #{get(
                "marginTop"
            )} - #{get("marginBottom")}
    );
    max-width: 100%;
}

.figure-wrapper {
    @if isDebug() {
        background: #eee;
        border: thin dotted black;
        background: rgb(213, 255, 255);
        figure {
            background: #eee;
        }
    }
    --image-max-height: calc(
        var(--content-height) - var(--figure-caption-min-height) - #{get("typography.caption.marginY")} - #{get(
                "marginTop"
            )} - #{get("marginBottom")}
    );
    --caption-max-height: calc(var(--content-height) - var(--image-max-height));

    img {
        margin: 0 auto;
        // object-fit: scale-down; // always make sure images stay within their content box -> this will scale down svgs
    }

    margin: #{get("marginTop")} auto #{get("marginBottom")} auto;

    @if isPagedMedia() {
        // if the figure is snapped to a top or bottom that margin is removed, so we just use the normal body padding
        -prince-margin-alt: calc(#{get("page.bodyPadding", 2mm, "Page")} + (var(--line-height, "6pt") * 0.5));
    }
}

// @if isDebug() {
//     // https://www.princexml.com/doc/cookbook/#image-magic
//     img::after {
//         position: absolute;
//         content: "10% debug quality";
//         color: white;
//         background: black;
//     }
//     img {
//         border: 2px solid orange;
//     }
// }

@if isPagedMedia() {
    table[data-orientation="landscape"] {
        page: wide;
        margin: 0 calc(2 * var(--body-padding));
        padding: 0;
    }
    .figure-wrapper[orientation="landscape"][data-has-notes="false"] {
        // if we do not have notes we can reserve less space, at least 2 lines
        --figure-caption-min-height-landscape: calc(3.5rem);
    }
    // for landscape (only paged media) the width and height properties are swapped
    .figure-wrapper[orientation="landscape"] {
        page: wide;
        margin: 0 calc(2 * var(--body-padding));
        padding: 0;

        [data-role="notes"] {
            padding: 0;
            margin: 0.5rem 0 0 0;
            p {
                margin: 0;
            }
        }

        img {
            // we need to use vw instead of height for landscape
            // the ex signify the lines of caption and notes that should always have room
            max-height: calc(
                var(--content-height-landscape) -
                    var(--figure-caption-min-height-landscape) -
                    #{get("typography.caption.marginY")} -
                    #{get("marginTop")} -
                    #{get("marginBottom")} -
                    calc(2 * var(--body-padding))
            );
            max-width: 100%;
            margin: 0 auto;
        }
    }
}

.inline-math {
    white-space: nowrap;
    svg {
        max-width: 100%;
    }
}

.caption {
    p {
        margin-top: 0.1rem;
    }
}

@each $key in getKeys("environments") {
    // only do non-standard environments
    @if $key != null and not ($key == "image" or $key == "table" or $key == "equation" or $key == "code") {
        figure-wrapper[data-environment="#{$key}"] {
            margin: #{get("environments.#{$key}.margin")};
            @if exists("environments.#{$key}.floats.placement") {
                @if isPagedMedia() {
                    -prince-float-reference: #{get("environments.#{$key}.floats.reference")};
                    -prince-float-placement: #{get("environments.#{$key}.floats.placement")};
                    -prince-float-defer-page: #{get("environments.#{$key}.floats.deferPage")};
                    -prince-float-modifier: #{get("environments.#{$key}.floats.modifier")};
                    -prince-float-policy: #{get("environments.#{$key}.floats.policy")};
                }
            }

            .caption-body {
                @if get("typography.label.float") {
                    display: flex;
                }
                justify-content: #{get("environments.#{$key}.align.caption")};
            }
            .figure-body {
                display: flex;
                justify-content: #{get("environments.#{$key}.align.content")};
            }
        }
    }
}

figure[data-type="equation"] {
    @if get("environments.equation.align.content") == "center" {
        width: 100%;
        .equation {
            width: 100%;
            text-align: center;
        }
    }
    margin: #{get("environments.equation.margin")};
    figcaption {
        font-size: inherit;
        padding: 0 0 0 0.25rem;
        margin: #{get("typography.label.marginRight")} #{get("typography.label.marginRight")};
    }
    .equation {
        padding-top: 0.5rem;
        svg {
            max-width: calc(
                100vw - #{get("page.margins.inner", undefined, "Page")} - #{get(
                        "page.margins.outer",
                        undefined,
                        "Page"
                    )} - 3.5em
            );
            // leave room for the caption of 5em
        }
    }
}

figure[data-image-mime-type="image/svg+xml"] {
    // we need svgs to be able to scale to the full width
    width: 100%;
}

// style tables outside of figures firsst
@include tables.styleTable("plain-outside");

table:not(figure > table) {
    @if get("environments.table.align.content") == "center" {
        margin: #{get("typography.caption.marginY")} auto;
    } @else {
        margin: 1rem 0;
    }
}

@if isDebug() {
    .caption-body,
    .caption,
    .label {
        border: thin dotted orange;
        p {
            border: thin dotted orange;
        }
    }
}

@if get("environments.table.align.content") == "center" {
    figure[data-environment="table"] {
        // table
        table,
        figcaption {
            margin: #{get("typography.caption.marginY")} auto;
        }

        .caption-body {
            @if get("typography.label.float") {
                display: flex;
            }
            justify-content: #{get("environments.table.align.caption")};
        }

        // in case the content is an image (that shows a table
        .figure-body {
            display: flex;
            justify-content: #{get("environments.table.align.content")};
        }
    }
}

@if exists("environments.image.floats.placement") {
    @if isDebug() {
        .figure-wrapper[data-environment="image"]:before {
            position: absolute;
            background: white;
            color: black;
            transform: translateY(2rem);
            content: "float placement: "
                attr(data-float-placement, '#{get("environments.table.floats.placement")}')
                "\A modifier\:  "
                attr(data-float-modifier, '#{get("environments.table.floats.modifier")}')
                "\A defer: "
                attr(data-float-defer-page, '#{get("environments.table.floats.deferPage")}');
        }
    }
    @if isPagedMedia() {
        .figure-wrapper[data-environment="image"] {
            -prince-float-reference: #{get("environments.image.floats.reference")};
            -prince-float-placement: #{get("environments.image.floats.placement")};
            -prince-float-defer-page: #{get("environments.image.floats.deferPage")};
            -prince-float-modifier: #{get("environments.image.floats.modifier")};
            -prince-float-policy: #{get("environments.image.floats.policy")};
        }
    }
}

@if exists("environments.table.floats.placement") {
    @if isDebug() {
        .figure-wrapper[data-environment="table"]:before {
            position: absolute;
            background: white;
            color: black;
            content: "float placement: "
                attr(data-float-placement, '#{get("environments.table.floats.placement")}')
                "\A modifier\:  "
                attr(data-float-modifier, '#{get("environments.table.floats.modifier")}')
                "\A defer: "
                attr(data-float-defer-page, '#{get("environments.table.floats.deferPage")}');
        }
    }
    @if isPagedMedia() {
        .figure-wrapper[data-environment="table"] {
            -prince-float-reference: #{get("environments.table.floats.reference")};
            -prince-float-placement: #{get("environments.table.floats.placement")};
            -prince-float-defer-page: #{get("environments.table.floats.deferPage")};
            -prince-float-modifier: #{get("environments.table.floats.modifier")};
            -prince-float-policy: #{get("environments.table.floats.policy")};
        }
    }
}

@if isPagedMedia() {
    .figure-wrapper[data-float-placement="none"] {
        -prince-float-placement: none;
    }
    .figure-wrapper[data-float-placement="snap"] {
        -prince-float-placement: snap;
    }
    .figure-wrapper[data-float-placement="top-bottom"] {
        -prince-float-placement: top-bottom;
    }
    .figure-wrapper[data-float-placement="top"] {
        -prince-float-placement: top;
    }
    .figure-wrapper[data-float-placement="bottom"] {
        -prince-float-placement: bottom;
    }

    .figure-wrapper[data-float-modifier="unless-fit"] {
        -prince-float-modifier: unless-fit;
    }
    .figure-wrapper[data-float-modifier="normal"] {
        -prince-float-modifier: normal;
    }
}

@if get("environments.image.align.content") == "center" {
    figure[data-environment="image"] {
        figcaption {
            margin: #{get("typography.caption.marginY")} auto;
        }

        .caption-body {
            text-align: #{get("environments.image.align.caption")};
        }

        // in case the content is an image (that shows a table
        .figure-body {
            display: flex;
            justify-content: #{get("environments.image.align.content")};
        }
    }
}

pre {
    font-size: #{get("environments.code.fontSize")};
}

@if get("environments.code.align.content") == "center" {
    figure[data-environment="code"] {
        pre,
        figcaption {
            margin: #{get("typography.caption.marginY")} auto;
        }

        .caption-body {
            display: flex;
            justify-content: #{get("environments.code.align.caption")};
        }

        // in case the content is an image (that shows a table
        .figure-body {
            display: flex;
            justify-content: #{get("environments.code.align.content")};
        }
    }
}

// make sure figcaptions do not have extra spacing on top if they are above the table/figure image
figure[data-caption-side="before"],
figure[data-caption-side="above"] {
    figcaption {
        // reset the margin top since the margin starts above
        margin: 0 0 #{get("typography.caption.marginY")} 0;
    }
}

:not([dir="rtl"]) {
    figure:not([data-type="equation"]) {
        .label {
            margin-right: #{get("typography.label.marginRight")};
        }
    }
}
[dir="rtl"] {
    figure {
        .label {
            margin-left: #{get("typography.label.marginRight")};
        }
    }
}

figure {
    @if get("typography.label.float") {
        table {
            caption {
                // we cannot set flex on the table caption directly or princexml will break
                .caption-body {
                    display: flex;
                    .label {
                        display: block;
                        margin-right: #{get("typography.label.marginRight")};
                    }
                    .caption {
                        text-align: start;
                    }
                }
            }
            [data-caption-side="above"] {
                caption-side: top;
            }

            [data-caption-side="below"] {
                caption-side: bottom;
            }
        }
        figcaption {
            .caption-body {
                display: flex;
                .label {
                    display: block;
                    margin-right: #{get("typography.label.marginRight")};
                }
            }
        }
    }
    .label {
        font-weight: #{get("typography.label.fontWeight")};
        font-style: #{get("typography.label.fontStyle")};
        white-space: nowrap;
    }

    figcaption {
        font-size: #{get("typography.caption.fontSize")};
        margin: #{get("typography.caption.marginY")} auto #{get("typography.caption.marginY")} auto;
        @if not get("renderCaptionsInline") {
            &:before {
                font-weight: bold;
                display: block;
                @debug "Figure image label - #{get('environments.image.label')}";
                content: "#{get('environments.image.label')} " attr(data-figure-number) " ";
            }
        }
    }
    + [data-role="notes"] {
        margin: 0.5rem 0.5rem 1rem 1rem;
        font-size: #{get("typography.caption.fontSize")};
        p:first-child {
            text-indent: initial;
        }
    }
    table {
        display: table;
        border-collapse: collapse;
        table-layout: fixed;
        margin-top: 0.25rem;
        min-width: 16rem;
        tr {
            th,
            td {
                p {
                    // white-space: pre-wrap;
                }
            }
        }

        @debug "Figure table captionSide - #{get('environments.table.captionSide')}";
        caption {
            text-align: start;
            @if isPagedMedia() {
                prince-caption-page: first;
            }
            font-size: #{get("typography.caption.fontSize")};
            margin-top: 0.25rem;
            @if #{get("environments.table.captionSide")} == "above" {
                caption-side: top;
                margin-top: 0;
            }
            @if #{get("environments.table.captionSide")} == "below" {
                caption-side: bottom;
            }
            @if isPagedMedia() {
                -prince-caption-page: #{get("environments.table.captionPage")};
            }
            @if not get("renderCaptionsInline") {
                &:before {
                    font-weight: bold;
                    display: block;
                    @debug "Figure table label - #{get('environments.table.label')}";
                    content: "#{get('environments.table.label')} " attr(data-figure-number) " ";
                }
            }
        }
        th,
        td {
            padding: 0.25rem;
            text-indent: initial;
            p {
                text-indent: initial;
            }
        }
    }

    @include tables.styleTable(get("environments.table.theme"));

    @if isDebug() {
        border: thin dotted black;
    }
}

figure[data-caption-side="before"] {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;

    figcaption {
        text-align: left;
    }
}

figure[data-caption-side="after"] {
    display: flex;
    justify-content: space-between;
    align-items: center;

    figcaption {
        text-align: right;
        margin-right: 0.125rem;
    }
}

// scale figures to fit the page by scaling down their width
figure[scale-width]:not([scale-width="1"]) {
    // reduce margins for any scale
    table p {
        margin: 0;
        padding: 1px;
    }
}

table {
    caption {
        min-width: 12rem;
    }
    th,
    td {
        vertical-align: top;
        text-indent: initial;
        font-size: #{get("environments.table.fontSize")};
        line-height: #{get("environments.table.lineHeight")};

        p {
            font-size: #{get("environments.table.fontSize")};
            line-height: #{get("environments.table.lineHeight")};
            text-indent: initial;
        }
    }
}

/**
 Scale figures to fit the page by scaling down their height to a fraction of the available height on a page
*/
@mixin scale-figure($scale) {
    $scalePercent: $scale * 100%;

    // -1 means we try not to influence image size at all
    @if $scale == -1 {
        .figure-wrapper[data-scale-width="-1"]:not([data-role="native-table"]) {
            .figure-body {
                display: flex;
                img {
                    width: unset;
                    height: unset;
                }
            }
        }
    } @else {
        .figure-wrapper[data-scale-width="#{$scale}"]:not([data-role="native-table"]) {
            .figure-body {
                display: flex;
                img {
                    width: $scalePercent;
                    height: auto;
                }
            }
        }

        figure[scale-width="#{$scale}"] {
            table {
                // scale table fonts to make more room
                font-size: #{$scale}rem;
            }
        }

        // scale standalone tables without a figure wrapper
        table[data-scale-width="#{$scale}"] {
            font-size: #{$scale}rem;
        }
    }

    @if $scale == -1 {
        // this will only target images without a wrapping figure
        .figure-body[scale-width="-1"] {
            display: inline;
            transform: unset;
            img {
                width: unset;
                height: unset;
            }
        }
    } @else {
        // this will only target images without a wrapping figure
        .figure-body[scale-width="#{$scale}"] {
            transform: scale($scale);
        }
    }

    @if $scale == -1 {
        figure[scale-width="-1"][orientation="landscape"] {
            .caption-body {
                margin: 0 auto;
            }
        }
    } @else {
        figure[scale-width="#{$scale}"][orientation="landscape"] {
            .caption-body {
                /* #FIXME 1171 margin: 0 calc(100vh * #{math.div(1 - $scale, 2)}); */
            }
        }
    }
}

@if not isRunner("epub") {
    @include scale-figure(-1);
    @include scale-figure(0.9);
    @include scale-figure(0.75);
    @include scale-figure(0.5);
    @include scale-figure(0.25);
}

// reset table caption font-size
table {
    caption {
        font-size: #{get("typography.caption.fontSize")};
        padding-bottom: 0.25rem;
    }
}

ol[data-list-style-type="upper-roman"] {
    list-style-type: upper-roman;
}

ol[data-list-style-type="lower-roman"] {
    list-style-type: lower-roman;
}

ol[data-list-style-type="lower-alpha"] {
    list-style-type: lower-alpha;
}

ol[data-list-style-type="upper-alpha"] {
    list-style-type: upper-alpha;
}

ol[data-list-style-type="decimal-brackets"] {
    counter-reset: list;
    li {
        counter-increment: list;
    }
    li::marker {
        content: "[" counter(list) "]";
    }
}

@if isRunner("epub") {
    .figure-body {
        display: flex;
        flex-direction: column;
    }
    figure[data-type="equation"] {
        display: flex;
        justify-content: space-between;
        .equation {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            figcaption {
                padding: 0.2rem 0 0.1rem 1.5rem;
            }
        }
    }

    @media (prefers-color-scheme: dark) {
        figure[data-type="equation"] {
            background-color: #757575;
        }
    }
}
