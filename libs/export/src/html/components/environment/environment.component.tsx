import { TSX } from '../../..';
import { TemplateComponent, Component } from '../../../component';
import { EnvironmentConfiguration } from './environment.schema';
@Component({
    readme: `
    # Environments
    Environments are wrappers around special elements like figures, poems, lemmas,.. Uually they have a label to be referenced or listed.
    Standard environments are the figure environments: image, table, code, and math.
    `,
    styleUrls: ['./environment.scss']
})
export class Environment extends TemplateComponent<EnvironmentConfiguration, any> {}
