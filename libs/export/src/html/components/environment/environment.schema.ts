/**
 * Figure component schema (extending the base schema needed by the figure renderer)
 */

import { FigureRendererConfiguration } from "../../renderers/figure/figure.schema";

/**
 * @title Figures
 * @_ui_icon perm_media
 * @description Customization of figures, tables, equations and more.
 */
export interface EnvironmentConfiguration extends FigureRendererConfiguration {
    /**
     * @title The default spacing above a figure
     * @default 1rem
     */
    marginTop: string;
    /**
     * @title The default spacing below a figure
     * @default 1rem
     */
    marginBottom: string;
    typography: {
        caption: {
            /**
             * @title Font size
             * @default 9pt
             */
            fontSize: string;
            /**
             * @title Caption margins above and below
             * @default 0.25rem
             */
            marginY: string;
            /**
             * @title Label line height
             * @default inherit
             */
            lineHeight: number | string;
        }
        label: {
            /**
             * @title Label font weight
             * @default bold
             */
            fontWeight: string | number;
            /**
             * @title Label font style
             * @default normal
             */
            fontStyle: 'normal' | 'italic' | 'oblique';
            /**
             * @title Float
             * @description Whether to float the label to the left of the caption (for multi-line captions that need to align vertically)
             * @default false
             */
            float: boolean;
            /**
             * @title Spacing after the label.
             * @default 0.35rem
             */
            marginRight: string;
        }
    }
}
