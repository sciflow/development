/**
 * Represents a group of related variants that cannot be applied simultaneously.
 * Used to prevent conflicting template modifications.
 */
export interface VariantGroup {
  name: string;
  /** Display name shown in user interfaces */
  title: string;
  /** Brief explanation of what variants in this group modify */
  description: string;
}

/**
 * Represents a predefined combination of compatible variants
 */
export interface SubTemplate {
  name: string;

  /** Display name shown in user interfaces */
  title: string;

  /** Brief explanation of the sub-template's purpose and usage */
  description: string;

  /**
   * Ordered list of variant names to apply.
   * Order determines how variants are applied to prevent conflicts.
   */
  variantOrder: string[];
}

/**
 * @title Template configuration
 */
export interface ConfigurationConfiguration {
  slug?: string; 

  /** The template name */
  title: string;

  /** Template description */
  description?: string;

  /**
   * @title Type
   * @description The type of template. For monographs use thesis.
   */
  type: 'thesis' | 'journal' | 'general' | 'sciflow' | 'scientific' | 'monograph';
  readme?: string;
  citationStyle: {id: string};

  language?: string;
  languageAlternatives?: string[];

  /** A list of resources used in the template */
  assets: {
    id: string;
    type: 'hbs' | 'logo' | string;
    path: string;
    /** The locales the asset is available in */
    locales?: string[];
    /** The role of the asset */
    role?: 'logo' | 'favicon' | 'image' | 'icon' | 'other';
    /** The dimensions of the asset */
    dimensions?: {
      width: string;
      height: string;
    };
    /** Whether the content should be inlined when compiled */
    inline?: boolean;
    /** The content of the file (if content was inlined) */
    content?: string;
    /** Prevents files from being removed from exports even if they are not directly referenced */
    static?: boolean;
  }[];

  /**
   * Groups of mutually exclusive variants that modify similar template aspects.
   */
  variantGroups?: VariantGroup[];

  /**
   * Pre-defined combination of variants, where latest takes precedence.
   */
  subTemplates?: SubTemplate[];

  /** Any additional keys are allowed for legacy reasons but should be avoided in new configurations */
  [key: string]: any;
}
