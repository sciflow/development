import {Component, TemplateComponent} from '../../../component';
import {ConfigurationConfiguration} from './configuration.schema';

@Component({
  readme: `
    # Template configuration`,
})
export class Configuration extends TemplateComponent<ConfigurationConfiguration, any> {}
