/**
 * @title Article Metadata
 * @description Metadata for the article
 * @_ui_icon article
 * @_ui_tag meta
 */
export interface ArticleMetaConfiguration {

  /**
   * @title Alt title
   * @description An alternative title for the publication (e.g. to be used as a short version)
   */
  altTitle?: string;

  /**
   * @title Article version
   * @description The version of the article.
   */
  version: 'draft' | 'submitted' | 'accepted' | 'proof' | 'version of record';

  /**
   * @title Article type
   */
  articleType: string;

  /**
   * @title Volume
   */
  volume: string;

  /**
  * @title Issue
  */
  issue: string;

  /**
   * @title IDs needed for publication
   */
  identifiers: {
    /**
     * @title Publisher ID
     * @description The publisher ID, e.g., 00778 for eLife.
     */
    publisher: string;

    /**
     * @title The DOI
     * @description The assigned DOI (e.g., 10.0000/example.00123).
     */
    doi: string;
  };
}
