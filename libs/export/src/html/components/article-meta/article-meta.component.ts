import {Component, TemplateComponent} from '../../../component';
import {ArticleMetaConfiguration} from './article-meta.schema';

@Component({})
export class ArticleMeta extends TemplateComponent<ArticleMetaConfiguration, any> {}
