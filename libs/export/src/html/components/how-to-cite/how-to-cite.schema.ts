export interface HowToCiteConfiguration {
    /**
     * @title Title
     * @default How to cite
     */
    title: string;
    /**
     * @title A locale for the title (other than the document locale)
     */
    locale: string;
    /**
     * @title Append the subtitle to the title.
     * @default ": "
     * @description If set, it uses the provided character to concatenate title: subtitle (must contain spaces if needed).
     */
    appendSubtitle: string;
    /** 
     * @title Text direction
     * @description A text direction other than the document's default direction.
     */
    dir: 'ltr' | 'rtl' | 'auto';
}