import { TSX } from '../../../TSX';
import { TemplateComponent, Component, PartForRendering } from '../../../component';
import { HowToCiteConfiguration } from './how-to-cite.schema';
import { getTextDirection } from '../../../helpers';
import { renderStaticBibliography } from '@sciflow/cite';

@Component({
    readme: `# How to cite
    
    If publication meta data was configured, this configuration can automatically generate a 
    citation in the publication's citation style to serve as a recommended citation.`,
    styles: `
    .how-to-cite h1 { margin-top: 0; }
    @if isPagedMedia() {
        .how-to-cite {
            -prince-float-reference: page;
            -prince-float-placement: bottom;
            .csl-left-margin {
                display: none;
            }
        }
    }`
})
export class HowToCite extends TemplateComponent<HowToCiteConfiguration, any> {
    public async renderDOM({ runner, componentPaths }): Promise<PartForRendering | undefined> {
        const renderer = this.engine.data.citationRenderer;
        const result: any = renderStaticBibliography([this.getCitationWith(this.get('appendSubtitle'))], renderer.styleXML, renderer.localeXML);

        if (result.entries.length > 0) {
            const entry = result.entries[0]?.renderedString || '';
            const locale = this.get('locale');
            const title = this.get('title', locale);
            const section = <section class="how-to-cite py1 m0">
                <h1>{title}</h1>
                {TSX.htmlStringToElement(entry)}
            </section>;

            if (locale) {
                section.setAttribute('lang', locale);
                section.setAttribute('dir', getTextDirection(locale));
            }

            const dir = this.get('dir');
            if (dir) {
                section.setAttribute('dir', dir);
            }

            return {
                id: 'how-to-cite',
                placement: this.page || 'front',
                dom: [section]
            }
        }

        return {
            id: 'how-to-cite',
            placement: this.page || 'front',
            dom: []
        }
    }
}
