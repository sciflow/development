export interface CommentedExample {
  /**
   * @ignore
   * A comment on a provided example.
   */
  _comment?: string;
}

/**
 * @title Translated Titles
 * @description Group of translated titles including main title and subtitle.
 */
export interface TransTitleGroup {
  /**
   * @title Language
   * @description Language of the translated title (e.g. en, fr, de)
   */
  language: string;

  /**
   * @title Translated Title
   * @description Translated main title.
   */
  transTitle: string;

  /**
   * @title Translated Subtitle
   * @description Translated subtitle.
   */
  transSubtitle?: string;
}

/**
 * @title Title Group
 */
export interface TitleGroup {
  // part of the document
  //title: string;

  // part of the document
  //subtitle?: string;

  /**
   * @title Alternative Title
   * @description An alternative or different version of the title (e.g. a shorter version). Please use the trans title for translations.
   * @_ui_widget sfo-rich-text
   */
  altTitle?: string;

  /**
   * @title Translated Titles
   * @description Translations of the title into other languages
   */
  transTitleGroup?: TransTitleGroup[];
}

/**
 * @title Permissions
 * @description Copyright statements and licenses.
 */
export interface Permissions {
  /**
   * @title Copyright statement
   * @description A copyright statement (e.g. (c) X 2021).
   */
  copyrightStatement?: string;

  /**
   * @title Copyright year
   */
  copyrightYear?: number;

  /**
   * @title Copyright holder
   */
  copyrightHolder?: string;

  /**
   * @title License type
   * @default open-access
   */
  licenseType?: string;

  /**
   * @title License link
   *  @description A link to the license (e.g. http://creativecommons.org/licenses/by/2.0/)
   */
  licenseLink?: string;

  /**
   * @title License text
   */
  licenseP?: string;
}

export interface Publisher {
  /**
   *  @title Publisher id
   *  @description The publisher id, e.g. 00778 for eLife.
   */
  publisher?: string;
}

export interface ISSN {
  /**
   * @title Publication Format
   */
  publicationFormat: 'electronic' | 'print' | 'video' | 'audio' | 'ebook' | 'online-only' | '';

  /**
   * @title ISSN
   */
  issn: string;
}

export interface Meta {
  /**
   * @title ISBN
   * @description The International Standard Book Number
   */
  isbn?: string;

  /**
   * @title ISSN
   * @description The International Standard Serial Number
   */
  issn?: ISSN;

  /**
   * @title Publisher
   * @description Information about the book's publisher
   */
  publisher: {
    /**
     * @title Publisher name
    */
    publisherName: string;
  }
}

/**
 * @title Contributor Group
 * @description Group of contributors for the book
 */
export interface ContribGroup {
  /** @title Contributors
   *  @description Array of contributors
   */
  contrib: Contrib[];
}

/**
 * @title Contributor
 * @description Individual contributor details
 */
export interface Contrib {
  /**
   * @title Contributor Type
   * @description The type of contributor (e.g., author, editor).
   * @example "author"
   */
  contribType: string;

  /**
   * @title Contributor Name
   * @description The name of the contributor.
   */
  name: Name;

  /**
   * @title Affiliation
   * @description Affiliation of the contributor.
   * @example "Nature Publishing Group"
   */
  aff?: string;
}

/**
 * @title Name
 * @description The name of the contributor
 */
export interface Name {
  /**
   * @title Surname
   * @description The surname of the contributor.
   */
  surname: string;

  /**
   * @title Given Names
   * @description The given names of the contributor.
   */
  givenNames: string;
}

/**
 * @title Publication Date
 * @description The date of publication
 */
export interface PubDate {
  /**
   * @title Publication Format
   * @description The format of publication.
   * @example "print"
   */
  publicationFormat?: 'print' | 'electronic' | 'video' | 'audio' | 'ebook' | 'online-only';
  /**
   * @title Data Type
   * @description The type of data.
   * @example "pub"
   */
  dataType?: 'pub' | 'preprint' | 'accepted' | 'retracted';
  /**
   * @title Date of Publication
   * @_ui_widget sfo-date-input
   */
  date: string;
}
