import { countryToLabel, extractHeadingTitle, getTextDirection } from '../../../public-api';
import { TSX } from '../../../TSX';
import { TemplateComponent, Component, PartForRendering } from '../../../component';
import { AuthorSectionConfiguration } from './author-section.schema';

const counterStyles = {
    symbols: ['*', '**', '***']
};

@Component({
    readme: `# Author list
    Creates the author section with a long form author list`,
    styleUrls: ['./author-section.scss']
})
export class AuthorSection extends TemplateComponent<AuthorSectionConfiguration, any> {

    format = new Intl.DateTimeFormat(this.engine.data.document.locale || 'en-US', { month: 'long', day: 'numeric', year: 'numeric', timeZone: 'UTC' });
    formatDate = (date: Date) => date ? this.format.format(new Date(date)) : '';

    public async renderDOM(defaults?: { runner?: string; componentPaths?: string[]; data?: unknown; }): Promise<PartForRendering | undefined> {

        const authors = this.engine.data.document.authors.filter(author => !author.roles || author.roles.includes('author'));
        const locale = this.get('locale') || this.engine.data.document.locale;

        /**
         * Creates a HTML string of the author names.
         */
        const getName = (author): HTMLSpanElement => {
            if (author?.firstName || author?.lastName) {
                return <span>{[author.firstName ? <span class="first-name">{author.firstName}</span> : null, TSX.createTextNode(' '), author.lastName ? <span class="last-name">{author.lastName}</span> : null]
                    .filter(n => n !== null)}
                </span>
            }
            if (author?.name) { return <span>author.name</span>; }
            return <span>Unknown author</span>;
        }

        const createInstitutionId = (institution) => institution.slug || [institution.department, institution.institution, institution.city].join('-');
        const correspondingAuthors = authors.filter(author => author.email && author.correspondingAuthor);
        const authorsWithComments = authors.filter(a => a.comment && a.comment?.length > 0);

        const getAuthorCommentSymbol = (author, list) => counterStyles.symbols[list.findIndex(a => a.authorId === author.authorId)];

        const section = <section class="authors">{
            ...authors.map((author) => {

                let affiliations: any = author.positions?.map((position) => {
                    position.slug = position.slug ?? createInstitutionId(position);
                    return position;
                });

                const affiliationEls = affiliations.map((affiliation) => (<span>
                    <address>{[
                        affiliation.institution,
                        affiliation.department,
                        affiliation.city,
                        affiliation.country ? countryToLabel(affiliation.country, locale) : undefined
                    ].filter(l => l?.length > 0).join(', ')}
                    </address>
                    {author.orcid ? <p class="orcid">ORCID: <a href="https://orcid.org/{author.orcid}" target="_blank" rel="noopener">{author.orcid}</a></p> : ''}
                    {author.email && !author.correspondingAuthor ? <p>Email: {author.email}</p> : ''}
                </span>));

                return <div class="author mb2">
                    <p>{getName(author)}
                        {author.comment ? <sup>{getAuthorCommentSymbol(author, authorsWithComments)}</sup> : ''}
                    </p>
                    <div class="affiliations">
                        {...affiliationEls}
                    </div>
                </div>;
            })
        }
            {correspondingAuthors.length > 0 ? <div class="corresponding mt2 mb1">Correspondence:&nbsp;
                {...correspondingAuthors.map(author => <span class="pr1">
                    {author.email}
                </span>)}
            </div> : ''}
            {authorsWithComments?.length > 0 ?
                <div class="author-comments pb1">
                    {authorsWithComments.map(author => <span>
                        <span>{getAuthorCommentSymbol(author, authorsWithComments)}&nbsp;</span>
                        {TSX.htmlStringToElement(author?.comment || '')}
                    </span>)}
                </div> : ''}
        </section>;

        if (locale) {
            section.setAttribute('lang', locale);
            section.setAttribute('dir', getTextDirection(locale));
        }

        const dir = this.get('dir');
        if (dir) {
            section.setAttribute('dir', dir);
        }

        return {
            id: 'author-section',
            placement: this.page || 'cover',
            dom: [section]
        };
    }
}
