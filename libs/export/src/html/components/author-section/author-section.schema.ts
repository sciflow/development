export interface AuthorSectionConfiguration {
    /**
     * @title A locale for the title (other than the document locale)
     */
    locale: string;
    
    typography: {
        /**
         * @title Title
         */
        title: {
            /**
             * @title Font size
             */
            fontSize: string;
            /**
             * @title Font weight
             * @default inherit
             */
            fontWeight: string | number;
        };
        /**
         * @title Authors
         */
        authors: {
            /**
             * @title Font size
             */
            fontSize: string;
            /**
             * @title Font weight
             * @default inherit
             */
            fontWeight: string | number;
        };
        affiliations: {
            /**
             * @title Font size
             */
            fontSize: string;
            /**
             * @title Font weight
             * @default inherit
             */
            fontWeight: string | number;
        }
        comments: {
            /**
             * @title Font size
             */
            fontSize: string;
            /**
             * @title Font weight
             * @default inherit
             */
            fontWeight: string | number;
        }
    }
    /** 
     * @title Text direction
     * @description A text direction other than the document's default direction.
     */
    dir: 'ltr' | 'rtl' | 'auto';
}