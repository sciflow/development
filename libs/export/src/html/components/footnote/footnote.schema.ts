import { FootnoteRendererConfiguration } from '../../renderers/footnote/footnote.schema';

/**
 * @title Footnotes
 * @description Apperance of footnotes (or endnotes)
 * @_ui_icon format_list_numbered
 * @_readme This configuration allows you to customize footnotes in HTML and PDF outputs, adjusting font size, line height, text alignment, and layout. It manages footnote placement, marker styling, and spacing, while also providing options for endnote titles, linking, and part exclusions. These settings ensure footnotes and endnotes are adaptable to different document formats and styles.
 * @examples require('./footnote.example.js').examples
 * 
 */
export interface FootnoteConfiguration extends FootnoteRendererConfiguration {
    /**
     * @title Font size
     * @default 9pt
     */
    fontSize?: string;
    /**
     * @title Font family
     */
    fontFamily?: string;
    /**
     * @title Line height
     * @description Lineheight within footnotes.
     * @default 1
     */
    lineHeight?: string | number;
    /**
     * @title Font weight for the footnote
     * @default initial
    */
    fontWeight?: string | number;
    /**
     * @title Text align
     * @default start
     */
    textAlign?: 'start' | 'justify' | 'center' | 'end';
    /**
     * @title Break footnotes up inside.
     * @description if set to auto, footnotes will be placed on a page even if they would continue on the next page
     * @default avoid
     */
    breakInside: 'auto' | 'avoid';
    /**
     * @title Footnote style
     * @description The main styles of footnote styling. Outside will write the footnote marker into the page margin instead of the footnote area.
     * Compact is suitable for documents with a lot of footnotes that need to fit. Footnotes may be collapsed into single lines if needed.
     * @default inside
     */
    style?: 'inside' | 'outside' | 'compact';
    footnote?: {
        /**
        * @title Margin of individual footnotes
        * @description Margins of footnotes. Add a left margin to move the footnote into the page (independently of the marker)
        * @default 0 0 0.125rem 0.85rem
        */
        margin: string;
    }
    margin?: {
        /**
         * @title Top margin of the footnote region
         * @description The spacing between the start of the footnotes and the text body (in addition to any body padding)
         * @default 0.5rem
         */
        top?: string;
        /**
         * @title Bottom margin of the footnote region
         * @description The spacing between the end of the footnotes and the footer
         * @default 1rem
         */
        bottom?: string;
    }
    /** 
     * @title Footnote call
     * @description Settings for the in-text footnote call
     */
    call?: {
        /**
         * @title Font size for the footnote/endnote call (numnber in the text)
         * @default 70%
        */
        fontSize?: string;
    }
    /**
     * @title Marker
     * @description Settings for the footnote marker in the footnote
     */
    marker?: {
        /**
         * @title Font size for the footnote marker (numnber in the footnote)
         * @default 9pt
        */
        fontSize?: string;
        /**
         * @title Suffix for the footote counter.
         * @default .
         */
        suffix?: string;
        /**
         * @title The alignment of the marker
         * @default auto
         */
        align?: 'auto' | 'super';
        /**
         * @title Space after marker
         * @description The distance between the footnote marker and the footnote body.
         * @default 0.25rem
         */
        spaceAfter?: string | number;
    }
    /**
     * @title Separator style
     * @default none
     */
    separatorStyle?: 'none' | 'line' | 'clipped-line';
    /**
     * @title Footnote policy
     * @default auto
     * @description Determines whether a footnote has to be displayed on the same page as the in-text footnote call
     */
    policy?: 'auto' | 'keep-with-line' | 'keep-with-block';
    /**
     * @title Endnote
     * @description Endnote settings (if applicable)
     */
    endnote?: {
        /**
         * @title Float the endnote number
         * @default false
         */
        floatNumber?: boolean;
        /**
         * @title Endnote section title
         * @default Notes
         * @description A heading that preceeds the endnotes sections (if mode is endnote)
        */
        title?: string;
        /**
         * @title Link from endnote to text
         */
        linkToText: undefined | 'link-and-icon' | 'link';
        /**
         * @title Exclude endnotes from parts
         * @description Whether endnotes from parts are excluded (e.g. because they have their own listing)
         * @default false
         */
        excludeParts: boolean;
    }
}
