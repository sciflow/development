const examples = [
  {
    _comment: 'Superscript footnote number with spacing',
    style: 'outside',
    footnote: {
      margin: '0 0 0.125rem 0.5rem'
    },
    fontSize: '8pt',
    marker: {
      align: 'super',
      suffix: '',
      fontSize: '7pt',
      spaceAfter: '0.75rem'
    },
    separatorStyle: 'clipped-line',
    margin: {
      bottom: '1cm'
    }
  }
];

module.exports = {
  examples
}