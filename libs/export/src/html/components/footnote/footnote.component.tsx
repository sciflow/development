import { DocumentNode, SFNodeType } from '@sciflow/schema';
import { TSX } from '../../../TSX';
import { Component, PartForRendering, TemplateComponent } from '../../../component';
import { FootnoteConfiguration } from './footnote.schema';
import { CitationRenderer } from '../../renderers/citation';

/**
 * Styles footnotes in PagedMedia
 * Also handles the endnote output, if needed.
 * @see https://www.princexml.com/howcome/2022/guides/footnotes/
 * @see https://www.w3.org/TR/css-gcpm-3/#footnote-terms
 */
@Component({
    styleUrls: ['./footnote.component.scss']
})
export class Footnote extends TemplateComponent<FootnoteConfiguration, any> {
    /**
     * Returns a list of all endnote items.
     */
    public getEndnoteItems() {
        if (!this.engine.data.document.tables) { throw new Error('Tables must be generated at this point'); }
        const citationsInFootnotes = this.engine.data.citationRenderer.footnotes === true;
        let endnoteItems = citationsInFootnotes ?
            this.engine.data.document.tables.footnotesAndCitations
            : this.engine.data.document.tables.footnotes;
        const excludeParts = this.get('endnote')?.excludeParts;
        if (excludeParts === true) {
            // filter out all endnotes from parts
            endnoteItems = endnoteItems.filter(note => {
                const part = this.engine.data.document.parts.find(p => p.partId === note.partId);
                return part?.type !== SFNodeType.part;
            });
        }

        return endnoteItems// we do not convert footnotes inside of footnotes (since they will be rendered anyways)
            .filter(element => element.parentType !== SFNodeType.footnote);
    }

    public async renderDOM(): Promise<PartForRendering | undefined> {
        const linkEndnoteToText = this.getAs('endnote.linkToText');
        if (this.get('mode') === 'endnote') {
            const endnoteItems = this.getEndnoteItems();
            const endnotes: any[] = await Promise.all(endnoteItems
                .map(async (element, index) => {
                    let content;
                    if (element.type === SFNodeType.footnote) {
                        // only render the content
                        content = await this.engine.renderContent(element);
                    } else if (element.type === SFNodeType.citation) {
                        // e,g. a citation
                        const citationRenderer = this.engine.renderers[SFNodeType.citation] as CitationRenderer;
                        const citation = await citationRenderer.renderCitation(element as DocumentNode<SFNodeType.citation>, false);
                        content = [citation];
                    } else {
                        throw new Error('Expecting a footnote or citation as endnote elements');
                    }

                    try {
                        const endnoteId = element?.attrs?.id ?? (index + 1);
                        const body = <span data-type="endnote-body">{...content}</span>;
                        if (element?.attrs?.lang) { body.setAttribute('lang', element.attrs.lang); }

                        return <div data-type="endnote" id={'en' + endnoteId}>
                            {linkEndnoteToText != undefined ? <a href={'#enref' + endnoteId} target="_self"><span data-type="endnote-marker">{index + 1}</span></a> : <span data-type="endnote-marker">{index + 1}</span>}
                            {body}
                            {linkEndnoteToText === 'link-and-icon' ? <a href={'#enref' + endnoteId} target="_self">&nbsp;↩︎</a> : ''}
                        </div>;
                    } catch (e: any) {
                        this.engine.log('error', 'Could not render endnote', { error: e.message, node: element });
                        debugger;
                        return <div>Could not render endnote {element?.attrs?.id}</div>;
                    }
                }));

            if (endnotes.length === 0) { return undefined; }
            const title = this.getAs<string>('endnote.title') || 'Notes';
            return {
                id: 'endnotes',
                placement: this.subPage || this.page || 'back',
                dom: [<section class="endnotes">
                    {this.page === 'part' ? <h2>{title}</h2> : <h1>{title}</h1>}
                    {...endnotes}
                </section>]
            };
        }
    }
}
