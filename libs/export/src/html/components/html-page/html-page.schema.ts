/**
 * @title HTML appearance
 * @description Set up page layout for HTML.
 * @_ui_icon html
 */
export interface HtmlPageConfiguration {
    /**
     * @title Indentation
     * @description The ident applied to paragraphs
     * @default 0mm
     */
    indent: string;
    /**
     * @title Paragraph spacing
     * @description The spacing between paragraphs
     * @default 2mm
     */
    paragraphSpacing: string;
    /**
     * @title Page layout and typography
    */
    page: {
        font: {
            /**
             * @title The font family
             * @default Arial, sans serif
             */
            family: string;
            /**
             * @title The font size
             * @default 12pt
             */
            size: string;
            /**
             * @title Line height
             * @default 1.5
             */
            lineHeight: string | number;
            /**
             * @title Text alignment
             * @default justify
             */
            textAlign: string;
        },
        /**
         * @title Hyphenation
         * @description Override the hyphenation settings from the general typography (leave empty to use typography defaults).
        */
        hyphens: 'auto' | 'manual' | 'none';
    }
    /** 
     * @title Custom CSS styles (advanced)
     * @description CSS styles that influence the look of the HTML output.
     */
    customStyles: {
        /** 
         * A custom scss string that is injected after the component styles 
         * @_ui_widget sfo-textarea
         */
        css?: string;
    }[];
}