import { TemplateComponent, Component } from '../../../component';
import { HtmlPageConfiguration } from './html-page.schema';

@Component({
    readme: `
    # Page setup
    General layout of the page like paper size, margins and typography.`,
    styleUrls: ['./html-page.scss']
})
// @ts-ignore
export class HtmlPage extends TemplateComponent<HtmlPageConfiguration, any> {
    public renderStyles(defaults?: { runner, assetBasePaths, componentPaths }): string {
        defaults = { ...defaults as any, assetBasePaths: [...(defaults?.assetBasePaths ? defaults.assetBasePaths : []), __dirname] };
        const styles = super.renderStyles(defaults);

        let css = '';
        let styleList = this.getAs<{css?: string}[]>('customStyles') || [];
        if (styleList && !Array.isArray(styleList)) {
            // stay backwards compatible to older templates
            styleList = [styleList];
        }

        for (let style of styleList) {
            if (style.css) {
                css += '\n\n' + style.css + '\n';
            }
        }

        if (css?.length > 0) { return styles + '\n\n' + css; }

        return styles;
    }
}
