import { TextProperties } from "../typography/typography.schema";

/**
 * @title Configuration for author biographies.
 */
export interface AuthorBiosConfiguration {
    /**
     * @title Title
     * @default About the author
     */
    title: string;
    /**
     * @title Plural title
     * @default About the authors
     * @description Used when multiple autors are included
     */
    titlePlural: string;
    /**
     * @title A locale for the title (other than the document locale)
     */
    locale: string;
    /**
     * @title Placement the section floats to (defaults to bottom)
     * @description see https://www.princexml.com/doc/css-props/#prop-prince-float-placement
     * @default bottom
     */
    floatPlacement: 'none' | 'left' | 'right' | 'inside' | 'outside'
    | 'top' | 'bottom' | 'top-bottom' | 'snap'
    | 'align-top' | 'align-bottom'
    | 'footnote' | 'inline-footnote';
    /**
     * @title Typography
     */
    typography: {
        /**
         * @title Heading
         */
        h1: TextProperties;
    }
    /** 
     * @title Text direction
     * @description A text direction other than the document's default direction.
     */
    dir: 'ltr' | 'rtl' | 'auto';
}