import { TSX } from '../../../TSX';
import { TemplateComponent, Component, PartForRendering } from '../../../component';
import { AuthorBiosConfiguration } from './author-bios.schema';
import { getTextDirection } from '../../../helpers';

@Component({
    readme: `# Author bios
    
    If author biographies were configured, they will be displayed here.`,
    styles: `
    .author-bios h1 {
        margin-top: 0;
        @if exists("typography.h1.fontSize") {
            font-size: #{get("typography.h1.fontSize")};
        }
        @if exists("typography.h1.fontWeight") {
            font-weight: #{get("typography.h1.fontWeight")};
        }
        @if exists("typography.h1.align") {
            text-align: #{get("typography.h1.align")};
        }
    }
    @if isPagedMedia() {
        .author-bios[data-author-count="1"], .author-bios[data-author-count="2"], .author-bios[data-author-count="3"] {
            float-reference: page;
            float-placement: #{get("floatPlacement")};
        }
    }
    `
})
export class AuthorBios extends TemplateComponent<AuthorBiosConfiguration, any> {
    public async renderDOM({ runner, componentPaths }): Promise<PartForRendering | undefined> {
        const authorsWithBio = this.engine.data.document.authors.filter(author => author.bio && (author.roles && author.roles.includes('author')));

        const empty = {
            id: 'author-bios',
            placement: this.page || 'back',
            dom: []
        };
        if (authorsWithBio?.length === 0) {
            return empty;
        }

        const locale = this.get('locale');
        const title = this.get('title', locale);
        const titlePlural = this.get('titlePlural', locale);

        const componentTitle = authorsWithBio?.length > 1 ? titlePlural : title;

        const htmlBios = authorsWithBio.map(author => {
            const bio = <div></div>;
            bio.innerHTML = author.bio;
            // make sure empty elements do not count as content
            if (bio.textContent?.trim()?.length === 0) { return undefined; }
            return bio;
        }).filter(el => el != undefined);

        if (htmlBios.length === 0) { return empty; }

        const section = <section class="author-bios py1 m0" data-author-count={authorsWithBio.length}>
            <h1 data-bookmark-level="none">{componentTitle}</h1>
            {...htmlBios}
        </section>;
        if (locale) {
            section.setAttribute('lang', locale);
            section.setAttribute('dir', getTextDirection(locale));
        }

        const dir = this.get('dir');
        if (dir) {
            section.setAttribute('dir', dir);
        }
        return {
            id: 'author-bios',
            placement: this.page || 'back',
            dom: [section]
        }
    }
}
