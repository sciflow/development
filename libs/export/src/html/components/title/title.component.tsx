import { TSX } from '../../../TSX';
import { TemplateComponent, Component, PartForRendering } from '../../../component';

@Component({
    readme: `Creates the title page`,
    styles: `
    .coverpage {
        page: coverpage;
        
        h1 {
            display: block;
            margin: auto;
            text-align: center;
        }
    }

    @page coverpage {
        prince-shrink-to-fit: auto;
        counter-reset: page -1;
    }
    `
})
export class Title extends TemplateComponent<any, any> {
    public async renderDOM(): Promise<PartForRendering | undefined> {
        return {
            id: 'title',
            placement: this.page || 'front',
            dom: [<section class="coverpage">
                <h1>{this.engine.data?.document?.title}</h1>
            </section>]
        }
    }
}
