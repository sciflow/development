import {Component, TemplateComponent} from '../../../component';
import {JournalMetaConfiguration} from './journal-meta.schema';

@Component()
export class BookMeta extends TemplateComponent<JournalMetaConfiguration, any> {}
