/**
 * @title Journal Metadata
 * @description Metadata for the journal
 * @_ui_icon newspaper
 * @_ui_tag meta
 */
export interface JournalMetaConfiguration {
  /**
   * @title Journal name
   */
  name: string;
  /**
   * @title Journal short name
   */
  shortName?: string;

  /**
   * @title Publisher
   */
  publisher: {
    /**
     * @title Publisher name
     * @default "Journal Publisher"
     */
    publisherName: string;

    /**
     * @title Publisher short name
     */
    publisherShortName?: string;

    /**
     * @title Publisher location
     */
    publisherLocation?: string;
  }
}
