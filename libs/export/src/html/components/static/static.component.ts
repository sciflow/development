import { TSX } from '../../../TSX';
import { Component, PartForRendering, TemplateComponent } from '../../../component';
import { StaticConfiguration } from './static.schema';

@Component({
    readme: `Outputs a static HTML element.`,
    styles: `
    @if isPagedMedia() {
        .#{getUniqueKey()} {
            float-reference: page;
            float-placement: #{get("floatPlacement")};
        }
    }
    `
})
export class Static extends TemplateComponent<StaticConfiguration, any> {
    public renderStyles(defaults): string {
        const fileId = this.getAs<string>('template.styleFileId');
        if (fileId) {
            const file = this.engine.data.document.files.find(f => f.id === fileId);
            if (file) {
                if (file.inline && file.content) {
                    return file.content;
                }
            }
        }

        return super.renderStyles(defaults);
    }

    public async renderDOM(d): Promise<PartForRendering | undefined> {
        const fileId = this.getAs<string>('template.fileId');
        const dom = this.get('dom');
        if (fileId && !dom) {
            const file = this.engine.data.document.files.find(f => f.id === fileId);
            if (file) {
                if (file.inline && file.content) {
                    this.setTemplate(file.content);
                } else {
                    this.setTemplate(file.url);
                }
            }
        } else if (dom) {
            this.setTemplate(dom);
        }

        return super.renderDOM(d);
    }
}
