export interface StaticConfiguration {
    styles?: string;
    dom?: string;
    template?: {
        /**
         * The scss template to use as the styles.
         */
        styleFileId?: string;
        /**
         * The hbs template to use as the dom.
         */
        fileId?: string;
    }
    /**
     * @title Placement the section floats to (defaults to bottom)
     * @description see https://www.princexml.com/doc/css-props/#prop-prince-float-placement
     * @default none
     */
    floatPlacement: 'none' | 'left' | 'right' | 'inside' | 'outside'
    | 'top' | 'bottom' | 'top-bottom' | 'snap'
    | 'align-top' | 'align-bottom'
    | 'footnote' | 'inline-footnote';
}