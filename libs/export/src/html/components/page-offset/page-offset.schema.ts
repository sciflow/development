/**
 * @title Page offset
 * @description Allows a publication to start with an offset (for page counters). The offset is usually defined through the meta data using offset or article-meta.fpage
 */
export interface PageOffsetConfiguration {
    /**
     * @title Counter offset
     * @description A value to start the counter at.
     */
    offset?: number;
    /**
    * @title Counter offset key (variable name)
    * @description Key in the meta data / settings the offset value should come from (e.g. 'article-meta.fpage' or 'offset')
    */
    offsetKey?: string;
}