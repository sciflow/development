import { Component, PartForRendering, TemplateComponent } from '../../../component';
import { PageOffsetConfiguration } from "./page-offset.schema";

@Component({
    readme: `#Page offset
    Introduces page offsets (numbering) in the document`
})
export class PageOffset extends TemplateComponent<PageOffsetConfiguration, any> {
    public renderStyles(defaults: { runner, assetBasePaths, componentPaths }): string {
        let offset: number | string | undefined = this.get('offset');
        let offsetKey = this.get('offsetKey');
        if (offsetKey && typeof offsetKey === 'string') {
            // get the value from the meta data using the specified key
            offset = this.getTemplateMetaData<any>(offsetKey) ?? offset;
        } // else offset is already a number and we continue

        if (typeof offset === 'number' && Number.isInteger(offset) && offset > 0) {
            return `${this.page ? '.' + this.page : 'html'} { counter-reset: page ${offset}; }`;
        }

        return '';
    }
}