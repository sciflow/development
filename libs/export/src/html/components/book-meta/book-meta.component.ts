import {Component, TemplateComponent} from '../../../component';
import {BookMetaConfiguration} from './book-meta.schema';

@Component({
  readme: `#Bits`,
})
export class BookMeta extends TemplateComponent<BookMetaConfiguration, any> {}
