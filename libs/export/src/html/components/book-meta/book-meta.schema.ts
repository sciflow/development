import { Meta, PubDate, TitleGroup } from '../shared/general.interface';

/**
 * @title Book Metadata
 * @description Metadata for the book, including identifiers, language, permissions, publisher, authors, publication date, and title.
 * @_ui_icon menu_book
 * @_ui_tag meta
 * @_readme Meta data can be used to display correct dates on cover pages, provide correct PDF tags, and make sure that XML exports (like BITS XML) have all the details needed to satisfiy the requirements for these exports.
 */
export interface BookMetaConfiguration extends Meta {
  /** @title Identifiers
   *  @description Identifiers for the book (e.g. DOI)
   */
  bookId: BookId[];
  /**
   * @title Titles
   * @description Book Title group to be displayed on the book cover
   */
  bookTitleGroup: TitleGroup;

  /**
   * @title Publication Date
   * @description The date of publication.
   */
  pubDate: PubDate;

  publisher: {
    /**
     * @title Publisher
     * @default Sample press
    */
    publisherName: string;
  }
}

/**
 * @title Identifier
 * @description Identifier for the book
 */
export interface BookId {
  /**
   * @title Book ID Type
   * @description Type of book identifier (e.g. doi)
   * @examples ["doi"]
   */
  bookIdType: string;

  /** @title Book ID Value
   *  @description Value of the book identifier
   */
  value: string;
}
