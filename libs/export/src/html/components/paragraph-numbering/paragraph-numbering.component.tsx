import { Component, TemplateComponent } from '../../../component';
import { ParagraphNumberingConfiguration } from './paragraph-numbering.schema';

@Component({
    styleUrls: ['./paragraph-numbering.component.scss']
})
export class ParagraphNumbering extends TemplateComponent<ParagraphNumberingConfiguration, any> {}