/**
 * @title Paragraph numbering
 * @description Settings to number paragraphs in the document.
 * @_ui_icon format_list_numbered
 */
export interface ParagraphNumberingConfiguration {
    // temporary property because we do not support empty interfaces
    temp: boolean;
}