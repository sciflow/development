/**
 * @title Google Font
 * @_ui_icon font_download
 * @description Set the font settings using the Google Font family.
 * @_readme The Google Font component automatically downloads open source fonts if a valid URL is supplied. To get the URL, select the fonts on fonts.google.com and then click the button "Get embed code" select "\n\n\@import" for web and then copy the url part of the code starting with https:// and usually ending on &display=swap
 */
export interface GoogleFontConfiguration {
    /**
     * @title The font family
     * @description e.g. EB Garamond', serif
     * 
     */
    fontFamily: string;
    /**
    * @title Google Font configuration
    */
    api?: {
        /**
         * @title URL
         * @description Full URL to a Google font (e.g. https://fonts.googleapis.com/css2?family=EB+Garamond:ital,wght@0,400;0,700;1,400;1,700)
         */
        url: string;
    }
    /**
     * @title One character REM width
     * @description An optional calculation for character widths based on the font and size and dpi
     * (!) We are not using em because we want the widths to be the same independent on the element font size
     */
    calculated?: {
        /**
         * @title Heading 1
         * @default 0.6rem
         */
        heading1: string;
        /**
         * @title Heading 2
         * @default 0.5rem
         */
        heading2: string;
        /**
         * @title Heading 3
         * @default 0.44rem
         */
        heading3: string;
        /**
         * @title Heading 4
         * @default 0.4rem
         */
        paragraph: string;
    }
}