import { Component, TemplateComponent } from '../../../component';
import { GoogleFontConfiguration } from './google-font.schema';

@Component({})
export class GoogleFont extends TemplateComponent<GoogleFontConfiguration, any> {}
