import { RenderingEngine } from '@sciflow/export';
import { TSX } from '../../../TSX';
import { Component, PartForRendering, TemplateComponent } from '../../../component';
import { extractHeadingOfType } from '../../../helpers';
import { slugify } from '../../../utils';
import { CollectionTableOfContentsConfiguration } from './collection-table-of-contents.schema';
import { SFNodeType } from '@sciflow/schema';
import { SciFlowDocumentData } from '../../..';

@Component({
  styleUrls: ['./collection-table-of-contents.scss']
})
export class CollectionTableOfContents extends TemplateComponent<CollectionTableOfContentsConfiguration, any> {
  public async renderDOM({ runner, componentPaths }): Promise<PartForRendering | undefined> {
    return {
      id: 'collection-table-of-contents',
      placement: this.page || 'front',
      dom: async (engine: RenderingEngine<any, SciFlowDocumentData>) => {
        const toc = await engine.data.tableOfContents;
        if (!toc?.headings || toc.headings?.filter(h => !h.skipToc).length === 0) { return []; }
        const partFileMap = engine.data.partFileMap;
        const currentFile = partFileMap?.['collection-table-of-contents'];

        const headings = this.engine.data.tableOfContents?.headings?.filter(h => !h.skipToc) || [];
        const headingStyles: string[] = [];
        const tocEntries: HTMLElement[] = [];

        for (let heading of headings.filter(heading => heading.level === 1)) {
          if (heading?.standalone && !heading?.isTitle) { continue; }
          const hostFileName = currentFile !== heading.hostFileName ? heading.hostFileName : undefined;
          let title = heading.titleHTML
            ? TSX.htmlStringToElement(`<span>${heading.titleHTML}</span>`)
            : <span class="toc-title">{heading.title}</span>;
          const part = this.engine.data.document.parts.find(p => p.partId === heading.partId);

          // see if the part has meta data from the parent document it came from
          const href = `${hostFileName ?? ''}#${heading.id}`;
          if (part?.parent) {
            const titlePart = part.parent.parts.find(p => p.type === 'title');
            const subtitle = (titlePart?.document ? extractHeadingOfType(titlePart?.document, undefined, SFNodeType.subtitle) : undefined);
            const authors = part?.parent?.authors
              ?.filter(author => !author.hideInPublication && author.roles?.includes('author'))
              .map(author => author.name ? author.name : `${author.firstName} ${author.lastName}`.trim())
              .filter(s => s.length > 0);

            const authorPlacement = this.get('authorNames.position');

            const authorNamesEl = !part.parent?.metaData?.supressAuthorInTitle && authors?.length > 0 ? <span class="author-names">
              {...authors.map(author => (<span class="author">{author}</span>))}
            </span> : undefined;

            const titleEl = <span id={'toc-call-title-' + slugify(heading.id)}>
              <span class="bold">{title}</span>
              {subtitle && subtitle?.length > 0 ? <span><br />{subtitle}</span> : ''}
            </span>;

            const elements = [titleEl];
            if (authorNamesEl) {
              elements.push(<br />);
              elements.push(authorNamesEl);
            }
            if (authorPlacement === 'above') {
              elements.reverse();
            }

            tocEntries.push(<div class="toc-entry my2">
              <a id={'toc-call-' + slugify(heading.id)} href={href} class="link-with-target toc-title">
                {...elements}
              </a>
            </div>);
          } else {
            tocEntries.push(<div class="toc-entry my2">
              <a id={'toc-call-' + slugify(heading.id)} href={`${hostFileName ?? ''}#${heading.id}`} class="link-with-target toc-title">
                <span class="bold">{title}</span>
              </a>
            </div>)
          }

          if (heading.placementNumbering) {
            headingStyles.push(`#toc-call-${slugify(heading.id)}::after { content: leader("${this.get('leader')}") target-counter(attr(href), page, ${heading.placementNumbering}); }`);
          } else {
            debugger;
          }
        }

        return [<div class={this.kind + ' section-like'}>
          <section id="collection-table-of-contents">
            <style>
              {headingStyles.join('')}
            </style>
            <h1><span>{this.get('label') || 'Contents'}</span></h1>
            <div class="pt1">
              {...tocEntries}
            </div>
          </section>
        </div>];
      }
    };
  }
}
