/**
 * @title Table of contents (collections)
 * @description A specialized configuration for table of contents that must include collection articles (with separate author names per article)
 */
export interface CollectionTableOfContentsConfiguration {
    /**
     * @title The leader symbol between the heading title and the page number
     * @default .
     */
    leader: '.' | undefined;
    /**
     * @title The text displayed above the table of contents
     * @default Table of Contents
     */
    label: string;

    authorNames: {
        /**
         * @title Upper case
         * @default true
         */
        upperCase: boolean;
        /**
         * @title Position
         * @description The position where the author names should appear (above or below the title)
         * @default above
         */
        position: 'above' | 'below';
    }

    /**
     * @title Hyphenation in listings
     * @default manual
     */
    hyphenation: 'auto' | 'manual' | 'none';
}
