import { Component, TemplateComponent } from '../../../component';
import { FontConfiguration } from './font.schema';

@Component({})
export class Font extends TemplateComponent<FontConfiguration, any> {}
