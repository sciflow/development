/**
 * @title Fonts
 * @_ui_icon font_download
 */
export interface FontConfiguration {
    /**
     * @title The font name
     * @description e.g. EB Garamond', serif
     * @_readme Any font listed here must either be made available on the exporting system (e.g. by specifying a path) or be pre-istalled (like MST core fonts)
     * 
     */
    fontFamily: string;
    /**
    * @title Font directory
    * @description A path to the directory with the font files (relative to the font directory)
    */
    path?: string;
    /**
     * @title REM width
     * @description A calculated value to align headings and table of contents
     * @_readme An optional calculation for character widths based on the font and size and dpi(!). We are not using em because we want the widths to be the same independent on the element font size
     */
    calculated?: {
        /**
         * @default 0.6rem
         */
        heading1: string;
        /**
         * @default 0.5rem
         */
        heading2: string;
        /**
         * @default 0.44rem
         */
        heading3: string;
        /**
         * @default 0.4rem
         */
        paragraph: string;
    }
}