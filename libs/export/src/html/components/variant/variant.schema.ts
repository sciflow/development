import { Operation } from 'fast-json-patch';

/**
 * Changes the default value in the schema and removes 
 * any configuration values that would overwrite it.
 * Patches need to end on /default
 */
export const patchDefault = 'default';

/**
 * Patches any existing configuration values without changing schema defaults.
 * Can be combined with default patches if applied after the default patch.
 * Patches that change the spec need to start with /spec
 */
export const patchConf = 'conf';

/** A target in the template to patch  */
interface PatchTarget {
    /** The name of the configuration to patch (e.g. HeadersAndFooters, Page, ..) */
    kind?: string;
    metadata?: {
        /**
         * The unique configuration name in case the configuration is used in the same document twice (e.g. headers-for-body,). 
         * It is a good practice to still add the kind even though it is not strictly needed since names are unique over all configurations.
        */
        name?: string;
    }
}

interface GenericPatch {
    /** A target is a configuration that should be overwritten */
    target: PatchTarget;
    /** default will modify the default values in the JSON schema, conf will change the configuration values set in the YAML template configuration. */
    type: 'default' | 'conf';
    /** JSON patch operations RFC 6902 */
    patch: Operation[];
}

/**
 * A variant applies patches to an existing template in order to change a number of values.
 * It can be used when creating an entirely new template would be too much effort but some settings have to be consistently changed.
 */
export interface VariantConfiguration {
  patches: GenericPatch[];
  [key: string]: any;
}
