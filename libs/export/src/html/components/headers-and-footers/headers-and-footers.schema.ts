/**
 * @title Headers and Footers
 * @description Layout and typography of headers and footers. Different Headers and footers may exist for each page/section of a document (cover, front, body, back) individually.
 * @_ui_icon view_agenda
 * @instructions
 * # About headers and footers
 * Layout and typography of headers and footers. Different Headers and footers may exist for each page/section of a document (cover, front, body, back) individually. Journals usually have only the body section whereas monographs make use of all types.

 * 
 * # Details for developers
 * 
 * That means that each page/section needs an additional HeadersAndFooters configuration. e.g.
 * 
 * ```yaml
 * kind: HeadersAndFooters
 * runners:
 *   - princexml
 * page: front
 * spec:
 *   template:
 *     fileId: headers-and-footers-for-front
 * ---
 * kind: HeadersAndFooters
 * runners:
 *   - princexml
 * page: body
 * spec:
 *   template:
 *     fileId: headers-and-footers-for-front
 * ```
 * 
 * The content itself is defined in a handlebar template that is provided seperately (see template.fileId).
 * 
 * The hbs templates use basscss to layout the header and footer sections. Classes are used to identify the recto, verso and first page headers. This is an example for a hbs file.
* The height of a header or footer is set from the outside by using the typography.(header|footer.height) property. The classes are then automatically projected into the \@page { \@top and \@ppage { \@bottom of
 * the respective CSS Paged Media named page (e.g. cover, front, body, back). To create a new hbs template, add it to the assets folder and link it in the assets array in the main Configuration.
 * 
 * You can have individual files for all pages (e.g. one for the front, a different one for the body etc.) but you can also use {{#equals getPage 'front'}} .. {{else}} ... {{/equals}} to have all in one file.
 * 
 * When modifying CSS classes, make sure to prefix with .#{getPage] so only one page gets modified (unless the headers and footers only exists once in the entire template).
 * If possible only update configuration values in spec and do not create custom CSS since this is harder to maintain in the future. Also prefer using basscss classes instead of creating custom css (e.g. by modifying justify-between)
 * The spec for headers and footers only allows a very limited list of modifications (because headers and footers are often quite different between templates). That is why most modifications have to be made in the hbs template if they are not
 * explicitly mentioned in the spec schema.
 * 
 * Note how the {{get}} invocation can access information from the spec. main-heading-text and sub-heading-text are variables that are automatically set by the system using the h1 and h2 content respectively.
 * renderAuthorNames renders author names according to the presets made in formatting.authorNames.preset
 * 
 * When setting page counters, the current counter style must be used <span class="page-number" style="content: counter(page, {{get 'pageCounter.style'}});"></span>
 * 
 * ```hbs
 * <div class="HeadersAndFooters">
 *   <style>
 *       #{getPage} .HeadersAndFooters {
 *           font-weight: lighter;
 *       }
 *   </style>
 *   <div class="first-page-header flex justify-between col-12 pt2"></div>
 *   <div class="first-page-footer flex justify-between col-12 pt2">
 *       <span></span> {{!-- to center the page number you would remove this span and set the class of the parent div to justify-center }
 *       <span class="page-number" style="content: counter(page, {{get 'pageCounter.style'}});"></span>
 *   </div>
 *
 *   {{!-- The header on recto pages (for LTR this means right pages) --}}
 *   <div class="recto-page-header flex justify-between col-12 pt3">
 *      {{!-- first-except is used in the content expression on the document title to not let it show up on the first page where the h1 content appears --}}
 *       <span class="document-title"><span style="content: string(main-heading-text, first-except);"></span></span>
 *   </div>
 *
 *   {{!-- The footer on recto pages --}}
 *   <div class="recto-page-footer flex justify-between col-12">
 *       <span class="page-number" style="content: counter(page, {{get 'pageCounter.style'}});"></span>
 *   </div>
 *
 *   {{!-- The header on verso pages (for LTR this means left) --}}
 *   <div class="verso-page-header flex justify-between col-12 pt3">
 *       <span class="document-title"><span style="content: string(sub-heading-text);"></span></span>
 *   </div>
 *
 *   {{!-- The footer on verso pages --}}
 *   <div class="verso-page-footer flex justify-between col-12">
 *       <span class="page-number" style="content: counter(page, {{get 'pageCounter.style'}});"></span>
 *       <span class="author-names" style="content: string(author-names);">{{renderAuthorNames}}</span>
 *   </div>
 * </div>
 * ```
 */
export interface HeadersAndFootersConfiguration {
    /**
     * @title Page counters
    */
    pageCounter: {
        /**
         * @title Counter style
         * @description The counter style (if a page number is present)
         * @default decimal
        */
        style: string;
        /**
         * @title Reset page counter
         * @description before, after, or before-and-after
        */
        reset: string;
    }
    typography: {
        /**
         * @title Configuration for page headers.
         */
        header: {
            /**
             * @title Font size
             * @default 83%
             */
            fontSize: string;
            /**
             * @title Header height
             * @default 20mm
             */
            height: string;
        }
        footer: {
            /**
             * @title Font size
             * @default 9pt
             */
            fontSize: string;
            /**
             * @title Footer height
             * @default 20mm
             */
            height: string;
        }
        /**
         * @title Configuration for the first page.
         */
        firstPage: {
            /**
             * @default first
             * @title naame for the first page (e.g. recto, first, verso) to re-use other headers
             */
            name: string;
            logo: {
                /**
                 * @title Logo
                 * @default 14pt
                 */
                fontSize: string;
            }
            header: {
                /**
                 * @title Header height
                 * @default 20mm
                 */
                height: string;
            }
            footer: {
                /**
                 * @title Footer height
                 * @default 20mm
                 */
                height: string;
            }
        }
    }
    formatting: {
        /**
         * @title Author Names
         */
        authorNames: {
            /**
             * @title Author name preset name
             * @description (e.g. authorsEtAl, authorsEtAlCaps, authorsEtAlLastNameCaps)
             * @default authorsEtAl
             */
            preset: string;
            /**
             * @title Locale
             * @description The locale to use (if the document locale is not correct)
             */
            locale?: string;
        }
    }
    /**
     * @title Template settings
     */
    template?: {
        /**
         * @title File id
         * A file id to a hbs template to render instead of the defaults.
         */
        fileId?: string;
        /**
         * @title A preset for journals or monographs with no further customizations (works for most publications) instead of providing a file
         * @description If a fileId is provided this overrides the preset.
         * @default journal
         */
        preset?: 'journal' | 'monograph';
    }
    /**
     * @title Overwrite placement
     * @description Overwrites the placement (e.g. if the component is placed into cover this will create styles that move the element into root scope instead of nesting the CSS inside the .cover/.front/.body sections)
     * This may be needed for (journal) templates that only have a body but document parts are in front/cover
     * There is no default value.
     */
    overridePlacement: undefined | 'all' | string;
}