import { Component, PartForRendering, TemplateComponent } from '../../../component';
import { HeadersAndFootersConfiguration } from './headers-and-footers.schema';

@Component({
    readme: `# Headers and footers

    Headers and footers are displayed on the first page of a document and respectively on the following recto and verso pages.
    This configuration can be configured to change their appearance and content.`,
    styleUrls: ['./headers-and-footers.scss'],
    templateUrl: './headers-and-footers-journal.hbs'
})
export class HeadersAndFooters extends TemplateComponent<HeadersAndFootersConfiguration, any> {
    public async renderDOM(defaults?: { runner?: string; componentPaths?: string[]; data?: unknown; }): Promise<PartForRendering | undefined> {
        const fileId = this.getAs<string>('template.fileId');
        const preset = this.getAs<string>('template.preset');

        if (fileId) {
            const file = this.engine.data.document.files.find(f => f.id === fileId);
            if (file) {
                if (file.inline && file.content) {
                    this.setTemplate(file.content);
                } else {
                    this.setTemplate(file.url);
                }
            }
        }

        if (preset !== 'journal') {
            this.setTemplateUrl('headers-and-footers-monograph.hbs');
        }

        const part = await super.renderDOM(defaults);
        if (!part) { return part; }
        return {
            ...part,
            moveIntoPageFlow: defaults?.runner === 'princexml',
            notDefining: true
        };
    }
}
