import { countryToLabel, extractHeadingTitle, getTextDirection } from '../../../helpers';
import { TSX } from '../../../TSX';
import { TemplateComponent, Component, PartForRendering } from '../../../component';
import { JournalTitleConfiguration } from './journal-title.schema';
import { SFNodeType } from '@sciflow/schema';

const orcidSvg = () => TSX.htmlStringToElement(`<svg style="width: 12px; height: 12px;" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
    viewBox="0 0 256 256" style="enable-background:new 0 0 256 256;" xml:space="preserve">
    <style type="text/css">
        .st0{fill:#A6CE39;}
        .st1{fill:#FFFFFF;}
    </style>
    <path class="st0" d="M256,128c0,70.7-57.3,128-128,128C57.3,256,0,198.7,0,128C0,57.3,57.3,0,128,0C198.7,0,256,57.3,256,128z" />
    <g>
        <path class="st1" d="M86.3,186.2H70.9V79.1h15.4v48.4V186.2z" />
        <path class="st1" d="M108.9,79.1h41.6c39.6,0,57,28.3,57,53.6c0,27.5-21.5,53.6-56.8,53.6h-41.8V79.1z M124.3,172.4h24.5
   c34.9,0,42.9-26.5,42.9-39.7c0-21.5-13.7-39.7-43.7-39.7h-23.7V172.4z"/>
        <path class="st1" d="M88.7,56.8c0,5.5-4.5,10.1-10.1,10.1c-5.6,0-10.1-4.6-10.1-10.1c0-5.6,4.5-10.1,10.1-10.1
   C84.2,46.7,88.7,51.3,88.7,56.8z"/>
    </g>
</svg>`);

const counterStyles = {
    symbols: '+¤^±§※∞♯⁂♪♫‡¶&'
};

@Component({
    readme: `#Journal title
    The journal title contains most of the meta data and affiliations of an article.`,
    styleUrls: ['./journal-title.scss']
})
export class JournalTitle extends TemplateComponent<JournalTitleConfiguration, any> {

    format = new Intl.DateTimeFormat(this.engine.data.document.locale || 'en-US', { month: 'long', day: 'numeric', year: 'numeric', timeZone: 'UTC' });
    formatDate = (date: Date) => date ? this.format.format(new Date(date)) : '';

    public async renderDOM(defaults?: { runner?: string; componentPaths?: string[]; data?: unknown; }): Promise<PartForRendering | undefined> {

        if (!this.engine.data.document.titlePart) { throw new Error('Document must have a title part'); }
        const titleEl = this.engine.data.document.titlePart?.document?.content.find(n => n.type === SFNodeType.heading);
        const title = this.engine.data.document.titlePart ? await extractHeadingTitle(titleEl, this.engine) : undefined;
        const subtitle = this.engine.data.document.titlePart ? await extractHeadingTitle(this.engine.data.document.titlePart?.document?.content.find(n => n.type === SFNodeType.subtitle || (n.type === SFNodeType.heading && n.attrs?.level === 2)), this.engine) : undefined;
        const renderAuthors = this.get('renderAuthors');

        let titlePartHTML;
        try {
            titlePartHTML = await this.engine.render(this.engine.data.document.titlePart.document);
            const altTitle = this.getAs<string>('article-meta.altTitle');
            if (altTitle && altTitle?.length > 0) {
                const heading1 = titlePartHTML.getElementsByTagName('h1');
                heading1?.[0]?.setAttribute && heading1?.[0]?.setAttribute('data-alt-title', altTitle);
            }
        } catch (e: any) {
            console.error(e);
        }

        const titleHeading = <h1 id={titleEl.attrs?.id}></h1>;
        if (title?.titleHTML) {
            titleHeading.innerHTML = title.titleHTML;
        } else {
            titleHeading.innerHTML = title?.title || this.engine.data.document.title;
        }

        const altTitle = this.getAs<string>('article-meta.altTitle');
        if (altTitle && altTitle?.length > 0) { titleHeading.setAttribute('data-alt-title', altTitle); }

        const subtitleHeading = <h2 data-bookmark-level='none'></h2>;
        if (subtitle?.titleHTML) {
            subtitleHeading.innerHTML = subtitle.titleHTML;
        } else if (subtitle?.title || this.engine.data.document.subtitle) {
            subtitleHeading.innerHTML = subtitle?.title || this.engine.data.document.subtitle;
        }

        const dir = this.get('dir');
        const authorSection = renderAuthors ? this.generateAuthors() : '';

        const fileId = this.getAs<string>('template.fileId');
        if (fileId) {
            const file = this.engine.data.document.files.find(f => f.id === fileId);
            if (file) {
                if (file.inline && file.content) {
                    this.setTemplate(file.content);
                } else {
                    this.setTemplate(file.url);
                }
                const result = await super.renderDOM({ ...defaults, data: { titlePartHTML: titlePartHTML?.outerHTML, authorSection: authorSection?.outerHTML, titleHeading: titleHeading?.outerHTML, subtitleHeading: subtitleHeading?.outerHTML } });
                if (result && result?.dom?.length > 0) {
                    result.dom[0].setAttribute('id', result.dom[0].getAttribute('id') || titleEl.attrs.id);
                }
                return result;
            }
        }

        const dom = <section class={'JournalTitle' + ' ' + this.uniqueKey}>
            {titleHeading}
            {this.engine.data?.document?.subtitle ? subtitleHeading : ''}
            {authorSection}
        </section>;

        if (this.engine.data.document.locale) {
            dom.setAttribute('lang', this.engine.data.document.locale);
            dom.setAttribute('dir', getTextDirection(this.engine.data.document.locale));
        }

        if (dir) { dom.setAttribute('dir', dir); }

        // no custom file, we continue with the standard DOM
        return {
            id: 'journal-title',
            placement: this.subPage || this.page || 'cover',
            dom: [dom]
        }
    }

    generateAuthors() {
        const authors = this.engine.data.document.authors.filter(author => !author.roles || author.roles.includes('author'));

        /**
         * Creates a HTML string of the author names.
         */
        const getName = (author): HTMLSpanElement => {
            if (author?.firstName || author?.lastName) {
                return <span>{[author.firstName ? <span class="first-name">{author.firstName}</span> : null, TSX.createTextNode(' '), author.lastName ? <span class="last-name">{author.lastName}</span> : null]
                    .filter(n => n !== null)}
                </span>
            }
            if (author?.name) { return <span>author.name</span>; }
            return <span>Unknown author</span>;
        }
        const createInstitutionId = (institution) => institution ? (institution.slug || [institution.department, institution.institution, institution.city].join('-')) : undefined;

        let affiliations: any = authors.map((author) => author.positions?.map((position) => {
            position.slug = position.slug ?? createInstitutionId(position);
            return position;
        })).flat().filter((value, index, list) => value && list.findIndex(a => a.slug === value.slug) === index);
        const mapAuthor = (position) => {
            const pos = affiliations.findIndex((affiliation: any) => affiliation?.slug === position.slug)
            return pos + 1;
        };

        const correspondingAuthors = authors.filter(author => author.email && author.correspondingAuthor);
        const authorsWithComments = authors.filter(a => a.comment && a.comment?.length > 0);

        const getCorrespondingAuthorSymbol = (author, list) => '*'.repeat(list.findIndex(a => a.authorId === author.authorId) + 1);
        const getAuthorCommentSymbol = (author, list) => counterStyles.symbols[list.findIndex(a => a.authorId === author.authorId)];

        return <section class="authors" data-author-names={this.authorNames}>{
            ...authors.map((author, index) => <span class="author">{getName(author)}
                <sup>{author.positions?.map(mapAuthor).filter(l => l != null).join(' ')}
                    {author.orcid ? (<a href={`https://orcid.org/${author.orcid}`} target="_blank">
                        {orcidSvg()}</a>) : ''}
                    {author.email && author.correspondingAuthor ? getCorrespondingAuthorSymbol(author, correspondingAuthors) : ''}
                </sup>
                {author.comment ? <sup>{getAuthorCommentSymbol(author, authorsWithComments)}</sup> : ''}
                {index + 1 < authors.length ? ', ' : ''}
            </span>)
        }
            <div class="affiliations my2">
                {...affiliations.map((affiliation, index) => (<address>
                    <sup>{`${index + 1}`}</sup>&nbsp;
                    <span>{[
                        affiliation.institution,
                        affiliation.department,
                        affiliation.city,
                        affiliation.country ? countryToLabel(affiliation.country, this.engine.data.document.locale) : undefined
                    ].filter(l => l?.length > 0).join(', ')}
                    </span>
                </address>))}
            </div>
            {correspondingAuthors.length > 0 ? <div class="corresponding">
                {...correspondingAuthors.map(author => <span class="pr1">
                    <sup>{getCorrespondingAuthorSymbol(author, correspondingAuthors)}</sup>&nbsp;{author.email}
                </span>)}
            </div> : ''}
            {authorsWithComments?.length > 0 ?
                <div class="author-comments py1">
                    {authorsWithComments.map(author => <span>
                        <span>{getAuthorCommentSymbol(author, authorsWithComments)}&nbsp;</span>
                        {TSX.htmlStringToElement(author?.comment || '')}
                    </span>)}
                </div> : ''}
        </section>
    }
}
