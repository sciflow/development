/**
 * @title Title section
 * @description Set the title and author settings for the journal.
 * @_ui_icon title
 */
export interface JournalTitleConfiguration {
    /**
     * @title Template
     * @description The template to be used for rendering the content */
    template?: {
        /** A reference to a file */
        fileId?: string;
    }

    /**
     * @title Render authors and affiliations
     * @description Render authors as part of the title section (compact list). Leave this turned on if you do not have a dedicated author section somewhere else in the template.
     * @default true
     */
    renderAuthors: boolean;
    typography: {
        /**
         * @title Title
         * @description Font settings for the journal title
         */
        title: {
            /**
             * @title Font size
             */
            fontSize: string;
            /**
             * @title Line height
             * @default 1.2
             */
            lineHeight: string | number;
            /**
             * @title Font size
             * @default bold
             */
            fontWeight: string | number;
            /**
             * @title Text alignment
             * @default start
             */
            textAlign: string;
        };
        /**
         * @title Sub title
         * @description Font settings for the sub title
         * @
         */
        subTitle: {
            /**
             * @title Font size
             * @default 1.1em
             */
            fontSize: string;
            /**
             * @title Line height
             * @default 1.3
             */
            lineHeight: string | number;
            /**
             * @title Font size
             * @default bold
             */
            fontWeight: string | number;
            /**
             * @title Text alignment
             * @default start
             */
            textAlign: string;
        };
        /**
         * @title Authors
         * @description Appearance of the author list
         */
        authors: {
            /**
             * @title Font size
             */
            fontSize: string;
            /**
             * @title Font weight
             * @examples ["normal", "bold", "lighter", "bolder"]
             */
            fontWeight: string | number;
            /**
            * @title Font style
            * @default normal
            */
            fontStyle: 'normal' | 'italic';
        };
        /**
         * @title Affiliations
         * @description Appearance of the author affiliations.
         */
        affiliations: {
            /**
             * @title Font size
             */
            fontSize: string;
        }
    },
    /**
     * @title Formatting
     */
    formatting: {
        /**
         * @title Author Names
         */
        authorNames: {
            /**
             * @title Preset
             */
            preset: string
            /**
             * @title Locale
             * @description The locale to use (if the document locale is not correct)
             */
            locale?: string;
        }
    }

    /** 
     * @title Text direction
     * @description A text direction other than the document's default direction.
     */
    dir: 'ltr' | 'rtl' | 'auto';
}