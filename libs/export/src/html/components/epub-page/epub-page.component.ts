import { Component, TemplateComponent } from '../../../component';
import { EpubPageConfiguration } from './epub-page.schema';

@Component({
    readme: `
    # EPUB Page setup
    General layout of the page like paper size, margins and typography.
    `,
    styleUrls: ['./epub-page.scss']
})
export class EpubPage extends TemplateComponent<EpubPageConfiguration, any> {}
