/**
 * @title EPUB configuration
 */
export interface EpubPageConfiguration {
    customStyles: {
         /** 
         * A custom scss string that is injected after the component styles 
         * @_ui_widget sfo-textarea
         */
        css?: string;
    }[];
    /**
     * @title Indentation
     * @default 0mm
     */
    indent: string;
    /**
     * @title Paragraph spacing
     * @default 2mm
     */
    paragraphSpacing: string;
    /** Page layout and typography */
    page: {
        font: {
            /**
             * @title The font family
             * @default Arial, sans serif
             */
            family: string;
            /**
             * @title The font size
             * @default 12pt
             */
            size: string;
            /**
             * @title Line height
             * @default 1.5
             */
            lineHeight: string | number;
            /**
             * @title Text alignment
             * @default justify
             */
            textAlign: string;
        }
    }
    /**
   * @title Use the WebP file format.
   */
  transformImages?: {
    /**
     * @title Format to use
     */
    format: 'WebP' | 'JPEG' | 'PNG' | undefined;
    /**
     * @title Webp quality
     * @default 80
     */
    quality: number;
    /**
     * @title DPI image quality
     * @default 96
     */
    dpi: number;
  }
}