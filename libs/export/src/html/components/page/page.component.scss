@use "@sciflow/tdk/styles/base";
@use "@sciflow/tdk/styles/debug" as debug;
@use "@sciflow/tdk/styles/pagedjs" as pagedjs;

$breakBeforeChapter: #{get("page.pageBreaks.beforeChapter")};
$breakBeforeDocument: #{get("page.pageBreaks.beforeDocument")};

@function keyExists($array, $string) {
    @return index($array, $string) != null;
}

@if not isPagedMedia() {
    @warn "The page component should only be included for paged media based runners";
}

@if isRunner("pagedjs") {
    @debug "Runner - pagedjs";
    @include pagedjs.pagedjs;
}

@debug "Creating page placements for #{get("placements")}";

@if keyExists(get("placements"), "body") {
    .body {
        page: body;
    }
}

@if keyExists(get("placements"), "cover") {
    /** Creating a page for cover will automatically create a page break when it begins.
    *   This might not be desirable for journals.
    */
    .cover {
        page: cover;
    }
} @else {
    .cover {
        page: body;
    }
}

@if keyExists(get("placements"), "front") {
    /** Creating a page for front will automatically create a page break when it begins.
    *   This might not be desirable for journals.
    */
    .front {
        page: front;
    }
} @else {
    .front {
        page: body;
    }
}

@if keyExists(get("placements"), "back") {
    /** Creating a page for back will automatically create a page break when it begins.
    *   This might not be desirable for journals.
    */
    .back {
        page: back;
    }
} @else {
    .back {
        page: body;
    }
}

html,
body {
    font-family: #{get("page.font.family")};
    font-size: #{get("page.font.size")};
    line-height: #{get("typesetting.lineHeight")};
    text-align: #{get("typesetting.textAlign")};
    padding: #{get("page.bodyPadding", 2mm)} 0;
}

table {
    font-size: #{get("page.font.size")};
}

.cover {
    font-family: #{get("page.font.cover.family")};
    font-size: #{get("page.font.cover.size")};
}

:root {
    --body-padding: 6mm;
    --figure-caption-min-height: calc(9rem);
    --line-height: #{getCalculatedProperty(lineHeightInPt, 1)};
    --figure-caption-min-height-landscape: calc(6.5rem); // since the page is wider
    // content height will be overwritten in the headers and  footers component since
    // different regions have different headers and footers
    --content-height: calc(100pvh - #{get("page.margins.top")} - #{get("page.margins.bottom")} - var(--body-padding));
    --content-width: calc(100pvw - #{get("page.margins.top")} - #{get("page.margins.bottom")} - var(--body-padding));
    --content-height-landscape: calc(100pvw - #{get("page.margins.inner")} - #{get("page.margins.outer")});
    --text-indent: #{get("typesetting.indent")};
}

@debug "Page handling - #{get('page.handling')}";

@if get("page.handling") == "double-sided" and get("page.duplex") {
    @prince-pdf {
        -prince-pdf-duplex: duplex-flip-long-edge;
        -prince-pdf-page-layout: get("page.pdfLayout", "two-page-right");
    }
}

@page {
    margin: 0;
    size: #{get("page.size")};
    font-family: #{get("page.font.family")};
    font-size: #{get("page.font.size")};
    // we do not set margins on unnabeled pages to allow for full bleed
}

@if isDebug() {
    @page:first {
        @prince-overlay {
            vertical-align: bottom;
            padding-bottom: 1cm;
            color: orange;
            content: "#{get("page.size")} / #{get('page.handling')}";
            font-size: 16pt;
        }
    }
}

p {
    widows: #{get("typesetting.widows")};
    orphans: #{get("typesetting.orphans")};
    text-indent: #{get("typesetting.indent")};
}

p:not(:empty) {
    margin-top: 0;
}

ol, ul {
    margin-bottom: #{get("typesetting.paragraphSpacing")};

    li ol, 
    li ul {
        margin-bottom: 0.125rem;
    }
}

.section-like {
    padding-top: #{get("typesetting.paragraphSpacing")};
}

[data-type="free"], [data-role="coverpage"] {
    p {
        text-indent: initial;
    }

    p:empty {
        min-height: 1em;
        width: 100%;
    }
}

.front,
.body,
.back {
    // we break before the placements (front, body..) and then remove the break from the first section (:not(:first-of-type)) in order to allow
    // empty elements (e.g. for the header and footer component to exist as a first dom element in the body
    break-before: $breakBeforeChapter;
}

body[data-standalone="true"] {
    break-before: $breakBeforeDocument;
}

// data-page-break is determined by the chapter settings while $breakBeforeChapter is a configuration option.
.front > section[data-level="1"]:not([data-page-break="auto"]),
.body > section[data-level="1"]:not([data-page-break="auto"]),
.back > section[data-level="1"]:not([data-page-break="auto"]),
[data-type="part"]:not([data-page-break="auto"]),
.section-like:not([data-page-break="auto"]):not(.listing) { //listings have their own settings
    break-before: $breakBeforeChapter;

    // if a page break is forced (e.g. not auto and not empty which is auto) then a new page group is stared. This creates a new :first page
    @if $breakBeforeChapter != auto and $breakBeforeChapter {
        -prince-page-group: start;
    }
}

h1 {
    string-set:
        main-heading-text content(),
        main-heading-separator "",
        sub-heading-separator "",
        main-heading-text-title content(),
        main-heading-text-numbering "",
        sub-heading-text content(),
        sub-heading-text-title content(),
        sub-heading-text-numbering "";

    .numbering-string {
        string-set:
            main-heading-text-numbering content(),
            sub-heading-text-numbering content(),
            /* we only want to set the separator when there is an actual number to show */
            main-heading-separator "\00a0\00a0",
            sub-heading-separator "\00a0\00a0";
    }
    .heading-title {
        string-set:
            main-heading-text-title content(),
            sub-heading-text-title content();
    }
}

h1[data-alt-title] {
    string-set:
        main-heading-text attr(data-alt-title),
        main-heading-text-title attr(data-alt-title),
        sub-heading-text-title attr(data-alt-title),
        sub-heading-text attr(data-alt-title);
}

h2 {
    string-set:
        sub-heading-text content(),
        sub-heading-text-numbering content(""),
        sub-heading-text-title content(""),
        sub-heading-separator "\00a0\00a0";
    .numbering-string {
        string-set: sub-heading-text-numbering content();
    }
    .heading-title {
        string-set: sub-heading-text-title content();
    }
}

@if isDebug() {
    div[data-page-break],
    section[data-page-break]::before {
        content: "page break: " attr(data-page-break);
        position: absolute;
        size: 90%;
        color: red;
        top: 1%;
        right: 1%;
    }
}

section[data-page-break="before-and-after"],
div[data-page-break="before-and-after"] {
    break-after: page;
    break-before: page;
}

/* Forced page break so content starts on a right page */
:not(.cover) section[data-page-break="right"],
div[data-page-break="right"] {
    break-before: right;
}

:not(.cover) section[data-page-break="after"],
div[data-page-break="after"] {
    break-after: page;
}

:not(.cover) section[data-page-break="before"],
div[data-page-break="before"] {
    break-before: page;
}

.no-headers-and-footers {
    page: blank;
}

@page blank {

    margin-left: #{get("page.margins.inner")};
    margin-right: #{get("page.margins.outer")};

    @if get("page.handling") == "double-sided" {
        margin-inside: #{get("page.margins.inner")};
        margin-outside: #{get("page.margins.outer")};
    }

    margin-top: #{get("page.margins.top")};
    margin-bottom: #{get("page.margins.bottom")};

    @include debug.overlay("blank");

    @top {
        border: none !important;
        content: none !important;
    }
    @bottom {
        border: none !important;
        content: none !important;
    }
}

$bookmarkLevels: none 1 2 3 4 5 6;
@each $level in $bookmarkLevels {
    [data-bookmark-level="#{$level}"] { -prince-bookmark-level: #{$level}; }    
}
