/**
 * @title PDF essentials
 * @description Set up page margins, fonts and page sizes.
 * @_readme You can change the general typography for all exports in the typography section.
 * @_ui_icon picture_as_pdf
 */
export interface PageConfiguration {
    /**
     * @description Page Size, Fonts, Margins, etc.
     */
    page: {
        /**
         * @title Size
         * @description The page size.
         * @examples ["A4", "A5", "17cm 24cm"]
         * @default A4
         */
        size: string;
        margins: {
            /**
             * @title The inner margin (for double-sided printing)
             * @description The inner margin is usually larger to account for binding a printed document. For single sided printing this is the right margin.
             * @default 25mm
             */
            inner: string;
            /**
             * @title The outer margin
             * @description The outer margin (for double-sided printing). For single-sided printing this is the left margin.
             * @default 20mm
             */
            outer: string;
            /**
             * @title The top margin
             * @default 25mm
             */
            top: string;
            /**
             * @title The bottom margin
             * @default 25mm
             */
            bottom: string;

        };
        /**
         * @title Handling
         * @description One sided or two sided
         * @default double-sided
         */
        handling: 'single-sided' | 'double-sided';
        /**
         * @title PDF page layout for pdf viewers
         * @description The initial layout
         * @default auto
         */
        pdfLayout: 'auto' | 'single-page' | 'two-page' | 'two-page-right';
        /**
         * @title Whether to set the PDF to duplex (may affect viewers)
         * @default true
         */
        duplex: boolean;
        /**
         * @title Page breaks
         * @description How to handle page breaks
         */
        pageBreaks: {
            /**
             * @title Break before chapters
             * @description determines whether to break before a chapter. Recto will automatically put all new chapters on recto (i.e. right for rtl languages) pages.
             * @default auto
             */
            beforeChapter: 'auto' | 'always' | 'recto' | 'verso' | 'page' | 'avoid';
            /**
             * @title Force standalone documents to start on a certain page (e.g. to force a recto page on standalone collection documents)
             * @description determines whether to break before a document. Recto will automatically put all new chapters on recto (i.e. right for rtl languages) pages. auto and page are equivalent since standalone documents always break page before.
             * @default recto
             */
            beforeDocument: 'recto' | 'verso' | 'auto';
        }

        /**
         * @title Wide page
         * @description Settings for landscape pages (e.g. if a figure is set to landscape)
         */
        widePage: {
            /**
             * @title Wide page orientation
             * @default landscape
             * @description By default a  wide page will rotate the content by 90deg (landscape).
             */
            orientation: 'portrait' | 'landscape' | '270deg';
            /**
             * @title Shrink to fit content
             * @decription Specifying a value of auto will result in wide web pages being scaled down in size to fit the paper width.
             * @default auto
             */
            shrinkToFit: 'auto' | 'none';
        }

        /** 
         * @title Font
         * @description The font to be used in the text. If a font family is specified it has to be either a standard font or configured through the (Google) Font configuration.
         */
        font: {
            /**
             * @title The font family
             * @default Arial
             */
            family: string;
            /**
             * @title Font size
             * @description The font size in pt
             * @default 12pt
             */
            size: string;
            /**
             * @title Specify different font for the cover
             */
            cover: {
                /**
                 * @title The font family
                 * @default inherit
                 */
                family: string;
                /**
                 * @title The font size
                 * @default inherit
                 */
                size: string;
            }
        }
        /**
         * @title Body padding
         * @description Padding between footers and headers and the text body
         * @default 2mm
         */
        bodyPadding: string;
    }

    /**
     * @title Typesetting
     * @description General options for typesetting.
     * @examples require('./page.example.js')
    */
    typesetting: TypesettingConfiguration;

    /**
     * @title The locale (e.g. de-DE, en-GB, ..)
     * @default en-US
     * @maxLength 5
     * @examples ["en-US", "en-GB", "de-DE", "de-CH", "cs-CZ", "pl-PL", "it-IT"]
    */
    locale?: string;

    /**
     * @title The available document placements (cover, front, body, back)
     * @default ["body"]
    */
    placements: string[];

    /**
     * @title Custom styles
     * @description CSS styles that influence the look of the PDF output. Accepts CSS Paged Media.
     */
    customStyles: {
        /**
         * @title CSS code
         * @_ui_widget sfo-textarea
         * */
        css?: string;
    }[];
    /**
     * @title Output
     */
    output: {
        /**
         * @title The pdf profile to use for the exports
         * @description The PDF profiles to be used (see https://www.princexml.com/doc/prince-output/)
         * @default PDF/A-3a+PDF/UA-1
         */
        pdfProfile: string;
    }
}

/**
 * @title Typesetting
 * @description General options for typesetting specifically in PDF exports.
 */
export interface TypesettingConfiguration {
    /** 
     * @title Widows
     * @default 3
     * @description Lines to maintain at the top of subsequent pages to avoid widowed headings or content
     */
    widows?: number;
    /**
     * @title Orphans
     * @default 3
     * @description Minimal orphaned lines at the bottom of pages
     */
    orphans?: number;
    /**
    * @title Indentation
    * @description Whitespace before the first line of a paragraph
    * @default 0mm
    */
    indent?: string;
    /**
     * @title Paragraph spacing
     * @description Space between two paragraphs
     * @default 2mm
     */
    paragraphSpacing?: string | number;
    /**
    * @title Line height
    * @default 1.2
    */
    lineHeight?: string | number;
    /**
     * @title Letter spacing
     * @default initial
     */
    letterSpacing?: string;
    /**
     * @title Text alignment
     * @default justify
     */
    textAlign?: string;
}