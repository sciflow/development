import { Component, PartForRendering, TemplateComponent } from '../../../component';
import { PageConfiguration } from './page.schema';

const isRem = (value) => typeof value === 'string' && value.endsWith('rem');
const isPt = (value) => typeof value === 'string' && value.endsWith('pt');

const remToPt = (remValue, rootFontSizeInPt) => parseFloat(remValue) * rootFontSizeInPt;
const ptToNumber = (ptValue) => parseFloat(ptValue);

@Component({
    readme: `
    # Page setup
    General layout of the page like paper size, margins and typography.`,
    styleUrls: ['./page.component.scss']
})
export class Page extends TemplateComponent<PageConfiguration, any> {

    public get lineHeightInPt() {
        // expecting 1.4, 12pt, 1.4rem, ..
        let lineHeight = this.get('typesetting.lineHeight') || '1';
        // expecting pt
        const fontSizePt = this.get('page.font.size');

        if (!isPt(fontSizePt)) { this.engine.log('warn', 'Page configuration value should end with pt but was ' + fontSizePt); }
        const fontSize = fontSizePt && parseFloat(fontSizePt);
        
        console.assert(fontSize != 0, "Font size cannot be 0");
        if (typeof lineHeight === 'string' && isRem(lineHeight)) { lineHeight = parseFloat(lineHeight.replace('rem', '')); }
        if (typeof lineHeight === 'string' && isPt(lineHeight)) { lineHeight = parseFloat(lineHeight) / (fontSize as number ); }
        return ((lineHeight as number) * (fontSize as number)) + 'pt';
    }

    public renderDOM(defaults?: { runner?: string; componentPaths?: string[]; data?: unknown; }): Promise<PartForRendering | undefined> {
        const placements = this.get('placements') || [];
        const parts = this.engine.data.document.parts.map(part => ({
            ...part,
            placement: part.options?.placement || part.placement || 'body'
        }));
        const outOfPlaceParts = parts.filter(part => !placements?.includes(part.placement));
        if (outOfPlaceParts && outOfPlaceParts.length > 0) {
            this.engine.log('warn', `The following parts are out of place the defined placements (${placements.join(', ')}): ${outOfPlaceParts.map(part => `${part.placement}/${part.partId}`).join(', ')}`);
        }

        return super.renderDOM(defaults);
    }

    public renderStyles(defaults: { runner, assetBasePaths, componentPaths }): string {
        defaults = { ...defaults, assetBasePaths: [...(defaults?.assetBasePaths ? defaults.assetBasePaths : []), __dirname] };
        const styles = super.renderStyles(defaults);
        let css = '';
        let styleList = this.getAs<{css?: string}[]>('customStyles') || [];
        if (styleList && !Array.isArray(styleList)) {
            // stay backwards compatible to older templates
            styleList = [styleList];
        }

        for (let style of styleList) {
            if (style.css) {
                css += '\n\n' + style.css + '\n';
            }
        }

        if (css?.length > 0) { return styles + '\n\n' + css; }

        return styles;
    }
}
