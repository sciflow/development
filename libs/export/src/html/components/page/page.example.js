exports.default = [
    {
        "_comment": "Apa style spacing.",
        "indent": "1cm",
        "paragraphSpacing": "0"
    },
    {
        "_comment": "Default spacing using paragraphs.",
        "indent": "0",
        "paragraphSpacing": "2mm"
    }
];
