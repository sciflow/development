import { Component, PartForRendering, TemplateComponent } from '../../../component';
import { ColumnsConfiguration } from './columns.schema';

@Component({
    styleUrls: ['./columns.scss']
})
export class Columns extends TemplateComponent<ColumnsConfiguration, any> {}
