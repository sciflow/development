export interface ColumnsConfiguration {
    /**
     * @title Column count
     * @default 2
     */
    count: number;
}