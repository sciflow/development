/**
 * @title Table of contents
 * @_ui_icon view_list
 */
export interface TableOfContentsConfiguration {
    /**
     * @title The leader symbol between the heading title and the page number
     * @default .
     */
    leader: '.' | undefined;
    /**
     * @title The text displayed above the table of contents
     * @default Table of Contents
     */
    label: string;
    /**
     * @title TOC levels
     * @description The number of heading levels to show in the table of contents (e.g. 3 means 1.1.1)
     * @default 3
     * @examples [1,2,3,4]
     */
    depth: number;
    /**
     * @title Hyphenation in listings
     * @default manual
     */
    hyphenation: 'auto' | 'manual' | 'none';
}
