import { wrapTOC } from '../../../helpers';
import { slugify } from '../../../utils';
import { TSX } from '../../../TSX';
import { TemplateComponent, Component, PartForRendering } from '../../../component';
import { TableOfContentsConfiguration } from './table-of-contents.schema';
import { RenderingEngine } from '@sciflow/export';
import { GoogleFont } from '../google-font/google-font.component';
import { GoogleFontConfiguration } from '../google-font/google-font.schema';
import { GenericConfiguration } from '../../../interfaces';
import { FontConfiguration } from '../font/font.schema';
import { Font } from '../font/font.component';

@Component({
  styleUrls: ['./table-of-contents.scss']
})
export class TableOfContents extends TemplateComponent<TableOfContentsConfiguration, any> {
  public async renderDOM({ runner, componentPaths }): Promise<PartForRendering | undefined> {
    const headings = this.engine.data.document.tables?.toc.headings?.filter(h => !h.skipToc);
    const defaultContent = {
      id: 'table-of-contents',
      placement: this.page || 'front',
      dom: []
    };

    const depth = this.get('depth') ?? 0;
    if (headings.length === 0) {
      return defaultContent;
    }

    let fontConfiguration: GenericConfiguration<FontConfiguration | GoogleFontConfiguration> = this.engine.data.configurations?.find(c => c.kind === Font.name);
    if (!fontConfiguration) {
      // fall back to google fonts but use the primary font for measurements
      fontConfiguration = this.engine.data.configurations?.find(c => c.kind === GoogleFont.name);
    }
    let maxLabelLength: { [level: number]: { length: number; width: number; } } = {};
    for (let heading of headings) {
      if (!maxLabelLength[heading.level] || maxLabelLength[heading.level]?.length < heading.numberingString.length) {
        const remWidth = Number.parseFloat(((fontConfiguration?.spec?.calculated?.paragraph || '0.45rem').replace('rem', '')));
        maxLabelLength[heading.level] = {
          length: heading.numberingString?.length,
          width: (remWidth * Math.max(2, heading.numberingString?.length) + 1)
        };
      }
    }

    const renderHeadings = (headings, level, parentStyle?) => level <= depth ? <ul style={parentStyle ? parentStyle : ''}>
      {...headings.map(heading => {
        let title = heading.titleHTML
          ? TSX.htmlStringToElement(`<span class="toc-title">${heading.titleHTML}</span>`)
          : <span class="toc-title">{heading.title}</span>;
        const style = heading.numberingString?.length > 0 ? `padding-left: ${maxLabelLength[heading.level]?.width}rem;` : '';
        const content = heading.content?.length > 0 ? renderHeadings(heading.content, level + 1, style) : '';

        const link = heading.title ? <a id={'toc-call-' + slugify(heading.id)} href={`${heading.hostFileName ?? ''}#${heading.id}`} class="link-with-target" style={style}>
          {heading.numberingString ? <span class="toc-numbering label">{heading.numberingString}</span> : ''}
          {title}
        </a> : '';

        return <li data-toc-level={heading.level} data-numbering={heading.numberingString}>
          {link}
          {content}
        </li>;
      })}
    </ul> : '';

    return {
      id: 'table-of-contents',
      placement: this.page || 'front',
      dom: async (engine: RenderingEngine<any, any>) => {
        const toc = await engine.data.tableOfContents;
        if (!toc?.headings || toc.headings.length === 0) {
          return [];
        }

        const headings = this.engine.data.tableOfContents?.headings?.filter(h => !h.skipToc) || [];
        const headingStyles: string[] = [];
        for (let heading of headings) {
          if (heading.placementNumbering) {
            headingStyles.push(`#toc-call-${slugify(heading.id)}::after { content: leader("${this.get('leader')}") target-counter(attr(href), page, ${heading.placementNumbering}); }`);
          }
        }

        return [<div class={this.kind + ' listing section-like'}>
          <section id="table-of-contents">
            <style>
              {headingStyles.join('')}
            </style>
            <h1><span class="heading-title">{this.get('label') || 'Contents'}</span></h1>
            {renderHeadings(wrapTOC(headings), 1)}
          </section>
        </div>];
      }
    };
  }
}
