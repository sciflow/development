/**
 * Heading component schema (extending the base schema needed by the figure renderer)
 */

// TODO: refactor counterStyle to work with Typescript, currently cannot be recognised
// import {counterStyle} from '@sciflow/schema/src/sciflow-types';
import {TypographyRendererConfiguration} from '../../renderers/headings/headings.schema';

export interface Heading1Properties extends TextProperties {
    /** Numbering options */
    numbering: {
        /**
         * @title Suffix
         * @description The suffix added to a chapter number
         * @default ""
         * @minLength 0
         */
        suffix: string;
    }
}

export interface TextProperties {
    /**
     * @title Font size
     */
    fontSize: string;
    /**
     * @title Font family
     * @default inherit
     */
    fontFamily: string;
    /**
         * @title Show heading in line with the text
         * @default false
         */
    inline: boolean;
    /**
     * @title Font weight
     * @default bold
     */
    fontWeight: string | number;
    /**
     * @title Font style
     * @default normal
     */
    fontStyle: 'normal' | 'italic';
    /**
     * @title Align text
     * @default start
     */
    align: string;
    /**
     * @title Line height
     * @default inherit
     * @description The line height of the heading
     */
    lineHeight: string | number;
    /**
     * @title Spacing above
     * @default 1.2em
     */
    marginTop: string;
    /**
     * @title Spacing below
     * @default 0.67em
     */
    marginBottom: string;
    /**
     * @title Letter spacing
     * @default initial
     */
    letterSpacing: string;

    /**
     * @title Text case
     * @description Controls the text case transformation style.
     * @default normal
     * @_readme Hint: Selecting uppercase will change all letters into uppercase letters, while caps will change the typography to use different all-caps symbols
     */
    case: 'normal' | 'uppercase' | 'capitalize' | 'small-caps' | 'all-small-caps';
}

/**
 * @title Typography
 * @_ui_icon format_size
 * @description Styling of headings, text and more.
 */
export interface TypographyConfiguration extends TypographyRendererConfiguration {
    /**
     * @title Hyphenation
     * @default auto
     */
    hyphens: 'auto' | 'manual' | 'none';
    /**
     * @title Numbering inline
     * @description Whether numbering should be included inline
     * @default true
     */
    numberingInline: boolean;
    // no default
    /**
     * @title Default chapter numbering
     */
    defaultNumberingStyle?: 'none' | 'decimal' | 'lower-roman' | 'upper-roman' | 'lower-alpha' | 'upper-alpha';
    
    /**
     * @title Heading 1
     * @description Options for chapter titles
     * @_ui_tag headings
     */
    h1: Heading1Properties;
    /**
     * @title Heading 2
     * @description Options for sub-chapters
     * @_ui_tag headings
     */
    h2: TextProperties;
    /**
     * @title Heading 3
     * @_ui_tag headings
     */
    h3: TextProperties;
    /**
     * @title Heading 4
     * @_ui_tag headings
     */
    h4: TextProperties;
    /**
     * @title Heading 5
     * @_ui_tag headings
     */
    h5: TextProperties;

    /**
     * @title Structural paragraphs
     * @description Allows paragraphs to be handled like sub-headings of no particular level
     */
    structuralParagraph: {
        /**
         * @title Make bold paragraphs act like a heading
         * @default true
         */
        asHeading: boolean;
        /**
         * @title Settings that make this paragraph appear more as a heading
         */
        headingEmulation?: TextProperties;
    }

    /** 
     * @title Lists
     */
    lists: {
        /**
         * @title Don't break before the last n list items
         * @default 1
         */
        dontBreakLast: number;
        /**
         * @title The padding of lists to the left in which the list marker will be placed (or right for rtl languages)
         * @description Additional spacing for the marker
         * @default 1.125em
         */
        paddingStart: string;
        /**
         * @title The margin of lists to the left (or right for rtl languages) (e.g. before the list marker)
         * @default 2rem
         */
        marginStart: string;
        /**
         * @title Align text (start, end, justify)
         * @default start
         */
        alignText: string;
    }

    /**
     * @title Blockquotes
     */
    blockquote: {
        /**
         * @title Font size
         * @default inherit
         */
        fontSize: string;
        /** 
         * @title The margins for blockquotes
         * @default 0.5rem 0 0.5rem 1rem
         */
        margin: string;
        /** 
         * @title Indent
         * @default inherit
         */
        indent: string;
        /**
         * @title Line height
         * @default inherit
         */
        lineHeight: string | number;
        /**
         * @title Font style
         * @default normal
         */
        fontStyle: 'italic' | 'normal';
    }

    poetry: {
        /**
         * @title Font size
         * @default inherit
         */
        fontSize: string;
        /**
         * @title Line height
         * @default inherit
         */
        lineHeight: string | number;
    }
    /**
     * @title Front
     * @description Typography options to style all chapters that are part of the document's front
     */
    front: {
        /**
         * @title Font size
         * @default 1rem
         */
        fontSize: string;
        /** 
         * @title The margins for the front parts
         * @default 0
         */
        margin: string | number;
        /** 
        * @title Indent
        * @default inherit
        */
        indent: string;
        /**
         * @title Line height
         * @default inherit
         */
        lineHeight: string | number;
        /**
         * @title Heading 1 inside the front
         * @description Options for chapter titles
         */
        h1: Heading1Properties;
    };
    /**
     * @title Settings for the bibliography / references section
     * @description The bibligraphy section consists or either automatically set or manually written reference lists with different typographic requirements than the rest of the document (e.g. hanging indents).
     */
    bibliography: {
        /**
         * @title Text align
         * @description Alignment of the individual references (e.g. the paragraph they are written into)
         * @default start
         */
        align: 'start' | 'end' | 'justify';
        /**
         * @title Paragraph
         * @description The indent of the individual references / paragraphs
         * @default 2em hanging
         */
        indent: string | number;
        /**
         * @title Margin
         * @description Margin of the individual references (e.g. the paragraph they are written into)
         * @default 0 0 1rem 0
         */
        margin: string | number;
        /**
         * @title Font size of the references
         * @default inherit
         */
        fontSize: string;
        /**
         * @title Line height of the bibligraphy section.
         * @default inherit
         */
        lineHeight: string | number;
    }
}
