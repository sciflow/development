import { TSX } from '../../..';
import { TemplateComponent, Component } from '../../../component';
import { TypographyConfiguration } from './typography.schema';
@Component({
    readme: `# Typography
    
    Set up heading font sizes, move them inline (into the same line with the following paragraph) and more.
    `,
    styleUrls: ['./typography.component.scss']
})
export class Typography extends TemplateComponent<TypographyConfiguration, any> {}
