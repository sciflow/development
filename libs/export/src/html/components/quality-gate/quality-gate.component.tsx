import { TSX } from '../../../TSX';
import { TemplateComponent, Component, PartForRendering } from '../../../component';
import { QualityGateConfiguration } from './quality-gate.schema';

@Component({
    secondPass: true
})
export class QualityGate extends TemplateComponent<QualityGateConfiguration, any> {
    public async renderDOM(defaults?: { runner?: string | undefined; componentPaths?: string[] | undefined; data?: unknown; } | undefined): Promise<PartForRendering | undefined> {
        return {
            id: this.key,
            placement: this.page,
            subPage: this.subPage,
            dom: [<section>
                {<script>{`
                /*** this script is executed for all PDF exports (@see https://www.princexml.com/doc/js-support) */

                function onComplete() {
                    Log.data("total-page-count", Prince.pageCount);
                }

                Prince.addEventListener("complete", onComplete, false);
                //Prince.registerPostLayoutFunc(function() {
                    //const els = Prince.getElementsByClassName('figure-wrapper');
                //});
                `}</script>}
            </section>]
        }
    }
}