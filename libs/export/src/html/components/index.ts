import { Environment } from './environment/environment.component';
import { Footnote } from './footnote/footnote.component';
import { Page } from './page/page.component';
import { TableOfContents } from './table-of-contents/table-of-contents.component';
import { Title } from './title/title.component';
import { JournalTitle } from './journal-title/journal-title.component';
import { EpubPage } from './epub-page/epub-page.component';
import { Typography } from './typography/typography.component';
import { HeadersAndFooters } from './headers-and-footers/headers-and-footers.component';
import { AuthorBios } from './author-bios/author-bios.component';
import { HowToCite } from './how-to-cite/how-to-cite.component';
import { AuthorSection } from './author-section/author-section.component';
import { MonographTitle } from './monograph-title/monograph-title.component';
import { Listings } from './listings/listings.component';
import { Static } from './static/static.component';
import { PageOffset } from './page-offset/page-offset.component';
import { GoogleFont } from './google-font/google-font.component';
import { CollectionTableOfContents } from './collection-table-of-contents/collection-table-of-contents.component';
import { Font } from './font/font.component';
import { QualityGate } from './quality-gate/quality-gate.component';
import { ContentHole } from './content-hole/content-hole.component';
import { HtmlPage } from './html-page/html-page.component';
import { Settings } from './settings/settings.component';
import { ParagraphNumbering } from './paragraph-numbering/paragraph-numbering.component';
import { Columns } from './columns/columns.component';
import { MetaData } from './meta-data/meta-data.component';
import { PermissionsMeta } from './permissions-meta/permissions-meta.component';
import { BookMeta } from './book-meta/book-meta.component';

export { Page } from './page/page.component';
export { TableOfContents } from './table-of-contents/table-of-contents.component';
export { Typography } from './typography/typography.component';
export { EpubPage } from './epub-page/epub-page.component';

export { Configuration } from './configuration/configuration.component';
export { PageConfiguration } from './page/page.schema';
export { GoogleFontConfiguration } from './google-font/google-font.schema';
export { GoogleFont } from './google-font/google-font.component';
export { TypographyConfiguration } from  './typography/typography.schema';
export { EnvironmentConfiguration } from './environment/environment.schema';
export { FontConfiguration } from './font/font.schema';
export { EpubPageConfiguration } from './epub-page/epub-page.schema';
export { Font } from './font/font.component';
export { Columns } from './columns/columns.component';
export { Environment } from './environment/environment.component';
export { ParagraphNumbering } from './paragraph-numbering/paragraph-numbering.component';
export { ParagraphNumberingConfiguration } from './paragraph-numbering/paragraph-numbering.schema';

import { Configuration } from './configuration/configuration.component';
export { ConfigurationConfiguration } from './configuration/configuration.schema';
export { MetaDataConfiguration } from './meta-data/meta-data.schema';

const components = [
    Configuration,
    Page,
    HtmlPage,
    EpubPage,
    TableOfContents,
    CollectionTableOfContents,
    Columns,
    JournalTitle,
    Font,
    PageOffset,
    MonographTitle,
    GoogleFont,
    Title,
    Static,
    ParagraphNumbering,
    Settings,
    AuthorSection,
    EpubPage,
    AuthorBios,
    HowToCite,
    Footnote,
    Typography,
    Environment,
    Listings,
    HeadersAndFooters,
    QualityGate,
    ContentHole,
    MetaData,
    PermissionsMeta,
    BookMeta
];

export const htmlComponents = components;
export default components;