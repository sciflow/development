interface EnvironmentListing {
    /**
     * @title The threshold to show the listing
     * @description -1 deactivates the listing, 3 would show it if at least three items can be found in the document, 0 would always show it.
     * @default -1
     */
    threshold: number;
    /**
     * @title Listing label
     * The name for the environment listing (e.g. List of lemmata)
     */
    listingLabel: string;
    /**
     * Whether to list the listing in the table of contents.
     * @title List in the table of contents?
     * @default false
     */
    toc: boolean;
}

interface ImageListing extends EnvironmentListing {
    /**
     * @title The threshold to show the listing
     * @description -1 deactivates the listing, 3 would show it if at least three items can be found in the document, 0 would always show it.
     * @default 2
     */
    threshold: number;
    /**
     * @title Listing label for figures
     * @default List of Figures
     */
    listingLabel: string;
    /**
     * @title List in the table of contents?
     * @default false
     */
    toc: boolean;
}
interface TableListing extends EnvironmentListing {
    /**
     * @title The threshold to show the listing
     * @description -1 deactivates the listing, 3 would show it if at least three items can be found in the document, 0 would always show it.
     * @default 2
     */
    threshold: number;
    /**
     * @title Listing label for tables
     * @default List of Tables
     */
    listingLabel: string;
    /**
     * @title List in the table of contents?
     * @default false
     */
    toc: boolean;
}

/**
 * @title Listings
 */
export interface ListingsConfiguration {
    /**
     * @title The leader symbol between the heading title and the page number
     * @default .
     */
    leader: '.' | undefined;
    /**
     * @title Title
     * @description An optional title for the page that contains the listings
     */
    title: string;
    /**
     * @title Listings on individual page
     * @default listings
     * @_readme Determines whether listings end up on their own page (each-listing), on their own recto page (each-listing-recto) or they share a page. each-listing-recto will also affect the table of contents (since it always has it's own page but not necessarily starts on a right page).
     * @examples ["each-listing", "each-listing-recto", "listings"]
     */
    individualPage: undefined | 'each-listing' | 'each-listing-recto' | 'listings';
    environments: {
        image: ImageListing;
        table: TableListing;
        [environment: string]: EnvironmentListing;
    }
    /**
     * @title Align all labels based on the longest label in the listing
     * @default true
    */
    alignLabels: boolean;
    /**
     * @title Hyphenation in listings
     * @default manual
     */
    hyphenation: 'auto' | 'manual' | 'none';
}
