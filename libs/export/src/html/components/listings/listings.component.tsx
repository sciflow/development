import { createFreeDocument } from '@sciflow/schema';
import { TSX } from '../../../TSX';
import { Component, PartForRendering, TemplateComponent } from '../../../component';
import { slugify } from '../../../utils';
import { Environment } from '../environment/environment.component';
import { EnvironmentConfiguration } from '../environment/environment.schema';
import { ListingsConfiguration } from './listings.schema';
import { GoogleFont } from '../google-font/google-font.component';
import { GoogleFontConfiguration } from '../google-font/google-font.schema';
import { GenericConfiguration } from '../../../interfaces';
import { FontConfiguration } from '../font/font.schema';
import { Font } from '../font/font.component';

/**
 * Creates listings for any of the defined environments.
 */
@Component({
  styleUrls: ['./listings.scss']
})
export class Listings extends TemplateComponent<ListingsConfiguration, any> {

  public async renderDOM({ runner, componentPaths }): Promise<PartForRendering | undefined> {

    const combineLabelAndCounter = (label = '', counter: number | string) => `${label} ${typeof counter === 'number' ? counter + 1 : counter}:`;
    let listings: HTMLDivElement[] = [];
    const headings: any[] = [];
    const attrs = {
      placement: this.page || 'front',
      placementNumbering: this.engine.placement[this.page || 'front']?.numbering
    };
    for (let environment of this.getListings()) {
      // TODO make sure the right counter is used (e.g. lower-roman)

      const renderTable = (items, labelLengthRem?) => {
        return <ul>
          {items.map((i, index) => {
            const caption = <span class="caption">{i.title}</span>;
            if (i.titleHTML) { caption.innerHTML = i.titleHTML; }
            return <li data-target-id={i.id}>
              <a href={`${i.hostFileName ?? ''}#${i.id}`} class="link-with-target" style={labelLengthRem ? `padding-left: ${labelLengthRem}rem;` : ''}>
                <span class="label">{combineLabelAndCounter(i.customLabel || environment?.label, i.customNumber || index)} </span>
                {caption}
              </a>
            </li>
          }
          )}
        </ul>;
      };

      // register the items in the headings for the toc to pick it up
      const id = slugify('List of ' + environment?.name) || 'x';
      const tocItems: any[] = [];
      if (environment?.toc) {
        tocItems.push({
          id,
          counterStyle: 'none',
          level: 1,
          partId: id,
          title: environment?.title,
          numberingString: '',
          ...attrs
        });
        headings.push({
          id,
          title: environment?.title,
          ...attrs
        });
      }

      let fontConfiguration: GenericConfiguration<FontConfiguration | GoogleFontConfiguration> = this.engine.data.configurations?.find(c => c.kind === Font.name);
      if (!fontConfiguration) {
        // fall back to google fonts but use the primary font for measurements
        fontConfiguration = this.engine.data.configurations?.find(c => c.kind === GoogleFont.name);
      }

      let labelLengthRem;
      if (this.get('alignLabels')) {
        const labelLengths = [...(environment?.items.map((item, i) => environment?.label ? combineLabelAndCounter(item.customLabel || environment?.label, item.customNumber || i).length : 2) || [])];
        const remWidth = Number.parseFloat(((fontConfiguration?.spec?.calculated?.paragraph || '0.45rem').replace('rem', '')));
        labelLengthRem = remWidth * (Math.max(...labelLengths) + 1) + 0.25;
      }
      listings.push(<div class="listing" id={id}>
        <h1>{environment?.title}</h1>
        {renderTable(environment?.items, labelLengthRem)}
      </div>);
    }

    if (listings.length === 0) {
      return {
        id: 'listings',
        placement: this.page || 'front',
        dom: []
      }
    }

    const title = this.get('title') ?? 'Listings';

    return {
      id: 'listings',
      placement: this.page || 'front',
      title,
      document: headings?.length > 0 ? createFreeDocument(headings, { numbering: 'none', ...attrs }) : undefined,
      // we want the wrapper to be section-like so it breaks like a chapter (e.g. for recto placements)
      dom: [<div class={this.kind + ' section-like'}>
        <section id="listings">
          {...listings}
        </section>
      </div>]
    };
  }

  /**
   * Gets all listings that should be included in the document.
   */
  getListings() {
    const environmentComponent = new Environment(this.engine);
    const configuration: EnvironmentConfiguration = environmentComponent.getValues();
    const listingValues = this.getValues();
    if (!this.engine.data.document.tables) { throw new Error('Tables must be generated at this point'); }
    const labels = this.engine.data.document.tables.labels;
    return Object.keys(labels)
      .map((envName) => {
        const items = labels[envName];
        const environment = configuration.environments[envName];
        const listingConfiguration = listingValues.environments[envName];
        if (!listingConfiguration?.threshold || listingConfiguration?.threshold === -1) { return null; }
        if (!items.length || items.length < listingConfiguration?.threshold) { return null; }

        return {
          name: envName,
          title: listingConfiguration.listingLabel,
          label: environment.label,
          toc: listingConfiguration.toc,
          items
        };

      })
      .filter(v => v !== null);
  }
}
