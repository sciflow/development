import {Component, TemplateComponent} from '../../../component';
import {JatsConfiguration} from './jats.schema';

@Component({
  readme: `#JATS`,
})
export class Jats extends TemplateComponent<JatsConfiguration, any> {}
