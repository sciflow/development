import {Permissions, Publisher} from '../shared/general.interface';

/**
 * @title JATS Export
 * @description This data is needed for a final publication in JATS XML (Authoring Tag Set)
 */
export interface JatsConfiguration {
  /** @title Article data
   *  @description General data about the article usually added by the journal editor.
   */
  articleMeta?: ArticleMeta;

  /** @title Journal meta data
   *  @description Information on the publication
   */
  journalMeta?: JournalMeta;

  /** @title Permissions
   *  @description Copyright statements and licenses.
   */
  permissions?: Permissions;
}

/**
 * @title Article data
 * @description General data about the article usually added by the journal editor.
 */
export interface ArticleMeta {
  /** @title Article version
   *  @description The article version
   *  @default draft
   */
  version?: 'draft' | 'submitted' | 'accepted' | 'proof' | 'version of record';

  /** @title IDs needed for publication
   */
  identifiers?: Identifiers;
}

/**
 * @title IDs needed for publication
 */
export interface Identifiers {
  publisher?: Publisher;
  /** @title The DOI
   *  @description The assigned DOI (e.g. 10.0000/example.00123)
   */
  doi?: string;
}

/**
 * @title Journal meta data
 * @description Information on the publication
 */
export interface JournalMeta {
  /** @title Publication title
   */
  title?: string;

  /** @title Publisher name
   */
  publisher?: string;
}
