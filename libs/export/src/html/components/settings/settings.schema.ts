export interface SettingsConfiguration {
    /**
     * @title The quick settings offered to users.
     * @descriptions A list of paths that will are usually changed often within a template.
     */
    quickSettings: {
        /**
         * @title Components
         */
        components: {
            component: {
                /**
                 * @title Component name
                 * @description The name uniquely identifying the component
                 */
                name: string;

                /**
                 * @title Options
                 * @description An array of . separated paths to be shown
                 */
                options: {
                    /**
                     * @title The path
                     */
                    path: string;
                    /**
                     * @title A label to overwrite the default */
                    title: string;
                    /**
                     * @title Description/hint for the option
                     */
                    description: string;
                }[];
            }
        }[];
    };
}
