import { TSX } from '../../../TSX';
import { Component, PartForRendering, TemplateComponent } from '../../../component';
import { SettingsConfiguration } from "./settings.schema";

@Component({
    readme: `Outputs a static HTML element.`
})
export class Settings extends TemplateComponent<SettingsConfiguration, any> {

}
