import { JSDOM } from "jsdom";

export const createXHTMLInstance = (xmlRootNote: string) => new JSDOM(xmlRootNote, {
    url: 'https://localhost',
    contentType: 'application/xml'
});
