import {mkdirSync, writeFileSync} from 'fs';
import {sync} from 'glob';
import {basename, dirname, extname, join, resolve} from 'path';
import {ModuleKind, ScriptTarget} from 'typescript';
import * as TJS from 'typescript-json-schema';
import {createLogger, format, transports} from 'winston';
import {kebabToPascal} from '../utils/helpers';

const { LOG_LEVEL = 'info' } = process.env;
const customFormat = format.printf(({level, message, label, timestamp, stack}) => {
  return `${timestamp} [${label}] ${level}: ${stack || message}`;
});

const logger = createLogger({
  level: LOG_LEVEL,
  format: format.combine(
    format.label({label: 'generate-schema'}),
    format.timestamp(),
    format.errors({stack: true}), // to include stack trace in the log info
    customFormat,
  ),
  transports: [new transports.Console({ level: LOG_LEVEL })],
});

interface Schema {
  [key: string]: any;
  definitions?: {[key: string]: Schema};
}

/**
 * Resolves refs and deletes resolved refs
 * This is needed as the generation from `typescript-json-schema` creates relative refs inside the generated schema.
 * Parsing these schemas for validations is unideal and should be resolved with the use of `ajv` validation.
 * See here for more info: https://ajv.js.org/guide/combining-schemas.html#combining-schemas-with-ref
 * @param {Schema} schema - The schema to resolve refs within.
 * @param {any} [current=schema] - The current node being processed.
 * @param {any} [parent=null] - The parent node of the current node.
 * @param {string|number|null} [keyInParent=null] - The key in the parent node pointing to the current node.
 * @param {Set<string>} [referencedDefinitions=new Set()] - A set of referenced definitions.
 * @param {Set<any>} [visited=new Set()] - A set of visited nodes to avoid circular references.
 * @returns {Schema} - The schema with resolved refs.
 */
const resolveAndDeleteRefs = (
  schema: Schema,
  current: any = schema,
  parent: any = null,
  keyInParent: string | number | null = null,
  referencedDefinitions: Set<string> = new Set(),
  visited: Set<any> = new Set(),
): Schema => {
  if (current === null || typeof current !== 'object') {
    return current;
  }

  if (visited.has(current)) {
    return current; // Prevent infinite recursion by returning if already visited
  }

  visited.add(current);

  if ('$ref' in current) {
    const refPath = current['$ref'].replace('#/definitions/', '');
    const resolvedSchema = schema.definitions && schema.definitions[refPath];
    if (!resolvedSchema) {
      console.error('Reference not found:', refPath);
      return current; // Return current if the reference cannot be resolved
    }

    // Track the referenced definition
    referencedDefinitions.add(refPath);

    // Merge the resolved schema with any additional properties in current, excluding $ref
    const mergedSchema = {...resolvedSchema, ...current};
    delete mergedSchema['$ref'];

    // Replace the parent's reference to the current object if parent exists
    if (parent && keyInParent !== null) {
      parent[keyInParent] = mergedSchema;
    }

    // Continue resolving and cleaning the merged schema
    Object.keys(mergedSchema).forEach((key) => {
      resolveAndDeleteRefs(
        schema,
        mergedSchema[key],
        mergedSchema,
        key,
        referencedDefinitions,
        visited,
      );
    });

    return mergedSchema;
  } else if (Array.isArray(current)) {
    // Handle arrays separately
    current.forEach((item, index) => {
      resolveAndDeleteRefs(schema, item, current, index, referencedDefinitions, visited);
    });
  } else {
    // Recursively process each property of the object
    Object.keys(current).forEach((key) => {
      resolveAndDeleteRefs(schema, current[key], current, key, referencedDefinitions, visited);
    });
  }

  // Clean referenced definitions when at the root level
  if (parent === null && keyInParent === null) {
    if (schema.definitions) {
      referencedDefinitions.forEach((definitionKey) => {
        delete schema.definitions![definitionKey];
      });
    }
  }

  return current;
};

const shapeSchema = (schema: Schema): Schema => {
  const {$schema, definitions, ...rest} = schema;
  return {
    $schema,
    properties: {
      spec: {
        ...rest,
      },
    },
  };
};

const getSchemaFiles = (baseUrl: string): string[] => {
  const schemaGlob = `${baseUrl}/components/**/*.schema.@(ts|tsx)`;
  return sync(schemaGlob);
};

/**
 * Gets the entry points from the provided files and base URL
 * @param {string[]} files - The array of schema file paths.
 * @param {string} baseUrl - The base URL.
 * @returns {Array<{kind: string; path: string}>} - An array of entry point objects.
 */
const getEntryPoints = (files: string[], baseUrl: string): Array<{kind: string; path: string}> => {
  return files.map((file) => ({
    kind: kebabToPascal(basename(file, extname(file)).replace('.schema', '')), // for getting configuration only
    path: resolve(baseUrl, file),
  }));
};

const getCompilerOptions = (baseUrl: string): TJS.CompilerOptions => {
  return {
    strictNullChecks: true,
    target: ScriptTarget.ES2020,
    lib: ['es6', 'ES2021', 'dom'],
    module: ModuleKind.CommonJS,
    skipLibCheck: true,
    allowUnionTypes: true,
    noEmit: true,
    baseUrl,
    experimentalDecorators: true,
    emitDecoratorMetadata: true,
    allowUnusedLabels: true,
    paths: {
      '@sciflow/*': ['../../../../libs/*/src']
    },
  };
};

/**
 * Processes a single schema file to generate the corresponding JSON schema
 * @param {string} kind - The kind of schema configuration.
 * @param {string} file - The schema file path.
 * @param {string} baseUrl - The base URL.
 * @param {TJS.CompilerOptions} compilerOptions - The compiler options.
 * @returns {Promise<boolean>} - A promise that resolves to true if processing is successful, false otherwise.
 */
const processFile = async (
  kind: string,
  file: string,
  baseUrl: string,
  compilerOptions: TJS.CompilerOptions,
): Promise<boolean> => {
  const settings: TJS.PartialArgs = {
    tsNodeRegister: true, // enable requires
    validationKeywords: ['widget', '_widget'],
  };

  let program;

  try {
    program = TJS.getProgramFromFiles([resolve(file)], compilerOptions);
  } catch (e: any) {
    logger.error('Could not get program files', {message: e.message, file, compilerOptions});
    return false;
  }

  let schemaFragment;
  try {
    schemaFragment = TJS.generateSchema(program, `${kind}Configuration`, settings);

    if (!schemaFragment) {
      return false;
    } else {
      const relativePath = file.replace(baseUrl, '');
      const outputPath = join(
        'dist',
        'libs',
        'export',
        relativePath.replace(/\.[jt]sx?$/, '.json'),
      );

      mkdirSync(dirname(outputPath), {recursive: true});
      schemaFragment = resolveAndDeleteRefs(schemaFragment); // workaround for
      schemaFragment = shapeSchema(schemaFragment);

      writeFileSync(outputPath, JSON.stringify(schemaFragment, null, 2));
      logger.info(`Writing to ${outputPath}`);
      return true;
    }
  } catch (e: any) {
    logger.error(`Could not generate schema for ${file}: ${e.message}`, {
      stack: e.stack,
    });
    return false;
  }
};

const run = async () => {
  const baseUrl = join(__dirname);

  const files = getSchemaFiles(baseUrl);
  if (files.length === 0) {
    const errorMessage = `No schema files found`;
    logger.error(errorMessage);
    throw new Error(errorMessage);
  }

  const entryPoints = getEntryPoints(files, baseUrl);
  const compilerOptions = getCompilerOptions(baseUrl);

  const processedFiles: string[] = [];
  const failedFiles: string[] = [];

  for (let {kind, path: file} of entryPoints) {
    const success = await processFile(kind, file, baseUrl, compilerOptions);
    if (success) {
      processedFiles.push(file);
    } else {
      failedFiles.push(file);
    }
  }

  if (failedFiles.length > 0) {
    failedFiles.forEach((file) => logger.error(`Failed to process file: ${file}`));
  }

  logger.log('info', `Processed ${processedFiles.length} out of ${files.length}.`);
};

run().catch(console.error);
