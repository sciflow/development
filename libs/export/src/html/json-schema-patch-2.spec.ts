// Reference: https://github.com/Starcounter-Jack/JSON-Patch/blob/master/test/spec/validateSpec.mjs

import {expect} from 'chai';
import {GenericConfiguration} from '../interfaces';
import {patchConfigurations, patchSchema} from './json-schema-patch-2';
import {VariantConfiguration} from './components/variant/variant.schema';
import {ComponentSchema} from './json-schema-2';

describe('PatchConfigurations', () => {
  it('should return expected value with new property "b" in object "a" when applying "add" patch', () => {
    const configurations: GenericConfiguration<unknown>[] = [
      {kind: 'TestConfig', spec: {a: {foo: 'bar'}}},
    ];

    const variants: GenericConfiguration<VariantConfiguration>[] = [
      {
        kind: 'Variant',
        spec: {
          patches: [
            {
              type: 'conf',
              target: {kind: 'TestConfig'},
              patch: [{op: 'add', path: '/spec/a/b', value: 'sample'}],
            },
          ],
        },
      },
    ];

    const result = patchConfigurations(configurations, variants);

    expect(result).to.deep.equal([{kind: 'TestConfig', spec: {a: {b: 'sample', foo: 'bar'}}}]);
  });

  it('should return expected value with new property at root when applying "add" patch', () => {
    const configurations: GenericConfiguration<unknown>[] = [
      {kind: 'TestConfig', spec: {a: {foo: 'bar'}}},
    ];

    const variants: GenericConfiguration<VariantConfiguration>[] = [
      {
        kind: 'Variant',
        spec: {
          patches: [
            {
              type: 'conf',
              target: {kind: 'TestConfig'},
              patch: [{op: 'add', path: '/disabled', value: true}],
            },
          ],
        },
      },
    ];

    const result = patchConfigurations(configurations, variants);

    expect(result).to.deep.equal([{kind: 'TestConfig', disabled: true, spec: {a: {foo: 'bar'}}}]);
  });

  it('should throw error when applying "remove" patch on non-existing path', () => {
    const configurations: GenericConfiguration<unknown>[] = [
      {kind: 'TestConfig', spec: {value: 'original'}},
    ];

    const variants: GenericConfiguration<VariantConfiguration>[] = [
      {
        kind: 'Variant',
        spec: {
          patches: [
            {
              type: 'conf',
              target: {kind: 'TestConfig'},
              patch: [{op: 'remove', path: '/spec/nonExistent'}],
            },
          ],
        },
        metadata: {
          name: 'Test Variant',
        },
      },
    ];

    expect(() => patchConfigurations(configurations, variants)).to.throw(
      'Failed to patch the configurations with the following variants: "Test Variant" with the following: Unable to apply patch to configuration "TestConfig" in variant "Test Variant": OPERATION_PATH_UNRESOLVABLE Operation "remove" failed at path "/spec/nonExistent"',
    );
  });

  it('should return expected value when applying "add" patch that contains a sub-path where root does not exist', () => {
    const configurations: GenericConfiguration<unknown>[] = [
      {kind: 'TestConfig', spec: {q: {bar: 2}}},
    ];

    const variants: GenericConfiguration<VariantConfiguration>[] = [
      {
        kind: 'Variant',
        spec: {
          patches: [
            {
              type: 'conf',
              target: {kind: 'TestConfig'},
              patch: [{'op': 'add', 'path': '/spec/a/b', 'value': 'sample'}],
            },
          ],
        },
      },
    ];

    const result = patchConfigurations(configurations, variants);

    expect(result).to.deep.equal([
      {
        kind: 'TestConfig',
        spec: {
          a: {
            b: 'sample',
          },
          q: {
            bar: 2,
          },
        },
      },
    ]);
  });

  it('should throw an error when applying "add" patch on path where the parent path is not an object', () => {
    const configurations: GenericConfiguration<unknown>[] = [
      {kind: 'TestConfig', spec: {a: 'original'}},
    ];

    const variants: GenericConfiguration<VariantConfiguration>[] = [
      {
        kind: 'Variant',
        spec: {
          patches: [
            {
              type: 'conf',
              target: {kind: 'TestConfig'},
              patch: [{'op': 'add', 'path': '/spec/a/b', 'value': 'newValue'}],
            },
          ],
        },
      },
    ];

    expect(() => patchConfigurations(configurations, variants)).to.throw();
  });

  it('should create missing parent paths when applying "replace" patch', () => {
    const configurations: GenericConfiguration<unknown>[] = [
      {kind: 'TestConfig', spec: {value: 'original'}},
    ];

    const variants: GenericConfiguration<VariantConfiguration>[] = [
      {
        kind: 'Variant',
        spec: {
          patches: [
            {
              type: 'conf',
              target: {kind: 'TestConfig'},
              patch: [{op: 'replace', path: '/spec/parent/child', value: true}],
            },
          ],
        },
      },
    ];

    const result = patchConfigurations(configurations, variants);
    expect(result).to.deep.equal([
      {
        kind: 'TestConfig',
        spec: {
          value: 'original',
          parent: {
            child: true,
          },
        },
      },
    ]);
  });

  describe('array patches', () => {
    it('should return expected value with a new path at modified element (index 0) of the array when applying "add" patch', () => {
      const configurations: GenericConfiguration<unknown>[] = [
        {kind: 'TestConfig', spec: {items: [{fooOne: 'bar1'}]}},
      ];

      const variants: GenericConfiguration<VariantConfiguration>[] = [
        {
          kind: 'Variant',
          spec: {
            patches: [
              {
                type: 'conf',
                target: {kind: 'TestConfig'},
                patch: [{op: 'add', path: '/spec/items/0/fooTwo', value: 'bar2'}],
              },
            ],
          },
        },
      ];

      const result = patchConfigurations(configurations, variants);

      expect(result).to.deep.equal([
        {kind: 'TestConfig', spec: {items: [{fooOne: 'bar1', fooTwo: 'bar2'}]}},
      ]);
    });

    it('should return expected value with modified element (index 0) of the array when applying "replace" patch', () => {
      const configurations: GenericConfiguration<unknown>[] = [
        {kind: 'TestConfig', spec: {items: [{fooOne: 'bar1', fooTwo: 'bar2'}]}},
      ];

      const variants: GenericConfiguration<VariantConfiguration>[] = [
        {
          kind: 'Variant',
          spec: {
            patches: [
              {
                type: 'conf',
                target: {kind: 'TestConfig'},
                patch: [{op: 'replace', path: '/spec/items/0/fooOne', value: 'bar1New'}],
              },
            ],
          },
        },
      ];

      const result = patchConfigurations(configurations, variants);

      expect(result).to.deep.equal([
        {kind: 'TestConfig', spec: {items: [{fooOne: 'bar1New', fooTwo: 'bar2'}]}},
      ]);
    });

    it('should throw error when "remove" patch is applied to an array with out-of-bounds index', () => {
      const configurations: GenericConfiguration<unknown>[] = [
        {kind: 'TestConfig', spec: {items: ['item1', 'item2', 'item3']}},
      ];

      const variants: GenericConfiguration<VariantConfiguration>[] = [
        {
          kind: 'Variant',
          spec: {
            patches: [
              {
                type: 'conf',
                target: {kind: 'TestConfig'},
                patch: [{op: 'remove', path: '/spec/items/5'}],
              },
            ],
          },
        },
      ];

      expect(() => patchConfigurations(configurations, variants)).to.throw();
    });

    it('should throw error when "add" patch is applied to a complex array with out-of-bounds index', () => {
      const configurations: GenericConfiguration<unknown>[] = [
        {kind: 'TestConfig', spec: {items: [{fooOne: 'bar1'}]}},
      ];

      const variants: GenericConfiguration<VariantConfiguration>[] = [
        {
          kind: 'Variant',
          spec: {
            patches: [
              {
                type: 'conf',
                target: {kind: 'TestConfig'},
                patch: [{op: 'add', path: '/spec/items/5', value: {fooTwo: 'bar2'}}],
              },
            ],
          },
        },
      ];

      expect(() => patchConfigurations(configurations, variants)).to.throw();
    });

    it('should throw error when "add" patch is applied to a simple array with out-of-bounds index', () => {
      const configurations: GenericConfiguration<unknown>[] = [
        {kind: 'TestConfig', spec: {items: ['item1', 'item2', 'item3']}},
      ];

      const variants: GenericConfiguration<VariantConfiguration>[] = [
        {
          kind: 'Variant',
          spec: {
            patches: [
              {
                type: 'conf',
                target: {kind: 'TestConfig'},
                patch: [{op: 'add', path: '/spec/items/-1', value: 'item5'}],
              },
            ],
          },
        },
      ];

      expect(() => patchConfigurations(configurations, variants)).to.throw();
    });

    it('should throw error when "replace" patch is applied to an array with out-of-bounds index', () => {
      const configurations: GenericConfiguration<unknown>[] = [
        {kind: 'TestConfig', spec: {items: ['item1', 'item2', 'item3']}},
      ];

      const variants: GenericConfiguration<VariantConfiguration>[] = [
        {
          kind: 'Variant',
          spec: {
            patches: [
              {
                type: 'conf',
                target: {kind: 'TestConfig'},
                patch: [{op: 'replace', path: '/spec/items/5', value: 'item-1'}],
              },
            ],
          },
        },
      ];

      expect(() => patchConfigurations(configurations, variants)).to.throw();
    });

    it('should create non-existing property on array element when applying "replace" patch', () => {
      const configurations: GenericConfiguration<unknown>[] = [
        {kind: 'TestConfig', spec: {items: [{foo1: 'bar1'}, {foo2: 'bar2'}, {foo3: 'bar3'}]}},
      ];

      const variants: GenericConfiguration<VariantConfiguration>[] = [
        {
          kind: 'Variant',
          spec: {
            patches: [
              {
                type: 'conf',
                target: {kind: 'TestConfig'},
                patch: [{op: 'replace', path: '/spec/items/0/foo1a', value: 'item-1'}],
              },
            ],
          },
        },
      ];

      const result = patchConfigurations(configurations, variants);
      expect(result).to.deep.equal([
        {
          kind: 'TestConfig',
          spec: {
            items: [{foo1: 'bar1', foo1a: 'item-1'}, {foo2: 'bar2'}, {foo3: 'bar3'}],
          },
        },
      ]);
    });
  });
});

describe('Patch Schema', () => {
  it('should return expected value when applying "add" default patch to add a title, description and readme value', () => {
    const schema = {
      type: 'object',
      properties: {
        'A-for-front': {
          type: 'object',
          properties: {
            b: {
              properties: {
                c: {
                  type: 'string',
                },
              },
            },
          },
          component: {
            kind: 'A',
            metadata: {
              name: 'for-front',
            },
            page: 'front',
            runners: ['princexml'],
          },
        },
      },
    } as ComponentSchema;

    const variants: GenericConfiguration<VariantConfiguration>[] = [
      {
        kind: 'Variant',
        spec: {
          patches: [
            {
              type: 'default',
              target: {kind: 'A-for-front', metadata: {name: 'for-front'}},
              patch: [
                {op: 'add', path: '/properties/b/properties/c/title', value: 'Title C New'},
                {
                  op: 'add',
                  path: '/properties/b/properties/c/description',
                  value: 'Description C New',
                },
                {op: 'add', path: '/properties/b/properties/c/_readme', value: 'Readme C New'},
              ],
            },
            {
              type: 'conf',
              target: {kind: 'A-for-front', metadata: {name: 'for-front'}},
              patch: [
                {op: 'add', path: '/properties/b/properties/c/title', value: 'Should be ignored'},
              ],
            },
          ],
        },
      },
    ];

    const result = patchSchema(schema, variants);

    expect(result).to.deep.equal({
      type: 'object',
      properties: {
        'A-for-front': {
          type: 'object',
          properties: {
            b: {
              properties: {
                c: {
                  type: 'string',
                  title: 'Title C New',
                  description: 'Description C New',
                  _readme: 'Readme C New',
                },
              },
            },
          },
          component: {
            kind: 'A',
            metadata: {
              name: 'for-front',
            },
            page: 'front',
            runners: ['princexml'],
          },
        },
      },
    });
  });

  it('should return expected value when applying "replace" patch to replace the title, description and readme value', () => {
    const schema = {
      type: 'object',
      properties: {
        'A-for-front': {
          type: 'object',
          properties: {
            b: {
              properties: {
                c: {
                  type: 'string',
                  title: 'Title C',
                  description: 'Description C',
                  _readme: 'Readme C',
                },
              },
            },
          },
          component: {
            kind: 'A',
            metadata: {
              name: 'for-front',
            },
            page: 'front',
            runners: ['princexml'],
          },
        },
      },
    } as ComponentSchema;

    const variants: GenericConfiguration<VariantConfiguration>[] = [
      {
        kind: 'Variant',
        spec: {
          patches: [
            {
              type: 'default',
              target: {kind: 'A-for-front', metadata: {name: 'for-front'}},
              patch: [
                {op: 'replace', path: '/properties/b/properties/c/title', value: 'Title C New'},
                {
                  op: 'replace',
                  path: '/properties/b/properties/c/description',
                  value: 'Description C New',
                },
                {op: 'replace', path: '/properties/b/properties/c/_readme', value: 'Readme C New'},
              ],
            },
          ],
        },
      },
    ];

    const result = patchSchema(schema, variants);

    expect(result).to.deep.equal({
      type: 'object',
      properties: {
        'A-for-front': {
          type: 'object',
          properties: {
            b: {
              properties: {
                c: {
                  type: 'string',
                  title: 'Title C New',
                  description: 'Description C New',
                  _readme: 'Readme C New',
                },
              },
            },
          },
          component: {
            kind: 'A',
            metadata: {
              name: 'for-front',
            },
            page: 'front',
            runners: ['princexml'],
          },
        },
      },
    });
  });

  it('should return expected value when applying "remove" patch to remove the title, description, and readme value across multiple variants', () => {
    const schema = {
      type: 'object',
      properties: {
        'A-for-front': {
          type: 'object',
          properties: {
            b: {
              properties: {
                c: {
                  type: 'string',
                  title: 'Title C',
                  description: 'Description C',
                  _readme: 'Readme C',
                },
              },
            },
          },
          component: {
            kind: 'A',
            metadata: {
              name: 'for-front',
            },
            page: 'front',
            runners: ['princexml'],
          },
        },
      },
    } as ComponentSchema;

    const variants: GenericConfiguration<VariantConfiguration>[] = [
      {
        kind: 'Variant',
        spec: {
          patches: [
            {
              type: 'default',
              target: {kind: 'A-for-front', metadata: {name: 'for-front'}},
              patch: [{op: 'remove', path: '/properties/b/properties/c/title'}],
            },
          ],
        },
      },
      {
        kind: 'Variant',
        spec: {
          patches: [
            {
              type: 'default',
              target: {kind: 'A-for-front', metadata: {name: 'for-front'}},
              patch: [{op: 'remove', path: '/properties/b/properties/c/description'}],
            },
          ],
        },
      },
      {
        kind: 'Variant',
        spec: {
          patches: [
            {
              type: 'default',
              target: {kind: 'A-for-front', metadata: {name: 'for-front'}},
              patch: [{op: 'remove', path: '/properties/b/properties/c/_readme'}],
            },
          ],
        },
      },
    ];

    const result = patchSchema(schema, variants);

    expect(result).to.deep.equal({
      type: 'object',
      properties: {
        'A-for-front': {
          type: 'object',
          properties: {
            b: {
              properties: {
                c: {
                  type: 'string',
                },
              },
            },
          },
          component: {
            kind: 'A',
            metadata: {
              name: 'for-front',
            },
            page: 'front',
            runners: ['princexml'],
          },
        },
      },
    });
  });

  it('should return expected value when applying "replace" patch to replace an element in the default and enum array', () => {
    const schema = {
      type: 'object',
      properties: {
        'A-for-front': {
          type: 'object',
          properties: {
            b: {
              properties: {
                c: {
                  type: 'string',
                  default: 'enum2',
                  enum: ['enum1', 'enum2', 'enum3'],
                },
              },
            },
          },
          component: {
            kind: 'A',
            metadata: {
              name: 'for-front',
            },
            page: 'front',
          },
        },
      },
    } as ComponentSchema;

    const variants: GenericConfiguration<VariantConfiguration>[] = [
      {
        kind: 'Variant',
        spec: {
          patches: [
            {
              type: 'default',
              target: {kind: 'A-for-front', metadata: {name: 'for-front'}},
              patch: [
                {
                  op: 'replace',
                  path: '/properties/b/properties/c/default',
                  value: 'enum1',
                },
                {
                  op: 'replace',
                  path: '/properties/b/properties/c/enum/0',
                  value: 'enum1New',
                },
              ],
            },
          ],
        },
      },
    ];

    const result = patchSchema(schema, variants);

    expect(result).to.deep.equal({
      type: 'object',
      properties: {
        'A-for-front': {
          type: 'object',
          properties: {
            b: {
              properties: {
                c: {
                  type: 'string',
                  enum: ['enum1New', 'enum2', 'enum3'],
                  default: 'enum1',
                },
              },
            },
          },
          component: {
            kind: 'A',
            metadata: {
              name: 'for-front',
            },
            page: 'front',
          },
        },
      },
    });
  });

  it('should return expected value when applying "replace" patch to replace an element in the examples array', () => {
    const schema = {
      type: 'object',
      properties: {
        'A-for-front': {
          type: 'object',
          properties: {
            b: {
              properties: {
                c: {
                  type: 'string',
                  examples: [
                    {
                      '_comment': 'Apa style spacing.',
                      'indent': '1cm',
                      'paragraphSpacing': '0',
                    },
                    {
                      '_comment': 'Default spacing using paragraphs.',
                      'indent': '0',
                      'paragraphSpacing': '2mm',
                    },
                  ],
                },
              },
            },
          },
          component: {
            kind: 'A',
            metadata: {
              name: 'for-front',
            },
            page: 'front',
          },
        },
      },
    } as ComponentSchema;

    const variants: GenericConfiguration<VariantConfiguration>[] = [
      {
        kind: 'Variant',
        spec: {
          patches: [
            {
              type: 'default',
              target: {kind: 'A-for-front', metadata: {name: 'for-front'}},
              patch: [
                {
                  op: 'replace',
                  path: '/properties/b/properties/c/examples/1/indent',
                  value: '100cm',
                },
                {
                  op: 'replace',
                  path: '/properties/b/properties/c/examples/0/_comment',
                  value: 'this is a new comment',
                },
              ],
            },
          ],
        },
      },
    ];

    const result = patchSchema(schema, variants);

    expect(result).to.deep.equal({
      type: 'object',
      properties: {
        'A-for-front': {
          type: 'object',
          properties: {
            b: {
              properties: {
                c: {
                  type: 'string',
                  examples: [
                    {
                      '_comment': 'this is a new comment',
                      'indent': '1cm',
                      'paragraphSpacing': '0',
                    },
                    {
                      '_comment': 'Default spacing using paragraphs.',
                      'indent': '100cm',
                      'paragraphSpacing': '2mm',
                    },
                  ],
                },
              },
            },
          },
          component: {
            kind: 'A',
            metadata: {
              name: 'for-front',
            },
            page: 'front',
          },
        },
      },
    });
  });

  it('should throw an error when examples are removed then try to append to the array', () => {
    const schema = {
      type: 'object',
      properties: {
        'A-for-front': {
          type: 'object',
          properties: {
            locale: {
              examples: ['en-US', 'en-GB', 'de-DE', 'de-CH', 'cs-CZ', 'pl-PL', 'it-IT'],
              type: 'string',
            },
          },
          component: {
            kind: 'A',
            metadata: {
              name: 'for-front',
            },
          },
        } as ComponentSchema,
      },
    } as ComponentSchema;

    const variants: GenericConfiguration<VariantConfiguration>[] = [
      {
        kind: 'Variant',
        spec: {
          patches: [
            {
              type: 'default',
              target: {kind: 'A', metadata: {name: 'for-front'}},
              patch: [
                {op: 'remove', path: '/properties/locale/examples'},
                {
                  op: 'add',
                  path: '/properties/locale/examples/-',
                  value: 'es-ES',
                },
              ],
            },
          ],
        },
      },
    ];

    expect(() => patchSchema(schema, variants)).to.throw();
  });

  it('should not apply patch when target does not match', () => {
    const schema: ComponentSchema = {
      type: 'object',
      properties: {
        'A': {
          type: 'object',
          properties: {
            A: {type: 'string', title: 'A '},
          },
        },
      },
    };

    const variants: GenericConfiguration<VariantConfiguration>[] = [
      {
        kind: 'Variant',
        spec: {
          patches: [
            {
              type: 'default',
              target: {kind: 'B'},
              patch: [{op: 'add', path: '/properties/title', value: 'B'}],
            },
          ],
        },
      },
    ];

    const result = patchSchema(schema, variants);

    expect(result).to.deep.equal(schema);
  });
});
