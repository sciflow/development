import { JSDOM } from 'jsdom';
import { TSX } from '../TSX';
import { dump } from 'js-yaml';

import { createLogger, format, transports } from 'winston';
import { ExportOptions, GenericConfiguration, SciFlowDocumentData } from "../interfaces";
import { RenderingEngine } from "../renderers";

import { getDefaults, ReferenceRenderer } from '@sciflow/cite';
import { DocumentNode, DocumentSnapshot, DocumentSnapshotResource, ProjectSnapshot, SFNodeType } from '@sciflow/schema';
import { extractHeadingTitle, getTextContent } from '../helpers';
import { decamelize } from '../utils/helpers';

import JSZip from 'jszip';
import { join } from 'path';
import { getValueOrDefault, PartForRendering } from '../component';
import { getComponentSchema } from '../json-schema';
import defaultTemplateComponents, { Page, PageConfiguration } from './components';
import { mapDoc } from './convert';
import { createInstancesAndStyles, instancesToDom } from './createDocument';
import { createRenderingEngine } from './createEngine';
import { transformFiles } from './files';
import { FigureRendererConfiguration } from './renderers/figure/figure.schema';
// TODO move this into a shared space
import { getCitation } from '../xml/renderers/references';

export * from './convert';
export * from './createDocument';
export * from './createEngine';
export * from './files';
export * from './generateListings';
export * from './json-schema-2';

const { styleXML, localeXML } = getDefaults();

const dom = new JSDOM();
const virtualDocument = dom.window.document;
const { LOG_LEVEL = 'info' } = process.env;
const logger = createLogger({
    level: LOG_LEVEL,
    format: format.json(),
    defaultMeta: { service: 'html-main' },
    transports: [
        new transports.Console()
    ],
});

let defaultSchemas: { [runnersAndTemplate: string]: any } = {};
export const getSchemaPath = () => {
    return __dirname;
}

/**
 * 
 * @deprecated Use json-schema-2
 * @param kinds 
 * @param componentPath 
 * @returns 
 */
export const getSchemaDefinitionsFromPath = (kinds: string[], componentPath = __dirname) => {
    const configurations: GenericConfiguration<any>[] = kinds.map(kind => ({ kind, spec: {} }));
    return getComponentSchema(kinds, configurations, componentPath);
}

/** Loads and caches the default component configurations. */
export const getDefaultSchemas = (templateSlug: string, kinds: string[], configurations: GenericConfiguration<any>[], runner?: string) => {
    if (!runner) { runner = 'all-runners'; }

    if (!configurations.some(c => c.kind === 'Environment')) {
        kinds.push('Environment');
        configurations.push({
            kind: 'Environment',
            spec: {
                environments: {}
            }
        });
    }

    if (!defaultSchemas[runner + '&' + templateSlug]) {
        defaultSchemas[runner + '&' + templateSlug] = getComponentSchema(kinds, configurations, join(__dirname));
    }

    return defaultSchemas[runner + '&' + templateSlug];
};

export const labelToEnvironment = (node: DocumentNode<SFNodeType.label>, environments: FigureRendererConfiguration): string | undefined => {
    if (!node) { return undefined; }
    if (node.type !== SFNodeType.label) { throw new Error('Must provide label node. Received ' + node.type); }
    const labelText = getTextContent(node);
    if (!labelText) { return undefined; }
    for (let key of Object.keys(environments.environments)) {
        const env = environments.environments[key];
        if (labelText?.startsWith(env.label)) {
            return key;
        }
    }

    return undefined;
}

const flattenMetaData = (metaData: any) => {
    const retval = {};
    for (const key in metaData) {
        if (!metaData.hasOwnProperty(key)) { continue; }
        if ((typeof metaData[key]) === 'object' && metaData[key] !== null) {
            const flatObject = flattenMetaData(metaData[key]);
            for (var childKey in flatObject) {
                if (!flatObject.hasOwnProperty(childKey)) { continue; }
                retval[key + '.' + childKey] = flatObject[childKey];
            }
        } else {
            retval[key] = metaData[key];
        }
    }

    return retval;
}

export const generateRootVarsFromMetaData = (metaData) => {
    let values = flattenMetaData(metaData);
    let rootVars = {};
    for (let key in values) {
        if (key === 'id' || key === '_id' || key === '_rev') { continue; }
        rootVars['--' + key.replaceAll('.', '-')] = `"${values[key]}"`;
    }
    return rootVars;
}

export const createComponent = async (configuration: GenericConfiguration<any>, { document }): Promise<string> => {
    const citationRenderer = new ReferenceRenderer(styleXML, localeXML, []);
    //const componentPath = join(__dirname, 'resources');
    const componentPath = getSchemaPath();
    const configurations = [configuration];
    if (!configurations.some(c => c.kind === 'Environment')) {
        configurations.push({
            kind: 'Environment',
            spec: {
                environments: {}
            }
        });
    }

    const metaDataSchema = getComponentSchema(configurations.map(c => c.kind), configurations,
        componentPath,
        (name => configurations.map(c => c.kind).some(kind => name.includes(decamelize(kind))))
    );

    // const tables = await generateListings(document.parts.filter(p => p.role !== 'coverpage'), undefined, undefined, configurations);
    const engine: RenderingEngine<HTMLElement, SciFlowDocumentData> = await createRenderingEngine({
        document: {
            ...document,
            // tables
        },
        documentSnapshot: document,
        configurations,
        citationRenderer,
        customRenderers: {},
        templateOptions: {},
        metaData: {},
        metaDataSchema
    });

    const c = defaultTemplateComponents.find(c => c.name === configuration.kind);
    if (c) {
        const instance = new c(engine);
        const dom = await instance.renderDOM({ runner: 'html', componentPaths: [componentPath] });
        if (typeof dom?.dom === 'function') {
            const componentHTML = await dom.dom(engine);
            const styles = await instance.renderStyles();
            if (componentHTML) {
                const html = <html>
                    <head>
                        <style>
                            {styles}
                        </style>
                    </head>
                    <body>
                        {...componentHTML}
                    </body>
                </html>;
                return html.outerHTML;
            }
        }
    }

    return 'No component found';
}

/**
 * Renders a document as a ZIP
 * @param exportData 
 * @param options 
 * @returns the ZIP
 */
export const createHTMLZip = async (exportData: DocumentSnapshot | ProjectSnapshot, options?: ExportOptions) => {

    const runner = options?.runner || 'html';
    const scripts: any[] = options?.scripts || [];

    const zip = new JSZip();
    const assetDir = zip.folder('assets');
    const fontDir = zip.folder('fonts');

    // download files if needed
    const preparedFiles = await transformFiles(exportData, options, { assetDir, fontDir });
    // create the main document
    const instanceResult = await createInstancesAndStyles(exportData, options, { runner, files: preparedFiles, id: 'main', assetDir, fontAsSeparateFile: options?.separateFonts });
    const {
        engine,
        document,
        inlineStyles,
        files,
        cssStyles,
        locale,
        title,
        templateComponents,
        orderedPartsAndInstances
    } = instanceResult;

    const citation = getCitation(exportData);

    //const citationString = renderAuthorList('filename', citation, true, localeXML[locale]);

    /** A string that is put out as a readme file with the ZIP */
    let readmeMd = `---\n${dump({
        citation,
        locale,
    })}\n---\n`;

    if (!engine.data.document.tables) { throw new Error('Tables must be generated at this point'); }
    const images = engine.data.document.tables.images;
    const ids = engine.data.document.tables.ids;

    const pageConfigurationInstance = new Page(engine as any);
    const pdfPageConfiguration: PageConfiguration = pageConfigurationInstance.getValues();

    const altTitle = getValueOrDefault('article-meta.altTitle', exportData.metaData, options?.metaDataSchema?.properties?.MetaData);
    const publisherName = getValueOrDefault('journal-meta.publisherName', exportData.metaData, options?.metaDataSchema?.properties?.MetaData);
    const titleContent = document.titlePart ? await extractHeadingTitle(document.titlePart?.document?.content.find(n => n.type === SFNodeType.heading), engine) : undefined;

    let nav: { i?: number; name?: string; partId?: string; }[] = [];
    /** A map of all parts and corresponding file names for cross-file linking */
    let partFileMap = {};

    let styleFileName = 'styles.css';
    if (options?.separateFonts) {
        // we extract the css responsible for adding the fonts because we assume it is globally available
        const fontStyleFile = files.find(f => f.id === 'fonts.css' && f.mimeType == 'text/css');
        if (fontStyleFile?.content) {
            // TODO move this outside of the file
            readmeMd += '# Readme\n\n';
            readmeMd += `## Hosting Fonts for This Publication (notes for web admins)

This section only applies if you want to host the fonts for an issue or journal in a single place instead of uploading the entire contents of this ZIP file.

### Overview
This publication includes custom fonts to ensure proper styling and readability. These fonts are referenced in the \`fonts.css\` file and are stored in the \`fonts/\` directory within this ZIP archive.

If you prefer to host the fonts on your own domain instead of using the provided files, you must modify the \`fonts.css\` file to reflect the new font locations.

### How to Host Fonts on Your Own Domain

1. **Upload the \`fonts/\` directory**  
   Place the \`fonts/\` directory (included in this ZIP) onto your web server at a publicly accessible location.

2. **Modify \`fonts.css\` to update font URLs**  
   - Open \`fonts.css\` in a text editor.
   - Locate all instances of \`src: url("fonts/...")\`.
   - Replace \`"fonts/..."\` with the full URL where the fonts are hosted on your domain.  
     Example:
     \`\`\`css
     src: url("https://yourdomain.com/path-to-fonts/Lato-400.woff2") format("woff2");
     \`\`\`

3. **Ensure the modified \`fonts.css\` is used**  
   - If the document system allows you to upload a custom stylesheet, replace the existing \`fonts.css\` with your modified version.
   - fonts.css is automatically imported in the first line of the styles.css

### Important Notes
- Hosting the fonts on your domain ensures faster loading and avoids potential issues with cross-origin resource sharing (CORS).
- If you experience font loading issues, verify that your web server correctly serves font files with the appropriate MIME types (\`woff2: font/woff2\`).
- Some fonts may have licensing restrictions. Ensure you comply with any licensing agreements before hosting them yourself.
- When hosting fonts and css on a different domain, make sure the content security policy allows including files from that domain.
\n`;
            await zip.file('fonts.css', fontStyleFile.content);
        }
    }
    if (cssStyles) { await zip.file(styleFileName, options?.separateFonts ? `/* styles for hosted fonts */ \n@import url("fonts.css");\n\n\n'${cssStyles || ''}` : cssStyles); }

    if (!orderedPartsAndInstances.some(p => p.standalone)) {
        // standard export as one html file
        zip.file('manuscript.html', (await instancesToDom(orderedPartsAndInstances, { engine, altTitle, title, locale, cssStyles, inlineStyles, scripts, styleFileName, document, standalone: false })).outerHTML);
        nav.push({ name: 'manuscript.html' });
    } else {
        // handle documents that need the content to be split into multiple html files (e.g. to isolate document styles between chapters/subdocuments)
        // this is needed to re-use article headers and footers in a collection amongst other use cases
        // if parts have a parent (e.g. they come from a subdocument) we render them as their own document, parts without parents are rendered together
        let partFiles: (PartForRendering | PartForRendering[])[] = [];
        for (let part of orderedPartsAndInstances) {
            // we'll end up with an array of parts with parents and arrays or parts without parent
            if (part.parent && part.standalone) {
                if (!part.template) { throw new Error('Sub parts must have templates'); }
                partFiles.push(part);
            } else {
                // try to find an active array to push into
                let pushInto: PartForRendering | PartForRendering[] = partFiles[partFiles.length - 1];
                if (!pushInto || !Array.isArray(pushInto)) {
                    // create a new group
                    pushInto = [];
                    partFiles.push(pushInto);
                }
                // attach the document part to the previous ones to later be rendered together
                (pushInto as PartForRendering[]).push(part);
            }
        }

        const padding = partFiles?.length > 100 ? 3 : partFiles?.length > 10 ? 2 : 1;

        let i = 0;
        // run through the final file names so they can be used while the content is generated
        for (let groupOrPart of partFiles) {
            i++;
            const prefix = `${(i + '').padStart(padding, '0')}`;
            if (Array.isArray(groupOrPart)) {
                for (let part of groupOrPart) {
                    partFileMap[part.id] = `${prefix}.html`;
                }
            } else {
                const partFileName = `${prefix}_${groupOrPart.id}.html`;
                partFileMap[groupOrPart.id] = partFileName;
                // also add the parts of the sub-document that is rendered standalone for cross referenceing to it
                for (let part of groupOrPart.parent?.parts) {
                    partFileMap[part.id] = partFileName;
                }
            }
        }

        // generate new listings based on the partFileMap
        await engine.generateListings(engine.data.document.parts.map(part => mapDoc(part, engine.placement)), 1, partFileMap);
        // FIXME #1146 we seem to have a race condition where numberedString is not available when some parts are being generated
        await new Promise(r => setTimeout(r, 250));

        i = 0;
        for (let groupOrPart of partFiles) {
            i++;
            const prefix = `${(i + '').padStart(padding, '0')}`;
            const partId = Array.isArray(groupOrPart) ? undefined : groupOrPart.id;
            const name = `${prefix}${Array.isArray(groupOrPart) ? '' : ('_' + groupOrPart.id)}.html`;

            let partEl;
            if (Array.isArray(groupOrPart)) {
                // render the grouped instances now
                // we use the shared stylefile for all of these grouped parts
                partEl = await instancesToDom(groupOrPart, { index: i, engine, altTitle, title, locale, cssStyles, inlineStyles, scripts, styleFileName, document, standalone: false });
            } else {
                // create a new engine and content for the child document
                // using the sub-template
                const subData = {
                    ...groupOrPart.parent,
                };
                const subOptions = {
                    ...options,
                    configurations: groupOrPart.template.configurations,
                    metaData: groupOrPart.parent.metaData || {},
                    metaDataSchema: groupOrPart.template.metaData,
                    files: groupOrPart.template.files,
                    customRenderers: groupOrPart.template.customRenderers,
                    locale: groupOrPart.parent.locale || groupOrPart.template.locale,
                    customTemplateComponents: [
                        ...(options?.customTemplateComponents || []),
                        ...groupOrPart.template.customTemplateComponents
                    ],
                };
                const subfiles = await transformFiles({
                    ...subData,
                    files: [] // we do not want to re-process document files again since they are part of the parent files array
                }, subOptions, { assetDir, fontDir });
                subfiles.push(...files.filter(f => subData.documentId && f.documentId === subData.documentId)
                    .map(f => ({ ...f, id: f.id || f.fileId } as DocumentSnapshotResource))
                    .map(f => ({ ...f, id: f.id.startsWith(subData.documentId) ? f.id : `${subData.documentId}-${f.id}` }))
                    .map(f => ({ ...f, fileId: f.id }))
                );
                const partContent = await createInstancesAndStyles(subData, subOptions, { runner, files: subfiles, id: [i, partId].filter(v => v != undefined).join('-'), assetDir, fontAsSeparateFile: false });
                if (partContent.cssStyles) { await zip.file(groupOrPart.id ? (groupOrPart.id + '.css') : 'styles.css', partContent.cssStyles); }

                // render the sub document into an HTML element
                // just access the pre-rendered dom
                // the styles were already written previously when the DOM was created
                const subAltTitle = getValueOrDefault('article-meta.altTitle', groupOrPart.parent.metaData, groupOrPart.template.metaData);
                const subTitleContent = document.titlePart ? await extractHeadingTitle(groupOrPart.parent?.parts?.find(p => p.type === 'title')?.document?.content.find(n => n.type === SFNodeType.heading), engine) : undefined;
                partEl = await instancesToDom(partContent.orderedPartsAndInstances, {
                    document: groupOrPart.parent,
                    engine,
                    altTitle,
                    title: subAltTitle,
                    locale,
                    cssStyles,
                    inlineStyles,
                    scripts,
                    standalone: groupOrPart.standalone,
                    styleFileName: groupOrPart.id ? (groupOrPart.id + '.css') : 'styles.css'
                });
            }

            nav.push({ i, partId, name });
            await zip.file(name, partEl.outerHTML);
        }
    }

    // create a directory of all used files (similar to an epub nav document)
    await zip.file('index.json', JSON.stringify({
        files: nav,
        partFileMap,
        pdfProfile: pdfPageConfiguration?.output?.pdfProfile
    }, undefined, 2));

    if (readmeMd?.length > 0) {
        await zip.file('readme.md', readmeMd);
    }

    zip.forEach(f => console.log(f));

    // clean up the unused images before continuing
    assetDir?.forEach((path, file) => {
        if (file.name.startsWith('assets') && !file.dir) {
            const originalFile = files.find(i => path.startsWith(i.id));
            // find the image in the main document or any id that ends with the file name (might be prefixed with the documentId)
            let image = images.find(i => path.startsWith(i.id)) || (originalFile && ids.find(i => i.id != undefined && `${i.id}`.endsWith(originalFile.id)));
            if (!image && !originalFile?.static) {
                engine.log('verbose', 'Removing unused image ' + path);
                assetDir.remove(path);
            }
        }
    });

    return await zip.generateAsync({
        type: 'nodebuffer',
        mimeType: 'application/epub+zip'
    });
}

export {
    createRenderingEngine,
    defaultTemplateComponents
};

