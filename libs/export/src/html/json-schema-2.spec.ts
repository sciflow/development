import {expect} from 'chai';
import {join} from 'path';
import {GenericConfiguration} from '../interfaces';
import {removeNestedNullUndefined} from '../utils/helpers';
import {
  dereferenceDefinitions,
  dereferenceSchema,
  generateSchema,
  getComponentPaths,
  buildTemplateSchema,
  replaceUnionTypes,
  setDefaultValues,
} from './json-schema-2';
import {readYAMLDocumentsFromFile} from '../configuration';

const genericConfigurationStructure = {
  $id: '/template/configuration/Generic',
  type: 'object',
  additionalProperties: false,
  properties: {
    kind: {
      description: 'The component name (unique across all components)',
      type: 'string',
    },
    metadata: {
      description:
        'Meta data that can be used to uniquely identify instances of a component inside a configuration.',
      type: 'object',
      additionalProperties: {},
      properties: {
        name: {
          type: 'string',
        },
        title: {
          description: 'A human readable title',
          type: 'string',
        },
        description: {
          description: 'The instance description with details for users',
          type: 'string',
        },
        group: {
          description: 'Related to a configuration kind',
          type: 'string',
        },
      },
    },
    page: {
      type: 'string',
    },
    subPage: {
      description:
        'A placement if this configuration should be used in a different document position',
      type: 'string',
    },
    spec: {
      type: 'object',
      additionalProperties: false,
      description: 'The configuration values',
    },
    translations: {
      description: 'Optional translations for the values provided in the spec.',
      type: 'object',
      additionalProperties: {
        type: 'object',
        additionalProperties: false,
      },
    },
    locales: {
      description: 'The locales supported by a component (leave empty for all)',
      type: 'array',
      items: {
        type: 'string',
      },
    },
    runners: {
      description: 'The export runners the configuration is meant for (e.g. princexml, html, epub)',
      type: 'array',
      items: {
        type: 'string',
      },
    },
    disabled: {
      type: 'boolean',
    },
    ui: {
      type: 'object',
      properties: {
        showTo: {
          description: 'Who the UI should be shown to (empty array for no-one)',
          type: 'array',
          items: {
            type: 'string',
          },
        },
      },
      additionalProperties: false,
    },
  },
  $schema: 'http://json-schema.org/draft-07/schema#',
};

describe('JSON Schema', async () => {
  describe('getComponentPaths', () => {
    it('GIVEN - valid subdirectory containing a schema - WHEN - calling getComponentPaths - THEN - it should return only the schema', () => {
      // ARRANGE
      const kind: string = 'Simple';
      const kinds: string[] = [kind];
      const dir: string = join(__dirname, '..', '..', 'test', 'fixtures', 'fixture-1');

      // ACT
      const results: Map<string, string> = getComponentPaths(kinds, dir);

      // ASSERT
      expect(results).to.have.all.keys(kind);
      expect(results.get(kind)).to.include(
        '/fixtures/fixture-1/components/simple/simple.schema.ts',
      );
    });

    it('GIVEN - valid subdirectories containing the same schema - WHEN - calling getComponentPaths - THEN - it should return only the latest file', () => {
      /*
      fixture-2/components/page/page.schema.ts
      fixture-2/components/page-2/page.schema.ts
      */
      // ARRANGE
      const kind: string = 'Page';
      const kinds: string[] = [kind];
      const dir: string = join(__dirname, '..', '..', 'test', 'fixtures', 'fixture-2');

      // ACT
      const results: Map<string, string> = getComponentPaths(kinds, dir);

      // ASSERT
      expect(results).to.have.all.keys(kind);
      expect(results.get(kind)).to.include('/fixtures/fixture-2/components/page-2/page.schema.ts');
    });

    it('GIVEN - a subdirectory containing non .tsx or .ts files - WHEN - calling getComponentPaths - THEN - it should return an empty map', () => {
      // ARRANGE
      const kind: string = 'Page';
      const kinds: string[] = [kind];
      const dir: string = join(__dirname, '..', '..', 'test', 'fixtures', 'fixture-3');

      // ACT
      const results: Map<string, string> = getComponentPaths(kinds, dir);

      // ASSERT
      expect(results).to.be.empty;
    });

    it('GIVEN - a non-existing path - WHEN - calling getComponentPaths - THEN - it should return an empty map', () => {
      // ARRANGE
      const kind: string = 'Page';
      const kinds: string[] = [kind];
      const dir: string = join(__dirname, '..', '..', 'test', 'fixtures', 'fixture-missing');

      // ACT
      const results: Map<string, string> = getComponentPaths(kinds, dir);

      // ASSERT
      expect(results).to.be.empty;
    });
  });

  describe('generateSchema', function () {
    this.timeout(6000);

    it('GIVEN - valid simple annotated schema - WHEN - calling generateSchema - THEN - it should return a schema', async () => {
      // ARRANGE
      const expected = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        additionalProperties: false,
        title: 'PDF Setup',
        description: 'Set up page margins, fonts and page sizes.',
        type: 'object',
        properties: {
          a: {
            title: 'A',
            description: 'Some description',
            type: 'number',
          },
        },
      };

      const expectedSchema = {
        ...genericConfigurationStructure,
        $id: '/template/configuration/Simple.json',
        additionalProperties: false,
        properties: {
          ...genericConfigurationStructure.properties,
          spec: expected,
          translations: {
            ...genericConfigurationStructure.properties.translations,
            additionalProperties: expected,
          },
        },
      };

      // ACT
      const result = await generateSchema(
        'Simple',
        'libs/export/test/fixtures/fixture-1/components/simple/simple.schema.ts',
        __dirname,
      );

      // ASSERT
      expect(result).to.deep.equal(expectedSchema);
    });

    it('GIVEN - valid schema that extends another schema with parent annotations - WHEN - calling generateSchema - THEN - it should return a schema ', async () => {
      // ARRANGE
      const expected = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        title: 'A',
        additionalProperties: false,
        properties: {
          a: {
            title: 'A',
            type: 'number',
          },
          c: {
            title: 'c',
            type: 'string',
          },
        },
      };

      const expectedSchema = {
        ...genericConfigurationStructure,
        $id: '/template/configuration/A.json',
        additionalProperties: false,
        properties: {
          ...genericConfigurationStructure.properties,
          spec: expected,
          translations: {
            ...genericConfigurationStructure.properties.translations,
            additionalProperties: expected,
          },
        },
      };

      // ACT
      const result = await generateSchema(
        'A',
        'libs/export/test/fixtures/fixture-0/components/extend/extend.schema.ts',
        __dirname,
      );
      // ASSERT

      expect(result).to.deep.equal(expectedSchema);
    });

    it('GIVEN - valid schema with additional properties - WHEN - calling generateSchema - THEN - it should return a schema with the additional properties', async () => {
      // ARRANGE
      const expected = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        additionalProperties: false,
        type: 'object',
        properties: {
          a: {
            type: 'object',
            additionalProperties: {
              additionalProperties: false,
              type: 'object',
              properties: {
                c: {
                  type: 'string',
                },
              },
            },
          },
          d: {
            type: 'number',
          },
        },
      };

      const expectedSchema = {
        ...genericConfigurationStructure,
        $id: '/template/configuration/AdditionalProperties.json',
        additionalProperties: false,
        properties: {
          ...genericConfigurationStructure.properties,
          spec: expected,
          translations: {
            ...genericConfigurationStructure.properties.translations,
            additionalProperties: expected,
          },
        },
      };

      // ACT
      const result = await generateSchema(
        'AdditionalProperties',
        'libs/export/test/fixtures/fixture-0/components/additional-properties/additional-properties.schema.ts',
        __dirname,
      );
      // ASSERT

      expect(result).to.deep.equal(expectedSchema);
    });

    it('GIVEN - a ts file with no contents - WHEN - calling generateSchema - THEN - it should throw an error', async () => {
      // ARRANGE
      // ACT
      // ASSERT
      try {
        const result = await generateSchema(
          'Invalid',
          'libs/export/test/fixtures/fixture-0/components/invalid/invalid.schema.ts',
          __dirname,
        );
        throw new Error('Expected this test to throw an error.');
      } catch (error) {
        expect(error).to.be.not.null;
      }
    });

    it('GIVEN - an empty configuration - WHEN - calling generateSchema - THEN - it will generate nothing.', async () => {
      // ARRANGE
      const expected = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        additionalProperties: false,
        type: 'object',
      };

      const expectedSchema = {
        ...genericConfigurationStructure,
        $id: '/template/configuration/Empty.json',
        properties: {
          ...genericConfigurationStructure.properties,
          spec: expected,
          translations: {
            ...genericConfigurationStructure.properties.translations,
            additionalProperties: expected,
          },
        },
      };
      // ACT
      const result = await generateSchema(
        'Empty',
        'libs/export/test/fixtures/fixture-0/components/empty/empty.schema.ts',
        __dirname,
      );
      // ASSERT

      expect(result).to.deep.equal(expectedSchema);
    });
  });

  describe('dereference', () => {
    /*
    Derefernced schemas should not retain any $refs properties.
    Any $refs in the original schema, should be resolved and dereferenced with the value from the definition
    */
    it('Given - a valid generated schema - When - dereferencing - Then - it should return a schema $refs resolved into the definitions ', () => {
      // Arrange
      const data = {
        type: 'object',
        properties: {
          a: {
            $ref: '#/definitions/SubSchema',
          },
        },
        definitions: {
          SubSchema: {
            type: 'object',
            properties: {
              c: {
                type: 'string',
              },
            },
          },
        },
        $schema: 'http://json-schema.org/draft-07/schema#',
      };

      const expected = {
        type: 'object',
        properties: {
          a: {
            type: 'object',
            properties: {
              c: {
                type: 'string',
              },
            },
          },
        },
        $schema: 'http://json-schema.org/draft-07/schema#',
      };

      // Act
      const result = dereferenceSchema(data, data);

      // Assert
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a valid schema with additional propertes - WHEN - resolving - THEN - it should return a resolved schema', () => {
      const data = {
        type: 'object',
        properties: {
          a: {
            additionalProperties: {
              $ref: '#/definitions/SubSchema',
            },
          },
        },
        definitions: {
          SubSchema: {
            type: 'object',
            properties: {
              c: {
                type: 'string',
              },
            },
          },
        },
        $schema: 'http://json-schema.org/draft-07/schema#',
      };

      // Expected
      const expected = {
        type: 'object',
        properties: {
          a: {
            additionalProperties: {
              type: 'object',
              properties: {
                c: {
                  type: 'string',
                },
              },
            },
          },
        },
        $schema: 'http://json-schema.org/draft-07/schema#',
      };

      // Act
      const result = dereferenceSchema(data, data);

      // Assert
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a valid schema parent level annotations - WHEN - dereferencing - THEN - it should return preserve parent level annotations', () => {
      // Arrange
      const data = {
        type: 'object',
        properties: {
          a: {
            $ref: '#/definitions/SubSchema',
            title: 'A',
            description: 'A description',
          },
        },
        definitions: {
          SubSchema: {
            title: 'Subschema Title',
            description: 'Subschema Description',
            type: 'object',
            properties: {
              c: {
                title: 'C',
                type: 'string',
              },
            },
          },
        },
        $schema: 'http://json-schema.org/draft-07/schema#',
      };

      // Expected
      const expected = {
        type: 'object',
        properties: {
          a: {
            title: 'A',
            description: 'A description',
            type: 'object',
            properties: {
              c: {
                title: 'C',
                type: 'string',
              },
            },
          },
        },
        $schema: 'http://json-schema.org/draft-07/schema#',
      };

      // Act
      const result = dereferenceSchema(data, data);

      // Assert
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - an invalid schema with circular dependency - WHEN - dereferencing - THEN - it should throw an error', () => {
      const data = {
        type: 'object',
        properties: {
          a: {
            $ref: '#/definitions/b',
          },
        },
        definitions: {
          b: {
            type: 'object',
            properties: {
              b1: {
                $ref: '#/definitions/c',
              },
            },
          },
          c: {
            type: 'object',
            properties: {
              c1: {
                $ref: '#/definitions/b',
              },
            },
          },
        },
        $schema: 'http://json-schema.org/draft-07/schema#',
      };

      // Act
      // Assert
      expect(() => dereferenceDefinitions(data)).to.throw('Circular dependency detected.');
    });
  });

  describe('setDefaults', () => {
    // defaults - spec - positive
    it('GIVEN - a schema with nested properties - WHEN - setting defaults from config - THEN - applies defaults to matching properties', () => {
      // Arrange
      const configuration: GenericConfiguration<any>[] = [
        {
          kind: 'Simple',
          spec: {
            a: 1,
            b: {
              b1: 2,
            },
            c: {
              c1: 'pre {\n  font-family: ‘Noto Sans Mono’, monospace;\n}',
            },
          },
        },
      ];

      const schema = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        properties: {
          a: {
            type: 'number',
          },
          b: {
            type: 'object',
            properties: {
              b1: {
                type: 'number',
              },
            },
          },
          c: {
            type: 'array',
            items: {
              type: 'object',
              properties: {
                c1: {
                  description: 'A custom scss string that is injected after the component styles',
                  type: 'string',
                },
              },
            },
          },
        },
      };

      const expected = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        properties: {
          a: {
            type: 'number',
            default: 1,
          },
          b: {
            type: 'object',
            properties: {
              b1: {
                default: 2,
                type: 'number',
              },
            },
          },
          c: {
            type: 'array',
            items: {
              type: 'object',
              properties: {
                c1: {
                  default: 'pre {\n  font-family: ‘Noto Sans Mono’, monospace;\n}',
                  description: 'A custom scss string that is injected after the component styles',
                  type: 'string',
                },
              },
            },
          },
        },
      };

      // ACT
      const result = setDefaultValues(schema, configuration[0].spec);

      // ASSERT
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a schema with string properties - WHEN - setting empty string defaults - THEN - accepts empty strings as valid defaults', () => {
      // Arrange
      const configuration: GenericConfiguration<any>[] = [
        {
          kind: 'Simple',
          spec: {
            a: '',
            b: {
              b1: '',
            },
            c: [''],
          },
        },
      ];

      const schema = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        properties: {
          a: {
            type: 'string',
          },
          b: {
            type: 'object',
            properties: {
              b1: {
                type: 'string',
              },
            },
          },
          c: {
            type: 'array',
            items: {
              type: 'object',
              properties: {
                c1: {
                  description: 'A custom scss string that is injected after the component styles',
                  type: 'string',
                },
              },
            },
          },
        },
      };

      const expected = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        properties: {
          a: {
            type: 'string',
            default: '',
          },
          b: {
            type: 'object',
            properties: {
              b1: {
                default: '',
                type: 'string',
              },
            },
          },
          c: {
            type: 'array',
            items: {
              type: 'object',
              properties: {
                c1: {
                  description: 'A custom scss string that is injected after the component styles',
                  type: 'string',
                },
              },
            },
            default: [''],
          },
        },
      };

      // ACT
      const result = setDefaultValues(schema, configuration[0].spec);

      // ASSERT
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a schema with nullable properties - WHEN - setting null defaults - THEN - accepts null as valid defaults', () => {
      // Arrange
      const configuration: GenericConfiguration<any>[] = readYAMLDocumentsFromFile(
        join(__dirname, '../../test/template/configuration-null.yml'),
      );

      console.log(configuration);

      const schema = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        properties: {
          a: {
            type: 'string',
          },
          b: {
            type: 'string',
          },
          c: {
            type: 'string',
          },
        },
      };

      const expected = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        properties: {
          a: {
            type: 'string',
            default: null,
          },
          b: {
            type: 'string',
            default: null,
          },
          c: {
            type: 'string',
            default: null,
          },
        },
      };

      // ACT
      const result = setDefaultValues(schema, configuration[0].spec);

      // ASSERT
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a schema with array of objects - WHEN - setting array defaults - THEN - applies defaults to array and its items', () => {
      // Arrange
      const schema = {
        type: 'object',
        properties: {
          items: {
            type: 'array',
            items: {
              type: 'object',
              properties: {
                name: {type: 'string'},
                value: {type: 'number'},
              },
            },
          },
        },
      };
      const values = {
        items: [
          {name: 'Item1', value: 10},
          {name: 'Item2', value: 20},
        ],
      };
      const expected = {
        type: 'object',
        properties: {
          items: {
            type: 'array',
            default: [
              {name: 'Item1', value: 10},
              {name: 'Item2', value: 20},
            ],
            items: {
              type: 'object',
              properties: {
                name: {type: 'string'},
                value: {type: 'number'},
              },
            },
          },
        },
      };

      // Act
      const result = setDefaultValues(schema, values);

      // Assert
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a schema with anyOf type union - WHEN - setting defaults - THEN - applies defaults while preserving union types', () => {
      // Arrange
      const schema = {
        type: 'object',
        properties: {
          value: {
            anyOf: [{type: 'string'}, {type: 'number'}],
          },
        },
      };
      const values = {
        value: 'default string',
      };
      const expected = {
        type: 'object',
        properties: {
          value: {
            anyOf: [{type: 'string'}, {type: 'number'}],
            default: 'default string',
          },
        },
      };

      // Act
      const result = setDefaultValues(schema, values);

      // Assert
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a schema with additionalProperties - WHEN - setting complex object defaults - THEN - inherits additional property structure', () => {
      const schema = {
        type: 'object',
        properties: {
          existingProp: {type: 'string'},
        },
        additionalProperties: {
          type: 'object',
          properties: {
            label: {type: 'string'},
            description: {type: 'string', default: 'Default description'},
          },
        },
      };

      const values = {
        existingProp: 'should be expected',
        map: {
          label: 'Map',
        },
      };

      const expected = {
        'type': 'object',
        'properties': {
          'existingProp': {
            'type': 'string',
            'default': 'should be expected',
          },
          'map': {
            'type': 'object',
            'properties': {
              'label': {
                'type': 'string',
                'default': 'Map',
              },
              'description': {
                'type': 'string',
                'default': 'Default description',
              },
            },
          },
        },
        'additionalProperties': {
          'type': 'object',
          'properties': {
            'label': {
              'type': 'string',
            },
            'description': {
              'type': 'string',
              'default': 'Default description',
            },
          },
        },
      };
      const result = setDefaultValues(schema, values);
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a schema with additionalProperties - WHEN - setting null - THEN - should preserve the original schema', () => {
      const schema = {
        type: 'object',
        properties: {
          existingProp: {type: 'string'},
        },
        additionalProperties: {
          type: 'object',
          properties: {
            label: {type: 'string'},
          },
        },
      };

      const values = {
        nullField: null,
      };

      const expected = {
        'type': 'object',
        'properties': {
          'existingProp': {
            'type': 'string',
          },
        },
        'additionalProperties': {
          'type': 'object',
          'properties': {
            'label': {
              'type': 'string',
            },
          },
        },
      };
      const result = setDefaultValues(schema, values);
      expect(result).to.deep.equal(expected);
    });

    // defaults - spec - positive
    it('GIVEN - a schema allowing additional string properties - WHEN - setting custom property defaults - THEN - adds properties with defaults', () => {
      // Arrange
      const configuration: GenericConfiguration<any> = {
        kind: 'Simple',
        spec: {
          a: 1,
          c: '123',
        },
      };

      const schema = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        properties: {
          a: {
            type: 'number',
          },
          b: {
            type: 'string',
          },
        },
        additionalProperties: {
          type: 'string',
        },
      };

      const expected = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        properties: {
          a: {
            type: 'number',
            default: 1,
          },
          b: {
            type: 'string',
          },
          c: {
            type: 'string',
            default: '123',
          },
        },
        additionalProperties: {
          type: 'string',
        },
      };

      // ACT
      const result = setDefaultValues(schema, configuration.spec);

      // ASSERT
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a schema with nested additionalProperties - WHEN - setting deep defaults - THEN - handles nested additional property inheritance', () => {
      // Arrange
      const configuration: GenericConfiguration<any> = {
        kind: 'Simple',
        spec: {
          a: 1,
          b: {
            b2: 'b2',
          },
        },
      };

      const schema = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        properties: {
          a: {
            type: 'number',
          },
          b: {
            type: 'object',
            properties: {
              b1: 'string',
            },
            additionalProperties: {
              type: 'string',
            },
          },
        },
      };

      const expected = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        properties: {
          a: {
            type: 'number',
            default: 1,
          },
          b: {
            type: 'object',
            properties: {
              b1: 'string',
              b2: {
                type: 'string',
                default: 'b2',
              },
            },
            additionalProperties: {
              type: 'string',
            },
          },
        },
      };

      // ACT
      const result = setDefaultValues(schema, configuration.spec);

      // ASSERT
      expect(result).to.deep.equal(expected);
    });

    // defaults - spec - negative
    it('GIVEN - a schema - WHEN - setting defaults for non-matching properties - THEN - preserves original schema unchanged', () => {
      // Arrange
      const configuration: GenericConfiguration<any> = {
        kind: 'Simple',
        spec: {
          b: 1,
        },
      };
      const schema = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        title: 'SubSchema Title',
        description: 'Subschema description',
        type: 'object',
        properties: {
          a: {
            title: 'A',
            type: 'number',
          },
        },
      };

      const expected = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        title: 'SubSchema Title',
        description: 'Subschema description',
        type: 'object',
        properties: {
          a: {
            title: 'A',
            type: 'number',
          },
        },
      };

      // ACT
      const result = setDefaultValues(schema, configuration.spec);

      // ASSERT
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a schema with additionalProperties false - WHEN - setting non-schema properties - THEN - ignores custom values (additional properties)', () => {
      const schema = {
        type: 'object',
        properties: {
          existingProp: {type: 'string'},
        },
        additionalProperties: false,
      };

      const values = {
        existingProp: 'should be expected',
        map: {
          label: 'Map',
        },
      };

      const expected = {
        'type': 'object',
        'properties': {
          'existingProp': {
            'type': 'string',
            'default': 'should be expected',
          },
        },
        'additionalProperties': false,
      };
      const result = setDefaultValues(schema, values);
      expect(result).to.deep.equal(expected);
    });

    // translations - positive
    it('GIVEN - a schema - WHEN - setting defaults from translations - THEN - applies translation values as defaults', () => {
      // Arrange
      const configuration: GenericConfiguration<any> = {
        kind: 'Simple',
        spec: {},
        translations: {
          'de-DE': {
            a: 10,
            b: {
              b1: 20,
            },
            c: {
              c1: 'Custom Translated Text',
            },
          },
        },
      };

      const schema = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        properties: {
          a: {
            type: 'number',
          },
          b: {
            type: 'object',
            properties: {
              b1: {
                type: 'number',
              },
            },
          },
          c: {
            type: 'array',
            items: {
              type: 'object',
              properties: {
                c1: {
                  description: 'A custom scss string that is injected after the component styles',
                  type: 'string',
                },
              },
            },
          },
        },
      };

      const expected = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        properties: {
          a: {
            type: 'number',
            default: 10,
          },
          b: {
            type: 'object',
            properties: {
              b1: {
                default: 20,
                type: 'number',
              },
            },
          },
          c: {
            type: 'array',
            items: {
              type: 'object',
              properties: {
                c1: {
                  default: 'Custom Translated Text',
                  description: 'A custom scss string that is injected after the component styles',
                  type: 'string',
                },
              },
            },
          },
        },
      };

      // ACT
      const result = setDefaultValues(schema, configuration?.translations?.['de-DE']);

      // ASSERT
      expect(result).to.deep.equal(expected);
    });

    // translations - negative
    it('GIVEN - a schema - WHEN - setting unrelated translation defaults - THEN - preserves schema unchanged', () => {
      // Arrange
      const configuration: GenericConfiguration<any> = {
        kind: 'Simple',
        spec: {},
        translations: {
          'de-DE': {
            d: 10,
          },
        },
      };

      const schema = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        properties: {
          a: {
            type: 'number',
          },
        },
      };

      const expected = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        properties: {
          a: {
            type: 'number',
          },
        },
      };

      // ACT
      const result = setDefaultValues(schema, configuration?.translations?.['de-DE']);

      // ASSERT
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a schema - WHEN - setting both spec and translation defaults - THEN - applies both sequentially', () => {
      // Arrange
      const configuration: GenericConfiguration<any> = {
        kind: 'Simple',
        spec: {
          a: 1,
        },
        translations: {
          'de-DE': {
            b: {
              b1: 20,
            },
          },
        },
      };

      const schema = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        properties: {
          a: {
            type: 'number',
          },
          b: {
            type: 'object',
            properties: {
              b1: {
                type: 'number',
              },
            },
          },
        },
      };

      const expected = {
        $schema: 'http://json-schema.org/draft-07/schema#',
        type: 'object',
        properties: {
          a: {
            type: 'number',
            default: 1,
          },
          b: {
            type: 'object',
            properties: {
              b1: {
                default: 20,
                type: 'number',
              },
            },
          },
        },
      };

      // ACT
      const firstResult = setDefaultValues(schema, configuration?.spec);

      const secondResult = setDefaultValues(firstResult, configuration?.translations?.['de-DE']);

      // ASSERT
      expect(secondResult).to.deep.equal(expected);
    });
  });

  describe('generateTemplateSchema', () => {
    const componentPath: string = join(__dirname, '..', '..', 'test', 'fixtures', 'fixture-0');

    it('GIVEN - a valid template configuration - WHEN - generating - THEN - it should return the metadata', async () => {
      // ARRANGE
      const data = [
        {
          kind: 'Configuration',
          spec: {
            slug: 'sciflow-thesis2',
            title: 'Thesis template (v2)',
          },
        },
        {
          kind: 'Test-Page',
          metadata: {
            name: 'PdfPage',
          },
          spec: {
            page: {
              size: 'A5',
            },
            typesetting: {
              lineHeight: '1.6',
            },
          },
        },
      ];

      const expected = {
        type: 'object',
        title: 'Template configuration',
        properties: {
          'Test-Page-PdfPage': {
            title: 'PDF Setup',
            description: 'Set up page margins, fonts and page sizes.',
            type: 'object',
            properties: {
              page: {
                type: 'object',
                properties: {
                  size: {
                    title: 'Size',
                    description: 'The page size.',
                    examples: ['A4', 'A5', '17cm 24cm'],
                    default: 'A5',
                    type: 'string',
                  },
                },
              },
              placements: {
                title: 'The available document placements (cover, front, body, back)',
                default: ['body'],
                type: 'array',
                items: {
                  type: 'string',
                },
              },
              typesetting: {
                type: 'object',
                properties: {
                  lineHeight: {
                    title: 'Line height',
                    default: '1.6',
                    type: 'string',
                  },
                },
                title: 'Typesetting',
                description: 'General options for typesetting.',
              },
            },
            component: {
              kind: 'Test-Page',
              metadata: {
                name: 'PdfPage',
              },
            },
            $schema: 'http://json-schema.org/draft-07/schema#',
            additionalProperties: false,
            $id: '/template/schema/sciflow-thesis2-test-page-pdfpage',
          },
        },
      };

      // ACT
      try {
        const {schema: result, configurations: _configurations} = await buildTemplateSchema(
          data,
          '',
          [],
          componentPath,
        );
        removeNestedNullUndefined(result); // removes nested

        // ASSERT
        expect(result).to.deep.equal(expected);
      } catch (error) {
        console.error('Error generating template schema:', error);
        throw error;
      }
    });

    it('GIVEN - a valid template with a variant with "conf" patches - WHEN - generating - THEN - it should return the metadata', async () => {
      // ARRANGE
      const templateConfigurations = [
        {
          kind: 'Test-Page',
          metadata: {
            name: 'PdfPage',
          },
          spec: {
            placements: ['cover'],
          },
        },
        {
          kind: 'Variant',
          metadata: {
            name: 'report',
          },
          spec: {
            patches: [
              {
                type: 'conf',
                target: {kind: 'Test-Page'},
                patch: [{'op': 'add', 'path': '/spec/placements', 'value': ['front', 'back']}],
              },
            ],
          },
        },
      ];

      const expected = {
        type: 'object',
        title: 'Template configuration (report)',
        properties: {
          'Test-Page-PdfPage': {
            title: 'PDF Setup',
            description: 'Set up page margins, fonts and page sizes.',
            type: 'object',
            properties: {
              page: {
                type: 'object',
                properties: {
                  size: {
                    title: 'Size',
                    type: 'string',
                    description: 'The page size.',
                    examples: ['A4', 'A5', '17cm 24cm'],
                    default: 'A4',
                  },
                },
              },
              placements: {
                title: 'The available document placements (cover, front, body, back)',
                default: ['front', 'back'],
                type: 'array',
                items: {
                  type: 'string',
                },
              },
              typesetting: {
                title: 'Typesetting',
                description: 'General options for typesetting.',
                type: 'object',
                properties: {
                  lineHeight: {
                    title: 'Line height',
                    default: 1.2,
                    type: 'string',
                  },
                },
              },
            },
            component: {
              kind: 'Test-Page',
              metadata: {
                name: 'PdfPage',
              },
            },
            $schema: 'http://json-schema.org/draft-07/schema#',
            additionalProperties: false,
            $id: '/template/schema/none-test-page-pdfpage',
          },
        },
      };

      // ACT
      const {schema: result, configurations: _configurations} = await buildTemplateSchema(
        templateConfigurations,
        '',
        ['report'],
        componentPath,
      );
      removeNestedNullUndefined(result); // removes nested

      // ASSERT
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a valid template with a variant with "default" patches - WHEN - generating - THEN - it should return the metadata', async () => {
      // ARRANGE
      const templateConfigurations = [
        {
          kind: 'Test-Page',
          metadata: {
            name: 'PdfPage',
          },
          spec: {
            placements: ['cover'],
          },
        },
        {
          kind: 'Variant',
          metadata: {
            name: 'report',
          },
          spec: {
            patches: [
              {
                type: 'default',
                target: {kind: 'Test-Page'},
                patch: [
                  {'op': 'remove', 'path': '/properties/placements'},
                  {'op': 'remove', 'path': '/properties/typesetting'},
                ],
              },
            ],
          },
        },
      ];

      const expected = {
        type: 'object',
        title: 'Template configuration (report)',
        properties: {
          'Test-Page-PdfPage': {
            title: 'PDF Setup',
            description: 'Set up page margins, fonts and page sizes.',
            type: 'object',
            properties: {
              page: {
                type: 'object',
                properties: {
                  size: {
                    title: 'Size',
                    type: 'string',
                    description: 'The page size.',
                    examples: ['A4', 'A5', '17cm 24cm'],
                    default: 'A4',
                  },
                },
              },
            },
            component: {
              kind: 'Test-Page',
              metadata: {
                name: 'PdfPage',
              },
            },
            $schema: 'http://json-schema.org/draft-07/schema#',
            additionalProperties: false,
            $id: '/template/schema/none-test-page-pdfpage',
          },
        },
      };

      // ACT
      const {schema: result, configurations: _configurations} = await buildTemplateSchema(
        templateConfigurations,
        '',
        ['report'],
        componentPath,
      );
      removeNestedNullUndefined(result); // removes nested

      // ASSERT
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a valid template with a variant with "conf" (add object) and "default" (add to array) patches - WHEN - generating - THEN - it should return the metadata', async () => {
      // ARRANGE
      const templateConfigurations = [
        {
          kind: 'Test-Page',
          metadata: {
            name: 'PdfPage',
          },
          spec: {
            placements: ['cover'],
          },
        },
        {
          kind: 'Variant',
          metadata: {
            name: 'report',
          },
          spec: {
            patches: [
              {
                type: 'conf',
                target: {kind: 'Test-Page'},
                patch: [{'op': 'add', 'path': '/spec/placements', 'value': ['front', 'back']}],
              },
              {
                type: 'default',
                target: {kind: 'Test-Page'},
                patch: [{'op': 'add', 'path': '/properties/placements/default/-', 'value': 'body'}],
              },
            ],
          },
        },
      ];

      const expected = {
        type: 'object',
        title: 'Template configuration (report)',
        properties: {
          'Test-Page-PdfPage': {
            title: 'PDF Setup',
            description: 'Set up page margins, fonts and page sizes.',
            type: 'object',
            properties: {
              page: {
                type: 'object',
                properties: {
                  size: {
                    title: 'Size',
                    type: 'string',
                    description: 'The page size.',
                    examples: ['A4', 'A5', '17cm 24cm'],
                    default: 'A4',
                  },
                },
              },
              placements: {
                title: 'The available document placements (cover, front, body, back)',
                default: ['front', 'back', 'body'],
                type: 'array',
                items: {
                  type: 'string',
                },
              },
              typesetting: {
                title: 'Typesetting',
                description: 'General options for typesetting.',
                type: 'object',
                properties: {
                  lineHeight: {
                    title: 'Line height',
                    default: 1.2,
                    type: 'string',
                  },
                },
              },
            },
            component: {
              kind: 'Test-Page',
              metadata: {
                name: 'PdfPage',
              },
            },
            $schema: 'http://json-schema.org/draft-07/schema#',
            additionalProperties: false,
            $id: '/template/schema/none-test-page-pdfpage',
          },
        },
      };

      // ACT
      const {schema: result, configurations: _configurations} = await buildTemplateSchema(
        templateConfigurations,
        '',
        ['report'],
        componentPath,
      );
      removeNestedNullUndefined(result); // removes nested

      // ASSERT
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a valid template with a variant with "conf" (replace object) and "default" (add to array) patches - WHEN - generating - THEN - it should throw an error', async () => {
      // ARRANGE
      const templateConfigurations = [
        {
          kind: 'Test-Page',
          metadata: {
            name: 'PdfPage',
          },
          spec: {
            placements: ['cover'],
          },
        },
        {
          kind: 'Variant',
          metadata: {
            name: 'report',
          },
          spec: {
            patches: [
              {
                type: 'conf',
                target: {kind: 'Test-Page'},
                patch: [{'op': 'replace', 'path': '/spec/placements', 'value': 'test'}],
              },
              {
                type: 'default',
                target: {kind: 'Test-Page'},
                patch: [{'op': 'add', 'path': '/properties/placements/default/-', 'value': 'body'}],
              },
            ],
          },
        },
      ];

      // ACT
      // ASSERT
      try {
        await buildTemplateSchema(templateConfigurations, '', ['report'], componentPath);
      } catch (error) {
        expect(error).to.be.not.null;
      }
    });

    it('GIVEN - a template config and invalid schema - WHEN - generating - THEN - it should throw an error', async () => {
      // ARRANGE
      const data = [
        {
          kind: 'invalid',
          spec: {},
        },
      ];

      // ACT
      // ASSERT
      try {
        await buildTemplateSchema(data, undefined, undefined, componentPath);
      } catch (error) {
        expect(error).to.be.an('Error');
      }
    });

    it('GIVEN - a template configuration with translations and spec - WHEN - buildTemplateSchema - THEN - it should set the value from the config as a default value to the schema', async () => {
      // ARRANGE
      const templateConfigurations = [
        {
          kind: 'Test-Star-Trek',
          metadata: {
            name: 'Test-Star-Trek',
          },
          spec: {
            shipName: 'Enterprise',
          },
          translations: {
            'tlh-Latn': {
              shipName: "tu'",
            },
            'tlh-Piqd': {
              shipName: 'tI',
            },
          },
        },
      ];

      const expected = {
        type: 'object',
        title: 'Template configuration',
        properties: {
          'Test-Star-Trek': {
            title: 'Star Trek',
            description: 'Star Trek Ship Name',
            type: 'object',
            properties: {
              shipName: {
                type: 'string',
                default: "tu'",
              },
            },
            component: {
              kind: 'Test-Star-Trek',
              metadata: {
                name: 'Test-Star-Trek',
              },
            },
            $schema: 'http://json-schema.org/draft-07/schema#',
            additionalProperties: false,
            $id: '/template/schema/none-test-star-trek',
          },
        },
      };

      // ACT
      const {schema: result, configurations: _configurations} = await buildTemplateSchema(
        templateConfigurations,
        'tlh-Latn',
        [],
        componentPath,
      );
      removeNestedNullUndefined(result); // removes nested

      // ASSERT
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a template configuration with translations and no spec - WHEN - buildTemplateSchema - THEN - it should set the value from the config as a default value to the schema', async () => {
      // ARRANGE
      const templateConfigurations = [
        {
          kind: 'Test-Star-Trek',
          metadata: {
            name: 'Test-Star-Trek',
          },
          spec: {},
          translations: {
            'tlh-Latn': {
              shipName: "tu'",
            },
            'tlh-Piqd': {
              shipName: 'tI',
            },
          },
        },
      ];

      const expected = {
        type: 'object',
        title: 'Template configuration',
        properties: {
          'Test-Star-Trek': {
            title: 'Star Trek',
            description: 'Star Trek Ship Name',
            type: 'object',
            properties: {
              shipName: {
                type: 'string',
                default: "tu'",
              },
            },
            component: {
              kind: 'Test-Star-Trek',
              metadata: {
                name: 'Test-Star-Trek',
              },
            },
            $schema: 'http://json-schema.org/draft-07/schema#',
            additionalProperties: false,
            $id: '/template/schema/none-test-star-trek',
          },
        },
      };

      // ACT
      const {schema: result, configurations: _configurations} = await buildTemplateSchema(
        templateConfigurations,
        'tlh-Latn',
        [],
        componentPath,
      );
      removeNestedNullUndefined(result); // removes nested

      // ASSERT
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - an empty template configuration - WHEN - generating - THEN - it should empty for the properties', async () => {
      // ARRANGE
      const data = [];
      const expected = {
        type: 'object',
        title: 'Template configuration',
        properties: {},
      };

      // ACT
      const {schema: result, configurations: _configurations} = await buildTemplateSchema(data);
      removeNestedNullUndefined(result); // removes nested

      // ASSERT
      expect(result).to.deep.equal(expected);
    });
  });

  describe('replaceUnionTypes', () => {
    it('GIVEN - a schema with a union type ["string", "number"] - WHEN - processed by replaceUnionTypes - THEN - it should convert it to an anyOf structure', () => {
      // ARRANGE
      const schema = {
        type: 'object',
        properties: {
          fontWeight: {
            type: ['string', 'number'],
          },
          fontSize: {
            type: 'string',
          },
        },
      };

      const expected = {
        type: 'object',
        properties: {
          fontWeight: {
            anyOf: [{type: 'string'}, {type: 'number'}],
          },
          fontSize: {
            type: 'string',
          },
        },
      };

      // ACT
      const result = replaceUnionTypes(schema);

      // ASSERT
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a schema with no union type - WHEN - processed by replaceUnionTypes - THEN - it should remain unchanged', () => {
      // ARRANGE
      const schema = {
        type: 'object',
        properties: {
          fontWeight: {
            type: 'string',
          },
          fontSize: {
            type: 'string',
          },
        },
      };

      const expected = {
        type: 'object',
        properties: {
          fontWeight: {
            type: 'string',
          },
          fontSize: {
            type: 'string',
          },
        },
      };

      // ACT
      const result = replaceUnionTypes(schema);

      // ASSERT
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a schema with multiple properties including a union type ["string", "number"] - WHEN - processed by replaceUnionTypes - THEN - only the union type should be replaced', () => {
      // ARRANGE
      const schema = {
        type: 'object',
        properties: {
          fontWeight: {
            type: ['string', 'number'],
          },
          isBold: {
            type: 'boolean',
          },
          fontSize: {
            type: 'string',
          },
        },
      };

      const expected = {
        type: 'object',
        properties: {
          fontWeight: {
            anyOf: [{type: 'string'}, {type: 'number'}],
          },
          isBold: {
            type: 'boolean',
          },
          fontSize: {
            type: 'string',
          },
        },
      };

      // ACT
      const result = replaceUnionTypes(schema);

      // ASSERT
      expect(result).to.deep.equal(expected);
    });

    it('GIVEN - a schema with nested objects containing a union type ["string", "number"] - WHEN - processed by replaceUnionTypes - THEN - the nested union type should be replaced', () => {
      // ARRANGE
      const schema = {
        type: 'object',
        properties: {
          typography: {
            type: 'object',
            properties: {
              fontWeight: {
                type: ['string', 'number'],
              },
              fontSize: {
                type: 'string',
              },
            },
          },
        },
      };

      const expected = {
        type: 'object',
        properties: {
          typography: {
            type: 'object',
            properties: {
              fontWeight: {
                anyOf: [{type: 'string'}, {type: 'number'}],
              },
              fontSize: {
                type: 'string',
              },
            },
          },
        },
      };

      // ACT
      const result = replaceUnionTypes(schema);

      // ASSERT
      expect(result).to.deep.equal(expected);
    });
  });
});
