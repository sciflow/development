
import { GenericConfiguration, SciFlowDocumentData } from "../../interfaces";
import { ALL_PAGES, PartForRendering, TemplateComponent, TemplateComponentOptions } from '../../component';
import { RenderingEngine } from "../../renderers";

/**
 * Creates factories from a list of components (duplicates without a specific name are overwritten, last one wins)
 */
export const createFactories = (templateComponents, engine: RenderingEngine<any, any>): { name: string; factory: (configuration?: GenericConfiguration<any>, options?: TemplateComponentOptions) => TemplateComponent<any, SciFlowDocumentData> }[] => {
    // components can be used to inject DOM into the document (e.g. for the table of contents)
    // or just render CSS (e.g. to style the @page)
    return [
        ...templateComponents // reverse will change the array
    ]
        .reverse()
        .filter(r => typeof r === 'function')
        .map((r: any) => ({
            name: r.name,
            factory: (configuration?: GenericConfiguration<any>, options?: TemplateComponentOptions) => typeof r?.prototype?.constructor !== 'function' ? r(engine, configuration, options) : new r(engine, configuration, options)
        }));
}

/**
 * Go through the template configuration values and create the styles and additional DOM elements
 */
export const createInstances = async (configurations, componentFactories, { runner, componentPaths, assetBasePaths, logger }): Promise<{ instances: PartForRendering[]; styles: any[] }> => {
    const styles: any[] = [];
    const instances: PartForRendering[] = [];
    if (configurations) {
        for (let configuration of configurations) {
            const instance = componentFactories
                .find(templateComponent => templateComponent.name === configuration.kind);
            let rendererInstance: TemplateComponent<any, SciFlowDocumentData> | undefined = instance?.factory(configuration);
            if (!rendererInstance) { continue; }
            try {
                const style = rendererInstance.renderStyles({ runner, assetBasePaths, componentPaths: componentPaths || [] });
                if (style) {
                    styles.push({
                        kind: rendererInstance?.kind,
                        css: style
                    });
                }
            } catch (e: any) {
                logger.error('Could not render component styles for: ' + configuration?.kind, { message: e.message, configuration });
                debugger;
            }

            try {
                const instance = await rendererInstance.renderDOM({ runner, componentPaths: componentPaths || [] });
                if (instance) {
                    instances.push({
                        configuration,
                        ...instance
                    });
                }
            } catch (e: any) {
                logger.error('Could not render component dom for ' + configuration?.kind, { message: e.message, configuration });
                debugger;
            }
        }
    }

    return {
        instances,
        styles
    }
}