import { TSX } from '../../TSX';

import { DocumentNode, SFNodeType } from '@sciflow/schema';
import { getDefaultEnvironments, labelToEnvironment } from '..';
import { SciFlowDocumentData } from "../../interfaces";
import { NodeRenderer } from "../../node-renderer";
import { Environment } from '../components/environment/environment.component';
import { FigureRendererConfiguration, TableEnvironment } from './figure/figure.schema';

class TableRenderer extends NodeRenderer<HTMLTableElement, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.table>, parent?: DocumentNode<SFNodeType.figure>) {
        if (!node.content) {
            return <table></table>;
        }

        const figureComponent = new Environment(this.engine as any);
        const configuration: FigureRendererConfiguration = figureComponent.getValues();
        const renderCaptionsInline = configuration.renderCaptionsInline;

        const colcount = node.content.length > 0 && node.content[0].content && node.content[0].content.length || 0;

        const indexOfFirstRow = node.content.findIndex(row => row.content?.some(n => n.type !== SFNodeType.table_header));
        const headers = node.content.slice(0, indexOfFirstRow);
        const rows = node.content.slice(indexOfFirstRow, node.content.length);
        const headersRendered = await this.engine.renderContent({ ...node, content: headers });
        const rowsRendered = await this.engine.renderContent({ ...node, content: rows });

        const captionNode = parent?.content?.find(node => node?.type === SFNodeType.caption);
        const labelNode = captionNode?.content?.find(c => c.type === SFNodeType.label);
        let captionText: HTMLElement[] = [];
        let noteText: HTMLElement[] = [];
        let environments = configuration?.environments || getDefaultEnvironments();
        let environment = environments.table;
        const captionContent = captionNode?.content?.filter(c => c.type !== SFNodeType.label);
        // notes are rendered in the wrapper const noteContent = captionContent?.slice(1);
        let envName = 'table';
        if (captionContent && captionContent?.length > 0) {
            const titleNode = captionContent[0];
            if (titleNode) {
                if (labelNode && titleNode?.content) {
                    // remove the label from the caption
                    const env = labelNode ? labelToEnvironment(labelNode, configuration) : undefined;
                    if (env) {
                        environment = configuration?.environments[env] as TableEnvironment;
                        envName = env;
                    }
                }
                if (labelNode && titleNode?.content) {
                    // remove the label from the caption
                    titleNode.content = titleNode.content.filter(c => c.type !== SFNodeType.label);
                }
                captionText = await this.engine.renderContent(titleNode);
            }
        }

        // see figure renderer: We do not render captions for centered tables (because of caption width issues)
        // we render figcaptions in the parent figure instead
        const renderTableCaptionAsFigCaption = environment?.align?.content === 'center';

        // only get tables that have a caption
        if (!this.engine.data.document.tables) { throw new Error('Tables must be generated at this point'); }
        const tables = this.engine.data.document.tables?.labels[envName].filter(table => table?.title?.length != undefined && table?.title?.length > 0 || environment?.countWithoutCaption);
        const index = tables?.findIndex(t => t.nodeId === parent?.attrs?.id || t.nodeId === node?.attrs?.id);
        const itemFromListing = this.engine.data.document?.tables?.labels?.[envName]?.find(i => i.id === node.attrs.id || i.id === parent?.attrs?.id);

        const label = itemFromListing?.customLabel || environment.label;
        const caption = <span class="caption">{...captionText}</span>;
        if (itemFromListing?.customNumber) {
            // replace label and numbering the user provided
            caption.innerHTML = caption.innerHTML.replace(`${label} ${itemFromListing?.customNumber}${environment.labelSuffix} `, '');
        }

        const captionSide = environment.captionSide || 'above';
        const table = <table id={'tbl-' + node?.attrs?.id}>
            {!renderTableCaptionAsFigCaption && (index > -1 && captionText?.length > 0) ? <caption data-caption-side={captionSide} data-table-number={itemFromListing?.customNumber || (index > -1 ? index + 1 : null)}>
                <span class="caption-body">
                    {renderCaptionsInline ? <span class="label">
                        {label}&nbsp;{itemFromListing?.customNumber || (index + 1)}
                        {environment.labelSuffix}&nbsp;
                        {environment.breakAfterLabel ? <br /> : ''}
                    </span> : ''}
                    {caption}
                </span>
            </caption> : ''}
            {headers.length > 0 ? <thead>
                {...headersRendered}
            </thead> : ''}
            <tbody>
                {...rowsRendered}
            </tbody>
        </table> as unknown as HTMLTableElement;

        if (parent?.attrs?.['scale-width'] && this.engine.runner !== 'xml') {
            table.setAttribute('data-scale-width', parent?.attrs?.['scale-width']);
        }

        if (parent?.attrs?.['orientation'] && this.engine.runner !== 'xml') {
        table.setAttribute('data-orientation', parent?.attrs?.['orientation']);
        }

        return table;
    }
}

class TableCellRenderer extends NodeRenderer<HTMLTableCellElement, SciFlowDocumentData> {
    async render(
        node: DocumentNode<SFNodeType.table_cell | SFNodeType.table_header>,
        parent: DocumentNode<any>
    ) {
        const children = await this.engine.renderContent(node);
        const align = node.content?.find(({ attrs }) => attrs && attrs['text-align'])?.attrs['text-align'];
        let htmlEl = <td>{...children}</td> as unknown as HTMLTableCellElement;
        if (node.type === SFNodeType.table_header) {
            htmlEl = <th>{...children}</th> as unknown as HTMLTableCellElement;
            // htmlEl.setAttribute('custom-style', 'Table Heading');

            const isHeaderRow = parent?.content?.every((c) => c.type === SFNodeType.table_header);
            if (isHeaderRow) {
              const scope =
                node.attrs?.rowspan > 1 ? 'rowgroup' : node.attrs?.colspan > 1 ? 'colgroup' : 'col';
                htmlEl.setAttribute('scope', scope);
            } else {
                htmlEl.setAttribute('scope', node.attrs?.rowspan > 1 ? 'rowgroup' : 'row');
            }

        }
        if (align) {
            htmlEl.setAttribute('style', `text-align: ${align}`);
        }

        if (node.attrs && node.attrs.colwidth) {
            htmlEl.setAttribute('width', node.attrs.colwidth);
        }

        if (node.attrs && node.attrs.colspan) {
            htmlEl.setAttribute('colspan', node.attrs.colspan);
        }

        if (node.attrs && node.attrs.rowspan) {
            htmlEl.setAttribute('rowspan', node.attrs.rowspan);
        }

        return htmlEl;
    }
}

class TableRowRenderer extends NodeRenderer<HTMLTableRowElement, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.table_row>, _parent?: DocumentNode<any>) {
        const children = await this.engine.renderContent(node);

        const tr = <tr>
            {...children}
        </tr>;

        if (node.attrs?.breakAfter?.length > 0) {
            tr.setAttribute('data-break-after', node.attrs.breakAfter);
        }

        return tr;
    }
}

const renderers: { [key: string]: any } = {
    [SFNodeType.table]: TableRenderer,
    [SFNodeType.table_row]: TableRowRenderer,
    [SFNodeType.table_cell]: TableCellRenderer,
    [SFNodeType.table_header]: TableCellRenderer
};

export default renderers;