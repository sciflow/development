export interface FootnoteRendererConfiguration {
    /**
     * @title Mode
     * @default footnote
     * @description Where to display footnotes.
     */
    mode?: 'footnote' | 'endnote';
}