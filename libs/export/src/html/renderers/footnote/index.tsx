import { TSX } from '../../../TSX';

import { SciFlowDocumentData } from "../../../interfaces";
import { SFNodeType, DocumentNode } from '@sciflow/schema';
import { NodeRenderer } from "../../../node-renderer";
import { Footnote } from '../../components/footnote/footnote.component';
import { FootnoteRendererConfiguration } from './footnote.schema';

const renderers: { [key: string]: any } = {
    [SFNodeType.footnote]: class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.footnote>) {
            const footnoteComponent = new Footnote(this.engine);
            const configuration: FootnoteRendererConfiguration = footnoteComponent.getValues();

            // to get the right numbering we need to include citations if they are in a footnote style
            const citationsInFootnotes = this.engine.data.citationRenderer.footnotes === true;
            if (!this.engine.data.document.tables) { throw new Error('Tables must be generated at this point'); }
            const footnotes = citationsInFootnotes ?
                this.engine.data.document.tables.footnotesAndCitations
                : this.engine.data.document.tables.footnotes;
            const index = footnotes?.findIndex((footnote) => footnote.attrs?.id === node.attrs?.id);

            // endnote templates are responsible to render the fn elements somewhere else in the document.
            if (configuration?.mode === 'endnote') {
                const endnotes = footnoteComponent.getEndnoteItems();
                const endnoteIndex = endnotes.findIndex(endnote => endnote.id === node.attrs?.id);
                const endnoteId = node?.attrs?.id ?? (endnoteIndex + 1);
                const numberWidth = `${index + 1}`?.length;
                return <a data-type="endnote-call" data-number-width={numberWidth} id={'enref' + endnoteId} href={'#en' + endnoteId} target="_self">
                    {endnoteIndex + 1}
                </a>;
            }

            const body = <span data-type="footnote" href={'#fn' + (index + 1)} id={'fn' + (index + 1)}>
                {...(await this.engine.renderContent(node))}
            </span>;;
            if (node?.attrs?.lang) { body.setAttribute('lang', node.attrs.lang); }

            return body;
        }
    }
};

export default renderers;
