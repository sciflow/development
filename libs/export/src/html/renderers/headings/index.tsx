import { TSX } from '../../../TSX';

import { DocumentNode } from '@sciflow/schema';
import { GenericConfiguration, SciFlowDocumentData } from "../../../interfaces";
import { NodeRenderer } from "../../../node-renderer";
import { Typography } from '../../components/typography/typography.component';
import { TypographyConfiguration } from '../../components/typography/typography.schema';
import { GoogleFontConfiguration } from '../../components/google-font/google-font.schema';
import { GoogleFont } from '../../components/google-font/google-font.component';
import { FontConfiguration } from '../../components/font/font.schema';
import { Font } from '../../components/font/font.component';

export class HeadingRenderer extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
    async render(node: DocumentNode<any>) {

        // we are importing the default implementation for a figure component here. Be aware that this might be different,
        // for some templates. So we cannot rely on the shape of the data provided.
        const headingComponent = new Typography(this.engine);
        if (!node.attrs) { node.attrs = {}; }
        const configuration: TypographyConfiguration = headingComponent.getValues();
        // TODO we should probably read the heading font configuration and use the correct one based on that
        let fontConfiguration: GenericConfiguration<FontConfiguration | GoogleFontConfiguration> = this.engine.data.configurations?.find(c => c.kind === Font.name);
        if (!fontConfiguration) {
            // fall back to google fonts but use the primary font for measurements
            fontConfiguration = this.engine.data.configurations?.find(c => c.kind === GoogleFont.name);
        }

        const heading = this.engine.data.document.tables?.toc.headings.find(h => h.id === node.attrs?.id);
        let content = await this.engine.renderContent(node);
        if (content.length == 0) { return <span data-role="empty-heading" />; }

        const htmlEl = this.engine.data.virtualDocument.createElement(`h${node.attrs?.level ?? 1}`) as HTMLHeadingElement;

        let titlePaddingRem;
        const alignHeadingsBy = headingComponent.get('alignHeadingsBy') || 'heading2';
        if (headingComponent.get('alignHeadings') === true) {
            const remWidth = Number.parseFloat(((fontConfiguration?.spec?.calculated?.[alignHeadingsBy] || '0.6rem').replace('rem', '')));
            const maxNumberingLength = Math.max(...(this.engine.data.tableOfContents?.headings || []).map(h => h.numberingString?.length || 0));
            titlePaddingRem = (remWidth * (Math.max(4, maxNumberingLength) + 1));
        }

        if (heading?.numberingString) {
            // TODO this might not reflect the actual numbering string since not taken from the listings
            node.attrs['data-numbering'] = node.attrs.numbering;
            delete node.attrs.numbering;
        }

        const h1NumberingSuffix = configuration.h1?.numbering?.suffix?.length > 0 ? TSX.createTextNode(configuration.h1?.numbering?.suffix) : TSX.createTextNode('');
        if (configuration.numberingInline && heading?.numberingString?.length > 0) {
            content = [
                <span class="numbering-string">{heading?.numberingString}{heading?.level === 1 ? h1NumberingSuffix : ''}&nbsp;</span>,
                <span class="heading-title" style={titlePaddingRem ? `padding-left: ${titlePaddingRem}rem;` : ''}>{...content}</span>
            ];
        }

        const span = <span class="heading">{...content}</span>

        if (heading?.numberingString?.length > 0 && !configuration.numberingInline) {
            span.setAttribute('data-numbering', heading?.numberingString);
        }

        for (let att in node.attrs) {
            if (att === 'level') {
                span.setAttribute('data-' + att, node.attrs[att]);
            } else {
                span.setAttribute(att, node.attrs[att]);
            }
        }

        if (heading && heading?.bookmarkLevel !== heading?.level) {
            htmlEl.setAttribute('data-bookmark-level', heading.bookmarkLevel);
        }

        htmlEl.append(span);
        return htmlEl;
    }
}
