/**
 * Heading renderer schema with the basic shape of configurations handed to the heading renderer.
 */


/**
* @title General heading settings
*/
export interface TypographyRendererConfiguration {
    /**
     * @title Render numbering inside
     * @description Whether heading numbers should rendered inside instead of as a before element (needed for accessibility in HTML outputs)
     * @default false
     */
    numberingInline: boolean;
    // no default value
    /**
     * @title Default chapter numbering
     */
    defaultNumberingStyle?: 'none' | 'decimal' | 'lower-roman' | 'upper-roman' | 'lower-alpha' | 'upper-alpha';
    /**
     * @title Align all headings based on the longest label
     * @default false
     */
    alignHeadings: boolean;
    /**
     * @title The heading font size to use for clculation of the ewidth
     * @default heading3
     */
    alignHeadingsBy: string;
}