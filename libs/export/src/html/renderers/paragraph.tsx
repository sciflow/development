import { TSX } from '../../TSX';

import { DocumentNode, SFMarkType, SFNodeType } from '@sciflow/schema';
import { SciFlowDocumentData } from "../../interfaces";
import { NodeRenderer } from "../../node-renderer";
import { mergeTextNodes as renderTextNodes } from './text';

class ParagraphRenderer1 extends NodeRenderer<HTMLParagraphElement, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.paragraph>, parent: DocumentNode<SFNodeType>) {
        const htmlEl: HTMLParagraphElement = await (<p />);
        try {
            if (node.attrs) {
                for (let att in node.attrs) {
                    if (node.attrs[att] !== null) {
                        htmlEl.setAttribute(att, node.attrs[att]);
                    }
                }
            }

            if (node.text) { htmlEl.textContent = node.text; }
            if (node.content && node.content?.length > 0) {
                for (let children of await this.engine.renderContent(node)) {
                    try {
                        if (Array.isArray(children)) {
                            for (let child of children) {
                                htmlEl.appendChild(child);
                            }
                        } else {
                            if (typeof children === 'string') {
                                htmlEl.appendChild(TSX.createTextNode(children));
                            } else {
                                htmlEl.appendChild(children);
                            }
                        }
                    } catch (e: any) {
                        if (e instanceof Error) {
                            console.error('Error when adding child', e.message, children);
                        }
                        throw e;
                    }
                }

                const paragraphContent = node.content?.filter(n => n.type !== SFNodeType.footnote);
                // if we have a paragraph containing only a text node that is strong, we mark it to style it differently if neede (e.g. to style like a heading)
                if (paragraphContent.length === 1 && paragraphContent[0].type === SFNodeType.text) {
                    if (node.content[0].marks && node.content[0].marks.length === 1 && node.content[0].marks[0].type === 'strong' && parent?.type !== SFNodeType.table_cell && parent?.type !== SFNodeType.table_header) {
                        htmlEl.setAttribute('role', 'heading');
                    }
                }
            }
            return htmlEl;
        } catch (e: any) {
            if (e instanceof Error) {
                console.error('Could not render paragraph: ', e.message, htmlEl?.innerHTML);
            }
            throw e;
        }
    };
}

export class ParagraphRenderer extends NodeRenderer<HTMLParagraphElement, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.paragraph>, parent: DocumentNode<SFNodeType>) {
        const htmlEl: HTMLParagraphElement = await (<p />);
        try {
            if (node.attrs) {
                for (let att in node.attrs) {
                    if (node.attrs[att] !== null) {
                        htmlEl.setAttribute(att, node.attrs[att]);
                    }
                }
            }

            if (node.text) { htmlEl.textContent = node.text; }
            if (node.content && node.content?.length > 0) {
                // split the node content into arrays of text nodes and other nodes
                let nodeLists = node.content.reduce<DocumentNode<SFNodeType>[][]>((list, currentNode) => {
                    if (
                        list[list.length - 1].length === 0 ||
                        // the previous type matches the current
                        list[list.length - 1][0].type === currentNode.type) {
                        list[list.length - 1] = [...list[list.length - 1], currentNode];
                    } else {
                        list = [...list, [currentNode]];
                    }

                    return list;
                }, [[]]);

                // now render each list of lists (this way we get to render text nodes together and can organize the DOM hierarchy)
                for (let nodeList of nodeLists) {
                    try {
                        let renderedNodes: (HTMLElement | Text)[] = []
                        if (nodeList[0]?.type == SFNodeType.text) {
                            renderedNodes = renderTextNodes(nodeList as DocumentNode<SFNodeType.text>[]);
                        } else {
                            renderedNodes = await Promise.all(nodeList.map(node => this.engine.render(node)));
                        }

                        for (let n of renderedNodes) {
                            htmlEl.appendChild(n);
                        }
                    } catch (e: any) {
                        if (e instanceof Error) {
                            console.error('Error when adding node list', e.message, nodeList);
                        }
                        throw e;
                    }
                }

                const paragraphContent = node.content?.filter(n => n.type !== SFNodeType.footnote);
                // if we have a paragraph containing only a text node that is strong, we mark it to style it differently if neede (e.g. to style like a heading)
                if (paragraphContent.length === 1 && paragraphContent[0].type === SFNodeType.text) {
                    if (node.content[0].marks && node.content[0].marks.length === 1 && node.content[0].marks[0].type === 'strong' && parent?.type !== SFNodeType.table_cell && parent?.type !== SFNodeType.table_header) {
                        htmlEl.setAttribute('role', 'heading');
                    }
                }
            }
            return htmlEl;
        } catch (e: any) {
            if (e instanceof Error) {
                console.error('Could not render paragraph: ', e.message, htmlEl?.innerHTML);
            }
            throw e;
        }
    };
}
