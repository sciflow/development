import { TSX } from '../../TSX';

import { SciFlowDocumentData } from "../../interfaces";
import { SFNodeType, DocumentNode, SFMarkType, DocumentSnapshotResource } from '@sciflow/schema';
import { NodeRenderer } from "../../node-renderer";
import { HeadingRenderer } from './headings';
import { Environment } from '../components/environment/environment.component';
import { FigureRendererConfiguration } from './figure/figure.schema';
import { domFromMarkedText } from './text';

export class TextRenderer extends NodeRenderer<HTMLElement | Text, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.text>, parent?: DocumentNode<any>) {
        // since this only renders a single text node, you may want to consider grouping text nodes first
        // @see mergeTextNodes 
        return domFromMarkedText(node.marks, node.text);
    }
}

class LinkRenderer extends NodeRenderer<HTMLLinkElement, SciFlowDocumentData> {
    async render(node: DocumentNode<any>) {
        const referencedElement = node.attrs.href.replace('#', '');
        if (!this.engine.data.document.tables) { throw new Error('Tables must be generated at this point'); }
        const locale = this.engine.data.document.locale || 'en-US';
        const idMap = this.engine.data.document.tables.idMap;
        const toc = this.engine.data.document.tables.toc;
        const link = idMap[referencedElement];
        if (!link) {
            return <small style="color: red;">Cross reference to deleted element {referencedElement}</small>;
        }
        const index = link?.customNumber ?? this.engine.data.document.tables.labels[link.type]?.findIndex(d => d.id === referencedElement);
        let text = index > -1 ? (index + 1).toLocaleString(locale === 'ar' ? 'ar-SA' : locale) : link.title;

        const environmentComponent = new Environment(this.engine as any);
        const configuration: FigureRendererConfiguration = environmentComponent.getValues();
        const renderCaptionsInline = configuration.renderCaptionsInline;
        const environment = configuration.environments?.[link.type];

        if (link.type === SFNodeType.heading) {
            const tocEntry = toc.headings?.find(h => h.id === referencedElement);
            if (tocEntry?.numberingString) {
                text = tocEntry?.numberingString;
            }
        }

        const itemFromListing = this.engine.data.document?.tables?.labels?.[link.type]?.find(i => i.id === referencedElement);
        const referenceLabel = itemFromListing?.customLabel
            || environmentComponent.getAs<string>('environments.' + link.type + '.referenceLabel')
            || environment?.referenceLabel ? environment?.referenceLabel + ' ' : '';
        const htmlEl = <a>{referenceLabel}{text}</a>;
        for (let att in node.attrs) {
            htmlEl.setAttribute(att, node.attrs[att]);
        }
        return htmlEl;
    }
}

class ImageRenderer extends NodeRenderer<HTMLImageElement, SciFlowDocumentData> {
    async render(node: DocumentNode<any>) {
        let htmlEl = this.engine.data.virtualDocument.createElement('img');
        const file: DocumentSnapshotResource | undefined = this.engine.data.document.files?.find(f => f.id === node.attrs?.id);
        const url = (file?.compressedUrl || file?.url) as string | undefined;
        if (node.type === SFNodeType.ordered_list && node.attrs?.order) {
            node.attrs.start = node.attrs.order;
            delete node.attrs.order;
        }

        for (let att in node.attrs) {
            if (node.attrs[att] == undefined) { continue; }
            switch (att) {
                case 'height':
                    if (file) {
                        continue;
                    }
                case 'width':
                    if (file) {
                        continue;
                    }
                    /* if (node.attrs[att]) {
                        htmlEl.setAttribute(att, 'auto');
                    } else {
                        htmlEl.setAttribute(att, node.attrs[att]);
                    } */
                    break;
                case 'alt':
                    if (node.attrs.decorative === true) {
                        // we do not set alt-texts on decorative images
                        htmlEl.setAttribute(att, '');
                    } else {
                        htmlEl.setAttribute(att, node.attrs[att]);
                    }
                    break;
                case 'decorative':
                    if (node.attrs[att] === true) {
                        htmlEl.setAttribute('role', 'presentation');
                        htmlEl.setAttribute('alt', '');
                    }
                    break;
                case 'src':
                    if (att === 'src' && url && url?.length > 0) {
                        htmlEl.setAttribute(att, url);
                        break;
                    } else {
                        htmlEl.setAttribute(att, node.attrs[att]);
                    }
                    break;
                default:
                    htmlEl.setAttribute(att, node.attrs[att]);
            }

        }

        if (node.text) {
            htmlEl.textContent = node.text;
        }

        if (node.content) {
            for (let children of await this.engine.renderContent(node)) {
                try {
                    if (Array.isArray(children)) {
                        for (let child of children) {
                            htmlEl.appendChild(child);
                        }
                    } else {
                        htmlEl.appendChild(children);
                    }
                } catch (e: any) {
                    console.error('Error when adding child', e.message, children);
                    throw e;
                }
            }
        }

        return htmlEl;
    }
}

/**
 * 
 * @param tagName the tag to render
 * @param attrs the initial attributes to set
 * @param dataAttrs the data- attributes to include
 * @returns the rendered HTML
 */
export const basicNode = (tagName: string, attrs?: any, dataAttributes?: string[]) => {
    return class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {

        async render(node: DocumentNode<any>) {

            let htmlEl = this.engine.data.virtualDocument.createElement(tagName);

            if (node.text) {
                htmlEl.textContent = node.text;
            }

            if (node.content) {
                for (let children of await this.engine.renderContent(node)) {
                    try {
                        if (Array.isArray(children)) {
                            for (let child of children) {
                                htmlEl.appendChild(child);
                            }
                        } else {
                            htmlEl.appendChild(children);
                        }
                    } catch (e: any) {
                        console.error('Error when adding child', e.message, children);
                        throw e;
                    }
                }
            }

            if (node.attrs) {
                for (let attr of Object.keys(node.attrs)) {
                    if (dataAttributes?.includes(attr)) {
                        htmlEl.setAttribute('data-' + attr, node.attrs[attr]);
                    }
                }
            }

            return htmlEl;
        }
    }
}

class PageBreakRenderer extends NodeRenderer<HTMLSpanElement, SciFlowDocumentData> {
    async render(node: DocumentNode<any>) {
        return <div data-page-break="after"></div>;
    }
}

basicNode('span', { 'data-page-break': 'after' })

const renderers: { [key: string]: any } = {
    [SFNodeType.image]: ImageRenderer,
    [SFNodeType.text]: TextRenderer,
    [SFNodeType.quote]: basicNode('q'),
    [SFNodeType.hardBreak]: basicNode('br'),
    [SFNodeType.pageBreak]: PageBreakRenderer,
    [SFNodeType.ordered_list]: basicNode('ol', {}, ['list-style-type']),
    [SFNodeType.bullet_list]: basicNode('ul'),
    [SFNodeType.link]: LinkRenderer,
    [SFNodeType.hyperlink]: basicNode('a'),
    [SFNodeType.list_item]: basicNode('li'),
    [SFNodeType.horizontalRule]: basicNode('hr'),
    [SFNodeType.subtitle]: basicNode('h2'),
    [SFNodeType.heading]: HeadingRenderer,
    'heading2': basicNode('h1')
};

export default renderers;