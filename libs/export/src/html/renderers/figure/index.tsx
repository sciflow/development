import { TSX } from '../../../TSX';

import { DocumentNode, DocumentSnapshotResource, SFNodeType } from '@sciflow/schema';
import { SciFlowDocumentData } from "../../../interfaces";
import { NodeRenderer } from "../../../node-renderer";

import { getDefaultEnvironments, labelToEnvironment } from '../..';
import { getTextDirection } from '../../../helpers';
import { Environment } from '../../components/environment/environment.component';
import { FigureRendererConfiguration, GenericEnvironment } from './figure.schema';

class FigureRenderer extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.table>, _parent: DocumentNode<SFNodeType.figure>) {
        // we are importing the default implementation for a figure component here. Be aware that this might be different,
        // for some templates. So we cannot rely on the shape of the data provided.
        const environmentComponent = new Environment(this.engine);
        const configuration: FigureRendererConfiguration = environmentComponent.getValues();
        const renderCaptionsInline = configuration.renderCaptionsInline;

        const data = this.engine.data.document;
        const extractTitleFromCaption = async (captionNode): Promise<string | null> => {
            try {
                if (!captionNode?.content) { return ''; }
                if (captionNode.content && captionNode.content?.length > 0) {
                    // extract the first node only
                    const title = await this.engine.render(captionNode.content[0]);
                    if (!title) { return null; }
                    return title.innerHTML;
                }
            } catch (e: any) {
                console.error(e);
            }
            return null;
        };

        let image: HTMLSpanElement | undefined;
        let $figcaption = this.engine.data.virtualDocument.createTextNode('');

        const captionNode = node.content?.find(n => n.type === 'caption');

        let title, titleNode;
        let notes = this.engine.data.virtualDocument.createTextNode('');
        const noteList: HTMLElement[] = [];
        const hasNotes = notes.length !== 0;

        const labelNode = captionNode?.content?.find(c => c.type === SFNodeType.label);
        if (labelNode && captionNode?.content) {
            // remove the label from the caption
            captionNode.content = captionNode.content.filter(c => c.type !== SFNodeType.label);
        }

        let envName = labelNode ? labelToEnvironment(labelNode, configuration) : undefined;
        let environment: GenericEnvironment | undefined = undefined;

        let environments = configuration?.environments || getDefaultEnvironments();

        // try to guess the environment from the label text
        if (envName) {
            environment = environments?.[envName];
        } else {
            switch (node.attrs.type) {
                case 'figure':
                case 'image':
                    environment = environments?.image;
                    envName = 'image';
                    break;
                case 'native-table':
                case 'table':
                    environment = environments?.table;
                    envName = 'table';
                    break;
                case 'code':
                    environment = environments?.code;
                    envName = 'code';
                    break;
                default:
                    environment = environments?.image;
                    envName = 'image';
            }
        }

        if (captionNode?.content !== undefined) {
            title = await extractTitleFromCaption(captionNode);
            if (captionNode.content) {
                titleNode = captionNode?.content.filter(n => n.type !== SFNodeType.label)?.length > 0 ? captionNode.content.filter(n => n.type !== SFNodeType.label)?.[0] : null;
                if (captionNode?.content?.filter(n => n.type !== SFNodeType.label)?.length > 1) {
                    for (let child of captionNode.content.filter(n => n.type !== SFNodeType.label).slice(1, captionNode.content.length)) {
                        noteList.push(await this.engine.render(child, captionNode));
                    }
                    if (!environment?.notesInCaption) {
                        notes = <div data-role="notes">{...noteList}</div>;
                    }
                }
            }
        }

        const altText = node.attrs.decorative !== true ? node.attrs.alt : '';
        const decorative = node.attrs.decorative === true;

        let imgEl;
        let hasImage = false, imageMimeType, file: DocumentSnapshotResource | undefined;
        // tables can have an image as well
        if (node.attrs && node.attrs.src) {
            file = this.engine.data.document.files?.find(f => f.id === node.attrs.id);
            const url = file?.compressedUrl || file?.url;
            if (configuration.transformImages && file?.compressedUrl) {
                imgEl = <img title={title ?? ''} src={url} data-width={file.dimensions?.width} data-height={file.dimensions?.height}></img>;
                image = <div class="figure-body">
                    {imgEl}
                </div>;
                hasImage = true;
            } else if (file && url) {
                imgEl = <img title={title ?? ''} src={url} data-width={file?.dimensions?.width} data-height={file?.dimensions?.height}></img>;
                image = <div class="figure-body">
                    {imgEl}
                </div>;
                hasImage = true;
            } else if (node.attrs.src?.length > 0) {
                hasImage = true;
                if (this.engine.runner === 'epub') {
                    // do not export data: to keep epub file size down
                    if (node.attrs.src.length > 256) {
                        image = <div class="figure-body empty-image" title={title ?? ''}>
                            <svg width="100" height="100" viewBox="0 0 100 100" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="0.5" y="0.5" width="99" height="99" fill="white" stroke="black" /></svg>
                        </div>
                    }
                } else {
                    imgEl = <img title={title ?? ''} src={node.attrs.src} />;
                    image = <div class="figure-body">
                        {imgEl}
                    </div>;
                }
            }

            if (altText?.length > 0) {
                imgEl.setAttribute('alt', altText);
            }

            if (decorative) {
                imgEl.setAttribute('role', 'presentation');
                imgEl.setAttribute('alt', '');
            }

            if (file?.mimeType) {
                imageMimeType = file.mimeType;
                image?.setAttribute('data-mime-type', file.mimeType);
            }
        }

        /** If the element can be exported without a caption we just render it */
        const elementWithoutCaption = !titleNode?.content && environment?.countWithoutCaption !== true;
        if (elementWithoutCaption) {
            if (hasImage) {
                if (!image) { <span class="missing-image" />; }
                if (image && node.attrs) {
                    for (let att in node.attrs) {
                        if (att != null && att !== 'src') {
                            image.setAttribute(att, node.attrs[att]);
                        }
                    }
                    if (node.attrs?.orientation?.length > 0) {
                        image.setAttribute('orientation', node.attrs.orientation);
                    }
                }
                return image || <span class="missing-image" />;
            }
            const content: HTMLElement[] = await this.engine.renderContent(node);
            if (content?.length === 1) {
                return content[0];
            }
        }

        const centerEnvironment = environment?.align?.content === 'center';
        // we try to use the table caption
        // however, when the table caption needs to get wider than the 
        // table we fall back to figcaption (e.g. for centering)
        const renderTableCaptionAsFigCaption = hasImage || !titleNode || centerEnvironment;
        const captionSide = environment.captionSide;
        if (!data.tables) { throw new Error('Tables must be generated at this point'); }
        const index = data.tables!.labels[envName]
            ?.filter(c => c?.title?.length != undefined && c?.title?.length > 0 || environment?.countWithoutCaption)
            .findIndex(c => c.nodeId === node.attrs.id || c.id === node.attrs.id);

        const locale = this.engine.data.document.locale || 'en-US';
        const direction = getTextDirection(locale);

        const itemFromListing = this.engine.data.document?.tables?.labels?.[envName]?.find(i => i.id === node.attrs.id);
        const label = itemFromListing?.customLabel || environmentComponent.getAs<string>('environments.' + envName + '.label');

        // if the title node contains the custom label we need to remove it
        if (titleNode?.content && itemFromListing?.completeLabel) {
            if (titleNode?.content?.[0]?.type === SFNodeType.text && titleNode?.content?.[0]?.text?.startsWith(itemFromListing.completeLabel)) {
                titleNode.content[0].text = titleNode?.content?.[0]?.text.substring(itemFromListing.completeLabel.length);
                if (titleNode.content[0].text?.startsWith(' ')) {
                    titleNode.content[0].text = titleNode.content[0].text.substring(1);
                }
            }
        }

        const localizedIndex = itemFromListing?.customNumber || (index + 1).toLocaleString(locale === 'ar' ? 'ar-SA' : locale);
        let labelElements = [
            environment?.leftDelimiter ? environment.leftDelimiter : '',
            !environment?.suppressLabel ? label : '',
            !environment?.suppressLabel ? ' ' : '',
            localizedIndex,
            environment?.labelSuffix,
            environment?.rightDelimiter ? environment.rightDelimiter : '',
            ' ',
            environment?.breakAfterLabel ? <br /> : ''
        ].filter(s => typeof s !== 'string' || s?.length > 0);

        const $captionLabel = renderCaptionsInline ? <span class="label">
            {...labelElements}
        </span> : '';

        let figureContent: HTMLElement[] = [];
        // filter out caption
        if (node.content) {
            for (let c of node?.content) {
                if (c.type === SFNodeType.caption) { continue; }
                const r = await this.engine.render(c, node);
                figureContent.push(r);
            }
        }

        let $figureWrapper: HTMLDivElement, $figure: HTMLElement | undefined = undefined;
        switch (node.attrs.type) {
            case 'image':
            case 'figure':
                if (titleNode?.content) {
                    const captionContent = await this.engine.renderContent(titleNode);
                    const caption = <span class="caption">
                        {...captionContent}
                        {...(environment?.notesInCaption ? noteList : [])}
                    </span>;
                    if (itemFromListing?.customNumber) {
                        // replace label and numbering the user provided
                        caption.innerHTML = caption.innerHTML.replace(`${label} ${itemFromListing?.customNumber}${environment.labelSuffix} `, '');
                    }
                    $figcaption = <figcaption data-width={file?.dimensions?.width} data-figure-number={index > -1 ? index + 1 : null}>
                        <div class="caption-body">
                            {[$captionLabel, caption]}
                        </div>
                    </figcaption>;
                }
                $figure = <figure data-environment={envName} data-caption-side={captionSide}>
                    {captionSide === 'above' ? $figcaption : ''}
                    {image}
                    {...figureContent}
                    {captionSide === 'below' || captionSide === 'after' ? $figcaption : ''}
                </figure>;
                $figureWrapper =
                    <div class="figure-wrapper" data-align={centerEnvironment ? 'center' : 'start'} data-has-notes={hasNotes ? true : false}>
                        {$figure}
                        {notes ? notes : ''}
                    </div>;
                break;
            case 'native-table':
            case 'table':
                const role = hasImage ? 'image-table' : 'native-table';
                const caption = <span class="caption">
                    {...(titleNode ? await this.engine.renderContent(titleNode) : [])}
                    {...(environment?.notesInCaption ? noteList : [])}
                </span>;
                if (titleNode?.content) {
                    $figcaption = <figcaption data-table-number={index > -1 ? index + 1 : null}>
                        <div class="caption-body">
                            {[$captionLabel, caption]}
                        </div>
                    </figcaption>;
                }
                if (captionSide === 'before' || captionSide === 'after') {
                    // this would usually be combined with supress label and right and left delimiters
                    $figcaption = <figcaption data-number={index > -1 ? index + 1 : null}>
                        <div class="caption-body">
                            {$captionLabel}
                        </div>
                    </figcaption>
                }
                $figure = <figure data-environment={envName} data-caption-side={captionSide} data-role={role}>
                    {/* caption is part of a native table, we will still add the caption if no caption body is provided (to position numbering) */}
                    {renderTableCaptionAsFigCaption && (captionSide === 'above' || captionSide === 'before') ? $figcaption : ''}
                    {image}
                    {...figureContent}
                    {renderTableCaptionAsFigCaption && (captionSide === 'below' || captionSide === 'after') ? $figcaption : ''}
                </figure>;
                $figureWrapper =
                    <div class="figure-wrapper" data-role={role} data-align={centerEnvironment ? 'center' : 'start'} data-has-notes={hasNotes ? true : false}>
                        {$figure}
                        {notes ? notes : ''}
                    </div>;
                break;
            case 'code':
                if (titleNode) {
                    const caption = <span class="caption">
                        {...(await this.engine.renderContent(titleNode))}
                        {...(environment?.notesInCaption ? noteList : [])}
                    </span>;
                    $figcaption = <figcaption data-code-block-number={index > -1 ? index + 1 : null}>
                        <div class="caption-body">
                            {[$captionLabel, caption]}
                        </div>
                    </figcaption>;
                }
                $figure = <figure data-environment={envName} data-caption-side={captionSide}>
                    {...figureContent}
                    {$figcaption}
                </figure>;
                $figureWrapper =
                    <div class="figure-wrapper" data-align={centerEnvironment ? 'center' : 'start'} data-has-notes={hasNotes ? true : false}>
                        {$figure}
                        {notes ? notes : ''}
                    </div>;
                break;
            default:
                $figureWrapper = <div>Unknown figure type {node.attrs.type}</div>;
                break;
        }

        if (imageMimeType) {
            $figure?.setAttribute('data-image-mime-type', imageMimeType);
        }

        $figureWrapper.setAttribute('data-environment', envName);

        // see prince page floats for more
        if (node.attrs['float-placement']) { $figureWrapper.setAttribute('data-float-placement', node.attrs['float-placement']); }
        if (node.attrs['float-reference']) { $figureWrapper.setAttribute('data-float-reference', node.attrs['float-reference']); }
        if (node.attrs['float-defer-page']) { $figureWrapper.setAttribute('data-float-defer-page', node.attrs['float-defer-page']); }
        if (node.attrs['float-modifier']) { $figureWrapper.setAttribute('data-float-modifier', node.attrs['float-modifier']); }
        if (node.attrs['scale-width']) { $figureWrapper.setAttribute('data-scale-width', node.attrs['scale-width']); }

        if (node.attrs && $figure) {
            for (let att in node.attrs) {
                if (att === 'width' || att === 'height') { continue; }
                if (att != null && att !== 'src') {
                    $figure.setAttribute(att, node.attrs[att]);
                }
            }
            if (node.attrs?.orientation?.length > 0) {
                $figureWrapper.setAttribute('orientation', node.attrs.orientation);
            }
        }

        return $figureWrapper;
    }
}

class LabelRenderer extends NodeRenderer<HTMLTableCaptionElement, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.table>, parent: DocumentNode<SFNodeType.figure>) {
        return <span></span>;
    }
}

class CaptionRenderer extends NodeRenderer<HTMLTableCaptionElement, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.table>, parent: DocumentNode<SFNodeType.figure>) {
        if (!node.content || node.content?.length === 0) { return []; }
        const label = await this.engine.renderContent(node.content[0]);
        if (label?.length === 0) { return []; }
        return <caption><span class="caption">{...label}</span></caption>;
    }
}

const renderers: { [key: string]: any } = {
    [SFNodeType.figure]: FigureRenderer,
    [SFNodeType.caption]: CaptionRenderer,
    [SFNodeType.label]: LabelRenderer
};

export default renderers;