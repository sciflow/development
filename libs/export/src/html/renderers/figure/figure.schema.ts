export interface GenericEnvironment {
  /**
   * @title Image caption/counter position
   * @default below
   */
  captionSide: 'above' | 'below' | 'before' | 'after';

  /**
   * @title Label
   * @default Figure
   */
  label: string;

  /**
   * @title Notes in caption
   * @description Puts the notes inside the caption instead of in a seperate element after the figure.
   * @default false
  */
  notesInCaption?: boolean;

  /**
   * @title Suffix for label
   * @description a suffix for a label, e.g. "." (leads to Figure 1.)
   * @default .
   */
  labelSuffix: string;

  /**
   * @title Font weight of the label
   * @default bold
   */
  labelWeight: string;

  /**
   * @title Whether to break line after the label.
   * @default false
   */
  breakAfterLabel: boolean;

  /**
   * @title Reference label
   * @description Label to be used in cross references (e.g. see fig. 1)
   * @default fig.
   * @minLength 0
   */
  referenceLabel: string;

  /**
   * @title Left delimiter
   * @description Delimiter for a counter (e.g. [ for [0])
  */
  leftDelimiter?: string;

  /**
 * @title Right delimiter
 * @description Delimiter for a counter (e.g. ] for [0])
*/
  rightDelimiter?: string;

  /**
   * @title Numbering separator
   * @default .
   * @description the separator used to create a number (e.g. . for 1.1)
  */
  numberingSeparator?: string;

  /**
   * @title Supress label
   * @default false
   * @description Supresses the label in a caption (so numbering is shown alone)
   */
  suppressLabel?: boolean;

  /**
   * @title Count even without caption
   * @default false
   * @description By default, environments without captions are not numbered
   * (but this might be wanted for environments that usually do not have a caption text but just a number)
   */
  countWithoutCaption?: boolean;

  /**
   * @title Align text in figure
   * @default 1rem auto
   */
  margin?: string;

  /**
   * @title Center content (like images)
   * @description (!) Warning,. HTML tables can not have table captions wider than the table itelf. This option will force a figcaption which may impede accessibility
   */
  align: {
    /**
     * @title Align the content (e.g. image/table)
     * @default start
     */
    content: string;

    /**
     * @title Align the caption
     * @default start
     */
    caption: string;
  };

  /**
   * @title Float options for PDF (Paged Media)
   * @description see https://www.princexml.com/doc/styling/#floats
   */
  floats?: {
    /**
     * @title Float placement
     * @description https://www.princexml.com/doc/css-props/#prop-prince-float-placement
     * @default none
     */
    placement: 'none' | 'left' | 'right' | 'inside' | 'outside'
    | 'top' | 'bottom' | 'top-bottom' | 'snap'
    | 'align-top' | 'align-bottom'
    | 'footnote' | 'inline-footnote';

    /**
     * @title Float reference
     * @description https://www.princexml.com/doc/css-props/#prop-prince-float-reference
     * @default page
     */
    reference: 'column' | 'page'
    | 'sidenote' | 'leftnote' | 'rightnote' | 'insidenote' | 'outsidenote'
    | 'wide' | 'wide-left' | 'wide-right' | 'wide-inside' | 'wide-outside';

    /**
     * @title Defer page
     * @default none
     * @description float to a right or left page
     */
    deferPage: 'none' | 'left' | 'right';
    /**
     * @title Float modifiers (conditions)
     * @default normal
     * @description https://www.princexml.com/doc/css-props/#prop-prince-float-modifier
     */
    modifier: 'normal' | 'unless-fit';

    /**
     * @title Float policy
     * @description Determins whether the order of floats can be changed to improve whitespace.
     * @default in-order
     */
    policy: 'normal' | 'in-order';
  };
}

interface ImageEnvironment extends GenericEnvironment { }

export interface TableEnvironment extends GenericEnvironment {
  /**
   * @title Label
   * @default Table
   */
  label: string;

  /**
   * @title Reference label
   * @description Label to be used in cross references (e.g. see fig. 1)
   * @default tbl.
   * @minLength 0
   */
  referenceLabel: string;

  /**
* @title Image caption/counter position
* @default above
*/
  captionSide: 'above' | 'below' | 'before' | 'after';

  /**
 * @title Whether whether table captions will be displayed on the first page of a table, or only on following pages, or repeated on every page that a table appears on.
 * @default all
 */
  captionPage: 'first' | 'following' | 'all';

  /**
   * @title Table styling
   * @default apa
   */
  theme: 'apa' | 'table1' | 'table2' | 'table3';

  /**
   * @title Center content (like images)
   * @description (!) Warning,. HTML tables can not have table captions wider than the table itelf. This option will force a figcaption which may impede accessibility
   */
  align: {
    /**
     * @title Align the content (e.g. image/table)
     * @default start
     */
    content: string;

    /**
     * @title Align the caption
     * @default start
     */
    caption: string;
  };
  /**
   * @title Font size
   * @default inherit
   */
  fontSize: string;

  /**
   * @title Line height
   * @default 1
   * @description The line height of text inside tables
   */
  lineHeight: string | number;
}

interface CodeEnvironment extends GenericEnvironment {
  /**
   * @title Label
   * @default Code
   */
  label: string;

  /**
   * @title Reference label
   * @description Label to be used in cross references (e.g. see fig. 1)
   * @default code
   * @minLength 0
   */
  referenceLabel: string;

  /**
   * @title Center content (like images)
   * @description (!) Warning,. HTML tables can not have table captions wider than the table itelf. This option will force a figcaption which may impede accessibility
   */
  align: {
    /**
     * @title Align the content (e.g. image/table)
     * @default start
     */
    content: string;

    /**
     * @title Align the caption
     * @default start
     */
    caption: string;
  };
  /**
 * @title Font size
 * @default inherit
 */
  fontSize: string;
}

interface EquationEnvironment extends GenericEnvironment {
  /**
 * @title Render MathML instead of a Mathjax svg
 * @default false
 */
  renderMathML: boolean;

  /**
   * @title Scaling factor for equations
   */
  scale: {
    /**
     * @title Inline equations
     * @default 1
     */
    inline: number;

    /**
     * @title Block equations
     * @default 1
     */
    block: number;
  }
  /**
 * @title Left delimiter
 * @default (
 * @description Delimiter for a counter (e.g. [ for [0])
*/
  leftDelimiter?: string;

  /**
 * @title Right delimiter
 * @default )
 * @description Delimiter for a counter (e.g. ] for [0])
*/
  rightDelimiter?: string;

  /**
* @title Image caption/counter position
* @default after
*/
  captionSide: 'above' | 'below' | 'before' | 'after';

  /**
  * @title Count even without caption
  * @default true
  * @description By default, environments without captions are not numbered
  * (but this might be wanted for environments that usually do not have a caption text but just a number)
  */
  countWithoutCaption?: boolean;

  /**
   * @title Supress label
   * @default true
   * @description Supresses the label in a caption (so numbering is shown alone) (if numbering is supressed the label will also not show)
   */
  suppressLabel?: boolean;

  /**
   * @title Numbering separator
   * @default .
   * @description the separator used to create a number (e.g. . for 1.1)
   */
  numberingSeparator: string;

  /**
 * @title Supress numbering
 * @default false
 * @description Supresses the numbering
 */
  suppressNumbering?: boolean;

  /**
   * @title Center content (like images)
   * @description (!) Warning,. HTML tables can not have table captions wider than the table itelf. This option will force a figcaption which may impede accessibility
   */
  align: {
    /**
     * @title Align the content (e.g. image/table)
     * @default start
     */
    content: string;

    /**
     * @title Align the caption
     * @default start
     */
    caption: string;
  };
}

/**
* @title General figure/environment settings
*/
export interface FigureRendererConfiguration {
  environments: {
    /**
    * @title Table options
    */
    table: TableEnvironment;

    /**
     * @title Image options
     */
    image: ImageEnvironment;

    /**
     * @title Code options
     */
    code: CodeEnvironment;

    /**
     * @title Equation options
     */
    equation: EquationEnvironment;

    [environment: string]: GenericEnvironment;
  }
  /**
   * @title Allow custom labels
   * @description Whether custom labels (other than for the default environments) are allowed
   * @default false
   */
  allowCustomLabels: boolean;

  /**
   * @title Inline captions
   * @description Whether captions should be included in the output (and not styling). This is preferred for accessibility.
   * @default true
   */
  renderCaptionsInline: boolean;

  /**
   * @title Transform images
   * @description Image compression, format and preferred DPI
   */
  transformImages?: {
    /**
     * @title Format to use
     */
    format: 'WebP' | 'JPEG' | 'PNG' | undefined;

    /**
     * @title Webp quality
     * @default 80
     */
    quality: number;

    /**
     * @title DPI image quality
     * @default 96
     */
    dpi: number;
  }
  
  numbering: {
    /**
     * @title Resetting numbering
     * @description Where to reset numbering (0 means global numbering, 2 would create Figure 1.1.2)
     * @default 0
     */
    resetAt: number;
  }
}
