import { TSX } from '../../TSX';

import { SciFlowDocumentData } from "../../interfaces";
import { SFNodeType, DocumentNode } from '@sciflow/schema';
import { NodeRenderer } from "../../node-renderer";
import { JSDOM } from 'jsdom';

import { convert, create } from 'xmlbuilder2';
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces';

const { TeX } = require('mathjax-full/js/input/tex.js');
import { HTMLDocument } from 'mathjax-full/js/handlers/html/HTMLDocument.js';
import { chooseAdaptor } from 'mathjax-full/js/adaptors/chooseAdaptor';
import { JsdomAdaptor, jsdomAdaptor } from 'mathjax-full/js/adaptors/jsdomAdaptor';

import { AllPackages } from 'mathjax-full/js/input/tex/AllPackages.js';
import { RegisterHTMLHandler } from 'mathjax-full/js/handlers/html';


import { SerializedMmlVisitor } from 'mathjax-full/js/core/MmlTree/SerializedMmlVisitor.js';
import { SVG } from 'mathjax-full/js/output/svg.js';
import { FigureRendererConfiguration } from './figure/figure.schema';
import { Environment } from '../components/environment/environment.component';
import { getDefaultEnvironments } from '..';
import * as SRE from 'speech-rule-engine';

const packages = AllPackages.filter((name) => name !== 'bussproofs');

const dom = new JSDOM();
const document = dom.window.document;

const jdomAdaptor = jsdomAdaptor(JSDOM);
const HTMLHandler = RegisterHTMLHandler(jdomAdaptor);

const initializeSRE = async () => {
    SRE.setupEngine({
        locale: 'en',
        domain: 'clearspeak',
        style: 'default',
        speech: 'deep'
    });

    await SRE.engineReady();
}

const renderers: { [key: string]: any } = {
    [SFNodeType.math]: class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.math>) {

            // we are importing the default implementation for a figure component here. Be aware that this might be different,
            // for some templates. So we cannot rely on the shape of the data provided.
            const figureComponent = new Environment(this.engine);
            const figureConfiguration: FigureRendererConfiguration = figureComponent.getValues();
            let environments = figureConfiguration?.environments || getDefaultEnvironments();
            const equationConf = environments?.equation;

            const scaleInlineEquations = equationConf?.scale?.inline || 1;
            const scaleBlockEquations = equationConf?.scale?.block || 1;

            try {
                await initializeSRE();
            } catch (error) {
                debugger;
                console.error('Could not initialize SRE', error);
            }

            /**
             * Renders an SVG from TeX.
             * @param s the tex expression (compatible with MathJax)
             * @param inline if inline or block
             * @param localID an id prefix to make the SVG ids unique
             * @returns an svg string
             */
            const tex2svg = (s: string, inline = true, localID = '0') => {
                const tex = new TeX({
                    packages,
                    inlineMath: [              // start/end delimiter pairs for in-line math
                        ['\\(', '\\)']
                    ],
                    displayMath: [             // start/end delimiter pairs for display math
                        ['$$', '$$'],
                        ['\\[', '\\]']
                    ],
                    processEscapes: true,      // use \$ to produce a literal dollar sign
                    processEnvironments: true, // process \begin{xxx}...\end{xxx} outside math mode
                    processRefs: true,         // process \ref{...} outside of math mode
                    digits: /^(?:[0-9]+(?:\{,\}[0-9]{3})*(?:\.[0-9]*)?|\.[0-9]+)/,
                    // pattern for recognizing numbers
                    tags: 'none',              // or 'ams' or 'all'
                    tagSide: 'right',          // side for \tag macros
                    tagIndent: '0.8em',        // amount to indent tags
                    useLabelIds: true,         // use label name rather than tag for ids
                    multlineWidth: '85%',      // width of multline environment
                    maxMacros: 1000,           // maximum number of macro substitutions per expression
                    maxBuffer: 5 * 1024,       // maximum size for the internal TeX string (5K)
                    /*  baseURL:                   // URL for use with links to tags (when there is a <base> tag in effect)
                         (document.getElementsByTagName('base').length === 0) ?
                             '' : String(document.location).replace(/#.*$/, ''), */
                    formatError:               // function called when TeX syntax errors occur
                        (jax: any, err: any) => {
                            throw err;
                            // return jax.formatError(err);
                        }
                });

                const scale = inline ? scaleInlineEquations : scaleBlockEquations;

                //const adaptor = liteAdaptor();
                const svg = new SVG({
                    scale: 1, // global scaling factor for all expressions
                    minScale: .5,                                               // smallest scaling factor to use
                    mtextInheritFont: false,                                    // true to make mtext elements use surrounding font
                    merrorInheritFont: true,                                    // true to make merror text use surrounding font
                    mathmlSpacing: false,                                       // true for MathML spacing rules, false for TeX rules
                    skipAttributes: {},                                         // RFDa and other attributes NOT to copy to the output
                    exFactor: .5,                                               // default size of ex in em units
                    displayAlign: 'center',                                     // default for indentalign when set to 'auto'
                    displayIndent: '0',                                         // default for indentshift when set to 'auto'
                    fontCache: 'local',                                         // or 'global' or 'none'
                    localID,                                                    // ID to use for local font cache (for single equation processing)
                    internalSpeechTitles: true,                                 // insert <title> tags with speech content
                    titleID: 0
                });

                const mathml = tex2mml(s);
                const speech = SRE.toSpeech(mathml.trim());
                
                let html, node;
                try {
                    html = new HTMLDocument(document, jdomAdaptor, { InputJax: tex, OutputJax: svg });
                    node = html.convert(s, {
                        display: !inline,
                        em: 16,
                        ex: 8,
                        containerWidth: 80 * 16
                    });
                } catch (e) {
                    debugger;
                    throw new Error('Failed to convert TeX');
                }

                try {
                    const svgEl = node.firstChild as Element;
                    if (!svgEl) throw new Error('No SVG element found in node');

                    const originalWidth = parseFloat(svgEl.getAttribute('width') ?? '0') * scale;
                    const originalHeight = parseFloat(svgEl.getAttribute('height') ?? '0');

                    const height = originalHeight * scale;
                    const width = originalWidth * scale;

                    svgEl.setAttribute('width', `${width}ex`);
                    svgEl.setAttribute('height', `${height}ex`);
 
                    if (speech) {
                        const desc = document.createElementNS('http://www.w3.org/2000/svg', 'desc');
                        desc.textContent = `This SVG represents the equation: ${speech}`;
                        svgEl.prepend(desc);

                        svgEl.setAttribute('aria-label', speech);
                    }
                    return convert(node.firstChild.outerHTML, {
                        headless: true,
                        prettyPrint: true
                    })?.toString();
                } catch (e) {
                    debugger;
                    throw new Error('Failed to render TeX to SVG');
                }
            }

            const tex2mml = (s: string) => {
                const tex = new TeX({ packages });
                const html = new HTMLDocument('', jdomAdaptor, { InputJax: tex });
                const visitor: any = new SerializedMmlVisitor();
                const toMathML = (node => visitor.visitTree(node, html));
                return toMathML(html.convert(s));
            }

            // make sure block equations have a counter already
            if (node.attrs?.style === 'block' && !this.engine.data.document.tables) { throw new Error('Tables must be generated at this point'); }
            const equations = this.engine?.data?.document?.tables?.equations?.filter((e) => e.attrs?.style === 'block');



            const index = equations?.findIndex(t => t.id === node.attrs?.id) ?? 0;
            const itemFromListing = this.engine.data.document?.tables?.labels?.equation?.find(i => i.id === node.attrs.id);
            const inline = node.attrs.style !== 'block';

            let label, customNumber: string | number = '';
            // if there is no numbering, we will not consider the label either
            if (equationConf.suppressNumbering !== true) {
                customNumber = itemFromListing?.customNumber || (index !== -1 ? index + 1 : '');
                label = (equationConf.suppressLabel ? '' : equationConf.label + ' ') + customNumber;
                if (equationConf?.leftDelimiter) { label = equationConf?.leftDelimiter + label; }
                if (equationConf?.rightDelimiter) { label = label + equationConf?.rightDelimiter; }
            }

            const renderSvg = !equationConf?.renderMathML === true;
            let tex = node.attrs.tex;
            if (!tex) {
                if (node.content?.some((c) => c.type === SFNodeType.text)) {
                    tex = node.content.find((c) => c.type === SFNodeType.text)?.text;
                }
            }

            if (renderSvg) {
                const svgAsFile = this.engine.runner === 'epub';
                let mathEl: HTMLOrSVGElement | HTMLImageElement | null | MathMLElement = null;
                try {
                    let svgXMLString = tex2svg(tex, inline, node.attrs.id);
                    if (!svgXMLString != undefined) {
                        mathEl = TSX.htmlStringToElement(svgXMLString as string);

                        const title = document.createElementNS('http://www.w3.org/2000/svg','title');
                        title.textContent = label !== '()' ? `Equation: ${label}` : 'Inline-Equation';
                        (mathEl as SVGElement)?.prepend(title);

                        if (inline) { return <span class="inline-math">{mathEl}</span>; }
                        if (svgAsFile) {
                            const url = 'assets/' + node.attrs.id + '.svg';
                            const id = node.attrs.id;
                            this.engine.data.document.files.push({
                                id,
                                fileId: id,
                                type: 'image',
                                url,
                                inline: true,
                                mimeType: 'image/svg+xml',
                                content: `${svgXMLString}`
                            });
                            mathEl = <img src={url} id={'eq-' + id} />;
                        }
                    }
                } catch (e: any) {
                    this.engine.log('error', 'Could not render equation as svg. Falling back to MathML', { message: e.message });
                }

                if (!mathEl) {
                    try {
                        let mathMLString = tex2mml(tex);
                        const speech = SRE.toSpeech(mathMLString.trim());
                        mathEl = TSX.htmlStringToElement(mathMLString);

                        if (speech) {
                            (mathEl as MathMLElement).setAttribute('alttext', speech);
                        }

                        if (inline && mathEl) {
                            (mathEl as MathMLElement).setAttribute('display', 'inline');
                        }
                        if (this.engine.isDebug) {
                            mathEl = <span style="color: red;">could not render SvG for <span class="p1">{mathEl}</span><span class="p1" style="font-family: monospace;">{tex}</span></span>;
                        }
                    } catch (e: any) {
                        this.engine.log('error', 'Could not render equation as MathML', { message: e.message });
                    }
                }

                try {
                    const captionSide = environments?.equation?.captionSide;
                    // we use a block element to wrap the equation since it's not inline
                    const svgWrapper = <div class="equation">{mathEl ? mathEl : <span style="color:red">{tex}</span>}</div>;

                    const figure = <figure id={node.attrs.id} data-type='equation' data-format='svg' data-label={label} data-caption-side={captionSide}>
                        {label && captionSide === 'before' ? <figcaption class="label">{label}</figcaption> : ''}
                        {svgWrapper}
                        {label ? <figcaption class="label">{label}</figcaption> : ''}
                    </figure>;
                    return figure;
                } catch (e: any) {
                    debugger;
                    // throw new RenderError('Could not render equation svg', { message: e.message, attrs: node.attrs });
                    console.error('Could not render equation svg', { message: e.message, attrs: node.attrs });
                    this.engine.log('warn', 'Could not render equation svg', { message: e.message, attrs: node.attrs });
                    // fall back to MathML
                    return <span style="color: red; font-size: 90%; margin-left: 1rem;">could not render equation ${tex} (${e.message})</span>;
                }
            } else {
                try {
                    const ret = tex2mml(tex);
                    const speech = SRE.toSpeech(ret.trim());
                    const mathML = JSDOM.fragment(ret);

                    if (speech) {
                        mathML.querySelector('math')?.setAttribute('alttext', speech);
                    }

                    if (!inline) {
                        return <figure id={node.attrs.id} data-type='equation' data-format='mathml' data-label={label} data-caption-side={equationConf?.captionSide}>
                            {label ? <figcaption class="label">{label}</figcaption> : ''}
                            <span class="equation">{mathML}</span>
                        </figure>;
                    }

                    const equation = mathML?.firstChild as HTMLElement;
                    if (!equation) { return mathML };
                    equation.setAttribute('display', 'inline');

                    return mathML;
                } catch (e: any) {
                    console.error('Could not render equation MathML', { message: e.message, attrs: node.attrs });
                }

            }

            return <figure id={node.attrs.id} data-type='equation' data-format='mathml' data-label={customNumber} data-caption-side={equationConf?.captionSide}>
                TeX: {node.attrs.tex}
            </figure>

        }
    }
};

export default renderers;