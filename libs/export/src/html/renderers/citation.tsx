import { JSDOM } from 'jsdom';
import { TSX } from '../../TSX';

import { SFNodeType, DocumentNode } from '@sciflow/schema';

import { SciFlowDocumentData } from './../../interfaces';
import { NodeRenderer } from '../../node-renderer';
import { SourceField, Citation } from '@sciflow/cite';
import { escapeId } from '../../helpers';
import { FootnoteRendererConfiguration } from './footnote/footnote.schema';
import { Footnote } from '../components/footnote/footnote.component';

export class CitationRenderer extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.citation>, parent: DocumentNode<SFNodeType>) {
        const citation: Citation = SourceField.fromString(node.attrs.source);
        let placeholder = '[' + citation.citationItems.map(c => c.id).join(',') + ']';
        if (!this.engine.data.document.tables) { return <span style="color: red;">placeholder</span>; }

        const citationsAsFootnotes = parent?.type !== SFNodeType.footnote && this.engine.data.citationRenderer.footnotes === true;
        if (citationsAsFootnotes) {
            const footnoteComponent = new Footnote(this.engine);
            const configuration: FootnoteRendererConfiguration = footnoteComponent.getValues();
            const index = this.engine.data.document.tables.footnotesAndCitations?.findIndex((footnote) => footnote.attrs?.id === node.attrs?.id);
            // move citation footnotes into endnotes (the endnote renderer will have to render the citation)
            if (configuration?.mode === 'endnote') {
                const endnoteId = node?.attrs?.id ?? (index + 1);
                const numberWidth = `${index + 1}`?.length;
                return <a data-type="endnote-call" data-number-width={numberWidth} id={'enref' + endnoteId} href={'#en' + endnoteId} target="_self">
                    {index + 1}
                </a>;
            }
        }

        return this.renderCitation(node, citationsAsFootnotes);
    }

    /**
     * Renders the citation irrespective of endnote settings.
     */
    async renderCitation(node: DocumentNode<SFNodeType.citation>, asFootnote = false) {
        const citation = SourceField.fromString(node.attrs.source);
        if (!citation) {
            return <citation style="color: red;">Unreadable reference</citation>
        }

        try {
            const renderedCitation = this.engine.data.citationRenderer.renderCitation(citation);
            const id = citation.citationItems?.[0]?.id;
            return <a href={'#ref-' + escapeId(id)} data-type={asFootnote ? 'footnote' : 'citation'}>{JSDOM.fragment(renderedCitation.renderedCitation)}</a>
        } catch (e: any) {
            if (e instanceof Error) {
                this.engine.log('error', 'Could not render reference ' + citation?.citationItems?.map(i => i.id).join(), { message: e.message, reference: citation });
            }
            return <citation style="color: red;">Missing reference {citation?.citationItems?.map(i => i.id).join()}</citation>;
        }
    }
}

const renderers: { [key: string]: any } = {
    [SFNodeType.citation]: CitationRenderer
};

export default renderers;