import { TSX } from '../../TSX';

import { SciFlowDocumentData } from "../../interfaces";
import { SFNodeType, DocumentNode } from '@sciflow/schema';
import { NodeRenderer } from "../../node-renderer";

import inlineRenderers from './inline';
import footnoteRenderers from './footnote';
import tableRenderers from './table';
import figureRenderers from './figure';
import citationRenderers from './citation';
import mathRenderers from './math';
import { escapeId, getTextDirection, wrapUrlLinks } from '../../helpers';
import { createFactories, createInstances } from './helpers';
import { ParagraphRenderer } from './paragraph';

const renderers: { [key: string]: any } = {
    [SFNodeType.part]: class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.part>) {

            let partNode = { ...node };
            const elements: { [subPage: string]: HTMLElement[] } = { front: [] };

            /*
            We treat parts almost like its own sub-document if there are configurations for parts 
            */
            const partConfigurations = this.engine.data.configurations?.filter(c => c.page === 'part') || [];
            if (this.engine.data.templateComponents && partConfigurations.length > 0) {
                // components can be used to inject DOM into the document (e.g. for the table of contents)
                // or just render CSS (e.g. to style the @page)
                const componentFactories = createFactories(this.engine.data.templateComponents, this.engine);

                // go through the template configuration values and create the styles and additional DOM elements
                let { styles, instances } = await createInstances(partConfigurations, componentFactories, {
                    runner: 'html',
                    componentPaths: this.engine.data.componentPaths,
                    logger: this.engine.logging?.logger,
                    assetBasePaths: []
                });
                if (styles.length > 0) {
                    elements['front'].push(<style>
                        {styles.map(s => s.css).join('/n/n')}
                    </style>);
                }
                for (let instance of instances) {
                    const page = instance.subPage || 'body';
                    if (!elements[page]) { elements[page] = []; }
                    let dom;
                    if (typeof instance.dom === 'function') {
                        dom = await instance.dom(this.engine);
                    } else {
                        dom = instance.dom;
                    }

                    if (dom) { elements[page].push(...dom); }
                }

                const content = node.content?.filter(n => n.attrs.type !== 'title');

                // we filter out the title since the part brings its own components
                partNode = {
                    ...partNode,
                    content
                };
            }

            let content = await this.engine.renderContent(partNode);
            const section = <section id={partNode.attrs?.id} data-type="part">
                {...(elements['front'] ? elements['front'] : [])}
                {...content}
                {...(elements['body'] ? elements['body'] : [])}
                {...(elements['back'] ? elements['back'] : [])}
            </section>;
            return section;
        }
    },
    [SFNodeType.document]: class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.document>) {
            if (!node.attrs) { node.attrs = {}; }

            let content = await this.engine.renderContent(node);
            if (node.attrs?.type === 'bibliography') {
                let { references, formatting } = await this.engine.data.citationRenderer.getBibliography();
                for (const [i, reference] of references.entries()) {
                    const source = formatting.entry_ids[i];
                    const el = TSX.htmlStringToElement(reference?.trim());
                    const wrappedEl = wrapUrlLinks(el);
                    wrappedEl?.setAttribute('data-maxoffset', formatting.maxoffset + '');
                    wrappedEl?.setAttribute('data-entryspacing', formatting.entryspacing + '');
                    wrappedEl?.setAttribute('data-linespacing', 'true');
                    if (formatting.hangingindent) {
                        wrappedEl?.setAttribute('data-hangingindent', 'true');
                    }
                    if (formatting['second-field-align']) {
                        wrappedEl?.setAttribute('data-second-field-align', formatting['second-field-align']);
                    }
                    if (source?.[0]) {
                        wrappedEl?.setAttribute('id', 'ref-' + escapeId(source[0]));
                    }
                    if (wrappedEl) { content.push(wrappedEl); }
                }
            }

            const section = <section id={node.attrs.id} data-id={node.attrs.id} data-type={node.attrs.type} data-role={node.attrs.role} data-level='1'>
                {...content}
            </section>;

            const pageBreak = node.attrs.pageBreak;
            if (Array.isArray(pageBreak) && pageBreak.length === 0) { section.setAttribute('data-page-break', 'auto'); }
            if (Array.isArray(pageBreak) && pageBreak.length > 0) { section.setAttribute('data-page-break', pageBreak.join(' ')); }

            if (node.attrs?.pageBreak?.length > 0) {
                if (node.attrs?.pageBreak.some(pb => pb === 'right')) {
                    section.setAttribute('data-page-break', 'right');
                } else if (node.attrs?.pageBreak.some(pb => pb === 'before-and-after') || (node.attrs?.pageBreak.some(pb => pb === 'before') && node.attrs?.pageBreak.some(pb => pb === 'after'))) {
                    section.setAttribute('data-page-break', 'before-and-after');
                } else if (node.attrs?.pageBreak.some(pb => pb === 'before')) {
                    section.setAttribute('data-page-break', 'before');
                } else if (node.attrs?.pageBreak.some(pb => pb === 'after')) {
                    section.setAttribute('data-page-break', 'after');
                }
            }

            const locale = node.attrs.locale || this.engine.data.document.locale;
            let direction = 'ltr';
            if (locale) {
                // make sure we add the direction if its not ltr
                direction = getTextDirection(locale) || direction;
            }

            if (node.attrs.locale?.length > 0) {
                section.setAttribute('lang', node.attrs.locale);
            }

            if (node.attrs.direction || direction != 'ltr') {
                section.setAttribute('dir', node.attrs.direction || direction);
                section.setAttribute('text-direction', node.attrs.direction || direction);
            }
            if (node.attrs.locale) {
                section.setAttribute('lang', node.attrs.locale);
            }

            return section;
        }
    },
    [SFNodeType.section]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.section>) {
            const section = <section id={node.attrs.id} data-type={node.attrs.type} data-level={node.attrs.level} data-role={node.attrs.role} data-numbering={node.attrs.numbering}>
                {await Promise.all(node.content?.map(async (c) => {
                    const rc: any = await this.engine.render(c);
                    return rc;
                }) || '')}
            </section>;
            if (node.attrs.direction) {
                section.setAttribute('text-direction', node.attrs.direction);
            }
            if (node.attrs.locale) {
                section.setAttribute('lang', node.attrs.locale);
            }
            return section;
        }
    },
    ...inlineRenderers,
    [SFNodeType.header]: class extends NodeRenderer<HTMLHeadElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.header>) {
            const content = await this.engine.renderContent(node);
            return <header>
                {...content}
            </header>
        }
    },
    [SFNodeType.paragraph]: ParagraphRenderer,
    [SFNodeType.code]: class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.code>) {
            return <pre>{...(await this.engine.renderContent(node))}</pre>
        }
    },
    ...footnoteRenderers,
    ...citationRenderers,
    ...mathRenderers,
    ...tableRenderers,
    ...figureRenderers,
    [SFNodeType.placeholder]: class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.placeholder>) {
            const locale = this.engine.data.document.locale || 'en-US';
            let placeHolderAsset = this.engine.data.document.files?.find(asset => asset.role === node.attrs.type && asset.locales?.includes(locale));
            if (!placeHolderAsset) {
                placeHolderAsset = this.engine.data.document.files?.find(asset => asset.role === node.attrs.type && (asset.locales?.includes('en-US') || !asset.locales));
            }

            if (placeHolderAsset) {
                const image = <img id={placeHolderAsset.id} src={placeHolderAsset.url} />;
                if (placeHolderAsset.dimensions) {
                    image.setAttribute('style', `width: ${(placeHolderAsset.dimensions as any)?.width}; height: ${(placeHolderAsset.dimensions as any)?.height}; max-height: initial !important;`);
                }
                return <span data-type={node.attrs?.type}>
                    {image}
                </span>;
            }
            return <span data-type={node.attrs?.type} />;
        }
    },
    [SFNodeType.blockquote]: class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.blockquote>) {
            const content = await this.engine.renderContent(node);
            const body = <blockquote id={node.attrs?.id}>
                {...content}
            </blockquote>;
            if (node?.attrs?.lang) { body.setAttribute('lang', node.attrs.lang); }
            return body;
        }
    },
    [SFNodeType.poetry]: class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.poetry>) {
            const content = await this.engine.renderContent(node);
            const body = <blockquote data-type={node.attrs?.type} id={node.attrs?.id}>
                {...content}
            </blockquote>;
            if (node?.attrs?.lang) { body.setAttribute('lang', node.attrs.lang); }
            return body;
        }
    },
    [SFNodeType.note]: class extends NodeRenderer<HTMLElement[], SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.note>) {
            // we do not render notes in the output
            return [];
        }
    }
};

export default renderers;
