import { TSX } from '../../TSX';

import { DocumentNode, SFMarkType, SFNodeType } from '@sciflow/schema';

const markOrder: string[] = [SFMarkType.anchor, SFMarkType.bdi, SFMarkType.subscript, SFMarkType.superscript, SFMarkType.strong, SFMarkType.emphasis];
const sortMarks = (a?: string, b?: string) => (a ? markOrder.indexOf(a) : 0) - (b ? markOrder.indexOf(b) : 0);
const same = (a, b) => (a.marks || []).sort(sortMarks).join() == (b.marks || []).sort(sortMarks).join();
const markToNode = {
    [SFMarkType.bdi]: 'bdi',
    [SFMarkType.strong]: 'b',
    [SFMarkType.emphasis]: 'i',
    [SFMarkType.subscript]: 'sub',
    [SFMarkType.superscript]: 'sup',
    [SFMarkType.anchor]: 'a'
}

interface TextAuxiliaryNode extends DocumentNode<any> {
    parent?: TextAuxiliaryNode;
    text: string;
    marks?: { type: string; }[];
    content: TextAuxiliaryNode[];
}

/** Joins two text nodes if they are compatible
 * Only works if the next node contains all the marks of the previous.
*/
export const canJoin = (node: DocumentNode<SFNodeType.text>, next: DocumentNode<SFNodeType.text>) => {
    if (node?.type == SFNodeType.text && next?.type == SFNodeType.text) {
        const nodeMarks: string[] = (node.marks || []).map(({ type }) => type);
        const nextMarks: string[] = next.marks?.map(({ type }) => type) || [];
        // check that all marks from node are in next
        return nodeMarks.every(nodeMark => nextMarks.includes(nodeMark));
    } else {
        return false;
    }
};


/**
 * Renders a group of text node, respecting the order of marks (e.g. a,bdi on the outside) to create a DOM element.
 * If text nodes are unrelated (e.g. do not share marks) they are returned as individual array elements.
*/
export const mergeTextNodes = (nodes: DocumentNode<SFNodeType.text>[]): (HTMLElement | Text)[] => {
    /** Nests the auxiliary structure */
    const nest = (parent: TextAuxiliaryNode, child: TextAuxiliaryNode): void => {
        parent.content = [...(parent.content || []), child];
        child.parent = parent;
    }

    /**
     * internal helper: Serializes a list of text proxies into DOM elements.
     */
    const serialize = (list: TextAuxiliaryNode[], activeMarks: string[] = []): (HTMLElement | Text)[] => {
        let doms: (HTMLElement | Text)[] = [];
        for (let node of list) {
            // create a DOM elements from the nodes
            let content = domFromMarkedText(node.marks?.filter(m => !activeMarks.includes(m.type)), node.text);
            // append all further nodes contained in the text proxy
            if (node.content?.length > 0) {
                const siblings = serialize(node.content, [...activeMarks, ...(node.marks || [])?.map(m => m.type)]);
                if (content.nodeName === '#text') {
                    doms = [...doms, content, ...siblings];
                } else {
                    let n: ChildNode = content;
                    // find the inner most node (in case multiple mark tags were applied to the parent)
                    while (n.hasChildNodes()) {
                        const childElement = Array.from(n.childNodes).find(node => node.nodeType === 1 /*Node.ELEMENT_NODE*/);
                        if (childElement != undefined) { n = childElement; }
                        else { break; }
                    }
                    for (let child of siblings) { n.appendChild(child); }
                    doms = [...doms, content];
                }
            } else {
                doms = [...doms, content];
            }
        }

        return doms;
    }

    // copy nodes and make sure they all have a content array.
    const initial: TextAuxiliaryNode[] = JSON.parse(JSON.stringify(nodes))
        .map(n => ({ ...n, marks: (n.marks || []).sort(sortMarks), content: n.content || [], parent: undefined }));
    if (!nodes || nodes.length === 0) { return []; }
    const isInline = (node: DocumentNode<any>) => node.type === SFNodeType.text;

    let current: TextAuxiliaryNode | undefined;
    let rootList: TextAuxiliaryNode[] = [];
    let list: TextAuxiliaryNode[] = rootList;

    for (let node of initial) {
        if (!current) {
            // we're at the root
            list.push(node);
        } else if (canJoin(current as TextAuxiliaryNode, node)) {
            // add to current
            nest(current, node);
        } else {
            // go back down the list until we can join
            while (list !== rootList && !canJoin(current as TextAuxiliaryNode, node)) {
                current = current?.parent;
                list = current?.content || rootList;
            }
            list.push(node);
        }
        current = node;
        list = current.content;
    }
    return serialize(rootList);
}

/**
 * Creates a DOM fragment from the marks on a text string.
 * e.g. : test with marks em, strong would be <em><strong>test</strong></em>
 * Strings prefixxed with four spaces will get wrapped into a span to preserve white-space
 */
export const domFromMarkedText = (marks: { type: string; attrs?: any; }[] = [], text?: string): (HTMLElement | Text) => {
    let fragment: HTMLElement | undefined;
    if (!text) { text = ''; }
    if (text && / {4}/.test(text)) { fragment = TSX.createElement('span', { class: 'pre-wrap', style: 'white-space: pre-wrap;' }); }
    if (marks.length === 0) { return TSX.createTextNode(text); }
    let curr = fragment;
    for (let mark of marks) {
        if (!markToNode[mark.type]) {
            throw new Error('Unknown mark type ' + mark?.type);
        }
        const el = TSX.createElement(markToNode[mark.type], {});
        if (mark.type === SFMarkType.anchor && mark.attrs) { el.setAttribute('href', mark.attrs?.href); }
        if (!fragment || !curr) { fragment = el; } else { curr.append(el); }
        curr = el;
    }

    curr !== undefined ? curr.append(TSX.createTextNode(text)) : TSX.createTextNode(text);
    return fragment as HTMLElement | Text;
}
