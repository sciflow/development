// this is duplicated for v1 compatibility and renderers are copied with webpack before libraries are built and linked
// usage of interface.ts will have issues due to libraries not being built yet
// TODO: refactor GenericConfiguration

export interface GenericConfiguration<ConfigurationSpecSchema> {
  /** The component name (unique across all components) */
  kind: string;
  /**
   * Meta data that can be used to uniquely identify instances of a component inside a configuration.
   */
  metadata?: {
    name?: string;
    /** A human readable title */
    title?: string;
    /** The instance description with details for users */
    description?: string;
    /** Related to a configuration kind */
    group?: string;
    [key: string]: any;
  };
  page?: string;
  /** A placement if this configuration should be used in a different document position */
  subPage?: string;
  /**
   * The configuration values.
   */
  spec: ConfigurationSpecSchema;
  /**
   * Optional translations for the values provided in the spec.
   */
  translations?: {[locale: string]: ConfigurationSpecSchema};
  /** The locales supported by a component (leave empty for all) */
  locales?: string[];
  /** The export runners the configuration is meant for (e.g. princexml, html, epub) */
  runners?: string[];
  disabled?: boolean;
  ui?: {
    /** Who the UI should be shown to (empty array for no-one) */
    showTo: ('author' | 'editor' | string)[];
  };
  [key: string]: any;
}