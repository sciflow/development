
import { DocumentData } from "../interfaces";

import { DocumentNode, DocumentSnapshot, DocumentSnapshotImage, PlacementOption, rootDoc, SFNodeType } from '@sciflow/schema';
import { extractHeadingOfType } from '../helpers';

/**
 * Creates a part document with the most important part information in the root node.
 */
export const mapDoc = (p, placements?: { [placement: string]: PlacementOption }) => {
    return {
        ...p.document,
        attrs: {
            label: p.label,
            ...p.document.attrs,
            id: p.document.attrs?.id || p.partId || p.id,
            numbering: p.numbering || p.options?.numbering || p.document?.attrs?.numbering,
            direction: p.options?.direction || p.document?.attrs?.direction,
            role: p.role || p.document?.attrs?.role,
            schema: p.schema || p.document?.attrs?.schema,
            locale: p.locale || p.document?.attrs?.locale,
            type: p.type || p.document?.attrs?.type,
            partId: p.partId,
            skipToc: p.skipToc || p.document?.attrs?.skipToc,
            placement: p.placement || p.options?.placement || p.document?.attrs?.placement,
            pageBreak: p.pageBreak || p.options && p.options.pageBreak || p.document?.attrs?.pageBreak,
            placementNumbering: placements ? placements[p.placement || p.options?.placement || 'body']?.numbering : p.document?.attrs?.placementNumbering,
            standalone: p.standalone
        }
    } as DocumentNode<rootDoc>;
};


/**
 * Creates the legacy snapshot format.
 */
export const documentDataFromSnapshot = async (exportData: DocumentSnapshot, { files, locale, configurations }): Promise<DocumentData> => {
    // create a deep copy for legacy components not to influence the rest
    exportData = JSON.parse(JSON.stringify(exportData));

    const titlePart =  exportData.parts.find(p => p.type === 'title') || (exportData.title && typeof exportData.title === 'object' ? exportData.title : undefined);
    const title = (titlePart?.document ? extractHeadingOfType(titlePart.document, undefined, SFNodeType.heading) : undefined) || 'Untitled document';
    const subtitle = (titlePart?.document ? extractHeadingOfType(titlePart.document, undefined, SFNodeType.subtitle) : undefined);

    return {
        documentId: exportData.documentId,
        title,
        subtitle,
        locale,
        titlePart,
        parts: exportData.parts,
        index: exportData.parts.filter(p => p.type !== 'title').map(p => p.id),
        metaData: exportData?.metaData || {},
        tables: undefined, // await generateListings(exportData.parts.map(p => mapDoc(p)).filter(p => p.attrs.role !== 'coverpage'), undefined, undefined, configurations),
        images: (files || []).filter(file => file.type === 'image') as DocumentSnapshotImage[],
        files,
        authors: exportData.authors || [],
        references: exportData.references || []
    }
}