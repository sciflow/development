import { expect } from 'chai';
import { describe, it } from 'mocha';
import { matchLabel } from './generateListings';

import { GenericEnvironment } from './renderers/figure/figure.schema';

describe('Label matchers', async () => {

    it('Matches a table label', async () => {
        const conf: GenericEnvironment = {
            breakAfterLabel: false,
            captionSide: 'below',
            align: { caption: 'center', content: 'title' },
            countWithoutCaption: false,
            label: 'Table',
            labelSuffix: ':',
            labelWeight: 'bold',
            margin: '1rem auto',
            referenceLabel: 'tbl.',
            suppressLabel: false
        };

        const labels = [
            { text: 'Table 2.a.1: Test Lorem ipsum dolor sit amet, consetetur sadipscing elitr', label: 'Table', number: '2.a.1' },
            { text: 'Table 1.3: Another Example', label: 'Table', number: '1.3' },
            { text: 'Table 7.c: Lorem Ipsum', label: 'Table', number: '7.c' },
            { text: 'Table 16: Short Text', label: 'Table', number: '16' },
            { text: 'Table 8.d.7: Random Text with :', label: 'Table', number: '8.d.7' },
            { text: 'Table 3.2: Some Content', label: 'Table', number: '3.2' },
            { text: 'Table 9.e.1: The Last Table', label: 'Table', number: '9.e.1' }
        ];

        const notLabels = [
            { text: 'Random text without a label', label: '', number: '' },
            { text: 'Some text with a number 123', label: '', number: '' },
            { text: 'Text with multiple numbers 1.2.3.4.5', label: '', number: '' },
            { text: 'No label here either', label: '', number: '' },
            { text: 'Only a number here 45.a', label: '', number: '' }
        ];

        for (let labelTest of labels) {
            const result = matchLabel(labelTest.text, conf);
            expect(result?.customLabel).to.equal(labelTest.label);
            expect(result?.customNumber).to.equal(labelTest.number);
        }

        for (let labelTest of notLabels) {
            const result = matchLabel(labelTest.text, conf);
            expect(result).to.be.undefined;
        }
    });
});
