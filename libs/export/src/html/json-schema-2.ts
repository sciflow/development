import { readFile } from 'fs/promises';
import { sync } from 'glob';
import { JSONSchema7 } from 'json-schema';
import { basename, extname, join, resolve } from 'path';
import { ModuleKind, ScriptTarget } from 'typescript';
import * as TJS from 'typescript-json-schema';
import { createLogger, format, transports } from 'winston';
import { GenericConfiguration } from '../interfaces';
import { decamelize, dfs, getConfigName, kebabToPascal, slugify } from '../utils/helpers';
import { VariantConfiguration } from './components/variant/variant.schema';
import { patchConfigurations, patchSchema } from './json-schema-patch-2';
import { existsSync, readdirSync } from 'fs';
const { LOG_LEVEL = 'info' } = process.env;
const logger = createLogger({
  level: LOG_LEVEL,
  format: format.json(),
  defaultMeta: { service: 'schema' },
  transports: [new transports.Console({ level: LOG_LEVEL })],
});

export const validAtKeywords = [
  '_ui_icon',
  '_ui_widget',
  '_ui_tag',
  '_readme',
  '_ui_group',
  '_ui_hide',
];

export interface ComponentSchema extends JSONSchema7 {
  /** The component that provided the schema */
  component?: {
    kind: string;
    metadata?: {
      /** A unique and machine readable name */
      name?: string;
      /** A human readable title (will be sluggified if no name exists and used as a name) */
      title?: string;
      [key: string]: any;
    };
    locales?: string[];
    page?: string;
    runners?: string[];
  };
}

/**
 * Common compiler options for schema generation
 */
const baseCompilerOptions: TJS.CompilerOptions = {
  strictNullChecks: true,
  allowUnionTypes: true,
  target: ScriptTarget.ES2020,
  lib: ['es6', 'ES2021', 'dom'],
  include: ['./components'],
  exclude: ['./node_modules/*', '**/node_modules/*', 'plugins', 'app', 'dist', 'test'],
  module: ModuleKind.CommonJS,
  skipLibCheck: true,
  noEmit: true,
  experimentalDecorators: true,
  emitDecoratorMetadata: true,
  allowUnusedLabels: true,
  paths: {
    '@sciflow/*': ['../../../../libs/*/src'],
  },
};

/**
 * Interface for schema generation options
 */
interface SchemaGenerationOptions {
  noExtraProps: boolean;
  schemaType: 'configuration' | 'node-schema';
  includeGenericConfig?: boolean;
}

/**
 * Gets path of the components.
 * Latest entry of the same component schema takes priority
 * @param {string[]} kinds - desired kinds to be get path
 * @param {string} componentPath - path to the components (assuming components are in schema.ts)
 * @returns Map<kind: string, path: string>
 * @example
 * // returns
 * Map {
 *  'Page': 'path/to/page.schema.ts'
 * }
 */
export const getComponentPaths = (
  kinds: string[],
  componentPath: string,
  filterFiles?: (filename: string) => boolean,
): Map<string, string> => {
  let files: string[] = sync(`${componentPath}/**/*.schema.@(ts|tsx)`); // implicit descending alphabetical order

  if (files?.length === 0) {
    logger.warn('Could not find any components', {
      componentPath,
      search: `${componentPath}/**/*.schema.@(ts|tsx)`,
    });
  }

  let filteredFiles: string[] = files.filter((path) => {
    for (let kind of kinds) {
      if (path.includes(kind.toLowerCase())) {
        return true;
      }
      const slug = slugify(kind);
      if (slug && path.includes(slug.toLowerCase())) {
        return true;
      }
      const slug2 = decamelize(kind);
      if (slug2 && path.includes(slug2.toLowerCase())) {
        return true;
      }
    }
    return false;
  });

  if (filterFiles) {
    filteredFiles = filteredFiles.filter(filterFiles);
  }

  filteredFiles.sort((a, b) =>
    basename(b).localeCompare(basename(a), 'en', { sensitivity: 'base' }),
  ); // explicit descending alphabetical sort base on dirname

  const kindtoFileMap = new Map<string, string>();

  for (let filePath of filteredFiles) {
    let fileName = basename(filePath, extname(filePath));
    let kind = kebabToPascal(fileName.replace('.schema', ''));

    // paths in descending alphabetical order - first path in is the latest version
    if (!kindtoFileMap.has(kind)) {
      kindtoFileMap.set(kind, filePath);
    }
  }

  return kindtoFileMap;
};

/**
 * Detects circular dependencies in a schema.
 * @param {object} schema - The schema to check
 * @returns {boolean} True if circular dependencies are detected, otherwise false
 */
export const detectCircularDependencies = (schema: object): boolean => {
  const visited: { [key: string]: boolean } = {};
  const stack: { [key: string]: boolean } = {};

  const definitions = schema['definitions'];
  if (definitions) {
    for (const definition in definitions) {
      if (definitions.hasOwnProperty(definition)) {
        if (!visited[definition]) {
          if (dfs(schema, definition, visited, stack)) {
            return true;
          }
        }
      }
    }
  }

  return false;
};

/**
 * Deferences the pointer $ref that exists in the provided schema.
 * Given schemas containing a definitions property is removed.
 * @param {object} schema - The schema to dereference
 * @param {object} fullSchema - The full schema containing all definitions
 * @returns {object} The dereferenced schema
 * @throws {Error} If circular dependencies are detected
 */
export const dereferenceSchema = (schema: object, fullSchema: object) => {
  const dereference = (schema: any, fullSchema: object) => {
    if (schema['$ref']) {
      const clonedSchema = structuredClone(schema);
      const refSchema =
        fullSchema['definitions'][clonedSchema['$ref'].replace('#/definitions/', '')];
      const newSchema = { ...refSchema, ...clonedSchema };
      delete newSchema['$ref'];
      return dereference(newSchema, fullSchema);
    }

    const { definitions: _definitions, ...rest } = schema;

    let newSchema = { ...rest };

    if (newSchema['properties']) {
      newSchema = {
        ...newSchema,
        properties: Object.keys(newSchema['properties']).reduce((acc, key) => {
          acc[key] = dereference(newSchema['properties'][key], fullSchema);
          return acc;
        }, {}),
      };
    }

    if (newSchema['items']) {
      newSchema = {
        ...newSchema,
        items: dereference(newSchema['items'], fullSchema),
      };
    }

    if (newSchema['additionalProperties']) {
      newSchema = {
        ...newSchema,
        additionalProperties: dereference(newSchema['additionalProperties'], fullSchema),
      };
    }

    ['allOf', 'anyOf', 'oneOf'].forEach((key) => {
      if (newSchema[key]) {
        newSchema[key] = newSchema[key].map((item) => dereference(item, fullSchema));
      }
    });

    return newSchema;
  };

  return dereference(schema, fullSchema);
};

/**
 * Resolves $ref pointers inside of definitions.
 * @param {object} schema - The schema containing definitions
 * @returns {object} The schema with resolved definitions
 * @throws {Error} If circular dependencies are detected
 */
export const dereferenceDefinitions = (schema: object): object => {
  const clonedSchema = structuredClone(schema);

  if (detectCircularDependencies(clonedSchema)) {
    throw new Error('Circular dependency detected.');
  }

  if (clonedSchema['definitions']) {
    for (let key in clonedSchema['definitions']) {
      clonedSchema['definitions'][key] = dereferenceSchema(
        clonedSchema['definitions'][key],
        clonedSchema,
      );
    }
  }

  return clonedSchema;
};

export const shapeWithGenericConfiguration = (
  schema: JSONSchema7,
  genericConfigurationSchema: JSONSchema7,
) => {
  const modifiedSchema = structuredClone(genericConfigurationSchema);
  const properties = modifiedSchema?.['properties'];

  if (properties && properties?.['spec']) {
    properties['spec'] = structuredClone(schema);
  }

  if (properties && properties?.['translations']?.['additionalProperties']) {
    properties['translations']['additionalProperties'] = structuredClone(schema);
  }

  delete modifiedSchema['$id'];
  delete modifiedSchema['definitions'];
  modifiedSchema.additionalProperties = false;

  return modifiedSchema;
};

// post-processing to comply with json schema
export const replaceUnionTypes = (schema: any): any => {
  if (Array.isArray(schema)) {
    return schema.map(replaceUnionTypes);
  } else if (typeof schema === 'object' && schema !== null) {
    // Check specifically for ["string", "number"]
    if (
      Array.isArray(schema.type) &&
      schema.type.length === 2 &&
      schema.type.includes('string') &&
      schema.type.includes('number')
    ) {
      schema.anyOf = schema.type.map((type: string) => ({ type }));
      delete schema.type;
    }
    for (const key in schema) {
      if (schema.hasOwnProperty(key)) {
        schema[key] = replaceUnionTypes(schema[key]);
      }
    }
  }
  return schema;
};

/**
 * Helper function to generate a schema with common logic
 */
const generateSchemaHelper = async (
  kind: string,
  file: string,
  baseUrl: string,
  options: SchemaGenerationOptions,
): Promise<object> => {
  const compilerOptions = {
    ...baseCompilerOptions,
    baseUrl,
  };

  const settings: TJS.PartialArgs = {
    tsNodeRegister: true,
    noExtraProps: options.noExtraProps,
    validationKeywords: validAtKeywords,
  };

  let program;
  try {
    program = TJS.getProgramFromFiles([resolve(file)], compilerOptions);
  } catch (e: any) {
    logger.error('Could not get program files', { message: e.message, compilerOptions });
    throw new Error('Could not get program files: ' + e.message + ' / ' + file);
  }

  try {
    let schemaFragment = TJS.generateSchema(program, `${kind}Configuration`, settings);

    if (!schemaFragment) {
      return {};
    }

    schemaFragment = replaceUnionTypes(schemaFragment);

    let dereferencedSchema;

    // Resolve any $refs that may have been generated
    if (schemaFragment?.['definitions']) {
      const resolvedDefinitions = dereferenceDefinitions(schemaFragment);
      dereferencedSchema = dereferenceSchema(resolvedDefinitions, schemaFragment);
    } else {
      dereferencedSchema = { ...schemaFragment };
    }

    // Add generic configuration if needed
    if (options.includeGenericConfig) {
      const genericConfigurationSchema = await getGenericConfigurationSchema(kind);
      if (genericConfigurationSchema && Object.keys(genericConfigurationSchema).length > 0) {
        dereferencedSchema = shapeWithGenericConfiguration(
          dereferencedSchema,
          genericConfigurationSchema,
        );
      }
    }

    dereferencedSchema = {
      $id: `/template/${options.schemaType}/${kind}.json`,
      ...dereferencedSchema,
    };

    return dereferencedSchema;
  } catch (e: unknown) {
    logger.error('Could not generate schema', {
      message: e?.['message'],
      file,
    });
    throw new Error('Could not generate schema: ' + e?.['message'] + ' / ' + file);
  }
};

/**
 * Gets the generic configuration schema
 */
const getGenericConfigurationSchema = async (kind: string): Promise<object | null> => {
  const schemaPath = resolve(
    __dirname,
    '../../../../',
    'dist/libs/export/html/renderers/generic-configuration/generic-configuration.schema.json',
  );

  try {
    const genericConfigurationContents = await readFile(schemaPath, 'utf8');
    return JSON.parse(genericConfigurationContents);
  } catch (e: any) {
    if (kind !== 'Generic') {
      let files = sync(
        `${__dirname}/@(components|renderers)/**/generic-configuration.schema.@(ts|tsx)`,
      );
      return await generateSchema('Generic', files[0]);
    }
    return null;
  }
};

/**
 * Generates a resolved JSON schema for a given component kind.
 * Definitions that may be generated when using composition interfaces are dereferenced.
 * (i.e.) The value of the referenced external location is copied to the $ref pointer's location and the pointer is removed.
 * @param {string} kind - The kind of the component
 * @param {string} file - The file path of the component schema
 * @param {string} [baseUrl=__dirname] - The base URL for the component
 * @returns {object|null} The generated schema fragment or null if generation fails
 */
export const generateSchema = async (
  kind: string,
  file: string,
  baseUrl: string = __dirname,
): Promise<object> => {
  return generateSchemaHelper(kind, file, baseUrl, {
    noExtraProps: true,
    schemaType: 'configuration',
    includeGenericConfig: true,
  });
};

/**
 * Generates a node schema for a given component kind.
 */
export const generateNodeSchema = async (
  kind: string,
  file: string,
  baseUrl: string = __dirname,
): Promise<object> => {
  return generateSchemaHelper(kind, file, baseUrl, {
    noExtraProps: false,
    schemaType: 'node-schema',
    includeGenericConfig: false,
  });
};

/**
 * Sets custom values from a given object to the 'default' key for the given schema key definition.
 *
 * @param {object} schema - The JSON schema object where default values need to be set.
 * @param {object} values - An object containing key-value pairs to set as default values in the schema.
 */
export const setDefaultValues = (schema: any, values: object): object => {
  let newSchema = structuredClone(schema);

  for (let key in values) {
    if (newSchema?.properties?.[key]) {
      if (typeof values[key] === 'object' && !Array.isArray(values[key])) {
        values[key] === null
          ? (newSchema.properties[key].default = values[key])
          : (newSchema.properties[key] = setDefaultValues(newSchema.properties[key], values[key]));
      } else {
        newSchema.properties[key].default = values[key];
      }
    } else if (newSchema?.items?.properties?.[key]) {
      if (typeof values[key] === 'object' && !Array.isArray(values[key])) {
        newSchema.items.properties[key] = setDefaultValues(
          newSchema.items.properties[key],
          values[key],
        );
      } else {
        newSchema.items.properties[key].default = values[key];
      }
    } else if (newSchema?.additionalProperties) {
      if (values[key] !== null) {
        newSchema.properties = newSchema.properties || {};
        if (typeof values[key] === 'object' && !Array.isArray(values[key])) {
          newSchema.properties[key] = structuredClone(newSchema.additionalProperties);
          newSchema.properties[key].default = values[key];
          if (newSchema.additionalProperties.properties) {
            newSchema.properties[key] = setDefaultValues(
              newSchema.additionalProperties,
              values[key],
            );
          }
        } else {
          newSchema.properties[key] = {
            ...structuredClone(newSchema.additionalProperties),
            default: values[key],
          };
        }
      }
    }
  }
  return newSchema;
};

/**
 * Build and returns a template from a provided configuration. Values defined in the template will override default values of the pre-generated component schema. Locale values will also take high priority than values defined in a spec.
 *
 * @param {GenericConfiguration<any>[]} configurations - Array of template configurations.
 * @param {string} [locale] - Optional locale to apply translations from the configuration.
 * @param {string[]} [variants] - Optional array of variant names to filter configurations.
 * @param {string} [componentPath=join(__dirname)] - The path where component schemas are located.
 * @returns {Promise<{schema: ComponentSchema, configurations: GenericConfiguration<unknown>[],  prosemirrorNodeSchemas: { [key: string]: JSONSchema7 }}>} -
 * A Promise that resolves with an object containing:
 * - `schema`: The built template schema (as an object).
 * - `configurations`: Processed configurations after applying variants and patches or original configurations if no variants or patches are applied.
 * - `prosemirrorNodeSchemas`: A collection of key-value pairs of node schemas.
 * @throws {Error} - Throws an error if there is an issue reading schema files.
 */
export const buildTemplateSchema = async (
  configurations: GenericConfiguration<any>[],
  locale?: string,
  variants?: string[],
  componentPath: string = join(__dirname),
): Promise<{
  schema: ComponentSchema;
  configurations: GenericConfiguration<unknown>[];
  prosemirrorNodeSchemas: { [key: string]: JSONSchema7 };
}> => {
  const templateDefinitions: { [name: string]: any } = {}; // collection of key-value pairs of kind to component schema
  const schemaDefinitions: { [key: string]: object } = {};
  const variantConfigurations: GenericConfiguration<VariantConfiguration>[] = [];

  const templateId =
    configurations.find((config) => config.kind === 'Configuration')?.spec?.slug || 'none';

  if (variants) {
    variants = variants.map((variant) => slugify(variant) || 'default');

    variantConfigurations.push(
      ...configurations.filter((config): config is GenericConfiguration<VariantConfiguration> => {
        const configName = config?.metadata?.name || config?.metadata?.title || '';
        const slugifiedConfigName: string = slugify(configName) || 'default';
        return config.kind === 'Variant' && variants!.includes(slugifiedConfigName);
      }),
    );
  }

  const patchedConfigurations = patchConfigurations(configurations, variantConfigurations);

  for (let configuration of patchedConfigurations) {
    const kind = decamelize(configuration.kind);

    if (!schemaDefinitions[kind]) {
      const schemaGlob = `${componentPath}/components/**/*.schema.json`;
      let files: string[] = sync(schemaGlob);
      if (!existsSync(componentPath)) {
        debugger;
        throw new Error('Component directory does not exist: ' + componentPath);
      }
      if (!readdirSync(componentPath).some((v) => v == 'components')) {
        debugger;
        throw new Error('Component directory does not exist in ' + componentPath);
      }
      if (files?.length === 0) {
        debugger;
        throw new Error('No components found in ' + schemaGlob);
      }

      files.sort((a, b) => basename(b).localeCompare(basename(a), 'en', { sensitivity: 'base' })); // explicit descending alphabetical sort base on dirname
      const file = files.find((f) => f.endsWith(`/${kind}.schema.json`));

      if (!file) {
        // we ignore missing schemas as long as any schemas exist in the component dir
        continue;
      }

      try {
        const schemaContents = await readFile(file, 'utf8');
        const {
          $schema: schemaVersion,
          properties: { spec, ...remainingProperties },
          title,
          description,
          ...rest
        } = JSON.parse(schemaContents);
        schemaDefinitions[kind] = spec;
      } catch (e: any) {
        logger.error('Could not get program files', { e });
        throw new Error('Could not read program files: ' + e.message + ' / ' + files[0]);
      }

      if (!schemaDefinitions[kind]) {
        throw new Error(`Empty definition for ${files[0]}`);
      }
    }

    let componentSchema = structuredClone(schemaDefinitions[kind]);

    if (configuration?.spec && Object.keys(configuration.spec).length > 0) {
      // 1st pass-through to apply defaults from the spec
      const patchedProperties = setDefaultValues(
        componentSchema as object,
        configuration?.spec as object,
      );
      componentSchema = {
        ...componentSchema,
        ...patchedProperties,
      };
    }

    if (locale && configuration?.translations?.[locale]) {
      // 2nd pass-through to apply defaults from the translations
      const patchedTranslations = setDefaultValues(
        componentSchema as object,
        configuration?.translations[locale] as object,
      );
      componentSchema = {
        ...componentSchema,
        ...patchedTranslations,
      };
    }

    const page = configuration?.page;
    const name = getConfigName({ page, ...configuration });
    configuration = {
      ...configuration,
      metadata: {
        name, // only write the name if it was not defined
        ...(configuration.metadata || {}),
      },
    };

    templateDefinitions[name] = {
      ...(componentSchema ? componentSchema : {}),
      title:
        configuration.metadata?.title ||
        componentSchema?.['title'] + (page ? ' for ' + page : '') ||
        kind + (page ? ' for ' + page : ''),
      description: configuration.metadata?.description || componentSchema?.['description'],
      // TODO: component should not be set in the schema
      component: {
        kind: configuration.kind,
        metadata: configuration.metadata,
        locales: configuration.locales,
        page: configuration.page,
        runners: configuration.runners,
      },
      ui: configuration.ui,
      $id: `/template/schema/${templateId}-${name?.toLowerCase()}`,
    };
  }

  const title =
    'Template configuration' +
    (variants && variants.length > 0 ? ' (' + variants.join(', ') + ')' : '');

  const metaDataSchemaForComponents: ComponentSchema = {
    type: 'object',
    title,
    properties: {
      ...templateDefinitions,
    },
  };

  const patchedSchema = patchSchema(metaDataSchemaForComponents, variantConfigurations);
  const prosemirrorNodeSchemas = await getProseMirrorNodeSchemas(componentPath);
  return { schema: patchedSchema, configurations: patchedConfigurations, prosemirrorNodeSchemas };
};

/**
 * Retrieves JSONSchema7 definitions to generate the UI for editing Prosemirror Node Attributes.
 *
 * Searches in sub-directory `/node-schemas` for `*.schema.json` files containing valid JSONSchema7.
 * Base name of each file is used as the key in the resulting object mapping.
 *
 * @param {string} componentPath - The base directory path of where `node-schemas` and its components are stored.
 * @returns {Promise<{ [key: string]: JSONSchema7 }>} A promise that resolves to an object mapping each node's
 * base name to its corresponding JSONSchema7 object. Returns an empty object if the `node-schemas` directory
 * does not exist.
 */
export const getProseMirrorNodeSchemas = async (
  componentPath: string,
): Promise<{ [key: string]: JSONSchema7 }> => {
  const schemaPath = join(componentPath, 'node-schemas');

  if (!existsSync(schemaPath)) {
    logger.warn('Node schemas path does not exist', { schemaPath });
    return {};
  }

  const nodeSchemas: { [key: string]: JSONSchema7 } = {};
  try {
    const schemaFiles = sync(`${schemaPath}/**/*.schema.json`);

    for (const file of schemaFiles) {
      const baseName = file.split('/').pop()?.replace('.schema.json', '');

      if (!baseName) {
        logger.error('Could not get base name from file path', { file });
        continue;
      }

      try {
        const schemaContent = await readFile(file, 'utf-8');
        const schema = JSON.parse(schemaContent) as JSONSchema7;
        nodeSchemas[baseName] = schema;
      } catch (error: any) {
        logger.error('Error reading schema file', {
          file,
          error: error.message,
        });
      }
    }

    return nodeSchemas;
  } catch (e: any) {
    logger.error('Could not get prosemirror node schema', { message: e.message });
    return {};
  }
};
