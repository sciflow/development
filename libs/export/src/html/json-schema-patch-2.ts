import Ajv from 'ajv';
import {applyPatch, JsonPatchError} from 'fast-json-patch';
import {GenericConfiguration} from '../interfaces';
import {
  isValidOperation,
  jsonPatchSchema,
  matchTargetSchema,
  matchTargetYaml,
} from '../json-schema-patch';
import {VariantConfiguration} from './components/variant/variant.schema';
import {ComponentSchema} from './json-schema-2';

const createMissingParentPaths = (
  obj: GenericConfiguration<unknown>,
  path: string,
  value?: unknown,
): GenericConfiguration<unknown> => {
  const clonedObject: GenericConfiguration<unknown> = structuredClone(obj);
  const pathSegments = path.split('/').slice(1);
  let objectPointer: any = clonedObject;

  // 1. construct path up to second last segment
  for (let i = 0; i < pathSegments.length - 1; i++) {
    const segment = pathSegments[i];
    const nextSegment = pathSegments[i + 1];
    const isNextSegmentArrayIndex = !isNaN(Number(nextSegment)); // true = traverse array || false = traverse object

    if (!(segment in objectPointer)) {
      objectPointer[segment] = isNextSegmentArrayIndex ? [] : {};
    } else if (Array.isArray(objectPointer[segment])) {
      // We should check if it's within they array boundaries otherwise - an out of array boundary might not be nice to deal with.
      if (isNextSegmentArrayIndex) {
        const index = Number(nextSegment);
        if (index > objectPointer[segment].length || index < 0) {
          throw new Error(`Array index ${index} is out of bounds`);
        }
      }
    } else if (typeof objectPointer[segment] !== 'object' || objectPointer[segment] === null) {
      throw new Error(`Cannot create path, '${segment}' is not an object`);
    }

    objectPointer = objectPointer[segment];
  }

  const lastSegment = pathSegments[pathSegments.length - 1]; // 2. construct the last path segment
  if (Array.isArray(objectPointer)) {
    const index = Number(lastSegment);
    if (!isNaN(index) && (index >= objectPointer.length || index < 0)) {
      throw new Error(`Array index ${index} is out of bounds`);
    }
  }

  if (value !== undefined) {
    objectPointer[lastSegment] = value;
  }

  return clonedObject;
};

/**
 * Applies configuration patches from variants to base configurations in a template system.
 * Handles only 'conf' type patches that directly modify configuration settings rather
 * than schema defaults. The function processes variants sequentially, validating each
 * patch against JSON Patch schema, creating missing parent paths as needed, and applying
 * standard JSON Patch operations (add, replace, remove).
 *
 * @param configurations Base configurations to be modified, following GenericConfiguration
 *                      structure with kind and spec properties.
 * @param variants Variant configurations containing patches to be applied. Each variant
 *                can have multiple patches targeting different configurations.
 * @returns Array of patched configurations. Returns original configurations if no
 *          variants are provided or no patches are applicable.
 * @throws {Error} When patch validation fails, patch operation is invalid, patch
 *                 application fails, or no matching configuration is found.
 * @remarks Part of the template variant system for template customization. Uses
 *          structuredClone for immutability.
 */
export const patchConfigurations = (
  configurations: GenericConfiguration<unknown>[],
  variants: GenericConfiguration<VariantConfiguration>[],
): GenericConfiguration<unknown>[] => {
  if (!variants.length) return configurations;

  const ajv = new Ajv({allErrors: true, strict: true, allowUnionTypes: true});
  const validate = ajv.compile(jsonPatchSchema);

  const updatedConfigurations = structuredClone(configurations);

  try {
    for (const variant of variants) {
      for (const {patch, target, type} of variant.spec?.patches || []) {
        if (type !== 'conf') continue; // Only process 'conf' type patches

        const valid = validate(patch);
        if (!valid) {
          throw new Error('Invalid patch: ' + JSON.stringify({patch}));
        }

        let match = updatedConfigurations.find((c) => matchTargetYaml(target, c));
        if (!match) continue;
        const matchIndex = updatedConfigurations.findIndex((c) => matchTargetYaml(target, c));
        if (matchIndex === -1) continue;

        for (const op of patch) {
          if (!isValidOperation(op)) throw new Error('Invalid operation ' + JSON.stringify(op));

          if (op.op === 'add' || op.op === 'replace') {
            match = createMissingParentPaths(
              match,
              op.path,
              op.op === 'replace' ? op.value : undefined,
            );
          }
        }

        try {
          // needed to be undefined to not throw an error for "removal" of a non-existing paths
          const result = applyPatch(match, patch, true, false);
          if (result.newDocument) {
            updatedConfigurations[matchIndex] = result.newDocument;
          }
        } catch (e) {
          if (e instanceof JsonPatchError) {
            const operation = e?.operation;
            const op = operation?.op;
            const path = operation?.path;
            const variantName =
              variant.metadata?.name || variant.metadata?.title || 'Unknown variant';
            const configurationName = match.kind || 'Unknown configuration';
            const errorDescription = e?.name;

            const errorMessage = `Unable to apply patch to configuration "${configurationName}" in variant "${variantName}": ${errorDescription} Operation "${op}" failed at path "${path}".`;

            throw new Error(errorMessage);
          }

          const errorMessage = `An error occurred while applying the patch to configuration "${match.kind}": ${e?.['message']}`;
          throw new Error(errorMessage);
        }
      }
    }
    return updatedConfigurations;
  } catch (e) {
    throw new Error(
      'Failed to patch the configurations with the following variants: "' +
        variants.map((v) => v.metadata?.name || v.metadata?.title).join() +
        '" with the following: ' +
        e?.['message'],
    );
  }
};

/**
 * Processes variants sequentially to apply 'default' type patches to matching schema
 * components, validating each patch against JSON Patch schema and updating component
 * properties while maintaining schema structure integrity. Unlike configuration patches,
 * these modify the schema defaults rather than specific configuration values.
 *
 * @param schema The base component schema containing properties and their
 *              configurations. Must follow ComponentSchema structure with a
 *              properties object containing component configurations.
 * @param variants Array of variant configurations containing default patches.
 *                 Each variant can contain multiple patches targeting different
 *                 schema components.
 * @returns A new ComponentSchema with patched default values. Returns original
 *          schema if no variants are provided or no patches are applicable.
 * @throws {Error} When patch validation fails, operation is invalid, or patch
 *                 application fails. Error includes variant and component context.
 */
export const patchSchema = (
  schema: ComponentSchema,
  variants: GenericConfiguration<VariantConfiguration>[],
): ComponentSchema => {
  if (!variants.length) return schema;

  const ajv = new Ajv({allErrors: true, strict: true, allowUnionTypes: true});
  const validate = ajv.compile(jsonPatchSchema);

  const updatedSchema = structuredClone(schema);
  const properties = updatedSchema.properties;

  if (!properties) return updatedSchema;

  let schemaConfigurations = Object.keys(properties).map((key) => ({
    key,
    configuration: properties[key],
  }));

  try {
    for (const variant of variants) {
      for (const {patch, target, type} of variant.spec?.patches || []) {
        // Only process 'default' type patches
        if (type !== 'default') continue;

        const valid = validate(patch);
        if (!valid) {
          throw new Error('Invalid patch: ' + JSON.stringify({patch}));
        }

        let match = schemaConfigurations.find((c) => matchTargetSchema(target, c.configuration));
        if (!match) continue;

        for (const op of patch) {
          if (!isValidOperation(op)) throw new Error('Invalid operation ' + JSON.stringify(op));
        }

        try {
          const result = applyPatch(match.configuration, patch, true, false);
          if (result.newDocument) {
            properties[match.key] = result.newDocument;
            schemaConfigurations = Object.keys(properties).map((key) => ({
              key,
              configuration: properties[key],
            }));
          }
        } catch (e) {
          if (e instanceof JsonPatchError) {
            const errorMessage = {
              message: `Unable to apply patch due to: ${e?.name}`,
              configuration: match.key,
              variant: variant.metadata?.name || variant.metadata?.title,
              operation: e?.operation,
            };
            throw new Error(JSON.stringify(errorMessage, null, 2));
          }

          const errorMessage = {
            message: 'Failed to apply the following patch to the schema',
            configuration: match.key,
            variant: variant.metadata?.name || variant.metadata?.title,
            error: e?.['message'],
          };
          throw new Error(JSON.stringify(errorMessage, null, 2));
        }
      }
    }

    return updatedSchema;
  } catch (e) {
    throw new Error(
      'Failed to patch the schema with the following variants: "' +
        variants.map((v) => v.metadata?.name || v.metadata?.title).join() +
        '" with the following:\n' +
        e?.['message'],
    );
  }
};
