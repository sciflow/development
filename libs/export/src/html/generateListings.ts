
import { DocumentElement, DocumentTables, GenericConfiguration, SciFlowDocumentData } from "../interfaces";
import { generateToc, RenderingEngine } from "../renderers";

import { DocumentNode, isRootDoc, SFNodeType } from '@sciflow/schema';
import { extractCaptionOrTitle, extractHeadingOfType, getTextContent } from '../helpers';

import { Environment } from './components/environment/environment.component';
import { EnvironmentConfiguration } from './components/environment/environment.schema';
import { Typography } from './components/typography/typography.component';
import { TypographyConfiguration } from './components/typography/typography.schema';
import { GenericEnvironment } from './renderers/figure/figure.schema';

export interface PartFileMap {
    [partId: string]: string;
}

/** Generates listings of all figures (equations, ..), headings, and 
 * footnotes with their respective numbering
 * @param partId optional partId to be added to elements
 */
export const generateListings = async (
    content: DocumentNode<any> | DocumentNode<any>[],
    partId?: string,
    engine?: RenderingEngine<HTMLElement, SciFlowDocumentData>,
    configurations?: GenericConfiguration<any>[],
    /**
     * The heading level at which numbering startx (usually h1 or h2)
     */
    baseLevel = 1,
    /** Mapping of parts to export files. */
    partFileMap?: PartFileMap

): Promise<DocumentTables> => {
    if (!configurations && engine) { configurations = engine.data?.configurations; }
    if (content instanceof Array) {
        // create a virtual root document with all documents
        content = {
            type: null,
            content
        };
    }

    // alternative with performance benefits would be to set the objects to read-only
    // we do not want to modify the document
    content = JSON.parse(JSON.stringify(content)) as DocumentNode<any>;

    let labels: { [key: string]: DocumentElement<any>[] } = {
        table: [] as DocumentElement<any>[],
        image: [] as DocumentElement<any>[],
        code: [] as DocumentElement<any>[],
        equation: [] as DocumentElement<any>[]
    };

    // create simple defaults
    let environments = getDefaultEnvironments();

    /** at what level environment counters should be reset (0 means never) */
    let resetAt = 0;

    const typographyConfiguration: GenericConfiguration<TypographyConfiguration> | undefined = configurations?.find(conf => conf.kind === Typography.name);
    const defaultNumberingType = typographyConfiguration?.spec?.defaultNumberingStyle || 'decimal';;

    // if the engine (e.g. template schemas, etc.) are loaded we extract custom settingsfor the export
    if (engine) {
        const figureComponent = new Environment(engine);
        const configuration: EnvironmentConfiguration = figureComponent.getValues();
        // get additional environments
        environments = configuration.environments;
        resetAt = figureComponent.get('numbering.resetAt') ?? 0;
    }

    // extract labels from the environment configurations and create a listing for each
    if (environments) {
        for (let env of Object.keys(environments)) {
            const elements = await extractByEnvironment(env, environments[env], content, undefined, { partId, resetAt }, engine);
            labels[env] = elements;
        }
    }

    try {

        // for legacy reasons fill the dedicated arrays
        const equations = labels['equation'];
        const tables = labels['table'];
        const codeBlocks = labels['code'];
        const figures = labels['image'];

        const images = extractImages(content);

        // extract elements from the document
        const headings = (await extractType<SFNodeType.heading>(SFNodeType.heading, content, undefined, partId, partFileMap)).filter(heading => heading.title && heading.title?.length > 0);
        const footnotes = await extractType<SFNodeType.footnote>(SFNodeType.footnote, content, undefined, partId, partFileMap);
        const citations = await extractType<SFNodeType.citation>(SFNodeType.citation, content, undefined, partId, partFileMap);
        const footnotesAndCitations = await extractTypes<SFNodeType.citation | SFNodeType.footnote>([SFNodeType.citation, SFNodeType.footnote], content, undefined, partId, partFileMap, engine);

        const getAttr = (attrs, key): string | undefined => attrs[key] ? attrs[key] as string : undefined;

        // transfer attributes from the documents/parts into a virtual part structure
        const parts = content.content?.filter(c => isRootDoc(c.type)).map(d => ({
            id: getAttr(d.attrs, 'id') || 'UNKNOWN_PART',
            title: extractHeadingOfType(d),
            type: getAttr(d.attrs, 'type'),
            role: getAttr(d.attrs, 'role'),
            skipToc: getAttr(d.attrs, 'skipToc'),
            numbering: getAttr(d.attrs, 'numbering'),
            placement: getAttr(d.attrs, 'placement'),
            placementNumbering: getAttr(d.attrs, 'placementNumbering'),
            standalone: getAttr(d.attrs, 'standalone')
        })) || [];

        // create map of all elements for quick access
        const ids = [...headings, ...images, ...Object.keys(labels).map(key => labels[key]).flat()];
        const idMap = ids.reduce((prev, element) => ({ ...prev, [element.id]: element }), {});

        // generate headings
        const toc = await generateToc(isRootDoc(content?.type) ? [content] : content.content?.filter(c => isRootDoc(c.type)) || [], partId, defaultNumberingType, engine, baseLevel, { partFileMap, idMap });
        // generate environment numbering (may depend on heading numbering)
        for (let label of Object.keys(labels)) {
            for (let item of labels[label]) {
                const heading = toc.headings.find(h => h.id === item.nearestHeading);
                if (!heading) { continue; }
                const index = labels[label].filter(i => i.partId === item.partId && i.customNumber?.startsWith(heading?.numberingString)).length;
                const separator = environments?.[label]?.numberingSeparator || '.';
                item.customNumber = item.customNumber ?? heading?.numberingString?.length > 0 ? `${heading?.numberingString}${separator}${index + 1}` : `${index + 1}`;
            }
        }

        return {
            toc,
            equations,
            tables,
            codeBlocks,
            figures,
            headings,
            footnotes,
            footnotesAndCitations,
            citations,
            images,
            labels,
            parts,
            ids,
            idMap
        };
    } catch (e) {
        debugger;
        console.error('Could not generate listings', e);
    }

    throw new Error('Could not generate listings');
};

/**
 * Walks a document and finds all labels.
 * If a node is found that contains one of the original standard figure environments it is added instead.
 */
async function extractByEnvironment(
    filterForEnv: string,
    conf: GenericEnvironment,
    node: DocumentNode<any>,
    parent?: DocumentNode<any>,
    location?: { partId?: string; nearestHeading?: string; resetAt?: number; },
    engine?: RenderingEngine<any, any>
): Promise<DocumentElement<any>[]> {

    try {

        if (!location) { location = {}; }
        const resetAt = location.resetAt ?? 0;

        // If you're coming here to optimize, run a single pass instead of extracting for each label
        if (!location?.partId && isRootDoc(node.type)) { location.partId = node.attrs?.id; }
        if (node.attrs?.type === 'native-table') { node.attrs.type = 'table'; }
        let list: DocumentElement<any>[] = [];

        /*
            <figure id="custom-figure-1">
                <caption>
                    <label>Custom</label>
                    <p>Title</p>
                    <p>Notes..</p>
                </caption>
            <figure>
    
            <figure id="custom-figure-1" type="table">
                <caption>
                    <p>Title</p>
                    <p>Notes..</p>
                </caption>
            <figure>
        */
        if (node.type === SFNodeType.figure) {
            // does it have a label?
            // otherwise use the type to match a default label
            let labelInCaption = node.content?.find(n => n.type === 'caption')?.content?.find(n => n.type === 'label');
            if (labelInCaption) {
                const labelText = getTextContent(labelInCaption);
                // TODO use labelToEnvironment here
                if (labelText && labelText?.startsWith(conf.label)) {
                    const { title, titleHTML } = await extractCaptionOrTitle(node, parent, { engine });
                    if (!conf.countWithoutCaption && (!title || title?.length === 0)) { return []; }
                    return [{
                        type: filterForEnv,
                        id: node.attrs?.id,
                        attrs: node.attrs,
                        ...(matchLabel(title, conf) || { title }),
                        titleHTML,
                        content: node.content,
                        nodeId: node.attrs?.id,
                        partId: location.partId,
                        nearestHeading: location.nearestHeading,
                        environment: conf,
                        label: labelInCaption
                    }];
                }
            } else {
                // try to match the node attr type to the currently looked for environment
                if (
                    defaultEnvironments[filterForEnv] && // standard environment for this name exists (e.g. image)
                    node?.attrs?.type === defaultEnvironments[filterForEnv] || node?.attrs?.type === 'image' && filterForEnv === 'image' // e.g. table, figure, ..
                ) {
                    let { title, titleHTML } = await extractCaptionOrTitle(node, parent, { engine });

                    if (!conf.countWithoutCaption && (!title || title?.length === 0)) { return []; }
                    return [{
                        type: filterForEnv,
                        id: node.attrs?.id,
                        attrs: node.attrs,
                        ...(matchLabel(title, conf) || { title }),
                        titleHTML,
                        content: node.content,
                        nodeId: node.attrs?.id,
                        partId: location.partId,
                        nearestHeading: location.nearestHeading,
                        environment: conf,
                        label: undefined
                    }];
                }
            }

            return [];
        }

        /*
        <math id="eq1"></math>
        */
        if (node.type === SFNodeType.math && filterForEnv === 'equation' && node.attrs.style === 'block') {
            return [{
                type: filterForEnv,
                id: node.attrs?.id,
                attrs: node.attrs,
                title: undefined,
                content: node.content,
                nodeId: node.attrs.id,
                partId: location.partId,
                nearestHeading: location.nearestHeading,
                environment: conf,
                label: undefined
            }];
        }

        /*
        <blockquote id="poem-1">
            <label>Poem</label>
            <p>I am a poem</p>
        <blockquote>
        */
        if (node.type === SFNodeType.blockquote) {
            // does it have a label?
            // otherwise we keep processing
            const labelChild = node?.content?.find(n => n.type === 'label');
            if (labelChild) {
                const labelText = getTextContent(labelChild);
                if (labelText && labelText?.startsWith(conf.label)) {
                    return [{
                        type: filterForEnv,
                        id: node.attrs?.id,
                        attrs: node.attrs,
                        title: undefined,
                        content: node.content,
                        nodeId: node.attrs.id,
                        partId: location.partId,
                        nearestHeading: location.nearestHeading,
                        environment: conf,
                        label: undefined
                    }];
                }
            }
            // we do not return to go into children later
        }

        // descend into children
        if (node.content) {
            const values = await node.content.reduce(async (prevPromise, n) => {
                const prev = await prevPromise;
                // whenever we read a heading we save it to attach it to following environment nodes
                // the nearest heading is used to calculate numbering prefixes, e.g. Figure 1.1.2 (where the figure is the 2nd figure in chapter 1.1)
                // we use resetAt to cut off headings at a certain level (0 for global, 1 for numbering based on heading 1)
                const nearestHeading = n?.type === SFNodeType.heading && (resetAt > 0 && n.attrs?.level <= resetAt) ? n?.attrs?.id : prev.nearestHeading ?? location?.nearestHeading;
                const extracted = await extractByEnvironment(filterForEnv, conf, n, node, { ...location, nearestHeading }, engine);
                return {
                    list: [...prev.list, ...extracted].flat(),
                    nearestHeading
                };
            }, Promise.resolve({ list: [] as DocumentElement<any>[], nearestHeading: undefined as string | undefined }));
            list = [...list, ...values.list].flat();
        }

        return list.flat().filter(n => n != null);
    } catch (e) {
        debugger;
        console.error('Could not extract from environment ' + filterForEnv);
        throw new Error('Could not extract from environment');
    }
}

/**
 * Extracts nodes of a particular type.
 * @param partFileMap the files the element will be exported in
 */
async function extractType<SFNodeType>(type: SFNodeType, node: DocumentNode<any>, parent?: DocumentNode<any>, partId?: string, partFileMap?: PartFileMap): Promise<DocumentElement<SFNodeType>[]> {
    return extractTypes([type], node, parent, partId, partFileMap);
}

/**
 * Extract notes of particular types.
 * @param partFileMap the file the element will be exported in
 */
async function extractTypes<SFNodeType>(types: SFNodeType[], node: DocumentNode<any>, parent?: DocumentNode<any>, partId?: string, partFileMap?: PartFileMap, engine?: RenderingEngine<any, any>): Promise<DocumentElement<SFNodeType>[]> {
    if (!types) { return []; }
    if (!partId && isRootDoc(node.type)) { partId = node.attrs?.id; }
    let c: DocumentElement<any>[] = [];
    for (let n of (node.content || [])) {
        c = [...c, ...(await extractTypes(types, n, node, partId, partFileMap)).flat()]
    }

    const hostFileName = partId && partFileMap ? partFileMap?.[partId] : undefined;

    if (types.includes(node.type)) {
        let { title, titleHTML } = await extractCaptionOrTitle(node, parent, { engine });
        if (parent?.type === SFNodeType.figure) {
            c.push({ type: node.type, id: node.attrs?.id, attrs: node.attrs, title, titleHTML, content: node.content, nodeId: parent.attrs?.id, partId, hostFileName, parentType: parent?.type });
        } else {
            c.push({ type: node.type, id: node.attrs?.id, attrs: node.attrs, title, titleHTML, content: node.content, partId, hostFileName, parentType: parent?.type });
        }
    }

    return c.flat().filter(n => n != null);
}

export const getDefaultEnvironments: () => { [key: string]: GenericEnvironment } = () => ({
    image: {
        label: 'Figure',
        labelSuffix: '.',
        countWithoutCaption: true
    } as GenericEnvironment,
    table: {
        label: 'Table',
        labelSuffix: '.',
        countWithoutCaption: true
    } as GenericEnvironment,
    equation: {
        label: 'Code',
        labelSuffix: '.',
        countWithoutCaption: true
    } as GenericEnvironment,
    code: {
        label: 'Code',
        labelSuffix: '.',
        countWithoutCaption: true
    } as GenericEnvironment
});

/** Uses the caption to guess the label and numbering part (if provided) */
export function matchLabel(title, conf: GenericEnvironment): { title: string; customNumber: string; customLabel: string; completeLabel: string } | undefined {

    const labelSuffix = conf.labelSuffix === '.' ? '\\.' : conf.labelSuffix;
    const cleanedLabelSuffix = labelSuffix.replace(/\\/g, ''); // Remove escape characters
    const matchLabel = new RegExp(`${conf.label}\\s((?:\\d*[.-]?)+[A-Za-z]?(?:\\.\\d+)?)\\s*${labelSuffix}`, 'g');

    if (title) {
        const match = matchLabel.exec(title);
        if (match) {
            const [label, number] = match;
            if (label && number) {
                return {
                    title: title.substring(label.length)?.trim(),
                    customNumber: number,
                    completeLabel: label,
                    customLabel: label.substring(0, label.length - number.length - cleanedLabelSuffix.length - 1)
                }
            }
        }
    }

    return undefined;
}

function extractImages(node: DocumentNode<any>): DocumentElement<SFNodeType.image>[] {
    let images: any[] = [];
    if (node.type === SFNodeType.image) {
        images.push({
            id: node.attrs?.id,
            type: SFNodeType.image,
            attrs: node.attrs,
            nodeId: node.attrs?.id
        });
    }
    if (node.type == SFNodeType.figure && node.attrs?.src?.length > 0) {
        images.push({
            id: node.attrs?.id,
            type: SFNodeType.image,
            attrs: node.attrs,
            nodeId: node.attrs?.id
        });
    }

    if (node.content?.length) {
        images = [...images, ...(node.content.map(extractImages).flat())];
    }

    return images;
}

const defaultEnvironments = {
    image: SFNodeType.figure,
    equation: SFNodeType.math,
    code: SFNodeType.code,
    table: SFNodeType.table
}