import { TSX } from '../TSX';
import { DocumentData, SciFlowDocumentData } from "../interfaces";
import { RenderingEngine } from "../renderers";
import defaultTemplateComponents from './components';
import { createLogger, format, transports } from 'winston';
import { ReferenceRenderer, getDefaults } from '@sciflow/cite';
import { existsSync } from 'fs';
import { PartForRendering, TemplateComponent } from '../component';
import { renderCss } from '../css';
import {
    ContentHole
} from './components/content-hole/content-hole.component';
import { documentDataFromSnapshot, mapDoc } from "./convert";
import { createRenderingEngine } from "./createEngine";
import { createFactories, createInstances } from './renderers/helpers';
import { transformImage } from './files';
import { slugify } from '../utils/helpers';

const { styleXML, localeXML } = getDefaults();
const { LOG_LEVEL = 'info' } = process.env;
const logger = createLogger({
    level: LOG_LEVEL,
    format: format.json(),
    defaultMeta: { service: 'html-main-create-document' },
    transports: [
        new transports.Console({ level: LOG_LEVEL })
    ],
});

/**
 * Create DOM and Styles for a given document.
 */
export const createInstancesAndStyles = async (exportData, options, { runner, files, id, assetDir, fontAsSeparateFile }) => {
    const customTemplateComponents = options?.customTemplateComponents || [];
    const templateComponents = [...defaultTemplateComponents, ...customTemplateComponents] as TemplateComponent<any, SciFlowDocumentData>[];

    const configuration = options?.configurations?.find(c => c.kind === 'Configuration');
    let configurations = options?.configurations || [];
    const locale = options?.locale ?? exportData.locale ?? configuration?.spec?.lang ?? 'en-US';
    const citationRenderer = new ReferenceRenderer(options?.citationStyleXML?.[locale] || styleXML, options?.localeXML?.[locale] || localeXML, exportData.references ?? []);
    const document: DocumentData = await documentDataFromSnapshot(exportData, { files, locale, configurations });
    const { title } = document;

    let customRenderers = {};
    // the node renderer renders the document tree itself
    const engine: RenderingEngine<HTMLElement, SciFlowDocumentData> = await createRenderingEngine({
        document,
        documentSnapshot: exportData,
        configurations,
        citationRenderer,
        customRenderers,
        templateOptions: configuration?.spec || configuration,
        metaData: options?.metaData,
        metaDataSchema: options?.metaDataSchema,
        logging: options?.logging,
        runner,
        templateComponents,
        componentPaths: options?.componentPaths || []
    }, options?.debug, id);

    // compress or transform images into web formats
    files = await Promise.all(files.map(file => transformImage(file, { imageDirectory: assetDir, engine })));

    let partsWithMetaData: PartForRendering[] = [];
    for (let p of exportData.parts
        .filter(p => p.type !== 'title')) {
        const doc = mapDoc(p, engine.data?.templateOptions?.placement);
        let activeEngine: RenderingEngine<HTMLElement, SciFlowDocumentData> = engine;
        // we replace the active engine with one using the parent meta data if provided
        // this allows for document collections to access the meta data inside a sub/document
        if (p.parent) {
            if (p.standalone) {
                // do not render the DOM if we're going individually render sub-documents anyways
                partsWithMetaData.push({
                    ...p,
                    id: p.document.attrs?.id || p.partId || p.id,
                    dom: undefined,
                    placement: p.options?.placement || p.placement,
                    document: doc,
                    parent: {
                        ...p.parent,
                        parts: p.parent.parts.map(p2 => ({ ...p2, standalone: true }))
                    },
                    notDefining: false
                });
                continue;
            }

            const partDoc = await documentDataFromSnapshot(p.parent, { files, locale, configurations });
            activeEngine = await createRenderingEngine({
                document: partDoc,
                documentSnapshot: p.parent,
                configurations,
                citationRenderer,
                customRenderers,
                templateOptions: configuration?.spec || configuration,
                metaData: partDoc.metaData,
                metaDataSchema: options?.metaDataSchema,
                logging: options?.logging,
                runner,
                componentPaths: options?.componentPaths || [],
                templateComponents,
                baseLevel: p.level ?? 1,
            }, options?.debug, 'sub-for-part-' + partDoc.documentId);
        }

        const renderDOM = async (e: RenderingEngine<HTMLElement, SciFlowDocumentData>) => {
            try {
                return await e.render(doc);
            } catch (e: any) {
                debugger;
                return <section id={p?.document?.attrs?.id}>
                    <h1>Could not render section(deferred) </h1>
                    <p class="m0 p0"> {e.message} </p>
                    <h2 style="color: red;" > Error: {e.payload.message} </h2>
                    <pre> {JSON.stringify(e.payload, null, 2)} </pre>
                </section>;
            }
        };

        // render bibliography later (so all references are registered, even the ones after the bibliography)
        if (p.type === 'bibliography') {
            partsWithMetaData.push({
                id: p.document.attrs?.id || p.partId || p.id,
                dom: (_engine) => renderDOM(activeEngine),
                placement: p.options?.placement || p.placement,
                document: doc,
                parent: p.parent,
                standalone: p.standalone
            });
            continue;
        }

        partsWithMetaData.push({
            ...p,
            id: p.document.attrs?.id || p.partId || p.id,
            dom: await renderDOM(activeEngine),
            placement: p.options?.placement || p.placement,
            document: doc,
            parent: p.parent,
            notDefining: false
        });
    }

    // components can be used to inject DOM into the document (e.g. for the table of contents)
    // or just render CSS (e.g. to style the @page)
    const componentFactories = createFactories(templateComponents, engine);

    // go through the template configuration values and create the styles and additional DOM elements
    let { styles, instances } = await createInstances(
        // do not add part configurations since they will be rendered in sub-documents
        configurations.filter(c => c.page !== 'part'),
        componentFactories,
        {
            runner,
            componentPaths: options?.componentPaths,
            logger,
            assetBasePaths: options?.assetBasePaths
        }
    );

    if (options?.stylePaths && options?.stylePaths?.length > 0) {
        const templateStyles = (await Promise.all((options?.stylePaths || [])
            ?.filter(stylePath => {
                if (Array.isArray(stylePath.runners) && options?.runner) {
                    return stylePath.runners.includes(options?.runner);
                }
                return true;
            })
            .map(async (stylePath) => {
                try {
                    if (!existsSync(stylePath.path)) {
                        throw new Error('Style file did not exist: ' + stylePath.path);
                    }
                    const { styles } = await renderCss(stylePath.path, {
                        document: { title: title && title?.length > 60 ? title?.substring(0, 60) + '...' : title, ...(options?.metaData ?? {}) },
                        configurations: options?.configurations
                    });
                    return {
                        css: styles.css,
                        type: stylePath.type,
                        position: stylePath.position || 'start'
                    };
                } catch (e: any) {
                    console.error('Failed to render html styles', { message: e.message, stylePath: stylePath.path });
                    debugger;
                    return null;
                }
            }))).filter(v => v && v?.css?.length > 0);

        styles.push(...templateStyles);
    }

    if (fontAsSeparateFile === true) {
        const fontFiles = styles?.filter(s => s.type === 'font-styles');
        styles = styles?.filter(s => s.type !== 'font-styles');
        if (fontFiles?.length > 0) {
            const fontFile = fontFiles.map(f => f.css).join('\n\n');
            fontFile?.length > 0 && files.push({
                type: 'style',
                id: 'fonts.css',
                content: fontFile,
                mimeType: 'text/css',
            });
        }
    }

    instances = instances.map(part => ({
        ...part,
        // we create a default body placement if none was provided so matching does not always have to check for empty placements
        placement: part.placement || 'body'
    }));

    partsWithMetaData = partsWithMetaData.map(part => ({
        ...part,
        // we create a default body placement if none was provided so matching does not always have to check for empty placements
        placement: part.placement || 'body'
    }));

    const templateRendersTitle = configurations.some(c => c.kind === 'JournalTitle' || c.kind === 'MonographTitle');
    /**
     * Order parts
     */
    let orderedPartsAndInstances: PartForRendering[] = [
        ...fillContentHoles({ parts: partsWithMetaData, instances }, 'cover'),
        ...fillContentHoles({ parts: partsWithMetaData, instances }, 'front'),
        ...fillContentHoles({ parts: partsWithMetaData, instances }, 'body'),
        ...fillContentHoles({ parts: partsWithMetaData, instances }, 'back'),
    ].filter(part => !(part.document?.attrs.id === 'title' && templateRendersTitle));

    // we re-generate the tables to have access to titleHTMl elements and other rendered captions/titles
    // this is the first time we know the final order of all content        

    await engine.generateListings(orderedPartsAndInstances
        .filter(part => typeof part.document === 'object')
        .map(part => mapDoc(part, engine.placement))
        .filter(p => p.attrs.role !== 'coverpage')
    );

    if (!engine.data.document.tables) { throw new Error('Tables must be generated at this point'); }
    const images = engine.data.document.tables.images;

    // go through all parts and move components into the page flow (to avoid additional page breaks)
    // an example where this is needed is the headers and footers and css paged media which can not be the
    // first child of the body but should rather be moved into the first section inside (to maintain page breaks)
    // see https://www.princexml.com/forum/topic/4877/create-complex-headers-that-work-with-page-groups-not-sure
    for (let part of orderedPartsAndInstances) {
        if (part.moveIntoPageFlow) {
            // find the next part the element can be injected into (e.g. into it's so called content hole)
            let partForPlacement;
            if (part.placement) {
                // move it only into the part of the same placement
                // make sure the part has dom content, otherwise it might get skipped
                partForPlacement = orderedPartsAndInstances.find(p => p.placement === part.placement && !p.moveIntoPageFlow);
            } else {
                // move it into the first possible part
                partForPlacement = orderedPartsAndInstances.find(p => p !== part && !p.moveIntoPageFlow);
            }

            if (!partForPlacement) { continue; }
            // add the part to the content hole of the part that will be rendered
            partForPlacement.contentHole = [...(partForPlacement.contentHole || []), part];
            // remove from list of parts to be rendered
            orderedPartsAndInstances = orderedPartsAndInstances.filter(p => p !== part);
        }
    }

    const firstStyles = styles.filter(style => style.position === 'start');
    const middleStyles = styles.filter(style => !style.position);
    const endStyles = styles.filter(style => style.position === 'end');

    let inlineStyles: any[] = [];
    let cssStyles: string = '';
    if (options?.inline) {
        inlineStyles.push(...firstStyles, ...middleStyles, ...endStyles);
    } else {
        cssStyles = [
            ...firstStyles.filter(style => !style.inline),
            ...middleStyles.filter(style => !style.inline),
            ...endStyles.filter(style => !style.inline)
        ].map(s => s?.css)
            .join('\n\n');
    }

    return {
        engine,
        document,
        inlineStyles,
        cssStyles,
        locale,
        files,
        title,
        templateComponents,
        orderedPartsAndInstances
    };
}

/**
 * Uses content holes to order parts of a page placement.
 */
export const fillContentHoles = (opts: { parts: PartForRendering[], instances: PartForRendering[] }, placement = 'body') => {
    const list: PartForRendering[] = [];

    let partsForPlacement = opts.parts.filter(p => p.placement === placement);
    const instancesForPlacement = opts.instances.filter(p => p.placement === placement);
    if (!instancesForPlacement.some(i => i.configuration?.kind === ContentHole.name)) {
        // create a hole at the end if none exists
        instancesForPlacement.push({
            id: 'content-hole-front',
            dom: [],
            placement,
            configuration: {
                kind: ContentHole.name,
                page: placement,
                spec: {}
            },
            notDefining: true
        });
    }

    for (let instance of instancesForPlacement) {
        // fill all content holes with matching parts
        if (instance.configuration?.kind === ContentHole.name) {
            // TODO match more than just the page/placement by using specificity
            // (so right now we only handle one content hole but we can expand that to match roles)
            list.push(...partsForPlacement);
        } else {
            list.push(instance);
        }
    }

    // TODO once content holes have selectors we need to make sure that any content that was not matched
    // is still added at the end

    return list;
}

/** The dom attribute can be a function in case it needs to be
* rendered after most other instances have been rendered (e.g. for the table of contents)
* 
* This will also move content holes from one component into others if needed.
* */
export const instanceToDom = async (partToRender: PartForRendering, engine) => {
    let holes: HTMLElement[] = [];
    if (partToRender.contentHole && partToRender.contentHole?.length > 0) {
        for (let hole of partToRender.contentHole) {
            const d = typeof hole.dom === 'function' ? await hole.dom(engine) : hole.dom;
            if (d != undefined) {
                holes.push(...d);
            }
        }
        holes = holes.filter(e => e != undefined);
    }
    let instanceDom = typeof partToRender.dom === 'function' ? await partToRender.dom(engine) : partToRender.dom;
    if (holes.length > 0 && instanceDom) {
        if (!Array.isArray(instanceDom)) { instanceDom = [instanceDom]; }
        if (!instanceDom[0]) {
            logger.error('Encountered empty element from component instance', { partId: partToRender.id })
            // throw new Error('Instance result must be a dom element');
            return <div class="virtual">{...holes}</div>
        }
        if (instanceDom[0].firstChild) {
            for (let node of holes) {
                instanceDom[0].insertBefore(node, instanceDom[0].firstChild);
            }
        }
    }

    return instanceDom;
};

export const instancesToDom = async (orderedPartsAndInstances, { index, document, engine, altTitle, title, locale, cssStyles, inlineStyles, scripts, styleFileName, standalone }: {
    index?: number;
    document: any;
    engine: any;
    altTitle?: string;
    title?: string;
    locale: any;
    cssStyles: any;
    inlineStyles: any;
    scripts: any[];
    styleFileName?: string;
    standalone?: boolean;
}): Promise<HTMLElement> => {
    /**
     * Finalizes the parts in order.
     * This is important so content holes get filled appropriately.
     * We extract styles during rendering of the dom elements to move them into the head later
     */
    const toDom = async (parts: PartForRendering[], engine: RenderingEngine<any, SciFlowDocumentData>): Promise<{ dom: any[]; styles: any[]; }> => {
        let dom: (HTMLElement[] | undefined)[] = [];
        let styles: (HTMLStyleElement | Comment)[] = [];
        for (let part of parts) {
            try {
                let result: HTMLElement[] = await instanceToDom(part, engine);
                if (!Array.isArray(result)) { result = result ? [result] : []; }
                for (let el of result) {
                    const styleNodes = Array.from(el.querySelectorAll('style'));
                    for (let style of styleNodes) {
                        const movedStyle = style.cloneNode(true) as HTMLStyleElement;
                        styles.push(movedStyle);
                        style.classList.add('moved-style-' + slugify(part.id));
                        style.innerHTML = '/** moved ' + part.id + ' */';
                    }
                }

                dom.push(result);
            } catch (e: any) {
                debugger;
                logger.error('Could not finalized part ' + part.id, { message: e.message });
            }
        }

        return { dom, styles };
    };

    let extractedStyles: HTMLStyleElement[] = [];
    let cover: any[] = [];
    if (orderedPartsAndInstances.filter(s => s.placement === 'cover').some(p => !p.notDefining)) {
        // only add the back if there are parts in it other than headers and footers
        const result = await toDom(orderedPartsAndInstances.filter(s => s.placement === 'cover'), engine);
        cover = result.dom;
        extractedStyles = [...extractedStyles, ...result.styles];
    }
    let front: any[] = [];
    if (orderedPartsAndInstances.filter(s => s.placement === 'front').some(p => !p.notDefining)) {
        // only add the back if there are parts in it other than headers and footers
        const result = await toDom(orderedPartsAndInstances.filter(s => s.placement === 'front'), engine);
        front = result.dom;
        extractedStyles = [...extractedStyles, ...result.styles];
    }

    let body: any[] = [];
    if (orderedPartsAndInstances.filter(s => s.placement === 'body').some(p => !p.notDefining)) {
        const result = await toDom(orderedPartsAndInstances.filter(s => s.placement === 'body' || !s.placement), engine);
        body = result.dom;
        extractedStyles = [...extractedStyles, ...result.styles];
    }

    let back: any[] = [];
    if (orderedPartsAndInstances.filter(s => s.placement === 'back').some(p => !p.notDefining)) {
        // only add the back if there are parts in it other than headers and footers
        const result = await toDom(orderedPartsAndInstances.filter(s => s.placement === 'back'), engine);
        back = result.dom;
        extractedStyles = [...extractedStyles, ...result.styles];
    }

    const bodyEl: HTMLBodyElement = <body>
        {(cover.length > 0 || cover.length > 0) ? <section class="cover" aria-label="Cover">
            {...cover}
        </section> : ''}
        {(front.length > 0 || front.length > 0) ? <section class="front" aria-label="Front matter">
            {...front}
        </section> : ''}
        {(body.length > 0 || body.length > 0) ? <section class="body" aria-label="Main text">
            {...body}
        </section> : ''}
        {(back.length > 0 || back.length > 0) ? <section class="back" aria-label="Back matter">
            {...back}
        </section> : ''}
    </body>;

    if (index != undefined) {
        bodyEl.setAttribute('data-document-index', `${index}`);
    }

    bodyEl.setAttribute('data-standalone', `${standalone === true}`);

    styleFileName = styleFileName || 'styles.css';
    if (standalone === true) { bodyEl.setAttribute('data-standalone', 'true'); }
    return <html lang={locale}>
        <head>
            <title>{title}</title>
            {altTitle ? <style>{`
        :root {
            --alt-title: "${altTitle}";
        }\n`
            }</style> : ''}
            {inlineStyles?.length > 0 ? <style>
                {inlineStyles.map(s => s?.css).filter(s => s != undefined)
                    .map(style => style
                        .replaceAll('url("fonts/', 'url("/export/fonts/')
                        .replaceAll('url("tpl/', 'url("/export/tpl/')
                        .replaceAll('url("assets/', 'url("/export/assets/')
                    ).join('\n\n')}
            </style> : ''}
            {cssStyles?.length > 0 ? <link rel="stylesheet" href={styleFileName} /> : ''}
            {scripts.map((script: any) => script.src ? <script src={script.src}></script> : <script>{script.content}</script>)}
            {...extractedStyles}
        </head>
        {bodyEl}
    </html>;
}