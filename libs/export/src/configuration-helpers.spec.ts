import { sameConfiguration, sameConfigurationAndLocales } from "./configuration-helpers";
import { GenericConfiguration } from "./interfaces";
import { expect } from 'chai';
import { describe } from 'mocha';

const configs: GenericConfiguration<any>[] = [
    {
        kind: 'A',
        spec: {},
        page: 'body',
        runners: [
            'princexml'
        ]
    },
    {
        kind: 'A',
        spec: {},
        page: 'body',
        locales: [
            'en-US'
        ],
        runners: [
            'princexml'
        ]
    },
    {
        kind: 'B',
        spec: {},
        page: 'body',
        locales: [
            'en-US'
        ],
        runners: [
            'princexml'
        ]
    }
];

describe('Configuration helpers', async () => {
    it('The same configurations are always equal', async () => {
        expect(sameConfiguration(configs[0], configs[0])).to.equal(true);
    });
    it('The configurations with different locales are equal for the sameConfiguration function', async () => {
        expect(sameConfiguration(configs[0], configs[1])).to.equal(true);
    });
    it('The configurations with different locales are NOT equal for the sameConfigurationAndLocales function', async () => {
        expect(sameConfigurationAndLocales(configs[0], configs[1])).to.equal(false);
    });
    it('The different configurations with the same locales are NOT equal', async () => {
        expect(sameConfiguration(configs[1], configs[2])).to.equal(false);
        expect(sameConfigurationAndLocales(configs[1], configs[2])).to.equal(false);
    });
});