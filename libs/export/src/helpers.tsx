import { DocumentNode, DocumentSnapshot, DocumentSnapshotPart, HeadingDocumentNode, SFNodeType, counterStyle, createId, rootDoc } from "@sciflow/schema";
import { SourceField } from "@sciflow/cite";
import { basename, extname, join } from "path";
import { v4 as uuidv4 } from 'uuid';
import { TSX } from './TSX';
import { SciFlowDocumentData, SingleDocumentExportData, TocHeading } from "./interfaces";
import { RenderingEngine } from "./renderers";
import romanize from 'romanize';

/** Hashes a string */
export const hash = (json: any): string => {
    const s = JSON.stringify(json);
    var hash = 0, i, chr;
    if (s.length === 0) return `${hash}`;
    for (i = 0; i < s.length; i++) {
        chr = s.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }

    return hash
        .toString()
        .replace('-', '9')
        .split('')
        .map(char => Number.parseInt(char) + 65)
        .map(char => String.fromCharCode(char))
        .join('')
        .toLowerCase();
}


const counterStyleValues: { [counterStyle: string]: (v: number) => string | number; } = {
    'decimal': (v: number) => v,
    'lower-alpha': (v: number) => 'a b c d e f g h i j k l m n o p q r s t u v w x y z'[(v - 1) * 2],
    'upper-alpha': (v) => 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z'[(v - 1) * 2],
    'upper-roman': (v) => romanize(v),
    'lower-roman': (v) => romanize(v).toLowerCase()
};

/**
* Takes an array with numbering e.g. [1,1,2] for a h3 and returns a string with the values concatenated. (e.g. I.1.1 for upper-roman counter styles)
* Exported for testability. Not an external API.
*/
export const createPrefix = (levels: number[], styles: counterStyle[]): string => {
    const mapStyle = (style: counterStyle) => {
        if (style === 'none') { return ''; }
        if (style === 'alpha') { style = 'upper-alpha'; }
        if (style === 'numeric') { style = 'decimal'; }
        return style;
    }

    styles = styles.map(mapStyle);

    // we only create a prefixi if have a style for each level
    // we filter for level > 0 because we allow numbering to be skipped on the top level
    if (levels.some((counterValue, level) => level > 0 && counterValue > 0 && (!styles[level] || styles[level]?.length === 0))) {
        return '';
    };

    return levels.filter(v => v > 0).map((counterValue, level): string => {
        let style = mapStyle(styles[level]);
        if (!style) { return ''; }
        return `${counterStyleValues[style] ? counterStyleValues[style](counterValue) : counterValue}`;

    }).filter(s => s?.length > 0).join('.');
}


export const getTextContent = (node: DocumentNode<any>): string => {
    let text = node.text || '';
    if (!node.content) { return text; }
    for (let child of node.content) {
        if (child.type === SFNodeType.footnote) { continue; }
        if (child.type === SFNodeType.text) {
            text += child.text;
        } else if (child.type === SFNodeType.citation) {
            if (!child.attrs?.source) { return '[citation]'; }
            const citation = SourceField.fromString(child.attrs?.source)
            text += '[' + citation.citationItems.map(c => c.id).join(',') + ']';
        } else if (child.content && Array.isArray(child.content)) {
            text += child.content.map(getTextContent).join(' ');
        }
    }
    return text;
}

/** Generates a list of author names, using et al. for more than 2 */
export const generateAuthorList = (authors) => {
    if (authors.length > 2) {
        const author = authors[0];
        return (author?.firstName ? author?.firstName[0] + '.' : '') + ' ' + (author?.lastName || '') + ' et al.'
    }

    return authors
        .map((author) => (author?.firstName ? author?.firstName[0] + '.' : '') + ' ' + (author?.lastName || ''))
        .map(n => n.trim())
        .filter(n => n?.length > 0)
        // cocatenate as Author, Author & Author
        .map((author, index, list) => (index === list.length - 1 && index > 0) ? ' & ' + author : index > 0 ? ', ' + author : author)
        .join('') + (authors.length > 2 ? ' et al.' : '');
};

/**
 * Converts a document from the single chapter schema to a document split into parts.
 * This is needed to import into SciFlow or use advanced template processing.
 */
export const convertToSnapshot = (input: SingleDocumentExportData, opts: { assetUrl?: string; }): DocumentSnapshot => {

    // split the document
    if (!input.document?.content) {
        throw new Error('Document can\'t be empty');
    }

    const parts = createParts(input.document.content);
    const locale = input.configuration?.locale ?? 'en-US';
    const documentId = uuidv4();

    if (!parts.some(p => (p.type === 'title' || p.partId === 'title'))) {
        parts.push({
            partId: 'title',
            type: 'title',
            schema: 'title',
            options: {},
            document: {
                type: SFNodeType.document,
                attrs: { id: 'title' },
                content: [{ type: SFNodeType.heading, content: [{ type: SFNodeType.text, text: 'Untitled document' }] }]
            }
        });
    }

    const title = parts.find(p => p.type === 'title');

    // TODO type this
    const data = {
        id: documentId,
        documentId,
        title,
        authors: input.authors,
        createdAt: Date.now(),
        files: input.files?.map(file => {
            return { type: 'image', ...file };
        }) || [],
        index: {
            index: parts.filter(p => p.type !== 'title').map(p => p.partId)
        },
        metaData: input.metaData,
        parts,
        references: input.references,
        locale,
    };

    return data;
}

/**
 * A caption should not contain links or similar.
 */
const removeUnwantedTagsFromCaption = (htmlString?: string) => {
    return htmlString
        ?.replaceAll(/<a\s+.*?>(.*?)<\/a>/gi, '$1');
}

export const extractCaptionOrTitle = async (node: DocumentNode<any>, parent?: DocumentNode<any>, opts?: { engine?: RenderingEngine<any, any> }): Promise<{ title?: string; titleHTML?: string; }> => {

    let titleHTML;
    switch (node.type) {
        case SFNodeType.figure:
            {
                const caption: DocumentNode<any, any> | undefined = node?.content?.find(n => n.type === 'caption');
                const firstCaptionContent: DocumentNode<any, any> | undefined = caption?.content?.filter(c => c.type !== 'label')?.[0];
                const title = firstCaptionContent && getTextContent(firstCaptionContent);
                if (firstCaptionContent) {
                    if (opts?.engine) {
                        try {
                            titleHTML = (await opts.engine.render(firstCaptionContent))?.innerHTML;
                        } catch (e: any) {
                            console.error('Could not render figure caption html', { message: e.message, id: node.attrs.id });
                        }
                    }
                    return { title, titleHTML: removeUnwantedTagsFromCaption(titleHTML) };
                }
            }
            break;
        case SFNodeType.table:
            {
                const caption: DocumentNode<any, any> | undefined = node?.content?.find(n => n.type === 'caption');
                const firstCaptionContent: DocumentNode<any, any> | undefined = caption?.content?.filter(c => c.type !== 'label')?.[0];
                if (firstCaptionContent) {
                    if (opts?.engine) {
                        try {
                            titleHTML = (await opts.engine.render(firstCaptionContent))?.innerHTML;
                        } catch (e: any) {
                            console.error('Could not render table caption html', { message: e.message, id: node.attrs.id });
                        }
                    }
                    return { title: getTextContent(firstCaptionContent), titleHTML: removeUnwantedTagsFromCaption(titleHTML) };
                }
            }
            break;
        case SFNodeType.caption:
            {
                if (node?.content && node.content.filter(c => c.type !== 'label')?.length > 0) {
                    const caption: DocumentNode<any, any> | undefined = node;
                    const firstCaptionContent: DocumentNode<any, any> | undefined = caption?.content?.filter(c => c.type !== 'label')?.[0];
                    if (firstCaptionContent) {
                        if (opts?.engine) {
                            try {
                                titleHTML = (await opts.engine.render(firstCaptionContent))?.innerHTML;
                            } catch (e: any) {
                                console.error('Could not render caption html', { message: e.message, id: node.attrs.id });
                            }
                        }
                        return { title: getTextContent(firstCaptionContent), titleHTML: removeUnwantedTagsFromCaption(titleHTML) };
                    }
                }
            }
            break;
        case SFNodeType.heading:
            if (opts?.engine) {
                try {
                    titleHTML = (await opts.engine.render(node))?.outerHTML;
                } catch (e: any) {
                    console.error('Could not render heading html', { message: e.message, id: node.attrs.id });
                }
            }
            return { title: getTextContent(node), titleHTML: removeUnwantedTagsFromCaption(titleHTML) };
    }

    return { title: undefined, titleHTML: removeUnwantedTagsFromCaption(titleHTML) };
}

export const escapeId = (s: string) => s.replace(/[^a-zA-Z0-9]/g, '');

const textFromNode = (n) => {
    if (n.text) {
        return n.text;
    }

    if (!n.content) {
        return '';
    }

    return n.content.map(n2 => textFromNode(n2)).join('');
};

/**
 * @deprecated will not extract italics and other elements
 */
export const extractHeadingOfType = (doc: DocumentNode<rootDoc>, engine?: RenderingEngine<HTMLElement, SciFlowDocumentData>, type = SFNodeType.heading): string | undefined => {
    const heading = doc?.content?.find(heading => heading?.type === type);
    if (!heading) { return undefined; }
    return extractTitleFromHeading(heading as HeadingDocumentNode);
}

/**
 * @deprecated will not extract italics and other elements
 */
export const extractTitleFromHeading = (heading: HeadingDocumentNode) => {
    if (heading?.type === SFNodeType.heading || heading?.type === SFNodeType.subtitle) {
        return textFromNode(heading);
    }
}

export const extractHeadingTitle = async (heading: HeadingDocumentNode, engine?: RenderingEngine<HTMLElement, SciFlowDocumentData>): Promise<{ title?: string; titleHTML?: string; } | undefined> => {
    if (!heading) {
        return undefined;
    }
    if (heading.type !== SFNodeType.heading && heading.type !== SFNodeType.subtitle) { throw new Error('Must provide heading for text extraction'); }
    const title = heading.content?.map((n) => n.text).join('');
    // if we have an engine, render the node to get a more complete string
    if (engine && heading.content) {
        const renderedHeading = await engine.renderContent(heading);
        return {
            title,
            titleHTML: (<span>{...renderedHeading}</span>).innerHTML
        }
    }

    return {
        title,
        titleHTML: undefined
    };
}

/**
 * @deprecated will not extract italics and other elements
 */
export const extractTitle = (doc: DocumentNode<rootDoc>): string | undefined => {
    const header = doc?.content && doc?.content.length > 0 ? doc.content[0] : undefined;
    if (header?.type === 'header') {
        const titleNode = header.content && header.content[0];
        if (titleNode) {
            return titleNode.content?.map((n) => n.text).join('');
        }
    }
}

/**
 * Wraps the toc listing into a sectioned structure.
 */
export const wrapTOC = (headings: TocHeading[], level = 1) => {

    let sections: any[] = [];
    let section: any = undefined;
    let i = 0;
    while (i < headings.length) {
        let heading = headings[i];

        if (heading.level === level) {
            section = {
                ...heading,
                content: []
            };
            sections.push(section);
        } else if (heading.level && heading.level > level) {
            let nextHeading;
            if (!section) {
                section = {
                    level,
                    content: []
                };
                sections.push(section);
            }
            nextHeading = headings.findIndex((n, index) => index > i && n.level <= heading.level);
            if (heading.level === level + 1) {
                section.content?.push(...wrapTOC(headings.slice(i, nextHeading === -1 ? headings.length : nextHeading), heading.level));
            } else {
                section.content?.push(...wrapTOC(headings.slice(i, nextHeading === -1 ? headings.length : nextHeading), level + 1));
            }
            i = nextHeading === -1 ? headings.length : nextHeading - 1;
        } else {
            //
        }
        i++;
    }

    return sections;
}

/**
 * Wraps the text into parts (one per h1) with an otherwise linear document inside (e.g. no sub-sections).
 * If the heading 1 node attributes contain meta data it will be transfered into the parts.
 * @param nodes
 */
export const createParts = (nodes: DocumentNode<any>[]): DocumentSnapshotPart[] => {
    const parts: DocumentSnapshotPart[] = [];
    let activePart;
    for (let node of nodes) {
        if (node.type === SFNodeType.part) {

            let attrs = node.attrs || {};

            let type = node.attrs.type || 'chapter', schema = node.attrs.schema || 'chapter';
            const firstNode = node?.content?.[0];
            if (!node.content || (node.content && !(firstNode?.type === SFNodeType.heading && firstNode?.attrs?.level === 1))) {
                schema = 'free';
                type = 'free';
            } else if (firstNode?.type === SFNodeType.heading && firstNode?.attrs?.level === 1) {
                attrs = {
                    ...firstNode.attrs, // merge in the heading attributes
                    ...attrs
                };
            }

            parts.push({
                partId: attrs.id || createId(),
                type,
                schema,
                options: {
                    numbering: attrs.numbering,
                },
                role: attrs.role,
                placement: attrs.placement,
                document: {
                    type: SFNodeType.document,
                    attrs: { type },
                    content: node.content
                }
            });
            continue;
        }

        if (node.type === SFNodeType.header) {
            parts.push({
                partId: 'title',
                type: 'title',
                schema: 'title',
                document: {
                    type: SFNodeType.document,
                    attrs: { type: 'chapter' },
                    content: node.content
                }
            });
        } else if (node.type === SFNodeType.heading && node.attrs?.level === 1) {
            activePart = {
                partId: node.attrs.id || createId(),
                type: node.attrs.type || 'chapter',
                schema: node.attrs.schema || 'chapter',
                options: {
                    numbering: node.attrs.numbering,
                },
                role: node.attrs.role,
                placement: node.attrs.placement,
                document: {
                    type: SFNodeType.document,
                    attrs: { type: 'chapter' },
                    content: [node] // add the heading
                }
            };
            delete node.attrs.numbering;
            delete node.attrs.placement;
            parts.push(activePart);
        } else {
            // add to the current part
            if (!activePart) {
                // create an empty part if the document did not begin with a heading 1
                activePart = {
                    partId: 'chapter',
                    type: 'chapter',
                    schema: 'chapter',
                    document: {
                        type: SFNodeType.document,
                        attrs: { type: 'chapter' },
                        content: []
                    }
                };
                parts.push(activePart);
            }

            activePart.document.content.push(node);
        }

    }

    return parts;
};

/**
 * Wraps text into section nodes using headings.
 * This can be used to render documents that require a strict hierarchy like JATS XML.
 * @param nodes the node list
 * @param level the base level the sections start at
 */
export const createSectionHierarchy = (nodes: DocumentNode<any>[], level = 1) => {
    const sections: DocumentNode<SFNodeType.section>[] = [];

    // we will never modify the original array of nodes
    let nodeList = Object.freeze([...nodes.map(n => Object.freeze(n))]).map(node => {
        if (typeof node.attrs?.level === 'string') { node.attrs.level = Number.parseInt(node.attrs.level); }
        return node;
    });

    // a section at the level handed to the function
    let section: DocumentNode<SFNodeType.section> | undefined;

    let i = 0;
    while (i < nodeList.length) {
        let currentNode = nodeList[i];
        if (currentNode.type === SFNodeType.heading) {
            // we start a new base level section and wrap all children inside it
            if (currentNode.attrs.level === level) {
                // create a new wrapper section with the current heading node
                section = {
                    type: SFNodeType.section,
                    attrs: {
                        id: 's-' + (currentNode.attrs?.id || 'not-unique'),
                        level
                    },
                    content: [currentNode]
                }
                sections.push(section);
            } else {
                // a heading at a different level ()
                const difference = currentNode.attrs?.level - level;
                let nextSectionIndex, subsecs;

                if (!section) {
                    section = {
                        type: SFNodeType.section,
                        attrs: {
                            id: 's-v-' + nodes[0]?.attrs?.id,
                            level
                        },
                        content: []
                    };
                    sections.push(section);
                }

                if (difference > 1) {
                    // we jumped heading levels (e.g. from 1 to 3). This does happen so we just wrap the heading onto a section
                    // instead  of calling wrap sections with the node's heading level we use level + 1 to nest sections until we reach the right level
                    nextSectionIndex = nodeList.findIndex((n, index) => index > i && n.type === SFNodeType.heading && n.attrs?.level <= currentNode.attrs?.level);
                    // set the index to the end if no other sections are following
                    if (nextSectionIndex === -1) { nextSectionIndex = nodeList.length; }
                    subsecs = createSectionHierarchy(nodeList.slice(i, nextSectionIndex), level + 1);
                } else {
                    // we found a sub-section at level currentNode.attrs?.level
                    // find the next higher up heading (excluding the current heading at i)
                    nextSectionIndex = nodeList.findIndex((n, index) => index > i && n.type === SFNodeType.heading && n.attrs?.level <= currentNode.attrs?.level);
                    // set the index to the end if no other sections are following
                    if (nextSectionIndex === -1) { nextSectionIndex = nodeList.length; }
                    // get all nodes that should be part of this sub-section
                    const no = nodeList.slice(i, nextSectionIndex);
                    // push the sub-sections
                    subsecs = createSectionHierarchy(no, currentNode.attrs?.level);
                }

                section?.content?.push(...subsecs);
                // move the index before the next heading node we should process (since i is incremented at the end)
                i = nextSectionIndex - 1;
            }
        } else {
            // process non-heading nodes
            // we found a node in the beginning that is not a heading, so we create an empty section for it
            if (!section) {
                section = {
                    type: SFNodeType.section,
                    attrs: {
                        id: 's-' + nodes[0]?.attrs?.id,
                        level
                    },
                    content: []
                }
                sections.push(section);
            }

            section.content?.push(currentNode);
        }
        i++;
    }
    return sections;
}

/** Incomplete mapping from a character to their unicode superscript or subscript character */
const supSubMapping = {
    '0': { sup: '\u2070', sub: '\u2080' },
    '1': { sup: '\u00B9', sub: '\u2081' },
    '2': { sup: '\u00B2', sub: '\u2082' },
    '3': { sup: '\u00B3', sub: '\u2083' },
    '4': { sup: '\u2074', sub: '\u2084' },
    '5': { sup: '\u2075', sub: '\u2085' },
    '6': { sup: '\u2076', sub: '\u2086' },
    '7': { sup: '\u2077', sub: '\u2087' },
    '8': { sup: '\u2078', sub: '\u2088' },
    '9': { sup: '\u2079', sub: '\u2089' },
    'a': { sup: '\u1d43', sub: '\u2090' },
    'b': { sup: '\u1d47', sub: undefined },
    'c': { sup: '\u1d9c', sub: undefined },
    'd': { sup: '\u1d48', sub: undefined },
    'e': { sup: '\u1d49', sub: '\u2091' },
    'f': { sup: '\u1da0', sub: undefined },
    'g': { sup: '\u1d4d', sub: undefined },
    'h': { sup: '\u02b0', sub: '\u2095' },
    'i': { sup: '\u2071', sub: '\u1d62' },
    'j': { sup: '\u02b2', sub: '\u2c7c' },
    'k': { sup: '\u1d4f', sub: '\u2096' },
    'l': { sup: '\u02e1', sub: '\u2097' },
    'm': { sup: '\u1d50', sub: '\u2098' },
    'n': { sup: '\u207f', sub: '\u2099' },
    'o': { sup: '\u1d52', sub: '\u2092' },
    'p': { sup: '\u1d56', sub: '\u209a' },
    'r': { sup: '\u02b3', sub: '\u1d63' },
    's': { sup: '\u02e2', sub: '\u209b' },
    't': { sup: '\u1d57', sub: '\u209c' },
    'u': { sup: '\u1d58', sub: '\u1d64' },
    'v': { sup: '\u1d5b', sub: '\u1d65' },
    'w': { sup: '\u02b7', sub: undefined },
    'x': { sup: '\u02e3', sub: '\u2093' },
    'y': { sup: '\u02b8', sub: undefined },
    'A': { sup: '\u1d2c', sub: undefined },
    'B': { sup: '\u1d2e', sub: undefined },
    'D': { sup: '\u1d30', sub: undefined },
    'E': { sup: '\u1d31', sub: undefined },
    'G': { sup: '\u1d33', sub: undefined },
    'H': { sup: '\u1d34', sub: undefined },
    'I': { sup: '\u1d35', sub: undefined },
    'J': { sup: '\u1d36', sub: undefined },
    'K': { sup: '\u1d37', sub: undefined },
    'L': { sup: '\u1d38', sub: undefined },
    'M': { sup: '\u1d39', sub: undefined },
    'N': { sup: '\u1d3a', sub: undefined },
    'O': { sup: '\u1d3c', sub: undefined },
    'P': { sup: '\u1d3e', sub: undefined },
    'R': { sup: '\u1d3f', sub: undefined },
    'T': { sup: '\u1d40', sub: undefined },
    'U': { sup: '\u1d41', sub: undefined },
    'V': { sup: '\u2c7d', sub: undefined },
    'W': { sup: '\u1d42', sub: undefined },
    '+': { sup: '\u207A', sub: '\u208A' },
    '-': { sup: '\u207B', sub: '\u208B' },
    '=': { sup: '\u207C', sub: '\u208C' },
    '(': { sup: '\u207D', sub: '\u208D' },
    ')': { sup: '\u207E', sub: '\u208E' }
};

/**
 * Transforms a string to super/subscript characters.
 * Characters that do not have super/subscript remain the same.
 * @param s the string
 * @param type sup(erscript) or sub(script)
 * @returns the transformed string
 */
export const toSupSubScript = (s: string, type: 'sup' | 'sub'): string => {
    if (!s || typeof s !== 'string') { return s; }
    let superscripted = '';
    for (let i = 0; i < s.length; i++) {
        const char = s[i];
        superscripted = supSubMapping[char]?.[type] ? supSubMapping[char]?.[type] : char;
    }

    return superscripted;
}

/** Takes a country code and returns the localized name. */
export const countryToLabel = (countryCode: 'DE' | string, locale = 'en') => {
    try {
        // @ts-ignore TS2339: Property 'DisplayNames' does not exist on type 'typeof Intl'
        const regionNamesInEnglish = new Intl.DisplayNames([locale], { type: 'region' });
        return regionNamesInEnglish.of(countryCode) || countryCode;
    } catch (e: any) {
        console.error(e.message, { countryCode, locale });
        return countryCode;
    }
}

export const getTextDirection = (locale?: string, dir?: 'auto' | 'rtl' | 'ltr'): 'auto' | 'rtl' | 'ltr' | undefined => {
    if (dir) { return dir; }
    if (locale === 'none') { return 'auto'; }
    if (locale === 'ar') { return 'rtl'; }
    if (locale === 'fa') { return 'rtl'; }
    if (locale === 'fa-IR') { return 'rtl'; }
    try {
        if (locale) {
            const localeObj: any = new Intl.Locale(locale);
            if (localeObj?.textInfo) {
                // not in Firefox/Node
                return localeObj.textInfo?.direction;
            }
        }
    } catch (e: any) {
        console.error(e);
        return 'ltr';
    }

    return 'ltr';
};

/**
 * Searches for any HTTP or HTTPS URLs in its content, and wraps them in <a> tags with
 * `target="_blank"` and `rel="noopener noreferrer"`.
 *
 * @param element - The HTMLElement to process. If null, the function returns null.
 * @returns A new HTMLElement with URLs wrapped in secure anchor tags, or null if 
 * the input is null.
 */
export const wrapUrlLinks = (element: HTMLElement | null) => {
    if (!element) {
        return null;
    }

    const urlRegex = /(https?:\/\/[^\s<]+[^<.,;:"')\]\s])/g;
    const clonedElement = element.cloneNode(true) as HTMLElement;
    const innerHTML = clonedElement.innerHTML;
    const updatedHTML = innerHTML.replace(urlRegex, (url) => {
        return `<a href="${url}" target="_blank" rel="noopener noreferrer">${url}</a>`;
    });

    clonedElement.innerHTML = updatedHTML;

    return clonedElement;
};
