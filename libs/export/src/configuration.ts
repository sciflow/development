import {readFileSync} from 'fs';
import yaml from 'js-yaml';

/**
 * Reads and parses a YAML configuration file.
 *
 * This function attempts to read a YAML file at the specified path and parse its contents into
 * an array of documents. If the file does not exist or an error occurs during reading or parsing,
 * it logs the error to the console and returns an empty array.
 *
 * @param {string} path - The filesystem path to the YAML configuration file.
 * @returns {any[]} An array of documents parsed from the YAML file, or an empty array if an error occurs.
 */
export const readYAMLDocumentsFromFile = (path: string): any[] => {
  try {
    const file = readFileSync(path, 'utf-8');
    return yaml.loadAll(file) || [];
  } catch (error: any) {
    console.error(`Error reading YAML from ${path}:`, error.message);
    throw Error(error);
  }
};

/**
 * Reads a YAML file and returns it as JSON.
 */
export const readYAML = (path: string): object | null => {
  try {
    const file = readFileSync(path, 'utf-8');
    return yaml.load(file) || null;
  } catch (error: any) {
    console.error(`Error reading YAML from ${path}:`, error.message);
    throw Error(error);
  }
};
