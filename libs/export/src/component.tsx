import { renderAuthorList } from '@sciflow/cite';
import { DocumentNode, rootDoc } from '@sciflow/schema';
import { existsSync, readFileSync } from 'fs';
import jp from 'jsonpath';
import Handlebars from 'handlebars';
import { join } from 'path';
import { SassBoolean, SassList, SassNumber, SassString, Value, compile, compileString, sassFalse, sassNull, sassTrue } from 'sass';
import { TSX } from './TSX';
import { SassLogger, createFileImporter } from './css';
import { switchHelper } from './handlebars';
import { decamelize, getConfigName } from './utils/helpers';
import { GenericConfiguration, SciFlowDocumentData } from './interfaces';
import { RenderingEngine } from "./renderers";
import { getCitation } from './xml/renderers/references';
import { JSONSchema7 } from 'json-schema';
import { getContrastingColor } from './colorHelper';

export const ALL_PAGES = undefined;

/**
 * Normalizes SCSS values by removing surrounding quotes if present.
 * @param value - The SCSS string value.
 * @returns The unquoted string.
 */
function stripValue(value: string): string {
    if (!value) { return value; }
    if (value.startsWith('"') && value.endsWith('"')) {
        return value.slice(1, -1);
    }
    return value;
}

type NestedKeyOf<T, Depth extends number = 3> = [Depth] extends [never]
    ? never
    : T extends object
    ? {
        [K in keyof T]-?: K extends string
        ? T[K] extends object
        ? `${K}.${NestedKeyOf<NonNullable<T[K]>, Prev[Depth]>}` | K
        : K
        : never;
    }[keyof T]
    : never;

type Prev = [never, 0, 1, 2, 3, ...0[]];

type NestedValueOf<T, K extends string> = K extends `${infer Key}.${infer Rest}`
    ? Key extends keyof T
    ? Rest extends NestedKeyOf<NonNullable<T[Key]>>
    ? NestedValueOf<NonNullable<T[Key]>, Rest>
    : never
    : never
    : K extends keyof T
    ? T[K]
    : never;

export interface PartForRendering {
    id: string;
    /** An optional title */
    title?: string;
    /**
     * The configuration used to create the part (if any)
     */
    configuration?: GenericConfiguration<any>;
    /** The dom elements to be rendered or a function to be called when inserting into the document
     * (!) If you would like to render the part document instead, use dom as a function to call the engine to render the document.
    */
    dom: HTMLElement[] | ((engine: RenderingEngine<any, any>) => Promise<HTMLElement[] | undefined>);
    /** If a part was written in the editor the content is represented in a ProseMirror document
     * To bring any other parts with headings into the toc, the document node can be used as well.
    */
    document?: DocumentNode<rootDoc>;
    placement?: string;
    /** A page/placement to be used in collections */
    subPage?: string;
    /** 
     * (!) Special handling for CSS Paged Media / PrinceXML where elements (even empty ones cause page breaks
     * so if set to true, the component will be moved into the first section in order to not cause an additional
     * page break. ALso see: https://www.princexml.com/forum/topic/4877/create-complex-headers-that-work-with-page-groups-not-sure for more.
     **/
    moveIntoPageFlow?: boolean;
    /**
     * A hole that can be filled with content from other components (e.g. to facilitate a moveIntoPageFlow).
     */
    contentHole?: PartForRendering[];
    /** Determines whether this part is important to be rendered or can be dropped if a placement is otherwise empty
     * (e.g. if a part only renders headers and footers but the placement has no other parts it would not be needed)
     */
    notDefining?: boolean;

    /** Whether a part should be rendered as a standalone document */
    standalone?: boolean;
    /** The document data of a standalone document */
    parent?: any;
    /** The template of a standalone document */
    template?: any;
}

/**
 * Additional options given to a component (usually as part of the @Component decorator)
 */
export interface TemplateComponentOptions {
    /**
     * A readme that explains what the component does to an end user configuring it.
     */
    readme?: string;
    /** **A scss string**
     * 
     * You can use *get(key, defaultValue)* to retrieve values from the configuration.
     */
    styles?: string;
    /**
     * An array of scss resources.
     */
    styleUrls?: string[];
    /**
     * A handlebar resource that should be rendered when toDOM is called (also @see template).
     * (!) If template is set, it takes precedence.
     */
    templateUrl?: string;
    /**
     * A handlebar resource that should be rendered when toDOM is called (also @see templateUrl)
     */
    template?: string;
    /**
     * A custom DOM renderer that overwrites the default implementation.
     */
    renderDOM?: any;
    /**
     * Whether to render on a second pass of the a PDF generation.
     */
    secondPass?: boolean;
}

/**
 * Decorator function for template components (@Component({ .. })).
 */
export function Component(decorations?: TemplateComponentOptions) {
    return (target) => {
        target.prototype.decorations = decorations;
        return target;
    }
}

/**
 * Meta data was traditionally stored directly inside the metaData root object (and not prefixed by config names).
 * We thus support access without including the config names (e.g. articleType instead of ArticleMeta.articleType)
 */
const getValueFromMetaData = (objectPath: string, documentMetaData: any, templateSchema: JSONSchema7) => {

    // meta data will either be stored at the root or inside the TemplateMetaData prefix.
    // This is also where the schema lives

    // the meta data schema may be used for fallbacks
    const templateMetaDataSchema = templateSchema?.properties?.['TemplateMetaData'];

    {
        // try to find the value inside the TemplateMetaData object
        // e.g. TemplateMetaData.themeColor
        const value = getValueFromObject(objectPath, documentMetaData?.['TemplateMetaData']);
        if (value) { return value; }
    }

    {
        // try to find the value directly in the meta data without any nestimgs/ prefixes
        // e.g. themeColor
        const value = getValueFromObject(objectPath, documentMetaData);
        if (value) { return value; }
    }

    {
        // try to find the value inside the template meta data schema
        const value = getValueFromSchema(objectPath, templateMetaDataSchema);
        if (value && value.default !== undefined) { return value?.default; }
    }

    {
        // fall back to the main schema
        const value = getValueFromSchema(objectPath, templateSchema);
        if (value && value.default !== undefined) { return value?.default; }
    }

    return undefined;
}

/** 
 * Gets a value from the provided component and meta data
 */
const getValue = (objectPath: string, configName: string, documentMetaData: any, templateSchema: JSONSchema7) => {
    // when getting values from scsss, they might be quoted
    if (!objectPath) { return undefined; }
    objectPath = stripValue(objectPath);
    configName = stripValue(configName);

    /** The document's meta data for this publication */
    /** The template schema with the applied locale and variants, and configuration as default values */

    /** The user's meta data responsible for configuring the current component
     * e.g. Typography.h1.fontSize (object path would be h1.fontSize) with configName as Typography
     */
    const metaDataForConfig = documentMetaData?.[configName];
    {
        const value = getValueFromObject(objectPath, metaDataForConfig);
        if (value) { return value; }
    }

    /** Get the default value from the schema if no meta data was present */
    const schemaForConfig = templateSchema?.properties?.[configName];
    {
        const value = getValueFromSchema(objectPath, schemaForConfig);
        if (value && value.default !== undefined) { return value.default; }
    }

    return undefined;
}

/**
 * A component that emits CSS and possibly DOM into the document.
 */
export class TemplateComponent<ConfigurationSpecSchema, _DataFormat extends SciFlowDocumentData> {
    /**
     * The decamelized name of the configuration (e.g. journal-title from kind: JournalTitle)
     * This is used to find the configuration in the resource paths.
     * The key may not be unique if multiple configurations of the same kind exist (with different pages).
     */
    public get key() { return decamelize(this.kind); }

    private _uniqueKey?: string;

    /** Decorations injected by the @Component decoration */
    private decorations?: TemplateComponentOptions;

    /**
     * A unique identifier for the configuration (including the kind and page).
     * @deprecated use uniqueKey for now
     */
    public get id() {
        this.engine.log('warn', 'Deprecated use of get id for ' + this.uniqueKey);
        return this.key + 'For' + this.page?.toUpperCase();
    }

    /** The template configuration values set for this component */
    public get conf(): GenericConfiguration<ConfigurationSpecSchema> {
        if (this._conf) { return this._conf; }
        // fall back in case the component was created without a conf attached
        return this.engine.data.configurations?.find(c => c.kind === this.kind && c.page === this._conf?.page);
    }

    /**
     * Returns a unique human readable key for this component.
     */
    public get uniqueKey() {
        return this._uniqueKey || this.key;
    }
    /** The kind of component (equal to the class name of the template component implementation) */
    public get kind(): string { return this._conf?.kind || this.constructor.name; };
    public get metadata() { return this.conf?.metadata || {}; };
    /** Gets the page (front, body, .., ALL_PAGES) the component was created */
    public get page(): string | undefined { return this.conf?.page || ALL_PAGES; };
    /** Gets the sub page (front, body, .., ALL_PAGES) the component was created (inside collections/sub-parts) */
    public get subPage(): string | undefined { return this.conf?.subPage; };
    /**
     * Returns translations for values by locale.
     */
    public get translations(): { [locale: string]: ConfigurationSpecSchema } | undefined {
        return this.conf?.translations;
    }

    constructor(
        /** the rendering engine for the actual document data and meta data */
        public engine: RenderingEngine<HTMLElement, SciFlowDocumentData>,
        private readonly _conf?: GenericConfiguration<ConfigurationSpecSchema>,
        protected options?: TemplateComponentOptions,
    ) {
        this.options = {
            ...(this.decorations || {}),
            ...(this.options || {})
        };

        this._uniqueKey = this.conf ? getConfigName(this.conf) : this.key;
        if (this.options?.renderDOM) { this.options.renderDOM = this.options.renderDOM.bind(this); }
    }

    /**
     * Sets a template URL to use to render the DOM.
     */
    public setTemplateUrl(path: string) {
        if (!this.options) { this.options = {}; }
        this.options.templateUrl = path;
    }

    /**
     * Sets a template to use to render the DOM.
     */
    public setTemplate(content: string) {
        if (!this.options) { this.options = {}; }
        this.options.template = content;
    }

    /**
     * Returns a value from the document meta data or meta data template schema defaults (legacy support).
     * (!) We recommend using standardized meta data for JATS/XML instead of custom meta data schemas.
     */
    public getTemplateMetaData<K extends NestedKeyOf<ConfigurationSpecSchema>>(path: K): any | undefined {
        return getValueFromMetaData(path, this.engine.data.metaData, this.engine.data.metaDataSchema);
    }

    /**
     * Returns a value from the document meta data or template schema defaults.
     */
    public get<K extends NestedKeyOf<ConfigurationSpecSchema>>(path: K, locale?: string): NestedValueOf<ConfigurationSpecSchema, K> | undefined {
        if (!this.conf) { return undefined; }

        // FIXME add locales here
        // while the schema is compile with a specific language a bi-lingual publication may still request a different locale at times
        const translations = (locale && this.conf.translations?.[locale]) || {};

        // the name under which the current component is referenced in the schema and meta data
        const configName = this.conf && getConfigName(this.conf as GenericConfiguration<any>);

        return getValue(path, configName, this.engine.data.metaData, this.engine.data.metaDataSchema);
    }

    /**
     * Gets the value from a component with a different kind (only works for unique kinds like Page or Typography)
     * @param kind the kind
     * @param path the path
     */
    public getFromKind<U, K extends keyof U>(kind: string, path: string): U[K] | undefined {
        const configurations = this.engine.data.configurations?.filter(c => c.kind === kind) || [];
        if (configurations.length > 1) { this.engine.log('warn', path + ' value for kind ' + kind + ' had multiple possible matches'); }
        if (configurations.length === 0) { return undefined; }
        const configuration = configurations[0];

        const configName = getConfigName(configuration as GenericConfiguration<any>);
        return getValue(path, configName, this.engine.data.metaData, this.engine.data.metaDataSchema);
    }

    /**
     * Gets a meta data value or its default.
     * @param path the meta data path (ignoring all typings)
     * @returns the value if it exists or a default if not.
     */
    public getAs<T>(path: string): T | undefined {
        return this.get(path as any) as T;
    }

    /**
     * Returns the configuration as an object (using the schema, the template configuration and any meta data from the document)
     */
    public getValues(): ConfigurationSpecSchema {
        const paths = getPaths('', this.engine.data.metaDataSchema?.properties?.[this.uniqueKey] || {});
        const values = {};
        for (let path of paths) {
            const value = this.getAs<any>(path);
            if (value != undefined) {
                if (typeof value !== 'object') {
                    setValue(path, values, value);
                }
            }
        }
        return values as ConfigurationSpecSchema;
    }

    /**
     * Retrieves a value from component configurations.
     */
    private getValue(key: SassString | string, configuration = this.conf) {
        return getValueForKind(key, configuration, this.engine.data.metaData, this.engine.data.metaDataSchema);
    }

    /**
     * Renders a provided template/templateUrl using Handlebars.
     * If a component provides their own renderDOM, it is called instead.
     */
    public async renderDOM(defaults?: { runner?: string; componentPaths?: string[]; data?: unknown; }): Promise<PartForRendering | undefined> {
        if (typeof this.options?.renderDOM === 'function') {
            return this.options?.renderDOM(defaults);
        }

        const componentPaths = defaults?.componentPaths || [];
        if (!componentPaths.includes(__dirname)) {
            componentPaths.push(__dirname);
        }

        let content;
        // template takes precedence over template url
        if (this.options?.template) {
            content = this.options.template;
        } else if (this.options?.templateUrl) {
            let file = existsSync(this.options?.templateUrl) ? this.options?.templateUrl : undefined;
            if (!file) {
                const possiblePaths = componentPaths.map(path => join(path, 'components', this.key, this.options?.templateUrl as string));
                file = possiblePaths.find(path => existsSync(path));
            }
            if (file) {
                content = readFileSync(file, 'utf-8');
            }
        }

        if (content) {
            try {
                const format = new Intl.DateTimeFormat(this.engine.data.document.locale || 'en-US', { month: 'long', day: 'numeric', year: 'numeric', timeZone: 'UTC' });
                const document = this.engine.data.document;
                const formatDate = (date: string) => date ? format.format(new Date(date)) : '';
                const getYear = (date: string) => {
                    if (typeof date === 'string') {
                        return (new Date(date)).getFullYear();
                    }
                    return (new Date())?.getFullYear();
                };
                /** Finds an image by the file id */
                const image = (id, alt, scale) => {
                    if (!alt || typeof alt !== 'string') { alt = ''; }
                    if (!scale || typeof scale === 'object') {
                        scale = 1;
                    }
                    const image: any = this.engine.data.document.files?.find(asset => asset.id === id);

                    if (!image) {
                        return '';
                    }

                    const img = <img alt={image.alt || alt} class={id} src={image.compressedUrl || image.url} />;
                    let style = '';
                    const width = Number.isInteger(image?.dimensions?.width) ? `${image?.dimensions?.width}px` : image?.dimensions?.width;
                    const height = Number.isInteger(image?.dimensions?.height) ? `${image?.dimensions?.height}px` : image?.dimensions?.height;
                    if (image?.dimensions?.width) { style += `width:calc(${width} * ${scale});` }
                    if (image?.dimensions?.height) { style += `height:calc(${height} * ${scale});` }
                    if (style?.length > 0) { img.setAttribute('style', style); }
                    return img.outerHTML;
                };
                const getUniqueKey = () => this.uniqueKey;
                const getPage = () => this.page;

                /**
                 * Returns the contrast of a hex color value
                 */
                const contrastColor = (hexColor: string): string => {
                    if (!hexColor || !hexColor.startsWith('#')) { return "#FF000"; }
                    return getContrastingColor(hexColor);
                }

                /**
                 * Gets a value from a path. If the path is prefixed with ComponentName: the component will be used.
                 * Otherwise the component that hosts the hbs file will be used.
                 */
                const get = (objectPath, defaultValue, options) => {
                    if (typeof defaultValue === 'object') {
                        options = defaultValue;
                        defaultValue = undefined;
                    }

                    let value = undefined;

                    const paths = objectPath?.split(':');
                    if (paths.length === 1) {
                        value = this.get(paths[0]) ?? defaultValue;
                    } else if (paths.length === 2) {
                        value = this.getFromKind(paths[0], paths[1]) ?? defaultValue;
                    }

                    if (!value && this.engine.isDebug) {
                        this.engine.log('warn', 'No value for ' + objectPath, { key: this.uniqueKey });
                    }

                    return value;
                };
                const getMetaData = (objectPath, defaultValue, options) => {
                    if (typeof defaultValue === 'object') {
                        options = defaultValue;
                        defaultValue = undefined;
                    }
                    return this.getTemplateMetaData<any>(objectPath) ?? defaultValue;
                };
                const exists = (objectPath, defaultValue, options) => {
                    if (typeof defaultValue === 'object') {
                        options = defaultValue;
                        defaultValue = undefined;
                    }

                    let value = undefined;
                    const paths = objectPath?.split(':');
                    if (paths.length === 1) {
                        value = this.get(paths[0]) ?? defaultValue;
                        if (!value) {
                            // try to find the value in the meta data
                            value = this.getTemplateMetaData(paths[0]) ?? defaultValue;
                        }
                    } else if (paths.length === 2) {
                        value = this.getFromKind(paths[0], paths[1]) ?? defaultValue;
                    }

                    return value != undefined;
                };

                const getCitationStyleXML = (locale?: string) => {
                    if (!locale || this.engine.data.citationRenderer?.locale === locale) { return this.engine.data.citationRenderer?.localeXML; }
                    // TODO we do not cache other locales so we fall back to en-US instead of loading the different locale
                    // const locale = this.getAs<string>('formatting.authorNames.locale') || this.engine.data.citationRenderer?.locale;
                    return undefined; // this will cause a fallback to en-US
                }

                const renderAuthorNames = () => {
                    const preset = this.getAs<string>('formatting.authorNames.preset');
                    return renderAuthorList(preset as any, this.citation, false, getCitationStyleXML(this.getAs<string>('formatting.authorNames.locale')));
                };
                const hasEditors = (): boolean => this.citation?.editor?.length > 0;
                const renderEditorNames = () => {
                    return renderAuthorList('editorsOnly', this.citation, false, getCitationStyleXML(this.getAs<string>('formatting.authorNames.locale')));
                };
                /** Retrieves a value from an object */
                const getFrom = (object, path: string) => {
                    const queryResult = jp.query(object, path);
                    let result: any | null = null;
                    if (queryResult?.length > 0 && queryResult[0] != undefined) { result = queryResult[0]; }
                    if (result && typeof result === 'string' && result.trim().length === 0) { return null; }
                    return result;
                };
                const renderAuthorName = (author) => {
                    const preset = this.getAs<string>('formatting.authorNames.preset');
                    return renderAuthorList(preset as any, {
                        ...this.citation,
                        author: [{
                            name: author.name,
                            family: author.lastName,
                            given: author.firstName
                        }]
                    }, false, getCitationStyleXML(this.getAs<string>('formatting.authorNames.locale')));
                };
                const getDocumentValue = (objectPath, defaultValue) => {
                    if (typeof defaultValue === 'object') { defaultValue = undefined; }
                    const value = getValueFromObject(objectPath, document);
                    return value ?? defaultValue;
                }

                /**
                 * Retrieves additional data that was passed as the data argument in the render function.
                 */
                const getRenderData = (objectPath, defaultValue) => {
                    if (typeof defaultValue === 'object') { defaultValue = undefined; }
                    const value = getValueFromObject(objectPath, defaults?.data || {});
                    return value || defaultValue;
                };

                const getCalculatedProperty = (key: string) => this.getCalculatedProperty(key);

                Handlebars.registerHelper('formatDate', (dateString) => formatDate(dateString));
                Handlebars.registerHelper('equals', (arg1, arg2, options) => (arg1 == arg2) ? options.fn(this) : options.inverse(this));
                Handlebars.registerHelper('startsWith', (arg1, arg2, options) => arg1 && arg2 && arg1.startsWith(arg2) ? options.fn(this) : options.inverse(this));
                Handlebars.registerHelper('isPagedMedia', () => defaults?.runner && ['princexml', 'pagedjs'].includes(defaults?.runner)),
                Handlebars.registerHelper('image', image);
                Handlebars.registerHelper('get', get);
                Handlebars.registerHelper('contrastColor', contrastColor);
                Handlebars.registerHelper('getMetaData', getMetaData);
                Handlebars.registerHelper('exists', exists);
                Handlebars.registerHelper('getCalculatedProperty', getCalculatedProperty);
                Handlebars.registerHelper('getPage', getPage);
                Handlebars.registerHelper('getRenderData', getRenderData);
                Handlebars.registerHelper('getDocumentValue', getDocumentValue);
                Handlebars.registerHelper('getYear', getYear);
                Handlebars.registerHelper('getUniqueKey', getUniqueKey);
                Handlebars.registerHelper('renderAuthorNames', renderAuthorNames);
                Handlebars.registerHelper('hasEditors', hasEditors);
                Handlebars.registerHelper('renderEditorNames', renderEditorNames);
                Handlebars.registerHelper('renderAuthorName', renderAuthorName);
                Handlebars.registerHelper('getFrom', getFrom);

                const template = Handlebars.compile(content, {
                    knownHelpers: {
                        get: true,
                        image: true
                    }
                });

                const values = this.getValues();
                const result = template({ values }, { helpers: { ...switchHelper } });
                // TODO check whether there is a root element
                const domNodes = TSX.htmlStringToElements(result);
                let dom;
                if (domNodes.length === 0) {
                    throw new Error('Could not render ' + this.key);
                }

                for (let node of domNodes) {
                    const styleNodes = node?.querySelectorAll('style');
                    for (let styleNode of Array.from(styleNodes)) {
                        styleNode.innerHTML = `/* styles for ${this.key} */\n` + styleNode.innerHTML;
                    }
                }

                return {
                    id: this.uniqueKey,
                    placement: this.page,
                    subPage: this.subPage,
                    dom: domNodes
                };
            } catch (e: any) {
                return {
                    id: this.uniqueKey,
                    placement: this.page,
                    subPage: this.subPage,
                    dom: <span style="color: red;">{e.message}</span>
                }
            }
        }
    }

    /**
     * Returns a custom property on the component.
     */
    public getCalculatedProperty(key: string) {
        if (!this[key] == undefined) { throw new Error(this.uniqueKey + ' does not have a property ' + key); }
        return this[key];
    }

    /**
     * Gets a schema definition.
     * @param objectPath the path to the definition
     * @returns the schema definition for a property
     */
    public getDefinition(objectPath: string) {
        // when getting values from scsss, they might be quoted
        if (objectPath.startsWith('"') && objectPath.endsWith('"')) { objectPath = objectPath.slice(1, objectPath.length - 1); }

        const configName = this.conf && getConfigName(this.conf as GenericConfiguration<any>);
        return getDefinitionFromSchema(objectPath, this.engine.data.metaDataSchema?.properties[configName]);
    }

    /** The author names as a string */
    public get authorNames(): string {
        const preset = this.getAs<string>('formatting.authorNames.preset');
        let localeXML;

        if (!this.getAs<string>('formatting.authorNames.locale') || this.engine.data.citationRenderer?.locale === this.getAs<string>('formatting.authorNames.locale')) {
            localeXML = this.engine.data.citationRenderer?.localeXML;
        } else {
            // TODO we do not cache other locales so we fall back to en-US instead of loading the different locale
            // const locale = this.getAs<string>('formatting.authorNames.locale') || this.engine.data.citationRenderer?.locale;
            localeXML = undefined; // this will cause a fallback to en-US
        }

        return renderAuthorList(preset as any, this.citation, false, localeXML);
    }

    getCitationWith(appendSubtitle?: string) {
        if (!this.engine.data.documentSnapshot) { throw new Error('Document snapshot must be provided'); }
        return getCitation(this.engine.data.documentSnapshot, { metaDataSchema: this.engine.data.metaDataSchema, appendSubtitle });
    }

    public get citation() {
        return this.getCitationWith();
    }

    /** Renders the component's CSS styles */
    public renderStyles(defaults?: { runner, assetBasePaths, componentPaths }): string {

        let styleUrls;
        try {
            const getSassValue = (value: any) => {
                if (!value) { return sassNull; }
                if (value instanceof SassList) { return value; }
                if (value instanceof SassBoolean) { return value; }
                if (value === true || value === false) { return value ? sassTrue : sassFalse; }

                if (Array.isArray(value)) {
                    return new SassList(value.map(getSassValue));
                } else if (Number.isInteger(value)) {
                    return new SassNumber(value);
                } else if (Number.isFinite(value)) {
                    return new SassNumber(value);
                }
                return (value instanceof Value ? value : new SassString(value));
            }

            const styles = this.options?.styles;
            styleUrls = this.options?.styleUrls;
            if (this.kind === 'Page' && !styleUrls && !styles) {
                // we use this as a fail-safe if decorators break for some reason
                // page component should always have styles
                this.engine.logging?.logger?.error('Expecting the page component to have styleUrls');
            }

            const importers = [createFileImporter(defaults?.assetBasePaths || [])];
            const functions: any = {
                'getUniqueKey()': () => {
                    return new SassString(this.uniqueKey);
                },
                'getPage()': () => {
                    // include the document id in the page name for multi document exports
                    return this.page ? new SassString(this.page) : sassNull;
                },
                'getCoverPageName()': () => {
                    // include the document id in the page name for multi document exports
                    return new SassString('cover');
                },
                'getFrontPageName()': () => {
                    // include the document id in the page name for multi document exports
                    return new SassString('front');
                },
                'getBodyPageName()': () => {
                    // include the document id in the page name for multi document exports
                    return new SassString('body');
                },
                'getBackPageName()': () => {
                    // include the document id in the page name for multi document exports
                    return new SassString('back');
                },
                'isPagedMedia()': () => {
                    return ['princexml', 'pagedjs'].includes(defaults?.runner) ? sassTrue : sassFalse;
                },
                'isDebug()': () => this.engine.isDebug ? sassTrue : sassFalse,
                'isRunner($name)': ([runner]) => {
                    return (runner?.toString() === `"${defaults?.runner}"`) ? sassTrue : sassFalse;
                },
                'exists($key, $kind: null)': ([key, kind]) => {

                    if (kind !== sassNull && kind?.toString()?.length > 0) {
                        const val = this.getFromKind(stripValue(kind.toString()), stripValue(key.toString()));
                        if (val !== undefined) { return sassTrue; }
                        return sassFalse;
                    }

                    const retval = this.getAs<any>(stripValue(key.toString()));
                    if (retval != undefined) {
                        return sassTrue;
                    }

                    return sassFalse;
                },
                'getCalculatedProperty($key, $defaultValue: null)': ([key, defaultValue, kind]) => {
                    key = key.toString();
                    const value = this.getCalculatedProperty(key);
                    if (!value && defaultValue) { return defaultValue; }
                    return getSassValue(value);
                },
                'getKeys($key)': ([key]) => {
                    const value = getValueFromObject(stripValue(key.toString()), this.getValues());

                    if (typeof value === 'object') {
                        return getSassValue(Object.keys(value));
                    }

                    return getSassValue([]);
                },
                'get($key, $defaultValue: null, $kind: null)': ([key, defaultValue, kind]) => {
                    if (defaultValue === sassNull) { defaultValue = null; }

                    let retval;
                    if (kind !== sassNull && kind?.toString()?.length > 0) {
                        retval = this.getFromKind(stripValue(kind.toString()), stripValue(key.toString())) ?? defaultValue;
                    } else {
                        retval = this.getAs<any>(stripValue(key.toString())) ?? defaultValue;
                    }

                    if (retval == undefined) {
                        const definition = this.getDefinition(key.toString());
                        this.engine.log('error', 'No value for ' + key.toString() + ' and no default value', { definition });
                        throw new Error(`${kind?.toString() || this.kind} No value for ${key?.toString()}`);
                    }

                    return getSassValue(retval);
                },
                'getDocumentValue($key)': ([key]) => {
                    const keyValue = (key as SassString).toString();
                    if (!document[keyValue]) {
                        const definition = this.getDefinition(key.toString());
                        this.engine.log('error', 'No value for ' + key.toString(), { definition });
                        throw new Error('No value for ' + keyValue);
                    }
                    const retval = new SassString(document[keyValue]);
                    return retval;
                }
            };

            let renderedStyles: string[] = [];
            const componentPaths = defaults?.componentPaths || [];
            if (!componentPaths.includes(__dirname)) {
                componentPaths.push(__dirname);
            }
            if (styleUrls && styleUrls?.length > 0) {
                for (let url of styleUrls) {
                    const possiblePaths = componentPaths.map(path => join(path, 'components', this.key, url));
                    const file = possiblePaths.find(path => existsSync(path));
                    if (file) {
                        // TODO do we want the source maps here?
                        const s = compile(file, { importers, functions, logger: SassLogger });
                        if (s.css?.length > 0) {
                            renderedStyles.push(s.css);
                        }
                        continue;
                    } else {
                        console.error('Could not find scss resource', { kind: this.kind, url, possiblePaths });
                    }
                }
            }

            if (styles) {
                const s = compileString(styles, { importers, functions, logger: SassLogger });
                if (s.css?.length > 0) {
                    renderedStyles.push(s.css);
                }
            }

            if (renderedStyles.length > 0) {
                renderedStyles = [`/* styles for ${this.uniqueKey} */\n`, ...renderedStyles];
            }

            return renderedStyles.join('\n\n');
        } catch (e: any) {
            this.engine.log('error', `Could not render styles for ${this.uniqueKey}`, {
                kind: this.kind,
                message: e.message
            });
            return '';
        }
    }
}

/**
 * Gets a value either from the meta data or the schema defaults.
 * @param paths an array of "." separated path to an object property
 * @param metaData an object with user provided meta data
 * @param schema a JSON schema in the structure of the data
 * @param translations object with translations
 * @returns a value in order: meta data, translation value or schema default if nothing else is found. May be undefined
*/
export const getValueOrDefault = (paths: string | string[], metaData = {}, schema: JSONSchema7 = {}, translations: any = {}): any | undefined => {
    if (Array.isArray(paths)) {
        let v: any = undefined;
        for (let path of paths) {
            // if the user provided meta data for this key it has to be used
            const value = getValueFromObject(path, metaData || {});
            if (value) { return value; }

            // the default values from the JSON schema (that has been compiled with the template configuration)
            const defaultValue = getValueFromSchema(path, schema || {});
            v = defaultValue?.default;
            if (v) { return v; }
        }
        return v;
    } else {
        return getValueOrDefault([paths], metaData, schema, translations);
    }
}

export const getDefinitionFromSchema = (path: string, object) => {
    if (!path || !object) { return undefined; }
    if (object?.properties?.[path]) {
        return object?.properties[path];
    }
    const segments = path.split('.');
    if (segments.length === 0) { return undefined; }
    if (object?.properties?.[segments[0]] === undefined) { return undefined; }
    return getDefinitionFromSchema(segments.slice(1, segments.length).join('.'), object?.properties[segments[0]]);
}

/** Gets a value from an object if provided with a path (e.g. object.key.key2 ) */
export const getValueFromObject = (path: string, object: any) => {
    if (!path || !object) { return undefined; }
    if (object[path] != undefined) { return object[path]; }
    const segments = path.split('.');
    if (segments.length === 0) { return undefined; }
    if (object[segments[0]] === undefined) { return undefined; }
    return getValueFromObject(segments.slice(1, segments.length).join('.'), object[segments[0]]);
}

/** Gets a value in an object if provided with a path (e.g. object.key.key2 ) */
export const setValue = (path: string, object: any, value: any) => {
    if (!path) { return object; }

    const segments = path.split('.');
    if (segments.length === 1) {
        if (!object) { object = {}; }
        object[path] = value;
        return object;
    } else {
        object[segments[0]] = setValue(segments.slice(1, segments.length).join('.'), object[segments[0]] || {}, value);
        return object;
    }
}

/**
 * Gets a value for a provided key and configuration.
 * @param key the path to the property (e.g. typography.h1.fontSize)
 * @param configuration a component configuration object
 * @param metaData the user provided meta data
 * @param metaDataSchema the template's meta data schema (with default values) 
 * @returns a configuration value
 */
export const getValueForKind = (key: SassString | string, configuration, metaData, metaDataSchema) => {
    try {
        const lookup = (key instanceof SassString ? key.toString() : key)?.replace(/"/gi, '',);

        if (!configuration) { return undefined; }
        const configurationNameWithPage = getConfigName(configuration);
        // get the default from the schema (priority #3)
        const configurationValue = getValueFromObject(lookup, configuration.spec);
        // read defaults from the meta data schema (and set a fallback if there, priority #2)
        const schema = getValueFromSchema(lookup, metaDataSchema?.properties?.[configurationNameWithPage] || {});
        // get the any user provided values (priority #1)
        const userSetMetaDataValue = getValueFromObject(configurationNameWithPage + '.' + lookup, metaData || {});
        return userSetMetaDataValue ?? schema?.default ?? configurationValue;
    } catch (e: any) {
        console.error('Could not extract value for ' + key, { message: e.message });
    }
}

/** Gets a value from an object if provided with a path (e.g. object.key.key2 ) */
const getValueFromSchema = (path: string, object: any): { type: string; default: any;[key: string]: any; } | undefined => {
    if (!path || !object) { return undefined; }
    if (object?.properties?.[path]) { return object?.properties[path]; }
    const segments = path.split('.');
    if (segments.length === 0) { return undefined; }
    if (object?.properties?.[segments[0]] === undefined) { return undefined; }
    return getValueFromSchema(segments.slice(1, segments.length).join('.'), object?.properties[segments[0]]);
}

/**
 * Returns all possible paths in a schema.
 */
export const getPaths = (prefix: string, schema): string[] => {
    let paths: string[] = [];
    if (!schema?.properties) { return [prefix]; }
    for (let key of Object.keys(schema.properties)) {
        //obj[key] = { ...(obj[key]  || {}), ...deepMerge(prefix + '.' + key, schema.properties[key]) };
        paths = [...paths, ...getPaths((prefix ? prefix + '.' : prefix) + key, schema.properties[key])];
    }

    return [prefix, ...paths];
}

export const mini = (styles: string, dom: any) => {
    return {
        styles,
        dom
    }
}
