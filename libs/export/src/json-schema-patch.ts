import { applyPatch, Operation } from 'fast-json-patch';
import Ajv from 'ajv';
import jp from 'jsonpath';
import { GenericConfiguration } from './interfaces';
import { VariantConfiguration } from './html/components/variant/variant.schema';

export const matchTargetSchema = (target, configuration) => {
    if (target?.metadata?.name) { return configuration?.component?.metadata?.name === target?.metadata?.name; }
    return configuration.component?.kind === target?.kind;
};
export const matchTargetYaml = (target, configuration) => {
    if (target?.metadata?.name) { return configuration?.metadata?.name === target?.metadata?.name; }
    return configuration?.kind === target?.kind;
}

export const popFromPath = (path: string, segmentsToPop: number = 0) => {
    // Remove the leading slash and split the path
    const pathSegments = path.substring(1).split('/');
    // Determine the number of segments to truncate
    const segmentsToKeep = Math.max(0, pathSegments.length - segmentsToPop);
    return '/' + pathSegments.slice(0, segmentsToKeep).join('/');
};

export const compileJsonPathFromPatchPath = (path: string): string => {
    const pathSegments = path.substring(1).split('/');

    // Convert each path segment to JSONPath format
    const jsonPathSegments = pathSegments.map(segment => {
        // If the segment is a number, don't wrap it in quotes
        return isNaN(Number(segment)) ? `['${segment}']` : `[${segment}]`;
    });

    // Join the segments to form the complete JSONPath expression
    return '$' + jsonPathSegments.join('');
}

/**
 * If variants are merged into the schema we must remove the old value that might exist in the configurations.
 */
export const removePatchesFromConfiguration = (variants: GenericConfiguration<VariantConfiguration>[], yamlConfigurations: any[]) => {
    const ajvInstance = new Ajv();
    const validate = ajvInstance.compile(jsonPatchSchema);

    let patchedConfigurations = JSON.parse(JSON.stringify(yamlConfigurations));
    try {
        for (let variant of variants) {
            for (let { patch, target, type } of variant.spec?.patches || []) {
                // the type determines what part of the configuration will be patched (e.g. a default value or a conf value)
                let prefix = '', pop = 0;
                switch (type) {
                    case 'default':
                        // we need to add the spec path since the schema only reflects the spec
                        // and pop the /default
                        prefix = '/spec';
                        pop = 1;
                        break;
                    case 'conf':
                        break;
                    default:
                        throw new Error('Unknown patch type: ' + type);
                }

                const valid = validate(patch);
                if (!valid) { throw new Error('Invalid patch: ' + JSON.stringify({ patch })); }
                let configuration = patchedConfigurations.find(c => matchTargetYaml(target, c));
                if (!configuration) { continue; }
                for (let op of patch) { if (!isValidOperation(op)) { throw new Error('Invalid operation ' + JSON.stringify(op)); } }

                try {
                    const updatedPatch = patch
                        .map((patch) => {
                            const path = popFromPath(prefix + patch.path.replaceAll('/properties', ''), pop);
                            return {
                                ...patch,
                                path
                            };
                        })
                        // only remove paths that exist
                        .filter((patch) => patch.op !== 'replace' || jp.query(
                            configuration,
                            compileJsonPathFromPatchPath(patch.path)
                        )?.length > 0);
                    const result = applyPatch(configuration, updatedPatch, undefined, false);
                    if (result.newDocument) {
                        patchedConfigurations = patchedConfigurations.map((conf) => {
                            if (conf === configuration) { return result.newDocument; }
                            return conf;
                        });
                    }
                } catch (e) {
                    debugger;
                    console.error('Failed to patch the configurations', { patch, message: e?.['message'], kind: configuration.kind });
                    throw new Error('Failed to patch the configurations: ' + e?.['message']);
                }
            }
        }
    } catch (e: any) {
        console.error(e);
        debugger;
        throw new Error('Failed to patch the schema for ' + variants.map(v => v.metadata?.name || v.metadata?.title).join() + '(' + e.message + ')');
    }

    return patchedConfigurations;
}

/**
 * Uses JSON-Patch (RFC6902) embedded into the Variants component/schema to update a specific configuration specified as a target.
 * @returns the updated schema and the history of changes
 */
export const patchSchema = (schema: any, variants: GenericConfiguration<VariantConfiguration>[]) => {
    const ajvInstance = new Ajv();
    const validate = ajvInstance.compile(jsonPatchSchema);
    let history: any = [];

    let patchedSchema = JSON.parse(JSON.stringify(schema));
    let schemaConfigurations = Object.keys(patchedSchema.properties).map(key => ({ key, configuration: patchedSchema.properties[key] }));
    try {
        for (let variant of variants) {
            for (let { patch, target, type } of variant.spec?.patches || []) {
                if (type !== 'default') { continue; }
                const valid = validate(patch);
                if (!valid) { throw new Error('Invalid patch: ' + JSON.stringify({ patch })); }
                let match = schemaConfigurations.find(c => matchTargetSchema(target, c.configuration));
                if (!match) { continue; }
                for (let op of patch) {
                    if (!isValidOperation(op)) {
                        throw new Error('Invalid operation ' + JSON.stringify(op));
                    }
                }

                try {
                    const result = applyPatch(match.configuration, patch, undefined, false);
                    if (result.newDocument) {
                        patchedSchema.properties[match.key] = result.newDocument;
                        patchedSchema.title = variant.metadata?.name;
                        schemaConfigurations = Object.keys(patchedSchema.properties).map(key => ({ key, configuration: patchedSchema.properties[key] }));
                    }
                    let updates: any[] = patch.map(op => ({
                        op,
                        prev: getValueFromSchema(target, compileJsonPathFromPatchPath(popFromPath(op.path, 1)), schema),
                        next: getValueFromSchema(target, compileJsonPathFromPatchPath(popFromPath(op.path, 1)), patchedSchema)
                    }));
                    history.push({ match: result.newDocument, patch, updates });
                } catch (e) {
                    debugger;
                    console.error('Failed to patch the configurations', { patch, message: e?.['message'], kind: match.configuration?.kind });
                    throw new Error('Failed to patch the schema: ' + e?.['message']);
                }
            }
        }
    } catch (e: any) {
        console.error(e);
        debugger;
        throw new Error('Failed to patch the schema for ' + variants.map(v => v.metadata?.name || v.metadata?.title).join() + '(' + e.message + ')');
    }

    return { schema: patchedSchema, history };
}

export const getValueFromSchema = (target, jsonPath: string, schema: any) => {
    try {
        if (!schema) { return undefined; }
        const configurations = Object.keys(schema.properties).map(key => schema.properties[key]);
        let match = configurations.find(c => matchTargetSchema(target, c));
        if (!match) { return undefined; }
        const result = jp.query(match, jsonPath);
        return result?.[0];
    } catch (e) {
        debugger;
        throw new Error('Could not get value for ' + jsonPath);
    }
}

export const jsonPatchSchema = {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "title": "JSON Patch Schema",
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "type": {
                "type": "string",
                "enum": ["default", "conf"]
            },
            "op": {
                "type": "string",
                "enum": ["add", "remove", "replace", "move", "copy", "test"]
            },
            "path": {
                "type": "string"
            },
            "value": {
                "type": ["number", "string", "boolean", "null", "object", "array"]
            }
        },
        "required": ["op", "path"]
    }
};

export const isValidOperation = (operation: Operation): boolean => {
    switch (operation.op) {
        case 'add':
        case 'replace':
        case 'test':
            return operation.path !== undefined && operation.value !== undefined;
        case 'remove':
        case 'move':
        case 'copy':
            return operation.path !== undefined;
        default:
            // Unsupported operation
            return false;
    }
};