import { basename, extname, join, resolve } from 'path';
import { sync } from 'glob';
import * as TJS from 'typescript-json-schema';
import { ScriptTarget, ModuleKind } from 'typescript';
import { createLogger, format, transports } from 'winston';
import { GenericConfiguration } from './interfaces';
import { ALL_PAGES } from './component';
import { decamelize, slugify, getConfigName, kebabToPascal } from './utils';
import { existsSync, readFileSync } from 'fs';

const { LOG_LEVEL = 'info' } = process.env;

const logger = createLogger({
  format: format.json(),
  defaultMeta: { service: 'schema' },
  transports: [
    new transports.Console()
  ],
});

/**
 * Extracts the JSON schema from interfaces used to configure components. Files must be named *.schema.ts(x)
 * @deprecated use json-schema-2
 * @param kinds 
 * @param componentPath the path to the component implementations
 * @returns the meta data schema for all components with their default values.
 */
export const getComponentSchema = (kinds: string[], configurations: GenericConfiguration<any>[], componentPath = join(process.cwd(), '..', 'dist', 'libs'), filterFiles?: (filename: string) => boolean) => {
  let files = sync(`${componentPath}/@(components|renderers)/**/*.schema.@(ts|tsx)`);
  // we only read the compiled component schemas
  let compiledFiles: string[] = sync(`${componentPath}/@(components)/**/*.schema.@(json)`);

  // only load the schemas that were requested
  files = files.filter((path) => {
    for (let kind of kinds) {
      if (path.includes(kind.toLowerCase())) { return true; }
      const slug = slugify(kind);
      if (slug && path.includes(slug.toLowerCase())) { return true; }
      const slug2 = decamelize(kind);
      if (slug2 && path.includes(slug2.toLowerCase())) { return true; }
    }
    return false;
  });
  compiledFiles = compiledFiles.filter((path) => {
    for (let kind of kinds) {
      if (path.includes(kind.toLowerCase())) { return true; }
      const slug = slugify(kind);
      if (slug && path.includes(slug.toLowerCase())) { return true; }
      const slug2 = decamelize(kind);
      if (slug2 && path.includes(slug2.toLowerCase())) { return true; }
    }
    return false;
  });
  const startTime = Date.now();

  if (files.length === 0) {
    logger.warn('Could not find any components', { componentPath, search: '/@(components|renderers)/**/*.schema.@(ts|tsx)', kinds })
  }

  const templateId = configurations.find(config => config.kind === 'Configuration')?.spec?.slug || 'none';

  try {
    let schema: any = { definitions: {} };
    if (filterFiles) { files = files.filter(filterFiles); }
    if (filterFiles) { compiledFiles = compiledFiles.filter(filterFiles); }
    const dir = __dirname;
    const schemaGlob = `${dir}/@(components|renderers)/**/*.schema.@(ts|tsx)`;
    const baseUrl = join(__dirname, 'src', 'html');
    logger.info('building schema from ', { schemaGlob, __dirname, baseUrl, files: files.length });

    let compareCache: any = {};

    for (let file of files) {
      const schemaName = file.match(/([^/]+\.schema)\.ts$/)?.[1];
      const compiledFile = compiledFiles.find(f => f.endsWith('/' + schemaName + '.json'));

      if (compiledFile && existsSync(compiledFile)) {
        const content = JSON.parse(readFileSync(compiledFile, 'utf-8'));
        if (content?.properties?.spec) {
          let fileName = basename(compiledFile, extname(compiledFile));
          let kind = kebabToPascal(fileName.replace('.schema', ''));

          compareCache = {
            ...compareCache,
            [kind + 'Configuration']: content?.properties?.spec
          };
        }
      }
    }

    // We can either get the schema for one file and one type...
    const definitions: any = {};
    for (let kind of kinds) {
      try {
        const matches = configurations.filter(config => config.kind === kind);
        for (let configuration of matches) {
          // get the default values from the schema definition
          const definition = compareCache[`${kind}Configuration`];
          if (!definition) { continue; }
          const page = configuration?.page || ALL_PAGES;
          // use the page as a suffix since different document placements have different spec values
          const name = getConfigName({ page, ...configuration });
          configuration = {
            ...configuration,
            metadata: {
              name, // only write the name if it was not defined
              ...(configuration.metadata || {})
            }
          };

          // make sure to modify a copy with the defaults
          const defaults = setDefaults(JSON.parse(JSON.stringify(definition)), configuration?.spec, schema);
          if (!defaults) { debugger; }
          definitions[name] = {
            ...defaults,
            title: configuration.metadata?.title || defaults?.title + (page ? ' for ' + page : '') || kind + (page ? ' for ' + page : ''),
            description: configuration.metadata?.description || defaults?.description,
            component: {
              kind: configuration.kind,
              metadata: configuration.metadata,
              locales: configuration.locales,
              page: configuration.page,
              runners: configuration.runners
            },
            $id: '/template/schema/' + templateId + '-' + name?.toLowerCase()
          };
        }
      } catch (e: any) {
        debugger;
        throw new Error('Could not read component schema for ' + kind + ': ' + e.message);
      }
    }

    const extractExample = (schema: any, path = '', parent: { title?: string; description?: string; }) => {
      let examples: any[] = [];
      if (schema.examples) {
        examples.push({
          path,
          title: schema.title,
          description: schema.description,
          parent,
          examples: schema.examples
        });
      }
      if (schema.properties) {
        examples = [
          ...examples,
          ...Object.keys(schema.properties).map(prop => extractExample(schema.properties[prop], path + '.' + prop, {
            title: schema.title || prop,
            description: schema.description
          })).flat()
        ];
      }

      return examples.filter(e => e != undefined);
    }

    const _examples = Object.keys(definitions).map(key => ({
      key,
      title: definitions[key].title,
      description: definitions[key].description,
      examples: extractExample(definitions[key], key, {
        title: definitions[key].title || key,
        description: definitions[key].description
      })
    })).filter(c => c.examples.length > 0);

    const metaDataSchemaForComponents = {
      type: 'object',
      title: 'Template configuration',
      properties: {
        ...definitions
      },
      _examples
    };

    logger.info('Generated schema for ' + files.length + ' components', { time: (Math.round((Date.now() - startTime) / 100) / 10) + 's' });
    return metaDataSchemaForComponents;

  } catch (e: any) {
    logger.error('Could not generate schema', { message: e.message, componentPath });
    throw new Error('Could not generate schema at ' + componentPath + ': ' + e.message);
  }
}

/**
 * Sets default values based on a provided schema.
 * @param schema the JSON schema
 * @param values values for the schema
 * @param fullSchema the full schema with access to #/definitions
 */
const setDefaults = (schema, values: any, fullSchema) => {
  const examples = schema.examples;
  if (schema.$ref) {
    // find the definition and use it for defaults
    schema = fullSchema.definitions?.[schema.$ref.replace('#/definitions/', '')];
    if (schema) {
      // make sure we don't modify the full schema
      schema = JSON.parse(JSON.stringify(schema));
    }
  }

  if (examples) { schema.examples = examples; }

  if (schema?.properties) {
    for (let prop of Object.keys(schema.properties)) {
      const value = schema.properties[prop];
      if (prop === '$ref') {
        schema.properties = fullSchema.definitions?.[schema.properties[prop].replace('#/definitions/', '')];
      } else if (value.type === 'array' && value.items?.$ref) {
        schema.properties[prop].items = fullSchema.definitions?.[schema.properties[prop].items.$ref.replace('#/definitions/', '')];
      } else {
        schema.properties[prop] = setDefaults(schema.properties[prop], values?.[prop], fullSchema);
      }
    }
  } else if (schema?.items && Array.isArray(values)) {
    schema.default = values;
  }

  if (values !== undefined && (schema.type === 'string' || schema.type === 'number')) {
    schema.default = values;
  }

  return schema;
};