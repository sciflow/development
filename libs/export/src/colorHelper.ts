/**
 * Mapping of common CSS color names to their hex values.
 * Includes standard CSS colors and additional ones like "rebeccapurple."
 */
const cssColors: Record<string, string> = {
    black: '#000000',
    white: '#ffffff',
    red: '#ff0000',
    navy: '#000080',
    blue: '#0000ff',
    green: '#008000',
    lime: '#00ff00',
    yellow: '#ffff00',
    fuchsia: '#ff00ff',
    gray: '#808080',
    silver: '#c0c0c0',
    maroon: '#800000',
    olive: '#808000',
    purple: '#800080',
    teal: '#008080',
    aqua: '#00ffff',
    rebeccapurple: '#663399'
};

/**
 * Converts a hex color code to an RGB array.
 * @param hex - The hex color code (e.g., "#ff0000" or "ff0000").
 * @returns An array of three numbers representing RGB values.
 * @throws Will throw an error if the provided hex code is invalid.
 */
const hexToRgb = (hex: string): [number, number, number] => {
    if (hex.startsWith('#')) {
        hex = hex.slice(1);
    }

    // Expand 3-digit hex to 6-digit hex
    if (hex.length === 3) {
        hex = hex.split('').map(char => char + char).join('');
    }

    if (hex.length !== 6) {
        throw new Error('Invalid HEX color.');
    }

    const r = parseInt(hex.slice(0, 2), 16);
    const g = parseInt(hex.slice(2, 4), 16);
    const b = parseInt(hex.slice(4, 6), 16);

    return [r, g, b];
};

/**
 * Calculates the relative luminance of an RGB color.
 * @param rgb - An array of three numbers representing RGB values.
 * @returns The relative luminance as a number between 0 and 1.
 */
const calculateLuminance = ([r, g, b]: [number, number, number]): number => {
    const [rr, gg, bb] = [r, g, b].map(c => {
        const channel = c / 255;
        return channel <= 0.03928 ? channel / 12.92 : Math.pow((channel + 0.055) / 1.055, 2.4);
    });

    return 0.2126 * rr + 0.7152 * gg + 0.0722 * bb;
};

/**
 * Determines the better contrasting color (black or white) for a given input color.
 * Converts named colors to hex if applicable, then calculates the contrast.
 * @param color - The input color, either a named CSS color or a hex code.
 * @returns The hex value of the contrasting color ('#ffffff' for white, '#000000' for black).
 */
export const getContrastingColor = (color: string): string => {
    const hex = cssColors[color.toLowerCase()] || color;

    const rgb = hexToRgb(hex);
    const luminance = calculateLuminance(rgb);

    // Calculate contrast with white and black
    const contrastWithWhite = (1 + 0.05) / (luminance + 0.05);
    const contrastWithBlack = (luminance + 0.05) / 0.05;

    return contrastWithWhite > contrastWithBlack ? '#ffffff' : '#000000';
};