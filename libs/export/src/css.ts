import { existsSync } from 'fs';
import { join } from 'path';
import sass, { Deprecation, FileImporter, Logger, SassString } from 'sass';
import { pathToFileURL } from 'url';
import { createLogger, format, transports } from 'winston';

const { LOG_LEVEL = 'info' } = process.env;

/** Gets a value from an object if provided with a path (e.g. object.key.key2 ) */
const getValue = (path: string, object: any) => {
    if (!path) { return undefined; }
    if (object[path]) { return object[path]; }
    const segments = path.split('.');
    if (segments.length === 0) { return undefined; }
    if (object[segments[0]] === undefined) { return undefined; }
    return getValue(segments.slice(1, segments.length).join('.'), object[segments[0]]);
}

export const logger = createLogger({
    level: LOG_LEVEL,
    format: format.json(),
    defaultMeta: { service: 'styles' },
    transports: [
        new transports.Console({ level: LOG_LEVEL })
    ],
});

export const SassLogger: Logger = {
    debug(message: string): void {
        logger.info('Styles:' + message);
    },
    warn(message: string, options: { span: sass.SourceSpan; deprecation: boolean; deprecationType: Deprecation }): void {
        logger.warn('Style warning:' + message, { context: options?.span?.context, deprecation: options?.deprecation });
    }
}

/** Creates a SASS file importer
 * @params assetBasePaths: The base directory that will be used to resolve use and import statements.
 */
export const createFileImporter = (assetBasePaths: string[]) => {
    return new class FindFiles implements FileImporter<'sync'> {
        findFileUrl(url: string, options: { fromImport: boolean; containingUrl: URL; }): URL | null {

            // allow partial matches _partial.scss
            const prefixFile = (p: string): string => {
                const paths = p.split('/');
                paths[paths.length - 1] = '_' + paths[paths.length - 1];
                return paths.join('/');
            }

            let match;
            // allow for relative imports
            const localPath = join(options?.containingUrl?.pathname, '..');
            for (let asssetPath of [localPath, ...assetBasePaths, __dirname]) {
                for (let path of ['', '..', '../..', '../../..', '../../../..'].map(rel => join(asssetPath, rel))) {
                    if (url.startsWith('~') || url.startsWith('@sciflow')) { path = join(path, 'node_modules'); }
                    if (!existsSync(path)) { continue; }
                    if (existsSync(join(path, url))) { match = join(path, url); }
                    if (existsSync(join(path, url + '.scss'))) { match = join(path, url + '.scss'); }
                    if (existsSync(join(path, url + '.css'))) { match = join(path, url + '.css'); }
                    if (existsSync(join(path, prefixFile(url)))) { match = join(path, prefixFile(url)); }
                    if (existsSync(join(path, prefixFile(url) + '.scss'))) { match = join(path, prefixFile(url) + '.scss'); }
                    if (existsSync(join(path, url + '.css'))) { match = join(path, url + '.css'); }
                }
                if (match) { break; }
            }

            if (match) { return pathToFileURL(match); }
            debugger;
            console.error('Could not resolve scss file ' + url, { assetBasePaths, localPath });
            return null;
        }
    }
}

export const renderFragment = (styles: string) => {
    return sass.compileString(styles, { style: 'expanded', logger: SassLogger });
};

export const renderCss = async (file: string, { document, configurations, assetBasePaths = [] }): Promise<{
    styles: sass.CompileResult,
    importers: any,
    functions: any
}> => {
    // FIXME #47 make meta data available
    
    const importers = [createFileImporter(assetBasePaths)];
    const functions: any = {
        'getConf($key, $defaultValue: null, $kind: Configuration)': ([key, defaultValue, kind]) => {
            const configuration = configurations?.find(c => c.kind === kind.text);
            if (!configuration) { return defaultValue; }
            const lookup = key.toString()?.replace(/"/gi, '',);
            const value = getValue(lookup, configuration);
            // TODO: we should make a difference between undefined and purposefully set to null
            if (!value) { return defaultValue; }
            // TODO we should respect the value type
            const retval = new SassString(value);
            return retval;
        },
        'getDocumentValue($key)': ([key]) => {
            const keyValue = (key as SassString).toString();
            if (!document[keyValue]) {
                debugger;
                throw new Error('No value for ' + keyValue);
            }
            const retval = new SassString(document[keyValue]);
            return retval;
        }
    };

    try {
        const styles = sass.compile(file, {
            importers,
            functions,
            logger: SassLogger
        });

        return {
            styles,
            importers,
            functions
        }
    } catch (e: any) {
        console.error('Could not compile sass', { message: e.message, file });
        throw new Error('Could not compile sass');
    }
};
