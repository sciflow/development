import { DocumentNode } from "@sciflow/schema";
import { RenderingEngine, Toc } from "./renderers";
import { GenericConfiguration, SciFlowDocumentData } from "./interfaces";
import { TemplateComponent } from "./component";
import { PartFileMap } from "./html";

export interface Renderer<OutputFormat> {
    engine?: RenderingEngine<OutputFormat, any>;
    render(node: DocumentNode<any>, parent?: DocumentNode<any>): Promise<OutputFormat>;
}

/**
 * Data that is shared between all renderers (HTML, XML, ...)
 */
export interface RendererData {
    /**
     * A listing of all items that should make up a table of contents or similar listings (e.g. list of figures).
     */
    tableOfContents?: Toc;
    /** A list of component configurations */
    configurations?: GenericConfiguration<any>[];
    /** A list of template components (classes/functions with the component interface) */
    templateComponents?: TemplateComponent<any, SciFlowDocumentData>[];
    /** The locations to find assets related to component paths */
    componentPaths?: string[];
    /** A map of all parts to the filenames they are exported into */
    partFileMap?: PartFileMap;
}

/**
 * An abstract renderer that takes a document node and returns the results as OutputFormat.
 */
export abstract class NodeRenderer<OutputFormat, DataFormat extends RendererData> implements Renderer<OutputFormat> {
    _engine: RenderingEngine<OutputFormat, DataFormat>;

    get engine(): RenderingEngine<OutputFormat, DataFormat> {
        return this._engine;
    }

    constructor(engine: RenderingEngine<OutputFormat, DataFormat>) {
        this._engine = engine;
    }

    public abstract render(node: DocumentNode<any>, parent?: DocumentNode<any>): Promise<OutputFormat>;
}