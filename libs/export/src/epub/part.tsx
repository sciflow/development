import { TSX } from '../XHTMLTSX';
import { createXHTMLInstance } from '../html/helpers';

import { generateListings } from "../html";

export const addParts = async (sections: any[], { document, renderedFootnotes, contentDir, logger }): Promise<void> => {

    /** Takes a HTML or XML element into the EPUB XHTML namespaces. */
    const toXHTML = (dom) => {
        if (typeof dom === 'string') { return ''; }
        // const imgTagRegex = /<img\s+([^>]+)>/g;

        // transform xml first
        if (typeof dom?.end === 'function') {
            dom = TSX.htmlStringToElement(dom.end({ headless: true }));
        }

        //if (!body) { throw new Error('Cannot tranform empty element'); }
        try {
            // clean up self closing elements that will not be accepted in xhtml
            /* body = body
                .replaceAll('&nbsp;', ' ')
                .replaceAll('<br>', '<br />')
                .replaceAll('<svg ', '<svg xmlns="http://www.w3.org/2000/svg" ')
                .replaceAll(imgTagRegex, '<img $1 />'); */
            const el: HTMLElement = <body></body>;
            el.append(dom);
            return el.firstChild;
        } catch (e: any) {
            debugger;
            logger.error('Could not transform section', { message: e.message });
        }
    }

    for (let section of sections) {
        let docEndnotes;
        if (!section.partId) { throw new Error('Unnamed parts are not allowed'); }

        if (section.document) {
            const sectionListings = await generateListings(section.document);
            if (sectionListings.footnotes.length > 0) {
                docEndnotes = <section epub:type="endnotes" role="doc-endnotes">
                    {sectionListings.footnotes.map(footnote => {
                        if (!document.tables) { throw new Error('Tables must be generated at this point'); }
                        const index = document.tables.footnotes.findIndex(fn => fn.id === footnote.id);
                        const renderedFootnote = renderedFootnotes.find(fn => fn.id === footnote.id)?.dom || '';
                        return <aside id={'fn' + (index + 1)} epub:type="footnote" class="small">
                            <a href={'#fna' + (index + 1)} role="doc-backlink" title={`Go to note reference [${index + 1}]`}><sup>  [{index + 1}]</sup>
                                {toXHTML(renderedFootnote)}
                            </a>
                        </aside>;
                    })}
                </section>;
            }
        }

        const doc = createXHTMLInstance(`<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="${section.document.attrs.locale || 'en'}">
            <head>
                ${section.title ? `<title>${section.title}</title>` : ''}
                <link href="styles/styles.css" rel="stylesheet" type="text/css" />
            </head>
            <body>
            </body>
        </html>`);
        const documentBody = doc.window.document.body;

        // const documentBody = doc.window.document.body;
        for (let domElement of section.dom) {
            const instance = toXHTML(domElement);
            documentBody.append(instance as Node);
        }

        if (docEndnotes) { documentBody.append(docEndnotes); }
        contentDir?.file(`part_${section.partId}.xhtml`, doc.serialize());
    }
};
