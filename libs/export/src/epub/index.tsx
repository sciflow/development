import { TSX as HTMLFactory } from '../TSX';
import { XMLJSXFactory as TSX } from '../XMLJSXFactory';

import mime from 'mime';
import sharp from 'sharp';
import { createLogger, format, transports } from 'winston';
import { DocumentData, ExportOptions, SciFlowDocumentData } from "../interfaces";
import { NodeRenderer } from "../node-renderer";
import { v4 as uuidv4 } from 'uuid';

import { getDefaults, ReferenceRenderer, SourceField } from '@sciflow/cite';

import JSZip from 'jszip';

import { DocumentNode, DocumentSnapshot, DocumentSnapshotImage, DocumentSnapshotResource, SFNodeType } from '@sciflow/schema';
import { existsSync } from 'fs';
import { getValueOrDefault, PartForRendering, TemplateComponent } from '../component';
import { renderCss } from '../css';
import { escapeId, extractHeadingOfType, wrapTOC } from '../helpers';
import { slugify } from '../utils';
import { createRenderingEngine, documentDataFromSnapshot, fillContentHoles, instanceToDom, generateListings, mapDoc, transformImage, transformFiles } from '../html';
import defaultTemplateComponents, { EpubPage, Typography, Configuration } from '../html/components';
import { Environment } from '../html/components/environment/environment.component';
import { FigureRendererConfiguration } from '../html/renderers/figure/figure.schema';
import { createFactories, createInstances } from '../html/renderers/helpers';
import { RenderingEngine } from '../renderers';
import { basename } from 'path';
import { addParts } from './part';

const { styleXML, localeXML } = getDefaults();
const { LOG_LEVEL = 'info' } = process.env;

const logger = createLogger({
    level: LOG_LEVEL,
    format: format.json(),
    defaultMeta: { service: 'epub-main' },
    transports: [
        new transports.Console({ level: LOG_LEVEL })
    ],
});

/**
 * Finalizes the parts in order.
 * This is important so content holes get filled appropriately.
 */
export const finalizeInOrder = async (parts: PartForRendering[], engine: RenderingEngine<any, any>): Promise<any[]> => {
    let results: any[] = [];
    for (let part of parts) {
        if (part.document?.attrs && !part.document?.attrs?.partId) { part.document.attrs.partId = part.document.attrs?.id ?? part.id; }
        if (part.document?.attrs && !part.document?.attrs?.id) { part.document.attrs.id = part.document.attrs?.partId; }
        if (!part.document?.attrs?.partId) {
            (engine.logging?.logger || logger)
                .warn('Encountered template part without a partId in epub export', {
                    component: part.id,
                    documentId: engine.data?.document?.documentId,
                    transactionId: engine.logging?.transactionId
                });
            continue;
        }
        try {
            // execute all dom functions
            let dom = await instanceToDom(part, engine);
            if (dom && !Array.isArray(dom)) { dom = [dom]; }

            results.push({
                partId: part.document?.attrs?.id,
                title: part.document ? extractHeadingOfType(part.document, engine) : undefined,
                document: part.document,
                dom: dom || []
            });
        } catch (e) {
            logger.error('Could not finalized part ' + part.id);
        }
    }

    return results;
};

/** Renders a manuscript to a EPUB 3 ZIP blob
 * (!) Expects document to be split into parts/chapters already
 */
export const renderEPUB = async (exportData, options?: ExportOptions) => {

    const citationRenderer = new ReferenceRenderer(styleXML, localeXML, exportData.references ?? []);
    const configuration = options?.configurations?.find(c => c.kind === 'Configuration');
    const configurations = options?.configurations || [];
    const title = (exportData.title?.document ? extractHeadingOfType(exportData.title.document) : undefined) || 'Untitled document';

    const runner = 'epub';
    const locale = options?.locale ?? exportData.locale ?? configuration?.spec?.lang ?? 'en-US';

    const zip = new JSZip();
    zip.file('mimetype', 'application/epub+zip');
    const contentDir = zip.folder('content');
    const assetDir = contentDir?.folder('assets');
    const fontDir = zip.folder('fonts');

    let files: DocumentSnapshotResource[] = await transformFiles(exportData, options, { assetDir, fontDir })

    const customTemplateComponents = options?.customTemplateComponents || [];
    // we make this generic for typescript to not analyze all typings here which leads to high compilation times
    const templateComponents = [...defaultTemplateComponents, ...customTemplateComponents] as any[];

    const tables = await generateListings(exportData.parts.map(p => mapDoc(p)).filter(p => p.attrs.role !== 'coverpage'), undefined, undefined, configurations);

    const document: DocumentData = {
        documentId: exportData.documentId,
        title: exportData.title?.document ? extractHeadingOfType(exportData.title?.document) : 'Untitled document',
        subtitle: undefined,
        titlePart: exportData.parts?.filter(p => p.type === 'title'),
        parts: exportData.parts?.filter(p => p.type !== 'title'),
        index: exportData.parts?.filter(p => p.type !== 'title').map(p => p.id),
        metaData: {},
        images: (files || []).filter(file => file.type === 'image') as DocumentSnapshotImage[],
        files,
        tables,
        authors: exportData.authors || [],
        references: exportData.references || [],
        locale
    }

    const templateConfiguration = configurations.find(c => c.kind === Configuration.name);
    const epubConfiguration = configurations.find(c => c.kind === EpubPage.name);
    if (!epubConfiguration) { throw new Error('No EPUB export configured for this template. Please add a EPUB configuration to ' + templateConfiguration?.spec.title); }

    class CitationRenderer extends NodeRenderer<HTMLElement | HTMLSpanElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.citation>) {
            const citation = SourceField.fromString(node.attrs.source);
            if (!citation) {
                return <span style="color: red;">Unreadable reference</span>
            }
            try {
                const rendered = this.engine.data.citationRenderer.renderCitation(citation);
                const citationEl = HTMLFactory.htmlStringToElement(`<a>${rendered.renderedCitation}</a>`) as HTMLLinkElement;
                if (!this.engine.data.document.tables) { throw new Error('Tables must be generated at this point'); }
                const part = this.engine.data.document.tables.parts.find(p => p.type === 'bibliography');
                const id = citation.citationItems?.[0]?.id;
                if (part?.id && id) {
                    citationEl.setAttribute('href', `part_${part.id}.xhtml#ref-${escapeId(id)}`);
                } else {
                    return HTMLFactory.htmlStringToElement(`<span>${rendered.renderedCitation}</span>`) as HTMLSpanElement;
                }

                citationEl.setAttribute('backlink', 'doc-backlink');
                citationEl.setAttribute('epub:type', 'backlink');

                return citationEl;
            } catch (e: any) {
                if (e instanceof Error) {
                    this.engine.log('error', 'Could not render reference ' + citation?.citationItems?.map(i => i.id).join(), { message: e.message, citation });
                }
                return HTMLFactory.htmlStringToElement(` <citation style="color: red;">Missing reference ${citation?.citationItems?.map(i => i.id).join()}</citation>`);
            }
        }
    }

    /** Since cross references may go across files, we need to add the filename to the link */
    class LinkRenderer extends NodeRenderer<HTMLLinkElement, SciFlowDocumentData> {
        async render(node: DocumentNode<any>) {
            if (!this.engine.data.document.tables) { throw new Error('Tables must be generated at this point'); }
            const referencedElement = node.attrs.href.replace('#', '');
            const idMap = this.engine.data.document.tables.idMap;
            const link = idMap[referencedElement];
            if (!link) {
                return <small style="color: red;">Cross reference to deleted element {referencedElement}</small>;
            }
            const index = this.engine.data.document.tables.labels[link.type]?.findIndex(d => d.id === referencedElement);
            let text = index > -1 ? (index + 1).toLocaleString(locale === 'ar' ? 'ar-SA' : locale) : link.title;

            const environmentComponent = new Environment(this.engine as any);
            const configuration: FigureRendererConfiguration = environmentComponent.getValues();
            const environment = configuration.environments?.[link.type];
            const toc = this.engine.data.document.tables.toc;

            let tocEntry;
            if (link.type === SFNodeType.heading) {
                tocEntry = toc.headings?.find(h => h.id === referencedElement);
                if (tocEntry?.numberingString) {
                    text = tocEntry?.numberingString;
                }
            }

            const itemFromListing = this.engine.data.document?.tables?.labels?.[link.type]?.find(i => i.id === referencedElement) || tocEntry;
            const referenceLabel = itemFromListing?.customLabel
                || environmentComponent.getAs<string>('environments.' + link.type + '.referenceLabel')
                || environment?.referenceLabel ? environment?.referenceLabel + ' ' : '';
            const linkEl = HTMLFactory.createElement('a', {
                'backlink': 'doc-backlink',
                'epub:type': 'backlink'
            });

            linkEl.innerHTML = referenceLabel + text;
            for (let att in node.attrs) {
                linkEl.setAttribute(att, node.attrs[att]);
            }

            return linkEl;
        }
    }

    const renderedFootnotes: { id: string; dom: any; }[] = [];
    // we render the footnote content here so we can maintain order of references inside footnotes
    class EpubFootnoteRenderer extends NodeRenderer<HTMLLinkElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.footnote>) {
            if (!document.tables) { throw new Error('Tables must be generated at this point'); }
            // the rest of the file renders XML, so switch the renderer
            const index = document.tables.footnotes.findIndex(fn => fn.id === node.attrs.id);
            // since the renderer will give us HTML we convert it into XML
            const footnoteContent = HTMLFactory.createElement('span', { 'data-type': 'endnote-body' }, await this.engine.renderContent(node));
            renderedFootnotes.push({
                id: node.attrs.id,
                dom: TSX.stringToElement(footnoteContent.outerHTML)
            });

            const link = HTMLFactory.createElement('a', {
                id: `fna${index + 1}`,
                href: `#fn${index + 1}`,
                role: 'doc-noteref',
                ['epub:type']: 'noteref'
            }, HTMLFactory.htmlStringToElement(`<sup class="small" data-type="endnote-call">[${index + 1}]</sup>`))
            return link as HTMLLinkElement;
        }
    }

    const customRenderers = {
        [SFNodeType.footnote]: EpubFootnoteRenderer,
        [SFNodeType.link]: LinkRenderer,
        [SFNodeType.citation]: CitationRenderer
    };

    const environmentConfiguration = configurations.find(c => c.kind === Environment.name);
    const typographyConfiguration = configurations.find(c => c.kind === Typography.name);
    if (typographyConfiguration?.spec) {
        typographyConfiguration.spec.alignHeadings = false;
    }

    if (environmentConfiguration) {
        // overwrite image export quality
        if (!environmentConfiguration.spec) { environmentConfiguration.spec = {}; }
        if (epubConfiguration.spec?.transformImages) {
            environmentConfiguration.spec.transformImages = epubConfiguration.spec?.transformImages;
        } else {
            environmentConfiguration.spec.transformImages = {
                format: 'JPEG',
                quality: 80,
                dpi: 96
            }
        }
    }

    // the node renderer renders the document tree itself
    const engine: RenderingEngine<HTMLElement, SciFlowDocumentData> = await createRenderingEngine({
        document,
        documentSnapshot: exportData,
        configurations,
        citationRenderer,
        customRenderers,
        templateOptions: configuration?.spec || configuration,
        metaData: options?.metaData,
        metaDataSchema: options?.metaDataSchema,
        logging: options?.logging,
        runner,
        templateComponents,
        componentPaths: options?.componentPaths || []
    });

    document.files = await Promise.all(files.map(file => transformImage(file, { imageDirectory: assetDir, engine })));

    let partsWithMetaData: PartForRendering[] = [];
    for (let p of exportData.parts
        .filter(p => p.type !== 'title')) {
        const doc = mapDoc(p, engine.data?.templateOptions?.placement);
        let activeEngine: RenderingEngine<HTMLElement, SciFlowDocumentData> = engine;
        // we replace the active engine with one using the parent meta data if provided
        // this allows for document collections to access the meta data inside a sub/document
        if (p.parent) {
            if (p.standalone) {
                // do not render the DOM if we're going individually render sub-documents anyways
                partsWithMetaData.push({
                    ...p,
                    id: p.document.attrs?.id || p.partId || p.id,
                    dom: undefined,
                    placement: p.options?.placement || p.placement,
                    document: doc,
                    parent: {
                        ...p.parent,
                        parts: p.parent.parts.map(p2 => ({ ...p2, standalone: true }))
                    },
                    notDefining: false
                });
                continue;
            }

            const partDoc = await documentDataFromSnapshot(p.parent, { files, locale, configurations });
            activeEngine = await createRenderingEngine({
                document: partDoc,
                documentSnapshot: p.parent,
                configurations,
                citationRenderer,
                customRenderers,
                templateOptions: configuration?.spec || configuration,
                metaData: partDoc.metaData,
                metaDataSchema: options?.metaDataSchema,
                logging: options?.logging,
                runner,
                componentPaths: options?.componentPaths || [],
                templateComponents,
                baseLevel: p.level ?? 1,
            }, options?.debug, 'sub-for-part-' + partDoc.documentId);
        }

        const renderDOM = async (e: RenderingEngine<HTMLElement, SciFlowDocumentData>) => {
            try {
                return await e.render(doc);
            } catch (e: any) {
                debugger;
                return <section id={p?.document?.attrs?.id}>
                    <h1>Could not render section(deferred) </h1>
                    <p class="m0 p0"> {e.message} </p>
                    <h2 style="color: red;" > Error: {e.payload.message} </h2>
                    <pre> {JSON.stringify(e.payload, null, 2)} </pre>
                </section>;
            }
        };

        // render bibliography later (so all references are registered, even the ones after the bibliography)
        if (p.type === 'bibliography') {
            partsWithMetaData.push({
                id: p.document.attrs?.id || p.partId || p.id,
                dom: (_engine) => renderDOM(activeEngine),
                placement: p.options?.placement || p.placement,
                document: doc,
                parent: p.parent,
                standalone: p.standalone
            });
            continue;
        }

        partsWithMetaData.push({
            ...p,
            id: p.document.attrs?.id || p.partId || p.id,
            dom: await renderDOM(activeEngine),
            placement: p.options?.placement || p.placement,
            document: doc,
            parent: p.parent,
            notDefining: false
        });
    }

    // components can be used to inject DOM into the document (e.g. for the table of contents)
    // or just render CSS (e.g. to style the @page)
    const componentFactories = createFactories(templateComponents, engine);

    // go through the template configuration values and create the styles and additional DOM elements
    let { styles, instances } = await createInstances(
        // do not add part configurations since they will be rendered in sub-documents
        configurations.filter(c => c.page !== 'part'),
        componentFactories,
        { runner, componentPaths: options?.componentPaths, logger, assetBasePaths: options?.assetBasePaths }
    );

    if (options?.stylePaths && options?.stylePaths?.length > 0) {
        const templateStyles = (await Promise.all((options?.stylePaths || [])
            ?.filter(stylePath => {
                if (Array.isArray(stylePath.runners) && options?.runner) {
                    return stylePath.runners.includes(options?.runner);
                }
                return true;
            })
            .map(async (stylePath) => {
                try {
                    if (!existsSync(stylePath.path)) {
                        throw new Error('Style file did not exist: ' + stylePath.path);
                    }
                    const { styles } = await renderCss(stylePath.path, {
                        document: { title: title && title?.length > 60 ? title?.substring(0, 60) + '...' : title, ...(options?.metaData ?? {}) },
                        configurations: options?.configurations
                    });
                    return {
                        path: stylePath.path,
                        css: styles.css,
                        position: stylePath.position || 'start'
                    };
                } catch (e: any) {
                    console.error('Failed to render styles', { message: e.message, stylePath: stylePath.path });
                    return null;
                }
            }))).filter(v => v && v?.css?.length > 0);

        styles.push(...templateStyles);
    }

    instances = instances.map(part => ({
        ...part,
        // we create a default body placement if none was provided so matching does not always have to check for empty placements
        placement: part.placement || 'body'
    }));

    partsWithMetaData = partsWithMetaData.map(part => ({
        ...part,
        // we create a default body placement if none was provided so matching does not always have to check for empty placements
        placement: part.placement || 'body'
    }))

    const templateRendersTitle = configurations.some(c => c.kind === 'JournalTitle' || c.kind === 'MonographTitle');

    /**
     * Order parts
     */
    let orderedPartsAndInstances: PartForRendering[] = [
        ...fillContentHoles({ parts: partsWithMetaData, instances }, 'cover'),
        ...fillContentHoles({ parts: partsWithMetaData, instances }, 'front'),
        ...fillContentHoles({ parts: partsWithMetaData, instances }, 'body'),
        ...fillContentHoles({ parts: partsWithMetaData, instances }, 'back'),
    ].filter(part => !(part.document?.attrs.id === 'title' && templateRendersTitle));

    // we re-generate the tables to have access to titleHTMl elements and other rendered captions/titles
    // this is the first time we know the final order of all content        

    await engine.generateListings(orderedPartsAndInstances
        .filter(part => typeof part.document === 'object')
        .map(part => mapDoc(part, engine.placement))
        .filter(p => p.attrs.role !== 'coverpage')
    );

    if (!engine.data.document.tables) { throw new Error('Tables must be generated at this point'); }
    for (let equation of engine.data.document?.tables?.labels?.equation || []) {
        const equationFile = engine.data.document.files.find(file => file.id === equation.id);
        if (equationFile?.inline === true && equationFile?.content?.length > 0) {
            const equationFileName = basename(equationFile.url);
            assetDir?.file(equationFileName, equationFile.content);
        }
    }
    const images = engine.data.document.tables.images;
    const usedFiles: any[] = [];

    const assetFiles = Object.keys(assetDir?.files || {});
    for (let image of images) {
        if (!assetFiles.find(name => name.includes(image.id))) {
            logger.warn('Image file was not found in assets', { id: image.id });
        }
    }

    // clean up the unused images before continuing
    assetDir?.forEach((path, file) => {
        if (file.name.startsWith('content/assets/') && !file.dir) {
            let image: any = images.find(i => path.startsWith(i.id));
            if (!image) {
                image = files.find(usedFile => file.name.endsWith(usedFile.url));
            }
            const originalFile = files.find(i => path.startsWith(i.id));
            if (!image && !originalFile?.static) {
                engine.log('verbose', 'Removing unused image ' + path);
                assetDir.remove(path);
            } else {
                usedFiles.push(originalFile);
            }
        }
    });

    // go through all parts and move components into the page flow (to avoid additional page breaks)
    // an example where this is needed is the headers and footers and css paged media which can not be the
    // first child of the body but should rather be moved into the first section inside (to maintain page breaks)
    // see https://www.princexml.com/forum/topic/4877/create-complex-headers-that-work-with-page-groups-not-sure
    for (let part of orderedPartsAndInstances) {
        if (part.moveIntoPageFlow) {
            // find the next part the element can be injected into (e.g. into it's so called content hole)
            let partForPlacement;
            if (part.placement) {
                // move it only into the part of the same placement
                // make sure the part has dom content, otherwise it might get skipped
                partForPlacement = orderedPartsAndInstances.find(p => p.placement === part.placement && !p.moveIntoPageFlow);
            } else {
                // move it into the first possible part
                partForPlacement = orderedPartsAndInstances.find(p => p !== part && !p.moveIntoPageFlow);
            }

            if (!partForPlacement) { continue; }
            // add the part to the content hole of the part that will be rendered
            partForPlacement.contentHole = [...(partForPlacement.contentHole || []), part];
            // remove from list of parts to be rendered
            orderedPartsAndInstances = orderedPartsAndInstances.filter(p => p !== part);
        }
    }

    let cover: any[] = [];
    if (orderedPartsAndInstances.filter(s => s.placement === 'cover').some(p => !p.notDefining)) {
        // only add the back if there are parts in it other than headers and footers
        cover = await finalizeInOrder(orderedPartsAndInstances.filter(s => s.placement === 'cover'), engine);
    }
    let front: any[] = [];
    if (orderedPartsAndInstances.filter(s => s.placement === 'front').some(p => !p.notDefining)) {
        // only add the back if there are parts in it other than headers and footers
        front = await finalizeInOrder(orderedPartsAndInstances.filter(s => s.placement === 'front'), engine);
    }
    const body = await finalizeInOrder(orderedPartsAndInstances.filter(s => s.placement === 'body' || !s.placement), engine);
    let back: any[] = [];
    if (orderedPartsAndInstances.filter(s => s.placement === 'back').some(p => !p.notDefining)) {
        // only add the back if there are parts in it other than headers and footers
        back = await finalizeInOrder(orderedPartsAndInstances.filter(s => s.placement === 'back'), engine);
    }

    // no fonts for epub
    styles = styles.filter(path => !path.path?.endsWith('fonts.scss'));

    const firstStyles = styles.filter(style => style.position === 'start');
    const middleStyles = styles.filter(style => !style.position);
    const endStyles = styles.filter(style => style.position === 'end');

    let inlineStyles: any[] = [];
    let cssStyles: string = '';
    if (options?.inline) {
        inlineStyles.push(...firstStyles, ...middleStyles, ...endStyles);
    } else {
        cssStyles = [
            ...firstStyles.filter(style => !style.inline),
            ...middleStyles.filter(style => !style.inline),
            ...endStyles.filter(style => !style.inline)
        ].map(s => s?.css)
            .join('\n\n');
        await contentDir?.file('styles/styles.css', cssStyles);
    }

    const sections = [...cover, ...front, ...body, ...back].map(section => {
        // since epub manuscripts are split across multiple documents we need to update references
        if (!section.dom) { return section; }
        for (let el of section.dom as HTMLElement[]) {
            const anchors = el.getElementsByTagName('a');
            if (anchors.length > 0) {
                for (let i = 0; i < anchors.length; i++) {
                    const link = anchors.item(i);
                    if (link?.getAttribute('href')?.startsWith('#')) {
                        const id = link?.getAttribute('href')?.replace('#', '');
                        const entry = id && document?.tables?.idMap?.[id];
                        if (
                            entry &&
                            section.partId &&
                            entry.partId &&
                            !id?.startsWith(entry.partId) && entry.partId !== section.partId
                        ) {
                            link.setAttribute('href', `part_${entry.partId}.xhtml${entry.id ? '#' + entry.id : ''}`);
                        }
                    }
                }
            }
        }

        return section;
    });

    // go through all links and rewrite them to the correct file
    //if (itemFromListing && itemFromListing.partId) {
    //    linkEl.setAttribute('href', `part_${itemFromListing.partId}.xhtml#${itemFromListing.id ? '#' + itemFromListing.id : ''}`);
    //}

    const metaInf = zip.folder('META-INF');
    // TODO move these into their own 'renderers'
    metaInf?.file('container.xml', (<container version="1.0" xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
        <rootfiles>
            <rootfile
                full-path="content/content.opf"
                media-type="application/oebps-package+xml" />
        </rootfiles>
    </container>).end());

    if (!document.tables) { throw new Error('Tables must be generated at this point'); }

    const tocItems: any[] = [];
    for (let part of sections) {
        const items = document.tables.toc?.headings.filter(h => h.partId === part.partId);
        if (items.length === 0) {
            const partInstance: any = orderedPartsAndInstances.find(p => p.id === part.partId);
            let title = partInstance?.title || (part.document ? extractHeadingOfType(part.document, engine) : undefined);
            if (!title && partInstance?.role?.length > 0) {
                title = partInstance?.role?.[0].toUpperCase() + partInstance?.role.substring(1, partInstance?.role.length);
            } else if (!title && partInstance?.placement === 'cover') {
                title = 'Cover';
            } else if (!title) {
                title = 'Part';
            }

            tocItems.push({
                title,
                partId: part.partId,
                counterStyle: 'none',
                level: 1
            });
        } else {
            tocItems.push(...items);
        }
    }

    const wrappedTOC = wrapTOC(tocItems);
    const depth = 3;
    const renderHeadings = (headings, level = 0) => level <= depth ? <ol>
        {headings.map(heading => <li>
            {heading.title ? <a href={`${heading.partId ? `part_${heading.partId}.xhtml` : ''}${heading.id ? '#' + heading.id : ''}`}>
                {heading?.numberingString ? heading.numberingString : ''}&nbsp;
                {heading.title ? heading.title : ''}
            </a> : ''}
            {heading.content?.length > 0 ? renderHeadings(heading.content, level + 1) : ''}
        </li>)}
    </ol> : '';

    const publisherName = getValueOrDefault('book-meta.publisherName', exportData.metaData, options?.metaDataSchema?.properties?.MetaData);
    const bookIdtype = getValueOrDefault('book-meta.book-id.pub-id-type', exportData.metaData, options?.metaDataSchema?.properties?.MetaData);
    const bookId = getValueOrDefault('book-meta.book-id.id', exportData.metaData, options?.metaDataSchema?.properties?.MetaData);

    const authors = document.authors.filter(a => a.roles?.includes('author')).map(author => {
        let name;
        if (author.name) { name = author.name }
        if (author.lastName) {
            name = [author.firstName, author.lastName].filter(n => n != undefined).join(' ');
        }

        if (!name) { return undefined; }

        return { id: 'author-' + slugify(name), name };
    }).filter(n => n != undefined);

    const id = uuidv4(document.documentId);

    const dateString = new Date().toISOString().split('.')[0] + 'Z'; // remove milliseconds
    logger.info('Writing EPUB', { id, dateString });

    const nav = renderHeadings(wrappedTOC.filter(entry => entry.partId !== 'title'));
    const content = (<package xmlns="http://www.idpf.org/2007/opf" xmlns:epub="http://www.idpf.org/2007/ops"
        version="3.0" xml:lang={locale} unique-identifier="uuid">
        <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:opf="http://www.idpf.org/2007/opf">
            <dc:coverage>Content Documents</dc:coverage>
            <dc:date>{dateString}</dc:date>
            <dc:identifier id="uuid" opf:scheme="uuid">{id}</dc:identifier>
            {/* <dc:description>A Presentation MathML equation is displayed as part of the XHTML content.</dc:description>
            <dc:identifier id="pub-id">cnt-mathml-support</dc:identifier>
             */}
            {bookId ? <dc:identifier id="bookid">{bookIdtype}{bookId}</dc:identifier> : ''}
            {...authors.map((author) => <dc:creator id={author?.id}>{author?.name}</dc:creator>)}
            <dc:language>{locale}</dc:language>
            {publisherName ? <dc:publisher>{publisherName}</dc:publisher> : ''}
            <dc:title>{document.title}</dc:title>
            <link rel="dcterms:rights"
                href="https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document" />
            <link rel="dcterms:rightsHolder" href="https://www.w3.org" />
            <meta property="dcterms:isReferencedBy">
                https://www.w3.org/TR/epub-rs-33/#confreq-mathml-rs-behavior</meta>
            <meta property="dcterms:isReferencedBy">
                https://www.w3.org/TR/epub-rs-33/#confreq-mathml-rs-render</meta>
            <meta property="dcterms:modified">{dateString}</meta>
        </metadata>
        <manifest>
            <item id="nav" properties="nav" href="nav.xhtml" media-type="application/xhtml+xml" />
            <item id="css" href="styles/styles.css" media-type="text/css" />
            {sections.map(section => (<item id={'part_' + section.partId} properties="mathml" href={'part_' + section.partId + '.xhtml'}
                media-type="application/xhtml+xml" />))}
            {usedFiles.map((file) => {
                if (file.type === 'image') {
                    return <item id={file.fileId || file.id} href={file.compressedUrl || file.url} media-type={file.mimeType} />
                }
            }).filter(f => f != null)}
        </manifest>
        <spine>
            {sections.map(section => <itemref idref={'part_' + section.partId} />)}
        </spine>
    </package>)
        .end({ prettyPrint: true });

    contentDir?.file('content.opf', content);

    const navContent = (<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="en">
        <head>
            <title>{title}</title>
        </head>
        <body>
            <nav epub:type="toc">
                <h1>{document.title}</h1>
                {nav}
            </nav>
        </body>
    </html>).end({ prettyPrint: true });
    contentDir?.file('nav.xhtml', navContent);

    try {
        await addParts(sections, { document, renderedFootnotes, contentDir, logger });
    } catch (e: any) {
        debugger;
        logger.error('Could not transform parts', { message: e.message });
    }

    return await zip.generateAsync({
        type: 'nodebuffer',
        mimeType: 'application/epub+zip'
    });
};
