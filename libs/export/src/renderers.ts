import { counterStyle, DocumentNode, HeadingDocumentNode, PlacementOption, rootDoc, SFNodeType } from '@sciflow/schema';
import { Logger } from 'winston';
import { createPrefix, extractHeadingTitle } from './helpers';

export class RenderError extends Error {
    constructor(message: string, public payload: any) {
        super(message);

        // Set the prototype explicitly.
        Object.setPrototypeOf(this, RenderError.prototype);
    }

    toString() {
        return `${this.name}: ${this.message}`;
    }

    toJSON() {
        return { message: this.message, payload: this.payload };
    }
}

import { v4 as uuidv4 } from 'uuid';
import { generateListings, PartFileMap } from './html';
import { Page } from './html/components';
import { HeadersAndFooters } from './html/components/headers-and-footers/headers-and-footers.component';
import { HeadersAndFootersConfiguration } from './html/components/headers-and-footers/headers-and-footers.schema';
import { PageConfiguration } from './html/components/page/page.schema';
import { GenericConfiguration, SciFlowDocumentData, TocHeading } from './interfaces';
import { Renderer, RendererData } from './node-renderer';

export type RendererPlugin<OutputFormat, DataFormat extends RendererData> = ((node: DocumentNode<any>, element: OutputFormat, engine: RenderingEngine<OutputFormat, DataFormat>) => Promise<OutputFormat>)

export interface Toc {
    headings: TocHeading[];
}

/**
 * Rendering engine that returns as OutputFormat.
 */
export class RenderingEngine<OutputFormat, DataFormat extends RendererData> {
    /** An internal ID for debugging of nested engines */
    id = uuidv4();

    renderers: { [key: string]: Renderer<OutputFormat> } = {};
    data: DataFormat;
    plugins: RendererPlugin<OutputFormat, DataFormat>[];

    logging?: {
        logger?: Logger;
        transactionId?: string;
    }

    get isDebug() {
        return this.debug;
    }

    constructor(
        renderers: { [key: string /** stay open to undefined types from plugins */]: any },
        data?: DataFormat,
        plugins = [],
        private readonly debug = false,
        public readonly runner?: string
    ) {
        this.data = data || {} as any;
        this.plugins = plugins;

        for (let key of Object.keys(renderers)) {
            if (typeof renderers[key] !== 'number') {
                if (!renderers[key]) {
                    throw new Error('Renderer must be defined or set to a default (' + key + ')');
                }
                try {
                    this.renderers[key] = new renderers[key](this);
                } catch (e: any) {
                    throw new Error(key + ':' + e.message);
                }
            } else {
                this.renderers[key] = renderers[key] as any;
            }
        }
    }

    /** Logs an error and retains it. */
    log(type: 'info' | 'warn' | 'verbose' | 'error' = 'warn', message: string, data?: any) {
        if (!this.logging?.logger) {
            console.log(type, message, data);
        } else {
            this.logging?.logger?.log(type, message, { transactionId: this.logging?.transactionId, ...data });
        }
    }

    /**
     * Returns the configured placements (e.g. body, front, ..) and their page numbering style.
     */
    public static getPlacements(configurations: GenericConfiguration<any>[] = [], defaultPlacementNumbering = 'decimal') {
        // placements are only relevant for paged media documents.
        let placement = {
            body: { numbering: defaultPlacementNumbering }
        };
        const pageConfiguration: GenericConfiguration<PageConfiguration> | undefined = configurations?.find(conf => conf.kind === Page.name);
        if (pageConfiguration) {
            const availablePlacements = pageConfiguration.spec?.placements || ['body'];
            for (const conf of configurations?.filter(conf => conf.kind === HeadersAndFooters.name) as GenericConfiguration<HeadersAndFootersConfiguration>[]) {
                if (conf.page && availablePlacements?.includes(conf.page)) {
                    placement[conf.page] = { numbering: conf.spec.pageCounter?.style || 'decimal' };
                }
            }
        }

        return placement;
    }

    /**
     * Placement configuration options with values for each document region.
     */
    get placement(): { [placement: string]: PlacementOption } {
        return RenderingEngine.getPlacements(this.data.configurations);
    }

    /**
     * Update listings for headings, figures, .. inside the engine (e.g. because new headings/elements were injected)
     */
    async generateListings(partDocs: DocumentNode<any>[] = [], baseLevel = 1, partFileMap?: PartFileMap): Promise<void> {
        if (!(this.data as any).document) { (this.data as any).document = {}; }
        const listings = await generateListings(partDocs, undefined, this as RenderingEngine<any, any>, this.data.configurations, baseLevel, partFileMap || this.data.partFileMap);
        this.data.partFileMap = partFileMap;
        // TODO we should be able to depend on the typings here
        (this.data as any).document.tables = listings;
        this.data.tableOfContents = listings.toc;
    }

    /** Renders a node using the supplied renderers. */
    async render(node: DocumentNode<any>, parent?: DocumentNode<any>): Promise<OutputFormat> {
        if (node == null) { throw new Error('Node may not be undefined'); }
        if (!node?.type) { throw new Error('Node type must be defined'); }
        if (this.renderers[node.type] == null) { throw new Error('No renderer for ' + node.type); }

        // FIXME check this.renderers[node.type] instanceof NodeRenderer
        if (typeof this.renderers[node.type]?.render === 'function') {
            const renderer = this.renderers[node.type] as Renderer<OutputFormat>;
            let renderedElement;
            try {
                renderedElement = await renderer.render(node, parent);
            } catch (e: any) {
                // only log render errors once
                if (e instanceof RenderError) { throw e; }
                throw new RenderError('Could not render node', { attrs: node.attrs, type: node.type, message: e.message });
            }

            for (let plugin of this.plugins) {
                try {
                    renderedElement = await plugin(node, renderedElement, this);
                } catch (e: unknown) {
                    if (e instanceof Error) {
                        throw new Error(`Plugin failed: ${e?.message}`);
                    }
                }
            }

            return renderedElement;
        } else {
            throw new Error('Renderer for ' + node.type + ' was not a valid renderer');
        }
    }

    /**
     * Renders the node content only.
     */
    async renderContent(node: DocumentNode<any>): Promise<OutputFormat[]> {
        if (node == null) { throw new Error('Node may not be undefined'); }
        let content: OutputFormat[] = [];
        if (node.content) {
            for (let child of node.content) {
                const renderResult = await this.render(child, node);
                if (Array.isArray(renderResult)) {
                    content = [...content, ...renderResult.flat()];
                } else {
                    content.push(renderResult);
                }
            }
        }

        return content;
    }
}

// extract parts to generate the tod
export const flattenParts = (parts: DocumentNode<SFNodeType.part>[]) =>
    parts.map(p => {
        if (p.type !== SFNodeType.part) {
            return [p];
        }

        if (p.content) {
            return p.content
                .map(p2 => ({ ...p2, attrs: { ...(p2.attrs || {}), standalone: p.attrs?.standalone } })) as DocumentNode<SFNodeType.document>[];
        }

        return [];
    }).flat();

let g = 0;

/**
 * Renders a numbered table of contents.
 */
export const generateToc = async (parts: DocumentNode<rootDoc>[], partId?: string, defaultNumbering?: counterStyle, engine?: RenderingEngine<HTMLElement, SciFlowDocumentData>, baseLevel = 1, opts?: { idMap: any; partFileMap?: PartFileMap }): Promise<Toc> => {
    let flattenedParts = [...parts];
    if (parts.some(p => p.type === SFNodeType.part)) {
        flattenedParts = flattenParts(parts as DocumentNode<SFNodeType.part>[]);
    }

    const numberingFallback = defaultNumbering || 'decimal';

    /** Initializes the counter array with 10 levels */
    const initCounter = (initial: number[] = []): Array<number> => '0'.repeat(10).split('').map((_, index) => initial[index] ?? 0);
    const initStyles = (initial: string[] = []): Array<string> => '0'.repeat(10).split('').map((_, index) => initial[index] ?? '');

    /** 
     * Creates a numbered value for each heading based on the providing numbering schemes.
     */
    const createNumberedHeadings = async (headingNodes: HeadingDocumentNode[]): Promise<TocHeading[]> => {
        let headings: TocHeading[] = [];
        /** Counters for heading levels counters[0] is the value for h1 */
        let counters = initCounter();
        let counterStyles = initStyles();
        // memorize the numbering style of the current chapter (e.g. level 1 heading)
        let currentChapterNumbering = numberingFallback;

        for (let heading of headingNodes) {
            // we allow headings to reset all counters (e.g. for standalone documents)
            let increment = true;
            let headingLevel = heading.attrs?.level || 1;
            let bookmarkLevel: 'none' | number | undefined = heading.attrs?.skipToc ? 'none' : headingLevel;
            if (heading.attrs?.standalone && heading.attrs?.isTitle) {
                counters = initCounter();
                counterStyles = initStyles();
                increment = false; // we do not count the title of a standalone doc
            } else if (heading.attrs?.standalone) {
                if (!heading.attrs?.skipToc && heading.attrs.level) {
                    // we want bookmarks of non titles to show up below the title
                    bookmarkLevel = heading.attrs.level + 1;
                }
            }

            if (typeof headingLevel === 'string') { headingLevel = Number.parseInt(headingLevel); }
            if (headingLevel === 1) { currentChapterNumbering = heading.attrs.numbering || numberingFallback; }
            let counterStyle = heading.attrs.numbering || currentChapterNumbering;
            // fix legacy naming
            if (counterStyle === 'numeric') { counterStyle = 'decimal'; }
            
            // when counter stye changes, reset everyt hing
            if (counterStyles[headingLevel - 1] !== counterStyle || counterStyle === 'none') {
                counters = initCounter(counters.slice(0, headingLevel - 1));
            }

            // reset styles below the current level...
            counterStyles = initStyles(counterStyles.slice(0, headingLevel));
            if (counterStyles[headingLevel - 1] !== counterStyle || counterStyle === 'none') {
                counterStyles[headingLevel - 1] = counterStyle;
            }

            // increase the counter for the current heading level
            increment && counters[headingLevel - 1]++;
            // and reset the level for all counters smaller than the current level
            counters = counters.map((value, index) => index >= headingLevel ? 0 : value); // [1,2,4] on level 2 becomes [1,3,1] after increasing h2 to 3 and resetting the rest

            const numberingString = createPrefix(counters, counterStyles);
            const headingEntry = opts?.idMap?.[heading.attrs.id];
            const headingPartId = partId ?? headingEntry?.partId ?? heading?.attrs?.partId;
            // if the document is split across multiple files we need to record the file as a prefix for links
            const hostFileName = headingPartId && opts?.partFileMap ? opts?.partFileMap?.[headingPartId] : undefined;
            headings.push({
                ...(await extractHeadingTitle(heading, engine)),
                id: heading.attrs.id,
                numberingString: numberingString,
                level: headingLevel,
                bookmarkLevel,
                skipToc: heading.attrs?.skipToc,
                standalone: heading.attrs?.standalone,
                isTitle: heading.attrs?.isTitle,
                counterStyle,
                partId: headingPartId,
                hostFileName,
                placement: heading.attrs?.placement,
                placementNumbering: heading.attrs?.placementNumbering
            } as any);
        }

        return headings;
    };

    const headings = await createNumberedHeadings(flattenedParts.map(part => {
        const skipToc = part.attrs?.skipToc === true;
        return (part?.content?.filter(d => d.type === SFNodeType.heading) as HeadingDocumentNode[] || []).map((heading, i) => {
            let level = heading.attrs?.level;
            if (typeof level === 'string') { level = Number.parseInt(level); }

            if (!heading.attrs) { heading.attrs = { id: part.attrs.id }; }
            heading.attrs.partId = part.attrs.id;
            heading.attrs.skipToc = skipToc;
            if (part.attrs.type === 'title') { heading.attrs.isTitle = true; }

            if (part.attrs.standalone) { heading.attrs.standalone = true; }
            if (!heading.attrs.placementNumbering) { heading.attrs.placementNumbering = part.attrs?.placementNumbering; }
            if (part.attrs.id === 'title' && !heading.attrs.numbering) { heading.attrs.numbering = 'none'; }

            // we only set the numbering if it was not explicitly set
            if (!heading.attrs.numbering && level) {
                if (level === baseLevel) {
                    // at the base level, use the part numbering
                    heading.attrs.numbering = part.attrs?.numbering || defaultNumbering;
                } else if (level > baseLevel) {
                    // above base level only inherit from part for decimal and none (there is no I.i.i or A.a.a)
                    if (['decimal', 'none'].includes(part.attrs?.numbering)) {
                        heading.attrs.numbering = part.attrs?.numbering || defaultNumbering;
                    } else {
                        heading.attrs.numbering = numberingFallback;
                    }
                }
            }

            return {
                ...heading,
                attrs: {
                    ...heading.attrs || {},
                    level
                },
                part,
                empty: !heading.content || heading.content.length === 0
            };
        });
    }).flat().filter(h => !h.empty));

    return {
        headings
    };
}

