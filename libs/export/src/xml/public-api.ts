import jatsRenderers from './renderers';
import * as jatsMetaData from './renderers/metaData';
import * as jatsReferences from './renderers/references';

export namespace JATS {
    export const renderers = jatsRenderers;
    export const metaData = jatsMetaData;
    export const references = jatsReferences;
};
