import { XMLJSXFactory as TSX } from '../../XMLJSXFactory';

import { SciFlowDocumentData } from "../../interfaces";
import { DocumentNode, SFNodeType } from '@sciflow/schema';
import { NodeRenderer } from "../../node-renderer";
import { XMLBuilderImpl } from 'xmlbuilder2/lib/builder';

class TableXMLRenderer extends NodeRenderer<XMLBuilderImpl, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.table>, parent?: DocumentNode<SFNodeType.figure>) {
        if (!node.content) {
            return (<table></table>);
        }

        const indexOfFirstRow = node.content.findIndex(row => row.content?.some(n => n.type !== SFNodeType.table_header));
        const headers = node.content.slice(0, indexOfFirstRow);
        const rows = node.content.slice(indexOfFirstRow, node.content.length);
        
        // build up the colgroup
        let colcount = 0;
        const colWidths = {};
        for (let row of rows) {
            if (row.content) {
                for (let i = 0; i < row.content?.length; i++) {
                    const cell = row.content[i];
                    const cellcount = row.content.reduce((count, cell, i) => count + (cell.attrs?.colspan || 1), 0);
                    if (cellcount > colcount) {
                        colcount = cellcount;
                    }
                    if (cell.attrs.colspan)
                    if (cell.attrs.colwidth) {
                        if (colWidths[i] != undefined && colWidths[i] !== cell.attrs.colwidth?.[0]) {
                            console.warn('Found two cells in the same column with different widths', { attrs: row.attrs, i, prev: colWidths[i] });
                        }
                        colWidths[i] = cell.attrs.colwidth?.[0];
                    } else {
                        if (colWidths[i] == undefined && colWidths[i] !== cell.attrs.colwidth?.[0]) {
                            console.warn('Found two cells in the same column with different widths', { attrs: row.attrs, i, prev: colWidths[i] });
                        }
                        colWidths[i] = undefined;
                    }
                }
            }
        }
        const colgroup = <colgroup>
            {...Object.keys(colWidths).map((i) => colWidths[i] != undefined ? <col width={colWidths[i]} /> : <col />)}
        </colgroup>;

        const headersRendered = await this.engine.renderContent({ ...node, content: headers });
        const rowsRendered = await this.engine.renderContent({ ...node, content: rows });

        const captionWrapper = parent?.content?.find(node => node?.type === SFNodeType.caption);
        let captionText: any[] = [];
        if (captionWrapper) {
            const caption = captionWrapper.content && captionWrapper.content[0];
            if (caption) {
                captionText = await this.engine.renderContent(caption);
            }
        }

        // only get tables that have a caption
        if (!this.engine.data.document.tables) { throw new Error('Tables must be generated at this point'); }
        const tables = this.engine.data.document?.tables?.tables.filter(table => table?.title?.length != undefined && table?.title?.length > 0);
        const index = tables?.findIndex(t => t.id === node?.attrs?.id);
        const table = <table id={'tbl-' + node?.attrs?.id}  frame="box" rules="rows" cellpadding="5">
            {(index > -1 && captionText?.length > 0) ? <caption data-table-number={index > -1 ? index + 1 : null}>{...captionText}</caption> : ''}
            {colgroup}
            {headers.length > 0 ? <thead>
                {...headersRendered}
            </thead> : ''}
            <tbody>
                {...rowsRendered}
            </tbody>
        </table>;

        return table;
    }
}

class TableCellXMLRenderer extends NodeRenderer<XMLBuilderImpl, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.table_cell | SFNodeType.table_header>) {

        const children = await this.engine.renderContent(node);
        const align = node.content?.find(({ attrs }) => attrs && attrs['text-align'])?.attrs['text-align'];
        let cellEl = <td>{...children}</td>;
        if (node.type === SFNodeType.table_header) {
            cellEl = <th>{...children}</th>;
            // htmlEl.setAttribute('custom-style', 'Table Heading');
        }
        if (align) {
            cellEl.att('style', `text-align: ${align}`);
        }

        if (node.attrs && node.attrs.colwidth) {
            // we use colgroups instead
            //htmlEl.setAttribute('width', node.attrs.colwidth);
        }

        if (node.attrs && node.attrs.colspan) {
            cellEl.att('colspan', node.attrs.colspan);
        }

        if (node.attrs && node.attrs.rowspan) {
            cellEl.att('rowspan', node.attrs.rowspan);
        }

        return cellEl;
    }
}

class TableRowXMLRenderer extends NodeRenderer<XMLBuilderImpl, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.table_row>, _parent?: DocumentNode<any>) {
        const children = await this.engine.renderContent(node);

        return <tr>
            {...children}
        </tr>;
    }
}

const renderers: { [key: string]: any } = {
    [SFNodeType.table]: TableXMLRenderer,
    [SFNodeType.table_row]: TableRowXMLRenderer,
    [SFNodeType.table_cell]: TableCellXMLRenderer,
    [SFNodeType.table_header]: TableCellXMLRenderer
};

export default renderers;