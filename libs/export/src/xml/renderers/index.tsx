import { DocumentNode, SFNodeType } from '@sciflow/schema';
import { XMLJSXFactory as TSX, XMLJSXFactory } from '../../XMLJSXFactory';
import { SciFlowDocumentData } from '../../interfaces';
import { NodeRenderer } from '../../node-renderer';
import htmlRenderers from '../../html/renderers';
import tableRenderers from './table';
import citationRenderers from './citation';
import { SourceField } from '@sciflow/cite';
import { create, convert } from 'xmlbuilder2';
import { FigureRendererConfiguration } from '../../html/renderers/figure/figure.schema';
import { Environment } from '../../html/components/environment/environment.component';
import { slugify } from '../../utils';
import { XMLBuilderImpl } from 'xmlbuilder2/lib/builder';
import { labelToEnvironment } from '../../html';

class RenderWarning extends NodeRenderer<any, SciFlowDocumentData> {
    async render(node: DocumentNode<SFNodeType.heading>) {
        return convert(`${node.type}`).toString();
    }
}

const renderers: { [key: string]: any } = {
    ...htmlRenderers,
    ...tableRenderers,
    ...citationRenderers,
    [SFNodeType.section]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.section>) {
            try {
                const content = await Promise.all(node.content?.map(async (c) => {
                    const rc: any = await this.engine.render(c);
                    return rc;
                }) || '');
                return <sec id={slugify(node.attrs.id)} sec-type={node.attrs.type}>
                    {...content}
                </sec>;
            } catch (e: any) {
                debugger;
                console.error(e);
                throw new Error('Could not render section ' + node?.attrs?.id);
            }
        }
    },
    [SFNodeType.blockquote]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.blockquote>) {
            const children = await this.engine.renderContent(node);
            return <disp-quote>{...children}</disp-quote>;
        }
    },
    [SFNodeType.poetry]: class extends NodeRenderer<string, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.poetry>) {
            const content = await this.engine.renderContent(node);
            return <disp-quote>{...content}</disp-quote>;
        }
    },
    [SFNodeType.note]: class extends NodeRenderer<string, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.note>) {
            // we do not render notes in the output
            return '';
        }
    },
    [SFNodeType.bullet_list]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.bullet_list>) {
            const children = await this.engine.renderContent(node);
            return <list list-type="bullet">{...children}</list>;
        }
    },
    [SFNodeType.list_item]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.list_item>) {
            const children = await this.engine.renderContent(node);
            return <list-item>{...children}</list-item>;
        }
    },
    [SFNodeType.ordered_list]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.ordered_list>) {
            const children = await this.engine.renderContent(node);
            return <list list-type="order">{...children}</list>;
        }
    },
    [SFNodeType.caption]: RenderWarning,
    [SFNodeType.label]: RenderWarning,
    [SFNodeType.code]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.code>) {
            const children = await this.engine.renderContent(node);
            return <code language={node.attrs.language ?? 'text'}>
                {...children}
            </code>;
        }
    },
    [SFNodeType.footnote]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.footnote>) {
            const index = this.engine.data.document.tables?.footnotes?.findIndex(fn => fn?.attrs?.id === node.attrs?.id);
            return <xref ref-type="fn" rid={slugify(node.attrs.id)}>{index > -1 ? (index + 1) : '*'}</xref>;
        }
    },
    [SFNodeType.hardBreak]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.hardBreak>) {
            // https://jats.nlm.nih.gov/publishing/tag-library/1.3/element/break.html
            // breaks are discourraged
            return '';
        }
    },
    [SFNodeType.header]: RenderWarning,
    [SFNodeType.horizontalRule]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.horizontalRule>) {
            return <hr />;
        }
    },
    [SFNodeType.hyperlink]: RenderWarning,
    [SFNodeType.image]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.image>) {
            const file = this.engine.data.document.files.find(f => f.id === node.attrs.id);
            let altText = <alt-text></alt-text>;
            if (node.attrs.decorative === true) {
                altText = <alt-text>null</alt-text>; // following https://jats4r.niso.org/accessibility/ 
                // quote: >> The explicit “null” value is preferred because an empty <alt-text> could mean that the value was simply forgotten
            } else if (node.attrs.alt?.length > 0) {
                altText = <alt-text>{node.attrs.alt}</alt-text>;
            }

            return <graphic xmlns_xlink="http://www.w3.org/1999/xlink" xlink_href={file?.url}>
                {altText}
            </graphic>;
        }
    },
    [SFNodeType.link]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.link>) {
            const referencedElement = node.attrs.href.replace('#', '');
            const idMap = this.engine.data.document.tables.idMap;
            const toc = this.engine.data.document.tables.toc;
            const link = idMap[referencedElement];
            if (!link) { return <xref>Unknown reference {referencedElement}</xref>; }
            if (!this.engine.data.document.tables.labels) {
                throw new Error('Labels must be defined to xref');
            }
            const index = this.engine.data.document.tables.labels[link.type]?.findIndex(d => d.id === referencedElement);
            let text = index > -1 ? (index + 1) : link.title;

            const environmentComponent = new Environment(this.engine as any);
            const configuration: FigureRendererConfiguration = environmentComponent.getValues();
            const environment = configuration.environments?.[link.type];

            if (link.type === SFNodeType.heading) {
                const tocEntry = toc.headings?.find(h => h.id === referencedElement);
                if (tocEntry?.numberingString) {
                    text = tocEntry?.numberingString;
                }
            }
            let refType = 'fig';
            switch (link.type) {
                case 'image':
                    refType = 'fig';
                    break;
                case 'table':
                    refType = 'table';
                default:
                    refType = link.attrs?.type || 'fig';
            }

            return <xref ref-type={refType} rid={slugify(referencedElement)}>
                {environment?.referenceLabel ? environment?.referenceLabel + ' ' : ''}{text}
            </xref>;
        }
    },
    [SFNodeType.math]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.math>) {
            if (node.attrs.style === 'block') {
                return <disp-formula id={slugify(node.attrs.id)}>
                    <tex-math>
                        {node.attrs.tex}
                    </tex-math>
                </disp-formula>;
            }

            return <inline-formula>
                <tex-math>
                    {node.attrs.tex}
                </tex-math>
            </inline-formula>;
        }
    },
    [SFNodeType.pageBreak]: RenderWarning,
    [SFNodeType.paragraph]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<SFNodeType.paragraph>) {
            const paragraphContent = await this.engine.renderContent(node);
            return <p id={slugify(node.attrs.id)}>{...paragraphContent}</p>;
        }
    },
    [SFNodeType.quote]: RenderWarning,
    [SFNodeType.text]: class extends NodeRenderer<XMLBuilderImpl | string, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.text>) {
            const hyperlink = node.marks?.find(m => m.type === 'anchor');
            let text = node.text;

            if (hyperlink) {
                return <ext-link ext-link-type='uri' xlink_href={hyperlink.attrs.href.replace('&', '#26')}>{text}</ext-link>;
            }

            if (node.marks?.some(mark => mark.type === 'strong')) {
                text = <b>{text}</b>;
            }

            if (node.marks?.some(mark => mark.type === 'em')) {
                text = <italic>{text}</italic>;
            }

            if (node.marks?.some(mark => mark.type === 'sub')) {
                text = <sub>{text}</sub>;
            }

            if (node.marks?.some(mark => mark.type === 'sup')) {
                text = <sup>{text}</sup>;
            }

            return text;
        }
    },
    [SFNodeType.figure]: class extends NodeRenderer<HTMLElement, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.figure>) {
            if (node.type !== 'figure') {
                throw new Error('Must start with a figure but found ' + node.type);
            }

            if (!node.content) {
                return <fig />;
            }

            const figureComponent = new Environment(this.engine);
            const configuration: FigureRendererConfiguration = figureComponent.getValues();

            const file = this.engine.data.document.files.find(f => f.id === node.attrs.id);

            const captionNode = node?.content?.find(node => node?.type === SFNodeType.caption);
            const labelNode = captionNode?.content?.find(c => c.type === SFNodeType.label);
            const notes = captionNode?.content && captionNode.content.slice(labelNode ? 2 : 1);
            let captionText: HTMLElement[] = [];
            const captionContent = captionNode?.content?.filter(c => c.type !== SFNodeType.label);

            const containsTableNode = node.content?.some(n => n.type === SFNodeType.table);
            const containsCode = node.content?.some(n => n.type === SFNodeType.code);
            let envName, environment;
            if (node.attrs.type === 'table') {
                // might have a figure
                envName = 'table';
            } else if (node.attrs.type === 'figure') {
                envName = 'image';
            } else if (containsCode) {
                envName = 'code';
            } else {
                envName = containsTableNode ? 'table' : 'image';
            }

            if (labelNode) {
                const env = labelNode ? labelToEnvironment(labelNode, configuration) : undefined;
                if (env) {
                    envName = env;
                }
            }

            environment = configuration?.environments?.[envName];
            if (captionContent && captionContent?.length > 0) {
                const titleNode = captionContent[0];
                if (titleNode) {
                    if (labelNode && titleNode?.content) {
                        // remove the label from the caption
                        titleNode.content = titleNode.content.filter(c => c.type !== SFNodeType.label);
                    }
                    captionText = await this.engine.renderContent(titleNode);
                }
            }

            const captionNotesContent = notes ? await Promise.all(notes.map(async (note) => this.engine.renderContent(note))) : [];
            if (!this.engine.data.document.tables) { throw new Error('Tables must be generated at this point'); }
            const tables = this.engine.data.document.tables?.labels[envName]
                .filter(table => table?.title?.length != undefined && table?.title?.length > 0 || environment?.countWithoutCaption);
            const index = tables
                .findIndex(c => c.nodeId === node.attrs.id || c.id === node.attrs.id);

            let altText = <alt-text></alt-text>;
            if (node.attrs.decorative === true) {
                altText = <alt-text>null</alt-text>; // following https://jats4r.niso.org/accessibility/ 
                // quote: >> The explicit “null” value is preferred because an empty <alt-text> could mean that the value was simply forgotten
            } else if (node.attrs.alt?.length > 0) {
                altText = <alt-text>{node.attrs.alt}</alt-text>;
            }

            if (containsTableNode) {
                const tableContent = await this.engine.renderContent({ ...node, content: node.content.filter(n => n.type === 'table') });
                return <table-wrap id={slugify(node.attrs?.id)}>
                    <label>
                        {environment?.leftDelimiter ? environment.leftDelimiter : ''}
                        {!environment?.suppressLabel ? environment?.label + ' ' : ''}{index + 1}
                        {environment?.labelSuffix}
                        {environment?.rightDelimiter ? environment.rightDelimiter : ''}
                    </label>
                    <caption>
                        <title>{...captionText}</title>
                    </caption>
                    {...tableContent}
                    {captionNotesContent.length > 0 ? <table-wrap-foot>{...captionNotesContent.map(note => <p>{note}</p>)}</table-wrap-foot> : ''}
                </table-wrap>;
            }

            return <fig id={slugify(node.attrs?.id)} fig-type="content-image">
                <label>
                    {environment?.leftDelimiter ? environment.leftDelimiter : ''}
                    {!environment?.suppressLabel ? environment?.label + ' ' : ''}{index + 1}
                    {environment?.labelSuffix}
                    {environment?.rightDelimiter ? environment.rightDelimiter : ''}
                </label>
                <caption>
                    <title>{...captionText}</title>
                    {...captionNotesContent.map(note => <p>{note}</p>)}
                </caption>
                <graphic xmlns_xlink="http://www.w3.org/1999/xlink" xlink_href={file?.url}>
                    {altText}
                </graphic>
            </fig>;
        }
    },
    [SFNodeType.heading]: class extends NodeRenderer<Element, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.heading>) {
            const content = await this.engine.renderContent(node);
            return <title>
                {...content}
            </title>;
        }
    },
    [SFNodeType.document]: class extends NodeRenderer<Element, SciFlowDocumentData> {
        async render(node: DocumentNode<SFNodeType.document>) {
            const content = await this.engine.renderContent(node);
            return <article xmlns_xlink="http://www.w3.org/1999/xlink" article-type="research-article">
                <front></front>
                <body>
                    {...content}
                </body>
                <back></back>
            </article>;
        }
    },
    [SFNodeType.placeholder]: RenderWarning
}

export default renderers;
