import { hash } from '../../helpers';
import { slugify } from '../../utils';
import { XMLJSXFactory as TSX } from '../../XMLJSXFactory';

export const getAffiliationSlug = (position) => {
    return slugify('aff' + hash([position.institution, position.country].filter(n => n != null).join('-')));
}

export const getAuthorSlug = (author) => {
    return slugify(hash([author.email?.replace('@', ''), author.name, author.firstName, author.lastName].filter(n => n != null).join('-')));
}

const getContribType = (roles: string[], type) => {
    if (type === 'group') { return type; }
    if (roles?.map(r => r.toLowerCase()).includes('editor')) { return 'editor'; }
    if (roles?.map(r => r.toLowerCase()).includes('author')) { return 'author'; }
    return 'person';
}

export const getAuthorObj = (author) => {
    return <contrib contrib-type={getContribType(author.roles, author.type)} corresp={author.correspondingAuthor ? 'yes' : 'no'} equal-contrib={author.equalContribution ? 'yes' : 'no'} deceased={author.deceased ? 'yes' : 'no'} id={getAuthorSlug(author)}>
    {author.orcid?.length > 0 ? <contrib-id contrib-id-type="orcid">https://orcid.org/{author.orcid}</contrib-id> : ''}
    {author.name?.length > 0 ? <named-content content-type="name">{author.name}</named-content> : ''}
    {!author.name || author.name?.length === 0 ? <name>
        <surname>{author.lastName}</surname>
        <given-names>{author.firstName}</given-names>
    </name> : ''}
    {author.email?.length > 0 ? <email>{author.email}</email> : ''}
    {author.comment?.length > 0 ? <author-comment>{TSX.stringToElement(author.comment)}</author-comment> : ''}
    {author.bio?.length > 0 ? <bio>{author.bio.startsWith('<') ? TSX.stringToElement(author.bio) :  author.bio}</bio> : ''}
    {/* <role vocab="CRediT" vocab-identifier="http://dictionary.casrai.org/Contributor_Roles" vocab-term="Writing – Original Draft"
vocab-term-identifier="http://dictionary.casrai.org/Contributor_Roles/Conceptualization" degree-contribution="lead">Translator</role> */}
    {/* <xref ref-type="fn" rid="no-conflict"/> */}
    {author.positions?.map(position => <xref ref-type="aff" rid={getAffiliationSlug(position)} />)}
    {(author.correspondingAuthor && author.email?.length > 0) ? <xref ref-type="corresp" rid={'corr-' + getAuthorSlug(author)}>&#x2010;</xref> : ''}
    {author.funders?.map(funder => <xref ref-type="award" rid={'fund' + funder.id} />)}
</contrib>;
};

export const getGroupObj = (group, authors) => {
    const collaborators = authors.filter(a => a.groups?.indexOf(group.authorId) > -1);
    return <contrib contrib-type="group" corresp={group.correspondingAuthor ? 'yes' : 'no'} equal-contrib={group.equalContribution ? 'yes' : 'no'} deceased={group.deceased ? 'yes' : 'no'} id={getAuthorSlug(group)}>
        <collab>
            <named-content content-type="name">{group.name}</named-content>
            {group.positions?.map(position => <xref ref-type="aff" rid={getAffiliationSlug(position)} />)}
            {group.funders?.map(funder => <xref ref-type="other" rid={funder.id} />)}
            {group.email?.length > 0 ? <email>{group.email}</email> : ''}
            {group.bio?.length > 0 ? <bio>{group.bio}</bio> : ''}
            {collaborators?.length > 0 ? <contrib-group>
                {...collaborators.map(collaborator => getAuthorObj(collaborator))}
            </contrib-group> : ''}
        </collab>
    </contrib>;
};