import { NodeRenderer } from "../../node-renderer";
import { DocumentNode, SFNodeType } from "../../../../schema/src";
import { RenderingEngine } from "../../renderers";

export class RenderContent extends NodeRenderer<any, any> {
    async render(node) {
        return this.engine.renderContent(node);
    }
}

/**
 * Renders a document to markdown.
 */
export interface DefaultMarkdownElement {
    render(node: DocumentNode<SFNodeType.document>, opts: { engine: RenderingEngine<string, any> }): Promise<string>;
}

const wrapIntoBlock = (content, attrs) => {
    if (!attrs) { return content; }
    let wrapper = `::: {${Object.entries(attrs).filter(([key, value]) => value != null).map(([key, value]) => `${key}='${value}'`).join(' ')}}\n`;
    wrapper += content;
    return wrapper + '\n:::\n';
}

/**
 * Renders the abstract with an inline heading text instead.
 */
export class CustomBlockElement implements DefaultMarkdownElement {
    constructor(private options: { attrs: any }) { }

    async render(node: DocumentNode<SFNodeType.document>, { engine }) {
        const attrs = this.options?.attrs || { id: node.attrs.id };
        if (!attrs.id) {
            this.options.attrs.id = node.attrs.id;
        }
        const nodeContent = await engine.render(node);
        return wrapIntoBlock(nodeContent, attrs);
    }
}

/**
 * Renders the abstract with an inline heading text instead.
 */
 export class CustomTextBodyElement implements DefaultMarkdownElement {
    constructor(private options?: { attrs: any }) { }

    async render(node: DocumentNode<SFNodeType.document>, { engine }) {
        const attrs = this.options?.attrs || { id: node.attrs.id };
        if (!attrs.id && this.options?.attrs.id) {
            this.options.attrs.id = node.attrs.id;
        }

        let content = '';
        const heading = node.content?.find(n => n.type === 'heading' && (node.attrs?.level === 1 || !node.attrs?.level));
        if (heading) {
            // only render the text
            content = await engine.render(heading);
        }

        const body = await engine.renderContent({ ...node, content: node.content?.slice(heading ? 1 : 0, node.content.length) });
        content += '\n' + wrapIntoBlock(body.join('\n'), attrs);
        return content;
    }
}

/**
 * Renders the abstract with an inline heading text instead.
 */
export class InlineHeadingElement implements DefaultMarkdownElement {
    constructor(private options?: { attrs: any }) { }

    async render(node: DocumentNode<SFNodeType.document>, { engine }) {
        const attrs = this.options?.attrs || { id: node.attrs.id };
        if (!attrs.id && this.options?.attrs.id) {
            this.options.attrs.id = node.attrs.id;
        }

        let content = '';
        const heading = node.content?.find(n => n.type === 'heading' && (node.attrs?.level === 1 || !node.attrs?.level));
        if (heading) {
            // only render the text
            const headingContent = await engine.renderContent(heading);
            content = `**${headingContent.join('')}** `;
        }

        const body = await engine.renderContent({ ...node, content: node.content?.slice(heading ? 1 : 0, node.content.length) });
        content += body.join('\n');
        return wrapIntoBlock(content, attrs);
    }
}
