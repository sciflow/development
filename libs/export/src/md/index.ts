import { extname } from 'path';
import { dump } from 'js-yaml';
import { stripIndents } from 'common-tags';
import { RenderingEngine } from '../';
import { hash } from '../helpers';

export * from './elements';

import renderers from './renderers';

interface File {
    isDirectory?: boolean;
    name?: string;
    mediaType?: string;
    children?: File[];
    content?: {
        blob?: string;
        path?: string;
        url?: string;
    }
}

/**
 * Renders a document node (mainly for testing)
 * @param documentNode 
 * @param parentNode 
 */
export const renderNode = (documentNode, parentNode?) => {
    const renderer = new RenderingEngine<string, any>(renderers, { document: documentNode });
    return renderer.render(documentNode, parentNode);
}

/**
 * File structure for a md file including all assets.
 * @param data meta data needed to construct the document.
 */
const createPandocMarkdownFile = async (data: { document, template, tables, references }, options: { absoluteUrls: boolean, format: 'md', citationStyle: string; lot: boolean; lof: boolean; authorsInMd: boolean }) => {
    const renderer = new RenderingEngine<string, any>(renderers, { ...data, options });
    const references = data.document.references
        .map(ref => {
            // we remove the abstract since pandoc will error with multi-line text
            delete ref.abstract
            return ref;
        })
        .map(({ id }) => data.document.references.find((ref => ref.id === id)))
        .map((reference) => {
            // @see https://github.com/Juris-M/citeproc-js/issues/84
            if (reference.journalAbbreviation?.length > 0 && !reference['container-title-short']) {
                reference['container-title-short'] = reference.journalAbbreviation;
            }
            if (reference.shortTitle?.length > 0 && !reference['title-short']) {
                reference['title-short'] = reference.shortTitle;
            }
            return reference;
        });

    const footnotes = await Promise.all(data.tables.footnotes.map(async (footnote) => {
        return {
            footnote,
            children: await renderer.renderContent(footnote)
        }
    }));

    const hasFigures = data.tables.figures.filter(figure => figure.title && figure.title.length > 0);
    const hasTables = data.tables.tables.filter(table => table.title && table.title.length > 0);

    const getAffiliationSlug = (position) => {
        return hash([position.institution, position.country].filter(n => n != null).join('-'));
    }

    const getAuthorSlug = (author) => {
        return hash([author.email.replace('@', ''), author.firstName, author.lastName].filter(n => n != null).join('-'));
    }

    const affiliations: any[] = [].concat.apply([], data.document.authors.map(author => author.positions?.map(position => ({
        slug: getAffiliationSlug(position),
        ...position
    }))))
        .filter((position: any, index, list: any[]) => list.findIndex((pos) => pos?.slug === position?.slug) === index)
        .filter(c => c != null);

    const institutes = affiliations.map((affiliation) => {

        const name = [affiliation.institution, affiliation.department]
            .map(v => v?.trim())
            .filter(v => v?.length > 0)
            .join(' ');

        const address = [affiliation.street, affiliation.city, affiliation.country]
            .map(v => v?.trim())
            .filter(v => v?.length > 0)
            .join(' ');

        const obj: any = {
            id: affiliation?.slug
        };

        if (name?.length > 0) {
            obj.name = name;
        }

        if (address?.length > 0) {
            obj.address = address;
        }

        // the meta data scripts always expect a name even if it is not filled
        if (!obj.name) {
            obj.name = name ?? address;
        }

        return obj;
    });

    const filterAuthor = author => author.hideInPublication !== true && (author.firstName || author.lastName || author.name) && author.roles.includes('Author');

    const authors = data.document.authors
            .filter(filterAuthor)
            .map((author) => {
        const authorData: any = {
            correspondence: author.correspondingAuthor ? 'yes' : 'no',
            deceased: author.deceased ? 'yes' : 'no',
            equal_contributor: author.equalContribution ? 'yes' : 'no'
        };

        if (author.email?.length > 0) {
            authorData.email = author.email;
        }

        if (author.orcid?.length > 0) {
            authorData.orcid = author.orcid;
        }

        if (author.positions?.length > 0) {
            authorData.institute = author.positions?.map(position => getAffiliationSlug(position));
        }

        if (!author.name || author.name.length === 0) {
            author.name = [author.firstName, author.lastName].filter(n => n != null).join(' ');
        }

        return {
            // we need to create an object with the author name as the key
            [author.name]: authorData
        };
    });

    let authorBlock = '';
    if (options.authorsInMd) {
        authorBlock = `\n::: {custom-style='Author'}\n\n${data.document.authors.filter(filterAuthor).map(author => {
            const positions = author.positions?.map(position => [position.institution, position.department, position.street, position.city, position.country].join(' ')).join('\n');
            const authorString = (author.firstName?.length > 0 || author.firstName.lastName?.length > 0) ? [author.firstName, author.lastName].filter(a => a != null).join(' ') : author.name;
            return [
                `**${authorString.length > 0 ? authorString : 'Unnamed author ' + (author.rank + 1)}**  `,
                author.orcid ? author.orcid + '  ' : author.orcid,
                positions ? positions + '  ' : null,
                author.email
            ]
                .filter(a => a != null)
                .join('\n');
        }).join('\n\n')}\n\n:::\n`;
    }

    const metaData = {
        title: data.document.title,
        lang: data.document.locale || data.template.language,
        author: authors,
        institute: institutes,
        'link-citations': true,
        secHeaderTemplate: '$$secHeaderPrefix[n]$$$$i$$. $$t$$',
        secHeaderPrefix: ['&#32;', '&#32;', ''],
        mathjax: true,
        math: true,
        // secHeaderPrefix: ['Chapter&#32;', 'Section&#32;', ''],
        // determine how deep numbering should go (e.g. 1.1.1)
        sectionsDepth: 4,
        numberSections: true,
        secLabels: 'arabic', // arabic, roman, lowercase roman, alpha x (where x is the first letter)
        'suppress-bibliography': false,
        //'reference-section-title': 'References',
        csl: options.citationStyle,
        figureTitle: 'Figure',
        figureTemplate: '**$$figureTitle$$ $$i$$$$titleDelim$$** $$t$$',
        tableTemplate: '**$$tableTitle$$ $$i$$$$titleDelim$$** $$t$$',
        tableTitle: 'Table',
        figPrefix: 'fig.',
        eqnPrefix: 'eq.',
        tblPrefix: 'tbl.',
        lof: false,
        lot: false,
        // figures will be referenced using the chapter number as a prefix and restart with each heading 1
        chapters: true,
        ...(data.template?.pandocOptions || {})
    };

    if (hasFigures && options.lof) {
        metaData.lof = true;
    }

    if (hasTables && options.lot) {
        metaData.lot = true;
    }

    if (references.length > 0) {
        metaData['references'] = references
            .map(({ _key, ...attrs }) => ({ ...attrs }))
            // bibliography can be an array (ee) or object (sfo)
            .filter(ref => data?.references.bibliography[ref.id] || (data?.references.bibliography?.some && data?.references.bibliography?.some(r => r.id === ref.id)));
    }

    return `---\n${dump(metaData)}...
${authorBlock}
${(await Promise.all(data.document.parts.map(async (part) => {

        // get custom renderers
        const templatePart = data.template?.headings?.elements?.find(heading => {
            if (heading.type === part.type && heading.role === part.role) {
                return heading;
            }

            if (heading.type === part.type && heading.role === 'unique-type') {
                return heading;
            }

            return null;
        });

        let renderedPart;
        if (templatePart?.element) {
            renderedPart = await templatePart.element.render(part.document, { engine: renderer, part });
        } else {
            renderedPart = await renderer.render({
                ...part.document,
                attrs: {
                    ...(part.document.attrs || {}),
                    type: part?.type || part.document.attrs?.type || 'chapter',
                    role: part?.role || part.document?.attrs?.role
                }
            });
            switch (part.type) {
                case 'bibliography':
                    renderedPart += stripIndents`\n\n::: {#refs custom-style="Bibliography"}
            :::`+ '\n\n';
                    break;
            }

            const attrs = [`id=${part.id}`];

            if (part.role && !['none', 'unique-type'].includes(part.role)) { attrs.push(`role=${part.role}`); }
            if (part.options?.pageBreak?.length > 0) { attrs.push(`pageBreak=${part.options?.pageBreak?.join(',')}`); }
            if (part.options?.numbering) { attrs.push(`numbering=${part.options?.numbering}`); }
            if (part.options?.placement) { attrs.push(`placement=${part.options?.placement}`); }
            if (part.type) { attrs.push(`type=${part.type}`); }
            if (part.schema) { attrs.push(`schema=${part.schema}`); }

            renderedPart = `::: {${attrs.join(' ')}}\n${renderedPart}\n\n:::\n\n`;
        }

        let pageBreak: any[] = [];
        if (templatePart?.pageBreak) {
            pageBreak = [...templatePart.pageBreak];
        }

        if (part.options?.pageBreak) {
            pageBreak = [...part.options?.pageBreak];
        }

        if (pageBreak.indexOf('after') !== -1) {
            renderedPart = `${renderedPart}\n\\newpage\n\n`;
        }

        // TODO add locale if part locale != document locale
        //::: {lang=fr-CA}
        // > Cette citation est écrite en français canadien.
        // :::
        // English text ['Zitat auf Deutsch.']{lang=de}

        return renderedPart;
    }
    ))).join('')}`;

    // we do not render the footnotes since we switched to the inline footnote syntax ${footnotes.map(({ footnote, children }) => `[^${footnote.attrs.id}]: ${children.join('')}`).join('\n')}
    // until we need multi-paragraph footnotes.
}

/** Renders a document to markdown */
export const renderMarkdown = async (data: { document, template, tables, references }, options?: { absoluteUrls?: boolean, format?: 'md', citationStyle?: string; lof?: boolean; lot?: boolean; authorsInMd?: boolean; }): Promise<File> => {
    options = {
        // set defaults
        absoluteUrls: false,
        authorsInMd: options?.authorsInMd === true,
        lof: false,
        lot: false,
        format: 'md',
        citationStyle: data.template?.citationStyle || options?.citationStyle || 'apa',
        ...(options || {})
    };

    const result = await createPandocMarkdownFile(data, options as { absoluteUrls: boolean, format: 'md', citationStyle: string; lot: boolean; lof: boolean; authorsInMd: boolean; });
    const children = [
        {
            name: 'manuscript.' + options.format,
            mediaType: 'text/markdown',
            content: {
                blob: result
            }
        },
        /*  {
                name: options.citationStyle + '.csl',
                content: {
                    url: 'https://www.zotero.org/styles/' + options.citationStyle
                }
        }, */
        options.absoluteUrls ? null : {
            name: 'assets',
            isDirectory: true,
            children: data.document.files.map(file => ({
                name: file.id + extname(file.url),
                isDirectory: false,
                mediaType: `image/${extname(file.url).replace('.', '').replace('jpg', 'jpeg')}`,
                content: {
                    url: file.url
                }
            }))
        }
    ];
    if (data.references?.bibliography?.length > 0) {
        children.push({
            name: 'references.bib',
            mediaType: 'text/plain',
            content: {
                blob: data.references.bibliography.map(({ renderedString }) => renderedString.replace('<div class="csl-entry">', '').replace('</div>', '').trim()).join('\n\n')
            }
        });
    }
    return {
        children: children.filter(c => c != null)
    }
}
