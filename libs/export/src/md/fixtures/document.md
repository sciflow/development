---
title: Manuscript title
lang: en
author:
  - Frederik Eichler:
      correspondence: 'no'
      deceased: 'no'
      equal_contributor: 'no'
      email: frederik.eichler@sciflow.net
      institute:
        - fegacfief
        - jcjiiehici
institute:
  - id: fegacfief
    name: SciFlow GmbH
    address: Altensteinstrasse Berlin Germany
  - id: jcjiiehici
    name: University of Paderborn Computer Science
    address: Fürstenallee 5 Paderborn Germany
link-citations: true
secHeaderTemplate: $$secHeaderPrefix[n]$$$$i$$. $$t$$
secHeaderPrefix:
  - '&#32;'
  - '&#32;'
  - ''
mathjax: true
math: true
sectionsDepth: 4
numberSections: true
secLabels: arabic
suppress-bibliography: false
csl: apa
figureTitle: Figure
figureTemplate: '**$$figureTitle$$ $$i$$$$titleDelim$$** $$t$$'
tableTemplate: '**$$tableTitle$$ $$i$$$$titleDelim$$** $$t$$'
tableTitle: Table
figPrefix: fig.
eqnPrefix: eq.
tblPrefix: tbl.
lof: false
lot: false
chapters: true
references: []
...

::: {id=cGFydDp5djBwZDQ= numbering=none type=abstract schema=chapter}

# <span>Abstract</span> {#sec:o0n1mgw .unnumbered epub:type=abstract}

<span>I'm baby asymmetrical tofu leggings schlitz single-origin coffee activated charcoal church-key chambray. Fashion axe blue bottle literally, trust fund irony try-hard sartorial palo santo. Activated charcoal photo booth paleo sustainable ennui, asymmetrical lo-fi pop-up swag neutra tumeric distillery DIY mixtape. Knausgaard mustache cray four loko. Mustache vape plaid, photo booth mumblecore meggings neutra pinterest XOXO hammock. Godard kogi everyday carry aesthetic. Etsy venmo chicharrones aesthetic.</span>


:::

::: {id=cGFydDphcHJ0Y3ZoODNx numbering=numeric placement=body type=chapter schema=chapter}

# <span>Introduction</span> {#sec:lv8qfg1thbq .unnumbered}

<span>Locavore chillwave ennui jianbing, bicycle rights mustache mixtape tote bag narwhal iceland seitan sartorial retro. Small batch kinfolk prism, cold-pressed vexillologist tumeric mumblecore raclette pop-up hoodie drinking vinegar before they sold out kale chips. Quinoa hot chicken wolf DIY roof party. VHS thundercats chicharrones intelligentsia aesthetic cross-ref to </span>[@sec:o0n1mgw]<span>. Raclette waistcoat drinking vinegar chillwave citation of </span>[@WittenDataTechniques]<span>.</span>

![<span>A figure image</span>](assets/bxwgw9j2mdv.png){#fig:bxwgw9j2mdv}

<span>A figure caption</span>



<span>Tilde street art</span><span>^2^</span><span> photo booth</span><span>~down~</span><span> flexitarian, knausgaard tousled glossier intelligentsia organic semiotics cloud bread franzen sriracha. Tousled hell of williamsburg letterpress, roof party try-hard shaman. Authentic direct trade sustainable, deep v affogato </span>[dreamcatcher](https://www.sciflow.net)<span> subway tile next level mixtape photo booth quinoa franzen. Af tattooed fingerstache, lo-fi master cleanse green juice craft beer leggings four dollar toast sriracha health goth try-hard. Four dollar toast banjo retro pok pok woke put a bird on it cray VHS. Lo-fi vegan subway tile, tattooed poke VHS activated charcoal.</span>

$\alpha + \beta$

<span>A list</span>

- <span>One (unordered)</span>
    - <span>Nested in one</span>
- <span>Two</span>

<span>Ordered:</span>

1. <span>Item one</span>
2. <span>Item two (ordered)</span>

<span>Some text.</span>

> <span>A blockquote</span>



:::

