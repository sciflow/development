import { assert, expect } from 'chai';

const data = require('./fixtures/document.json');
const tables = require('./fixtures/tables.json');
const template = require('./fixtures/template.json');

import { renderMarkdown } from './index';
import { readFileSync } from 'fs';
import { join } from 'path';

const testData = () => {
    const locale = 'en-US';
    const citationStyle = 'apa';
    const references = {
        bibliography: {
            id: 'citation-1',
            renderedString: 'RENDERED BIBLIOGRAPHY ITEM'
        },
        renderer: {}
    };

    return {
        document: data.document, references, tables, template
    }
}

describe('Chapter (part)', async () => {
    const data = testData();
    it('renders completely', async () => {
        const result = (await renderMarkdown(data)).children?.find(v => v.name === 'manuscript.md')?.content?.blob;
        const expected = readFileSync(join(__dirname, './fixtures/document.md'), 'utf-8');

        expect(result).not.to.be.empty;

        const resultLines = result!.split('\n');
        const expectedLines = expected?.split('\n');

        for (let i = 0;i<expectedLines.length - 1;i++) {
            expect(resultLines[i]).to.equal(expectedLines[i], 'Failed to compare line ' + i + 1);
        }

    });
});

/* 
describe('Author list is generated', async () => {
    const output = await renderer(data);
    const manuscript = output.children.find(c => c.name === 'manuscript.md');
    console.log(manuscript.content);
}); */

/* describe("Lists are generated correctly", async () => {
    const document = readFileSync(join(__dirname, './fixtures/list.json'), 'utf-8');
    const result = await renderNode(JSON.parse(document));
    console.log('RESULT\n', result, '\n\nEXPECTED\n', expected)
    expect(result).to.equal(expected);
}); */