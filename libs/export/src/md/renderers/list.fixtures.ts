export const bulletListNode = {
    "type": "bullet_list",
    "content": [
        {
            "type": "list_item",
            "content": [
                {
                    "type": "paragraph",
                    "attrs": {
                        "id": "ewwfhkzziryk",
                        "text-align": null
                    },
                    "content": [
                        {
                            "type": "text",
                            "text": "One (unordered)"
                        }
                    ]
                },
                {
                    "type": "bullet_list",
                    "content": [
                        {
                            "type": "list_item",
                            "content": [
                                {
                                    "type": "paragraph",
                                    "attrs": {
                                        "id": "b8jrz7tzcugt",
                                        "text-align": null
                                    },
                                    "content": [
                                        {
                                            "type": "text",
                                            "text": "Nested in one"
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "type": "list_item",
            "content": [
                {
                    "type": "paragraph",
                    "attrs": {
                        "id": "sm4s7ahsm6dr",
                        "text-align": null
                    },
                    "content": [
                        {
                            "type": "text",
                            "text": "Two"
                        }
                    ]
                }
            ]
        }
    ]
};

export const orderedListNode =   {
    "type": "ordered_list",
    "attrs": {
      "order": 1
    },
    "content": [
      {
        "type": "list_item",
        "content": [
          {
            "type": "paragraph",
            "attrs": {
              "id": "krjqnya1tzlb",
              "text-align": null
            },
            "content": [
              {
                "type": "text",
                "text": "Item one"
              }
            ]
          }
        ]
      },
      {
        "type": "list_item",
        "content": [
          {
            "type": "paragraph",
            "attrs": {
              "id": "fsiap0md6dik",
              "text-align": null
            },
            "content": [
              {
                "type": "text",
                "text": "Item two (ordered)"
              }
            ]
          }
        ]
      }
    ]
  };