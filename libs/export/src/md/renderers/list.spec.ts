import { expect } from 'chai';

import listRenderers from './list';
import { bulletListNode, orderedListNode } from './list.fixtures';
import defaultRenderers from './defaults';
import { stripIndents } from 'common-tags';
import { RenderingEngine } from '../../renderers';

describe("Lists", async () => {
    const renderer = new RenderingEngine<string, any>({ ...defaultRenderers, ...listRenderers }, {});
    it('should be numbered correctly (ordered list)', async () => {
        const result = await renderer.render(orderedListNode);

        expect(result).to.equal('1. <span>Item one</span>\n2. <span>Item two (ordered)</span>\n');
    });

    it('should nest', async () => {
        const result = await renderer.render(bulletListNode);

        expect(result).to.equal('- <span>One (unordered)</span>\n    - <span>Nested in one</span>\n- <span>Two</span>\n');
    })
});