// this import is needed for TSX to work (look into tsconfig.json for configuration)
import { TSX } from '../../TSX';
import { NodeRenderer } from "../../node-renderer";
import { SFNodeType, DocumentNode } from '@sciflow/schema';

class TableRenderer extends NodeRenderer<string, any> {
    async render(node: DocumentNode<SFNodeType.table>, parent: DocumentNode<SFNodeType.figure>) {
        const colcount = node.content && node.content.length > 0 && node.content?.[0].content && node.content?.[0].content.length || 0;

        const indexOfFirstRow = node.content?.findIndex(row => row.content?.some(n => n.type !== 'table_header'));
        const headers = node.content?.slice(0, indexOfFirstRow);
        const rows = node.content?.slice(indexOfFirstRow, node.content.length);
        const headersRendered = await this.engine.renderContent({ ...node, content: headers });
        const rowsRendered = await this.engine.renderContent({ ...node, content: rows });
        
        const captionWrapper = parent.content?.find(node => node?.type === 'caption');
        const caption = captionWrapper?.content && captionWrapper.content[0];
        const notes = captionWrapper?.content && captionWrapper?.content.slice(1);
        const captionText = caption ? (await this.engine.renderContent(caption)).join('') : undefined;

        const table = <table id={'tbl:' + parent?.attrs?.id}>
            {captionText ? <caption>{captionText}</caption> : ''}
            {headers && headers.length > 0 ? <thead>
                {...headersRendered}
            </thead> : ''}
            <tbody>
                {...rowsRendered}
            </tbody>
        </table> as unknown as HTMLTableElement;

        return table.outerHTML;
    }
}

class TableCellRenderer extends NodeRenderer<any, any> {
    async render(node: DocumentNode<SFNodeType.table_cell | SFNodeType.table_header>) {

        const children = await this.engine.renderContent(node);
        const align = node.content?.find(({ attrs }) => attrs && attrs['text-align'])?.attrs['text-align'];
        let htmlEl = <td>{...children}</td> as unknown as HTMLTableCellElement;
        if (node.type === SFNodeType.table_header) {
            htmlEl = <th><strong>{...children}</strong></th> as unknown as HTMLTableCellElement;
            htmlEl.setAttribute('custom-style', 'Table Heading');
        }
        if (align) {
            htmlEl.setAttribute('style', `text-align: ${align}`);
        }

        if (node.attrs && node.attrs.colwidth) {
            htmlEl.setAttribute('width', node.attrs.colwidth);
        }

        if (node.attrs && node.attrs.colspan) {
            htmlEl.setAttribute('colspan', node.attrs.colspan);
        }

        if (node.attrs && node.attrs.rowspan) {
            htmlEl.setAttribute('rowspan', node.attrs.rowspan);
        }

        return htmlEl;
    }
}

class TableRowRenderer extends NodeRenderer<any, any> {
    async render(node: DocumentNode<SFNodeType.table_row>, _parent?: DocumentNode<any>) {
        const children = await this.engine.renderContent(node);

        return <tr>
            {...children}
        </tr>;
    }
}

export default {
    [SFNodeType.table]: TableRenderer,
    [SFNodeType.table_row]: TableRowRenderer,
    [SFNodeType.table_cell]: TableCellRenderer,
    [SFNodeType.table_header]: TableCellRenderer
};
