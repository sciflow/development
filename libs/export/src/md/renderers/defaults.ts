import { NodeRenderer } from "../../node-renderer";
import { DocumentNode, SFNodeType } from "../../../../schema/src";

/**
 * Root document node.
 */
class DocRenderer extends NodeRenderer<string, any> {
    async render(node: DocumentNode<SFNodeType.document>, parent) {
        const children = await this.engine.renderContent(node);
        return children.join('\n');
    }
}

class ParagraphRenderer extends NodeRenderer<string, any> {
    async render(node: DocumentNode<SFNodeType.paragraph>, parent) {
        let breaks = '';

        // add a linebreak if not in tables/lists or similar
        if ([SFNodeType.list_item, SFNodeType.blockquote, SFNodeType.table_cell, SFNodeType.table_header].indexOf(parent?.type) === -1) {
            breaks = '\n';
        }

        const children = await this.engine.renderContent(node);
        // TODO render alignment
        return `${children.join('')}${breaks}`;
    }
}

class TextRenderer extends NodeRenderer<any, any> {
    async render(node: DocumentNode<SFNodeType.text>) {
        const hyperlink = node.marks?.find(m => m?.type === 'anchor');
        let text = node.text;
        if (hyperlink) {
            return `[${node.text}](${hyperlink.attrs.href})`;
        }

        if (text == ' ') { return text; }
        if (!text) { return ''; }
        const isBold = node.marks?.some(mark => mark.type === 'strong');
        const isItalic = node.marks?.some(mark => mark.type === 'em');
        const isSub = node.marks?.some(mark => mark.type === 'sub');
        const isSup = node.marks?.some(mark => mark.type === 'sup');
        const leadingSpace = text?.startsWith(' ') ? ' ' : '';
        const trailingSpace = text?.endsWith(' ') ? ' ' : '';

        text = text.trim();

        if (isBold && isItalic) {
            text = `***${text}***`;
        } else if (isBold) {
            text = `**${text}**`;
        } else if (isItalic) {
            text = `*${text}*`;
        }

        if (isSub) { text = `~${text}~`; }

        if (isSup) { text = `^${text}^`; }

        return `<span>${leadingSpace}${text}${trailingSpace}</span>`;
    }
}

const getEPubType = (role?: string) => {
    switch (role) {
        case 'abstract':
            return 'abstract';
        case 'keywords':
            return 'keywords';
        case 'imprint':
            return 'imprint';
        case 'appendix':
            return 'appendix';
        case 'bibliography':
            return 'bibliography';
        case 'dedication':
            return 'dedication';
        case 'acknowledgments':
            return 'acknowledgments';
        default:
            return undefined;
    }
}

class HeadingRenderer extends NodeRenderer<any, any> {
    async render(node: DocumentNode<SFNodeType.heading | 'heading2'>, parent: DocumentNode<SFNodeType.document>) {
        const children = await this.engine.renderContent(node);
        const level = node.attrs.level || 1;

        const heading = this.engine.data.tables.headings.find((heading) => heading.id === node.attrs.id);
        // undefined headings may happen for special chapters like cover pages where the title is not part of the table of contents
        const headingText = children.join('');
        let options = [`#sec:${node.attrs.id}`];

        // check options
        const numbering = heading?.numbering?.type || heading?.attrs?.numbering || 'decimal';
        if (numbering !== 'decimal') {
            // TODO skiptoc should be .unlisted
            options.push('.unnumbered');
        }

        if (heading && (level === 1 || level === '1')) {

            const epubTypeFromRole = getEPubType(parent?.attrs?.role);
            const epubTypeFromType = getEPubType(parent?.attrs?.type);
            if (epubTypeFromRole || epubTypeFromType) {
                options.push('epub:type=' + (epubTypeFromRole || epubTypeFromType));
            }

            // TODO can we map our template roles to epub:types?
            // {epub:type=copyright-page .unlisted .unnumbered }
            if (headingText === 'Keywords') {
                options.push('custom-style="Keywords"')
                options.push('.keywords')
            }
        }

        return `\n${'#'.repeat(level)} ${headingText} {${options.join(' ')}}\n`;
    }
}

export default {
    [SFNodeType.document]: DocRenderer,
    [SFNodeType.paragraph]: ParagraphRenderer,
    [SFNodeType.text]: TextRenderer,
    'heading2': HeadingRenderer,
    [SFNodeType.heading]: HeadingRenderer,
}
