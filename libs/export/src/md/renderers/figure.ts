
import { extname } from "path";
import { NodeRenderer } from "../../node-renderer";
import { DocumentNode, SFNodeType } from "../../../../schema/src";

class ImageRenderer extends NodeRenderer<string, any> {
    async render(node: DocumentNode<SFNodeType.figure>) {
        const file = this.engine.data.document.files.find(f => f.id === node.attrs.id || f.fileId === node.attrs.id);
        if (file?.url || node.attrs.src?.length > 0) {
            return `![${node.attrs.alt?.length > 0 ? node.attrs.alt : ''}](${file?.url || node.attrs.src})`;
        }

        return '';
    }
}

class FigureRenderer extends NodeRenderer<string, any> {
    async render(node: DocumentNode<SFNodeType.figure>) {

        const file = this.engine.data.document.files.find(f => f.id === node.attrs.id || f.fileId === node.attrs.id);
        const captionWrapper = node.content?.find(node => node?.type === 'caption');
        const caption = captionWrapper ? captionWrapper.content && captionWrapper.content[0] : null;
        const notes = captionWrapper?.content ? (captionWrapper.content && captionWrapper.content.slice(1)) : [];
        const captionText = caption ? (await this.engine.renderContent(caption)).join('') : null;

        let figure = '';
        switch (node.attrs?.type) {
            case 'table':
                if (node.attrs.src?.length > 0) {
                    // handle like a figure -> fall through
                } else {
                    const tableNode = node?.content?.find(node => node.type === 'table');
                    if (tableNode) {
                        const tableHTML = await this.engine.render(tableNode, node);
                        figure = `\n\`\`\` {=html}\n${tableHTML}\n\`\`\``;
                    }
                    break;
                }
            case 'figure':
            default:
                const absoluteUrls = this.engine.data.options?.absoluteUrls === true;
                // TODO: make this configurable (relative vs. absolute paths)
                let refLabel = 'fig';
                if (node.attrs.type === 'table') {
                    // image table referenced like a figure #506 @see https://github.com/lierdakil/pandoc-crossref/issues/336
                    refLabel = 'fig'; // change this to tbl when the bug is resolved
                }
                if (file) {
                    const ext = extname(file?.url).replace('.', '').replace('jpg', 'jpeg');
                    if (absoluteUrls) {
                        figure = `![${captionText}](${file.url}){#${refLabel}:${node.attrs.id}}`;
                    } else {
                        figure = `![${captionText}](assets/${file.id}.${ext}){#${refLabel}:${node.attrs.id}}`;
                    }
                } else {
                    this.engine.log('warn', 'No image data for figure', { title: captionText, id: node.attrs.id, type: node.attrs.type });
                    if (node.attrs?.src?.indexOf('data:image') === 0) {
                        // fall back to the low res data image
                        figure = `![${captionText}](${node.attrs?.src}){#${refLabel}:${node.attrs.id}}`;
                    } else {
                        figure = `![${captionText}](){#${refLabel}:${node.attrs.id}}`
                    }
                }
        }

        if (notes.length === 0) {
            return figure + '\n';
        }

        return `${figure}\n\n${(await Promise.all(notes.map(note => this.engine.render(note)))).join('\n')}`
    }
}

class LabelRenderer extends NodeRenderer<string, any> {
    async render(node: DocumentNode<SFNodeType.label>) {
        debugger;
        const children = await this.engine.renderContent(node);
        return children.join('');
    }
}


class CaptionRenderer extends NodeRenderer<string, any> {
    async render(node: DocumentNode<SFNodeType.caption>) {
        debugger;
        const children = await this.engine.renderContent(node);
        return children.join('');
    }
}

export default {
    [SFNodeType.figure]: FigureRenderer,
    [SFNodeType.caption]: CaptionRenderer,
    [SFNodeType.label]: LabelRenderer,
    [SFNodeType.image]: ImageRenderer
};
