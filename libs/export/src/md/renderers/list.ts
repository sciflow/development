import { NodeRenderer } from "../../node-renderer";
import { SFNodeType, DocumentNode } from '@sciflow/schema';

const indent = '    ';

class ListRenderer extends NodeRenderer<string, any>{
    public async render(node: DocumentNode<SFNodeType.ordered_list | SFNodeType.list_item>, parent?: DocumentNode<any>) {
        const children = await this.engine.renderContent(node);
        if (parent?.type === SFNodeType.list_item) {
            // indent in a new line
            return `\n${indent}${children.join(`\n${indent}`)}`;
        }
 
        return `${children.join('\n')}\n`;
    }
}

class ListItemRenderer extends NodeRenderer<any, any> {
    public async render(node: DocumentNode<SFNodeType.list_item>, parent?: DocumentNode<any>) {
        const children = await this.engine.renderContent(node);
        if (parent?.type === SFNodeType.ordered_list) {
            if (!parent.content) { return -1; }
            const index = parent.content.indexOf(node);
            return `${index + 1}. ${children.join('')}`;
        }

        return `- ${children.join('')}`;
    }
}

export default { 
    [SFNodeType.list_item]: ListItemRenderer, 
    [SFNodeType.ordered_list]: ListRenderer, 
    [SFNodeType.bullet_list]: ListRenderer
};
