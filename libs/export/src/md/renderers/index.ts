import { stripIndent } from 'common-tags';

import defaultRenderers from './defaults';
import listRenderers from './list';
import htmlTableRenderers from './table';
import figureRenderers from './figure';
import { NodeRenderer } from '../../node-renderer';
import { DocumentNode, SFNodeType } from '../../../../schema/src';
import { SourceField } from '../../../../cite/src';
import { RenderContent } from '../elements';

export default {
    ...defaultRenderers,
    [SFNodeType.code]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<'text'>) {
            const children = await this.engine.renderContent(node);
            return stripIndent`
            \`\`\`${node.attrs?.language ? node.attrs.language : ''}\n${children.map(c => c + '\n')}\n\`\`\``;
        }
    },
    [SFNodeType.math]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<'text'>) {
            let tex = node.attrs?.tex || '';
            tex = tex.replace('$', '\\$').trim();
            if (node.attrs?.style === 'block') {
                return `\n$$ ${tex} $$ {#eq:${node.attrs.id}}\n\n`;
            }

            return `$${tex}$`;
        }
    },
    [SFNodeType.placeholder]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<'text'>) {
            return ``;
        }
    },
    [SFNodeType.poetry]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<'text'>) {
            const children = await this.engine.renderContent(node);
            return stripIndent`
            ::: {custom-style="Poetry"}
            ${children.map(c => '> ' + c)}
            :::
            `;
        }
    },
    [SFNodeType.footnote]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<'text'>) {
            const children = await this.engine.renderContent(node);
            return `^[${children.join('')}]`;
        }
    },
    [SFNodeType.citation]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<'text'>) {
            const citation = SourceField.fromString(node.attrs.source);

            let useBrackets = true;
            let suppressYear = false;

            try {

                const renderedCitations = citation.citationItems.map(item => {
                    let suppressAuthor = item['suppress-author'] === true || (item['suppress-author'] as any) === 1;
                    let authorOnly = item['author-only'] === true || (item['author-only'] as any) === 1;
                    const combined = authorOnly && suppressAuthor;

                    if (authorOnly) {
                        // Name etc al.
                        suppressYear = true;
                    }

                    if (combined) {
                        // Name etc al. (2016)
                        useBrackets = false;
                        suppressAuthor = false;
                    }

                    const parts = [item?.prefix, `${suppressAuthor ? '-' : ''}@${item.id}`];
                    if (item?.locator) {
                        parts.push(`{ ${item?.label || 'page'} ${item.locator} }`);
                    }

                    return parts.filter(n => n != null).join(' ');
                }).join(';');

                if (useBrackets) {
                    if (suppressYear) {
                        return `[[${renderedCitations}]]{.authoronly}`;
                    }
                    return `[${renderedCitations}]`
                }
                return renderedCitations;
            } catch (e) {
                console.error(e);
            }

            return 'CITATION_ERROR';
        }
    },
    [SFNodeType.quote]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<'text'>) {
            const children = await this.engine.renderContent(node);
            return stripIndent`
            ${children.map(c => '> ' + c)}
            `;
        }
    },
    [SFNodeType.hardBreak]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<'text'>) {
            return '\n';
        }
    },
    [SFNodeType.blockquote]: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<'text'>) {
            const children = await this.engine.renderContent(node);
            return `${children.map(c => '> ' + c)}`;
        }
    },
    [SFNodeType.note]: class extends NodeRenderer<string, any> {
        async render() {
            return '';
        }
    },
    ...listRenderers,
    ...htmlTableRenderers,
    ...figureRenderers,
    link: class extends NodeRenderer<string, any> {
        async render(node: DocumentNode<'table'>) {

            const linkId = node.attrs && node.attrs.href && node.attrs.href.replace('#', '');
            const children = await this.engine.renderContent(node);

            const figure = this.engine.data.tables.figures.find(figure => figure.id === linkId);
            const table = this.engine.data.tables.tables.find(table => table.id === linkId);
            const equation = this.engine.data.tables.equations.find(equation => equation.id === linkId);
            const section = this.engine.data.tables.headings.find(heading => heading.id === linkId);

            const figures = this.engine.data.tables.figures.filter(figure => figure.title && figure.title.length > 0);
            const tables = this.engine.data.tables.tables.filter(table => table.title && table.title.length > 0);
            const tableNumber = tables.findIndex(table => table.id === linkId);

            let prefix = '';
            if (figure) {
                prefix = 'fig:';
            } else if (table) {
                if (table.attrs.src?.length > 0) {
                    // image table referenced like a figure #506 @see https://github.com/lierdakil/pandoc-crossref/issues/336
                    prefix = 'fig:';
                } else {
                    prefix = 'tbl:';
                }
            } else if (equation) {
                prefix = 'eq:'
            } else if (section) {
                prefix = 'sec:'
            }

            if (node.attrs && node.attrs?.type === 'xref') {
                return `[@${prefix}${linkId}]`;
            } else {
                return `[${node.attrs.href}](${children.join('\n')})`;
            }
        }
    },
    hyperlink: RenderContent,
    subtitle: RenderContent,
    pageBreak: class extends NodeRenderer<string, any> {
        async render() {
            return '\n\\newpage\n';
        }
    },
    [SFNodeType.horizontalRule]: class extends NodeRenderer<string, any> {
        async render() {
            return '\n_________________\n';
        }
    },
};