import { it, describe } from 'mocha';
import { expect } from 'chai';
import { join } from 'path';
import { readFileSync } from 'fs';
import { parsePandocAST } from '../../import/src/public-api';
import { convertToSnapshot, createSectionHierarchy, wrapTOC, wrapUrlLinks } from './helpers';
import { generateListings } from './html';
import { SFNodeType } from '@sciflow/schema';
import { JSDOM } from 'jsdom';

const numberedDocument = JSON.parse(readFileSync(join(__dirname, './fixtures/numbered-doc.json'), 'utf-8'));
const structuredDocumentPandocJSON = JSON.parse(readFileSync(join(__dirname, './fixtures/structured-document.json'), 'utf-8'));

describe('Helpers', async () => {
    it('Split document into sections and sub-sections', async () => {
        const { doc } = await parsePandocAST(structuredDocumentPandocJSON);
        if (!doc) { throw new Error('Doc cannot be empty'); }
        const jsonObject = doc?.toJSON();
        const sections = createSectionHierarchy(jsonObject.content.filter(t => t.type !== 'header'));
        expect(sections.length).to.equal(3);
        
        const methodsSection = sections.find(section => section.content?.[0]?.content?.[0]?.text === 'Methods');
        expect(methodsSection).to.exist;
        expect(methodsSection?.content?.[2]?.attrs?.level).to.equal(2);
        expect(methodsSection?.content?.[2]?.content?.[0]?.attrs?.level).to.equal(3);
    });
    it('Wraps the table of contents', async () => {
        const { doc } = await parsePandocAST(structuredDocumentPandocJSON);
        const listings = await generateListings(doc?.toJSON());
        const toc = wrapTOC(listings.toc.headings);
        expect(toc.length).to.equal(3);
        const methods = toc.find(s => s.title === 'Methods');
        expect(methods.content.length).to.equal(2);
        expect(methods.content?.[0]?.content[0]?.level).to.equal(3);
    });
    it('Removes numbering from headings and moves it into parts', async () => {
        const snapshot = convertToSnapshot(numberedDocument, {});
        
        const parts = snapshot.parts.filter(p => p.type !== 'title');
        const decimalPart = parts[0];
        expect(decimalPart.options.numbering).to.equal('decimal');
        expect(decimalPart.document?.content?.[0].type).to.equal(SFNodeType.heading);
        //  we want the heading not to retain the numbering information since it's in the part
        expect(decimalPart.document?.content?.[0].attrs?.numbering).to.not.exist;
        
        const unnumberedPart = parts[1];
        expect(unnumberedPart.options.numbering).to.be.empty;
        expect(unnumberedPart.document?.content?.[0].type).to.equal(SFNodeType.heading);
        expect(unnumberedPart.document?.content?.[0].attrs?.numbering).to.not.exist;
        
        const upperAlphaPart = parts[2];
        expect(upperAlphaPart.options.numbering).to.equal('upper-alpha');
        expect(upperAlphaPart.document?.content?.[0].type).to.equal(SFNodeType.heading);
        expect(upperAlphaPart.document?.content?.[0].attrs?.numbering).to.not.exist;
    });

    describe('wrapUrlLinks', () => {
        let dom: JSDOM;

        beforeEach(() => {
            dom = new JSDOM('<!DOCTYPE html><div id="test"></div>');
            global.document = dom.window.document;
        });

        it('should wrap a https URL in anchor tags with security attributes', () => {
            const element = document.createElement('div');
            element.innerHTML = 'some text https://example.com some other text';

            const result = wrapUrlLinks(element);

            expect(result?.innerHTML).to.equal(
            'some text <a href="https://example.com" target="_blank" rel="noopener noreferrer">https://example.com</a> some other text',
            );
        });

        it('should wrap a http URL in anchor tags with security attributes', () => {
          const element = document.createElement('div');
          element.innerHTML = 'some text http://example.com some other text';

          const result = wrapUrlLinks(element);

          expect(result?.innerHTML).to.equal(
            'some text <a href="http://example.com" target="_blank" rel="noopener noreferrer">http://example.com</a> some other text',
          );
        });

        it('should return null when given a null element', () => {
            const result = wrapUrlLinks(null);
            expect(result).to.be.null;
        });

        it('should leave content without URLs unchanged', () => {
            const element = document.createElement('div');
            element.innerHTML = 'This text has no URLs.';

            const result = wrapUrlLinks(element);

            expect(result?.innerHTML).to.equal('This text has no URLs.');
        });
    });
});
