import { describe, it } from 'mocha';
import { buildTemplateSchema, decamelize, getComponentSchema } from './public-api';
import { load } from 'js-yaml';
import { getValueFromSchema, compileJsonPathFromPatchPath, patchSchema, popFromPath } from './json-schema-patch';
import { dirname, join, resolve } from 'path';
import { expect } from 'chai';
import { EnvironmentConfiguration } from './html/components/environment/environment.schema';
import { existsSync } from 'fs';

describe('Schema', async () => {

    it('Creates variant patches using JSON patch', async function () {

        // since we're loading schemas
        this.timeout(5000);

        const environmentConfiguration = {
            kind: 'Environment',
            spec: {
                allowCustomLabels: false,
                numbering: {
                    resetAt: 1 
                }
            } as EnvironmentConfiguration
        };

        const configurations = [environmentConfiguration];

        // patches use JSON-Patch (RFC6902)
        const variant = load(`
        kind: Variant
        metadata:
            name: "Continuous counting"
            description: |-
              Sets the counting to not reset at a chapter level.
        spec:
          patches:
            - type: default
              patch:
                - op: replace
                  path: /properties/numbering/properties/resetAt/default
                  value: 0
              target:
                kind: Environment
            - type: default
              patch:
                - op: replace
                  path: /properties/allowCustomLabels/default
                  value: true
              target:
                kind: Environment
        `);

        const exportDistDir = dirname(require.resolve('dist/libs/export/package.json'));
        const metaDataSchema = (await buildTemplateSchema(configurations, 'en-US', undefined, exportDistDir))?.schema;

        const result = patchSchema(metaDataSchema, [variant]);
        const resetAt = getValueFromSchema({ kind: 'Environment' } , compileJsonPathFromPatchPath('/properties/numbering/properties/resetAt/default'), result.schema);
        expect(resetAt).to.equal(0);
        const allowCustomLabels = getValueFromSchema({ kind: 'Environment' } , compileJsonPathFromPatchPath('/properties/allowCustomLabels/default'), result.schema);
        expect(allowCustomLabels).to.be.true;
    });

});
