import fs, { appendFileSync, existsSync } from 'fs';
import { basename, dirname, join } from 'path';

const { LOG_LEVEL = 'info' } = process.env;
let { S3_ENDPOINT, S3_IMPORTER_BUCKET = 'demo', S3_ACCESS_KEY, S3_SECRET_KEY, ALLOW_UNAUTHORIZED_TLS = 'false', LOCAL_STORAGE_PATH = '', S3_REGION } = process.env;

import S3 from 'aws-sdk/clients/s3';
import https from 'https';
import winston from 'winston';

const logger = winston.createLogger({ format: winston.format.json(), transports: new winston.transports.Console({ level: LOG_LEVEL }), level: LOG_LEVEL });

logger.info('Configuring S3', { S3_ENDPOINT, S3_IMPORTER_BUCKET, localStorage: LOCAL_STORAGE_PATH !== undefined });

const localFs = LOCAL_STORAGE_PATH?.length > 0;

export class FileError extends Error {
    constructor(message: string, public payload: any) {
        super(message);

        // Set the prototype explicitly.
        Object.setPrototypeOf(this, FileError.prototype);
    }

    toString() {
        return `${this.name}: ${this.message}`;
    }

    toJSON() {
        return { message: this.message, payload: this.payload };
    }
}

const getS3Config = () => {
    if (localFs) {
        debugger;
        throw new Error('S3 not configured');
    }
    let config: any = {
        endpoint: S3_ENDPOINT,
        accessKeyId: S3_ACCESS_KEY,
        secretAccessKey: S3_SECRET_KEY,
        region: S3_REGION,
        s3ForcePathStyle: true,
        signatureVersion: 'v4'
    }

    if (ALLOW_UNAUTHORIZED_TLS === 'true') {
        config.httpOptions = {
            agent: new https.Agent({ rejectUnauthorized: false })
        }
    }

    return config;
}

const ensureBucket = async (bucket = S3_IMPORTER_BUCKET) => {
    if (localFs) { return logger.info('Using local file system', { path: LOCAL_STORAGE_PATH }); }
    const s3Client = new S3(getS3Config());
    return new Promise((resolve, reject) => {
        s3Client.createBucket({ Bucket: bucket }, (err, data) => {
            if (err) {
                logger.error('S3 bucket inspection failed: ' + err.message);
                if (err.statusCode === 400) { return reject(err); }
                s3Client.getBucketVersioning(({ Bucket: bucket }), (err, data) => {
                    if (err) {
                        return reject(err);
                    }

                    logger.info('Versioning', data.Status);
                    resolve(data);
                });
            } else {
                logger.info('S3 bucket inspection succeeded: ' + JSON.stringify(data));
                s3Client.putBucketVersioning({ Bucket: bucket, VersioningConfiguration: { Status: 'Enabled' } }, (err, data) => {
                    if (err) {
                        return reject(err);
                    }

                    logger.info('Enabled versioning', data);
                    resolve(data);
                });
            }
        });
    });
}

const putToS3 = async (s3Path: string, file: File) => {
    if (localFs) {
        try {
            const directory = dirname(join(LOCAL_STORAGE_PATH, s3Path));
            fs.mkdirSync(directory, { recursive: true });
            logger.info('Writing file', { s3Path, directory });

            const arrayBuffer = await file.arrayBuffer();
            appendFileSync(join(LOCAL_STORAGE_PATH, s3Path), Buffer.from(arrayBuffer));
            return {
                ETag: undefined,
                Key: s3Path
            }
        } catch (e: any) {
            logger.error('Could not write file', { message: e.message });
            return;
        }
    }
}

const streamToS3 = async (s3Path: string, stream: fs.ReadStream): Promise<{ ETag?: string; Key: string; VersionId?: string; } | undefined> => {
    if (localFs) {
        try {
            const directory = dirname(join(LOCAL_STORAGE_PATH, s3Path));
            fs.mkdirSync(directory, { recursive: true });
            logger.info('Writing file stream', { s3Path, directory });
            const targetStream = fs.createWriteStream(join(LOCAL_STORAGE_PATH, s3Path));
            stream.pipe(targetStream);
            await new Promise((resolve) => stream.on('end', resolve));
            return {
                ETag: undefined,
                Key: s3Path
            }
        } catch (e: any) {
            logger.error('Could not write file', { message: e.message });
            return;
        }
    }

    try {
        const s3Client = new S3(getS3Config());
        logger.verbose('Streaming file to S3', { Bucket: S3_IMPORTER_BUCKET, Key: s3Path, filename: basename(s3Path) });
        const result = await s3Client.upload({ Bucket: S3_IMPORTER_BUCKET, Key: s3Path, Body: stream }).promise();
        return result;
    } catch (e: any) {
        logger.error({
            bucket: S3_IMPORTER_BUCKET,
            path: s3Path,
            error: e.message,
            statusCode: e.statusCode
        });
        throw new FileError('Could not read file: ' + basename(s3Path), { s3Path });
    }
}

/** Lists objects with the most recently stored version first */
const listObjects = async (s3Path: string): Promise<any[]> => {
    try {
        if (!s3Path || s3Path?.length == 0) { return []; }
        if (localFs) {
            if (!existsSync(join(LOCAL_STORAGE_PATH, s3Path))) { return []; }
            const list = fs.readdirSync(join(LOCAL_STORAGE_PATH, s3Path));
            return list.map((key) => {
                const stats = fs.statSync(join(LOCAL_STORAGE_PATH, s3Path, key));
                return {
                    Key: join(s3Path, key),
                    ETag: undefined,
                    LastModified: new Date(stats.mtimeMs),
                    Size: stats.size,
                    versions: []
                }
            });
        }
    } catch (e: any) {
        console.error('Could not read file ' + s3Path, e.message);
        return [];
    }

    try {
        const s3Client = new S3(getS3Config());
        const result: any = await new Promise((resolve, reject) => {
            s3Client.listObjectsV2({ Bucket: S3_IMPORTER_BUCKET, Prefix: s3Path }, (err, data) => {
                if (err) { reject(err); }
                resolve(data);
            });
        });

        const versions: any = await new Promise((resolve, reject) => {
            s3Client.listObjectVersions({ Bucket: S3_IMPORTER_BUCKET, Prefix: s3Path }, (err, data) => {
                if (err) { reject(err); }
                resolve(data?.Versions);
            });
        });

        // versions are ordered from most recent to older
        return result?.Contents.map(({ Key, LastModified, ETag, Size }) => ({ Key, LastModified, Size, ETag, versions: versions?.filter(v => v.Key === Key) }));
    } catch (e: any) {
        logger.error({
            bucket: S3_IMPORTER_BUCKET,
            path: s3Path,
            error: e.message,
            statusCode: e.statusCode
        });
        throw new Error('Could not read directory ' + e.message);
    }
}

/***
 * Reads a file stored at an s3 path into a writable stream.
 */
const s3ToStream = async (object: { Key: string; VersionId?: string; }, stream: fs.WriteStream) => {
    if (localFs) {
        try {
            if (!existsSync(join(LOCAL_STORAGE_PATH, object.Key))) {
                logger.warn('File missing in local file system', { path: join(LOCAL_STORAGE_PATH, object.Key) });
                throw new Error('File not found in local file system');
            }

            const readStream = fs.createReadStream(join(LOCAL_STORAGE_PATH, object.Key));
            return readStream.pipe(stream);
        } catch (e) {
            throw new Error('Could not read file ' + object.Key);
        }
    }

    return new Promise((resolve, reject) => {
        try {
            const s3Client = new S3(getS3Config());
            const response = s3Client.getObject({ Bucket: S3_IMPORTER_BUCKET, ...object });
            const readStream = response.createReadStream();
            readStream.on('error', (err: any) => {
                logger.error({
                    bucket: S3_IMPORTER_BUCKET,
                    object,
                    code: err.statusCode,
                    error: err.message
                });
                // workaround until we have upgraded the s3 tools
                throw new FileError('Could not read file ' + err.statusCode, {
                    bucket: S3_IMPORTER_BUCKET,
                    object,
                    code: err.statusCode,
                    error: err.message
                });
            });
            resolve(readStream.pipe(stream));
        } catch (e: any) {
            if (e instanceof Error) {
                logger.error({
                    bucket: S3_IMPORTER_BUCKET,
                    object,
                    error: e.message
                });
            }
            reject('Could not read file');
        }
    });
}


/***
 * Reads a file stored at an s3 path into a writable stream.
 */
const s3ToBuffer = async (object: { Key: string; VersionId?: string; }): Promise<Buffer> => {
    if (localFs) {
        if (!existsSync(join(LOCAL_STORAGE_PATH, object.Key))) {
            logger.error('File not found in local file system', { LOCAL_STORAGE_PATH, object })
            throw new Error('File not found in local file system');
        }

        const result = fs.readFileSync(join(LOCAL_STORAGE_PATH, object.Key));
        return result;
    }

    return new Promise((resolve, reject) => {
        try {
            const s3Client = new S3(getS3Config());
            const response = s3Client.getObject({ Bucket: S3_IMPORTER_BUCKET, ...object });
            const stream = response.createReadStream();
            const chunks: Buffer[] = [];
            stream.on('data', chunk => chunks.push(chunk));
            stream.once('end', () => resolve(Buffer.concat(chunks)));
            stream.once('error', reject);
        } catch (e: any) {
            if (e instanceof Error) {
                logger.error({
                    bucket: S3_IMPORTER_BUCKET,
                    object,
                    error: e.message
                });
            }
            reject('Could not read file');
        }
    });
}


export {
    ensureBucket,
    listObjects, putToS3, s3ToBuffer, s3ToStream, streamToS3
};

