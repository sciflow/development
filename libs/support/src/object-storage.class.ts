import { BucketAlreadyExists, BucketAlreadyOwnedByYou, CreateBucketCommand, GetObjectCommand, GetObjectCommandInput, ListObjectsCommand, PutObjectCommand, PutObjectCommandInput, S3Client } from '@aws-sdk/client-s3';
import { getSignedUrl } from '@aws-sdk/s3-request-presigner';

export const createObjectStorage = () => new ObjectStorage();

/**
 * Provides access to files stored in S3 (AWS or compatible object storage)
 * @see https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/client/s3/
 */
export class ObjectStorage {

    private get credentials() {
        if (!process.env.S3_ACCESS_KEY || !process.env.S3_SECRET_KEY) {
            throw new Error('S3 not configured');
        }

        return {
            accessKeyId: process.env.S3_ACCESS_KEY as string,
            secretAccessKey: process.env.S3_SECRET_KEY as string
        };
    }

    private get client() {
        const region = process.env.S3_REGION;
        const endpoint = process.env.S3_ENDPOINT;
        return new S3Client({ region, credentials: this.credentials, endpoint })
    }

    /**
     * Creates a bucket if it does not exist.
     */
    async createBucket(bucket: string): Promise<boolean> {
        const command = new CreateBucketCommand({ Bucket: bucket, ACL: 'private' });
        try {
            await this.client.send(command);
            return true;
        } catch (e) {
            if (e instanceof BucketAlreadyExists || e instanceof BucketAlreadyOwnedByYou) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns a presigned URL 
     */
    async put(key: string, bucket: string, expiresIn = 60): Promise<string> {
        const command = new PutObjectCommand({ Bucket: bucket, Key: key });
        return await getSignedUrl(this.client, command, { expiresIn });
    }

    /**
     * Returns a presigned URL to fetch an object.
    */
    async get(key: string, bucket: string, expiresIn = 3600 * 12): Promise<string> {
        const command = new GetObjectCommand({ Bucket: bucket, Key: key });
        return await getSignedUrl(this.client, command, { expiresIn });
    }

    async fetch(url: string) {
        if (!url) { return null; }
        const response = await fetch(url);
        if (response.status == 404) { return null; }
        if (response.status > 500) { throw new Error('Could not fetch file: ' + response.status); }
        if (response.status === 200) {
            return response;
        }

        return null;
    }

    /**
     * Lists the objects under a prefix.
     */
    async list(bucket: string, prefix: string): Promise<string> {
        const command = new ListObjectsCommand({ Bucket: bucket, Prefix: prefix });
        return await getSignedUrl(this.client, command, { expiresIn: 600 });
    }
}
