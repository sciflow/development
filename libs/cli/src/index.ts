import { existsSync, watchFile, watch as watchDir, readdirSync } from 'fs';
import { debounceTime, Subject, takeUntil } from 'rxjs';
import { generateEPUB } from './transform/epub';
import { sync } from 'glob';

import { generateHTMLZip, generateHTML, generateHTMLPDF } from './transform/html';
import { createLogger, format as winstonFormat, transports } from 'winston';
import { exportToSnapshot } from './snapshot';

import { Command } from 'commander';
import { validateYamlFile, generateValidationReport } from '@sciflow/tdk';
import { resolve } from 'path';
const program = new Command();

const { LOG_LEVEL = 'info' } = process.env;

const logformat = winstonFormat.printf(
  ({ level, message, label, timestamp }) => `${timestamp} [${level}]: ${message}`,
);
const logger = createLogger({
  level: LOG_LEVEL,
  format: winstonFormat.combine(winstonFormat.timestamp(), logformat),
  defaultMeta: { service: 'cli' },
  transports: [new transports.Console({ level: LOG_LEVEL })],
});

const stop$ = new Subject<void>();
let watcher;

process.on('SIGTERM', () => {
  stop$.next();
  logger.warn('Terminating ..');
  watcher?.removeAllListeners();
});

program
  .name('SFO cli')
  .description('CLI for exports and other document operations')
  .version('0.0.1');

program
  .command('transform')
  .description('transforms a document using a template')
  .option('-t, --target <file>', 'The output filename/path')
  .option('-s, --source <file>', 'Location of a SciFLow json snapshot file')
  .option('-f, --format <format>', 'epub, pdf, or html')
  .option('-tpl, --template <slug>', 'The template slug to use')
  .option('-td, --template-dir <path>', 'The path to the template directory')
  .option('-fd, --font-dir <path>', 'Path to where font files are downloaded')
  .option('--log-file <path>', 'Path to a log file location')
  .option('--log-level <level>', 'Log level (defaults to error)')
  .option(
    '-p, --temp-path <path>',
    'An optional path where temporary files are stored (for debugging/testing)',
  )
  .option('-w, --watch', 'Whether to watch for changes in the template or document')
  .option('-f, --force', 'Overwrite existing files')
  .option('--debug', 'Debug mode')
  .action(
    async (
      {
        source,
        target,
        watch,
        force,
        format = 'html',
        templateDir,
        template,
        fontDir,
        tempPath,
        debug,
        logFile,
        logLevel = 'error',
      },
      options,
    ) => {
      const winstonTransports = [new transports.Console({ level: logLevel })];
      if (logFile) {
        new transports.File({ filename: logFile, level: logLevel });
      }

      const transactionLogger = createLogger({
        format: winstonFormat.json(),
        defaultMeta: { service: 'tdk-logger' },
        transports: winstonTransports,
      });

      const transactionId = 'cli-' + (Date.now() - new Date('2024-1-1').getTime());
      const logging = {
        transactionId,
        logger: transactionLogger,
      };

      logger.info('Running CLI ' + transactionId, {
        transactionId,
        source,
        target,
        watch,
        force,
        format,
        templateDir,
        fontDir,
        template,
        debug,
      });
      if (!existsSync(source)) {
        throw new Error('Source file does not exist at ' + source);
      }
      if (existsSync(target) && !force) {
        program.error('Target file already existed, use --force to overwrite.');
      }

      // we use the global path and serve from the hard drive
      const fontUrlPrefix = fontDir;

      if (templateDir && !existsSync(templateDir)) {
        throw new Error('Could not find template dir: ' + templateDir);
      }

      const updates$ = new Subject<string>();
      const generate = async () => {
        try {
          switch (format) {
            case 'html':
              if (target.endsWith('.html')) {
                await generateHTML(source, target, {
                  template,
                  templateDir,
                  fontDir,
                  fontUrlPrefix,
                  logging,
                  debug,
                  inline: true,
                });
              } else {
                await generateHTMLZip(source, target, {
                  template,
                  templateDir,
                  fontDir,
                  fontUrlPrefix,
                  tempPath,
                  logging,
                  debug,
                });
              }
              break;
            case 'princexml-html':
              await generateHTML(source, target, {
                template,
                templateDir,
                fontDir,
                fontUrlPrefix,
                logging,
                debug,
                inline: true,
                runner: 'princexml',
              });
              break;
            case 'epub':
              await generateEPUB(source, target, { template, templateDir, logging, debug });
              break;
            case 'pdf':
              await generateHTMLPDF(source, target, {
                template,
                templateDir,
                fontDir,
                fontUrlPrefix,
                tempPath,
                logging,
                debug,
                runner: 'princexml',
              });
          }
        } catch (e) {
          logger.error('Could not transform ' + format + ': ' + e.message, {
            exitCode: 1,
            code: 'Processing error',
          });
          throw new Error('Execution failed ' + e.message);
        }
      };

      if (watch) {
        watchFile(source, async (_curr, _prev) => {
          updates$.next(source);
        });
        if (templateDir) {
          watchDir(templateDir, { recursive: true }, async (_event, filename) => {
            filename && updates$.next(filename);
          });
        }
        logger.info('Watching files for changes ..');
      }

      updates$.pipe(debounceTime(250), takeUntil(stop$)).subscribe(async (path) => {
        logger.info('Files changed for ' + path + ' ..');
        await generate();
      });

      updates$.next(source);
    },
  );

program
  .command('snapshot')
  .description('Handles SciFlow snapshots')
  .option('-t, --target <file>', 'The output filename/path for the snapshot')
  .option('-s, --source <file>', 'An import file from SFO/OS-APS to transform')
  .option('-w, --watch', 'Whether to watch for changes in the template or document')
  .option('-f, --force', 'Overwrite existing files')
  .action(async ({ source, target }) => {
    console.table({ source, target });
    exportToSnapshot(source, target);
  });

program
  .command('validate')
  .description('Validate a YAML document against its schema.')
  .option('-s, --source <dir>', 'Directory of the source.')
  .option('-c, --componentPath <dir>', 'Directory of the components.')
  .option('-o, --output <dir>', 'The output directory')
  .option('-p, --pattern <pattern>', 'Glob pattern for YAML files')
  .action(async (options: any) => {
    const transactionId = 'cli-' + (Date.now() - new Date('2024-1-1').getTime());

    logger.info('Running CLI ' + transactionId, {
      transactionId,
    });

    const sourceDir = resolve(process.cwd(), options.source || '');
    const componentPath = options.componentPath
      ? [resolve(process.cwd(), options.componentPath)]
      : [];
    const outputDir = resolve(process.cwd(), options.output || 'out');
    const pattern = options.pattern || 'configurations';

    const templatePaths = sync(`${sourceDir}/**/${pattern}.@(yml|yaml)`);
    try {
      const result = await generateValidationReport(templatePaths, componentPath, outputDir);
    } catch (e) {
      logger.error('An error occurred during validation: ' + e.message);
      process.exit(1);
    }
  });

program.parse(process.argv);

if (!process.argv.slice(2).length) {
  program.outputHelp();
}
