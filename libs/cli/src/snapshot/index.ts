import { convertToSnapshot, extractTitle } from "@sciflow/export";
import { SimplifiedManuscriptFile } from "@sciflow/schema";
import { readFileSync, writeFileSync } from "fs";

/** Creates a snapshot of the document that can be read by SciFlow
 * @param source the source file path
 * @param target where to write the result
 * @param assetUrl the url where assets are hostet (e.g. to fetch images). e.g.: http://loccalhost:3000
*/
export const exportToSnapshot = async (source, target, assetUrl = 'http://loccalhost:3000') => {
    const manuscript: SimplifiedManuscriptFile = JSON.parse(readFileSync(source, 'utf-8'));
    const snapshot = convertToSnapshot({
        document: manuscript.document,
        title: extractTitle(manuscript.document),
        stylePaths: [],
        files: manuscript.files || [],
        templateOptions: null,
        citationStyleData: { styleXML: null, localeXML: null } as any,
        authors: manuscript.authors ?? [],
        configuration: [],
        configurations: [],
        references: manuscript.references ?? [],
        metaData: manuscript.metaData ?? {},
        metaDataSchema: {}
    }, {
        assetUrl
    });
    writeFileSync(target, JSON.stringify(snapshot, null, 2));
}
