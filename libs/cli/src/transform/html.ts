#!/usr/bin/env yarn run ts-node

import { DocumentData, createHTMLZip, extractTitle, generateListings } from '@sciflow/export';
import { loadTemplate } from '@sciflow/tdk';
import { copyFileSync, existsSync, mkdirSync, readFileSync, writeFileSync } from 'fs';
import { join } from 'path';
import JSZip from 'jszip';
import { dirSync } from 'tmp';
import Prince from 'prince';
import { createLogger, transports, format } from 'winston';

const templateDir = join(__dirname, 'fixtures', 'templates');
const componentDir = join(__dirname, '..', '..', '..', 'libs', 'export', 'src', 'html');

const logger = createLogger({
    level: 'info',
    format: format.json(),
    defaultMeta: { service: 'cli-export' },
    transports: [
        new transports.Console()
    ],
});

export const getDocumentFromSnapshot = async (documentId: string, snapshot: any): Promise<DocumentData> => {
    let title;
    let subtitle;
    try {
        title = extractTitle(snapshot.title.document);
    } catch (e) {
        console.error(e.message, null, { documentId });
    }

    const tables = await generateListings(snapshot.parts.map(p => ({
        ...p.document,
        attrs: {
            ...p.document.attrs,
            id: p.document.attrs?.id || p.partId || p.id,
            numbering: p.options?.numbering || p.numbering,
            role: p.role,
            schema: p.schema,
            type: p.type
        }
    })));

    const images = snapshot.files.filter(attachment => attachment.type === 'image');

    logger.info('Transformed snapshot for export', { title, parts: snapshot.parts?.length })
    return {
        documentId: snapshot.documentId,
        title,
        subtitle,
        index: snapshot.index,
        titlePart: snapshot.title,
        references: snapshot.references,
        tables,
        parts: snapshot.parts,
        metaData: snapshot.metaData,
        wordCount: snapshot.wordCount,
        images,
        files: snapshot.files,
        authors: snapshot.authors,
        annotations: [] // TODO
    };
}

/**
 * Generates a ZIP file of the HTML.
 */
export const generateHTMLZip = async (source, target?, opts: { templateDir, template, fontDir, fontUrlPrefix, tempPath?, logging?, debug?, inline?, runner?, separateFonts? } = { templateDir, template: 'monograph', fontDir: undefined, fontUrlPrefix: undefined, tempPath: undefined, logging: undefined, debug: false, inline: false, runner: 'html' }) => {

    let tmpPath = opts?.tempPath;
    if (!tmpPath) {
        const dir = dirSync({ unsafeCleanup: true });
        tmpPath = dir.name;
    }

    if (!existsSync(source)) {
        throw new Error('Source file did not exist at' + source);
    }

    let documentSnapshot = JSON.parse(readFileSync(source, 'utf-8'));
    // we're in dist/libs/cli but want to go to dist/libs/export
    const componentPath = join(__dirname, '..', 'export');

    const template = await loadTemplate({ projectId: undefined, templateId: opts.template }, {
        templateDir: opts?.templateDir,
        componentPath,
        fontPath: opts.fontDir,
        fontUrlPrefix: opts.fontUrlPrefix,
        runner: opts.runner
    });

    const parts = documentSnapshot.parts.map(part => ({
        ...part,
        id: part.partId || part.id
    }));
    const references = Object.keys(documentSnapshot.references || {}).map(id => documentSnapshot.references[id]);

    const exportData = {
        documentId: 'test',
        ...documentSnapshot,
        parts,
        title: documentSnapshot.title,
        references
    };

    const document = await createHTMLZip(
        exportData,
        {
            configurations: template.configurations,
            metaData: documentSnapshot.metaData,
            metaDataSchema: template.metaData,
            customTemplateComponents: template.customTemplateComponents,
            customRenderers: template.customRenderers,
            componentPaths: [componentPath],
            stylePaths: template.stylePaths || [],
            assetBasePaths: [], // TODO
            scripts: [],
            logging: opts.logging,
            debug: opts.debug,
            inline: opts.inline,
            runner: opts.runner,
            separateFonts: opts?.separateFonts === true
        });

    if (target) { writeFileSync(target, document); }

    return document;
};

/**
 * Generates HTML output.
 */
export const generateHTML = async (source, target = Date.now() + '.html', opts?: { templateDir, template, fontDir, fontUrlPrefix, additionalConfigurations?, logging?, debug?, inline?, runner?, separateFonts? }) => {
    const file = await generateHTMLZip(source, undefined, opts);
    const zip = new JSZip();
    const content = await zip.loadAsync(file);
    const manuscript = await content.file('manuscript.html')?.async('string');
    if (manuscript) {
        logger.info('Writing manuscript html ', target);
        writeFileSync(target, manuscript);
    }
};

export const generateHTMLPDF = async (source, target = Date.now() + '.html', opts?: { templateDir, template, fontDir, fontUrlPrefix, additionalConfigurations?, tempPath?, logging?, debug?, runner? }) => {

    let tmpPath = opts?.tempPath;
    if (!tmpPath) {
        const dir = dirSync({ unsafeCleanup: true });
        tmpPath = dir.name;
        logger.info('Creating new temporary path', tmpPath);
    } else {
        logger.info('Using provided temporary path', tmpPath);
    }

    const file = await generateHTMLZip(source, undefined, opts);
    const zip = new JSZip();
    const content = await zip.loadAsync(file);

    logger.info('Writing to temp dir ', tmpPath);
    for (let path of Object.keys(content.files)) {
        const file = await content.file(path)?.async('array');
        if (file) {
            logger.info('Writing ' + join(tmpPath, path))
            writeFileSync(join(tmpPath, path), Buffer.from(file));
        } else {
            mkdirSync(join(tmpPath, path), { recursive: true });
        }
    }

    const manuscript = await content.file('manuscript.html')?.async('string');
    if (manuscript) {
        logger.info('Writing manuscript html for prince ', tmpPath);
        writeFileSync(join(tmpPath, 'manuscript.html'), manuscript);
    }

    try {
        const prince = Prince()
        .inputs(join(tmpPath, 'manuscript.html'))
        .output(join(tmpPath, 'manuscript.pdf'));
        
        logger.info('Prince', { binary: prince.config.binary });
    
        await prince.execute();
    } catch (e) {
        logger.error(e);
        (opts?.logging?.logger?.error || logger.error)('PrinceXML failed', { message: e.message });
    }

    if (!existsSync(join(tmpPath, 'manuscript.pdf'))) {
        throw new Error('Could not find pdf file');
    }

    copyFileSync(join(tmpPath, 'manuscript.pdf'), target);
};
