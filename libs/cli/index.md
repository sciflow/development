# CLI

On \*nix systems, the cli is executed through a local bash script ./sfo

To continuously produce an epub from a template and document:

```
./sfo transform \
    -s libs/tdk/src/e2e/fixtures/thesis-mockup-en.snapshot.json \
    -t libs/tdk/src/e2e/test.epub \
    --format=epub \
    --template-dir=libs/tdk/src/e2e/fixtures/templates \
    --template=basic-epub \
    --font-dir=${PWD}/fonts \
    --watch \
    --force
```

To display the epub, you can open a http server in libs/tdk/src/e2e and open epubjs.html.
It will attempt to load the epub at localhost:8080/test.epub

Example for a single HTML file:

```
./sfo transform \
    --source=libs/tdk/src/e2e/fixtures/thesis-mockup-en.snapshot.json \
    --target=libs/tdk/src/e2e/test.html \
    --format=html \
    --template-dir=libs/tdk/src/e2e/fixtures/templates \
    --font-dir=${PWD}/fonts \
    --template=monograph \
    --watch \
    --force
```

Example for a single PDF file:

```
./sfo transform \
    --source=libs/tdk/src/e2e/fixtures/thesis-mockup-en.snapshot.json \
    --target=libs/tdk/src/e2e/test.pdf \
    --format=pdf \
    --template-dir=libs/tdk/src/e2e/fixtures/templates \
    --font-dir=${PWD}/local-fonts \
    --template=monograph \
    --watch \
    --force
```

Example to transform a sfo manuscript into a SciFlow snapshot.

```
./sfo snapshot \
    --source=libs/tdk/src/e2e/fixtures/moby-dick.sfo.snapshot.json \
    --target=libs/tdk/src/e2e/outfiles/moby-dick.snapshot.json \
    --watch \
    --force
```

## Validation

This CLI tool allows you to validate YAML documents against their corresponding JSON schemas.

### Usage

You would need to know the path to your YAML template. Once you have that, you can simply run

```bash
./sfo validate <path-to-your-file>
```

## MacOS (ARM)

You may need to install sharp individually.

```
npm install --cpu=arm64 --os=darwin sharp
```
