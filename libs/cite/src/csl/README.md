# Reference files

The files contained in this folder were created by the [Citation Style Language](http://citationstyles.org/) (CSL) project and are cached here for testing purposes and faster initial rendering in our libraries.

These files are shared under the [Creative Commons Attribution-ShareAlike 3.0 Unported license](http://creativecommons.org/licenses/by-sa/3.0/).

* APA 7th edition as a representative of the in-text author-name citation styles
* Vancouver as a number based in-text citation style
* Chicago Fullnote as (foot)note citation style.

We use the en-US locale for testing.

For more information about the used files and the format please see [CitationStyles.org](http://citationstyles.org/).
