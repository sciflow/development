import { it, describe } from 'mocha';
import { expect } from 'chai';

import { ReferenceRenderer } from './references';
import testReferences from './fixtures/references.json';
import { getDefaults } from './csl';

const { styleXML, localeXML } = getDefaults();

describe('Reference renderer', async () => {
    it('Renders unrelated citations', async () => {
        const renderer = new ReferenceRenderer(styleXML, localeXML, testReferences);
        expect(renderer.renderCitationOutOfContext([
            {
                id: 'test-book',
                prefix: 'see ',
                locator: '22',
                suffix: 'amongst others',
                label: 'page'
            },
        ])).to.equal('(see Samelastname, 2011, p. 22 amongst others)');
    });
    it('Renders two references with the same name differently', async () => {
        const renderer = new ReferenceRenderer(styleXML, localeXML, testReferences);

        renderer.addCitationAtEnd({
            citationID: 'test-book-citation',
            citationItems: [
                {
                    id: 'test-book',
                }
            ],
            properties: {
                noteIndex: 0
            }
        });

        renderer.addCitationAtPos({
            citationID: 'test-book-citation-2',
            citationItems: [
                { id: 'test-book-2' },
            ],
            properties: {
                noteIndex: 0
            }
        }, [['test-book-citation', 0]], []);

        // since both authors are called Samelastname and their first names both start with A we expect a and b
        expect(renderer.getCitation('test-book-citation').renderedCitation).to.equal('(Samelastname, 2011b)');
        expect(renderer.getCitation('test-book-citation-2').renderedCitation).to.equal('(Samelastname, 2011a)');
    });
    it('Renders a bibliography', async () => {
        const renderer = new ReferenceRenderer(styleXML, localeXML, testReferences, { format: 'text' });
        renderer.addCitationAtEnd({
            citationItems: [
                {
                    id: 'test-book',
                }
            ],
            properties: {
                noteIndex: 0
            }
        });
        renderer.addCitationAtEnd({
            citationItems: [
                {
                    id: 'test-book-2',
                }
            ],
            properties: {
                noteIndex: 1
            }
        });
        const { formatting, references } = renderer.getBibliography();
        expect(references.length).to.equal(2);
        expect(references[0]).to.equal('Samelastname, A. (2011a). A test book (Vol. 2).\n');
        expect(references[1]).to.equal('Samelastname, A. (2011b). Test title.\n');
    });
});
