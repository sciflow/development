import { expect } from 'chai';
import { describe, it } from 'mocha';

import { renderAuthorList } from './custom';
import testReferences from './fixtures/authors.json';

describe('Author name renderer', async () => {
    it('Renders author names et al.', async () => {
        expect(renderAuthorList('authorsEtAl', testReferences[0]))
            .to.equal('Richard L. Oliver et al.');
    });
    it('Renders author names et al. (caps)', async () => {
        expect(renderAuthorList('authorsEtAlCaps', testReferences[0]))
            .to.equal('<span style="font-variant:small-caps;">Richard L. Oliver et al.</span>');
    });
    it('Renders author names for file name', async () => {
        expect(renderAuthorList('filename', testReferences[0], true))
            .to.equal('Journal of Retailing-78-12002-Oliver-Rust-Varki-10.1016/s0022-4359(01)00065-2');
    });

});
