import { spawnSync } from "child_process";
import { CSLReference } from "./types";

export const readBibFile = (input: string): CSLReference[] => {
    const { signal, status, stderr, stdout } = spawnSync(
        'pandoc',
        ['--from', 'biblatex', '--to', 'csljson'],
        { input }
    );

    if (signal === 'SIGKILL') {
        throw new Error('Pandoc quit unexpectedly');
    }

    const errors = stderr.toString().split('\n').filter(s => s.length > 0);
    if (errors?.length > 0) {
        console.error('Reference import error', errors);
    }
    
    const logs = stdout.toString().split('\n').filter(s => s.length > 0);
    return JSON.parse(logs.join(''));
};
