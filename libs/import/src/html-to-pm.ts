import { schemas } from '@sciflow/schema';
import { DOMParser } from 'prosemirror-model';
import { JSDOM } from 'jsdom';
const dom = new JSDOM('<!DOCTYPE html>');
const document = dom.window.document;

/***
 * A basic HTML parser for ProseMirror documents.
 * This will be replaced by a parser for Pandoc AST
 */
const parseHTML = async (html: string) => {
    const parser = DOMParser.fromSchema(schemas.manuscript);
    const content = document.createElement('div');
    content.innerHTML = html;
    const doc = parser.parse(content);

    return doc;
};

export {
    parseHTML
};
