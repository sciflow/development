export enum PDNodeType {
    Div,
    Para,
    Header,
    Strong,
    Space,
    LineBreak,
    text = 'Str'
}

export interface PDDocumentNode {
    t: PDNodeType;
    // TODO
    c: string | [];
}

export interface ImportEngine<InputFormat> {
    
}