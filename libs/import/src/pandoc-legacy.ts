import { Mark, Schema, DOMSerializer } from 'prosemirror-model';
import { schemas, SFNodeType } from '@sciflow/schema';
import { JSDOM } from 'jsdom';

const dom = new JSDOM();
const document = dom.window.document;

export class ImportError extends Error {
    constructor(message: string, public metaData?: any) {
        super(message); // 'Error' breaks prototype chain here
        Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain
    }

    toString() {
        return `${this.message}: ${JSON.stringify(this.metaData ?? {})}`
    }

    toJSON() {
        return JSON.stringify({ message: this.message, metaData: this.metaData });
    }
}

export const renderProseMirror = (node, schema = 'chapter'): String => {
    const serialize = DOMSerializer.fromSchema(schemas[schema]);
    const fragment = serialize.serializeFragment(node.content, { document });
    const wrapper = document.createElement('body');
    wrapper.appendChild(fragment);

    return wrapper.innerHTML;
};

/**
 * A simple importer for Pandoc AST
 * @param pandocNodes 
 * @returns the prosemirror tree
 */
export const pandocImport = (pandocAST, schemaName = 'chapter') => {
    const schema: Schema = schemas[schemaName];
    // TODO enable multiple schemata depending on type
    const getNodeType = (type: string) => schema.nodes[type];
    const getMarkType = (type: string) => schema.marks[type];
    const createTextNode = (text: string, marks?: readonly Mark[]) => schema.text(text, marks);

    const render = (nodes: any[] | any, marks: Mark[] = []) => {
        if (Array.isArray(nodes)) {
            const result: any[] = [];
            for (let node of nodes) {
                if (!renderers[node.t]) {
                    return null;
                } else {
                    result.push(renderers[node.t](node, marks));
                }
            }
            return result.filter(v => v != null).flat();
        } else {
            return [renderers[nodes.t](nodes, marks)]
        }
    };

    const renderers: { [pandocTypeName: string]: any } = {
        Str: ({ c }, marks) => createTextNode(c, marks),
        Header: ({ c }) => {
            const classes: string[] = c[1][1];
            const numbering = classes.includes('unnumbered') ? 'none' : 'decimal';
            try {
                return getNodeType(SFNodeType.heading).create({ level: c[0], numbering }, render(c[2]));
            } catch (e) {
                throw new ImportError(e.message, { type: 'Header', classes, numbering });
            }
        },
        LineBreak: () => getNodeType(SFNodeType.hardBreak).create({}),
        Space: () => createTextNode(' '),
        Emph: ({ c }) => render(c, [getMarkType('em').create()]),
        Link: ({ c }) => {
            const [attributes, content, [href, title]] = c;
            const anchorMark = getMarkType('anchor').create({
                href,
                title: title?.length === 0 ? null : title
            });
            // TODO if the target contains a # it should be made a xref and not an anchor mark
            return render(content, [anchorMark]);
        },
        RawBlock: ({ c }) => {
            const [type, command] = c;
            if (type === 'tex') {
                if (command === '\\newpage') {
                    return getNodeType(SFNodeType.pageBreak).create();
                }
            }
        },
        BlockQuote: ({ c }) => getNodeType(SFNodeType.blockquote).create({}, render(c)),
        Strong: ({ c }) => render(c, [getMarkType('strong').create()]),
        Para: ({ c }) => {
            try {
                return getNodeType(SFNodeType.paragraph).create({}, render(c))
            } catch (e) {
                throw new ImportError(e.message, { type: 'Para' });
            }
        },
        Quoted: ({ c }) => {
            try {
                const [type, content] = c;
                return render(content);
            } catch (e) {
                throw new ImportError(e.message, { type: 'Quoted' });
            }
        },
        DoubleQuote: ({ c }) => {
            try {
                const [type, content] = c;
                return render(content);
            } catch (e) {
                throw new ImportError(e.message, { type: 'Quoted' });
            }
        },
        Plain: ({ c }) => {
            try {
                return getNodeType(SFNodeType.paragraph).create({}, render(c))
            } catch (e) {
                throw new ImportError(e.message, { type: 'Plain' });
            }
        },
        OrderedList: ({ c }) => {
            try {
                let listContent: any[] = [];
                //const type = c[0];
                for (let item of c[1]) {
                    listContent.push(getNodeType(SFNodeType.list_item).createChecked({}, render(item)))
                }

                return getNodeType(SFNodeType.ordered_list).createChecked({}, listContent);
            } catch (e) {
                throw new ImportError(e.message, { type: 'OrderedList', c });
            }
        },
        BulletList: ({ c }) => {
            try {
                let listContent: any[] = [];
                for (let item of c) {
                    listContent.push(getNodeType(SFNodeType.list_item).createChecked({}, render(item)))
                }
                
                return getNodeType(SFNodeType.bullet_list).createChecked({}, listContent);
            } catch (e) {
                debugger;
                throw new ImportError(e.message, { type: 'BulletList', c });
            }
        },
        Div: ({ c }) => {
            let [[id, _b, attrList], content] = c;
            // check the heading1 for information as well
            const heading1 = content.find(child => child.t === 'Header' && child.c && child.c[0] === 1);
            if (heading1) {
                for (let className of heading1.c[1][1]) {
                    if (className === 'unnumbered') {
                        attrList = [...attrList.filter(([key]) => key !== 'numbering'), ['numbering', 'none']];
                    }
                }
            }

            const attrs = attrList.reduce((obj, [key, value]) => ({ ...obj, [key.replace('data-', '')]: value }), {});
            // we ignore the refs id since we have our own way of placing a bibliography
            if (id === 'refs') { return null; }
            try {
                const node = getNodeType(SFNodeType.document).createChecked(attrs, render(content));
                return node;
            } catch (e) {
                if (e instanceof ImportError) {
                    throw new ImportError(e.message, e.metaData);
                } else {
                    throw new ImportError(e.message, { type: 'Div', attrs, id });
                }
            }
        }
    }

    const { references, title } = pandocAST.meta;
    let parts: any[] | null = [];
    let errors: any[] = [];
    try {
        parts = render(pandocAST.blocks);
    } catch (e) {
        debugger;
        errors.push({
            message: e.message,
            metaData: e.metaData
        });
    }

    // we get the title either from the document or the meta data
    let titleDoc = parts?.find(p => p.type === 'title') || getNodeType(SFNodeType.document).create({ type: 'title' }, [
        getNodeType(SFNodeType.heading).create({ level: 1 }, title?.c ? render(title.c) : createTextNode('Document title'))
    ]);

    // make sure the title comes first since that is required in the editor
    parts = [titleDoc || undefined, ...(parts?.filter(p => p.attrs.type !== 'title') || [])];

    // if a part does not begin with a heading 1, set it's schema to 'free'

    try {
        return {
            parts,
            references,
            errors
        };
    } catch (e) {
        debugger;
        console.error(e);
    }
}
