import { SFMarkType, SFNodeType } from '@sciflow/schema';
import { Fragment, Mark, MarkType } from 'prosemirror-model';
import { ImportError, Label, PandocNode, PandocRenderer, ProseMirrorNode, render, renderContent } from '.';
import * as Pandoc from './pandoc-types';
import { DELETED_NODE } from './pandoc-types';
import { JaroWinklerDistance } from 'natural';
import { getKnownHeadings, getKnownStyles } from '../tagging/heuristics';
import { addTag, addTags } from '../tagging';
import { validateContent } from './helpers';
import { v4 as uuidv4 } from 'uuid';

/**
 * Takes a pandoc AST node and returns a ProseMirror node.
 * A good overview of Pandoc's data model is available at [Pandoc Lua Filters](https://pandoc.org/lua-filters.html).
 */
export const renderers: { [pandocNodeName: string]: PandocRenderer } = {
    // virtual root node
    Document: async (contents: PandocNode[], opts, parentNode) => {
        return renderContent(contents, opts, parentNode);
    },
    Note: async (noteContents: Pandoc.Block[], opts) => {
        const children = await renderContent(noteContents, opts, noteContents);
        let footnote = opts.schema.nodes[SFNodeType.footnote].create({}, children, []);
        let inlineNodes: any[] = [];
        footnote.descendants((node, pos) => {
            if (!node.type.isInline) {
                if (pos > 0) {
                    inlineNodes.push(opts.schema.nodes[SFNodeType.hardBreak].create({}));
                }
                return true;
            }
            inlineNodes.push(node);
        });

        footnote = opts.schema.nodes[SFNodeType.footnote].create({}, inlineNodes, []);
        footnote.check();
        return footnote;
    },
    Figure: async (contents: Pandoc.FigureContents, opts, parentNode) => {
        const [[id], captionNode, content] = contents;
        const figureContent = await renderContent(content || [], opts, parentNode);
        const allowedContent: any[] = [];
        const figureId = id;
        let attrs = {
            id: figureId,
            figureId,
            src: undefined,
            type: 'image'
        };

        for (let node of figureContent) {
            switch (node.type.name) {
                case 'paragraph':
                    if (node.content?.content.some(n => n.type.name === SFNodeType.image)) {
                        const image = node.content?.content.find(n => n.type.name === SFNodeType.image);
                        const file: any = opts?.files?.find(f => f?.id === image?.attrs.id) || {};
                        // make sure we use the file
                        if (image?.attrs) {
                            attrs = {
                                ...attrs,
                                ...image?.attrs,
                            };
                            if (file?.id) { attrs.id = file.id; }
                            if (!attrs.src && file?.key) { attrs.src = file.key; }
                        }
                    }
                    break;
                case 'table':
                    allowedContent.push(node);
                    break;
                default:
                    //
                    break;
            }
        }

        if (attrs?.id) { attrs.id = attrs.id?.replaceAll('/', '_'); }
        if (attrs?.figureId) { attrs.figureId = attrs.figureId?.replaceAll('/', '_'); }

        const figcaption = await render({ t: '_Caption', c: captionNode }, opts);
        // TODO content
        return opts.schema.nodes[SFNodeType.figure].create(
            attrs,
            [
                ...allowedContent,
                figcaption
            ]
        );
    },
    Para: async (contents: Pandoc.Inline[], opts, parentNode: PandocNode, labels?: Label[]) => {
        // if a paragraph contains a single span we transfer the attributes to the paragraph and collapse the span
        let attrs = {};
        if (contents.some(c => c.t === 'Span') && contents.length === 1) {
            const span = contents[0];
            const [spanAttrs, spanContent] = span.c;
            if (spanAttrs) {
                const [identifier, classes, attributes] = spanAttrs;
                attrs = attributes.reduce((acc, [key, value]) => ({
                    ...acc, [key.replace('data-', '')]: value
                }), {
                    id: identifier?.length > 0 ? identifier : undefined,
                    class: classes?.length > 0 ? classes.join(' ') : undefined
                });
            }
            contents = spanContent;
        }

        // check for images inside the paragraph
        const children = await renderContent(contents, opts, contents, labels);

        //const prev = parentNode?.c.indexOf()

        // if a paragraph contains a single image we asume it is a figure
        // unless it's inside a table
        // TODO we should probably find the caption above or below instead of copying the alt-text
        // TODO we should guess image type based on the parent node, not the width
        if (children.some(c => c.type.name === 'image') && children.length === 1 && children[0]?.attrs?.metaData?.width > 400) {
            let figure, figureContent;
            try {
                const figureImage = children[0];
                let caption: ProseMirrorNode = [];

                const index = parentNode?.c?.findIndex(n => n.c === contents);
                const follower = index != undefined && parentNode?.c[index + 1];
                if (follower) {
                    const [followerAttrs] = follower?.c;
                    let hasCaptionClass = false;
                    if (Array.isArray(followerAttrs)) {
                        const [_identifier, classes, _attributes] = followerAttrs;
                        hasCaptionClass = classes.some(c => c?.toLowerString()?.includes('caption'));
                    }

                    const followerStringNode = await render(follower, opts, parentNode);
                    if (followerStringNode?.length === 1) {
                        const text = followerStringNode?.[0]?.textContent;
                        // TODO we could check for a prefix like Figure X here.
                        if (text?.startsWith('Figure') || text?.startsWith('Abbildung')) {
                            hasCaptionClass = true;
                        }

                        if (hasCaptionClass) {
                            caption = followerStringNode;
                            // TODO check validity
                        }
                        parentNode.c[index + 1] = DELETED_NODE;
                    }
                }

                const attrs = {};
                figureContent = [opts.schema.nodes[SFNodeType.caption].create(
                    attrs,
                    caption
                )];
                figure = opts.schema.nodes[SFNodeType.figure].create(
                    {
                        type: 'image',
                        src: figureImage.attrs.src, // TODO this should be a file reference, not the base64 rep
                        id: figureImage.attrs.id,
                        alt: figureImage.attrs.alt
                    },
                    figureContent
                );

                const { errors, valid } = validateContent(figureContent, opts.schema.nodes[SFNodeType.figure]);
                if (!valid) { throw new ImportError(`Invalid content for Figure "${figureContent?.toString()?.substring(0, 100)}" `, { errors, attrs }); }
                figure.check();

                return figure;
            } catch (e: any) {
                debugger;
                throw new ImportError('Could not render figure from image', {
                    message: e.message,
                    pandocNode: JSON.stringify(contents[0]),
                    parentNodeType: parentNode?.t,
                    figure,
                    figureContent
                });
            }
        }

        try {
            let marks: any[] = [];
            if (labels && labels?.length > 0) {
                const allowedMarks = opts.schema.nodes[SFNodeType.paragraph].spec?.marks || '';
                if (allowedMarks === null || allowedMarks.includes('labels') || allowedMarks === '_') {
                    marks = addTags(marks, labels.map(l => ({ key: l.key, value: l.value })), { schema: opts.schema });
                }
            }

            const para = opts.schema.nodes[SFNodeType.paragraph].create(
                attrs,
                children,
                marks
            );
            para.check();
            return para;
        } catch (e: any) {
            throw new ImportError('Could not render paragraph', {
                message: e.message,
                pandocNode: JSON.stringify(children),
                parentNodeType: parentNode?.t
            });
        }
    },
    Quoted: ([type, content], opts, parent) => {
        try {
            return renderContent(content, opts);
        } catch (e: any) {
            throw new ImportError(e.message, { type: 'Quoted' });
        }
    },
    RawBlock: ({ c }, opts) => {
        const [type, command] = c;
        if (type === 'tex') {
            if (command === '\\newpage') {
                return opts.schema.nodes[SFNodeType.pageBreak].create();
            }
        }
    },
    Plain: async (contents: Pandoc.Inline[], opts) => {
        const para = opts.schema.nodes[SFNodeType.paragraph].create(
            {},
            await renderContent(contents, opts, contents), []
        );
        para.check();
        return para;
    },
    OrderedList: async (list: Pandoc.OrderedListContents, opts) => {
        const [type, content] = list;
        // TODO store the type of list

        let listContent: any[] = [];
        for (let items of content) {
            listContent.push(await opts.schema.nodes[SFNodeType.list_item].create(
                {},
                await renderContent(items, opts, list)
            ));
        }

        // word treats heading numbering as list which may lead to empty artefacts from pandoc
        if (listContent.length > 0 && listContent[0].textContent?.length === 0) {
            // TODO what about lists that only contain images etc?
            return undefined;
        }

        return opts.schema.nodes[SFNodeType.ordered_list].create(
            {},
            listContent
        )
    },
    BulletList: async (items: Pandoc.BulletListContents, opts, parentNode) => {
        const renderedItems = await Promise.all(items.map(
            async (item) => opts.schema.nodes[SFNodeType.list_item].create(
                {},
                await renderContent(item, opts, items)
            )
        ));
        const list = opts.schema.nodes[SFNodeType.bullet_list].create(
            {},
            renderedItems
        )
        return list;
    },
    DefinitionList: async (items, opts) => {
        // https://pandoc.org/lua-filters.html#type-definitionlist
        const renderedItems = await Promise.all(items.map(
            async (item) => opts.schema.nodes[SFNodeType.paragraph].create(
                {},
                await renderContent(item[0], opts, items)
            )
        ));

        return renderedItems;
    },
    Div: async (pandocDiv: Pandoc.DivContents, opts, parentNode) => {
        const [attrs, content] = pandocDiv;
        let labels: any[] = [];
        let customStyle;

        if (attrs) {
            const [identifier, classes, attributes] = attrs;
            if (attributes?.length > 0) {
                for (let [key, value] of attributes) {
                    if (key === 'custom-style') {
                        customStyle = value;
                        for (let test of getKnownStyles()) {
                            const distance = JaroWinklerDistance(test.text, value, { ignoreCase: true });
                            if (distance > 0.9 || test.text?.length > 6 && value?.startsWith(test.text)) {
                                labels.push(test.label);
                            }
                        }
                    }
                }
            }

            if (classes.includes(SFNodeType.placeholder)) {
                if (opts.schema.nodes[SFNodeType.placeholder]) {
                    // placeholders have no content
                    return opts.schema.nodes[SFNodeType.placeholder].create(
                        attributes.reduce((acc, [key, value]) => ({
                            ...acc, [key.replace('data-', '')]: value
                        }), { id: identifier })
                    );
                }
            }
        }

        let divContent = await renderContent(content, opts, parentNode, labels.map(label => ({ key: label, value: undefined })));
        return divContent;
    },
    Header: async (header: Pandoc.HeaderContents, opts, parentNode) => {
        const headingType = opts.schema.nodes[SFNodeType.heading];
        const [level, attrs, headingContent] = header;

        const isUnnumbered = attrs[1].includes('unnumbered');
        // filter out hard breaks in headings
        let children: any[] = [];
        let footnotes: any[] = [];
        for (let node of headingContent) {
            let re = await render(node, opts, header);
            // inline nodes like Strong will render as an array
            if (re && !Array.isArray(re)) { re = [re]; }
            if (!re) { continue; }

            for (let child of re) {
                if (!child?.type || child.type.name === SFNodeType.hardBreak) { continue; }
                // TODO log a warning
                if (child.type.name === SFNodeType.math) { re = opts.schema.text(` ${child.attrs.tex} `); }
                if (child.type.name === SFNodeType.footnote) { footnotes.push(child); }
                children.push(child);
            }
        }

        const content = Fragment.from(children);
        const { errors, valid } = validateContent(content, headingType);
        if (!valid) { throw new ImportError(`Invalid content for Heading "${content?.toString()?.substring(0, 100)}" `, { errors, attrs }); }

        const headingAttributes: any = { level };
        if (isUnnumbered) {
            headingAttributes.numbering = 'none';
        }

        // TODO remove this once footnotes are allowed in headings
        if (footnotes.length > 0) {
            throw new ImportError('Footnote not allowed in heading', { pmNode: footnotes[0].toJSON() })
        }

        let heading = headingType.create(headingAttributes, content, []);

        let knownHeadings = getKnownHeadings();
        // we attempt a naive implementation of a heading heuristic here that can be augmented by more sophisticated approaches later
        if (heading.textContent?.length > 0) {
            let matches = [];
            for (let test of knownHeadings) {
                test.distance = JaroWinklerDistance(test.text, heading.textContent, { ignoreCase: true });
            }
        }

        knownHeadings = knownHeadings.sort((a, b) => (b.distance ?? 0) - (a.distance ?? 0));
        if (knownHeadings[0]?.distance != undefined && knownHeadings[0]?.distance > 0.95) {
            const test = knownHeadings[0];
            headingAttributes.type = test.type || headingAttributes.type;
            headingAttributes.numbering = test.numbering || headingAttributes.numbering;
            headingAttributes.role = test.role || headingAttributes.role;
            headingAttributes.locale = test.locale || headingAttributes.locale;
            heading = headingType.create(headingAttributes, content, []);
        }

        heading.check();

        return heading;
    },
    Table: async (tableContents: Pandoc.TableContents, opts) => {
        let rows: ProseMirrorNode[] = [];
        const [attr, pdCaption, colspecs, pdHead, pdBodies, pdFoot] = tableContents;

        const [headerAttrs, headerRows] = pdHead;
        // Table head @see https://pandoc.org/lua-filters.html#type-tablehead
        for (let row of headerRows) {
            // @see https://pandoc.org/lua-filters.html#type-row
            rows.push(await render(
                {
                    t: '_Row',
                    c: {
                        part: TablePart.Head,
                        attr: row[0],
                        cells: row[1],
                        rowHeaders: 0
                    }
                },
                opts,
                tableContents
            ));
        }

        // List of table bodies @see https://pandoc.org/lua-filters.html#type-tablebody
        for (let i = 0; i < pdBodies.length; i++) {
            const rowHeaders = pdBodies[i][1];
            const bodyRows = pdBodies[i][3];
            rows.push(...await Promise.all(bodyRows.map((row: Pandoc.Row) => render(
                {
                    t: '_Row',
                    c: {
                        part: TablePart.Body,
                        attr: row[0],
                        cells: row[1],
                        rowHeaders
                    }
                },
                opts,
                row
            ))));
        }
        rows.push(...await Promise.all(pdFoot[1].map((row: Pandoc.Row) => render(
            {
                t: '_Row',
                c: {
                    part: TablePart.Foot,
                    attr: row[0],
                    cells: row[1],
                    rowHeaders: 0
                }
            },
            opts,
            row
        ))));
        const caption = await render({ t: '_Caption', c: pdCaption }, opts);
        let table = opts.schema.nodes[SFNodeType.table].create({}, rows, []);

        // TODO move these matchers into their own functions / plugins
        // try to match tables used to layout equation numbers
        let labels: Mark[] = [];

        if (caption.nodeSize === 2 && table.textContent.startsWith('(') && table.textContent.endsWith(')') && table.textContent.replaceAll(/[^a-zA-Z]/g, '')?.length < 10) {
            for (let rowNumber = 0; rowNumber < table.childCount; rowNumber++) {
                const row = table.child(rowNumber);
                const matchEquationNumber = new RegExp(/\((\d+(\.\d+)?)\)/);
                const label: string = row.textContent;
                const match: any[] | null = label.match(matchEquationNumber);
                if (match?.length === 3) {
                    const equationNumber = match[1];
                    // last cell has the numbering
                    if (row.child(row.childCount - 1).textContent === row.textContent) {
                        for (let col = 0; col < row.childCount - 1; col++) {
                            const cell = row.child(col);
                            if (cell.childCount > 0 && cell.child(0)?.child(0)?.type.name === SFNodeType.math) {
                                labels = addTag(labels, { key: 'equation-in-table', value: { numberingString: equationNumber } }, { schema: opts.schema });
                            }
                        }
                    }
                }
            }
        } else if (table.textContent?.trim?.length === 0 &&
            table.childCount === 1 && table.child(0)?.child(0).childCount > 0 &&
            table.child(0)?.child(0)?.child(0).childCount > 0) {
            const c = table.child(0)?.child(0)?.child(0)?.child(0);
            if (c?.type.name === SFNodeType.image) {
                labels = addTag(labels, { key: 'image-in-empty-table', value: JSON.stringify({ alt: c.attrs?.alt, id: c.attrs?.id, title: c.attrs?.title }) }, { schema: opts.schema });
            }
        }

        try {
            table.check();
        } catch (e) {
            debugger;
            console.error(e);
            throw e;
        }

        return opts.schema.nodes[SFNodeType.figure].create(
            { type: 'native-table' },
            [table, caption],
            labels
        );
    },
    /*
    ** Pseudo-elements, which don't have a tag in the native pandoc representation
    */
    _TableHeaderCell: (pdHeaderCell: Pandoc.Cell, opts, parentNode) => {
        return renderCell(SFNodeType.table_header, pdHeaderCell, opts, parentNode);
    },
    _TableBodyCell: (pdBodyCell: Pandoc.Cell, opts, parentNode) => {
        return renderCell(SFNodeType.table_cell, pdBodyCell, opts, parentNode);
    },
    _Row: async (
        pandocRowComponents: RowComponents,
        opts
    ): Promise<ProseMirrorNode> => {
        const { part, cells, rowHeaders } = pandocRowComponents;
        const defaultElementType = part == TablePart.Body
            ? '_TableBodyCell'
            : '_TableHeaderCell';
        let pmCells: ProseMirrorNode[] = await Promise.all(cells.map(async (cell, i) => {
            try {
                const cellType = i < rowHeaders ? '_TableHeaderCell' : defaultElementType;
                return await render(
                    { t: cellType, c: cell },
                    opts
                );
            } catch (e: any) {
                if (e instanceof ImportError) { throw e; }
                throw new ImportError('Could not render table cell ' + i, {
                    message: e.message,
                    pandocNode: JSON.stringify(cells[i])
                });
            }
        }));
        return opts.schema.nodes[SFNodeType.table_row].createChecked({}, pmCells, []);
    },
    _Caption: async (pandocCaption: Pandoc.Caption, opts) => {
        const [_, longCaption] = pandocCaption;
        return opts.schema.nodes[SFNodeType.caption].createChecked(
            { id: uuidv4() },
            await renderContent(longCaption, opts, pandocCaption)
        );
    },
    BlockQuote: async (pandocNode: Pandoc.BlockQuote, opts) => {
        return opts.schema.nodes[SFNodeType.blockquote].createChecked(
            { id: uuidv4() },
            await renderContent(pandocNode, opts, pandocNode)
        );
    }
};

/**
 * Type collecting all information required to render a row.
 */
interface RowComponents {
    part: TablePart,
    attr: Pandoc.Attr,
    cells: Pandoc.Cell[],
    rowHeaders: number
}

/**
 * Parts of a table; either refers to the table head, table body, or
 * table foot.
 */
const enum TablePart {
    Head,
    Body,
    Foot
};

/**
 * Converts pandoc alignment information into a `text-align` attribute
 * value.
 */
const toTextAlign = (align: Pandoc.Alignment) => {
    switch (align) {
        case Pandoc.Alignment.AlignLeft: return 'left';
        case Pandoc.Alignment.AlignRight: return 'right';
        case Pandoc.Alignment.AlignCenter: return 'center';
        default: return null;
    }
}

/**
 * Helper function for table cell rendering.
 */
const renderCell = async (
    nodeType: SFNodeType,
    cell: Pandoc.Cell,
    opts,
    _parentNode: PandocNode
): Promise<ProseMirrorNode> => {
    let cellContents = cell[4];
    const rowspan = cell[2];
    const colspan = cell[3];
    const align = toTextAlign(cell[1].t);

    try {
        for (let i = 0; i < cellContents.length; i++) {
            let item: PandocNode = cellContents[i];
            switch (item.t) {
                case 'Header':
                    const [_level, _attrs, headingContent] = item.c;
                    cellContents[i] = {
                        t: 'Para',
                        c: headingContent
                    }
                    break;
            }
        }

        const content = (await renderContent(cellContents, opts, cell)).map((x) => {
            if (x.type.name == 'paragraph') {
                x.attrs['text-align'] = align;
            }
            return x;
        });

        const node = opts.schema.nodes[nodeType].create(
            { rowspan, colspan },
            content,
            []
        );

        try {
            node.check();
            return node;
        } catch (e: any) {
            debugger;
            throw new ImportError('Could not render table cell content', {
                message: e.name + ': ' + e.message,
                type: nodeType,
                pandocNode: JSON.stringify(content),
                pmNode: node.toString()
            });
        }

    } catch (e: any) {
        if (e instanceof ImportError) { throw e; }
        throw new ImportError('Could not render table cell', {
            message: e.name + ': ' + e.message,
            type: nodeType,
            pandocNode: JSON.stringify(cellContents)
        });
    }
}
