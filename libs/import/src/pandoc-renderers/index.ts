import { renderers as inlineRenderers } from './inline';
import { renderers as blockRenderers } from './block';
import { renderers as metaRenderers } from './meta';
import * as Pandoc from './pandoc-types';

export class ImportError extends Error {
    constructor(message: string, public payload: any) {
        super(message);

        // Set the prototype explicitly.
        Object.setPrototypeOf(this, ImportError.prototype);
    }

    toString() {
        return `${this.name}: ${this.message}`;
    }

    toJSON() {
        return { message: this.message, payload: this.payload };
    }
}

export type ProseMirrorNode = any;
export type ProseMirrorMark = any;

export interface RendererOptions {
    renderers: {
        [key: string]: PandocRenderer
    };
    schema: any;
    marks?: ProseMirrorMark[],
    /** The absolute path to a directory that holds any assets/media referenced */
    assets?: string;
    /** skip invalid content (like images in table headers) to get a rough document outline or meta data */
    skipInvalid?: boolean;
    files?: { id: string; key: string; stats?: any }[];
}

export type PandocRendererReturnType = ProseMirrorNode | ProseMirrorNode[];

/**
 * Renders pandoc nodes as ProseMirror objects.
 */
export interface PandocRenderer {
    (pandocNode: PandocNode, opts: RendererOptions, parentNode?: PandocNode, labels?: Label[]): Promise<PandocRendererReturnType>
}

export type PandocDefaultC = PandocNode[] | string | any[];
export type PandocNode =
    | Pandoc.Inline
    | Pandoc.Block
    | Pandoc.MetaValue
    | Pandoc.Document;

export const pandocRenderers = {
    ...inlineRenderers,
    ...blockRenderers,
    ...metaRenderers
};

export interface Label {
    key: string;
    value?: string;
}

/** Returns an array of pandoc notes for any pandoc node array or [] if the
 * pandocNode array is not defined. */
export const renderContent = async (
    pandocNodes: PandocNode[],
    opts: RendererOptions,
    parentNode?: PandocNode,
    labels?: Label[]
): Promise<ProseMirrorNode[]> => {

    if (!pandocNodes) { return []; }
    let renderResult: any[] = [];

    for (let child of pandocNodes) {
        try {
            const result = await render(child, opts, parentNode, labels);
            result && renderResult.push(result);
        } catch (e: any) {
            if (e instanceof ImportError) { throw e; }
            throw new ImportError(e.message, {
                ...(e?.payload || {}),
                pandocNode: JSON.stringify(child),
                parentNodeType: parentNode?.t
                // add additional details here if needed
            });
        }
    }

    return [].concat(...renderResult);
}

/** Renders a single pandoc node */
export const render: PandocRenderer = async (
    pandocNode: PandocNode,
    opts: RendererOptions,
    parentNode?: PandocNode,
    labels?: Label[]
): Promise<PandocRendererReturnType | undefined> => {

    if (pandocNode === Pandoc.DELETED_NODE) {
        return undefined;
    }

    if (Array.isArray(pandocNode)) {
        throw new ImportError('Expected Pandoc node but received array', { node: JSON.stringify(pandocNode).substring(0, 200), parentNodeType: parentNode?.t })
    }

    try {
        if (opts.renderers[pandocNode.t]) {
            const results = await opts.renderers[pandocNode.t](pandocNode.c, opts, parentNode, labels);
            return Array.isArray(results) ? [].concat(...results) : results;
        }
    } catch (e: any) {
        if (e instanceof ImportError) { throw e; }
        debugger;
        throw new ImportError('Could not render Pandoc node ' + pandocNode.t, { type: pandocNode.t, error: e.message, ...(e.payload || {}), parentNodeType: parentNode?.t });
    }

    debugger;
    throw new ImportError('No renderer defined for ' + pandocNode.t, { type: pandocNode.t, node: JSON.stringify(pandocNode).substring(0, 200), parentNodeType: parentNode?.t, availableRenderers: Object.keys(opts.renderers) });
}
