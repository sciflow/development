import { PandocNode, renderContent, PandocRenderer, ProseMirrorNode, ProseMirrorMark, Label } from '.';
import { SFMarkType, SFNodeType, createId } from '@sciflow/schema';
import { CitationItem, SourceField } from '@sciflow/cite';
import * as Pandoc from './pandoc-types';
import { stringRenderers } from './meta';
import sharp, { Sharp } from 'sharp';
import { existsSync, readFileSync } from 'fs';
import { basename, extname, join } from 'path';
import { placeholder } from './error-placeholder';
import { JaroWinklerDistance } from 'natural';
import { getKnownStyles } from '../tagging';

const convertToPixels = (valueWithUnit?: string) => {
    if (!valueWithUnit) { return undefined; }
    // Extract the numeric value and the unit (in, cm, pt, mm)
    const regex = /^([\d.eE+-]+)(in|cm|pt|mm)$/;
    const match = valueWithUnit.match(regex);

    if (!match) {
        throw new Error('Invalid format. Please provide a valid string with a numeric value and unit (e.g., "1.1in", "1.2cm", "12pt", "30mm").');
    }

    const value = parseFloat(match[1]);
    const unit = match[2];

    // Convert based on the unit
    switch (unit) {
        case 'px':
            return value;
        case 'in':
            return Math.round(value * 96); // Inches to pixels
        case 'cm':
            return Math.round((value * 96) / 2.54); // Centimeters to pixels
        case 'pt':
            return Math.round((value * 96) / 72); // Points to pixels
        case 'mm':
            return Math.round((value * 96) / 25.4); // Millimeters to pixels
        default:
            throw new Error('Unsupported unit. Only "in", "cm", "pt", and "mm" are supported.');
    }
};

/** How to handle an element that is nested below another element of the
same type. */
type NestingBehavior = (mark: ProseMirrorMark, marks: ProseMirrorMark[])
    => ProseMirrorMark[]

const ensureMark: NestingBehavior = (mark, marks) =>
    mark.addToSet(marks)

const toggleMark: NestingBehavior = (mark, marks) =>
    mark.isInSet(marks)
        ? mark.removeFromSet(marks)
        : mark.addToSet(marks);

const renderTextWithMark =
    (markType: SFMarkType, nesting: NestingBehavior): PandocRenderer => {
        return async (contents: Pandoc.Inline[], opts, parentNode?: PandocNode) => {
            if (parentNode?.t === 'Link' && markType === SFMarkType.emphasis) {
                // we do not want to transform underlines inside of anchors into emphasis
                return renderContent(
                    contents,
                    { ...opts, marks: opts.marks ?? [] }
                );    
            }
            return renderContent(
                contents,
                { ...opts, marks: nesting(opts.schema.marks[markType].create({}), opts.marks ?? []) }
            );
        };
    };

export const renderers: { [key: string]: PandocRenderer } = {
    Strong: renderTextWithMark(SFMarkType.strong, toggleMark),
    Emph: renderTextWithMark(SFMarkType.emphasis, toggleMark),
    Subscript: renderTextWithMark(SFMarkType.subscript, ensureMark),
    Superscript: renderTextWithMark(SFMarkType.superscript, ensureMark),
    // The doc model doesn't support deleted, smallcaps, or underlined
    // text, so those are treated just like emphasized text.
    Underline: renderTextWithMark(SFMarkType.emphasis, toggleMark),
    Strikeout: renderTextWithMark(SFMarkType.emphasis, toggleMark),
    SmallCaps: renderTextWithMark(SFMarkType.emphasis, toggleMark),
    Str: (str: string, { schema, marks }) => schema.text(str, marks ?? []),
    SoftBreak: (_, { schema, marks }) => schema.text(' ', marks ?? []),
    Space: (_, { schema, marks }) => schema.text(' ', marks ?? []),
    LineBreak: async (_, { schema, marks }) => {
        return schema.nodes[SFNodeType.hardBreak].create({}, [], marks ?? []);
    },
    Span: async (contents: [Pandoc.Attr, Pandoc.Inline[]], opts) => {
        const [identifier, classes, attributes] = contents[0];

        let customStyle;
        let labels: Label[] = []
        if (attributes?.length > 0) {
            for (let [key, value] of attributes) {
                if (key === 'custom-style') {
                    customStyle = value;
                    for (let test of getKnownStyles()) {
                        const distance = JaroWinklerDistance(test.text, value, { ignoreCase: true });
                        if (distance > 0.9 || test.text?.length > 6 && value?.startsWith(test.text)) {
                            labels.push(test.label);
                        }
                    }
                }
            }
        }

        return renderContent(contents[1], opts, undefined, labels);
    },
    Link: (link: Pandoc.LinkContents, opts, _parentNode, labels) => {
        const [attributes, content, [href, title]] = link;

        let type;
        if (href?.startsWith('#')) { type = 'xref'; }
        
        const anchorMark = opts.schema.marks[SFMarkType.anchor].create({
            href,
            type,
            title: title?.length === 0 ? null : title
        });
        return renderContent(
            content,
            { ...opts, marks: [...(opts.marks ?? [])?.filter(m => m.type.name !== 'anchor'), anchorMark] },
            { t: 'Link', content: link },
            labels
        );
    },
    Code: (codeContents: [Pandoc.Attr, string], { schema, marks }) => {
        const code = codeContents[1];
        return schema.text(code, marks ?? []);
    },
    RawInline: async () => {
        return [];
    },
    Quote: async (quoteContents, opts) => {
        const textMarks = opts.marks ?? []
        return [
            opts.schema.text('"', textMarks),
            await renderContent(quoteContents[1], opts),
            opts.schema.text('"', textMarks)
        ];
    },
    Cite: async (citeContents: Pandoc.CiteContents, opts) => {
        const citationData = citeContents[0];

        // we only expect primitives inside the citation data
        let source;
        if (citationData?.length > 0) {
            const citationItems: CitationItem[] = await Promise.all(citationData.map(async (data) => {
                let item: CitationItem = {
                    id: data.citationId
                };

                // TODO implement citationMode: NormalCitation, AuthorInText

                if (data.citationPrefix?.length > 0) {
                    item.prefix = (await renderContent(data.citationPrefix, { ...opts, renderers: stringRenderers })).join('');
                }

                if (data.citationSuffix?.length > 0) {
                    item.suffix = (await renderContent(data.citationSuffix, { ...opts, renderers: stringRenderers })).join('');
                }

                return item;
            }));
            source = encodeURI(SourceField.toString({
                citationItems,
                properties: {
                    noteIndex: 0
                }
            }));
        }

        const inlines = citeContents[1];
        const text = await renderContent(
            inlines,
            opts
        );
        const citeNode = opts.schema.nodes[SFNodeType.citation].createChecked({
            source
        }, text);
        return citeNode;
    },
    Math: (mathContents: Pandoc.MathContents, { schema, marks }) => {
        const mathTeX = mathContents[1]
        return schema.nodes[SFNodeType.math].createChecked(
            { tex: mathTeX },
            [],
            []
        );
    },
    Image: async (imageContents: Pandoc.LinkContents, opts) => {
        const [meta, alt, data] = imageContents;
        const [id, classes, attributes] = meta;
        const [url, title] = data;
        let width: number | undefined;
        let height: number | undefined;
        
        try {
            width = convertToPixels(attributes?.find(([key]) => key === 'width')?.[1]);
            height = convertToPixels(attributes?.find(([key]) => key === 'height')?.[1]);
        } catch (e) {
            debugger;
            console.error('Could not convert image dimensions: ' + e.message);
        }


        const altText = (await renderContent(alt, { ...opts, renderers: stringRenderers })).join('');
        const file: any = opts?.files?.find(f => f?.key === url) || {};
        let fullPath = data[0];

        let imagePlaceholder;
        let filename = basename(fullPath);
        if (opts.assets && !existsSync(filename) && existsSync(join(opts.assets, fullPath))) { fullPath = join(opts.assets, fullPath); }

        const imagePath = fullPath ? fullPath.replace(opts.assets + '/' ?? '', '') : createId();
        const ext = extname(imagePath)?.replace('.', '');
        const imageId = imagePath?.replaceAll('/', '_');

        if (existsSync(fullPath) && ext !== 'emf' && ext !== 'wmf') {
            try {
                let image: Sharp = await sharp(readFileSync(fullPath))?.withMetadata();
                const metadata = await image.metadata();
                const resizeTo = Math.min((metadata?.width || 128), 128);

                const imageBuffer = await sharp(
                    await image
                        ?.resize(resizeTo, null, { kernel: sharp.kernel.nearest, fit: 'inside' })
                        .flatten({ background: 'white' })
                        .toBuffer()
                )
                    .resize({ width: resizeTo, kernel: sharp.kernel.nearest, withoutEnlargement: true })
                    .webp({ lossless: false, quality: 100 })
                    .toBuffer();
                const thumb = imageBuffer?.toString('base64');
                imagePlaceholder = `data:image/webp;base64,${thumb}`;
            } catch (e) {
                if (e.message === 'Input buffer contains unsupported image format') {
                    const text = await sharp({
                        text: {
                            text: `no preview for ${filename}`,
                            width: width || 200,
                            height: height || 50,
                            font: 'sans'
                        }
                    })
                        .webp()
                        .toBuffer();
                    imagePlaceholder = `data:image/webp;base64,${text.toString('base64')}`;
                } else {
                    console.error('Failed reading the file', { message: e.message, imagePath, imageId });
                }
            }
        }

        return opts.schema.nodes[SFNodeType.image].createChecked({
            src: imagePlaceholder,
            alt: altText,
            width,
            height,
            id: imageId,
            title
        });
    }
}
