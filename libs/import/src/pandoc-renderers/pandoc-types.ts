import { PandocNode } from ".";

export const DELETED_NODE = { t: 'DELETED_NODE', c: [] };

/**
 * Type constructor
 */
export interface TaggedObject<Tag, Contents = undefined> {
    t: Tag,
    c: Contents
}

/**
 * Attributes of an element:
 *  - identifier
 *  - classes
 *  - key-value pairs
 */
export type Attr = [identifier: string, classes: string[], attributes: [key: string, value: string][]];

/** Pandoc inline element */
export type Inline =
    | TaggedObject<'Str', string>
    | TaggedObject<'Space'>
    | TaggedObject<'SoftBreak'>
    | TaggedObject<'LineBreak'>
    // emphasis and styling
    | TaggedObject<'Emph', Inline[]>
    | TaggedObject<'SmallCaps', Inline[]>
    | TaggedObject<'Strikeout', Inline[]>
    | TaggedObject<'Strong', Inline[]>
    | TaggedObject<'Subscript', Inline[]>
    | TaggedObject<'Superscript', Inline[]>
    | TaggedObject<'Underline', Inline[]>
    // other
    | TaggedObject<'Code', [Attr, string]>
    | TaggedObject<'Link', LinkContents>
    | TaggedObject<'Image', LinkContents>
    | TaggedObject<'Note', Block[]>
    | TaggedObject<'Span', [Attr, Inline[]]>
    | TaggedObject<'Cite', CiteContents>
    | TaggedObject<'Math', MathContents>
    | TaggedObject<'RawInline', [Format, string]>
    // TODO: fix these
    | TaggedObject<'Quoted', any>;

/** Document metadata value */
export type MetaValue = any;

/** A  */
export type MetaList = TaggedObject<MetaList, MetaMap[]>; 

export type MetaInlineList = TaggedObject<MetaList, MetaInlines[]>;

export type MetaMapContent = { [key:string]: MetaString | MetaMap | MetaList };

export type MetaInlines = TaggedObject<'MetaInlines', Inline[]>;

export type MetaMap = TaggedObject<'MetaMap', MetaMapContent>;

export type MetaString = TaggedObject<MetaString, string>;

/**
 * Virtual document handed to the root renderer.
 */
export type Document = TaggedObject<'Document', PandocNode[]>;

//
// Blocks
//

/**
 * Block element
 */
export type Block =
    | TaggedObject<'BlockQuote', BlockQuote[]>
    | TaggedObject<'BulletList', BulletListContents>
    | TaggedObject<'CodeBlock', CodeBlockContents>
    | TaggedObject<'DefinitionList', any>  // TODO: add proper content type
    | TaggedObject<'Div', DivContents>
    | TaggedObject<'Figure', FigureContents>
    | TaggedObject<'Header', HeaderContents>
    | TaggedObject<'HorizontalRule'>
    | TaggedObject<'LineBlock', LineBlockContents>
    | TaggedObject<'Null'>
    | TaggedObject<'OrderedList', OrderedListContents>
    | TaggedObject<'Para', Inline[]>
    | TaggedObject<'Plain', Inline[]>
    | TaggedObject<'RawBlock', [string, string]>
    | TaggedObject<'TableContents', TableContents>

/**
 * Heading element
 */
export type HeaderContents = [number, Attr, Inline[]];

/**
 * Generic block container with attributes.
 */
export type DivContents = [Attr, Block[]];

/**
 * Figure block container with attributes.
 */
export type FigureContents = [Attr, Block[], Inline[]];

/**
 * Multiple non-breaking lines
 */
export type LineBlockContents = Inline[][]

/**
 * Code block (literal) with attributes
 */
export type CodeBlockContents = [Attr, string]

/**
 * Bullet list (list of items, each a list of blocks)
 */
export type BulletListContents = Block[][]

/**
 * Bullet list (list of items, each a list of blocks)
 */
 export type BlockQuote = Block[][]

/**
 * Ordered list (attributes and a list of items, each a list of blocks)
 */
export type OrderedListContents = [ListAttributes, Block[][]]

/**
 * List attributes The first element of the triple is the start number
 * of the list.
 */
export type ListAttributes = [
    number,
    TaggedObject<ListNumberStyle>,
    TaggedObject<ListNumberDelim>
]

/**
 * Style of list numbers.
 */
export const enum ListNumberStyle {
    DefaultStyle = 'DefaultStyle',
    Example = 'Example',
    Decimal = 'Decimal',
    LowerRoman = 'LowerRoman',
    UpperRoman = 'UpperRoman',
    LowerAlpha = 'LowerAlpha',
    UpperAlpha = 'UpperAlpha'
}

/**
 * Delimiter of list numbers.
 */
export const enum ListNumberDelim {
    DefaultDelim = 'DefaultDelim',
    Period = 'Period',
    OneParen = 'OneParen',
    TwoParen = 'TwoParen'
}

/**
 * Row in a table.
 */
export type Row = [Attr, Cell[]]

export type Cell = [Attr, {t: Alignment}, RowSpan, ColSpan, Block[]];

/** Integer specifying the number of rows that a cell spans. */
export type RowSpan = number

/** Integer specifying the number of columns that a cell spans. */
export type ColSpan = number

/**
 * Table caption, containing an optional short caption usable, e.g., in a list
 * of tables, and a long caption that can span multiple blocks.
 */
export type Caption = [Inline[] | null, Block[]]

export type ColWidth = TaggedObject<'ColWidth', number>

export type ColWidthDefault = TaggedObject<'ColWidthDefault'>

/**
 * Column specification - alignment and column width
 */
export type ColSpec = [ TaggedObject<Alignment>, (ColWidth | ColWidthDefault)]


/**
 * Table head
 */
export type TableHead = [Attr, Row[]]

/**
 * Table foot
 */
export type TableFoot = [Attr, Row[]]

/**
 * The number of row header cells in a row.
 */
export type RowHeaders = number

/**
 * Table body - number of row headers, sub header rows, and body rows.
 */
export type TableBody = [Attr, RowHeaders, Row[], Row[]]


/**
 * Table having:
 *  - caption,
 *  - column alignments and relative widths,
 *  - table head
 *  - table bodies (each with subheader and rows)
 *  - table foot
 */
export type TableContents =
    [Attr, Caption, ColSpec[], TableHead, TableBody[], TableFoot]

/**
 * Alignment of a table column.
 */
export const enum Alignment {
    AlignLeft = 'AlignLeft',
    AlignRight = 'AlignRight',
    AlignCenter = 'AlignCenter',
    AlignDefault = 'AlignDefault'
}

//
// Inlines
//

/**
 * Hyperlink
 */
export type LinkContents = [Attr, Inline[], Target];

/**
 * Math formula.
 */
export type MathContents = [TaggedObject<MathType>, string]

/**
 * Type of math element (display or inline).
 */
export const enum MathType {
    DisplayMath = 'DisplayMath',
    InlineMath = 'InlineMath'
}

export const enum QuoteType {
    SingleQuote = 'SingleQuote',
    DoubleQuote = 'DoubleQuote'
}

/**
 * Markup format name (a string).
 */
export type Format = string

/**
 * Hyperlink target with target URL and title.
 */
export type Target = [string, string]

// Citations

/**
 * Collection of citation, and the placeholder value.
 */
export type CiteContents = [Citation[], Inline[]];

export interface Citation {
    citationId: string,
    citationPrefix: Inline[],
    citationSuffix: Inline[],
    citationMode: TaggedObject<CitationMode>,
    citationNoteNum: number,
    citationHash: number,
}

/**
 * How the citation is to be rendered.
 */
export const enum CitationMode {
    AuthorInText = 'AuthorInText',
    SuppressAuthor = 'SuppressAuthor',
    NormalCitation = 'NormalCitation'
}
