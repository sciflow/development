import { Fragment, NodeSpec } from "prosemirror-model";

export const validateContent = (content: Fragment, nodeType: NodeSpec) => {

    const errors: any = [];

    content.forEach(child => {
        if (!nodeType.contentMatch.matchType(child.type)) {
            errors.push({
                message: `${child.type.name} is not allowed ${nodeType.name}`,
                text: child.textContent
            });
        }
    });

    content.forEach(child => {
        child.marks.forEach(mark => {
            if (!nodeType.allowedMarks || !nodeType.allowsMarks([mark])) {
                errors.push({
                    message: `${mark.type.name} is not allowed inside ${nodeType.name}`,
                    text: child.textContent
                });
            }
        });
    });

    return errors.length === 0 ? {
        valid: true,
        errors: []
    } : {
        valid: false,
        errors
    };
}