import { createId, schemas, SFNodeType } from '@sciflow/schema';
import { assert } from 'console';
import { JSDOM } from 'jsdom';
import { DOMSerializer, Node, Schema } from 'prosemirror-model';
import { v4 as uuidv4 } from 'uuid';
import { ImportError, PandocNode, pandocRenderers, ProseMirrorNode, render, renderContent } from './pandoc-renderers';
import { MetaInlineList, MetaList } from './pandoc-renderers/pandoc-types';

const dom = new JSDOM();
const virtualDocument = dom.window.document;

/** Extacts the reference meta data */
const extractReferences = async (meta: { references?: MetaList }): Promise<any[]> => {
    if (!meta?.references) {
        return [];
    }

    return render(meta.references, { renderers: pandocRenderers, schema: schemas.manuscript });
};

/** Extacts the institute meta data */
const extractAffiliations = async (meta: { institute?: MetaList }): Promise<any[]> => {
    if (!meta?.institute) {
        return [];
    }

    return render(meta.institute, { renderers: pandocRenderers, schema: schemas.manuscript });
};

/** Extacts the institute meta data */
const extractAbstract = async (meta: { abstract?: MetaList }): Promise<any[]> => {
    if (!meta?.abstract) {
        return [];
    }

    const text = await render(meta.abstract, {
        renderers: {
            ...pandocRenderers,
            MetaBlocks: renderContent
        }, schema: schemas.manuscript
    }) || '';
    return text?.textContent?.replaceAll('\n', '');
};

const proseMirrorToHTML = async (proseMirrorDocument, schema) => {
    const serializer2 = DOMSerializer.fromSchema(schema);
    const content = serializer2.serializeFragment(proseMirrorDocument.content, { document: virtualDocument });
    const el = virtualDocument.createElement('div');
    el.append(content);
    return el.innerHTML;
}

export const assignIds = (node: Node, schema: Schema): Node | undefined => {
    if (!node) { return node; }
    const assignRandomIds = (node: any): any => {
        function assignIdRecursively(currentNode: any): any {
            if (currentNode.attrs?.id === null) {
                currentNode.attrs.id = uuidv4();
                if (/^[0-9]/.test(currentNode.attrs.id)) { currentNode.attrs.id = 'a' + currentNode.attrs.id.slice(1); }
            }

            const newContent = currentNode.content
                ? currentNode.content.map(assignIdRecursively)
                : currentNode.content;

            return { ...currentNode, content: newContent };
        }

        return assignIdRecursively(node);
    }

    return Node.fromJSON(schema, assignRandomIds(node.toJSON()));
}

interface ParsePandocAstOptions {
    mediaDir?: string;
    files?: { id: string; key: string; stats?: any; }[];
    renderStandalone?: boolean
    schemaName?: string,
    skipInvalid?: boolean;
}

/**
 * Extracts the document and meta data from a Pandoc JSON document.
 * @param pandocDocument the document as it comes from pandoc -t jsonma
 * @param renderStandalone split into parts/chapters or do a linear import
 * @param skipInvalid skip invalid content (like images in table headers) to get a rough document outline or meta data
 */
const parsePandocAST = async (pandocDocument: { blocks: PandocNode[], meta?: any; }, opts?: ParsePandocAstOptions) => {

    assert(pandocDocument.blocks != undefined, 'Pandoc blocks have to be defined');
    const schemaName = opts?.schemaName || 'manuscript';

    let errors: any[] = [];
    let referenceFiles: string[];

    const schema = schemas[schemaName];
    if (!schema) { throw new Error(`Schema ${schemaName} not found`); }

    try {
        const bibliography = pandocDocument.meta?.bibliography as MetaInlineList;
        referenceFiles = bibliography && bibliography.c.map(inline => inline.c.find(i => i.c.endsWith?.('.bib'))?.c).flat();
        const references = pandocDocument.meta?.references ? await extractReferences(pandocDocument.meta) : [];
        const affiliations = pandocDocument?.meta?.institute ? await extractAffiliations(pandocDocument.meta) : [];
        const abstract = pandocDocument?.meta?.abstract ? await extractAbstract(pandocDocument.meta) : '';

        let content: ProseMirrorNode[] = [];
        // render virtual root node
        const root = { t: 'Document', c: pandocDocument.blocks };
        // we render the root notes individually to catch all possible errors
        for (let blockNode of pandocDocument.blocks) {
            try {
                const node = await render(blockNode, {
                    ...(opts || {}),
                    renderers: pandocRenderers,
                    schema,
                    assets: opts?.mediaDir,
                    skipInvalid: opts?.skipInvalid
                }, root);
                if (node) {
                    if (Array.isArray(node)) {
                        content.push(...node);
                    } else {
                        content.push(node);
                    }
                }
            } catch (e: any) {
                if (e instanceof ImportError) {
                    const errorNode = schema.nodes.paragraph.createAndFill({ id: createId(), class: 'error', 'text-align': 'center' }, [
                        schema.text('Error importing text: ' + e.payload?.errors?.map(e => e.message).join(', '))
                    ]);
                    content.push(errorNode);
                    errors.push(e);
                } else {
                    throw e;
                }
            }
        }

        let header, title, subtitle;

        try {
            const hasTitle = pandocDocument.meta?.title?.c != undefined;
            const hasSubTitle = pandocDocument.meta?.title?.c != undefined;
            const requiredContent = schema.nodes[SFNodeType.document].spec.content;

            // if the document requires a header and the pandoc document has a title
            if (requiredContent?.startsWith('header') && hasTitle) {
                try {
                    let titleContent: ProseMirrorNode[] = [];
                    // (!) meta inlines do not render as ProseMirror but json / primitive types
                    if (pandocDocument.meta?.title?.c) {
                        titleContent = await renderContent(pandocDocument.meta?.title?.c || 'Untitled', { renderers: pandocRenderers, schema, skipInvalid: opts?.skipInvalid });
                        if (titleContent.some(c => c.isBlock)) {
                            // collapse block nodes
                            titleContent = titleContent.map(n => (n.content || [])).flat();
                        }
                        title = await render(pandocDocument.meta.title, { renderers: pandocRenderers, schema, skipInvalid: opts?.skipInvalid });
                        title = title?.replaceAll('\n', '').trim();
                    }

                    let subTitleContent: ProseMirrorNode[] = [];
                    // (!) meta inlines do not render as ProseMirror but json / primitive types
                    if (pandocDocument.meta?.subtitle?.c) {
                        subTitleContent = await renderContent(pandocDocument.meta?.subtitle?.c || 'Untitled', { renderers: pandocRenderers, schema, skipInvalid: opts?.skipInvalid });
                        if (subTitleContent.some(c => c.isBlock)) {
                            // collapse block nodes
                            subTitleContent = subTitleContent.map(n => (n.content || [])).flat();
                        }
                        subtitle = await render(pandocDocument.meta.subtitle, { renderers: pandocRenderers, schema, skipInvalid: opts?.skipInvalid });
                        subtitle = subtitle?.replaceAll('\n', '').trim();
                    }

                    const titleHeading = schema.nodes[SFNodeType.heading].create({ id: 'title', role: 'title', level: 1 }, titleContent);
                    let subtitleEl: Node | undefined;
                    if (subtitle?.length > 0) {
                        subtitleEl = schema.nodes[SFNodeType.subtitle].create({ id: 'subtitle' }, subTitleContent);
                        subtitleEl.check();
                    }

                    const headerContent = [
                        titleHeading,
                        subtitleEl
                    ].filter((n) => n != undefined) as Node[];
                    header = schema.nodes[SFNodeType.header].create({}, headerContent);
                    header.check();
                } catch (e: any) {
                    header = schema.nodes[SFNodeType.header].create({}, [
                        schema.nodes[SFNodeType.heading].create({ id: 'title', role: 'title', level: 1 }, [schema.text(title)]),
                        subtitle?.length > 0 ? schema.nodes.subtitle.create({ id: 'subtitle'}, [schema.text(subtitle)]) : undefined
                    ].filter(n => n != undefined));
                }
                // the header is not optional and the pandoc document has no title
            } else if (requiredContent?.startsWith('header') && !requiredContent?.startsWith('header?')) {
                throw new Error('A heading title is required');
            }
        } catch (e: any) {
            console.error(e);
        }

        let doc, parts;
        if (opts?.renderStandalone) {
            parts = [];
            let currentPartContent: any[] = [], startsWithHeading = false;
            const pushPart = () => {
                const attrs = { ...currentPartContent[0]?.attrs || {} };
                delete attrs.level;
                if (!attrs.id) {
                    attrs.id = uuidv4();
                    if (/^[0-9]/.test(attrs.id)) { attrs.id = 'a' + attrs.id.slice(1); }
                }
                let part;
                try {
                    part = schema.nodes[SFNodeType.document].create({ ...attrs, id: 'p+' + attrs.id }, currentPartContent);
                    part.check();
                    parts.push(part);
                } catch (e) {
                    debugger;
                    console.error('Could not create document', e, attrs);
                }
                currentPartContent = [];
            }
            // check part for heading 1 and split if needed
            for (let element of content) {
                if (element.type.name === SFNodeType.document) {
                    parts.push(element);
                } else {
                    if (currentPartContent.length === 0 && element.type.name === SFNodeType.heading && element.attrs.level === 1) {
                        startsWithHeading = true;
                    } else if (currentPartContent.length === 0 && element.type.name === SFNodeType.heading && element.attrs.level !== 1) {
                        startsWithHeading = false;
                    } else if (currentPartContent.length > 0 && element.type.name === SFNodeType.heading && element.attrs.level === 1) {
                        // start a new part
                        // todo make the schema dependent on whther it starts with a heading (startsWithHeading)
                        pushPart()
                    }
                    currentPartContent.push(element);
                }
            }
            if (currentPartContent.length > 0) {
                pushPart();
            }

            for (let partDoc of parts) {
                try {
                    partDoc.check()
                } catch (e: any) {
                    throw new Error("Invalid part document " + partDoc?.toString()?.substr(0, 50));
                }
            }
        } else {
            doc = schema.nodes[SFNodeType.document].create({}, header ? [header, ...content] : content);
            try {
                doc.check();
            } catch (e: any) {
                console.error('Failed to parse single part document with schema ' + schemaName, e);
                throw new Error("Invalid document: " + doc?.toString()?.substr(0, 50));
            }
        }

        return {
            title,
            subtitle,
            abstract,
            metaData: {},
            doc,
            parts,
            references,
            affiliations,
            referenceFiles,
            schema,
            errors
        };

    } catch (e: any) {
        console.error(e);
        return {
            title: null,
            subtitle: null,
            abstract: null,
            metaData: {},
            doc: null,
            referenceFiles: null,
            references: null,
            affilations: null,
            schema: null,
            errors: [...errors, e]
        }
    }
};

export {
    extractReferences, parsePandocAST,
    proseMirrorToHTML
};
