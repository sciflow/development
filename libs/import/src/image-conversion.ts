import { existsSync, readFileSync } from 'fs';
import mime from 'mime';
import { extname } from 'path';
import winston from 'winston';
const { LOG_LEVEL = 'info' } = process.env;
const logger = winston.createLogger({ format: winston.format.json(), transports: new winston.transports.Console({ level: LOG_LEVEL }), level: LOG_LEVEL });
const { TRANSFORM_IMAGE_URL } = process.env;

export const convertImage = async (buffer: string, filename: string): Promise<File | undefined> => {
    if (!TRANSFORM_IMAGE_URL || TRANSFORM_IMAGE_URL.length === 0) { return undefined; }
    // you can use any service here that accepts an image and returns a png (indicated by the accept headers)
    const requestFile = async () => {
        let response;

        try {
            const controller = new AbortController();
            const ext = extname(filename)?.replace('.', '');
            const id = setTimeout(() => {
                logger.warn('Could not transform file (timed out)', { filename });
                controller.abort();
            }, 60000);
            const formData = new FormData();
            formData.append('file', new Blob([buffer], { type: mime.getType(ext) as string }), 'input.' + ext);
            response = await fetch(TRANSFORM_IMAGE_URL, {
                method: 'POST',
                mode: 'no-cors',
                headers: {
                    Accept: 'image/png'
                },
                body: formData,
                signal: controller.signal
            });
            clearTimeout(id);
        } catch (e: any) {
            logger.warn('Could not complete request', { message: e.message, filename });
        }

        return response;
    }

    const ext = extname(filename)?.replace('.', '');
    switch (ext) {
        case 'emf':
            break;
        case 'wmf':
            break;
        default:
            return undefined;
    }

    const source = await requestFile();

    const content = await source?.blob();
    if (source?.status === 200) {
        const contentExt = mime.getExtension(content.type);
        const buffer = await content.arrayBuffer();
        return new File([buffer], 'output.' + contentExt, { type: content.type });
    } else {
        return undefined;
    }
}

export const convertImageFile = async (imagePath: string): Promise<File | undefined> => {
    if (!existsSync(imagePath)) { return undefined; }
    const file = readFileSync(imagePath, 'binary');
    return convertImage(file, imagePath);
}
