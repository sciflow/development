import { it, describe } from 'mocha';
import { expect } from 'chai';
import { readFileSync } from 'fs';
import { join } from 'path';

import { parsePandocAST } from './pandoc-to-pm';
import { SFNodeType, schemas } from '@sciflow/schema';
import { pandocRenderers, render, PandocNode } from './pandoc-renderers';

import { JSDOM } from 'jsdom';
const dom = new JSDOM('<!DOCTYPE html>');
import { DOMSerializer } from 'prosemirror-model';
const document = dom.window.document;

const simplePandocJSON = JSON.parse(readFileSync(join(__dirname, './fixtures/sample.pandoc.json'), 'utf-8'));
const pandocJSONTestFile = JSON.parse(readFileSync(join(__dirname, './fixtures/test-docx.json'), 'utf-8'));

/** Render a pandoc node as HTML by first converting it into a ProseMirror node,
 * which is then serialized as HTML. */
const renderAsHtml = async (pandocNode: PandocNode) => {
    const proseMirrorNode = await render(
        pandocNode,
        { renderers: pandocRenderers, schema: schemas.manuscript }
    );
    const serializer = DOMSerializer.fromSchema(schemas.manuscript);
    const htmlFrag = serializer.serializeNode(proseMirrorNode, { document }) as HTMLElement;
    return htmlFrag.outerHTML;
}

describe('Table importer', async () => {
    it('imports tables', async () => {
        const pandocTbl = {
            t: "Table",
            c: [
                ["", [], []],
                [null, [{ c: [{ c: "Caption", t: "Str" }], t: "Plain" }]],
                [[{ t: "AlignDefault" }, { t: "ColWidthDefault" }]
                ],
                [["", [], []],
                [[["", [], []],
                [[["", [], []], { "t": "AlignDefault" }, 1, 2,
                [{ c: [{ c: "Hello", t: "Str" }], t: "Plain" }]
                ]]]]],
                [[["", [], []], 0, [],
                [[["", [], []],
                [[["", [], []], { t: "AlignRight" }, 1, 1,
                [{ c: [{ c: "lovely", t: "Str" }], t: "Plain" }]
                ],
                [["", [], []], { "t": "AlignLeft" }, 1, 1,
                [{ c: [{ c: "World", t: "Str" }], t: "Plain" }]
                ]]
                ]]]],
                [["", [], []], []]
            ],
        };
        const expectedHtml = '<figure data-type="native-table" data-alt="" data-src="" data-orientation="portrait"><table><tbody><tr><th colspan="2"><p>Hello</p></th></tr><tr><td><p text-align="right" style="text-align: right">lovely</p></td><td><p text-align="left" style="text-align: left">World</p></td></tr></tbody></table><figcaption><p>Caption</p></figcaption></figure>';
        expect(await renderAsHtml(pandocTbl)).to.equal(expectedHtml);
    });

});
