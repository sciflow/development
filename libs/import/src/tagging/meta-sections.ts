import { getTextContent } from '@sciflow/export';
import { SFMarkType, SFNodeType } from '@sciflow/schema';
import { getKnownHeadings, getKnownStyles } from './heuristics';
import { JaroWinklerDistance } from 'natural';
import { addTagJSON, extractTextSizes, sizeFromPtString } from './helpers';

export const metaSections = (stats) => {
    return {
        tag: (doc) => {
            const sizes = extractTextSizes(stats.doc);
            const fontSizes = sizes.map(s => s.size);

            const tagKeywords = (node, parent?) => {
                if (node.type === SFNodeType.paragraph || node.type === SFNodeType.heading) {
                    let content = getTextContent(node);

                    // go through the stats document (a secondary import to extract details pandoc cannot capture (like styles))
                    const secondaryImportMatches: any = [];
                    if (content.length > 0) {
                        stats.doc.descendants((node, pos, parent) => {
                            if (node.type.name === 'paragraph' && node.textContent === content) {
                                secondaryImportMatches.push({ node, pos, parent });
                            }
                        });
                    }

                    const match = secondaryImportMatches[0];
                    let nthBiggestFont: number | undefined = undefined;
                    const nodeStyle = match?.node?.marks?.find(m => m.type.name === 'style')?.attrs;
                    if (nodeStyle?.size) {
                        const size = sizeFromPtString(nodeStyle?.size);
                        if (size) {
                            nthBiggestFont = fontSizes.findIndex(s => size >= s) + 1;
                        }
                    }

                    let mightBeHeading = node.type === SFNodeType.heading;
                    // word likes to use list items for chapter numbering
                    if (parent?.type === SFNodeType.list_item && parent.content.size === 1) { mightBeHeading = true; }

                    let marks: any[] = node.marks || [];

                    // try to match all texts that start with bold
                    if (node.content?.[0].marks?.some(m => m.type === SFMarkType.strong || m.type === SFMarkType.emphasis)) {
                        content = getTextContent(node.content[0]);
                        mightBeHeading = true;
                    }

                    if (mightBeHeading) {
                        let foundKnownHeading;
                        let dist = 0;
                        for (let test of getKnownHeadings()) {
                            const distance = JaroWinklerDistance(test.text, content, { ignoreCase: true });
                            if (distance > 0.9 && distance > dist) {
                                dist = distance;
                                foundKnownHeading = test;
                                marks = addTagJSON(marks, { key: test.key, value: { ...test, confidence: Math.round(distance * 1000) / 1000, nthBiggestFont, nodeStyle } });
                            }
                        }

                        if (!foundKnownHeading && secondaryImportMatches[0]) {
                            if (match?.node?.attrs?.classList) {
                                for (let name of match?.node?.attrs?.classList) {
                                    let dist = 0;
                                    for (let test of getKnownStyles()) {
                                        const distance = JaroWinklerDistance(test.text, name, { ignoreCase: true });
                                        // make sure we only replace the closest distances
                                        if (distance > 0.9 && distance > dist) {
                                            dist = distance;
                                            if (test.level >= 1) {
                                                foundKnownHeading = test;
                                                marks = addTagJSON(marks, { key: test.key, value: { ...test, confidence: Math.round(distance * 1000) / 1000, nthBiggestFont, nodeStyle } });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (marks.length > 0) { node.marks = marks; }
                }

                if (node.content) { node.content = node.content.map(c => tagKeywords(c, node)); }

                return node;
            }

            const tagAbstract = (node, parent?) => {
                return node;
            }

            doc = tagKeywords(doc);
            return tagAbstract(doc);
        },
        patch: () => {

        }
    }
}