interface KnownHeading {
    key: string;
    text: string;
    type: string;
    role?: string;
    numbering?: string;
    locale: string;
    distance?: number;
    level?: number;
  }

export const getKnownStyles = () => [
  { key: 'title', text: 'Title', type: 'title', label: 'title' },
  { key: 'title', text: 'Style Title', type: 'title', label: 'title' },
  { key: 'subtitle', text: 'Style Subtitle', type: 'subtitle', label: 'subtitle' },
  { key: 'authors', text: 'Authors', type: 'metaData', label: 'authors' },
  { key: 'abstract', text: 'Abstract', type: 'abstract', level: 1, numbering: 'none', label: 'abstract' },
  { key: 'corresponding', text: 'E-Mail', type: 'metaData', label: 'email' },
  { key: 'affiliations', text: 'Addresses', type: 'metaData', label: 'address' },
  { key: 'paragraph', text: 'Body text', type: 'paragraph', label: 'paragraph' },
  { key: 'paragraph', text: 'BodyText', type: 'paragraph', label: 'paragraph' },
  { key: 'paragraph', text: 'paragraph', type: 'paragraph', label: 'paragraph' },
  { key: 'caption', text: 'FigureCaption', type: undefined, label: 'caption' },
  { key: 'caption', text: 'Bildbeschriftung', type: undefined, label: 'caption' },
  { key: 'caption', text: 'FUP Abbildungsunterschrift', type: undefined, label: 'caption' },
  { key: 'caption', text: 'Abbildungsunterschrift', type: undefined, label: 'caption' },
  { key: 'caption', text: 'FUP Bild', type: undefined, label: 'figure-image' },
  { key: 'caption', text: 'Caption', type: undefined, label: 'caption' },
  { key: 'reference', text: 'Reference', type: undefined, label: 'reference' },
  { key: 'reference', text: 'References', type: undefined, label: 'reference' },
  { key: 'heading', text: 'Section', type: undefined, label: 'section', level: 1 },
  { key: 'heading', text: 'heading 1', type: 'chapter', label: 'heading', level: 1 },
  { key: 'heading', text: 'heading 2', type: undefined, label: 'heading', level: 2 },
  { key: 'heading', text: 'heading 3', type: undefined, label: 'heading', level: 3 },
  { key: 'heading', text: 'heading 4', type: undefined, label: 'heading', level: 4 },
  { key: 'heading', text: 'heading 5', type: undefined, label: 'heading', level: 5 },
  { key: 'heading', text: 'Section (no number)', type: undefined, level: 1, numbering: 'none', label: 'unumbered-section' },
  { key: 'equation', text: 'EQN', type: undefined, label: 'equation' },
] as any[];

export const getKnownHeadings = () => [
    { key: 'abstract', text: 'Abstract', type: 'abstract', numbering: 'none', locale: 'en-US', level: 1 },
    { key: 'keywords', text: 'Keywords', type: 'chapter', role: 'keywords', numbering: 'none', locale: 'en-US', level: 1 },
    { key: 'references', text: 'References', type: 'bibliography', locale: 'en-US', level: 1 },
    { key: 'bibliography', text: 'Bibliography', type: 'bibliography', locale: 'en-US', level: 1 },
    { key: 'introduction', text: 'Introduction', type: 'chapter', role: 'introduction', locale: 'en-US', level: 1 },
    { key: 'background', text: 'Background', type: 'chapter', role: 'background', locale: 'en-US', level: 1 },
    { key: 'bibliography', text: 'Literature Review', type: 'chapter', role: 'literature-review', locale: 'en-US', level: 1 },
    { key: 'methods', text: 'Methods', type: 'chapter', role: 'methods', locale: 'en-US', level: 1 },
    { key: 'methods', text: 'Methodology', type: 'chapter', role: 'methods', locale: 'en-US', level: 1 },
    { key: 'materials', text: 'Materials', type: 'chapter', role: 'materials', locale: 'en-US', level: 1 },
    { key: 'data', text: 'Data', type: 'chapter', role: 'data', locale: 'en-US', level: 1 },
    { key: 'results', text: 'Results', type: 'chapter', role: 'results', locale: 'en-US', level: 1 },
    { key: 'discussion', text: 'Discussion', type: 'chapter', role: 'discussion', locale: 'en-US', level: 1 },
    { key: 'conclusion', text: 'Conclusion', type: 'chapter', role: 'conclusion', locale: 'en-US', level: 1 },
    { key: 'conclusion', text: 'Conclusions and Future Work', type: 'chapter', role: 'conclusion', locale: 'en-US', level: 1 },
    { key: 'acknowledgements', text: 'Acknowledgements', type: 'chapter', role: 'acknowledgements', locale: 'en-US', level: 1 },
    { key: 'acknowledgements', text: 'Acknowledgments', type: 'chapter', role: 'acknowledgements', locale: 'en-US', level: 1 },
    { key: 'appendix', text: 'Appendix', type: 'chapter', role: 'appendix', numbering: 'alpha', locale: 'en-US', level: 1 },
    { key: 'supplemental-material', text: 'Supplementary Material', type: 'chapter', role: 'supplementary-material', locale: 'en-US', level: 1 },
    { key: 'figures-and-tables', text: 'Figures and Tables', type: 'chapter', role: 'figures-tables', locale: 'en-US', level: 1 },
    { key: 'glossary', text: 'Glossary', type: 'chapter', role: 'glossary', locale: 'en-US', level: 1 },
    { key: 'index', text: 'Index', type: 'chapter', role: 'index', locale: 'en-US', level: 1 },
    { key: 'abstract', text: 'Resumen', type: 'abstract', numbering: 'none', locale: 'es-ES', level: 1 },
    { key: 'keywords', text: 'Palabras clave', type: 'chapter', role: 'keywords', numbering: 'none', locale: 'es-ES', level: 1 },
    { key: 'introduction', text: 'Introducción', type: 'chapter', role: 'introduction', locale: 'es-ES', level: 1 },
    { key: 'methods', text: 'Metodología', type: 'chapter', role: 'methods', locale: 'es-ES', level: 1 },
    { key: 'results', text: 'Resultados', type: 'chapter', role: 'results', locale: 'es-ES', level: 1 },
    { key: 'discussion', text: 'Discusión', type: 'chapter', role: 'discussion', locale: 'es-ES', level: 1 },
    { key: 'conclusion', text: 'Conclusiones', type: 'chapter', role: 'conclusion', locale: 'es-ES', level: 1 },
    { key: 'acknowledgements', text: 'Agradecimientos', type: 'chapter', role: 'acknowledgements', locale: 'es-ES', level: 1 },
    { key: 'appendix', text: 'Apéndice', type: 'chapter', role: 'appendix', numbering: 'alpha', locale: 'es-ES', level: 1 },
    { key: 'bibliography', text: 'Referencias', type: 'bibliography', locale: 'es-ES', level: 1 },
    { key: 'abstract', text: 'Zusammenfassung', type: 'abstract', numbering: 'none', locale: 'de-DE', level: 1 },
    { key: 'keywords', text: 'Schlüsselwörter', type: 'chapter', role: 'keywords', numbering: 'none', locale: 'de-DE', level: 1 },
    { key: 'introduction', text: 'Einleitung', type: 'chapter', role: 'introduction', locale: 'de-DE', level: 1 },
    { key: 'methods', text: 'Methodik', type: 'chapter', role: 'methods', locale: 'de-DE', level: 1 },
    { key: 'results', text: 'Ergebnisse', type: 'chapter', role: 'results', locale: 'de-DE', level: 1 },
    { key: 'discussion', text: 'Diskussion', type: 'chapter', role: 'discussion', locale: 'de-DE', level: 1 },
    { key: 'conclusion', text: 'Schlussfolgerungen', type: 'chapter', role: 'conclusion', locale: 'de-DE', level: 1 },
    { key: 'acknowledgements', text: 'Danksagungen', type: 'chapter', role: 'acknowledgements', locale: 'de-DE', level: 1 },
    { key: 'appendix', text: 'Anhang', type: 'chapter', role: 'appendix', numbering: 'alpha', locale: 'de-DE', level: 1 },
    { key: 'bibliography', text: 'Literaturverzeichnis', type: 'bibliography', locale: 'de-DE', level: 1 },

    // Conflicts of Interest and Funding Statement for each locale
    { key: 'conflicts-of-interest', text: 'Conflicts of Interest', type: 'chapter', role: 'conflicts-of-interest', numbering: 'none', locale: 'en-US', level: 1 },
    { key: 'TOfunding-statementDO', text: 'Funding Statement', type: 'chapter', role: 'funding-statement', numbering: 'none', locale: 'en-US', level: 1 },
    
    { key: 'conflicts-of-interest', text: 'Conflictos de Interés', type: 'chapter', role: 'conflicts-of-interest', numbering: 'none', locale: 'es-ES', level: 1 },
    { key: 'funding-statement', text: 'Declaración de Financiación', type: 'chapter', role: 'funding-statement', numbering: 'none', locale: 'es-ES', level: 1 },
    
    { key: 'conflicts-of-interest', text: 'Interessenkonflikte', type: 'chapter', role: 'conflicts-of-interest', numbering: 'none', locale: 'de-DE', level: 1 },
    { key: 'funding-statement', text: 'Förderungserklärung', type: 'chapter', role: 'funding-statement', numbering: 'none', locale: 'de-DE', level: 1 },
] as KnownHeading[];