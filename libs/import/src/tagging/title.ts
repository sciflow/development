import { getTextContent } from '@sciflow/export';
import { SFMarkType, SFNodeType } from '@sciflow/schema';
import { addTagJSON, extractTextSizes } from './helpers';

export const tagTitle = (stats) => {
    return {
        tag: (doc) => {
            let biggestNode, biggestNodeText;
            if (stats?.doc) {
                const sizes = extractTextSizes(stats.doc);
                biggestNode = sizes[0];
                biggestNodeText = biggestNode?.node?.textContent;
            }
            if (!doc.content) { return doc; }
            for (let paragraphOrHeading of doc.content.slice(0, 15)) {
                if (paragraphOrHeading.type === SFNodeType.paragraph || paragraphOrHeading.type === SFNodeType.heading || paragraphOrHeading.type === SFNodeType.header) {
                    const textContent = getTextContent(paragraphOrHeading);
                    // try to find the biggest node first
                    if (biggestNode && textContent == biggestNodeText) {
                        if (!paragraphOrHeading.marks) { paragraphOrHeading.marks = []; }
                        paragraphOrHeading.marks = addTagJSON(paragraphOrHeading.marks, { key: 'title' });
                    }

                    // match nodes that are only bold
                    if (paragraphOrHeading.marks?.some(m => m.type === SFMarkType.strong)) {
                        if (!paragraphOrHeading.marks) { paragraphOrHeading.marks = []; }
                        paragraphOrHeading.marks = addTagJSON(paragraphOrHeading.marks, { key: 'title' });
                    }

                    // starts and ends with strong
                    const content = paragraphOrHeading.content?.filter(n => n.text && n.text.trim()?.length > 0);
                    if (content?.[0]?.marks?.some(m => m.type === SFMarkType.strong) && content?.[content?.length - 1].marks?.some(m => m.type === SFMarkType.strong)) {
                        if (!paragraphOrHeading.marks) { paragraphOrHeading.marks = []; }
                        paragraphOrHeading.marks = addTagJSON(paragraphOrHeading.marks, { key: 'title' });
                    }
                }
            }

            return doc;
        },
        patch: () => {

        }
    }
}