import { SFMarkType } from '@sciflow/schema';
import { Mark, Schema, Node } from 'prosemirror-model';

interface Tag {
    key: string;
    value?: any;
}

export const addTags = (marks: Mark[], tags: Tag[], opts: { schema: Schema }) => {
    let attrs: { tags?: Tag[] } = marks.find(m => m.type.name === SFMarkType.tags)?.attrs || {};
    if (!attrs.tags) { attrs.tags = []; }
    // replace a tag by the same key
    attrs.tags = [...attrs.tags.filter(t => !tags.some(t2 => t2.key === t.key)), ...tags];
    const tagMark = opts?.schema.marks[SFMarkType.tags].create(attrs);

    return [
        ...marks.filter(m => m.type.name !== SFMarkType.tags),
        tagMark
    ];
}
export const addTag = (marks: Mark[], tag: Tag, opts: { schema: Schema }) => addTags(marks, [tag], opts);

export const addTagsJSON = (marks: any[], tags: Tag[]) => {
    let existingTagsMark = marks.find(m => m.type === SFMarkType.tags);
    let attrs: { tags: Tag[] } = existingTagsMark?.attrs || { tags: [] };
    if (!attrs.tags) { attrs.tags = []; }

    for (let { key, value } of tags) {
        const existingIndex = attrs.tags.findIndex(tag => tag.key === key);
        if (existingIndex > -1) {
            attrs.tags[existingIndex] = { key, value };
        } else {
            attrs.tags.push({ key, value });
        }
    }

    return [
        ...marks.filter(m => m.type !== SFMarkType.tags),
        { type: SFMarkType.tags, attrs }
    ];
}
export const addTagJSON = (marks: any[], tag: any) => addTagsJSON(marks, [tag]);

export const sizeFromPtString = (s: string) => s?.match(/(\d+)pt/)?.[0];

export const extractTextSizes = (doc: Node) => {
    let sizes: any = {};

    doc.descendants((node, pos, parent) => {
        const styleMark = node?.marks.find(m => m.type.name === 'style');
        if (styleMark) {
            const size = styleMark.attrs?.size?.match(/(\d+)pt/);
            if (size?.[1]) {
                if (!sizes[size?.[1]]) {
                    sizes[size?.[1]] = { node, pos, parent, size: size?.[1] };
                }
            }
        }
    });

    return Object.keys(sizes).map(size => ({ size, ...sizes[size] })).sort((a, b) => b?.size - a?.size);
};