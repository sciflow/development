import { it, describe } from 'mocha';
import { expect } from 'chai';
import { readFileSync, writeFileSync } from 'fs';
import { join } from 'path';
import { pandocRenderers, renderContent } from './pandoc-renderers';
import { SFNodeType } from '@sciflow/schema';
import { schemas } from '@sciflow/schema';
import { parsePandocAST } from './pandoc-to-pm';

import { JSDOM } from 'jsdom';
const dom = new JSDOM('<!DOCTYPE html>');
import { DOMSerializer } from 'prosemirror-model';
const document = dom.window.document;

const structureJSON = JSON.parse(readFileSync(join(__dirname, './fixtures/structure.pandoc.json'), 'utf-8'));

describe('Importer', async () => {
    it('Parses the document structure and numbering', async () => {
        const { parts } = await parsePandocAST(structureJSON, { renderStandalone: true });
        expect(parts.length).to.equal(4);
        const abstract = parts[0].toJSON();
        expect(abstract.content[0].type).to.equal(SFNodeType.heading);
        expect(abstract.attrs?.numbering).to.equal('none');

        const methods = parts[2].toJSON();
        expect(methods.content[0].type).to.equal(SFNodeType.heading);
        expect(methods.attrs?.numbering).to.equal(undefined);

        const h1 = methods.content[0];
        const h2 = methods.content[2];
        
        
        expect(h1.content[0].text).to.equal('Methods');
        expect(h1.attrs?.level).to.equal(1);
        expect(h2.content[0].text).to.equal('A sub-chapter in methods');
        expect(h2.attrs?.level).to.equal(2);
    });
});