<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<w:styles xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
    xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
    xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
    xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
    xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
    xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
    xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
    xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
    xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
    mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh">
    <w:docDefaults>
        <w:rPrDefault>
            <w:rPr>
                <w:rFonts w:asciiTheme="minorHAnsi" w:eastAsiaTheme="minorHAnsi"
                    w:hAnsiTheme="minorHAnsi" w:cstheme="minorBidi" />
                <w:kern w:val="2" />
                <w:sz w:val="24" />
                <w:szCs w:val="24" />
                <w:lang w:val="en-DE" w:eastAsia="en-US" w:bidi="ar-SA" />
                <w14:ligatures w14:val="standardContextual" />
            </w:rPr>
        </w:rPrDefault>
        <w:pPrDefault />
    </w:docDefaults>
    <w:latentStyles w:defLockedState="0" w:defUIPriority="99" w:defSemiHidden="0"
        w:defUnhideWhenUsed="0" w:defQFormat="0" w:count="376">
        <w:lsdException w:name="Normal" w:uiPriority="0" w:qFormat="1" />
        <w:lsdException w:name="heading 1" w:uiPriority="9" w:qFormat="1" />
        <w:lsdException w:name="heading 2" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
            w:qFormat="1" />
        <w:lsdException w:name="Smart Link" w:semiHidden="1" w:unhideWhenUsed="1" />
    </w:latentStyles>
    <w:style w:type="paragraph" w:default="1" w:styleId="Normal">
        <w:name w:val="Normal" />
        <w:qFormat />
    </w:style>
    <w:style w:type="paragraph" w:styleId="Heading1">
        <w:name w:val="heading 1" />
        <w:basedOn w:val="Normal" />
        <w:next w:val="Normal" />
        <w:link w:val="Heading1Char" />
        <w:uiPriority w:val="9" />
        <w:qFormat />
        <w:rsid w:val="00C549E3" />
        <w:pPr>
            <w:keepNext />
            <w:keepLines />
            <w:spacing w:before="360" w:after="80" />
            <w:outlineLvl w:val="0" />
        </w:pPr>
        <w:rPr>
            <w:rFonts w:asciiTheme="majorHAnsi" w:eastAsiaTheme="majorEastAsia"
                w:hAnsiTheme="majorHAnsi" w:cstheme="majorBidi" />
            <w:color w:val="0F4761" w:themeColor="accent1" w:themeShade="BF" />
            <w:sz w:val="28" />
            <w:szCs w:val="40" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="Heading2">
        <w:name w:val="heading 2" />
        <w:basedOn w:val="Normal" />
        <w:next w:val="Normal" />
        <w:link w:val="Heading2Char" />
        <w:uiPriority w:val="9" />
        <w:semiHidden />
        <w:unhideWhenUsed />
        <w:qFormat />
        <w:rsid w:val="00C549E3" />
        <w:pPr>
            <w:keepNext />
            <w:keepLines />
            <w:spacing w:before="160" w:after="80" />
            <w:outlineLvl w:val="1" />
        </w:pPr>
        <w:rPr>
            <w:rFonts w:asciiTheme="majorHAnsi" w:eastAsiaTheme="majorEastAsia"
                w:hAnsiTheme="majorHAnsi" w:cstheme="majorBidi" />
            <w:color w:val="0F4761" w:themeColor="accent1" w:themeShade="BF" />
            <w:sz w:val="32" />
            <w:szCs w:val="32" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="Heading3">
        <w:name w:val="heading 3" />
        <w:basedOn w:val="Normal" />
        <w:next w:val="Normal" />
        <w:link w:val="Heading3Char" />
        <w:uiPriority w:val="9" />
        <w:semiHidden />
        <w:unhideWhenUsed />
        <w:qFormat />
        <w:rsid w:val="00C549E3" />
        <w:pPr>
            <w:keepNext />
            <w:keepLines />
            <w:spacing w:before="160" w:after="80" />
            <w:outlineLvl w:val="2" />
        </w:pPr>
        <w:rPr>
            <w:rFonts w:eastAsiaTheme="majorEastAsia" w:cstheme="majorBidi" />
            <w:color w:val="0F4761" w:themeColor="accent1" w:themeShade="BF" />
            <w:sz w:val="28" />
            <w:szCs w:val="28" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="Heading4">
        <w:name w:val="heading 4" />
        <w:basedOn w:val="Normal" />
        <w:next w:val="Normal" />
        <w:link w:val="Heading4Char" />
        <w:uiPriority w:val="9" />
        <w:semiHidden />
        <w:unhideWhenUsed />
        <w:qFormat />
        <w:rsid w:val="00C549E3" />
        <w:pPr>
            <w:keepNext />
            <w:keepLines />
            <w:spacing w:before="80" w:after="40" />
            <w:outlineLvl w:val="3" />
        </w:pPr>
        <w:rPr>
            <w:rFonts w:eastAsiaTheme="majorEastAsia" w:cstheme="majorBidi" />
            <w:i />
            <w:iCs />
            <w:color w:val="0F4761" w:themeColor="accent1" w:themeShade="BF" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="Heading5">
        <w:name w:val="heading 5" />
        <w:basedOn w:val="Normal" />
        <w:next w:val="Normal" />
        <w:link w:val="Heading5Char" />
        <w:uiPriority w:val="9" />
        <w:semiHidden />
        <w:unhideWhenUsed />
        <w:qFormat />
        <w:rsid w:val="00C549E3" />
        <w:pPr>
            <w:keepNext />
            <w:keepLines />
            <w:spacing w:before="80" w:after="40" />
            <w:outlineLvl w:val="4" />
        </w:pPr>
        <w:rPr>
            <w:rFonts w:eastAsiaTheme="majorEastAsia" w:cstheme="majorBidi" />
            <w:color w:val="0F4761" w:themeColor="accent1" w:themeShade="BF" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="Heading6">
        <w:name w:val="heading 6" />
        <w:basedOn w:val="Normal" />
        <w:next w:val="Normal" />
        <w:link w:val="Heading6Char" />
        <w:uiPriority w:val="9" />
        <w:semiHidden />
        <w:unhideWhenUsed />
        <w:qFormat />
        <w:rsid w:val="00C549E3" />
        <w:pPr>
            <w:keepNext />
            <w:keepLines />
            <w:spacing w:before="40" />
            <w:outlineLvl w:val="5" />
        </w:pPr>
        <w:rPr>
            <w:rFonts w:eastAsiaTheme="majorEastAsia" w:cstheme="majorBidi" />
            <w:i />
            <w:iCs />
            <w:color w:val="595959" w:themeColor="text1" w:themeTint="A6" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="Heading7">
        <w:name w:val="heading 7" />
        <w:basedOn w:val="Normal" />
        <w:next w:val="Normal" />
        <w:link w:val="Heading7Char" />
        <w:uiPriority w:val="9" />
        <w:semiHidden />
        <w:unhideWhenUsed />
        <w:qFormat />
        <w:rsid w:val="00C549E3" />
        <w:pPr>
            <w:keepNext />
            <w:keepLines />
            <w:spacing w:before="40" />
            <w:outlineLvl w:val="6" />
        </w:pPr>
        <w:rPr>
            <w:rFonts w:eastAsiaTheme="majorEastAsia" w:cstheme="majorBidi" />
            <w:color w:val="595959" w:themeColor="text1" w:themeTint="A6" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="Heading8">
        <w:name w:val="heading 8" />
        <w:basedOn w:val="Normal" />
        <w:next w:val="Normal" />
        <w:link w:val="Heading8Char" />
        <w:uiPriority w:val="9" />
        <w:semiHidden />
        <w:unhideWhenUsed />
        <w:qFormat />
        <w:rsid w:val="00C549E3" />
        <w:pPr>
            <w:keepNext />
            <w:keepLines />
            <w:outlineLvl w:val="7" />
        </w:pPr>
        <w:rPr>
            <w:rFonts w:eastAsiaTheme="majorEastAsia" w:cstheme="majorBidi" />
            <w:i />
            <w:iCs />
            <w:color w:val="272727" w:themeColor="text1" w:themeTint="D8" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="Heading9">
        <w:name w:val="heading 9" />
        <w:basedOn w:val="Normal" />
        <w:next w:val="Normal" />
        <w:link w:val="Heading9Char" />
        <w:uiPriority w:val="9" />
        <w:semiHidden />
        <w:unhideWhenUsed />
        <w:qFormat />
        <w:rsid w:val="00C549E3" />
        <w:pPr>
            <w:keepNext />
            <w:keepLines />
            <w:outlineLvl w:val="8" />
        </w:pPr>
        <w:rPr>
            <w:rFonts w:eastAsiaTheme="majorEastAsia" w:cstheme="majorBidi" />
            <w:color w:val="272727" w:themeColor="text1" w:themeTint="D8" />
        </w:rPr>
    </w:style>
    <w:style w:type="character" w:default="1" w:styleId="DefaultParagraphFont">
        <w:name w:val="Default Paragraph Font" />
        <w:uiPriority w:val="1" />
        <w:semiHidden />
        <w:unhideWhenUsed />
    </w:style>
    <w:style w:type="table" w:default="1" w:styleId="TableNormal">
        <w:name w:val="Normal Table" />
        <w:uiPriority w:val="99" />
        <w:semiHidden />
        <w:unhideWhenUsed />
        <w:tblPr>
            <w:tblInd w:w="0" w:type="dxa" />
            <w:tblCellMar>
                <w:top w:w="0" w:type="dxa" />
                <w:left w:w="108" w:type="dxa" />
                <w:bottom w:w="0" w:type="dxa" />
                <w:right w:w="108" w:type="dxa" />
            </w:tblCellMar>
        </w:tblPr>
    </w:style>
    <w:style w:type="numbering" w:default="1" w:styleId="NoList">
        <w:name w:val="No List" />
        <w:uiPriority w:val="99" />
        <w:semiHidden />
        <w:unhideWhenUsed />
    </w:style>
    <w:style w:type="character" w:customStyle="1" w:styleId="Heading1Char">
        <w:name w:val="Heading 1 Char" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:link w:val="Heading1" />
        <w:uiPriority w:val="9" />
        <w:rsid w:val="00C549E3" />
        <w:rPr>
            <w:rFonts w:asciiTheme="majorHAnsi" w:eastAsiaTheme="majorEastAsia"
                w:hAnsiTheme="majorHAnsi" w:cstheme="majorBidi" />
            <w:color w:val="0F4761" w:themeColor="accent1" w:themeShade="BF" />
            <w:sz w:val="28" />
            <w:szCs w:val="40" />
        </w:rPr>
    </w:style>
    <w:style w:type="character" w:customStyle="1" w:styleId="Heading2Char">
        <w:name w:val="Heading 2 Char" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:link w:val="Heading2" />
        <w:uiPriority w:val="9" />
        <w:semiHidden />
        <w:rsid w:val="00C549E3" />
        <w:rPr>
            <w:rFonts w:asciiTheme="majorHAnsi" w:eastAsiaTheme="majorEastAsia"
                w:hAnsiTheme="majorHAnsi" w:cstheme="majorBidi" />
            <w:color w:val="0F4761" w:themeColor="accent1" w:themeShade="BF" />
            <w:sz w:val="32" />
            <w:szCs w:val="32" />
        </w:rPr>
    </w:style>
    <w:style w:type="character" w:customStyle="1" w:styleId="Heading3Char">
        <w:name w:val="Heading 3 Char" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:link w:val="Heading3" />
        <w:uiPriority w:val="9" />
        <w:semiHidden />
        <w:rsid w:val="00C549E3" />
        <w:rPr>
            <w:rFonts w:eastAsiaTheme="majorEastAsia" w:cstheme="majorBidi" />
            <w:color w:val="0F4761" w:themeColor="accent1" w:themeShade="BF" />
            <w:sz w:val="28" />
            <w:szCs w:val="28" />
        </w:rPr>
    </w:style>
    <w:style w:type="character" w:customStyle="1" w:styleId="Heading4Char">
        <w:name w:val="Heading 4 Char" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:link w:val="Heading4" />
        <w:uiPriority w:val="9" />
        <w:semiHidden />
        <w:rsid w:val="00C549E3" />
        <w:rPr>
            <w:rFonts w:eastAsiaTheme="majorEastAsia" w:cstheme="majorBidi" />
            <w:i />
            <w:iCs />
            <w:color w:val="0F4761" w:themeColor="accent1" w:themeShade="BF" />
        </w:rPr>
    </w:style>
    <w:style w:type="character" w:customStyle="1" w:styleId="Heading5Char">
        <w:name w:val="Heading 5 Char" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:link w:val="Heading5" />
        <w:uiPriority w:val="9" />
        <w:semiHidden />
        <w:rsid w:val="00C549E3" />
        <w:rPr>
            <w:rFonts w:eastAsiaTheme="majorEastAsia" w:cstheme="majorBidi" />
            <w:color w:val="0F4761" w:themeColor="accent1" w:themeShade="BF" />
        </w:rPr>
    </w:style>
    <w:style w:type="character" w:customStyle="1" w:styleId="Heading6Char">
        <w:name w:val="Heading 6 Char" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:link w:val="Heading6" />
        <w:uiPriority w:val="9" />
        <w:semiHidden />
        <w:rsid w:val="00C549E3" />
        <w:rPr>
            <w:rFonts w:eastAsiaTheme="majorEastAsia" w:cstheme="majorBidi" />
            <w:i />
            <w:iCs />
            <w:color w:val="595959" w:themeColor="text1" w:themeTint="A6" />
        </w:rPr>
    </w:style>
    <w:style w:type="character" w:customStyle="1" w:styleId="Heading7Char">
        <w:name w:val="Heading 7 Char" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:link w:val="Heading7" />
        <w:uiPriority w:val="9" />
        <w:semiHidden />
        <w:rsid w:val="00C549E3" />
        <w:rPr>
            <w:rFonts w:eastAsiaTheme="majorEastAsia" w:cstheme="majorBidi" />
            <w:color w:val="595959" w:themeColor="text1" w:themeTint="A6" />
        </w:rPr>
    </w:style>
    <w:style w:type="character" w:customStyle="1" w:styleId="Heading8Char">
        <w:name w:val="Heading 8 Char" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:link w:val="Heading8" />
        <w:uiPriority w:val="9" />
        <w:semiHidden />
        <w:rsid w:val="00C549E3" />
        <w:rPr>
            <w:rFonts w:eastAsiaTheme="majorEastAsia" w:cstheme="majorBidi" />
            <w:i />
            <w:iCs />
            <w:color w:val="272727" w:themeColor="text1" w:themeTint="D8" />
        </w:rPr>
    </w:style>
    <w:style w:type="character" w:customStyle="1" w:styleId="Heading9Char">
        <w:name w:val="Heading 9 Char" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:link w:val="Heading9" />
        <w:uiPriority w:val="9" />
        <w:semiHidden />
        <w:rsid w:val="00C549E3" />
        <w:rPr>
            <w:rFonts w:eastAsiaTheme="majorEastAsia" w:cstheme="majorBidi" />
            <w:color w:val="272727" w:themeColor="text1" w:themeTint="D8" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="Title">
        <w:name w:val="Title" />
        <w:basedOn w:val="Normal" />
        <w:next w:val="Normal" />
        <w:link w:val="TitleChar" />
        <w:uiPriority w:val="10" />
        <w:qFormat />
        <w:rsid w:val="00A321B4" />
        <w:pPr>
            <w:spacing w:before="400" w:after="80" />
            <w:contextualSpacing />
            <w:pPrChange w:id="0" w:author="Frederik Eichler" w:date="2024-04-29T14:46:00Z">
                <w:pPr>
                    <w:spacing w:after="80" />
                    <w:contextualSpacing />
                </w:pPr>
            </w:pPrChange>
        </w:pPr>
        <w:rPr>
            <w:rFonts w:asciiTheme="majorHAnsi" w:eastAsiaTheme="majorEastAsia"
                w:hAnsiTheme="majorHAnsi" w:cstheme="majorBidi" />
            <w:spacing w:val="-10" />
            <w:kern w:val="28" />
            <w:sz w:val="36" />
            <w:szCs w:val="56" />
            <w:rPrChange w:id="0" w:author="Frederik Eichler" w:date="2024-04-29T14:46:00Z">
                <w:rPr>
                    <w:rFonts w:asciiTheme="majorHAnsi" w:eastAsiaTheme="majorEastAsia"
                        w:hAnsiTheme="majorHAnsi" w:cstheme="majorBidi" />
                    <w:spacing w:val="-10" />
                    <w:kern w:val="28" />
                    <w:sz w:val="36" />
                    <w:szCs w:val="56" />
                    <w:lang w:val="en-DE" w:eastAsia="en-US" w:bidi="ar-SA" />
                    <w14:ligatures w14:val="standardContextual" />
                </w:rPr>
            </w:rPrChange>
        </w:rPr>
    </w:style>
    <w:style w:type="character" w:customStyle="1" w:styleId="TitleChar">
        <w:name w:val="Title Char" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:link w:val="Title" />
        <w:uiPriority w:val="10" />
        <w:rsid w:val="00A321B4" />
        <w:rPr>
            <w:rFonts w:asciiTheme="majorHAnsi" w:eastAsiaTheme="majorEastAsia"
                w:hAnsiTheme="majorHAnsi" w:cstheme="majorBidi" />
            <w:spacing w:val="-10" />
            <w:kern w:val="28" />
            <w:sz w:val="36" />
            <w:szCs w:val="56" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="Subtitle">
        <w:name w:val="Subtitle" />
        <w:basedOn w:val="Normal" />
        <w:next w:val="Normal" />
        <w:link w:val="SubtitleChar" />
        <w:uiPriority w:val="11" />
        <w:qFormat />
        <w:rsid w:val="00C549E3" />
        <w:pPr>
            <w:numPr>
                <w:ilvl w:val="1" />
            </w:numPr>
            <w:spacing w:after="160" />
        </w:pPr>
        <w:rPr>
            <w:rFonts w:eastAsiaTheme="majorEastAsia" w:cstheme="majorBidi" />
            <w:color w:val="595959" w:themeColor="text1" w:themeTint="A6" />
            <w:spacing w:val="15" />
            <w:sz w:val="28" />
            <w:szCs w:val="28" />
        </w:rPr>
    </w:style>
    <w:style w:type="character" w:customStyle="1" w:styleId="SubtitleChar">
        <w:name w:val="Subtitle Char" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:link w:val="Subtitle" />
        <w:uiPriority w:val="11" />
        <w:rsid w:val="00C549E3" />
        <w:rPr>
            <w:rFonts w:eastAsiaTheme="majorEastAsia" w:cstheme="majorBidi" />
            <w:color w:val="595959" w:themeColor="text1" w:themeTint="A6" />
            <w:spacing w:val="15" />
            <w:sz w:val="28" />
            <w:szCs w:val="28" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="Quote">
        <w:name w:val="Quote" />
        <w:basedOn w:val="Normal" />
        <w:next w:val="Normal" />
        <w:link w:val="QuoteChar" />
        <w:uiPriority w:val="29" />
        <w:qFormat />
        <w:rsid w:val="00C549E3" />
        <w:pPr>
            <w:spacing w:before="160" w:after="160" />
            <w:jc w:val="center" />
        </w:pPr>
        <w:rPr>
            <w:i />
            <w:iCs />
            <w:color w:val="404040" w:themeColor="text1" w:themeTint="BF" />
        </w:rPr>
    </w:style>
    <w:style w:type="character" w:customStyle="1" w:styleId="QuoteChar">
        <w:name w:val="Quote Char" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:link w:val="Quote" />
        <w:uiPriority w:val="29" />
        <w:rsid w:val="00C549E3" />
        <w:rPr>
            <w:i />
            <w:iCs />
            <w:color w:val="404040" w:themeColor="text1" w:themeTint="BF" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="ListParagraph">
        <w:name w:val="List Paragraph" />
        <w:basedOn w:val="Normal" />
        <w:uiPriority w:val="34" />
        <w:qFormat />
        <w:rsid w:val="00C549E3" />
        <w:pPr>
            <w:ind w:left="720" />
            <w:contextualSpacing />
        </w:pPr>
    </w:style>
    <w:style w:type="character" w:styleId="IntenseEmphasis">
        <w:name w:val="Intense Emphasis" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:uiPriority w:val="21" />
        <w:qFormat />
        <w:rsid w:val="00C549E3" />
        <w:rPr>
            <w:i />
            <w:iCs />
            <w:color w:val="0F4761" w:themeColor="accent1" w:themeShade="BF" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="IntenseQuote">
        <w:name w:val="Intense Quote" />
        <w:basedOn w:val="Normal" />
        <w:next w:val="Normal" />
        <w:link w:val="IntenseQuoteChar" />
        <w:uiPriority w:val="30" />
        <w:qFormat />
        <w:rsid w:val="00C549E3" />
        <w:pPr>
            <w:pBdr>
                <w:top w:val="single" w:sz="4" w:space="10" w:color="0F4761" w:themeColor="accent1"
                    w:themeShade="BF" />
                <w:bottom w:val="single" w:sz="4" w:space="10" w:color="0F4761"
                    w:themeColor="accent1" w:themeShade="BF" />
            </w:pBdr>
            <w:spacing w:before="360" w:after="360" />
            <w:ind w:left="864" w:right="864" />
            <w:jc w:val="center" />
        </w:pPr>
        <w:rPr>
            <w:i />
            <w:iCs />
            <w:color w:val="0F4761" w:themeColor="accent1" w:themeShade="BF" />
        </w:rPr>
    </w:style>
    <w:style w:type="character" w:customStyle="1" w:styleId="IntenseQuoteChar">
        <w:name w:val="Intense Quote Char" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:link w:val="IntenseQuote" />
        <w:uiPriority w:val="30" />
        <w:rsid w:val="00C549E3" />
        <w:rPr>
            <w:i />
            <w:iCs />
            <w:color w:val="0F4761" w:themeColor="accent1" w:themeShade="BF" />
        </w:rPr>
    </w:style>
    <w:style w:type="character" w:styleId="IntenseReference">
        <w:name w:val="Intense Reference" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:uiPriority w:val="32" />
        <w:qFormat />
        <w:rsid w:val="00C549E3" />
        <w:rPr>
            <w:b />
            <w:bCs />
            <w:smallCaps />
            <w:color w:val="0F4761" w:themeColor="accent1" w:themeShade="BF" />
            <w:spacing w:val="5" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:customStyle="1" w:styleId="Sectiontitle">
        <w:name w:val="Section title" />
        <w:basedOn w:val="Heading1" />
        <w:qFormat />
        <w:rsid w:val="00484C46" />
        <w:pPr>
            <w:numPr>
                <w:numId w:val="2" />
            </w:numPr>
            <w:spacing w:before="720" />
            <w:ind w:left="0" w:firstLine="0" />
        </w:pPr>
        <w:rPr>
            <w:color w:val="auto" />
            <w:lang w:val="en-US" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:customStyle="1" w:styleId="Reference">
        <w:name w:val="Reference" />
        <w:basedOn w:val="Normal" />
        <w:qFormat />
        <w:rsid w:val="00C736AB" />
        <w:pPr>
            <w:ind w:left="567" w:hanging="567" />
        </w:pPr>
    </w:style>
    <w:style w:type="table" w:styleId="TableGrid">
        <w:name w:val="Table Grid" />
        <w:basedOn w:val="TableNormal" />
        <w:uiPriority w:val="39" />
        <w:rsid w:val="009359DA" />
        <w:tblPr>
            <w:tblBorders>
                <w:top w:val="single" w:sz="4" w:space="0" w:color="auto" />
                <w:left w:val="single" w:sz="4" w:space="0" w:color="auto" />
                <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto" />
                <w:right w:val="single" w:sz="4" w:space="0" w:color="auto" />
                <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto" />
                <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto" />
            </w:tblBorders>
        </w:tblPr>
    </w:style>
    <w:style w:type="character" w:styleId="PlaceholderText">
        <w:name w:val="Placeholder Text" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:uiPriority w:val="99" />
        <w:semiHidden />
        <w:rsid w:val="009359DA" />
        <w:rPr>
            <w:color w:val="666666" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="Caption">
        <w:name w:val="caption" />
        <w:basedOn w:val="Normal" />
        <w:next w:val="Normal" />
        <w:uiPriority w:val="35" />
        <w:unhideWhenUsed />
        <w:qFormat />
        <w:rsid w:val="009359DA" />
        <w:pPr>
            <w:spacing w:after="200" />
        </w:pPr>
        <w:rPr>
            <w:i />
            <w:iCs />
            <w:color w:val="0E2841" w:themeColor="text2" />
            <w:sz w:val="18" />
            <w:szCs w:val="18" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:customStyle="1" w:styleId="FigCaption">
        <w:name w:val="FigCaption" />
        <w:basedOn w:val="Normal" />
        <w:qFormat />
        <w:rsid w:val="00E0568A" />
        <w:pPr>
            <w:keepNext />
            <w:ind w:left="567" />
        </w:pPr>
        <w:rPr>
            <w:sz w:val="20" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="Revision">
        <w:name w:val="Revision" />
        <w:hidden />
        <w:uiPriority w:val="99" />
        <w:semiHidden />
        <w:rsid w:val="00773ABA" />
    </w:style>
    <w:style w:type="character" w:styleId="CommentReference">
        <w:name w:val="annotation reference" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:uiPriority w:val="99" />
        <w:semiHidden />
        <w:unhideWhenUsed />
        <w:rsid w:val="0031027D" />
        <w:rPr>
            <w:sz w:val="16" />
            <w:szCs w:val="16" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="CommentText">
        <w:name w:val="annotation text" />
        <w:basedOn w:val="Normal" />
        <w:link w:val="CommentTextChar" />
        <w:uiPriority w:val="99" />
        <w:semiHidden />
        <w:unhideWhenUsed />
        <w:rsid w:val="0031027D" />
        <w:rPr>
            <w:sz w:val="20" />
            <w:szCs w:val="20" />
        </w:rPr>
    </w:style>
    <w:style w:type="character" w:customStyle="1" w:styleId="CommentTextChar">
        <w:name w:val="Comment Text Char" />
        <w:basedOn w:val="DefaultParagraphFont" />
        <w:link w:val="CommentText" />
        <w:uiPriority w:val="99" />
        <w:semiHidden />
        <w:rsid w:val="0031027D" />
        <w:rPr>
            <w:sz w:val="20" />
            <w:szCs w:val="20" />
        </w:rPr>
    </w:style>
    <w:style w:type="paragraph" w:styleId="CommentSubject">
        <w:name w:val="annotation subject" />
        <w:basedOn w:val="CommentText" />
        <w:next w:val="CommentText" />
        <w:link w:val="CommentSubjectChar" />
        <w:uiPriority w:val="99" />
        <w:semiHidden />
        <w:unhideWhenUsed />
        <w:rsid w:val="0031027D" />
        <w:rPr>
            <w:b />
            <w:bCs />
        </w:rPr>
    </w:style>
    <w:style w:type="character" w:customStyle="1" w:styleId="CommentSubjectChar">
        <w:name w:val="Comment Subject Char" />
        <w:basedOn w:val="CommentTextChar" />
        <w:link w:val="CommentSubject" />
        <w:uiPriority w:val="99" />
        <w:semiHidden />
        <w:rsid w:val="0031027D" />
        <w:rPr>
            <w:b />
            <w:bCs />
            <w:sz w:val="20" />
            <w:szCs w:val="20" />
        </w:rPr>
    </w:style>
    <w:style w:type="table" w:styleId="GridTable1Light-Accent1">
        <w:name w:val="Grid Table 1 Light Accent 1" />
        <w:basedOn w:val="TableNormal" />
        <w:uiPriority w:val="46" />
        <w:rsid w:val="0037761B" />
        <w:tblPr>
            <w:tblStyleRowBandSize w:val="1" />
            <w:tblStyleColBandSize w:val="1" />
            <w:tblBorders>
                <w:top w:val="single" w:sz="4" w:space="0" w:color="83CAEB" w:themeColor="accent1"
                    w:themeTint="66" />
                <w:left w:val="single" w:sz="4" w:space="0" w:color="83CAEB" w:themeColor="accent1"
                    w:themeTint="66" />
                <w:bottom w:val="single" w:sz="4" w:space="0" w:color="83CAEB"
                    w:themeColor="accent1" w:themeTint="66" />
                <w:right w:val="single" w:sz="4" w:space="0" w:color="83CAEB" w:themeColor="accent1"
                    w:themeTint="66" />
                <w:insideH w:val="single" w:sz="4" w:space="0" w:color="83CAEB"
                    w:themeColor="accent1" w:themeTint="66" />
                <w:insideV w:val="single" w:sz="4" w:space="0" w:color="83CAEB"
                    w:themeColor="accent1" w:themeTint="66" />
            </w:tblBorders>
        </w:tblPr>
        <w:tblStylePr w:type="firstRow">
            <w:rPr>
                <w:b />
                <w:bCs />
            </w:rPr>
            <w:tblPr />
            <w:tcPr>
                <w:tcBorders>
                    <w:bottom w:val="single" w:sz="12" w:space="0" w:color="45B0E1"
                        w:themeColor="accent1" w:themeTint="99" />
                </w:tcBorders>
            </w:tcPr>
        </w:tblStylePr>
        <w:tblStylePr w:type="lastRow">
            <w:rPr>
                <w:b />
                <w:bCs />
            </w:rPr>
            <w:tblPr />
            <w:tcPr>
                <w:tcBorders>
                    <w:top w:val="double" w:sz="2" w:space="0" w:color="45B0E1"
                        w:themeColor="accent1" w:themeTint="99" />
                </w:tcBorders>
            </w:tcPr>
        </w:tblStylePr>
        <w:tblStylePr w:type="firstCol">
            <w:rPr>
                <w:b />
                <w:bCs />
            </w:rPr>
        </w:tblStylePr>
        <w:tblStylePr w:type="lastCol">
            <w:rPr>
                <w:b />
                <w:bCs />
            </w:rPr>
        </w:tblStylePr>
    </w:style>
</w:styles>