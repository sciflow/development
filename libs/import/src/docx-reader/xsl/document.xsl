<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="3.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:j="http://www.w3.org/2005/xpath-functions"
    exclude-result-prefixes="w map j">
    
    <xsl:output method="text" encoding="UTF-8"/>
    
    <xsl:template match="/">
        <xsl:variable name="document-structure">
            <j:map>
                <j:array key="elements">
                    <xsl:apply-templates select="//w:p | //w:tbl"/>
                </j:array>
            </j:map>
        </xsl:variable>
        
        <xsl:value-of select="xml-to-json($document-structure, map{'indent': true()})"/>
    </xsl:template>
    
    <xsl:template match="w:p">
        <j:map>
            <j:string key="type">paragraph</j:string>
            <xsl:if test="w:pPr/w:pStyle/@w:val">
                <j:string key="styleId"><xsl:value-of select="w:pPr/w:pStyle/@w:val"/></j:string>
            </xsl:if>
            <xsl:if test="w:r/w:rPr/w:lang/@w:val">
                <j:string key="language"><xsl:value-of select="(w:r/w:rPr/w:lang/@w:val)[1]"/></j:string>
            </xsl:if>
            <xsl:if test="w:r/w:rPr/w:sz/@w:val">
                <j:number key="fontSize"><xsl:value-of select="(w:r/w:rPr/w:sz/@w:val)[1]"/></j:number>
            </xsl:if>
            <xsl:if test="w:pPr/w:jc/@w:val">
                <j:string key="alignment"><xsl:value-of select="w:pPr/w:jc/@w:val"/></j:string>
            </xsl:if>
            <j:string key="text"><xsl:value-of select="normalize-space(string-join(w:r/w:t, ''))"/></j:string>
        </j:map>
    </xsl:template>
    
    <xsl:template match="w:tbl">
        <j:map>
            <j:string key="type">table</j:string>
            <xsl:if test="w:tblPr/w:tblStyle/@w:val">
                <j:string key="styleId"><xsl:value-of select="w:tblPr/w:tblStyle/@w:val"/></j:string>
            </xsl:if>
            <j:array key="rows">
                <xsl:apply-templates select="w:tr"/>
            </j:array>
        </j:map>
    </xsl:template>
    
    <xsl:template match="w:tr">
        <j:map>
            <j:array key="cells">
                <xsl:apply-templates select="w:tc"/>
            </j:array>
        </j:map>
    </xsl:template>
    
    <xsl:template match="w:tc">
        <j:map>
            <j:string key="text"><xsl:value-of select="normalize-space(string-join(.//w:t, ''))"/></j:string>
        </j:map>
    </xsl:template>
</xsl:stylesheet>