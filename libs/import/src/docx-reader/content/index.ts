import { DOMParser } from '@xmldom/xmldom';
import * as xpath from 'xpath';

import { SFMarkType, SFNodeType, createId, schemas } from "@sciflow/schema";
import { Node, Mark, Schema } from "prosemirror-model";

import { Style } from '../styles';
import { slugify } from '@sciflow/export';
import JSZip from 'jszip';

const docxSchema = schemas.docx;

const select = xpath.useNamespaces({
    w: 'http://schemas.openxmlformats.org/wordprocessingml/2006/main',
    a: 'http://schemas.openxmlformats.org/drawingml/2006/main',
    cp: 'http://schemas.openxmlformats.org/package/2006/metadata/core-properties',
    dc: 'http://purl.org/dc/elements/1.1/',
    pkg: 'http://schemas.microsoft.com/office/2006/xmlPackage',
    w14: 'http://schemas.microsoft.com/office/word/2010/wordml',
    wp: 'http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing',
    pic: 'http://schemas.openxmlformats.org/drawingml/2006/picture',
    r: 'http://schemas.openxmlformats.org/officeDocument/2006/relationships'
});

const createMarks = (defaults, style) => {
    let { bold, italic, size, lang, vertAlign, align, position } = defaults;

    bold = bold || style?.bold;
    italic = italic || style?.italic;
    lang = lang || style?.lang;
    align = align || style?.align;

    const styleAttrs = {
        size: size || style?.size,
        indent: style?.indent,
        marginTop: style?.marginTop,
        marginBottom: style?.marginBottom,
        direction: style?.direction,
        align,
        lang
    };

    for (let attr of Object.keys(styleAttrs)) {
        if (styleAttrs[attr] == undefined || styleAttrs[attr].length === 0) {
            delete styleAttrs[attr];
        } else {
            // convert to fractional pt values (docx uses half points and 1/20 points)
            switch (attr) {
                case 'size':
                    styleAttrs[attr] = styleAttrs[attr] / 2 + 'pt';
                    break;
                case 'position':
                    styleAttrs[attr] = styleAttrs[attr] / 2 + 'pt';
                    break;
                case 'indent':
                    styleAttrs[attr] = styleAttrs[attr] / 20 + 'pt';
                    break;
                case 'marginTop':
                case 'marginBottom':
                    styleAttrs[attr] = styleAttrs[attr] / 20 + 'pt';
                    break;
            }
        }
    }

    const marks = [
        vertAlign === 'subscript' || position < -4 ? docxSchema.marks.sub.create({}) : undefined,
        vertAlign === 'superscript' || position > 4 ? docxSchema.marks.sup.create({}) : undefined,
        bold ? docxSchema.marks.bold.create({}) : undefined,
        italic ? docxSchema.marks.italic.create({}) : undefined,
        Object.keys(styleAttrs).length > 0 ? docxSchema.marks.style.create(styleAttrs) : undefined,
        lang ? docxSchema.marks.lang.create({ lang }) : undefined,
    ].filter(m => m != undefined);

    return marks;
}

const processRun = (runs: any[], { style, parentStyle }) => {
    let textContent: any = [];
    for (let textRun of runs) {
        const textNodes = select("w:t | w:br | w:tab | w:softHyphen | w:noBreakHyphen | w:sym", textRun);
        if (!textNodes || !Array.isArray(textNodes)) { return undefined; }
        let runContent = '';

        textNodes.forEach(node => {
            switch (node.nodeName) {
                case 'w:t':
                    runContent += node.textContent;
                    break;
                case 'w:br':
                    runContent += '\n';
                    break;
                case 'w:tab':
                    runContent += '\t';
                    break;
                case 'w:softHyphen':
                    runContent += '\u00AD';
                    break;
                case 'w:noBreakHyphen':
                    runContent += '\u2011';
                    break;
                case 'w:sym':
                    const font = select("string(@w:font)", node);
                    const charCode = select("string(@w:char)", node);
                    runContent += typeof charCode === 'string' ? String.fromCharCode(parseInt(charCode, 16)) : undefined;
                    break;
                default:
                    break;
            }
        });

        const bold = select('boolean(w:rPr/w:b)', textRun) || style?.bold || parentStyle?.bold;
        const italic = select('boolean(w:rPr/w:i)', textRun) || style?.italic || parentStyle?.italic;
        const size = select('string(w:rPr/w:sz/@w:val)', textRun) || style?.size || parentStyle?.size;
        const position = select('string(w:rPr/w:position/@w:val)', textRun) || style?.position || parentStyle?.position;
        const lang = select('string(w:rPr/w:lang/@w:val)', textRun) || style?.lang || parentStyle?.lang;
        const vertAlign = select('string(w:rPr/w:vertAlign/@w:val)', textRun) || style?.vertAlign || parentStyle?.vertAlign;

        if (typeof runContent === 'string' && runContent?.length > 0) {
            const marks = createMarks({ bold, italic, size, lang, vertAlign, position }, style);
            const textNode = docxSchema.text(runContent as string, marks as Mark[]);

            if (runContent) {
                textContent.push(textNode);
            }
        }
    }

    return textContent;
}

function extractFigureCaption(drawingNode: Element): { label?: string; caption?: string; number?: string; } | null {
    const captionParagraph = drawingNode
        .parentNode // Navigate to the <w:r>
        ?.parentNode // Navigate to the <w:p>
        ?.nextSibling; // Get the next <w:p>

    if (
        captionParagraph &&
        captionParagraph.nodeType === 1 // Ensure it's an Element
    ) {
        // Check if the paragraph has the 'Caption' style
        const isCaption = select(
            "boolean(w:pPr/w:pStyle[@w:val='Caption'])",
            captionParagraph
        );
        if (!isCaption) return null;

        // Extract all <w:r> elements
        const runNodes = select("w:r", captionParagraph) as globalThis.Element[];

        // Separate the label, caption, and figure number
        let label: string | undefined;
        let captionParts: string[] = [];
        let figureNumber: string | undefined;

        for (const run of runNodes) {
            const text = select("string(w:t)", run);
            if (typeof text === 'string') {
                // If the text matches a figure label pattern, treat it as the label
                const figureMatch = text.match(/^(Fig(?:ure)?\s+(\d+(\.\d+)*))/i);
                if (!label && figureMatch) {
                    label = figureMatch[1]; // Full label, e.g., "Fig 1.1" or "Figure 1"
                    figureNumber = figureMatch[2]; // Extracted number, e.g., "1.1" or "1"
                } else {
                    // Otherwise, treat it as part of the caption
                    captionParts.push(text.trim());
                }
            } else {
                console.warn('Unexpected type for text:', typeof text, text);
            }
        }

        return {
            label,
            caption: captionParts.join(' ').trim(),
            number: figureNumber,
        };
    }

    return null; // Return null if no valid caption paragraph is found
}

export const readContent = async (xml: string, data: { styles: Style[]; zip: JSZip; }): Promise<{ doc: Node; images: any[] } | undefined> => {
    let content: any[] = [];
    let images: { id: string; url?: string; alt?: string; path: string; caption?: string; number?: string; label?: string; }[] = [];
    try {
        const documentDoc = new DOMParser().parseFromString(xml, 'application/xml');

        const relsFileContent = await data.zip.file('word/_rels/document.xml.rels')?.async('text');
        const relsDoc = relsFileContent
            ? new DOMParser().parseFromString(relsFileContent, 'application/xml')
            : null;

        const relsMap = relsDoc
            ? Array.from(relsDoc.getElementsByTagName('Relationship')).reduce((acc, rel) => {
                const id = rel.getAttribute('Id');
                const target = rel.getAttribute('Target');
                if (id && target) acc[id] = target;
                return acc;
            }, {} as Record<string, string>)
            : {};

        const bodyContent = select(
            '//w:body/*[self::w:p or self::w:tbl or self::w:sectPr or self::w:bookmarkStart or self::w:bookmarkEnd or self::w:sdt or self::w:altChunk] | //w:hdr/*[self::w:p or self::w:tbl or self::w:sectPr or self::w:bookmarkStart or self::w:bookmarkEnd or self::w:sdt or self::w:altChunk]',
            documentDoc
        );
        if (!bodyContent || !Array.isArray(bodyContent)) { return undefined; }

        for (let node of bodyContent.filter(n => n.nodeName === 'w:p')) {
            /** handles the async processing of the node */
            const handleNode = async (node: globalThis.Node) => {
                {
                    // TODO filter elements other than paragraphs

                    /** The session id of the element at the current revision */
                    const rsidR = select('string(@w:rsidR)', node);
                    /** The session id of the element */
                    const rsidRDefault = select('string(@w:rsidRDefault)', node);

                    /**
                     * Selects the run elements from the XML node.
                     * @example "w:r | w:ins/w:r"
                     * runs may be wrapped in w:hyperlink
                     */
                    const runs: xpath.SelectReturnType = select('w:r | w:ins/w:r | w:hyperlink/w:r | w:ins/w:hyperlink/w:r | w:hyperlink/w:ins/w:r', node);

                    /**
                     * Extracts the paragraph ID from the XML node.
                     * @example "1A2B3C4D"
                     */
                    const paraId: string | undefined = select('string(@w14:paraId)', node) as string;

                    /**
                     * Extracts the style ID from the paragraph properties.
                     * @example "Heading1"
                     */
                    const styleId: string | undefined = select('string(w:pPr/w:pStyle/@w:val)', node) as string;

                    /**
                     * Extracts the alignment from the paragraph properties.
                     * @example "center"
                     */
                    const align: string | undefined = select('string(w:pPr/w:jc/@w:val)', node) as string;

                    /**
                     * Checks if the paragraph or run properties indicate bold text.
                     * @example true
                     */
                    const bold: boolean | undefined = select('boolean(w:pPr/w:rPr/w:b)', node) as boolean || select('boolean(w:r/w:rPr/w:b)', node) as boolean;

                    /**
                     * Extracts the language from the paragraph or run properties.
                     * @example "en-US"
                     */
                    const lang: string | undefined = select('string(w:pPr/w:rPr/w:lang/@w:val)', node) as string;

                    /**
                     * Checks if the paragraph or run properties indicate italic text.
                     * @example true
                     */
                    const italic: boolean | undefined = select('boolean(w:pPr/w:rPr/w:i)', node) as boolean || select('boolean(w:r/w:rPr/w:i)', node) as boolean;

                    /**
                     * Extracts the font size from the paragraph or run properties.
                     * @example "24" (for a 12pt font size)
                     */
                    const size: string | undefined = select('string(w:pPr/w:rPr/w:sz/@w:val)', node) as string || select('string(w:r/w:rPr/w:sz/@w:val)', node) as string;

                    /**
                     * Extracts the text position from the run properties.
                     * @example "2" (for superscript)
                     */
                    const position: string | undefined = select('string(w:r/w:rPr/w:position/@w:val)', node) as string;

                    let style: Style | undefined, parentStyle: Style | undefined;
                    if (styleId) {
                        style = data.styles.find(s => s.styleId === styleId);
                        parentStyle = style && data.styles.find(s => s.styleId === style?.parentStyle);
                    }

                    const content = Array.isArray(runs) ? processRun(runs, { style, parentStyle }) : undefined;
                    const levelString = style?.level ?? parentStyle?.level;
                    let level: number | undefined = levelString && Number.isInteger(levelString) ? Number.parseInt(levelString) : undefined;
                    if (level != undefined && level > 0) { level += 1; }
                    if (!content || content.length === 0) { return undefined; }
                    const marks = createMarks({ bold, italic, size, lang, align/** , position */ }, style);

                    for (let node of bodyContent.filter(n => n.nodeName === 'w:p')) {
                        const drawingNodes = select(".//w:drawing", node) as Element[];

                        if (drawingNodes.length > 0) {
                            for (const drawingNode of drawingNodes) {
                                const relId = (select("string(.//@r:embed)", drawingNode) || select("string(.//@r:id)", drawingNode)) as string;
                                // we might encounter the node multiple times depending on the selector
                                if (images.some(i => i.id === relId)) { continue; }
                                const relTarget = relId && relsMap[relId] ? relsMap[relId] : undefined;

                                const path = `word/${relTarget}`;
                                const captionData = extractFigureCaption(drawingNode);
                                if (relTarget && /\.(jpg|jpeg|png|webp|gif)$/i.test(relTarget)) {
                                    const imageFile = await data.zip.file(path)?.async('base64');
                                    if (imageFile) {
                                        // Push image data to the images array
                                        images.push({
                                            id: relId,
                                            url: `data:image/${relTarget.split('.').pop()};base64,${imageFile}`,
                                            path,
                                            caption: captionData?.caption || undefined,
                                            number: captionData?.number || undefined,
                                            alt: select("string(.//@descr)", drawingNode) as string || undefined,
                                        });
                                    } else {
                                        console.warn(`Failed to read image file at ${path}`);
                                    }
                                } else {
                                    images.push({
                                        id: relId,
                                        path,
                                        label: captionData?.label,
                                        caption: captionData?.caption || undefined,
                                        number: captionData?.number || undefined,
                                        alt: select("string(.//@descr)", drawingNode) as string || undefined,
                                    });
                                }
                            }
                        }
                    }

                    let tagList: string[] = [];
                    const classList = [
                        styleId,
                        parentStyle?.styleId
                    ].filter(s => s != undefined && s?.length > 0)
                        .map(s => s?.toLowerCase())
                        .filter((s, i, a) => a.indexOf(s) === i) as string[];

                    if (style?.styleLabels && style?.styleLabels?.length > 0) {
                        tagList.push(style.styleLabels[0].label);
                        if (!style.styleLabels[0].level != undefined) {
                            level = style.styleLabels[0].level;
                        }
                    } else if (parentStyle?.styleLabels && parentStyle?.styleLabels?.length > 0) {
                        tagList.push(parentStyle.styleLabels[0].label);
                        if (!parentStyle.styleLabels[0].level != undefined) {
                            level = parentStyle.styleLabels[0].level;
                        }
                    }

                    tagList = tagList.map(s => s?.toLowerCase())
                        .filter((s, i, a) => a.indexOf(s) === i);

                    let id = paraId;
                    if (!paraId) {
                        id = slugify([styleId, ...content.slice(0, 3).map((c, i): string => {
                            return c.textContent?.slice(0, 10) || i;
                        })].join('-')) || createId();
                    }

                    const paragraph = docxSchema.nodes.paragraph.create({
                        id,
                        classList,
                        tagList,
                        level
                    }, content, marks as Mark[]);

                    try {
                        paragraph.check();
                    } catch (e) {
                        console.error(e);
                        debugger;
                    }

                    return paragraph;
                }
            };

            const n = await handleNode(node);
            if (n) {
                content.push(n);
            }
        }

    } catch (error) {
        debugger;
        console.error('Error processing the styles:', error);
    }

    const doc = docxSchema.nodes.doc.create({}, content);
    doc.check();
    return { doc, images };
}