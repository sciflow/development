import { DOMParser } from '@xmldom/xmldom';
import * as xpath from 'xpath';
import { getKnownStyles } from '../../tagging/heuristics';
import { JaroWinklerDistance } from 'natural';

export interface Style {
    /**
     * The unique identifier for the style.
     * @example "Heading1"
     */
    styleId: string;

    /**
     * The name of the style.
     * @example "Heading 1"
     */
    styleName: string;

    /**
     * Optional: The font size of the text.
     * @example "24" (for a 12pt font size)
     */
    size?: string;

    /**
     * Optional: The language of the text.
     * @example "en-US" (for English - United States)
     */
    lang?: string;

    /**
     * Optional: Indicates if the text is bold.
     * @example "true" (if the text is bold)
     */
    bold?: string;

    /**
     * Optional: The ID of the parent style from which this style is derived.
     * @example "Normal"
     */
    parentStyle?: string;

    /**
     * Optional: The outline level of the paragraph.
     * @example "1" (for a first-level heading)
     */
    level?: string;

    /**
     * Optional: The top margin spacing before the paragraph.
     * @example "200" (for 200 units of spacing)
     */
    marginTop?: string;

    /**
     * Optional: The direction of the text.
     * @example "ltr" (for left-to-right)
     */
    direction?: string;

    /**
     * Optional: The bottom margin spacing after the paragraph.
     * @example "200" (for 200 units of spacing)
     */
    marginBottom?: string;

    /** 
     * Style labels
     * Labels assigned by analyzing the style names and assigning the most likely match.
     */
    styleLabels?: any[];
}

export const readStyles = (xml: string): Style[] => {
    let styles: any[] = [];
    try {
        const doc = new DOMParser().parseFromString(xml, 'application/xml');

        const select = xpath.useNamespaces({
            w: 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'
        });

        const styleNodes = select('//w:style', doc);
        if (!styleNodes || !Array.isArray(styleNodes)) { return styles; }
        styles = styleNodes.map((node) => {
            const styleId = select('string(@w:styleId)', node);
            const props = {
                styleId, // The unique identifier for the style (e.g., "Heading1").
                styleName: select('string(w:name/@w:val)', node), // The name of the style (e.g., "Heading 1").
                size: select('string(w:rPr/w:sz/@w:val)', node), // The font size of the text (e.g., "24" for a 12pt font size).
                position: select('string(w:rPr/w:position/@w:val)', node), // The vertical position of the text (e.g., "2" for superscript).
                lang: select('string(w:rPr/w:lang/@w:val)', node), // The language of the text (e.g., "en-US" for English - United States).
                bold: select('boolean(w:rPr/w:b)', node), // Whether the text is bold (e.g., true if the text is bold, false otherwise).
                italic: select('boolean(w:rPr/w:i)', node), // Whether the text is italic (e.g., true if the text is italic, false otherwise).
                parentStyle: select('string(w:basedOn/@w:val)', node), // The ID of the parent style from which this style is derived (e.g., "Normal").
                styleLabels: [] as any[],
                level: select('string(w:pPr/w:outlineLvl/@w:val)', node), // The outline level of the paragraph (e.g., "1" for a first-level heading).
                direction: select('string(w:pPr/w:textDirection/@w:val)', node), // The direction of the text (e.g., "ltr" for left-to-right).
                marginTop: select('string(w:pPr/w:spacing/@w:before)', node), // The top margin spacing before the paragraph (e.g., "200" for 200 units of spacing).
                marginBottom: select('string(w:pPr/w:spacing/@w:after)', node) // The bottom margin spacing after the paragraph (e.g., "200" for 200 units of spacing).
            };

            if (props.styleName) {
                const value = props.styleName as string;
                if (value) {
                    for (let test of getKnownStyles()) {
                        const distance = JaroWinklerDistance(test.text, value, { ignoreCase: true });
                        if (distance > 0.9 || test.text?.length > 6 && value?.startsWith(test.text)) {
                            props.styleLabels.push({ ...test, distance });
                        }
                    }
                }

                props.styleLabels = props.styleLabels.sort((a, b) => b.distance - a.distance);
            }

            for (let prop of Object.keys(props)) {
                if (!props[prop] || props[prop].length === 0) {
                    delete props[prop];
                }
            }
            return props;
        });
    } catch (error) {
        console.error('Error processing the styles:', error);
    }

    return styles;
};
