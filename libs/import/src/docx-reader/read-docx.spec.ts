import { it, describe } from 'mocha';
import { expect } from 'chai';
import { join } from 'path';
import { readDocx } from './index';
import { readFileSync } from 'fs';
import { DOMSerializer } from 'prosemirror-model';
import { JSDOM } from 'jsdom';
import { schemas } from '@sciflow/schema';

const dom = new JSDOM('<!DOCTYPE html>');
const document = dom.window.document;

describe('DOCX reader', async () => {
    it('Reads a simple file', async () => {

        const { styles, doc } = await readDocx(readFileSync(join(__dirname, 'fixtures/test.docx')));
        expect(doc.child(0).textContent).to.equal('Document title');
        expect(doc.child(4).textContent).to.equal('Text in another font size');
        expect(styles.find(s => s.styleId === 'Customstyles')?.size).to.equal('32'); // 32 in half points equals 16pt

        const serializer = DOMSerializer.fromSchema(schemas.docx);
        const htmlFrag = serializer.serializeNode(doc.child(4), { document }) as HTMLElement;
        
        expect(htmlFrag?.outerHTML).to.equal('<span style="font-size: 36pt;"><span lang="en-US"><p id="7DE50325"><span style="font-size: 36pt;"><span lang="en-US">Text in another font size</span></span></p></span></span>');
    });
});
