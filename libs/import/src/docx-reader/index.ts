import { existsSync, readFileSync } from 'fs';
import JSZip from 'jszip';
import { readStyles } from './styles';
import { readContent } from './content';
import { schemas } from '@sciflow/schema';

import { JSDOM } from 'jsdom';
const dom = new JSDOM('<!DOCTYPE html>');
import { DOMSerializer } from 'prosemirror-model';
import { getKnownStyles } from '../tagging/heuristics';
import { JaroWinklerDistance } from 'natural';
const document = dom.window.document;

export const readDocxFile = async (inputFileContent: any) => {
    const zip = await JSZip.loadAsync(inputFileContent);
    const styleFile =  await zip.file('word/styles.xml')?.async('string');
    if (!styleFile) { throw new Error('Style file must exist'); }

    let headers: any[] = [], footers: any[] = [];
    for (let file of Object.keys(zip.files)) {
        const m = file.match(/(header|footer)\d+\.xml/);
        if (m) {
            if (m[1] === 'header') {
                headers.push({
                    name: file,
                    type: 'header',
                    content: await zip.file(file)?.async('string')
                });
            } else if (m[1] === 'footer') {
                footers.push({
                    name: file,
                    type: 'footer',
                    content: await zip.file(file)?.async('string')
                });
            }
        }
    }

    const documentFile =  await zip.file('word/document.xml')?.async('string');
    const relsFile = await zip.file('word/_rels/document.xml.rels')?.async('string');
    if (!documentFile) { throw new Error('Document file must exist'); }

    return { styleFile, documentFile, relsFile, headers, footers, zip };
}

export const readDocx = async (inputFileContent: any) => {

    const { styleFile, documentFile, headers, relsFile, footers, zip } = await readDocxFile(inputFileContent);
    
    const styles = readStyles(styleFile);

    for (let style of styles) {
        if (!style.styleLabels) { style.styleLabels = []; }
        if (style.styleName) {
            const value = style.styleName as string;
            if (value) {
                for (let test of getKnownStyles()) {
                    const distance = JaroWinklerDistance(test.text, value, { ignoreCase: true });
                    if (distance > 0.9 || test.text?.length > 6 && value?.startsWith(test.text)) {
                        style.styleLabels.push({ ...test, distance });
                    }
                }
            }
    
            style.styleLabels = style.styleLabels.sort((a, b) => b.distance - a.distance);
        }
    }

    let headersAndFooters: any[] = [];
    for (let file of [...headers, ...footers]) {
        const result = await readContent(file.content, { styles, zip });
        if (result?.doc && result?.doc?.textContent?.length > 0) {
            headersAndFooters.push({
                type: file.type,
                name: file.name,
                doc: result?.doc,
                html: serializeDocxNode(result?.doc),
                xml: result?.doc,
                images: result?.images
            });
        }
    }

    const result = await readContent(documentFile, { styles, zip });
    if (!result?.doc) { throw new Error('Could not read content'); }
    return { styles, doc: result?.doc, images: result?.images, html: serializeDocxNode(result?.doc), headersAndFooters, zip };
}

export const serializeDocxNode = (doc: any): string => {
    const docxSchema = schemas.docx;
    const serializer = DOMSerializer.fromSchema(docxSchema);
    const htmlFrag = serializer.serializeNode(doc, { document }) as HTMLElement;
    return htmlFrag.outerHTML;
}
