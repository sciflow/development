import { DocumentNode } from "./types";

export type RendererPlugin<OutputFormat, InputFormat> = ((node: DocumentNode<any>, element: OutputFormat, engine: RenderingEngine<OutputFormat, InputFormat>) => Promise<OutputFormat>)

/**
 * Handles the rendering of an output format from start to finish.
 */
export abstract class RenderingEngine<OutputFormat, InputFormat> {
    log: (level: string, message: string, payload?: any) => void;
    /**
     * Renderers for each node type.
     */
    renderers: { [nodeName: string]: Renderer<OutputFormat> };
    /**
     * Renderer state, if needed (e.g. for citation renderers).
     */
    state: { [rendererKey: string]: any; };

    constructor(
        renderers: { [key: string /** stay open to undefined types from plugins */]: any },
        data?: InputFormat,
        private readonly logLevel = 'error'
    ) {}
}

/**
 * Renders a document node type to an output format.
 */
export interface Renderer<OutputFormat> {
    /**
     * The renderering engine including the meta data.
     */
    engine?: RenderingEngine<OutputFormat, any>;
    /** Renders a note. */
    render(node: DocumentNode<any>, parent?: DocumentNode<any>): OutputFormat | Promise<OutputFormat>;
}
