export interface AnnotatedAuthor {
    /** 
     * The ID of the html element the author was found in 
     */
    htmlElId?: string;

    /** 
     * The identifying number or special character for affiliations from the first sup element after the last name 
     */
    affiliationNumbers?: string[];

    /** Optional: Any other symbols after an affiliation number like "*"" or "†" */
    symbols?: string[];

    /** The full author name */
    name: string;

    /** The first name(s) of the author. This includes all given names and initials except the last name
     * example: John B. MacIntyre -> John B.
     */
    firstName?: string;

    /** The last name or family name of the author. Look at the full name and extract the last name. */
    lastName?: string;

    /** The prefix like Dr. from the full author name */
    prefix?: string;

    /** The suffix like Jr., Sr. */
    suffix?: string;

    /** The ORCID if it was listed (16-digit number with dashes) */
    orcid?: string;

    /** 
     * Email address of the author
     */
    email?: string;

    /** 
     * Whether the author was designated for correspondence 
     */
    correspondingAuthor?: boolean;
}

export interface Authors {
    /** The authors listed on the top of the document */
    authors: AnnotatedAuthor[];
    /** The emails of corresponding authors */
    correspondenceEmail: string[];
}