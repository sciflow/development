import express from 'express';

import winston, { createLogger } from 'winston';
import bodyParser from 'body-parser';
import { spawnSync } from 'child_process';
import { existsSync, readFileSync, readdirSync, writeFileSync } from 'fs';
import mime from 'mime';
import multer from 'multer';
import { extname, join } from 'path';
import { dirSync } from 'tmp';

const { PORT = 3001 } = process.env;

const logger = createLogger({
    level: 'info',
    format: winston.format.json(),
    defaultMeta: { service: 'file-transform' },
    transports: [
        new winston.transports.Console({ format: winston.format.simple() }),
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.File({ filename: 'combined.log' }),
    ],
});

const main = async () => {
    const app = express();

    app.set('port', PORT);
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    // allow 200mb max
    app.use(multer({ limits: { fieldSize: 200 * 1024 * 1024 } }).single('file'));

    // put the newest version first if multiple are available
    const libreOfficeBinaries = ['libreoffice25.2', 'libreoffice', 'soffice'];
    let availableBinary;
    for (let binaryName of libreOfficeBinaries) {
        try {
            const { status, stderr, stdout } = spawnSync(
                binaryName,
                ['--version']
            );
            const errors = stderr?.toString().replaceAll('\n', ' ');
            const logs = stdout?.toString().replaceAll('\n', ' ');
            logger.info('Checking LibreOffice  ' + binaryName, status === 0 ? logs[0] : errors?.join(' '));
            if (logs?.startsWith('LibreOffice')) {
                logger.info(`Using binary (${binaryName}): ` + logs);
                availableBinary = binaryName;
                break;
            }
        } catch (e) {
            logger.error('Could not read libreoffice binary', e);
        }
    }

    app.get('/', (req, res) => {
        try {
            const { status, stderr, stdout } = spawnSync(
                availableBinary,
                ['--version']
            );
            const errors = stderr?.toString().split('\n').filter(s => s.length > 0);
            const logs = stdout?.toString().split('\n').filter(s => s.length > 0);
            logger.info('Checking LibreOffice: ', status === 0 ? logs[0] : errors.join(' '));
            res.send(logs[0]?.split(' ')[0] === 'LibreOffice');
        } catch (e) {
            logger.error('Could not initialize libreoffice', { message: e.message });
            res.send(e.message);
        }
    });

    app.post('/', (req, res) => {
        const { mimetype, originalname, buffer } = req.file;
        const targetExtension = req.headers.accept ? mime.getExtension(req.headers.accept) : 'png';
        if (!targetExtension) { return res.status(500).send('Could not handle input file. Did you supply an accept header?'); }
        const sourceExtension = mimetype ? mime.getExtension(mimetype) : (originalname && extname(originalname)?.replace('.', ''));

        const crop = req.query.cropping !== 'off';
        if (!['emf', 'wmf', 'pdf'].includes(sourceExtension)) {
            logger.info('Not converting source', { source: sourceExtension, target: targetExtension, crop, originalname, mimetype });
            return res.status(501).send('Not supported: ' + sourceExtension);
        }
        
        if (!['png', 'pdf'].includes(targetExtension)) {
            logger.info('Not converting target', { source: sourceExtension, target: targetExtension, crop, originalname, mimetype });
            return res.status(501).send('Not supported: ' + targetExtension);
        }
        
        logger.info('Converting', { source: sourceExtension, target: targetExtension, crop, originalname, mimetype });

        const tmpobj = dirSync({ unsafeCleanup: true });
        let activeFilePath = join(tmpobj.name, `input.${sourceExtension}`);
        writeFileSync(activeFilePath, buffer);

        try {
            // create a PDF first if needed
            if (sourceExtension === 'emf' || sourceExtension === 'wmf') {
                //--convert-to pdf:writer_pdf_Export
                //--convert-to pdf:calc_pdf_Export
                //--convert-to pdf:draw_pdf_Export
                //--convert-to pdf:impress_pdf_Export
                //--convert-to pdf:writer_web_pdf_Export
                const pdfOutputFilePath = join(tmpobj.name, 'input' + '.pdf');
                const commands = ['--headless', '--convert-to', 'pdf:writer_pdf_Export', activeFilePath];
                const result = spawnSync(
                    availableBinary,
                    commands,
                    {
                        cwd: tmpobj.name
                    }
                );

                const errors = result.stderr?.toString().split('\n').filter(s => s.length > 0);
                const logs = result.stdout?.toString().split('\n').filter(s => s.length > 0);
                logger.info('Running LibreOffice', { availableBinary, signal: result.signal, status: result.status, errors, logs: logs.slice(1) });

                if (!existsSync(pdfOutputFilePath)) {
                    logger.error('Could not convert ' + sourceExtension, { source: sourceExtension, target: targetExtension, files: readdirSync(tmpobj.name), path: tmpobj.name });
                    return res.status(500).send('Could not convert ' + sourceExtension);
                }

                if (existsSync(pdfOutputFilePath)) {
                    activeFilePath = pdfOutputFilePath;
                }
            }

            // remove any whitespace around the image
            if (crop) {
                logger.info('Cropping', { activeFilePath });
                const croppedFile = join(tmpobj.name, 'input-crop' + '.' + targetExtension);
                const cropresult = spawnSync(
                    'pdfcrop',
                    [activeFilePath, croppedFile]
                );
                if (cropresult.status != 0) {
                    logger.info(cropresult.stdout?.toString());
                    logger.error(cropresult.stderr?.toString());
                    return res.status(500).send('Could not crop pdf');
                }
                if (existsSync(croppedFile)) {
                    activeFilePath = croppedFile;
                }
            }

            if (!existsSync(activeFilePath)) {
                logger.error('Did not find output ' + activeFilePath, readdirSync(tmpobj.name));
                return res.status(500).send('Could not transform file');
            }

            if (targetExtension === 'png') {
                const pngFile = join(tmpobj.name, 'input' + '.' + 'png');
                const pdfResult = spawnSync(
                    'pdftoppm',
                    [activeFilePath, pngFile.replace('.png', ''), '-png', '-f', '1', '-singlefile', '-r', '220']
                );
                if (pdfResult.status != 0) {
                    logger.info(pdfResult.stdout?.toString());
                    logger.error(pdfResult.stderr?.toString());
                }
                if (existsSync(pngFile)) {
                    activeFilePath = pngFile;
                }
            }

            res.header('Content-Type', mime.getType(targetExtension));
            return res.send(readFileSync(activeFilePath));

        } catch (e) {
            logger.error('Could not transform', { reason: e.message });
            return res.status(500).send('Could not transform file');
        } finally {
            tmpobj.removeCallback();
        }
    });

    app.listen(PORT, () => logger.info('Listening on ' + PORT));
};

main().catch(logger.error);
