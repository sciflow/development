import { listObjects, s3ToStream } from '@sciflow/support';
import express, { static as serveStatic } from 'express';
import { createReadStream, createWriteStream, existsSync, mkdirSync, readFile, readFileSync, ReadStream, writeFileSync } from 'fs';
import { JSDOM, VirtualConsole } from 'jsdom';
import mime from 'mime';

import JSZip from 'jszip';
import { basename, extname, join } from 'path';
import Prince from 'prince';
import { dirSync } from 'tmp';
import winston from 'winston';
import { ExportService, injector } from './services';
import { spawnSync } from 'child_process';
import { Readable } from 'stream';
import { slugify } from '@sciflow/export';
const exportService: ExportService = injector.get(ExportService);
const { LOG_LEVEL = 'info' } = process.env;
const logger = winston.createLogger({ format: winston.format.json(), transports: new winston.transports.Console({ level: LOG_LEVEL }), level: LOG_LEVEL });

const fetchAsset = (req, res) => {
    const { templateId, filename, folder } = req.params;

    if (process.env.TEMPLATE_SOURCE) {
        const parts = [templateId, 'assets', folder, filename].filter(s => s != null);
        if (parts.includes('..')) { return res.status(404).send('File not found!'); }
        const filePath = join(process.env.TEMPLATE_SOURCE, ...parts);
        if (!existsSync(filePath)) {
            logger.debug('Asset not found', { filename, folder, templateId });
            return res.status(404).send('File not found');
        }

        readFile(filePath, (err, data) => {
            const type = mime.getType(extname(filename).replace('.', ''));
            res.setHeader('Content-Type', type);
            return res.send(data);
        });
    }

    // TODO implement fetching from S3
}

/**
 * We provide a number of default exports that can be used without further installation
 * @param app the express app
 */
export const registerExports = (app) => {

    const fontPath = process.env.FONT_PATH || join(__dirname, 'fonts');
    const fontUrlPrefix = process.env.FONT_URL_PREFIX || '/export/fonts/';
    const distPath = join(__dirname, 'static', 'export');
    const nodeModulesPath = existsSync(join(__dirname, '..', 'node_modules')) ? join(__dirname, '..', 'node_modules') : join(__dirname, '..', '..', 'node_modules');

    /**
     * Returns an asset from storage (e.g. s3).
     */
    app.get('/export/asset/:projectId/:filename', async (req, res) => {
        try {
            const type = mime.getType(extname(req.params.filename).replace('.', ''));
            res.setHeader('Content-Type', type);
            return await s3ToStream({ Key: `${req.params.projectId}/${req.params.filename}`, VersionId: undefined }, res) as ReadStream | Readable;
        } catch (e: any) {
            debugger;
            res.status(500).send('Could not stream file');
        }
    });

    app.use('/export/static', serveStatic(distPath));
    app.use(fontUrlPrefix, serveStatic(fontPath));
    app.get('/export/tpl/:templateId/assets/:filename', fetchAsset);
    app.get('/export/tpl/:templateId/assets/:folder/:filename', fetchAsset);

    /**
     * Handle express export requests.
     */
    const handleExport = async (req, res) => {
        const { filename, projectId, format = 'snapshot', templateId } = req.params;
        const { citationStyle, locale } = req.query;
        const debug = req.query.debug === 'true';
        const fileKey = `${projectId}/${decodeURIComponent(filename)}`;
        const baseFileName = basename(fileKey);
        
        try {
            const directory = await listObjects(projectId);
            const file = directory.find(({ Key }) => Key === fileKey);
            if (!file) {
                return res.status(404).render('error', { message: 'File not found' });
            }
            
            const dirObj = dirSync({ unsafeCleanup: true });
            
            const path = file.Key;
            const sourceFile = join(dirObj.name, baseFileName);

            const stream = createWriteStream(sourceFile);
            try {
                await s3ToStream({ Key: path, VersionId: undefined }, stream);
            } catch (e) {
                logger.error('Could not stream file for export', { message: e.message, path, baseFileName });
            }
            await new Promise((resolve) => stream.on('finish', resolve));

            const manuscript = JSON.parse(readFileSync(sourceFile, 'utf-8'));
            let scripts: { src?: string; content?: string; }[] = [];

            const virtualConsole = new VirtualConsole();
            virtualConsole.sendTo(console, { omitJSDOMErrors: true });

            let variant = '';
            let downloadname = `${slugify(filename?.replace('.json', ''))}`;
            if (variant?.length > 0) { downloadname += '-' + slugify(variant); }

            switch (format) {
                case 'epub':
                    {
                        const zip = await exportService.createEPUBZIP({ projectId, templateId, scripts, manuscript, tmpPath: dirObj.name, runner: 'epub', inline: false, citationStyle, locale });
                        res.setHeader('Content-Disposition', `attachment; filename=${downloadname}.epub`);
                        res.setHeader('Content-Type', 'application/epub+zip; charset=utf-8');
                        return res.send(zip);
                    }
                case 'snapshot':
                    {
                        const zip = await exportService.createSnapshotZIP({ projectId, manuscript });
                        res.setHeader('Content-Disposition', `attachment; filename=${downloadname}-snapshot.zip`);
                        res.setHeader('Content-Type', 'application/zip; charset=utf-8');
                        return res.send(zip);
                    }
                case 'pagedjs-cli':
                    {
                        const file = await exportService.renderHTML({ projectId, templateId, scripts, manuscript, tmpPath: dirObj.name, runner: 'pagedjs', inline: false, citationStyle, locale });
                        const zip = new JSZip();
                        const content = await zip.loadAsync(file);

                        for (let path of Object.keys(content.files)) {
                            const file = await content.file(path)?.async('array');
                            if (file) {
                                logger.info('Writing ' + join(dirObj.name, path))
                                writeFileSync(join(dirObj.name, path), Buffer.from(file));
                            } else {
                                mkdirSync(join(dirObj.name, path), { recursive: true });
                            }
                        }

                        const inFilename = join(dirObj.name, 'manuscript.html');
                        const outFilename = join(dirObj.name, 'manuscript.pdf');

                        try {
                            const { signal, status, stderr, stdout } = spawnSync(
                                'pagedjs-cli',
                                [inFilename, '-o', outFilename]
                            );
                            const errors = stderr.toString().split('\n').filter(s => s.length > 0);
                            const logs = stdout.toString().split('\n').filter(s => s.length > 0);
                            logger.info('Checking pandoc', { signal, status, errors, logs: logs.slice(1) });
                        } catch (e: any) {
                            logger.error('Could not initialize pandoc', { message: e.message });
                            return res.status(500).json({ message: e.message, stack: e.stack });
                        }

                        res.setHeader('Content-Disposition', `attachment; filename=${downloadname}.pdf`);
                        res.setHeader('Content-Type', 'application/pdf');
                        const streamFile = createReadStream(outFilename).pipe(res);
                        await new Promise((resolve) => streamFile.on('finish', resolve));
                        return res.end();
                    }
                    break;
                case 'pagedjs':
                    scripts.push({
                        src: '/export/static/pagedjs.base.js'
                    });
                    scripts.push({
                        src: '/export/static/pagedjs.polyfill.js'
                    });
                // fall through
                case 'pagedjs-html':
                    // pagedjs-html is meant as a pagedjs compatible html output without the pagedjs polyfills
                    {
                        const file = await exportService.renderHTML({ projectId, templateId, scripts, manuscript, tmpPath: dirObj.name, runner: 'pagedjs', inline: true, citationStyle, locale });
                        const zip = new JSZip();
                        const content = await zip.loadAsync(file);
                        const body = await content.file('manuscript.html')?.async('string');

                        const dom = new JSDOM(body, { virtualConsole });
                        const document = dom.window.document;

                        if (format === 'pagedjs') {
                            // we append the paged js styles for the in-browser view
                            const pagedjsStyles = document.createElement('link');
                            pagedjsStyles.setAttribute('rel', 'stylesheet');
                            pagedjsStyles.setAttribute('href', '/export/static/pagedjs.interface.css');
                            pagedjsStyles.setAttribute('type', 'text/css');
                            document.head.appendChild(pagedjsStyles);
                        }

                        res.setHeader('Content-Type', 'text/html; charset=utf-8');
                        return res.send(dom.window.document.documentElement.outerHTML);
                    }
                case 'princexml-html':
                // fall through
                case 'html':
                    {
                        const file = await exportService.renderHTML({
                            projectId,
                            templateId,
                            scripts,
                            manuscript,
                            tmpPath: dirObj.name,
                            runner: format === 'princexml-html' ? 'princexml' : 'html',
                            inline: true,
                            citationStyle,
                            locale,
                            debug
                        });
                        const zip = new JSZip();
                        const content = await zip.loadAsync(file);
                        const body = await content.file('manuscript.html')?.async('string');
                        const dom = new JSDOM(body, { virtualConsole });
                        const document = dom.window.document;

                        scripts.push({ src: '/export/mathjax/es5/tex-mml-chtml.js' });
                        const pagedjsStyles = document.createElement('link');
                        pagedjsStyles.setAttribute('rel', 'stylesheet');
                        document.head.appendChild(pagedjsStyles);

                        res.setHeader('Content-Type', 'text/html; charset=utf-8');
                        return res.send(dom.window.document.documentElement.outerHTML);
                    }
                case 'xml':
                    {
                        const body = await exportService.renderXML({ projectId, templateId, scripts, manuscript, tmpPath: dirObj.name, inline: true });
                        res.setHeader('Content-Type', 'application/xml; charset=utf-8');
                        return res.send(body);
                    }
                case 'princexml':
                    {
                        const file = await exportService.renderHTML({ projectId, templateId, scripts, manuscript, tmpPath: dirObj.name, runner: 'princexml', inline: false, citationStyle, locale, debug });
                        const zip = new JSZip();
                        const content = await zip.loadAsync(file);

                        for (let path of Object.keys(content.files)) {
                            const file = await content.file(path)?.async('array');
                            if (file) {
                                logger.info('Writing ' + join(dirObj.name, path))
                                writeFileSync(join(dirObj.name, path), Buffer.from(file));
                            } else {
                                mkdirSync(join(dirObj.name, path), { recursive: true });
                            }
                        }

                        let pdfProfile;
                        if (zip.file('index.json')) {
                            // concatenate names if there were multiple
                            const indexFile = zip.file('index.json');
                            if (indexFile) {
                                const index = JSON.parse(await indexFile.async('string'));
                                pdfProfile = index.pdfProfile;
                            }
                        }

                        const inFilename = join(dirObj.name, 'manuscript.html');
                        const outFilename = join(dirObj.name, 'manuscript.pdf');
                        try {
                            let prince = Prince()
                                .inputs(inFilename)
                                .output(outFilename);

                            if (pdfProfile) {
                              prince = prince.option('pdf-profile', pdfProfile);
                            }

                            if (existsSync(process.env.PRINCE_LICENSE_PATH as string)) {
                                prince = prince.license(process.env.PRINCE_LICENSE_PATH);
                            }

                            const result = await prince.execute();
                        } catch (err) {
                            console.error(err);
                            logger.error('Could not convert to PDF (PrinceXML)', { templateId, projectId, message: err?.message, stderr: err?.stderr.toString(), stdout: err?.stdout?.toString() });
                            // return res.status(500).render('error', { message: err.message, stack: err.stack });
                            if (!existsSync(outFilename)) {
                                debugger;
                                throw new Error('Could not convert file to PDF (no pdf created)');
                            }
                        }
                        res.setHeader('Content-Disposition', `attachment; filename=manuscript.pdf`);
                        res.setHeader('Content-Type', 'application/pdf');
                        res.setHeader('Cache-Control', 'no-cache');

                        const streamFile = createReadStream(outFilename);
                        streamFile.pipe(res);

                        // Wait for streaming to finish before ending response
                        await new Promise((resolve, reject) => {
                            streamFile.on('end', resolve);
                            streamFile.on('error', reject);
                        });

                        return res.end();
                    }
                default:
                    return res.status(400).render('error', { message: 'Unknown format' });
            }

        } catch (err) {
            logger.error('Could not convert: ' + format, { projectId, templateId, message: err?.message, stderr: err?.stderr?.toString(), stdout: err?.stdout?.toString() });
            return res.status(500).render('error', { message: err.message, stack: err.stack });
        }
    };

    app.get('/export/snapshot/:projectId/:filename.zip', handleExport)
    app.get('/export/:format/:templateId/:projectId/:filename', handleExport);

    app.use('/export/csl', express.static(join(__dirname, './csl/')));
    app.use('/export/lens', express.static(join(distPath, 'lens')));
    app.use('/export/mathjax', express.static(join(nodeModulesPath, 'mathjax')));
    console.log(join(nodeModulesPath, 'mathjax'))
}
