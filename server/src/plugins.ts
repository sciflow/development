import { join } from "path";
import { PluginService, injector } from "./services";

export const registerPlugins = async (app) => {
    const pluginService: PluginService = injector.get(PluginService);
    
    app.get('/plugins/script/:name/:type.js', async (req, res) => {
        const name = req.params.name;
        const plugins = await pluginService.getLoadedPlugin();
        const path = await pluginService.getPluginScriptPath(name, req.params.type);
        if (!path) { return res.status(404).send('Not found'); }
        res.sendFile(path);
    });

    app.get('/plugins/list.json', async (req, res) => {
        const plugins = await pluginService.getPlugins();
        res.json(plugins);
    })

}