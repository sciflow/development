import 'reflect-metadata';
import { ReflectiveInjector } from 'injection-js';

import { ReferenceService } from './references';
import { ExportService } from './export';
import { DocumentService } from './document';
import { TemplateService } from './template';
import { PluginService } from './plugins';

const injector = ReflectiveInjector.resolveAndCreate([PluginService, TemplateService, DocumentService, ExportService, ReferenceService]);

export {
    ReferenceService,
    DocumentService,
    TemplateService,
    PluginService,
    ExportService,
    injector
};