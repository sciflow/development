import {
  decamelize,
  GenericConfiguration,
  getSchemaDefinitionsFromPath,
  buildTemplateSchema,
  readYAMLDocumentsFromFile,
  ComponentSchema,
} from '@sciflow/export';
import { existsSync, readFileSync } from 'fs';
import { readFile } from 'fs/promises';
import { sync } from 'glob';
import { Injectable } from 'injection-js';
import { join } from 'path';
import 'reflect-metadata';
import { loadTemplateFromDisk, readFiles } from '@sciflow/tdk';
import winston from 'winston';
import { JSONSchema7 } from 'json-schema';
import { readYAML } from 'libs/export/src/configuration';
import { SFNodeType } from '@sciflow/schema';
/**
 * We cache the schemas for kind since they do not change for the container lifetime.
 */
const schemasForKind: { [kind: string]: any } = {};
const { LOG_LEVEL = 'info' } = process.env;
const logger = winston.createLogger({
  level: LOG_LEVEL,
  format: winston.format.json(),
  transports: new winston.transports.Console({ level: LOG_LEVEL }),
});

@Injectable()
export class TemplateService {
  /**
   * Gets the schema definition for a given kind.
   */
  async getComponentSchema(
    kind: string,
    componentPath = join(process.cwd(), '..', 'dist', 'libs'),
  ) {
    if (schemasForKind[kind]) {
      return schemasForKind[kind];
    }
    let schemas = {};

    if (!existsSync(componentPath)) {
      console.warn('Component path did not exist', { componentPath, __dirname });
    }

    try {
      const defaultMetaDataSchema = getSchemaDefinitionsFromPath([kind], componentPath);
      if (defaultMetaDataSchema?.properties) {
        schemas = { ...schemas, ...defaultMetaDataSchema?.properties };
      }
    } catch (e) {
      logger.error('Failed to generate default schemas', { message: e.message });
      return null;
    }
    if (!schemas[kind]) {
      debugger;
      return null;
    }

    delete schemas[kind].component;
    delete schemas[kind].$id;

    const shapedSchema = this.createSchemaForKind(kind, schemas[kind]);
    schemasForKind[kind] = shapedSchema;
    return schemasForKind[kind];
  }

  /**
   * Gets the schema definition for a given kind (version 2).
   */
  async getComponentSchemaV2(
    kind: string,
    componentPath = join(__dirname, '..', 'libs', 'export'),
  ) {
    if (!existsSync(componentPath)) {
      console.warn('Component path did not exist', { componentPath, __dirname });
    }

    const slug = decamelize(kind);
    const files = sync(`${componentPath}/components/**/${slug}.schema.@(json)`);

    if (files.length === 0) {
      logger.warn('Could not find any components', {
        componentPath,
        search: '/@(components|renderers)/**/*.schema.@(ts|tsx)',
        slug,
      });
      return;
    }

    try {
      const fileContent = await readFile(files[0], 'utf-8');
      const schema = JSON.parse(fileContent);
      return schema;
    } catch (e) {
      logger.error('Could not get program files', { e });
      throw new Error('Could not read program files: ' + e.message + ' / ' + files[0]);
    }
  }

  /** Retrieves the template without generating a schema (faster) */
  getTemplateWithoutSchema(templateSlug: string) {
    const templateSource = process.env.TEMPLATE_SOURCE;
    if (!templateSource || !existsSync(templateSource)) {
      return null;
    }
    const result = loadTemplateFromDisk(templateSource, templateSlug);
    if (!result) {
      return result;
    }

    return {
      slug: templateSlug,
      title: result.configuration?.spec.title,
      description: result.configuration?.spec.description,
      type: result.configuration?.spec.type,
      readme: result.configuration?.spec.readme,
      configurations: result.configurations,
      assets: result.assets || [],
      hidden: result.configuration?.spec?.hidden === true,
    };
  }

  /**
   * Retrieves the template including the schema.
   */
  async getTemplateWithSchema(templateSlug: string, locale?: string, variants?: string[]) {
    const template = this.getTemplateWithoutSchema(templateSlug);
    if (!template) {
      return template;
    }

    const legacyMetaDataSchemaPaths = template.assets.find((asset) =>
      asset.endsWith('metaData.schema.yml'),
    );
    const { template: metaData, prosemirrorNodeSchemas } = await this.getTemplateSchema(
      template.configurations,
      locale,
      legacyMetaDataSchemaPaths,
      variants,
    );

    return {
      ...template,
      prosemirrorNodeSchemas,
      metaData,
    };
  }

  /**
   * Retrieves the template schema as JSONSchema7.
   * @param templateSlug the slug
   * @param locale optional locale (defaults to the template default locale or en-US)
   * @returns the JSON schema or null if the template was not found
   */
  private async getTemplateSchema(
    configurations: GenericConfiguration<any>[],
    locale?: string,
    metaDataSchemaPaths?: string,
    variants?: string[],
  ): Promise<{ template: JSONSchema7; prosemirrorNodeSchemas: { [key: string]: object } }> {
    const {
      schema: template,
      configurations: _configurations,
      prosemirrorNodeSchemas,
    } = await buildTemplateSchema(configurations, locale, variants);

    if (metaDataSchemaPaths) {
      // we only use this if meta data was not specified through the standard meta data component
      // It's called template meta data because only one schema may exist per template.
      const templateMetaData: ComponentSchema = {
        title: 'Template Meta data',
        ...readYAML(metaDataSchemaPaths),
        $id: '/template/schema/template-meta-data',
        component: {
          kind: 'TemplateMetaData',
        },
      };

      const templateMetaDataSchema = {
        ...template,
        properties: {
          ...template.properties,
          'TemplateMetaData': templateMetaData,
        },
      };

      return { template: templateMetaDataSchema, prosemirrorNodeSchemas };
    }

    return { template, prosemirrorNodeSchemas };
  }

  /**
   * Create a schema object for the given kind.
   */
  private createSchemaForKind(kind: string, spec: any) {
    return {
      $schema: 'http://json-schema.org/draft-07/schema#',
      $id: `/template/configuration/${kind}.json`,
      type: 'object',
      properties: {
        kind: {
          type: 'string',
          enum: [kind],
        },
        page: {
          type: 'string',
        },
        runners: {
          type: 'array',
          items: {
            type: 'string',
            enum: ['princexml', 'html', 'epub', 'pagedjs', 'xml'],
          },
        },
        spec,
      },
      required: ['kind', 'spec'],
      additionalProperties: true,
    };
  }
}
