import { generateListings } from '@sciflow/export';
import { assignIds, parsePandocAST, proseMirrorToHTML } from '@sciflow/import';

import { sync } from 'glob';

import { spawnSync } from 'child_process';
import fs, { existsSync, lstatSync, mkdirSync, readFile, readFileSync, readdirSync, writeFileSync } from 'fs';
import { dirname, join } from 'path';
import 'reflect-metadata';
import { mediaStats } from '.';
import JSZip from 'jszip';
import { readBibFile } from 'libs/cite/src/pandoc';

/**
 * Extracts a ZIP archive containing a tex manuscript and converts it into a SciFlow document.
 */
export const convertTeXZIP = async (sourceFile: string, workdir: string) => {
    const zip = new JSZip();
    const content = await zip.loadAsync(readFileSync(sourceFile));
    // find the first folder path
    let manuscript = await content.file('manuscript.tex')?.async('string');

    let prefix = ''
    const entryFile = join(prefix, 'manuscript.tex');
    manuscript = await content.file(entryFile)?.async('string');

    if (!manuscript) {
        prefix = Object.keys(content.files).filter(path => path.endsWith('/'))?.[0];
        manuscript = await content.file(join(prefix, entryFile))?.async('string');
    }

    if (!manuscript) {
        throw new Error('manuscript.tex must exist in the archive');
    }

    console.log('Reading manuscript from ' + prefix, workdir);
    for (let path of Object.keys(content.files)) {
        const file = await content.file(path)?.async('array');
        if (file) {
            const dir = dirname(join(workdir, path));
            // make sure directories exist
            fs.mkdirSync(dir, { recursive: true });
            console.log('Writing ' + join(workdir, path));
            writeFileSync(join(workdir, path), Buffer.from(file));
        }
    }

    let documentDir = join(workdir, prefix);
    // convert tex file to Pandoc AST JSON
    const commands = [
        '-s', 'manuscript.tex',
        '-o', 'data.json',
        '--from', 'latex',
        '--citeproc',
        '-t', 'json'
    ];

    console.log('Running TeX conversion', { commands, documentDir });

    const { signal, status, stderr, stdout } = spawnSync(
        'pandoc',
        commands,
        { cwd: documentDir }
    );

    console.log(stdout?.toString());
    console.error(stderr?.toString());

    if (signal === 'SIGKILL') {
        throw new Error('Pandoc quit unexpectedly');
    }

    const result = join(documentDir, 'data.json');
    const output = existsSync(result);
    if (!output) { throw new Error('Could not read document data'); }

    let imageFiles = sync(join(documentDir, '**', '*.@(jpeg|jpg|png|webp|gif|avif)'))
        .filter((path => lstatSync(path).isFile()));
    let media = imageFiles
        .map(path => path.replace(documentDir.endsWith('/') ? documentDir : documentDir + '/', ''));
        const assets: any[] = [];
        for (let file of media) {
            try {
                const stats = await mediaStats(join(documentDir, file));
                assets.push({
                    id: file?.replaceAll('/', '_'),
                    key: file,
                    stats
                });
            } catch (e) {
                console.error('Could not process asset', { message: e.message });
                assets.push({
                    id: file?.replaceAll('/', '_'),
                    key: file,
                    stats: {}
                });
            }
        }

    const pandocErrors = stderr.toString().split('\n').filter(s => s.length > 0);
    const logs = stdout.toString().split('\n').filter(s => s.length > 0);
    const pandocAST = JSON.parse(fs.readFileSync(result, 'utf-8'));
    let { doc, references, schema, errors: renderErrors, referenceFiles } = await parsePandocAST(pandocAST, { mediaDir: documentDir, files: assets });
    if (!schema) { throw new Error('Schema must exist'); }
    if (referenceFiles) {
        for (let file of referenceFiles) {
            if (existsSync(join(documentDir, file))) {
                const refs = readBibFile(readFileSync(join(documentDir, file), 'utf-8'));
                if (refs) {
                    references = [...(references || []), ...refs];
                }
            } else {
                // TODO log an error here
                console.error('Reference file did not exist: ' + file);
            }
        }
    }
    doc = assignIds(doc, schema);
    const listings = await generateListings(doc?.toJSON());

    return {
        document: doc?.toJSON(),
        authors: [],
        assets,
        listings: [],
        lastModified: null,
        references,
        errors: [...pandocErrors, ...renderErrors]
    };
}