import { existsSync, readFileSync } from 'fs';
import { extname } from 'path';
import sharp from 'sharp';

/** Retrieves stats for files supported by sharp */
export const mediaStats = async (path: string) => {
    let extension;
    try {
        extension = extname(path)?.replace('.', '');
        if (!existsSync(path)) { throw new Error('File does not exist: ' + path); }
        if (extension === 'pdf_tex') {
            // TODO log an error here
            console.error('Not transforming pdf_tex', path);
            return null;
        }
        let image = await sharp(readFileSync(path)).withMetadata();
        const metaData = await image.metadata();
        return {
            width: metaData.width,
            height: metaData.height,
            format: metaData.format,
            dpi: metaData.density,
            size: metaData.size
        };
    } catch (e: any) {
        return null;
    }
};
