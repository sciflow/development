import 'reflect-metadata';

import { assignIds, ImportError, parsePandocAST, readDocx } from '@sciflow/import';
import { PluginError, processPluginsByEvents } from '@sciflow/plugin';
import { parse as parseJSON, schemas, SimplifiedManuscriptFile } from '@sciflow/schema';
import { listObjects, s3ToBuffer, s3ToStream } from '@sciflow/support';

import JSZip from 'jszip';

import { spawnSync } from 'child_process';
import fs, { createReadStream, existsSync, readdirSync, readFileSync, ReadStream } from 'fs';
import { GraphQLError } from 'graphql';
import { Injectable } from 'injection-js';
import { PandocNode } from 'libs/import/src/pandoc-renderers';
import { basename, dirname, extname, join } from 'path';
import { dirSync } from 'tmp';
import winston from 'winston';
import { injector } from '.';
import { mediaStats } from './handlers';
import { convertTeXZIP } from './handlers/tex';
import { PluginService } from './plugins';
const { LOG_LEVEL = 'info' } = process.env;
const logger = winston.createLogger({ format: winston.format.json(), transports: new winston.transports.Console({ level: LOG_LEVEL }), level: LOG_LEVEL });

@Injectable()
export class DocumentService {

    private pluginService: PluginService = injector.get(PluginService);

    async getManuscript(key, version, options?: { processCitations: boolean, shiftHeadingsBy: number, events?: string[]; errors?: any[]; }) {
        try {
            logger.info('Requesting file (json)', { version, key, events: options?.events });
            const result = await this.processFile({ key, version, processCitations: options?.processCitations, shiftHeadingsBy: options?.shiftHeadingsBy });
            let { manuscript, docxStats, errors, intelligence } = result;
            if (!errors) { errors = []; }
            // we only transform documents for now but could also tranform files and similar here
            const schema = schemas.manuscript;

            if (errors?.some(e => e.level === 'fatal')) {
                logger.error('Found errors', errors);

                // TODO display ui errors
                const errorsForUi = errors.map(error => {
                    if (error instanceof ImportError) {
                        if (error.payload?.pandocNode) {
                            try {
                                const renderedNode = this.renderPandocNode(error.payload?.pandocNode);
                                const prefix = `/import/extract-asset/${key}`;
                                error.payload.pandocNodeHTML = renderedNode?.replace(
                                    /<img([^>]*)\ssrc=(['"])(?:[^'"]*\/)*([^'"]+)\2/gi,
                                    (_match, p1, p2, p3) => `<img${p1} src=${p2}${prefix}/${p3}${p2}`
                                );
                                console.log(error.payload.pandocNodeHTML);
                            } catch (e: any) {
                                console.error('Could not render preview', e, error.payload?.pandocNode);
                            }
                        }
                    }
                    return error;
                });
                throw new GraphQLError('Could not convert document', {
                    extensions: {
                        code: 'PANDOC_CONVERSION_ERROR',
                        errors: errorsForUi
                    }
                });
            }

            let logs: any[] = [...errors.flat().map(e => {
                if (e instanceof ImportError) {
                    return {
                        level: 'error', // if the error does not override this, let's asume error (not fatal)
                        ...(e.toJSON())
                    };
                }
                return e;
            })];
            let events = options?.events || [];

            if (manuscript.document) {
                const plugins = await this.pluginService.getLoadedPlugin('TransformDocumentPlugin');
                const result = await processPluginsByEvents(manuscript, { events, schema, docxStats, intelligence, plugins, logger });
                manuscript = result?.manuscript;
                if (result?.logs) { logs.push(...result.logs); }

                if (!manuscript.document && !manuscript.html) {
                    logger.error('Did not find document in ' + Object.keys(manuscript).join(', '));
                    throw new GraphQLError('File must contain document');
                }

                const listings = manuscript.listings;
                delete manuscript.listings;

                return {
                    file: manuscript,
                    errors,
                    listings,
                    logs
                };

            }
        } catch (e: any) {
            logger.error('Could not retrieve manuscript', { type: e.type, message: e.message, payload: e.payload || e?.extensions });
            if (e instanceof GraphQLError || e instanceof PluginError) {
                throw e;
            }

            throw new Error(e.type ? `Could not retrieve manuscript (${e.type})` : `Could not retrieve manuscript`);
        }
    }

    public async loadFromZip({ projectId, filename, version, assetName }): Promise<any> {
        logger.info('Processing asset from ZIP', { projectId, filename, version, assetName });
        let file;

        try {
            file = await s3ToBuffer({
                Key: `${projectId}/${filename}`
            });
        } catch (e: any) {
            console.error(e);
            throw new Error('Could not access file ' + filename);
        }

        const zip = new JSZip();
        const content = await zip.loadAsync(file);

        if (!content.file(assetName)) { return undefined; }
        return content.file(assetName)?.async('nodebuffer');
    }

    /** 
     * Returns a file directly from within a ZIP file.
     * (!) This will download the ZIP file each time a file is requested.
    */
    public async streamAssetFromZip({ projectId, filename, version, assetName }): Promise<any> {
        logger.info('Processing asset', { projectId, filename, version, assetName });
        let file;

        try {
            file = await s3ToBuffer({
                Key: `${projectId}/${filename}`
            });
        } catch (e: any) {
            console.error(e);
            throw new Error('Could not access file ' + filename);
        }

        const zip = new JSZip();
        const content = await zip.loadAsync(file);

        const manuscriptLocation = Object.keys(content.files).find(path => path.endsWith('manuscript.tex'));
        if (!manuscriptLocation) { return undefined; }
        let prefix = dirname(manuscriptLocation);
        if (prefix === '.') { prefix = ''; }

        //const prefix = Object.keys(content.files).filter(path => path.endsWith('/'))?.[0];
        const pathInZIP = join(prefix, assetName);
        return await content.file(pathInZIP)?.async('nodebuffer');
    }

    /** Extracts an asset from a file (like docx) */
    public async streamPandocAsset({ projectId, filename, version, assetName }): Promise<ReadStream> {
        logger.info('Processing asset', { projectId, filename, version, assetName });
        const dirObj = dirSync({ unsafeCleanup: true });
        const projectDir = join(dirObj.name, projectId);

        try {
            // extract the assets
            const sourceFile = join(projectDir, filename);
            fs.mkdirSync(projectDir, { recursive: true });
            const stream = fs.createWriteStream(sourceFile);
            try {
                await s3ToStream({
                    Key: `${projectId} / ${filename} `
                }, stream);
            } catch (e: any) {
                console.error(e);
                throw new Error('Could not access file ' + filename);
            }

            await new Promise((resolve) => stream.on('finish', resolve));
            let commands = [
                '-s', sourceFile,
                '-o', join(dirObj.name, 'data.json'),
                '--extract-media', projectDir,
                '-t', 'json'
            ];

            const { signal, status, stderr, stdout } = spawnSync(
                'pandoc',
                commands,
                { cwd: projectDir }
            );

            // we had issues where files were not ready if they were big
            await new Promise(resolve => setTimeout(resolve, 500));

            if (signal === 'SIGKILL') {
                throw new ImportError('Pandoc received SIGKILL', { reason: 'The document could not be opened', status: 500 });
            }

            if (!existsSync(join(projectDir, assetName))) {
                logger.debug('Asset not found', { assetName, projectId });
                throw new ImportError('File not found ' + assetName, { reason: 'Asset file not found', status: 404 });
            }

            const readStream = createReadStream(join(projectDir, assetName));
            readStream.on('close', () => {
                try {
                    dirObj.removeCallback();
                } catch (e) {
                    logger.error('Could not remove temp directory', { message: e.message, projectDir, assetName });
                }
            });
            return readStream;
        } catch (e: any) {
            if (e instanceof ImportError) { throw e; }
            logger.error('Could not stream file', { projectId, filename, version, assetName, message: e.message });
            dirObj.removeCallback();
            throw new ImportError('Asset streaming error: ' + e.message, { reason: 'An error occured reading the asset', status: 500 });
        }
    }

    private renderPandocNode(node: PandocNode | string) {
        if (typeof node === 'string') { node = JSON.parse(node); }
        const input = {
            "pandoc-api-version": [1, 22, 2, 1],
            "meta": {},
            "blocks": [{
                "t": "Plain",
                "c": [
                    node
                ]
            }]
        };
        const { signal, status, stderr, stdout } = spawnSync('pandoc', ['--from', 'json', '--to', 'html'], { input: JSON.stringify(input) });
        if (signal === 'SIGKILL') {
            throw new Error('Pandoc quit unexpectedly');
        }

        const errors = stderr.toString().split('\n').filter(s => s.length > 0);
        if (errors?.length > 0) {
            console.error('Could not render node preview', errors);
        }
        const logs = stdout.toString().split('\n').filter(s => s.length > 0);

        return logs.join(' ');
    }

    /**
     * Transforms a file from a source format into a SFO document.
     */
    private async processFile({ key, version, processCitations, shiftHeadingsBy = 0 }): Promise<any> {
        processCitations !== false; // undefined is true
        logger.info('Processing file', { key, version, processCitations });

        const projectId = key.split('/')[0];
        // we may get paths with / like: project/subdir/doc.json so we want to remove the subdir for processing.
        let fileName = basename(key.replace(projectId + '/', ''));
        const subPath = key.replace(projectId + '/', '').replace(fileName, '');
        const extension = extname(fileName).replace('.', '');
        const dirObj = dirSync({ unsafeCleanup: true });
        const sourceFile = join(dirObj.name, fileName);
        let intelligence: any = {};

        try {
            const directory = await listObjects(projectId);
            const file = directory.find(({ Key }) => Key === key);
            // look for a file that has additional context
            const annotation = directory.find(({ Key }) => Key === key + '.json');
            if (!file) {
                throw new GraphQLError('File not found', { extensions: { code: '404' } });
            }

            if (annotation) {
                const content = await s3ToBuffer({ Key: annotation.Key });
                intelligence = JSON.parse(content.toString());
            }

            const start = Date.now();

            const stream = fs.createWriteStream(sourceFile);
            const reqObj: any = { Key: key };
            if (version?.length > 0) {
                reqObj.VersionId = version;
            }
            try {
                await s3ToStream(reqObj, stream);
            } catch (err) {
                logger.error('Could not stream file', { projectId, message: err.message, err, reqObj });
                throw new GraphQLError('Could not convert file', { extensions: { code: '500' } });
            }
            await new Promise((resolve) => stream.on('finish', resolve));

            const endStream = Date.now();

            logger.info('Converting file', { projectId, fileName, key, dir: readdirSync(dirObj.name) });

            if (!existsSync(sourceFile)) {
                logger.error('Source file not found', { projectId, sourceFile });
                throw new GraphQLError('Source file not found', { extensions: { code: '404' } });
            }

            const dataFile = join(dirObj.name, 'manuscript.json');
            let commands = [
                '-s', sourceFile,
                '-o', dataFile,
                '-t', 'json'
            ];

            let mediaDir = join(dirObj.name, 'media');

            // TODO move this into individual functions
            switch (extension) {
                case 'zip':
                    return { manuscript: await convertTeXZIP(sourceFile, dirObj.name), intelligence };
                case 'json': {
                    const file: SimplifiedManuscriptFile = JSON.parse(fs.readFileSync(sourceFile, 'utf-8'));
                    try {
                        const pmDoc = parseJSON(file.document);
                        pmDoc.check();
                    } catch (e: any) {
                        throw new GraphQLError('Could not parse document: ' + e.message, { extensions: { code: '500' } });
                    }

                    dirObj.removeCallback();
                    return { manuscript: file, intelligence, errors: [] };
                }
                case 'bib':
                    {
                        const { signal, status, stderr, stdout } = spawnSync(
                            'pandoc',
                            ['-s', sourceFile, '-o', join(dirObj.name, 'data.json'), '-f', 'biblatex', '-t', 'csljson'],
                            { cwd: dirObj.name }
                        );

                        const errors = stderr.toString().split('\n').filter(s => s.length > 0);
                        if (fs.existsSync(join(dirObj.name, 'data.json'))) {
                            const references = JSON.parse(fs.readFileSync(join(dirObj.name, 'data.json'), 'utf-8'));
                            dirObj.removeCallback();
                            return {
                                manuscript: {
                                    references,
                                },
                                intelligence,
                                errors
                            };
                        } else {
                            dirObj.removeCallback();
                            throw new GraphQLError('Could not process bib file', { extensions: { code: '500' } });
                        }
                    }
                case 'docx':
                // fall through
                case 'odt':
                    commands.push(...[
                        '-f', `${extension}+styles${processCitations ? '+citations' : ''} `,
                        `--shift-heading-level-by=${shiftHeadingsBy} `,
                        '--extract-media', dirObj.name // media is appended automatically
                    ]);
                default:
                    {
                        let references = [];
                        try {
                            // we read references separately to force pandoc to produce CSL JSON
                            const result = spawnSync('pandoc', [
                                sourceFile,
                                `--from`, `docx+citations`,
                                `--to`, `csljson`
                            ], { cwd: dirObj.name });
                            const pandocReferenceErrors = result?.stderr.toString().split('\n').filter(s => s.length > 0);
                            const pandocReferenceLogs = result?.stdout.toString();
                            if (pandocReferenceErrors?.length > 0) {
                                logger.error('Could not read references (pandoc)', { errors: pandocReferenceErrors });
                            }
                            references = JSON.parse(pandocReferenceLogs);
                        } catch (e) {
                            logger.error('Could not read references', { message: e.message });
                        }

                        const dataFile = join(dirObj.name, 'manuscript.json');
                        const { signal, status, stderr, stdout } = spawnSync('pandoc', commands, { cwd: dirObj.name });
                        if (signal === 'SIGKILL') {
                            throw new Error('Pandoc quit unexpectedly');
                        }

                        let media = existsSync(mediaDir) ? readdirSync(mediaDir) : [];

                        /** Pandoc errors are thrown by Pandoc during conversion of the input format
                         *  (while render errors happen during interpreting the Pandoc AST) */
                        const pandocErrors = stderr.toString().split('\n').filter(s => s.length > 0);
                        const logs = stdout.toString().split('\n').filter(s => s.length > 0);

                        let errors: any[] = [...pandocErrors];

                        if (!existsSync(dataFile)) {
                            return {
                                errors
                            };
                        }

                        logger.info('Executed pandoc', { signal, status, errors, logs });
                        const pandocAST = JSON.parse(fs.readFileSync(dataFile, 'utf-8'));

                        let { doc, schema, errors: renderErrors } = await parsePandocAST(pandocAST, { mediaDir });
                        const endConversion = Date.now();

                        errors = [...errors, renderErrors];

                        logger.info('Converted file', { extension, totalTimeMs: endConversion - start, timeReadingFilesMs: endStream - start });

                        if (!schema || schema == null) { throw new Error('Schema must exist'); }
                        doc = assignIds(doc, schema);

                        const assets: any[] = [];

                        for (let file of media) {
                            try {
                                const stats = await mediaStats(join(mediaDir, file));
                                assets.push({
                                    id: file?.replaceAll('/', '_'),
                                    key: join('media', file),
                                    stats
                                });
                            } catch (e) {
                                console.error('Could not process asset', { message: e.message });
                                assets.push({
                                    id: file?.replaceAll('/', '_'),
                                    key: join('media', file),
                                    stats: {}
                                });
                            }
                        }

                        let docxStats;
                        try {
                            docxStats = await readDocx(readFileSync(sourceFile));
                        } catch (e) {
                            console.error(e);
                            logger.error('Could not read DOCX stats', { message: e.message });
                            debugger;
                        }

                        return {
                            docxStats,
                            errors,
                            intelligence,
                            manuscript: {
                                document: doc?.toJSON(),
                                lastModified: null,
                                references,
                                assets,
                                errors: [...pandocErrors, ...renderErrors]
                            }
                        };
                    }
            }
        } catch (err) {
            if (err.message?.includes('looks like multiple versions of prosemirror-model were loaded')) {
                logger.error('Please ensure that all prosemirror-model versions are up to date (or the same)');    
            }
            logger.error('Could not convert file', { projectId, message: err.message, err });
            debugger;
            if (err.code === 'ENOENT') {
                throw new GraphQLError('File not found', { extensions: { code: '404' } });
            }
            throw new GraphQLError('Could not convert file', { extensions: { code: '500' } });
        } finally {
            dirObj.removeCallback();
        }
    }
}