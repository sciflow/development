import 'reflect-metadata';
import { ReflectiveInjector, Injectable, Injector } from 'injection-js';

import { zoteroSchema } from '@sciflow/cite';

@Injectable()
export class ReferenceService {
    getZoteroItemType(cslName: string) {
        return zoteroSchema.csl.types[cslName] && zoteroSchema.csl.types[cslName][0];
    }

    getZoteroFieldName(cslFieldName: string) {
        if (cslFieldName === 'id') { return 'id'; }
        if (cslFieldName === 'type') { return 'type'; }

        return zoteroSchema.csl.fields.text[cslFieldName] && zoteroSchema.csl.fields.text[cslFieldName][0] ||
            zoteroSchema.csl.fields.date[cslFieldName] && zoteroSchema.csl.fields.date[cslFieldName][0] ||
            zoteroSchema.csl.names[cslFieldName];
    }

    getFieldTranslation(cslFieldName: string) {
        const zoteroFieldName = this.getZoteroFieldName(cslFieldName);
        if (!zoteroFieldName) { return null; }

        return zoteroSchema.locales['en-US'].fields[zoteroFieldName] || zoteroSchema.locales['en-US'].creatorTypes[zoteroFieldName];
    }

    getZoteroItemTypeTranslation(name: string): string {
        return zoteroSchema.locales['en-US'].itemTypes[name];
    }

    getItemTypeTranslation(cslName: string): string {
        // get zotero name for csl
        const type = this.getZoteroItemType(cslName);
        if (!type) { return cslName; }

        // we default to the first match for now
        return zoteroSchema.locales['en-US'].itemTypes[type];
    }

    getType(cslName: string) {
        const type = this.getZoteroItemType(cslName);
        if (!type) { return null; }

        const zoteroType = zoteroSchema.itemTypes.find(t => t.itemType === type);
        if (!zoteroType) { return null; }

        return zoteroType;
    }
}
