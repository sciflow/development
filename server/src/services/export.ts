import { convertToSnapshot, ExportOptions, extractTitle, renderEPUB, SingleDocumentExportData } from '@sciflow/export';
import { SimplifiedManuscriptFile } from '@sciflow/schema';
import { loadTemplate, defaultHTMLTemplate, defaultXMLTemplate } from '@sciflow/tdk';
import { existsSync, lstatSync, readdirSync, symlinkSync } from 'fs';
import { Injectable } from 'injection-js';
import { basename, join } from 'path';
import 'reflect-metadata';
import { fetchFromZotero, getDefaults } from '@sciflow/cite';
import JSZip from 'jszip';
import { s3ToBuffer } from '@sciflow/support';
import { TemplateService } from './template';
import { injector } from '.';

let { styleXML, localeXML } = getDefaults();

@Injectable()
export class ExportService {

    templateService: TemplateService = injector.get(TemplateService);

    getRunners(): string[] {
        const runners = ['pagedjs', 'html', 'xml', 'epub'];
        // we only use prince if there is a license present
        // TODO activate this again when pagedjs is in a better state
        //if (existsSync(process.env.PRINCE_LICENSE_PATH as string)) {
        runners.push('princexml');
        //}
        return runners;
    }

    async getTemplates() {
        let templates: any[] = [];
        if (process.env.TEMPLATE_SOURCE) {
            const entries = readdirSync(process.env.TEMPLATE_SOURCE);
            for (let entry of entries.filter(slug => slug.indexOf('_') !== 0)) {
                const path = join(process.env.TEMPLATE_SOURCE, entry);
                const stat = lstatSync(path);
                if (stat.isDirectory()) {
                    try {
                        const template = this.templateService.getTemplateWithoutSchema(entry);
                        templates.push({
                            slug: entry,
                            ...(template || {})
                        });
                    } catch (e) {
                        console.error('Could not load template ' + entry, { message: e.message });
                    }
                }
            }
            return templates;
        }

        return templates;
    }

    async createEPUBZIP(data: {
        projectId: string;
        templateId: string;
        runner?: string;
        manuscript: SimplifiedManuscriptFile;
        scripts: {
            src?: string;
            content?: string;
        }[];
        tmpPath: string;
        inline?: boolean;
        citationStyle?: string;
        locale?: string;
        debug?: boolean;
    }): Promise<Buffer> {

        if (process.env.TEMPLATE_SOURCE) {
            symlinkSync(process.env.TEMPLATE_SOURCE, join(data.tmpPath, 'templates'));
        }

        const componentPath = join(__dirname, '..', 'libs', 'export');
        if (!existsSync(componentPath)) {
            console.warn('Component path did not exist', { componentPath, __dirname });
        }

        const instance = process.env.INSTANCE_URL || 'http://localhost:3000';

        const template = await loadTemplate({
            projectId: data.projectId,
            templateId: data.templateId
        }, {
            componentPath,
            fontPath: process.env.FONT_PATH,
            fontUrlPrefix: process.env.FONT_PATH,
            templateDir: process.env.TEMPLATE_SOURCE,
            baseUrl: instance,
            runner: data.runner
        });

        const snapshot = convertToSnapshot({
            document: data.manuscript.document,
            title: extractTitle(data.manuscript.document),
            stylePaths: (template?.stylePaths || []).map(style => ({ inline: true, ...style })),
            files: [...(data.manuscript.files || []), ...(template.files || [])].map(f => {
                if (instance?.length > 0 && f.url?.startsWith('/export/asset')) {
                    f.url = instance + f.url;
                }

                return f;
            }),
            templateOptions: null,
            authors: data.manuscript.authors ?? [],
            configuration: (template?.configuration || []),
            configurations: (template?.configurations ?? []).map(c => JSON.parse(JSON.stringify(c))),
            references: data.manuscript.references ?? [],
            metaData: data.manuscript.metaData ?? {},
            metaDataSchema: template?.metaData
        }, {
            assetUrl: instance + '/export/asset/' + data.projectId + '/'
        });

        snapshot.parts = snapshot.parts.map(part => ({
            ...part,
            id: part.partId || part.id
        }));

        // default to provided style, or the template style or the default
        const citationStyleId = data?.citationStyle || template.configuration?.citationStyle?.id || 'apa';
        const locale = data?.locale || template.configuration?.language || 'en-US';
        styleXML = await fetchFromZotero(citationStyleId) || styleXML;
        const citationStyleXML = { [locale]: styleXML };

        const options: ExportOptions = {
            configurations: template.configurations,
            metaData: snapshot.metaData,
            metaDataSchema: template.metaData,
            customTemplateComponents: template.customTemplateComponents,
            customRenderers: template.customRenderers,
            stylePaths: template.stylePaths || [],
            assetBasePaths: [], // TODO
            inline: data?.inline,
            scripts: data?.scripts || [],
            citationStyleXML,
            localeXML: { [locale]: localeXML }, // TODO load different locales
            logging: undefined,
            runner: data?.runner,
            debug: data?.debug
        };

        return await renderEPUB(snapshot, options);
    }

    async createSnapshotZIP(data: {
        projectId: string;
        manuscript: SimplifiedManuscriptFile;
    }): Promise<Buffer> {
        const snapshot = convertToSnapshot({
            document: data.manuscript.document,
            title: extractTitle(data.manuscript.document),
            files: data.manuscript.files || [],
            templateOptions: null,
            citationStyleData: { styleXML, localeXML },
            authors: data.manuscript.authors ?? [],
            references: data.manuscript.references ?? [],
            metaData: data.manuscript.metaData ?? {}
        }, {
            assetUrl: 'assets'
        });

        const zip = new JSZip();
        if (snapshot.files?.length > 0) {
            const contentDir = zip.folder('assets');
            for (let asset of snapshot.files) {
                if (!asset.name) { continue; }
                const path = asset.name || decodeURIComponent(asset.id);
                try {
                    if (typeof path === 'string' && path?.length > 0) {
                        const buffer = await s3ToBuffer({ Key: `${data.projectId}/${path}`, VersionId: undefined });
                        contentDir?.file(path, buffer);
                        asset.url = join('assets', path);
                    }
                } catch (e) {
                    console.error('Failed to process ' + path, { message: e.message });
                }
            }
        }

        zip.file('index.json', JSON.stringify(snapshot));
        zip.file('mimetype', 'application/sfo.snapshot');

        return await zip.generateAsync({
            type: 'nodebuffer',
            mimeType: 'application/zip'
        });
    }

    async renderHTML(data: {
        projectId: string;
        templateId: string;
        runner?: string;
        manuscript: SimplifiedManuscriptFile;
        scripts: {
            src?: string;
            content?: string;
        }[];
        tmpPath: string;
        inline?: boolean;
        citationStyle?: string;
        locale?: string;
        debug?: boolean;
    }): Promise<Buffer> {

        if (process.env.TEMPLATE_SOURCE) {
            symlinkSync(process.env.TEMPLATE_SOURCE, join(data.tmpPath, 'templates'));
        }

        const componentPath = join(__dirname, '..', 'libs', 'export');
        if (!existsSync(componentPath)) {
            console.warn('Component path did not exist', { componentPath, __dirname });
        }

        const instance = process.env.INSTANCE_URL || 'http://localhost:3000';

        const template = await loadTemplate({
            projectId: data.projectId,
            templateId: data.templateId
        }, {
            componentPath,
            fontPath: process.env.FONT_PATH,
            fontUrlPrefix: process.env.FONT_PATH,
            templateDir: process.env.TEMPLATE_SOURCE,
            baseUrl: instance,
            runner: data.runner
        });

        const snapshot = convertToSnapshot({
            document: data.manuscript.document,
            title: extractTitle(data.manuscript.document),
            stylePaths: (template?.stylePaths || []).map(style => ({ inline: true, ...style })),
            files: [...(data.manuscript.files || []), ...(template.files || [])].map(f => {
                if (instance?.length > 0 && f.url?.startsWith('/export/asset')) {
                    f.url = instance + f.url;
                }

                return f;
            }),
            templateOptions: null,
            authors: data.manuscript.authors ?? [],
            configuration: (template?.configuration || []),
            configurations: (template?.configurations ?? []).map(c => JSON.parse(JSON.stringify(c))),
            references: data.manuscript.references ?? [],
            metaData: data.manuscript.metaData ?? {},
            metaDataSchema: template?.metaData
        }, {
            assetUrl: instance + '/export/asset/' + data.projectId + '/'
        });

        try {
            if (typeof template?.render === 'function') {
                return await template?.render(snapshot);
            }

            return await defaultHTMLTemplate(snapshot, template, { debug: data.debug, locale: data.locale, citationStyle: data.citationStyle, inline: data.inline === true, scripts: data.scripts || [], runner: data.runner });
        } catch (e: any) {
            console.error('Could not render', e);
            throw new Error('Could not render template');
        }
    }

    async renderXML(data: {
        projectId: string;
        templateId: string;
        runner?: string;
        manuscript: SimplifiedManuscriptFile;
        scripts: {
            src?: string;
            content?: string;
        }[];
        tmpPath: string;
        inline?: boolean;
        citationStyle?: string;
        locale?: string;
    }): Promise<string | undefined> {

        if (process.env.TEMPLATE_SOURCE) {
            symlinkSync(process.env.TEMPLATE_SOURCE, join(data.tmpPath, 'templates'));
        }

        const componentPath = join(__dirname, '..', 'libs', 'export');
        if (!existsSync(componentPath)) {
            console.warn('Component path did not exist', { componentPath, __dirname });
        }

        const instance = process.env.INSTANCE_URL || 'http://localhost:3000';

        const template = await loadTemplate({
            projectId: data.projectId,
            templateId: data.templateId
        }, {
            componentPath,
            fontPath: process.env.FONT_PATH,
            fontUrlPrefix: process.env.FONT_PATH,
            templateDir: process.env.TEMPLATE_SOURCE,
            baseUrl: instance,
            runner: data.runner
        });

        const snapshot = convertToSnapshot({
            document: data.manuscript.document,
            title: extractTitle(data.manuscript.document),
            stylePaths: (template?.stylePaths || []).map(style => ({ inline: true, ...style })),
            files: [...(data.manuscript.files || []), ...(template.files || [])].map(f => {
                if (instance?.length > 0 && f.url?.startsWith('/export/asset')) {
                    f.url = instance + f.url;
                }

                return f;
            }),
            templateOptions: null,
            authors: data.manuscript.authors ?? [],
            configuration: (template?.configuration || []),
            configurations: (template?.configurations ?? []).map(c => JSON.parse(JSON.stringify(c))),
            references: data.manuscript.references ?? [],
            metaData: data.manuscript.metaData ?? {},
            metaDataSchema: template?.metaData
        }, {
            assetUrl: instance + '/export/asset/' + data.projectId + '/'
        });

        try {
            if (typeof template?.render === 'function') {
                return await template?.render(snapshot);
            }

            const file = await defaultXMLTemplate(snapshot, template, { locale: data.locale, citationStyle: data.citationStyle, inline: data.inline === true, scripts: data.scripts || [], runner: data.runner });
            const zip = new JSZip();
            const content = await zip.loadAsync(file);
            const manuscript = await content.file('manuscript.xml')?.async('string');
            return manuscript;
        } catch (e: any) {
            console.error('Could not render', e);
            throw new Error('Could not render template');
        }
    }
}