import { existsSync, readFileSync } from "fs";
import { sync } from "glob";
import { Injectable } from "injection-js";
import yaml from 'js-yaml';
import { basename, extname, join } from "path";
import winston from "winston";

import { SFMarkType, SFNodeType, schemas } from '@sciflow/schema';
import { Node, Schema } from 'prosemirror-model';
import { EditorState } from 'prosemirror-state';
import { slugify } from "@sciflow/export";
import { loadPluginsFromDisk, mapPlugin } from "libs/plugin/src/helpers";
import { PluginAction, getActions, tag, patch } from "@sciflow/plugin";

const { LOG_LEVEL = 'info' } = process.env;
const { NAMESPACE, PLUGIN_DIRS } = process.env;
const functionNamespace = NAMESPACE && NAMESPACE !== 'default' ? '.' + NAMESPACE : '';
const logger = winston.createLogger({ format: winston.format.json(), transports: new winston.transports.Console({ level: LOG_LEVEL }), level: LOG_LEVEL });

interface Tag {
    key: string;
    value?: any;
}

@Injectable()
export class PluginService {

    plugins = this.loadPlugins();

    async getLoadedPlugin(kind?: 'TransformDocumentPlugin') {
        const plugins = await this.plugins;
        return plugins.filter(plugin => !kind || plugin.manifest.kind === kind);
    }

    async getPlugins(kind?: 'TransformDocumentPlugin') {
        const plugins = await this.getLoadedPlugin(kind);
        return plugins.map(mapPlugin)
    }

    async tag(event: string, input: { state: EditorState; manuscript: any; }): Promise<{ state?: EditorState; manuscript?: any; logs: any[]; }> {
        const plugins = await this.getServerPluginsFor(event);
        const result = await tag(event, input, { plugins, logger });
        return result;
    }

    /**
     * Gets all available actions given the plugins and the current document.
     */
    async getActions(doc: Node, event: string) {
        const schema = schemas.manuscript;
        let state = EditorState.create({
            doc,
            schema
        });

        const plugins = await this.getServerPluginsFor(event);
        const actions = await getActions(state, event, { plugins, logger });
        return actions;
    }

    async getPluginScriptPath(name: string, type: 'patch' | 'tag') {
        for (let PLUGIN_DIR of PLUGIN_DIRS?.split(',').map(s => s.trim()).filter(s => s?.length > 0) || []) {
            if (!PLUGIN_DIR) { continue; }
            const pluginDir = PLUGIN_DIR && existsSync(PLUGIN_DIR) ? PLUGIN_DIR : undefined;
            if (!pluginDir) { continue; }
            let plugins = pluginDir && sync(`${pluginDir}/${name}/plugin.yml`);
            if (plugins.length === 1) {
                const plugin = plugins[0];
                const manifestFile = readFileSync(plugin, 'utf-8');
                const manifest = yaml.load(manifestFile);
                const moduleLocation = plugin.replace('/plugin.yml', '');
                if (!manifest?.spec?.[type]?.type) { return undefined; }
                if (existsSync(join(moduleLocation, manifest?.spec?.[type]?.source))) {
                    return join(moduleLocation, manifest?.spec?.[type]?.source);
                }
            }
        }
        return undefined;
    }

    /**
     * Loads all available plugins from the provided directory.
     */
    private async loadPlugins() {
        if (!PLUGIN_DIRS) { return []; }
        const instances = await loadPluginsFromDisk(PLUGIN_DIRS, { logger });
        logger.info('Loaded plugins', { ids: instances.map(instance => ({ id: instance.id, priority: instance.priority })) });
        return instances;
    }

    private async getServerPluginsFor(event: string) {
        const plugins = await this.plugins;
        return plugins.filter(plugin => {
            if (!plugin.manifest?.runners?.includes('server')) { return false; }
            if (plugin.manifest.triggers.some(t => t.event === event)) { return true; }
            return false;
        });
    }
}