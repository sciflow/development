window.errors = [];

window.onunhandledrejection = (event) => {
    const el = document.createElement('div');
    el.setAttribute('style', 'margin: 2rem; border: 4px dashed orange; padding: 1rem;');
    el.innerHTML = `<h1 style="margin: 1rem 0 1.75rem 0; padding: 0; text-align: center;">A Pagedjs error occured</h2>
    <pre style="margin: 1rem 4rem;">${event?.reason?.message || 'Unknown error' }</pre>
    <p style="text-align: center; margin-top: 2rem;">
        <small>Please check the developer console for more or reach out to support</small>
    </p>
    `;
    document.body.append(el);
    console.error(event?.reason || event);
};

window.onerror = function (message, source, lineNumber, colno, error) {
    console.warn(`UNHANDLED ERROR: ${error.stack}`);
};