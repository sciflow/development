import { ImportError, convertImage } from '@sciflow/import';
import { spawnSync } from 'child_process';
import * as mime from 'mime';
import { basename, extname, join } from 'path';
import winston from 'winston';
const { LOG_LEVEL = 'info' } = process.env;

import { DocumentService, injector } from './services';

export const registerImport = (app) => {
    const logger = winston.createLogger({ format: winston.format.json(), level: LOG_LEVEL, transports: new winston.transports.Console({ level: LOG_LEVEL }) });
    console.log('Registering import', { LOG_LEVEL }, logger.level);
    const documentService: DocumentService = injector.get(DocumentService);
    
    try {
        const { signal, status, stderr, stdout } = spawnSync(
            'pandoc',
            ['-v']
        );
        const errors = stderr.toString().split('\n').filter(s => s.length > 0);
        const logs = stdout.toString().split('\n').filter(s => s.length > 0);
        logger.info('Checking pandoc', { signal, status, errors, logs });
    } catch (e: any) {
        logger.error('Could not initialize pandoc', { message: e.message });
    }
    
    const extractFile = async (req, res) => {
        const assetParams = {
            projectId: req.params.projectId,
            filename: req.params.filename,
            assetName: req.params.assetName,
            version: undefined
        };
        
        logger.debug('Processing request for file extraction', assetParams);

        try {
            let prefix;
            // docx keeps media assets inside the word folder
            if (assetParams.filename.endsWith('.docx')) {
                prefix = 'word';
            }

            let type = mime.getType(extname(req.params.assetName).replace('.', ''));
            const ext = extname(req.params.assetName);

            const searchParams = {
                ...assetParams,
                assetName: prefix ? join(prefix, assetParams.assetName) : assetParams.assetName
            };

            let file = await documentService.loadFromZip(searchParams);
            
            let filename = basename(req.params.assetName);
            logger.info('Extracting asset from DOCX ZIP', { filename, searchParams, type, exists: file != undefined });

            if (!file) {
                logger.debug('Asset not found', { filename, assetParams });
                return res.status(404).send('File not found in docx file: ' + filename);
            }
            const image = await convertImage(file, assetParams.assetName);
            if (image) {
                type = image?.type;
                let newExt = mime.getExtension(type) || ext;
                filename = filename.replace(ext, newExt);
                file = await image?.arrayBuffer();
            }

            res.setHeader('Content-Disposition', `inline; filename="${filename}"`);
            res.setHeader('Content-Type', type);
            res.send(Buffer.from(file));

            return res.end();
        } catch (e: any) {
            // if a file was not found it may have been transformed in a previous step
            if (e instanceof ImportError && e.payload?.status === 404) { return res.redirect(`/export/asset/${req.params.projectId}/${encodeURIComponent(req.params.assetName)}`); }
            logger.error('Could not stream asset', { message: e.message, assetParams, payload: e.payload });
            res.status(500).send('Could not stream asset: ' + e.message);
        }
    };

    /**
     * Extracts an asset (e.g. media file) from a source document to display before the import
     */
    app.use('/import/extract-asset/:projectId/:filename/:assetName', extractFile);
};
