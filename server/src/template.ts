import { TemplateService, injector } from './services';

/**
 * We provide a number of default exports that can be used without further installation
 * @param app the express app
 */
export const registerTemplates = (app) => {
  const templateService: TemplateService = injector.get(TemplateService);

  // Remove this API once migration to V2 is complete
  app.get('/template/configuration/:kind.json', async (req, res) => {
    const schema = await templateService.getComponentSchema(req.params.kind);
    if (!schema) {
      return res.status(404).send('Kind ' + req.params.kind + ' not found');
    }
    res.json(schema);
  });

  app.get('/template/configuration/v2/:kind.json', async (req, res) => {
    const schema = await templateService.getComponentSchemaV2(req.params.kind);
    if (!schema) {
      return res.status(404).send('Kind ' + req.params.kind + ' not found');
    }
    res.json(schema);
  });

  app.get(`/template/schema/:templateSlug.json`, async (req, res) => {
    try {
      const locale: string = req.query?.locale;
      const templateSlug: string = req.params.templateSlug;
      const variantQuery: string | string[]  = req.query?.variant;

      const variant: string[] | undefined= Array.isArray(variantQuery)
      ? variantQuery
      : variantQuery
        ? [variantQuery]
        : undefined;

      const template = await templateService.getTemplateWithSchema(templateSlug, locale, variant);
      if (!template?.metaData) { return res.status(404).end(); }

      return res.json(template.metaData);
    } catch (e: any) {
      if (e.message === 'Template not found') {
        return res.status(404).send(e.message);
      }
      return res.status(500).send(e.message);
    }
  });
};
