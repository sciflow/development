import { loadTemplate } from '@sciflow/tdk';
import { ExportService, injector, TemplateService } from '../services';

import template from './Template.graphql';
const exportService: ExportService = injector.get(ExportService);
const templateService: TemplateService = injector.get(TemplateService);

export const typeDefs = [template];
export const resolvers = {
  Query: {
    templates: async () => {
      const templates = await exportService.getTemplates();
      return templates.filter(t => t.hidden !== true);
    },
    template: async (_parent, { projectId, templateId, locale }) => {
      const template = await templateService.getTemplateWithSchema(templateId, locale);
      if (!template) { return null; }
      return {
        runners: exportService.getRunners(),
        ...template
      };
    },
  }
};
