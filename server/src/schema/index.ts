import GraphQLDateTime from 'graphql-type-datetime';
import { streamToS3, listObjects } from '@sciflow/support';
import { createId } from '@sciflow/schema';
import { slugify } from '@sciflow/export';
import { GraphQLJSON, GraphQLJSONObject } from 'graphql-type-json';
import winston from 'winston';
import { extname } from 'path';
import generate from 'boring-name-generator';

// @ts-ignore
import query from './Query.graphql';
import * as referenceDef from './reference';
import * as templateDef from './template';
// @ts-ignore
import relay from './Relay.graphql';
import { toGlobalId } from 'graphql-relay';
import { DocumentService, PluginService, injector } from '../services';
import GraphQLUpload from 'graphql-upload/GraphQLUpload.js';
import { GraphQLError } from 'graphql';
const { LOG_LEVEL = 'info' } = process.env;
const logger = winston.createLogger({ format: winston.format.json(), transports: new winston.transports.Console({ level: LOG_LEVEL }), level: LOG_LEVEL });

const documentService: DocumentService = injector.get(DocumentService);
const pluginService: PluginService = injector.get(PluginService);

const resolvers = [
    {
        Query: {
            plugins: async (_parent) => {
                const plugins = await pluginService.getPlugins('TransformDocumentPlugin');
                return plugins;
            },
            project: async (_parent, { projectId }) => {
                try {
                    if (projectId?.length < 10) { throw new Error('Invalid project id'); }
                    const resources = await listObjects(projectId);
                    if (resources.some(file => file.Key.endsWith('/access.json'))) { throw new Error('Access objects are not supported'); }
                    const size = resources.reduce((s, resource) => resource.Size + s, 0);
                    return {
                        id: toGlobalId('project', projectId),
                        projectId,
                        resources,
                        size
                    };
                } catch (e) {
                    logger.error('Could not read project ' + projectId);
                    return {
                        id: toGlobalId('project', projectId),
                        projectId,
                        size: 0,
                        resources: []
                    };
                }
            },
            document: async (_parent, { key, version, shiftHeadingsBy, processCitations, events }) => {
                try {
                    const result = await documentService.getManuscript(key, version, { shiftHeadingsBy, processCitations, events });
                    return {
                        id: toGlobalId('document', key + version),
                        key,
                        version,
                        listings: result?.listings,
                        manuscript: result?.file,
                        errors: result?.errors,
                        logs: result?.logs || []
                    }
                } catch (e) {
                    logger.error('Could not transform document', { message: e.message, payload: e.payload });
                    if (e instanceof GraphQLError) { throw e; }
                    return {
                        id: toGlobalId('document', key + version),
                        key,
                        version: null,
                        manuscript: null,
                        listings: null,
                        logs: [
                            {
                                level: 'fatal',
                                message: e.message || 'Could not transform document',
                                payload: e.payload
                            }
                        ]
                    };
                }
            }
        },
        Subscription: {
        },
        Mutation: {
            createProject: async (_parent, { input }) => {
                const projectId = input.projectId || generate({ number: true, words: 3, alliterative: false }).dashed;
                logger.info('Creating project', {
                    projectId
                });

                return {
                    projectId
                };
            },
            /* deleteFile: async (_parent, { input }) => {
                // TODO
            }, */
            uploadFile: async (_parent, { input }) => {
                const projectId = input.projectId || `${createId()}-${createId()}-${createId()}`;
                const subPath = input.subPath?.length > 0 ? '/' +  input.subPath + '/' : '/';
                let { filename, mimetype, createReadStream } = await input.file;

                logger.info('Processing upload', {
                    subPath,
                    filename,
                    mimetype
                });

                const ext = extname(filename)?.replace('.', '');
                filename = decodeURIComponent(filename.replace(ext, '')).split('/').map(segment => slugify(segment)).join('/') + '.' + ext;
                if (filename?.includes('..')) { throw new GraphQLError('Relative directories not allowed in filenames'); }

                const stream = createReadStream();
                const result = await streamToS3(projectId + subPath + filename, stream);

                logger.info('Processed uploaded file as', {
                    projectId,
                    etag: result?.ETag,
                    key: result?.Key,
                    filename,
                    mimetype
                });

                return {
                    projectId,
                    etag: result?.ETag,
                    key: result?.Key,
                    name: filename,
                    version: result?.VersionId ? {
                        ETag: result?.ETag,
                        VersionId: (result as any).VersionId
                    } : null
                };
            }
        },
        Upload: GraphQLUpload,
        DateTime: GraphQLDateTime,
        JSON: GraphQLJSON,
        JSONObject: GraphQLJSONObject
    },
    referenceDef.resolvers,
    templateDef.resolvers
];

const typeDefs = [query, relay, ...referenceDef.typeDefs, ...templateDef.typeDefs];

export {
    typeDefs,
    resolvers
}
