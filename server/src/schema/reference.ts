import { s3ToBuffer, listObjects } from '@sciflow/support';
import { ReferenceRenderer } from '@sciflow/cite';
import { extname } from 'path';

import reference from './Reference.graphql';
import { spawnSync } from 'child_process';
import { getDefaults } from '@sciflow/cite';
import { readBibFile } from 'libs/cite/src/pandoc';

const { NAMESPACE } = process.env;
const { styleXML, localeXML } = getDefaults();

export const typeDefs = [reference];
export const resolvers = {
    Query: {
        references: async (_parent, { projectId }) => {
            const resources = await listObjects(projectId);
            const bibFiles = resources.filter(({ Key }) => extname(Key) === '.bib' || extname(Key) === '.csl.json');
            let references: any = [];

            for (let file of bibFiles) {
                try {

                    const versionId = file.versions[0]?.VersionId;
                    const buffer = await s3ToBuffer({ Key: file.Key, VersionId: versionId });

                    const refs = readBibFile(buffer.toString());

                    const refRenderer = new ReferenceRenderer(styleXML, localeXML, refs, { format: 'text' });
                    const referenceMap = refRenderer.getBibliographyIdMap();
                    let items = refs.map((csl) => {
                        const rendered = referenceMap.find((ref) => ref.id === csl.id)?.plainCitation;
                        return {
                            title: rendered,
                            key: csl.id,
                            csl,
                            bib: rendered
                        }
                    });

                    references.push({ Key: file.Key, LastModified: file.LastModified, items });

                } catch (e: any) {
                    console.error(e);
                }
            }

            return references;
        },
    }
}