import 'reflect-metadata';
import { json, urlencoded } from 'body-parser';
import express from 'express';

import { ApolloServer } from '@apollo/server';
import graphqlUploadExpress from 'graphql-upload/graphqlUploadExpress.js';
import { expressMiddleware } from '@apollo/server/express4';

import { createServer } from 'http';
import { ApolloServerPluginDrainHttpServer } from "@apollo/server/plugin/drainHttpServer";
import helmet, { contentSecurityPolicy } from 'helmet';
import cors from 'cors';
import winston from 'winston';

import { registerExports } from './export';
import { ensureBucket } from '@sciflow/support';

import { typeDefs, resolvers } from './schema/index';

import { join } from 'path';
import { registerClient } from './client';
import { registerImport } from './import';
import { registerTemplates } from './template';
import { registerPlugins } from './plugins';
const { LOG_LEVEL = 'info' } = process.env;
const logger = winston.createLogger({ format: winston.format.json(), transports: new winston.transports.Console({ level: LOG_LEVEL }), level: LOG_LEVEL });
const allowedOrigins = process.env.ALLOWED_ORIGINS?.split(',').map(s => s.trim());

(async () => {
  const { LOG_LEVEL = 'info' } = process.env;
  const { PORT = 3000, SIDECAR_PORT = 3001 } = process.env;

  /*   if (SIDECAR_PORT) {
      import('../file-transform/src/server');
    } */

  const path = '/api/graphql';

  const httpServer = createServer((req, res) => app(req, res));
  const app = express();

  const connectSrc = ['\'self\'', 'wss:', '*.intercom.io'];
  const scriptSrc = ['\'self\'', '\'unsafe-eval\'', '\'unsafe-inline\'', 'cdn.jsdelivr.net', '*.intercom.io', '*.intercomcdn.com']
  if (process.env.SCIFLOW_APP_DOMAIN) {
    connectSrc.push(process.env.SCIFLOW_APP_DOMAIN);
    scriptSrc.push(process.env.SCIFLOW_APP_DOMAIN);
  }

  const server = new ApolloServer({
    csrfPrevention: true,
    typeDefs,
    allowBatchedHttpRequests: true,
    resolvers,
    plugins: [
      // Proper shutdown for the HTTP server.
      ApolloServerPluginDrainHttpServer({ httpServer }),

      // Proper shutdown for the WebSocket server.
      {
        async serverWillStart() {
          return {
            async drainServer() {
              // cleanup where needed
            },
          };
        },
      },
    ]
  });

  await server.start();

  // security middleware
  app.use(
    cors({
      credentials: true,
      origin: function (origin, callback) {
        if (!origin || allowedOrigins?.some(allowed => origin.indexOf(allowed) === 0)) {
          callback(null, true);
        } else {
          callback(null, false);
        }
      }
    }),
    helmet(),
    contentSecurityPolicy({
      useDefaults: true,
      directives: {
        // cdn.jsdelivr.net is needed for the GraphQL playground
        'script-src': scriptSrc,
        'style-src': ['\'self\'', 'https://fonts.googleapis.com', '\'unsafe-inline\'', 'cdn.jsdelivr.net'],
        'connect-src': connectSrc,
        'img-src': ['\'self\'', 'data:', 'blob:', '\'unsafe-inline\'', 'cdn.jsdelivr.net']
      }
    }),
    json({ limit: '500mb' }),
    urlencoded({ limit: '500mb', extended: true })
  );

  registerTemplates(app);
  registerExports(app);
  registerPlugins(app);
  registerImport(app);

  app.disable('x-powered-by');
  app.set('view engine', 'pug');
  app.set('views', join(process.cwd(), 'src/views'));

  app.use(graphqlUploadExpress({ maxFileSize: 500000000, maxFiles: 10 })); // 500mb
  app.use(
    path,
    expressMiddleware(server)
  );

  // we run this last since it contains a catchall
  registerClient(app);

  app.use((req, res, next) => {
    logger.debug('File not found', { path: req.url });
    return res.status(404).render('error', { message: 'File not found' });
  });

  httpServer.listen({ port: PORT }, () => {
    logger.info(`🚀 Server ready`, { INSTANCE_URL: process.env.INSTANCE_URL, PORT, LOG_level: LOG_LEVEL, TRANSFORM_IMAGE_URL: process.env.TRANSFORM_IMAGE_URL, S3_ENDPOINT: process.env.S3_ENDPOINT });
    ensureBucket().catch((e) => {
      logger.error('Fatal error: Could not access S3', { e: e.message });
      shutdown();
    });
  });

  process.on('unhandledRejection', (reason) => {
    logger.error('unhandled rejection', reason);
  });

  const shutdown = async () => {
    logger.info('Gracefully shutting down server');
    server.stop();
    httpServer.close();

    // start any cleanup jobs that are needed

    await new Promise((r) => setTimeout(r, 1000));
    logger.info('Shutting down http server ..');
    httpServer.close();
    await new Promise((r) => setTimeout(r, 1000));
    process.exit(0);
  }

  process.on('uncaughtException', async (err: any) => {
    if (err.payload?.code === 404) {
      // workaround until we have upgraded the s3 tools
      console.error('Caught a file error: ', err.message, err.payload);
      return;
    }

    console.trace();
    console.error(err);
    logger.error('Fatal: Caught unhandled exception', { message: err.message });
    await shutdown();
  });

  process.on('SIGTERM', async () => {
    logger.info('Received SIGTERM');
    await shutdown();
  });
})().catch((e) => {
  console.error('Error caught', e.message);
});
