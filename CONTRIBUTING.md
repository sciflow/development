# Contributors

## Core Contributors

- Frederik Eichler
- Peter Qian

## Special Thanks

Marijn Haverbeke for his great ProseMirror library and to John MacFarlane, Albert Krewinkel, and team for their work on Pandoc.

Special thanks to all other contributors and the open-source community.
